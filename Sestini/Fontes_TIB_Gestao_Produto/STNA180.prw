#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA180   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �CONFIGURACAO DE CAMPOS P/ INCLUSAO DE INDICADORES DE PRODUTO���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA180()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZ1')
oBrowse:SetDescription('Configura��o Indicadores Produto')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'		ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'		ACTION 'VIEWDEF.STNA180'	OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'			ACTION 'VIEWDEF.STNA180'	OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'			ACTION 'VIEWDEF.STNA180'	OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'			ACTION 'VIEWDEF.STNA180'	OPERATION MODEL_OPERATION_DELETE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStrCabec  := FWFormStruct(1, 'SZ1', {|cCampo| .T.})
Local oStrCampos := FWFormStruct(1, 'SZ2', {|cCampo| .T.})

Local oModel     := NIL
Local bCommit    := {|oModel| STNA180Commit(oModel)}

oModel:= MPFormModel():New('STNA180',, {|oModel| STNA180Pos(oModel)}, bCommit)

oModel:SetDescription('Configura��o Indicadores Produto')
oModel:SetVldActivate({|oModel| STNA180Pre(oModel)})

oModel:AddFields('mdlCabec', NIL, oStrCabec)
oModel:GetModel('mdlCabec'):SetDescription('Configura��o Indicadores Produto')
oModel:SetPrimaryKey({'Z1_FILIAL+Z1_TIPO+Z1_GRTRIB'})

oModel:AddGrid('mdlCampos', 'mdlCabec', oStrCampos) 
oModel:SetRelation('mdlCampos', {{'Z2_FILIAL','xFilial("SZ2")'}, {'Z2_TIPO', 'Z1_TIPO'}, {'Z2_GRTRIB', 'Z1_GRTRIB'}}, SZ2->(IndexKey(1))) 
oModel:GetModel('mdlCampos'):SetUniqueLine({'Z2_CAMPO'})

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel('STNA180')
Local oStrCabec  := FWFormStruct(2, 'SZ1', {|cCampo| .T. })
Local oStrCampos := FWFormStruct(2, 'SZ2', {|cCampo| .T. })


//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('viewCabec', oStrCabec, 'mdlCabec')
oView:EnableTitleView('viewCabec', 'Configura��o Indicadores Produto')

oView:AddGrid('viewCampos', oStrCampos, 'mdlCampos')
oView:EnableTitleView('viewCampos', 'Campos')

oView:CreateHorizontalBox('hdrCabec'	, 30)
oView:CreateHorizontalBox('grdCampos'	, 70)

oView:SetOwnerView('viewCabec'	, 'hdrCabec')
oView:SetOwnerView('viewCampos'	, 'grdCampos')


Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA180Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA180Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA180Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA180Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA180Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA180Commit(oModel)
Local lRet        := .T.
Default oModel    := FWModelActive()

// ---------------------------
// TRATA O DOCUMENTO VINCULADO
// ---------------------------
If oModel:GetOperation() == MODEL_OPERATION_DELETE

EndIf


// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	lRet := FwFormCommit(oModel)
EndIf

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA180Vld   �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza validacoes genericas no formulario                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA180Vld(cCpo)
Local lRet     := .T.
Local aAreaSX3 := SX3->(GetArea())

cCampo := ReadVar()

If AllTrim(cCampo) == 'M->Z2_CAMPO'
	SX3->(DbSetOrder(2))
	If !SX3->(DbSeek(M->Z2_CAMPO)) .Or. SX3->X3_ARQUIVO <> 'SBZ'
		Help(" ",1,"REGNOIS")
		lRet := .F.
	EndIf
EndIf	


//--RESTAURA AMBIENTE
RestArea(aAreaSX3)

Return(lRet)