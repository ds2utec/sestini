#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA121   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �SELECIONA ITENS A SEREM ENVIADOS P/ ATUALIZACAO CADASTRAL   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA121()
Private oMark := NIL

//-- Atualiza botoes
aRotina := MenuDefMark()

oMark := FWMarkBrowse():New()
oMark:SetAlias('SZP')
oMark:SetDescription('Matriz de Desenvolvimento de Produtos - Atualiza��o Cadastral')
oMark:SetFieldMark('ZP_OK')
oMark:DisableReport()
oMark:DisableConfig()
oMark:DisableSaveConfig()
oMark:SetFilterDefault("ZP_IDPROC == '" + SZO->ZO_IDPROC + "' .AND. ZP_STATUS = '0'")

//-- ATIVA CLASSE:
oMark:Activate()


Return NIL


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA121Env�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Processa o envio da atualizacao cadastral                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA121Env()
Local cMarca      := oMark:Mark()
Local aDados      := {}
Local aMsExecAuto := {}
Local aCanaisVnd  := {}
Local aEmails     := {}
Local nCountA     := 0
Local nCountB     := 0
Local nPosCodPrd  := 0
Local nPosDesPrd  := 0
Local nPosPerson  := 0
Local lRet        := .T.
Local aProdutos   := {}

If MsgYesNo('Confirma o envio dos itens selecionados para atualiza��o cadastral?')
	SZP->(DbSetOrder(1)) //--ZP_FILIAL+ZP_IDPROC
	SZP->(DbSeek(xFilial('SZP')+SZO->ZO_IDPROC))
	While !SZP->(Eof()) .And. SZP->(ZP_FILIAL+ZP_IDPROC) == xFilial('SZP')+SZO->ZO_IDPROC

		aDados      := {}
		aMsExecAuto := {}
		aEmails     := {}

		If oMark:IsMark(cMarca)
			Begin Transaction
				// ----------------------------
				// CRIA PROCESSO DE ATUALIZACAO
				// ----------------------------
				SZQ->(DbSetOrder(1)) //--ZQ_FILIAL+ZQ_CODIGO
				SZQ->(DbGoTop())
				While !SZQ->(Eof())
					If SZQ->ZQ_ATIVA == 'S'
						SZR->(DbSetOrder(1)) //--ZR_FILIAL+ZR_CODAREA+ZR_CODUSR
						SZR->(DbSeek(xFilial('SZR')+SZQ->ZQ_CODIGO))
						While !SZR->(Eof()) .And. SZR->(ZR_FILIAL+ZR_CODAREA) == xFilial('SZR')+SZQ->ZQ_CODIGO
							RecLock('SZT', .T.)
							SZT->ZT_PROCESS := GETSXENUM("SZT","ZT_PROCESS")
							SZT->ZT_IDPROC  := SZP->ZP_IDPROC
							SZT->ZT_ITEM    := SZP->ZP_ITEM
							SZT->ZT_DATALIM := dDataBase + SZQ->ZQ_SLA
							SZT->ZT_CODAREA := SZQ->ZQ_CODIGO
							SZT->ZT_CODRESP := SZR->ZR_CODUSR
							SZT->ZT_STATUS  := '0'
							SZT->ZT_CODPRO  := SZP->ZP_CODPRO
							SZT->ZT_GRUPO   := SZP->ZP_GRUPO
							SZT->ZT_COLECAO := SZP->ZP_COLEC
							SZT->(MsUnLock())
						
							ConfirmSX8()
						
							SZS->(DbSetOrder(1)) //--ZS_FILIAL+ZS_CODAREA+ZS_CODRESP+ZS_TABELA+ZS_CAMPO
							SZS->(DbSeek(xFilial('SZS')+SZR->(ZR_CODAREA+ZR_CODUSR)))
							While !SZS->(Eof()) .And. SZS->(ZS_FILIAL+ZS_CODAREA+ZS_CODRESP) == xFilial('SZS')+SZR->(ZR_CODAREA+ZR_CODUSR)
								RecLock('SZU', .T.)
								SZU->ZU_PROCESS := SZT->ZT_PROCESS
								SZU->ZU_CODRESP := SZR->ZR_CODUSR
								SZU->ZU_CAMPO   := SZS->ZS_CAMPO
								SZU->(MsUnLock())
							
								SZS->(DbSkip())
							End

							//-- TRATA DESTINATARIOS DO E-MAIL DE NOTIFICACAO
							aAdd(aEmails, AllTrim(SZR->ZR_EMAIL))

							SZR->(DbSkip())
						End
					EndIf
					SZQ->(DbSkip())
				End

				// ---------------------------------------
				// ALIMENTA VETOR P/ GRAVACAO DOS PRODUTOS
				// ---------------------------------------
				//-- PRODUTO "PAI"
				aDados := { {'B1_COD'		, SZP->ZP_CODPRO	, NIL},;
							{'B1_DESC'		, SZP->ZP_DESCPOR	, NIL},;
							{'B1_TIPO'		, 'ME'				, NIL},;
							{'B1_GRUPO'		, '0001'			, NIL},;
							{'B1_UM'		, SZP->ZP_UM		, NIL},;
							{'B1_LOCPAD'	, 'ALMOX'			, NIL},;
							{'B1_ORIGEM'	, '1'				, NIL},;
							{'B1_XTIPO'		, SZP->ZP_TIPO		, NIL},;
							{'B1_XCATEG'	, SZP->ZP_CATEG		, NIL},;
							{'B1_XCOLEC'	, SZP->ZP_COLEC		, NIL},; 
							{'B1_XFORNE'	, SZP->ZP_FORNEC	, NIL},; 
							{'B1_XPARTNU'	, SZP->ZP_PARTNUM	, NIL},;
							{'B1_XCOR1'		, SZP->ZP_COR1		, NIL},;
							{'B1_XCUSTO'	, SZP->ZP_CSTFOB	, NIL},;
							{'B1_XPRCVEN'	, SZP->ZP_PRCVEN	, NIL},;
							{'B1_XSAZON'	, SZP->ZP_SAZON		, NIL},;
							{'B1_XPERCIN'	, SZP->ZP_PERCINS	, NIL},;
							{'B1_XGENE'		, SZP->ZP_GENERO	, NIL},;
							{'B1_XTAM'		, SZP->ZP_TAMANHO	, NIL},;
							{'B1_XPERSON'	, SZP->ZP_PERSON	, NIL},;
							{'B1_XESTILO'	, SZP->ZP_ESTVIDA	, NIL},;
							{'B1_XMATER1'	, SZP->ZP_MATLPRC	, NIL},;
							{'B1_XMATER2'	, SZP->ZP_MATLPUX	, NIL},;
							{'B1_XDESCB'	, SZP->ZP_DESBRIN	, NIL},;
							{'B1_XINMET'	, SZP->ZP_INMETRO	, NIL},;
							{'B1_POSIPI'	, SZP->ZP_NCM		, NIL},;
							{'B1_XMONTA'	, SZP->ZP_PRDMONT	, NIL},;
							{'B1_XEXCLUS'	, SZP->ZP_PRODEXC	, NIL},;
							{'B1_XGRPEMP'	, SZP->ZP_GRPEMPR	, NIL},;
							{'B1_XCTB'		, SZP->ZP_CTRLCTB	, NIL},;
							{'B1_XLOCPAD'	, SZP->ZP_GRPCQ		, NIL} }
				aAdd(aMsExecAuto, aDados)

				//-- PRODUTOS "FILHO"
				For nCountA := 1 To 10
					If !Empty(&('SZP->ZP_COR' + If(nCountA < 10, AllTrim(Str(nCountA)), 'A')))
						aDados := { {'B1_COD'		, AllTrim(SZP->ZP_CODPRO) + '-' + &('SZP->ZP_COR' + If(nCountA < 10, AllTrim(Str(nCountA)), 'A'))	, NIL},;
									{'B1_DESC'		, AllTrim(SZP->ZP_DESCPOR) + ' ' + AllTrim(Posicione('SZE', 1, xFilial('SZE')+&('SZP->ZP_COR' + If(nCountA < 10, AllTrim(Str(nCountA)), 'A')), 'ZE_DESCR'))	, NIL},;
									{'B1_TIPO'		, 'ME'				, NIL},;
									{'B1_GRUPO'		, '0001'			, NIL},;
									{'B1_UM'		, SZP->ZP_UM		, NIL},;
									{'B1_LOCPAD'	, 'ALMOX'			, NIL},;
									{'B1_ORIGEM'	, '1'				, NIL},;
									{'B1_CODBAR'	, STNXCodBar()		, NIL},;
									{'B1_XTIPO'		, SZP->ZP_TIPO		, NIL},;
									{'B1_XCATEG'	, SZP->ZP_CATEG		, NIL},;
									{'B1_XCOLEC'	, SZP->ZP_COLEC		, NIL},; 
									{'B1_XFORNE'	, SZP->ZP_FORNEC	, NIL},; 
									{'B1_XPARTNU'	, SZP->ZP_PARTNUM	, NIL},;
									{'B1_XCOR1'		, &('SZP->ZP_COR' + If(nCountA < 10, AllTrim(Str(nCountA)), 'A'))	, NIL},;
									{'B1_XCUSTO'	, SZP->ZP_CSTFOB	, NIL},;
									{'B1_XPRCVEN'	, SZP->ZP_PRCVEN	, NIL},;
									{'B1_XSAZON'	, SZP->ZP_SAZON		, NIL},;
									{'B1_XPERCIN'	, SZP->ZP_PERCINS	, NIL},;
									{'B1_XGENE'		, SZP->ZP_GENERO	, NIL},;
									{'B1_XTAM'		, SZP->ZP_TAMANHO	, NIL},;
									{'B1_XPERSON'	, SZP->ZP_PERSON	, NIL},;
									{'B1_XESTILO'	, SZP->ZP_ESTVIDA	, NIL},;
									{'B1_XMATER1'	, SZP->ZP_MATLPRC	, NIL},;
									{'B1_XMATER2'	, SZP->ZP_MATLPUX	, NIL},;
									{'B1_XDESCB'	, SZP->ZP_DESBRIN	, NIL},;
									{'B1_XINMET'	, SZP->ZP_INMETRO	, NIL},;
									{'B1_POSIPI'	, SZP->ZP_NCM		, NIL},;
									{'B1_XMONTA'	, SZP->ZP_PRDMONT	, NIL},;
									{'B1_XEXCLUS'	, SZP->ZP_PRODEXC   , NIL},;
									{'B1_XGRPEMP'	, SZP->ZP_GRPEMPR	, NIL},;
									{'B1_XCTB'		, SZP->ZP_CTRLCTB	, NIL},;
									{'B1_XLOCPAD'	, SZP->ZP_GRPCQ		, NIL} }
						aAdd(aMsExecAuto, aDados)
					EndIf
				Next nCountA

				// -----------------------------
				// PROCESSA O CADASTRO DOS ITENS
				// -----------------------------
				For nCountA := 1 To Len(aMsExecAuto)
					nPosCodPrd := aScan(aMsExecAuto[nCountA], {|x| AllTrim(x[1]) == 'B1_COD'})
					nPosDesPrd := aScan(aMsExecAuto[nCountA], {|x| AllTrim(x[1]) == 'B1_DESC'})
					nPosLocPad := aScan(aMsExecAuto[nCountA], {|x| AllTrim(x[1]) == 'B1_LOCPAD'})
					nPosPerson := aScan(aMsExecAuto[nCountA], {|x| AllTrim(x[1]) == 'B1_XPERSON'})

					// ---------------------------
					// REALIZA CADASTRO DO PRODUTO
					// ---------------------------
					/*
					lMSErroAuto := .F.
					MsgRun('Realizando cadastro dos itens... ' + Chr(10) + Chr(13) + 'Produto: ' + aMsExecAuto[nCountA, nPosCodPrd, 2], 'Aguarde...',;
							{|| MSExecAuto({|x, y, z| MATA010(x, y)}, aMsExecAuto[nCountA], 3)} )
					*/
 					MsgRun('Realizando cadastro - Produto: ' + AllTrim(aMsExecAuto[nCountA, nPosCodPrd, 2]), 'Aguarde...',;
 							{|| lRet := STNA121GrvPrd(aMsExecAuto[nCountA])})
 			
					If !lRet //lMsErroAuto
						Exit

					Else
						// -------------------------------------------
						// ATUALIZA INFORMACOES COMPLEMENTO DE PRODUTO
						// -------------------------------------------
						SB5->(DbSetOrder(1)) //--B5_FILIAL+B5_COD
						If SB5->(DbSeek(xFilial('SB5')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
							RecLock('SB5', .F.)
							SB5->B5_ECLARGU := SZP->ZP_LARG
							SB5->B5_ECPROFU := SZP->ZP_COMPR
							SB5->(MsUnLock())
						EndIf

						// ----------------------------------------------------------
						// ALIMENTA TABELA DE RESTRICOES x PRODUTOS x CANAIS DE VENDA
						// ----------------------------------------------------------
						aCanaisVnd := {}
						For nCountB := 1 To 7 //--Sao tratados 07 canais de venda na Matriz de Desenv.
							If !Empty(&('SZP->ZP_CNLVND' + AllTrim(Str(nCountB))))
								aAdd(aCanaisVnd, &('SZP->ZP_CNLVND' + AllTrim(Str(nCountB))))
							EndIf
						Next nCountB

						ZC0->(DbSetOrder(1))
						ZC0->(DbGotop())
						While !ZC0->(Eof())
							If aScan(aCanaisVnd, ZC0->ZC0_LOCAL) > 0
								RecLock('SZ8', .T.)
								SZ8->Z8_FILIAL  := xFilial('SZ8')
								SZ8->Z8_PRODUTO := aMsExecAuto[nCountA, nPosCodPrd, 2]
								SZ8->Z8_CANAL   := ZC0->ZC0_LOCAL
								SZ8->(MsUnLock())
							EndIf
							ZC0->(DbSkip())
						End

						// --------------------------------------
						// GERA CADASTRO DE PRODUTOS MASTER (CDA)							
						// --------------------------------------
						AH8->(DbSetOrder(1)) //--AH8_FILIAL+AH8_PRODUT
						If AH8->(DbSeek(xFilial('AH8')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
							RecLock('AH8', .F.)
						Else
							AH8->(DbSetOrder(3)) //--AH8_FILIAL+AH8_CODPRO
							If AH8->(DbSeek(xFilial('AH8')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
								RecLock('AH8', .F.)
							Else
								RecLock('AH8', .T.)
							EndIf
						EndIf
						AH8->AH8_FILIAL := xFilial('AH8')
						AH8->AH8_PRODUT := aMsExecAuto[nCountA, nPosCodPrd, 2]
						AH8->AH8_DESCMA := aMsExecAuto[nCountA, nPosDesPrd, 2]
						AH8->AH8_CODPRO := aMsExecAuto[nCountA, nPosCodPrd, 2]								
						AH8->AH8_PERSON := aMsExecAuto[nCountA, nPosPerson, 2]
						AH8->(MsUnLock())

						// -----------------------------------							
						// TRATAMENTO P/ BLOQUEIO DOS PRODUTOS
						// -----------------------------------
						aAdd(aProdutos, aMsExecAuto[nCountA, nPosCodPrd, 2])


					EndIf
				Next nCountA

				If !lRet
					DisarmTransaction()

				Else
					// ------------------------------------------------------
					// ATUALIZA O STATUS DO ITEM NA MATRIZ DE DESENVOLVIMENTO
					// ------------------------------------------------------
					RecLock('SZP', .F.)
					SZP->ZP_STATUS := '1'
					SZP->(MsUnLock())

					// -----------------------------------							
					// TRATAMENTO P/ BLOQUEIO DOS PRODUTOS
					// -----------------------------------
					For nCountA := 1 To Len(aProdutos)
						SB1->(DbSetOrder(1))
						If SB1->(DbSeek(xFilial('SB1')+aProdutos[nCountA]))
							RecLock('SB1', .F.)
							SB1->B1_MSBLQL := '1'
							SB1->(MsUnLock())
						EndIf
					Next nCountA

					// ----------------------------
					// ENVIA E-MAILS DE NOTIFICACAO
					// ----------------------------
					For nCountA := 1 To Len(aEmails)
						MsgRun('Enviando e-mail de notifica��o...', 'Aguarde...', {|| STNA121Mail(aEmails[nCountA])})
					Next nCountA

				EndIf
			End Transaction
		EndIf
		SZP->(DbSkip())
	End

	If lRet
		MsgInfo('Processo realizado com sucesso!')
	Else
		MsgAlert('Problemas ao enviar itens p/ atualiza��o cadastral')
	EndIf

	oMark:GetOwner():End()

EndIf

Return



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDefMark()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'			ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Enviar itens'			ACTION 'STNA121Env'			OPERATION MODEL_OPERATION_UPDATE	ACCESS 0

Return(aRotina)




/*/
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA121Body  �Autor  �V. Raspa                                  ���
�����������������������������������������������������������������������������Ĵ��
���DESCRICAO �MONTA CORPO DA MENSAGEM                                         ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Static Function STNA121Mail(cDest)
Local cBody := ''

cBody := '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'
cBody += '<html xmlns="http://www.w3.org/1999/xhtml">'
cBody += '	<head>'
cBody += '		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'
cBody += '		<style type="text/css">'
cBody += '			.button { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; border: 1px ridge #CC6600; font-weight: bold; margin: 1px; padding: 10px; background-color: #ECEEEB; }'
cBody += '			.text   { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: 660000; text-decoration: none; font-style: normal; }'
cBody += '			.title  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color: 660000; text-decoration:none; font-weight: bold; }'
cBody += '			.table  { border-bottom:1px solid #999; border-right:1px solid #999; border-left:1px solid #999; border-top:1px solid #999; margin:1em auto; }'
cBody += '			.form0  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color:#FFF; text-decoration: none; font-weight: bold; background-color:#788EA7}'
cBody += '			.form1  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; color: #000000; text-decoration: none; font-weight: bold; background-color:#ECF0EE;}'
cBody += '			.form2  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #333333; text-decoration: none; background-color:#F7F9F8;}'
cBody += '			.form3  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; color: #333333; text-decoration: none; background-color:#F7F9F8; font-weight:bold}'
cBody += '			.form4  { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; color:#F00; text-decoration: none;}'
cBody += '			.links  { font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 660000; text-decoration:underline; font-style: normal;}'
cBody += '			.top_bg { margin:0; padding:0; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; text-decoration: none; font-weight: bold; text-align:center; color:#FFFFFF;background-color:#CC0000;}'
cBody += '		</style>'
cBody += '		<title>ATUALIZA&Ccedil;&Atilde;O CADASTRAL</title>'
cBody += '	</head>'
cBody += '	<body>'
cBody += '		<table width="90%" class="table" align="center">'
cBody += '			<tr>'
cBody += '				<td width="100%" align="center" class="top_bg" height="50px">ATUALIZA&Ccedil;&Atilde;O CADASTRAL</td>'
cBody += '			</tr>'
cBody += '		</table>'
cBody += '		<table width="90%" class="table" align="center">'
cBody += '			<tr>'
cBody += '				<td width="100%">'
cBody += '					<table width="100%" style="border-bottom:1px solid #999">'
cBody += '						<tr>'
cBody += '							<td width="100%" colspan="2" class="top_bg" height="50px">DADOS DO PRODUTO </td>'
cBody += '						</tr>'
cBody += '						<tr>'
cBody += '							<td width="15%" class="form1">Id. do processo </td>'
cBody += '							<td width="85%" class="form2"><strong>' + SZT->ZT_PROCESS + '</strong></td>'
cBody += '						</tr>'
cBody += '						<tr>'
cBody += '							<td width="15%" class="form1">Produto</td>'
cBody += '							<td width="85%" class="form2">' + SZP->ZP_CODPRO + ' - ' + SZP->ZP_DESCPOR + '</td>'
cBody += '						</tr>'
cBody += '					</table>'
cBody += '				</td>'
cBody += '			</tr>'
cBody += '			<tr>'
cBody += '				<td width="100%">&nbsp;</td>'
cBody += '			</tr>'
cBody += '		</table>'
cBody += '		<table width="90%" align="center">'
cBody += '			<tr>'
cBody += '				<td height="52">'
cBody += '					<div class="top_bg">'
cBody += '						<div>'
cBody += '							<center>'
cBody += '								<font size="1px" face="verdana">TOTVS Ibirapuera  - Copyright 2018. Todos os direitos reservados</font>'
cBody += '							</center>'
cBody += '						</div>'
cBody += '					</div>'
cBody += '				</td>'
cBody += '			</tr>'
cBody += '		</table>'
cBody += '	</body>'
cBody += '</html>'


//-- ENVIA E-MAIL:
STNXSendMail(cDest, '', '', 'NOVO PRODUTO DESENVOLVIDO!', cBody)

Return




/*/
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA121GrvPrd�Autor  �V. Raspa                                  ���
�����������������������������������������������������������������������������Ĵ��
���DESCRICAO �GRAVACAO DO PRODUTO                                             ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Static Function STNA121GrvPrd(aDados)
Local oModel     := NIL
Local nCountA    := 0
Local lRet       := .T.
Local aErro      := {}

// -------------------------
// INSTANCIA MODELO DE DADOS
// -------------------------
oModel  := FwLoadModel('MATA010')
oModel:SetOperation(MODEL_OPERATION_INSERT)
oModel:Activate()

// --------------------------
// ATRIBUI VALORES AOS CAMPOS
// --------------------------
For nCountA := 1 To Len(aDados)
	lRet := oModel:SetValue('SB1MASTER', aDados[nCountA, 1], aDados[nCountA, 2])
	If !lRet
		Exit
	EndIf
Next nCountA

// -----------------------
// VALIDA E GRAVA OS DADOS
// -----------------------
If lRet .And. oModel:VldData()
	oModel:CommitData()
Else
	lRet := .F.
	aErro := oModel:GetErrorMessage()
	Help(,, 'STNA121GrvPrd',, 'Ocorreu um erro ao efetivar a grava��o do item! - ' + chr(13) + aErro[6], 1, 0)
EndIf

Return(lRet)



