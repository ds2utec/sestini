#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA110   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �MANUTENCAO TIPOS x GRUPOS x CATEGORIAS x COLECOES           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA110()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZK')
oBrowse:SetDescription('Tipos X Grupos X Categorias X Cole��es')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'		ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'		ACTION 'VIEWDEF.STNA110'	OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'			ACTION 'VIEWDEF.STNA110'	OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'			ACTION 'VIEWDEF.STNA110'	OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'			ACTION 'VIEWDEF.STNA110'	OPERATION MODEL_OPERATION_DELETE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStrCabec  := FWFormStruct(1, 'SZK', {|cCampo| .T.})
Local oStrGrupos := FWFormStruct(1, 'SZL', {|cCampo| .T.})
Local oStrCateg  := FWFormStruct(1, 'SZM', {|cCampo| .T.})
Local oStrAtrib  := FWFormStruct(1, 'SZW', {|cCampo| .T.})
Local oStrColec  := FWFormStruct(1, 'SZN', {|cCampo| .T.})

Local oModel     := NIL
Local bCommit    := {|oModel| STNA110Commit(oModel)}

oModel:= MPFormModel():New('STNA110',, {|oModel| STNA110Pos(oModel)}, bCommit)

oModel:SetDescription('Tipos X Grupos X Categorias X Atributos X Cole��es')
oModel:SetVldActivate({|oModel| STNA110Pre(oModel)})

// --------------------------------------
// CABECALHO: TIPO DO PRODUTO
// --------------------------------------
oModel:AddFields('mdlCabec', NIL, oStrCabec)
oModel:GetModel('mdlCabec'):SetDescription('Tipos de Produto')
oModel:SetPrimaryKey({'ZK_FILIAL+ZK_TIPO'})

// --------------------------------------
// 1o GRID: GRUPO DE PRODUTO
// --------------------------------------
oModel:AddGrid('mdlGrupos', 'mdlCabec', oStrGrupos) 
oModel:SetRelation('mdlGrupos', {{'ZL_FILIAL','xFilial("SZL")'}, {'ZL_TIPO', 'ZK_TIPO'}}, SZL->(IndexKey(1))) 
oModel:GetModel('mdlGrupos'):SetUniqueLine({'ZL_GRUPO'})

// --------------------------------------
// 2o GRID: CATEGORIA DE PRODUTOS
// --------------------------------------
oModel:AddGrid('mdlCateg', 'mdlGrupos', oStrCateg) 
oModel:SetRelation('mdlCateg', {{'ZM_FILIAL','xFilial("SZM")'}, {'ZM_TIPO', 'ZK_TIPO'}, {'ZM_GRUPO', 'ZL_GRUPO'}}, SZM->(IndexKey(1))) 
oModel:GetModel('mdlCateg'):SetUniqueLine({'ZM_CATEG'})

// --------------------------------------
// 3o GRID: ATRIBUTOS
// --------------------------------------
oModel:AddGrid('mdlAtrib', 'mdlCateg', oStrAtrib) 
oModel:SetRelation('mdlAtrib', {{'ZW_FILIAL','xFilial("SZW")'}, {'ZW_TIPO', 'ZK_TIPO'}, {'ZW_GRUPO', 'ZL_GRUPO'}, {'ZW_CATEG', 'ZM_CATEG'}}, SZW->(IndexKey(1))) 
oModel:GetModel('mdlAtrib'):SetUniqueLine({'ZW_ATRIB'})


// --------------------------------------
// 4o GRID: COLECAO DE PRODUTOS
// --------------------------------------
oModel:AddGrid('mdlColec', 'mdlGrupos', oStrColec) 
oModel:SetRelation('mdlColec', {{'ZN_FILIAL','xFilial("SZN")'}, {'ZN_TIPO', 'ZK_TIPO'}, {'ZN_GRUPO', 'ZL_GRUPO'}}, SZN->(IndexKey(1))) 
oModel:GetModel('mdlColec'):SetUniqueLine({'ZN_COLEC'})


Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel('STNA110')
Local oStrCabec  := FWFormStruct(2, 'SZK', {|cCampo| .T. })
Local oStrGrupos := FWFormStruct(2, 'SZL', {|cCampo| .T. })
Local oStrCateg  := FWFormStruct(2, 'SZM', {|cCampo| .T. })
Local oStrAtrib  := FWFormStruct(2, 'SZW', {|cCampo| .T. })
Local oStrColec  := FWFormStruct(2, 'SZN', {|cCampo| .T. })


//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('viewCabec', oStrCabec, 'mdlCabec')
oView:EnableTitleView('viewCabec', 'Tipos de Produto')

oView:AddGrid('viewGrupos', oStrGrupos, 'mdlGrupos')
oView:EnableTitleView('viewGrupos', 'Grupos de Produto')

oView:AddGrid('viewCateg', oStrCateg, 'mdlCateg')
oView:EnableTitleView('viewCateg', 'Categorias de Produtos')

oView:AddGrid('viewAtrib', oStrAtrib, 'mdlAtrib')
oView:EnableTitleView('viewAtrib', 'Atributos')

oView:AddGrid('viewColec', oStrColec, 'mdlColec')
oView:EnableTitleView('viewColec', 'Cole��es')

oView:CreateHorizontalBox('hdrCabec'	, 10)
oView:CreateHorizontalBox('grdGrupos'	, 22)
oView:CreateHorizontalBox('grdCateg'	, 22)
oView:CreateHorizontalBox('grdAtrib'	, 22)
oView:CreateHorizontalBox('grdColec'	, 22)

oView:SetOwnerView('viewCabec'	, 'hdrCabec')
oView:SetOwnerView('viewGrupos'	, 'grdGrupos')
oView:SetOwnerView('viewCateg'	, 'grdCateg')
oView:SetOwnerView('viewAtrib'	, 'grdAtrib')
oView:SetOwnerView('viewColec'	, 'grdColec')


Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA110Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA110Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA110Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA110Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA110Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA110Commit(oModel)
Local lRet        := .T.
Default oModel    := FWModelActive()

// ---------------------------
// TRATA O DOCUMENTO VINCULADO
// ---------------------------
If oModel:GetOperation() == MODEL_OPERATION_DELETE

EndIf


// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	lRet := FwFormCommit(oModel)
EndIf

Return(lRet)
