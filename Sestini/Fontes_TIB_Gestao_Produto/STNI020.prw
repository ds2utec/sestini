#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TBICONN.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI020   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �INTEGRACOES RAKUTEN - CADASTRO DE PRODUTOS "PAI"            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI020(cProdDe, cProdAte)
Local aRestr     := {}
Local cDbCorp    := GetMV('ES_DBCORP')
Local nHandle    := 0
Local cQuery     := ''
Local cAliasQry  := ''
Local aCaract    := ''
Local cDimensoes := ''

Default cProdDe     := Repl(' ', Len(SB1->B1_COD))
Default cProdAte    := Repl('Z', Len(SB1->B1_COD))


//-- OBTEM OS ITENS COM RESTRICAO AO CANAL E-COMM
If IsBlind()
	aRestr := STNI020Restr(cProdDe, cProdAte)

Else
	MsgRun('Obtendo itens com restri��o ao canal de e-comm...', 'Aguarde...',;
			{|| aRestr := STNI020Restr(cProdDe, cProdAte)})

EndIf


//-- OBTEM OS PRODUTOS A SEREM ENVIADOS
cQuery := "SELECT SZP.*, SB1.* "
cQuery += "  FROM " + RetSQLTab('SZP')
cQuery += "  JOIN " + RetSQLTab('SB1')
cQuery += "    ON SB1.B1_FILIAL = '" + xFilial('SB1') + "' "
cQuery += "   AND SB1.B1_COD = SZP.ZP_CODPRO "
cQuery += "   AND SB1.D_E_L_E_T_ = ' ' "
cQuery += " WHERE SZP.ZP_FILIAL = '" + xFilial('SZP') + "' "
cQuery += "   AND SZP.ZP_CODPRO BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND SZP.D_E_L_E_T_ = ' ' " 
cQuery := ChangeQuery(cQuery)

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)


While !(cAliasQry)->(Eof())

	If aScan(aRestr, (cAliasQry)->ZP_CODPRO) == 0


		// ----------------------------------------------
		// TRATA CARACTERISTICAS DO PRODUTO
		// ----------------------------------------------
		aCaract := {	(cAliasQry)->ZP_CARAC01,;
						(cAliasQry)->ZP_CARAC02,;
						(cAliasQry)->ZP_CARAC03,;
						(cAliasQry)->ZP_CARAC04,;
						(cAliasQry)->ZP_CARAC05,;
						(cAliasQry)->ZP_CARAC06,;
						(cAliasQry)->ZP_CARAC07,;						
						(cAliasQry)->ZP_CARAC08,;		
						(cAliasQry)->ZP_CARAC09,;
						(cAliasQry)->ZP_CARACA }

		// ----------------------------------------------
		// TRATA DIMENSOES DO PRODUTO
		// ----------------------------------------------
		cDimensoes := ''
		If !Empty((cAliasQry)->ZP_LARGEXT)
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_LARGEXT)) + '<br />'
		Else
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_LARG)) + '<br />'
		EndIf

		If !Empty((cAliasQry)->ZP_ALTEXT)
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_ALTEXT)) + '<br />'
		Else
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_ALTURA)) + '<br />'
		EndIf

		If !Empty((cAliasQry)->ZP_PROFEXT)
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_PROFEXT)) + '<br />'
		Else
			cDimensoes += AllTrim(Str((cAliasQry)->ZP_COMPR)) + '<br />'
		EndIf


		// -----------------------------------------
		// TRATA GRAVACAO/ATUALIZACAO DO PRODUTO
		// -----------------------------------------
		If STNI020Seek('K_R_RK_ENVIO_PRODUTO_PAI', (cAliasQry)->ZP_CODPRO)
			//--QUANDO ENCONTRAR, FAZ UPDATE



		Else
			cQuery := "INSERT "
			cQuery += "  INTO " + cDbCorp + ".dbo.K_R_RK_ENVIO_PRODUTO_PAI "
			cQuery += "       (HANDLE, " 
			cQuery += "        ID, "
			cQuery += "        LOJACODIGO, "
			cQuery += "        CODIGOINTERNOFORNECEDOR, "
			cQuery += "        NOMEPRODUTO, "
			cQuery += "        TITULOPRODUTO, "
			cQuery += "        SUBTITULOPRODUTO, "
			cQuery += "        DESCRICAOPRODUTO, "
			cQuery += "        CARACTERISTICAPRODUTO, "
			cQuery += "        TEXTO1PRODUTO, "
			cQuery += "        TEXTO2PRODUTO, "
			cQuery += "        TEXTO3PRODUTO, "
			cQuery += "        TEXTO4PRODUTO, "
			cQuery += "        TEXTO5PRODUTO, "
			cQuery += "        TEXT06PRODUTO, "
			cQuery += "        TEXTO7PRODUTO, "
			cQuery += "        TEXTO8PRODUTO, "
			cQuery += "        TEXTO9PRODUTO, "
			cQuery += "        TEXTO10PRODUTO, "
			cQuery += "        NUMERO1PRODUTO, "
			cQuery += "        NUMERO2PRODUTO, "
			cQuery += "        NUMERO3PRODUTO, "
			cQuery += "        NUMERO4PRODUTO, "
			cQuery += "        NUMERO5PRODUTO, "
			cQuery += "        NUMERO6PRODUTO, "
			cQuery += "        NUMERO7PRODUTO, "
			cQuery += "        NUMERO8PRODUTO, "
			cQuery += "        NUMERO9PRODUTO, "
			cQuery += "        NUMERO10PRODUTO, "
			cQuery += "        CODIGOINTERNOENQUADRAMENTO, "
			cQuery += "        MODELOPRODUTO, "
			cQuery += "        PESOPRODUTO, "
			cQuery += "        PESOEMBALAGEMPRODUTO, "
			cQuery += "        ALTURAPRODUTO, "
			cQuery += "        ALTURAEMBALAGEMPRODUTO, "
			cQuery += "        LARGURAPRODUTO, "
			cQuery += "        LARGURAEMBALAGEMPRODUTO, "
			cQuery += "        PROFUNDIDADEPRODUTO, "
			cQuery += "        PROFUNDIDADEEMBALAGEMPRODUTO, "
			cQuery += "        ENTREGAPRODUTO, "
			cQuery += "        QUANTIDADEMAXIMAPORVENDA, "
			cQuery += "        STATUSPRODUTO, "
			cQuery += "        TIPOPRODUTO, "
			cQuery += "        PRESENTE, "
			cQuery += "        PRECOCHEIOPRODUTO, "
			cQuery += "        PRECOPOR, "
			cQuery += "        PERSONALIZACAOEXTRA, "
			cQuery += "        PERSONALIZACAOLABEL, "
			cQuery += "        ISBN, "
			cQuery += "        EAN13, "
			cQuery += "        YOUTUBECODE, "
			cQuery += "        ACAOID, "
			cQuery += "        DATACRIACAO, "
			cQuery += "        DATAATUALIZACAO, "
			cQuery += "        STATUSPROCESSAMENTOID, "
			cQuery += "        MENSAGEMPROCESSAMENTO, "
			cQuery += "        PROCESSAR, "
			cQuery += "        CODIGOINTERNOPRODUTO, "
			cQuery += "        USUARIO, "
			cQuery += "        PRECOPOR2 "
			cQuery += "        )"

			cQuery += " VALUES ("

			cQuery += AllTrim(Str(STNI020Prox('K_R_RK_ENVIO_PRODUTO_PAI', 'HANDLE'))) + ", "							//-- <HANDLE, int>
			cQuery += AllTrim(Str(STNI020Prox('K_R_RK_ENVIO_PRODUTO_PAI', 'ID'))) + ", "								//-- <ID, int>
			cQuery += "0, "																								//-- <LOJACODIGO, int>
			cQuery += "'01', "																							//-- <CODIGOINTERNOFORNECEDOR, varchar(24)>
			cQuery += "'" + Capital(AllTrim((cAliasQry)->B1_DESC)) + "', "												//-- <NOMEPRODUTO, varchar(128)>
			cQuery += "'', "																							//-- <TITULOPRODUTO, varchar(128)>
			cQuery += "'', "																							//-- <SUBTITULOPRODUTO, varchar(128)>
			cQuery += "'', "																							//-- <DESCRICAOPRODUTO, varchar(2000)>
			cQuery += "'" + STNI010Carac(aCaract) + "', "																//-- <CARACTERISTICAPRODUTO, varchar(2000)>	
			cQuery += "'" + cDimensoes + "', "																			//-- <TEXTO1PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO2PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO3PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO4PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO5PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXT06PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO7PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO8PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO9PRODUTO, varchar(2000)>
			cQuery += "'', "																							//-- <TEXTO10PRODUTO, varchar(2000)>
			cQuery += "0, "																								//-- <NUMERO1PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO2PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO3PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO4PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO5PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO6PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO7PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO8PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO9PRODUTO, int>
			cQuery += "0, "																								//-- <NUMERO10PRODUTO, int>
			cQuery += "'',"																								//-- <CODIGOINTERNOENQUADRAMENTO, varchar(24)>
			cQuery += "'" + AllTrim((cAliasQry)->B1_CODBAR) + AllTrim((cAliasQry)->B1_COD) + "', "						//-- <MODELOPRODUTO, varchar(50)>
			cQuery += AllTrim(Str((cAliasQry)->B1_PESBRU)) + ", "														//-- <PESOPRODUTO, float>
			cQuery += "0, "																								//-- <PESOEMBALAGEMPRODUTO, float>
			cQuery += (cAliasQry)->(If(!Empty(ZP_ALTEXT), AllTrim(Str(ZP_ALTEXT)), AllTrim(Str(ZP_ALTURA)))) + ", "		//-- <ALTURAPRODUTO, float>
			cQuery += "0, "																								//-- <ALTURAEMBALAGEMPRODUTO, float>
			cQuery += (cAliasQry)->(If(!Empty(ZP_LARGEXT), AllTrim(Str(ZP_LARGEXT)), AllTrim(Str(ZP_LARG)))) + ", "		//-- <LARGURAPRODUTO, float>
			cQuery += "0, "																								//-- <LARGURAEMBALAGEMPRODUTO, float>
			cQuery += (cAliasQry)->(If(!Empty(ZP_PROFEXT), AllTrim(Str(ZP_PROFEXT)), Alltrim(Str(ZP_COMPR)))) + ", "	//-- <PROFUNDIDADEPRODUTO, float>
			cQuery += "0, "																								//-- <PROFUNDIDADEEMBALAGEMPRODUTO, float>
			cQuery += "3, "																								//-- <ENTREGAPRODUTO, int>
			cQuery += "5, "																								//-- <QUANTIDADEMAXIMAPORVENDA, int>
			cQuery += "2, "																								//-- <STATUSPRODUTO, int>
			cQuery += "'1', "																							//-- <TIPOPRODUTO, varchar(1)>
			cQuery += "1, "																								//-- <PRESENTE, int>
			cQuery += "0, "																								//-- <PRECOCHEIOPRODUTO, decimal(15,2)>
			cQuery += "0, "																								//-- <PRECOPOR, decimal(15,2)>
			cQuery += "2, " 																							//-- <PERSONALIZACAOEXTRA, int>
			cQuery += "'', "																							//-- <PERSONALIZACAOLABEL, varchar(50)>
			cQuery += "'', "																							//-- <ISBN, varchar(13)>
			cQuery += "'" + (cAliasQry)->B1_CODBAR + "', "																//-- <EAN13, varchar(13)>
			cQuery += "' ', "																							//-- <YOUTUBECODE, varchar(50)>
			cQuery += "1, "																								//-- <ACAOID, int>
			cQuery += "GETDATE(), "																						//-- <DATACRIACAO, datetime>
			cQuery += "NULL, "																							//-- <DATAATUALIZACAO, datetime>
			cQuery += "NULL, "																							//-- <STATUSPROCESSAMENTOID, int>
			cQuery += "' ', "																							//-- <MENSAGEMPROCESSAMENTO, varchar(1000)>
			cQuery += "2, "																								//-- <PROCESSAR, int>
			cQuery += "'" + AllTrim((cAliasQry)->B1_COD) + "', "														//-- <CODIGOINTERNOPRODUTO, varchar(24)>
			cQuery += "'CadProd', "																						//-- <USUARIO, varchar(15)>
			cQuery += "0)"																								//-- <PRECOPOR2, float>

			If STNI010Conn(.F., cDbCorp, @nHandle)
				If TCSQLExec(cQuery) < 0
					ConOut("[RAKUTEN][INTEGRACOES] - ERRO AO ATUALIZAR CADASTRO PRODUTO PAI - " + TCSQLError())
				EndIf
				TCUnlink()
			EndIf

		EndIf
	EndIf
	(cAliasQry)->(DbSkip())
End

(cAliasQry)->(DbCloseArea())

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI020Restr�Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �OBTEM OS PRODUTOS C/ RESTRICAO AO CANAL E E-COMMERCE        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI020Restr(cProdDe, cProdAte)
Local cQuery       := ''
Local cAliasQry    := ''
Local aRestr       := {}

// -------------------------------------------------
// OBTEM OS PRODUTOS COM RESTRICAO P/ O CANAL E-COMM
// -------------------------------------------------
cQuery := "SELECT SZ8.Z8_PRODUTO, SZ8.Z8_CANAL "
cQuery += "  FROM " + RetSQLTab('SZ8')
cQuery += "  JOIN " + RetSQLTab('ZC0') 
cQuery += "    ON ZC0.ZC0_FILIAL = '" + xFilial('ZC0') + "' "
cQuery += "   AND ZC0.ZC0_LOCAL = SZ8.Z8_CANAL "
cQuery += "   AND ZC0.ZC0_TIPO = '2' "
cQuery += "   AND ZC0.D_E_L_E_T_ = ' ' "
cQuery += " WHERE SZ8.Z8_FILIAL = '" + xFilial('SZ8') + "' "
cQuery += "   AND SZ8.Z8_PRODUTO BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND SZ8.D_E_L_E_T_ = ' ' "
cQuery := ChangeQuery(cQuery)

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

(cAliasQry)->(DbEval({|| aAdd(aRestr, Z8_PRODUTO)}, {|| .T.}, {|| !Eof()}))

(cAliasQry)->(DbCloseArea())

Return(aRestr)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI020Seek �Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �VERIFICA SE O PRODUTO JA EXISTE NA TABELA                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI020Seek(cTable, cCodPro)
Local cQuery    := ''
Local cAliasQry := ''
Local cDbCorp   := GetMV('ES_DBCORP')
Local nHandle   := 0
Local lFound    := .F.

If STNI010Conn(.F., cDbCorp, @nHandle)
	cQuery := "SELECT CODIGOINTERNOPRODUTO "
	cQuery += "  FROM " + cDbCorp + ".dbo." + cTable
	cQuery += " WHERE RTRIM(CODIGOINTERNOPRODUTO) = '" + AllTrim(cCodPro) + "' "
	
	cAliasQry := GetNextAlias()
	DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)
	
	If !(cAliasQry)->(Eof())
		lFound := .T.
	Else
		lFound := .F.
	EndIf
	
	(cAliasQry)->(DbCloseArea())

	TCUnlink()

EndIf

Return(lFound)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI010Carac�Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �TRATA O ENVIO DAS CARACTERISTICAS                           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI010Carac(aCaract)
Local nCountA  := 0
Local cCaract  := ''
Local aAreaSZF := SZF->(GetArea())

//-- TRATA CARACTERISTICAS DO PRODUTO
cCaract := ''
For nCountA := 1 To Len(aCaract)
	If !Empty(aCaract[nCountA])
		SZF->(DbSetOrder(1)) //--ZF_FILIAL+ZF_CODIGO
		SZF->(DbSeek(xFilial('SZF')+aCaract[nCountA]))
		cCaract += Capital(AllTrim(SZF->ZF_DESCR)) + '<br />'
	EndIf
Next nCountA

//-- RESTAURA AMBIENTE:
RestArea(aAreaSZF)

Return(cCaract)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI020Prox �Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �OBTEM O PROXIMO SEQUENCIAL CONFORME CAMPO ENVIADO           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI020Prox(cTable ,cCpo)
Local nRet      := 0
Local cQuery    := ''
Local cAliasQry := ''
Local cDbCorp   := GetMV('ES_DBCORP')
Local nHandle   := 0

If STNI010Conn(.F., cDbCorp, @nHandle)
	cQuery := "SELECT MAX(" + cCpo + ") nMAX "
	cQuery += "  FROM " + cDbCorp + ".dbo." + cTable

	cAliasQry := GetNextAlias()
	DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

	nRet := (cAliasQry)->nMAX + 1
	
	(cAliasQry)->(DbCloseArea())

	TCUnlink()

EndIf
	
Return(nRet)	




