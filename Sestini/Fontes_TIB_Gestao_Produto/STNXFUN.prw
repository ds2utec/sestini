#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNXFUN   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �BIBLIOTECA DE FUNCOES UTILIZADAS NO PROJETO SESTINI         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNXNext  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Obtem o proximo codigo do cliente/fornecedor disponivel,    ���
���          �quando os mesmos NAO sao pessoa Fisica/Juridica             ���
���          �Funcao para ser acionada no gatilho dos campos A1_CGC|A2_CGC���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNXNext(cTable)
Local cAliasQry := GetNextAlias()
Local cRet      := ''
Local aArea     := GetArea()
Local aAreaSA1  := SA1->(GetArea())
Local aAreaSA2  := SA2->(GetArea())

If cTable == 'SA1'
	BeginSQL Alias cAliasQry
		SELECT MAX(SA1.A1_COD) CODIGO
		  FROM %Table:SA1% SA1
		 WHERE SA1.A1_FILIAL = %xFilial:SA1%
		   AND SA1.A1_TIPO   = 'X'
		   AND SA1.%NotDel%
	EndSQL

	cRet := Soma1((cAliasQry)->CODIGO)
	SA1->(DbSetOrder(1)) //--A1_FILIAL+A1_COD+A1_LOJA
	While SA1->(DbSeek(xFilial('SA1')+cRet))
		cRet := Soma1(cRet)
	End

ElseIf cTable == 'SA2'
	BeginSQL Alias cAliasQry
		SELECT MAX(SA2.A2_COD) CODIGO
		  FROM %Table:SA2% SA2
		 WHERE SA2.A2_FILIAL = %xFilial:SA2%
		   AND SA2.A2_TIPO   = 'X'
		   AND SA2.%NotDel%
	EndSQL

	cRet := Soma1((cAliasQry)->CODIGO)
	SA2->(DbSetOrder(1)) //--A2_FILIAL+A2_COD+A2_LOJA
	While SA2->(DbSeek(xFilial('SA2')+cRet))
		cRet := Soma1(cRet)
	End

EndIf

//-- RESTAURA AMBIENTE
RestArea(aArea)
RestArea(aAreaSA1)
RestArea(aAreaSA2)

//-- FECHA QUERY:
(cAliasQry)->(DbCloseArea())

Return(cRet)



/*/
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNXSendMail �Autor  �V. Raspa                                  ���
�����������������������������������������������������������������������������Ĵ��
���DESCRICAO �PROCESSA O ENVIO DO EMAIL DE NOTIFICACAO REFERENTE A ATUALIZACAO���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Function STNXSendMail(cDest, cCC, cCCo, cSubject, cBody)
Local lRet			:= .T.
Local cFrom		    := SuperGetMV('MV_RELFROM'	, .T., '')
Local cSMTPServer	:= SuperGetMV('MV_RELSERV'	, .T., '')
Local cSMTPUser 	:= SuperGetMV('MV_RELACNT'	, .T., '')
Local cSMTPPass 	:= SuperGetMV('MV_RELPSW'	, .T., '')
Local lTLS		 	:= SuperGetMV('MV_RELTLS'	, .T., .F.)
Local lUsaSSL		:= SuperGetMV('MV_RELSSL'	, .T., .F.)
Local lRelAuth 	    := SuperGetMV('MV_RELAUTH'	, .T., .F.)
Local nSMTPPort	    := SuperGetMV('MV_GCPPORT'	, .T., 25)
Local nTimeout      := SuperGetMV('MV_RELTIME'	, .T., 120)
Local oMail		    := NIL
Local oMessage  	:= NIL
Local nErro		    := 0
Local cErro         := ''

Default cCC         := ''
Default cCCo        := ''
Default cSubject    := ''


If !Empty(cDest)
	If At(':', cSMTPServer) > 0
		cSMTPServer := SubStr(cSMTPServer, 1, At(':', cSMTPServer)-1)
	EndIf

	// --------------------------------------------
	// INICIANDO CONEXAO COM O SERVIDOR DE E-MAILS 
	// --------------------------------------------
	oMail := TMailManager():New()
	oMail:SetUseTLS(lTLS)
	oMail:SetUseSSL(lUsaSSL)
	oMail:Init('', cSMTPServer , cSMTPUser, cSMTPPass, 0, nSMTPPort)
	oMail:SetSMTPTimeOut(nTimeout)
	nErro := oMail:SMTPConnect()

	// --------------------------------------------
	// AUTENTICANDO O USUARIO NO SERVIDOR DE E-MAIL
	// --------------------------------------------
	If lRelAuth
		nErro := oMail:SMTPAuth(cSMTPUser ,cSMTPPass)
		If nErro <> 0
			// Recupera erro ...
			cMailError := oMail:GetErrorString(nErro)
			DEFAULT cMailError := '***UNKNOW***'
			ConOut('Erro de Autenticacao ' + Str(nErro, 4) + ' (' + cMailError + ')')
			lRet := .F.
		Endif
	EndIf

	If nErro <> 0
		// Recupera erro
		cMailError := oMail:GetErrorString(nErro)
		DEFAULT cMailError := '***UNKNOW***'
		ConOut(cMailError)
		oMail:SMTPDisconnect()
		lRet := .F.
	Endif

	// --------------------------------------------
	// CRIANDO O OBJETO DA MENSAGEM DO E-MAIL
	// --------------------------------------------
	oMessage := TMailMessage():New()
	oMessage:Clear()
	oMessage:cFrom		:= cFrom
	oMessage:cTo		:= AllTrim(cDest)
	oMessage:cCC		:= AllTrim(cCCo)
	oMessage:cBCC		:= AllTrim(cCCo)
	oMessage:cSubject	:= AllTrim(cSubject)
	oMessage:cBody		:= cBody
	oMessage:MsgBodyType( "text/html" )

	nErro := oMessage:Send( oMail )

	If nErro <> 0
		lRet := .F.
		cErro := oMail:GetErrorString(nErro)
		ConOut('Erro de Envio SMTP ' + Str(nErro,4) + ' (' + cErro + ')')
	Endif

	oMail:SMTPDisconnect()

Else
	lRet := .F.
	ConOut('Erro de envio! Destinatario nao informado!')

EndIf

Return(lRet)


/*/
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNXCodBar   �Autor  �V. Raspa                                  ���
�����������������������������������������������������������������������������Ĵ��
���DESCRICAO �MONTA CODIGO DE BARRAS PARA GRAVACAO DE UM NOVO PRODUTO         ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Function STNXCodBar()
Local cRet     := ''
Local cSequenc := ''
Local aArea    := GetArea()
Local aAreaSB1 := SB1->(GetArea())


cSequenc := GetMV('ES_NXTBAR') //--OBTEM O PROXIMO SEQUENCIAL
cRet     := '7899864' + cSequenc + '0' 
cRet     := cRet + EANDigito(cRet)

//--ATUALIZA SEQUENCIAL UTILIZADO
STNXPutMV(Space(FWGETTAMFILIAL), 'ES_NXTBAR', Soma1(cSequenc))

//--TRATAMENTO PARA PREVINIR A UTILIZACAO DO MESMO  
//--CODIGO DE BARRAS EM DOIS PRODUTOS DIFERENTES
SB1->(DbSetOrder(5)) //-- B1_FILIAL+B1_CODBAR
While SB1->(DbSeek(xFilial('SB1')+cRet))
	cSequenc := SuperGetMV('ES_NXTBAR',, '0001') //--OBTEM PROXIMO SEQUENCIAL
	cRet     := '7899864' + cSequenc + '0' 
	cRet     := cRet + EANDigito(cRet)

	//--ATUALIZA SEQUENCIAL UTILIZADO
	STNXPutMV(Space(FWGETTAMFILIAL), 'ES_NXTBAR', Soma1(cSequenc))
End


//--RESTAURA AMBIENTE
RestArea(aArea)
RestArea(aAreaSB1)

Return(cRet)



/*/
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNXPutMV    �Autor  �V. Raspa                                  ���
�����������������������������������������������������������������������������Ĵ��
���DESCRICAO �ATUALIZA CONTEUDO DOS PARAMETROS (SX6)                          ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
���������������������������������������������������������������������������������
/*/
Function STNXPutMV(cFil, cParam, cConteudo)
Local aAreaSX6 := SX6->(GetArea())

SX6->(DbSetOrder(1)) //--X6_FIL+X6_VAR
If SX6->(DbSeek(cFil+cParam))
	RecLock('SX6', .F.)
	SX6->X6_CONTEUD := cConteudo
	SX6->X6_CONTSPA := cConteudo
	SX6->X6_CONTENG := cConteudo
	SX6->(MsUnLock())

EndIf

RestArea(aAreaSX6)
Return






















