#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA140   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �ATUALIZACAO CADASTRAL                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA140()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZT')
oBrowse:SetDescription('Matriz de Desenvolvimento de Produtos - Atualiza��o Cadastral')
If RetCodUsr() <> '000000'
	oBrowse:SetFilterDefault("ZT_CODRESP == '" + RetCodUsr() + "'")
EndIf
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'			ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'			ACTION 'VIEWDEF.STNA140'	OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Atualizar'			ACTION 'VIEWDEF.STNA140'	OPERATION MODEL_OPERATION_UPDATE	ACCESS 0


Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStrCabec  := FWFormStruct(1, 'SZT', {|cCampo| .T.})
Local oStrItens  := FWFormStruct(1, 'SZU', {|cCampo| .T.})

Local oModel     := NIL
Local bCommit    := {|oModel| STNA140Commit(oModel)}

oModel:= MPFormModel():New('STNA140',, {|oModel| STNA140Pos(oModel)}, bCommit)

oModel:SetDescription('Matriz de Desenvolvimento de Produtos - Atualiza��o Cadastral')
oModel:SetVldActivate({|oModel| STNA140Pre(oModel)})

oModel:AddFields('mdlCabec', NIL, oStrCabec)
oModel:GetModel('mdlCabec'):SetDescription('Processo de Atualiza��o')
oModel:SetPrimaryKey({'ZT_FILIAL+ZT_PROCESS'})

oModel:AddGrid('mdlItens', 'mdlCabec', oStrItens) 
oModel:SetRelation('mdlItens', {{'ZU_FILIAL','xFilial("SZU")'}, {'ZU_PROCESS', 'ZT_PROCESS'}}, SZU->(IndexKey(1))) 

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel('STNA140')
Local oStrCabec  := FWFormStruct(2, 'SZT', {|cCampo| .T. })
Local oStrItens  := FWFormStruct(2, 'SZU', {|cCampo| .T. })


//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('viewCabec', oStrCabec, 'mdlCabec')
oView:EnableTitleView('viewCabec', 'Processo de Atualiza��o')

oView:AddGrid('viewItens', oStrItens, 'mdlItens')
oView:EnableTitleView('viewItens', 'Dados para Atualiza��o')

oView:CreateHorizontalBox('hdrCabec'	, 30)
oView:CreateHorizontalBox('grdItens'	, 70)

oView:SetOwnerView('viewCabec'	, 'hdrCabec')
oView:SetOwnerView('viewItens'	, 'grdItens')


Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA140Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA140Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA140Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA140Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA140Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA140Commit(oModel)
Local lRet        := .T.
Local nCountA     := 0
Local cCodPro     := ''
Local cCampo      := ''
Local cConteudo   := ''
Local aDadosSB1   := {}
Local aDadosSB5   := {}
Local aDadosSBZ   := {}
Local cAliasQry   := ''
Local cQuery      := ''
Local aAreaSX3    := SX3->(GetArea())
Default oModel    := FWModelActive()

// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	// -------------------------------------------
	// ATUALIZA OS DADOS NOS RESPECTIVOS CADASTROS
	// -------------------------------------------
	SZP->(DbSetOrder(1)) //--ZP_FILIAL+ZP_IDPROC+ZP_ITEM                                                                                                                                     
	SZP->(DbSeek(xFilial('SZP')+SZT->ZT_IDPROC+SZT->ZT_ITEM))
	cCodPro := PadR(SZP->ZP_CODPRO, Len(SB1->B1_COD))

	aDadosSB1 := { {'B1_COD', cCodPro, NIL} }
	aDadosSB5 := { {'B5_COD', cCodPro, NIL} }
	aDadosSBZ := { {'BZ_COD', cCodPro, NIL} }

	//-- VERIFICA SE LIBERA O CADASTRO DO PRODUTO
	SZQ->(DbSetOrder(1)) //--ZQ_FILIAL+ZQ_CODIGO
	SZQ->(DbSeek(xFilial('SZQ')+SZT->ZT_CODAREA))
	If SZQ->ZQ_LIBCAD == 'S'
		aAdd(aDadosSB1, {'B1_MSBLQL', '2', NIL})
	EndIf

	For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
		oModel:GetModel('mdlItens'):GoLine(nCountA)
		
		cCampo := oModel:GetModel('mdlItens'):GetValue('ZU_CAMPO')
		SX3->(DbSetOrder(2))
		SX3->(DbSeek(cCampo))

		If SX3->X3_TIPO == 'C'
			cConteudo := PadR(oModel:GetModel('mdlItens'):GetValue('ZU_CONTEUD'), SX3->X3_TAMANHO)
		
		ElseIf SX3->X3_TIPO == 'M'
			cConteudo := oModel:GetModel('mdlItens'):GetValue('ZU_CONTEUD')
			
		ElseIf SX3->X3_TIPO == 'N'
			cConteudo := Val(oModel:GetModel('mdlItens'):GetValue('ZU_CONTEUD'))
		
		ElseIf SX3->X3_TIPO == 'D'
			cConteudo := CtoD(oModel:GetModel('mdlItens'):GetValue('ZU_CONTEUD'))
		
		EndIf

		//-- CADASTRO DE PRODUTO:
		If Left(cCampo, 2) == 'B1'
			aAdd(aDadosSB1, {cCampo, cConteudo, NIL})
		ElseIf Left(cCampo, 2) == 'B5'
			aAdd(aDadosSB5, {cCampo, cConteudo, NIL})		
		ElseIf Left(cCampo, 2) == 'BZ'
			aAdd(aDadosSBZ, {cCampo, cConteudo, NIL})
		EndIf		
	Next nCountA

	Begin Transaction
		//-- ATUALIZA CADASTRO DE PRODUTO
		If Len(aDadosSB1) > 1
			lMSErroAuto := .F.
			MsgRun('Atualizando Cadastro Produto...', 'Aguarde...',;
					{|| MSExecAuto({|x, y, z| MATA010(x, y)}, aDadosSB1, 4)} )

			If lMsErroAuto
				lRet := .F.
				Help(,, 'STNA140',, 'Ocorreu um erro ao atualizar o cadastro do produto!', 1, 0)
				MostraErro()
				DisarmTransaction()
			Else
				//-- ATUALIZA PRODUTOS "FILHO"
				cAliasQry := GetNextAlias()

				cQuery := "SELECT SB1.B1_COD, SB1.R_E_C_N_O_ RECSB1 "
				cQuery += "  FROM " + RetSQLTab('SB1')
				cQuery += " WHERE SB1.B1_FILIAL = '" + xFilial('SB1') + "' "
				cQuery += "   AND SB1.B1_COD LIKE '" + AllTrim(cCodPro) + "%' "
				cQuery += "   AND SB1.D_E_L_E_T_ = ' ' " 
				dbUseArea(.T., "TOPCONN", TcGenQry(,,cQuery), cAliasQry, .T., .T.)
				
				While !(cAliasQry)->(Eof())
					SB1->(DbGoto((cAliasQry)->RECSB1))
					RecLock('SB1', .F.)
					For nCountA := 1 To Len(aDadosSB1)
						If AllTrim(aDadosSB1[nCountA, 1]) <> 'B1_COD'
							SB1->(FieldPut(FieldPos(aDadosSB1[nCountA, 1]), aDadosSB1[nCountA, 2]))
						EndIf
					Next nCountA
					SB1->(MsUnLock())
					(cAliasQry)->(DbSkip())					
				End
				(cAliasQry)->(DbCloseArea())

			EndIf
		EndIf

		//-- ATUALIZA CADASTRO DE COMPLEM. PRODUTO
		If lRet .And. Len(aDadosSB5) > 1
			lMSErroAuto := .F.
			MsgRun('Atualizando Complemento Produto...', 'Aguarde...',;
					{|| MSExecAuto({|x, y, z| MATA180(x, y)}, aDadosSB5, 4)} )

			If lMsErroAuto
				lRet := .F.
				Help(,, 'STNA140',, 'Ocorreu um erro ao atualizar o cadastro do produto!', 1, 0)
				MostraErro()
				DisarmTransaction()
			EndIf
		EndIf

		//-- ATUALIZA CADASTRO DE INDICADOR DE PRODUTO
		If lRet .And. Len(aDadosSBZ) > 1
			lMSErroAuto := .F.
			MsgRun('Atualizando Indicador Produto...', 'Aguarde...',;
					{|| MSExecAuto({|x, y, z| MATA018(x, y)}, aDadosSBZ, 4)} )

			If lMsErroAuto
				lRet := .F.
				Help(,, 'STNA140',, 'Ocorreu um erro ao atualizar o cadastro do produto!', 1, 0)
				MostraErro()
				DisarmTransaction()
			EndIf
		EndIf
	
		//-- ATUALIZA OS DADOS DO FORMULARIO
		If lRet 
			oModel:GetModel('mdlCabec'):SetValue('ZT_STATUS', '1')
			lRet := FwFormCommit(oModel)
			If !lRet
				DisarmTransaction()
			EndIf
		EndIf
	End Transaction
	
EndIf


RestArea(aAreaSX3)
Return(lRet)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA140ConPd �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �CONSULTA PADRAO PARA TRATAR O CONTEUDO DOS CAMPOS           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA140ConPd()
Local oModel    := FWModelActive()
Local cCampo    := ''
Local lRet      := .T.
Local cRetSXB   := ''
Local nRet      := 0
Local cAliasQry := ''
Local aCabec    := {'C�DIGO', 'DESCRI��O'}
Local aItens    := {}
Local aCombo    := {}
Local nCountA   := 0
Local aArea     := GetArea()
Local aAreaSX3  := SX3->(GetArea())
Local aAreaSXB  := SXB->(GetArea())

If IsInCallStack('STNA140')
	cCampo := oModel:GetModel('mdlItens'):GetValue('ZU_CAMPO')

ElseIf IsInCallStack('STNA180')
	cCampo := oModel:GetModel('mdlCampos'):GetValue('Z2_CAMPO')

EndIf

SX3->(DbSetOrder(2))
SX3->(DbSeek(cCampo))
If !Empty(SX3->X3_F3)
	SXB->(DbSetOrder(1))
	If SXB->(DbSeek(SX3->X3_F3))
		If SXB->XB_COLUNA == 'DB'
			SXB->(DbSeek(SX3->X3_F3 + '5'))
			cRetSXB := SXB->XB_CONTEM
		EndIf   

		VAR_IXB := CriaVar(SX3->X3_CAMPO)
		
		lRet := ConPad1(,,,SX3->X3_F3, 'VAR_IXB',, .F.)
		If lRet		
			VAR_IXB := &(cRetSXB)
		Else
			VAR_IXB := CriaVar('ZU_CONTEUD')
		EndIf

	Else		
		cAliasQry := GetNextAlias()
		BeginSql Alias cAliasQry
			SELECT SX5.X5_CHAVE CODIGO, SX5.X5_DESCRI DESCR
			  FROM %Table:SX5% SX5
			 WHERE SX5.X5_FILIAL = %xFilial:SX5%
			   AND SX5.X5_TABELA = %Exp:AllTrim(SX3->X3_F3)%
			   AND SX5.%NotDel% 
		EndSql

		// ------------------------------------
		// MONTA VETOR COM OS DADOS P/ CONSULTA
		// ------------------------------------
		While !(cAliasQry)->(Eof())
			AAdd(aItens, {(cAliasQry)->CODIGO, (cAliasQry)->DESCR})
			(cAliasQry)->(DbSkip())
		End
		(cAliasQry)->(DbCloseArea())

		nRet := TmsF3Array(aCabec, aItens, 'Consulta Padr�o', .T.,, aCabec)
		If nRet > 0
			VAR_IXB := aItens[nRet, 1]
		Else
			lRet := .F.
		EndIf

	EndIf

Else
	If !Empty(SX3->X3_CBOX)
		aCombo := StrTokArr(AllTrim(SX3->X3_CBOX), ';')
		For nCountA := 1 To Len(aCombo)
			aAdd(aItens, {SubStr(aCombo[nCountA], 1, At('=', aCombo[nCountA]) - 1), SubStr(aCombo[nCountA], At('=', aCombo[nCountA]) + 1)})
		Next nCountA

		nRet := TmsF3Array(aCabec, aItens, 'Consulta Padr�o', .T.,, aCabec)
		If nRet > 0
			VAR_IXB := aItens[nRet, 1]
		Else
			lRet := .F.
		EndIf

	Else
		If IsInCallStack('STNA140')
			VAR_IXB := CriaVar('ZU_CONTEUD')
		ElseIf IsInCallStack('STNA180')
			VAR_IXB := CriaVar('Z2_CONTEUD')
		EndIf
		lRet    := .F.
		Help(,, 'STNA140ConPd',, 'N�o h� consulta de conte�do associada � este campo', 1, 0)
	EndIf
EndIf


RestArea(aArea)
RestArea(aAreaSX3)

Return(lRet)