#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TBICONN.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI021   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �INTEGRACOES RAKUTEN - CADASTRO DE PRODUTOS "FILHOS"         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI021(cProdDe, cProdAte)
Local aRestr     := {}
Local aCores     := {}
Local cDbCorp    := GetMV('ES_DBCORP')
Local nHandle    := 0
Local cQuery     := ''
Local cAliasQry  := ''
Local nCountA    := 0

Default cProdDe     := Repl(' ', Len(SB1->B1_COD))
Default cProdAte    := Repl('Z', Len(SB1->B1_COD))


//-- OBTEM OS ITENS COM RESTRICAO AO CANAL E-COMM
If IsBlind()
	aRestr := STNI020Restr(cProdDe, cProdAte)

Else
	MsgRun('Obtendo itens com restri��o ao canal de e-comm...', 'Aguarde...',;
			{|| aRestr := STNI020Restr(cProdDe, cProdAte)})

EndIf


//-- OBTEM OS PRODUTOS A SEREM ENVIADOS
cQuery := "SELECT SZP.*, SB1.* "
cQuery += "  FROM " + RetSQLTab('SZP')
cQuery += "  JOIN " + RetSQLTab('SB1')
cQuery += "    ON SB1.B1_FILIAL = '" + xFilial('SB1') + "' "
cQuery += "   AND SB1.B1_COD = SZP.ZP_CODPRO "
cQuery += "   AND SB1.D_E_L_E_T_ = ' ' "
cQuery += " WHERE SZP.ZP_FILIAL = '" + xFilial('SZP') + "' "
cQuery += "   AND SZP.ZP_CODPRO BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND SZP.D_E_L_E_T_ = ' ' " 
cQuery := ChangeQuery(cQuery)

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)


While !(cAliasQry)->(Eof())
	If aScan(aRestr, (cAliasQry)->ZP_CODPRO) == 0

		// ----------------------------------------------------
		// OBTEM OS PRODUTOS "FILHOS" A PARTIR DO PRODUTO "PAI" 	
		// ----------------------------------------------------
		aCores := {	(cAliasQry)->ZP_COR1,;
					(cAliasQry)->ZP_COR2,;
					(cAliasQry)->ZP_COR3,;
					(cAliasQry)->ZP_COR4,;
					(cAliasQry)->ZP_COR5,;
					(cAliasQry)->ZP_COR6,;
					(cAliasQry)->ZP_COR7,;
					(cAliasQry)->ZP_COR8,;
					(cAliasQry)->ZP_COR9,;
					(cAliasQry)->ZP_CORA }			

		// -----------------------------------------
		// TRATA GRAVACAO/ATUALIZACAO DO PRODUTO
		// -----------------------------------------
		For nCountA := 1 To Len(aCores)
			If !Empty(aCores[nCountA])
	
				If STNI020Seek('K_R_RK_ENVIO_PRODUTO_FILHO', (cAliasQry)->ZP_CODPRO + '-' + aCores[nCountA])
		
				Else
					cQuery := "INSERT "
					cQuery += "  INTO " + cDbCorp + ".dbo.K_R_RK_ENVIO_PRODUTO_FILHO "
					cQuery += "       (HANDLE, "
					cQuery += "       Z_GRUPO, "
					cQuery += "       ID, "
					cQuery += "       LOJACODIGO, "
					cQuery += "       CODIGOINTERNOPRODUTO, "
					cQuery += "       PARTNUMBER, "
					cQuery += "       PRECOPOR, "
					cQuery += "       SK1POSICAO1, "
					cQuery += "       SK1POSICAO2, "
					cQuery += "       SK2POSICAO1, "
					cQuery += "       SK2POSICAO2, "
					cQuery += "       SK3POSICAO1, "
					cQuery += "       SK3POSICAO2, "
					cQuery += "       SK4POSICAO1, "
					cQuery += "       SK4POSICAO2, "
					cQuery += "       SK5POSICAO1, "
					cQuery += "       SK5POSICAO2, "
					cQuery += "       STATUSSKU, "
					cQuery += "       ACAOID, "
					cQuery += "       DATACRIACAO, "
					cQuery += "       DATAATUALIZACAO, "
					cQuery += "       STATUSPROCESSAMENTOID, "
					cQuery += "       MENSAGEMPROCESSAMENTO, "
					cQuery += "       PROCESSAR, "
					cQuery += "       USUARIO, "
					cQuery += "       ULTIMOPRECOENVIADO, "
					cQuery += "       ALTERARDATASUL) "

					cQuery += "VALUES ("

					cQuery += AllTrim(Str(STNI020Prox('K_R_RK_ENVIO_PRODUTO_FILHO', 'HANDLE'))) + ", "							//-- <HANDLE, int>
					cQuery += "'', "																							//-- <Z_GRUPO, int>
					cQuery += AllTrim(Str(STNI020Prox('K_R_RK_ENVIO_PRODUTO_FILHO', 'ID'))) + ", "								//-- <ID, int>
					cQuery += "0, "																								//-- <LOJACODIGO, int>
					cQuery += "'" + AllTrim((cAliasQry)->ZP_CODPRO) + "', "														//-- <CODIGOINTERNOPRODUTO, varchar(24)>
					cQuery += "'" + AllTrim((cAliasQry)->ZP_CODPRO) + '-' + aCores[nCountA] + "', "								//-- <PARTNUMBER, varchar(24)>
					cQuery += "0, "																								//-- <PRECOPOR, decimal(15,2)>
					cQuery += "'COR', "																							//-- <SK1POSICAO1, varchar(50)>
					cQuery += "'" + Capital(AllTrim(Posicione('SZE', 1, xFilial('SZE')+aCores[nCountA], 'ZE_DESCR'))) + "', "	//-- <SK1POSICAO2, varchar(50)>
					cQuery += "'', "																							//-- <SK2POSICAO1, int>
					cQuery += "'', "																							//-- <SK2POSICAO2, varchar(50)>
					cQuery += "'', "																							//-- <SK3POSICAO1, varchar(50)>
					cQuery += "'', "																							//-- <SK3POSICAO2, varchar(50)>
					cQuery += "'', "																							//-- <SK4POSICAO1, varchar(50)>
					cQuery += "'', "																							//-- <SK4POSICAO2, varchar(50)>
					cQuery += "'', "																							//-- <SK5POSICAO1, varchar(50)>
					cQuery += "'', "																							//-- <SK5POSICAO2, varchar(50)>
					cQuery += "2, "																								//-- <STATUSSKU, int>
					cQuery += "1, "																								//-- <ACAOID, int>
					cQuery += "GETDATE(), "																						//-- <DATACRIACAO, datetime>
					cQuery += "NULL, "																							//-- <DATAATUALIZACAO, datetime>
					cQuery += "NULL, "																							//-- <STATUSPROCESSAMENTOID, int>
					cQuery += "'', "																							//-- <MENSAGEMPROCESSAMENTO, varchar(1000)>
					cQuery += "2, "																								//-- <PROCESSAR, int>
					cQuery += "'CadProd', "																						//-- <USUARIO, varchar(15)>
					cQuery += "0, "																								//-- <ULTIMOPRECOENVIADO, decimal(15,2)>
					cQuery += "NULL) "																							//-- <ALTERARDATASUL, int>)

					If STNI010Conn(.F., cDbCorp, @nHandle)
						If TCSQLExec(cQuery) < 0
							ConOut("[RAKUTEN][INTEGRACOES] - ERRO AO ATUALIZAR CADASTRO PRODUTO FILHO - " + TCSQLError())
						EndIf
						TCUnlink()
					EndIf

				EndIf
			EndIf
		Next nCountA
	EndIf
	(cAliasQry)->(DbSkip())
End

(cAliasQry)->(DbCloseArea())

Return	
	
	
	
	