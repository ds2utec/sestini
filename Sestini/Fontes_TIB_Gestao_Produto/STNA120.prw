#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA120   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �MANUTENCAO MATRIZ DE DESENVOLVIMENTO DE PRODUTOS            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA120()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZO')
oBrowse:SetDescription('Matriz de Desenvolvimento de Produtos')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'					ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'					ACTION 'VIEWDEF.STNA120'	OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'						ACTION 'VIEWDEF.STNA120'	OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'						ACTION 'VIEWDEF.STNA120'	OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'						ACTION 'VIEWDEF.STNA120'	OPERATION MODEL_OPERATION_DELETE	ACCESS 0
ADD OPTION aRotina TITLE 'Atualiz. Cadastral'			ACTION 'STNA121'			OPERATION MODEL_OPERATION_UPDATE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStrCabec  := FWFormStruct(1, 'SZO', {|cCampo| .T.})
Local oStrItens  := FWFormStruct(1, 'SZP', {|cCampo| .T.})
Local oModel     := NIL
Local bCommit    := {|oModel| STNA120Commit(oModel)}

oModel:= MPFormModel():New('STNA120',, {|oModel| STNA120Pos(oModel)}, bCommit)

oModel:SetDescription('Matriz de Desenvolvimento de Produtos')
oModel:SetVldActivate({|oModel| STNA120Pre(oModel)})

oModel:AddFields('mdlCabec', NIL, oStrCabec)
oModel:GetModel('mdlCabec'):SetDescription('Matriz de Desenvolvimento de Produtos')
oModel:SetPrimaryKey({'ZO_FILIAL+ZO_IDPROC'})

oModel:AddGrid('mdlItens', 'mdlCabec', oStrItens, {|oModelGrid, nLine, cAction, cField| STNA120GrdPre(oModelGrid, nLine, cAction, cField)})
oModel:SetRelation('mdlItens', {{'ZP_FILIAL','xFilial("SZP")'}, {'ZP_IDPROC', 'ZO_IDPROC'}}, SZP->(IndexKey(1))) 
oModel:GetModel('mdlItens'):SetUniqueLine({'ZP_TIPO', 'ZP_GRUPO', 'ZP_CATEG', 'ZP_ATRIB', 'ZP_COLEC'})

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel('STNA120')
Local oStrCabec  := FWFormStruct(2, 'SZO', {|cCampo| .T. })
Local oStrItens  := FWFormStruct(2, 'SZP', {|cCampo| .T. })


//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('viewCabec', oStrCabec, 'mdlCabec')
oView:EnableTitleView('viewCabec', 'Matriz Desenvolvimento de Produtos')

oView:AddGrid('viewItens', oStrItens, 'mdlItens')
oView:EnableTitleView('viewItens', 'Itens Matriz Desenvolvimento de Produtos')

oView:CreateHorizontalBox('hdrCabec'	, 20)
oView:CreateHorizontalBox('grdItens'	, 80)

oView:SetOwnerView('viewCabec'	, 'hdrCabec')
oView:SetOwnerView('viewItens'	, 'grdItens')

oView:AddIncrementField('viewItens', 'ZP_ITEM')
oView:AddUserButton('Status Atualiz. Cadastral', '', {|oView| STNA120Stat()})


Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA120Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA120Pos(oModel)
Local lRet := .T.

Return(lRet)



 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120GrdPre�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �VALIDA A EDICAO DO GRID                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA120GrdPre(oModelGrid, nLine, cAction, cField)
Local lRet   := .T.

If cAction == 'DELETE' .Or. cAction == 'UNDELETE'
	lRet := .T.
Else
	If oModelGrid:GetValue('ZP_STATUS') <> '0' 
		lRet := .F.
	EndIf
EndIf

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA120Commit(oModel)
Local lRet        := .T.
Local nCountA     := 0
Local nCountB     := 0
Local nCountC     := 0
Local aDados      := {}
Local aMsExecAuto := {}
Local cAliasQry   := ''
Local nPosCodPrd  := 0
Local aFiliais    := FWAllFilial(FWCodEmp(), FWUnitBusiness())
Local cFilBkp     := FWCodEmp() + FWUnitBusiness() + FWFilial()
Default oModel    := FWModelActive()

If oModel:GetOperation() == MODEL_OPERATION_DELETE
	// ------------------------------------------------
	// TRATA EXCLUSAO DA MATRIZ (EXCLUSAO DOS PRODUTOS)
	// ------------------------------------------------
	If MsgYesNo('Confirma exclus�o de todos os itens da Matriz? (Essa a��o n�o poder� ser desfeita!)')
		For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
			oModel:GetModel('mdlItens'):GoLine(nCountA)
			If oModel:GetModel('mdlItens'):GetValue('ZP_STATUS') <> '0' //-- 0=Em Desenvolvimento;1=Cadastro em Atualizacao;2=Item Efetivado
				//-- TRATA EXCLUSAO DOS ITENS "FILHOS"			
				For nCountB := 1 To 10
					If !Empty(oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')))
						For nCountC := 1 To Len(aFiliais)
							aDados := {	{'BZ_FILIAL', FWCodEmp() + FWUnitBusiness() + aFiliais[nCountC], NIL},;
										{'BZ_COD'	, AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')) + '-' + oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')), NIL}}
			
							aAdd(aMsExecAuto, aDados)
						Next nCountC
						
						aDados := {{'B1_COD', AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')) + '-' + oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')), NIL}}
						aAdd(aMsExecAuto, aDados)
					EndIf
				Next nCountB
			
				//-- TRATA EXCLUSAO DO PRODUTO "PAI"
				For nCountB := 1 To Len(aFiliais)
					aDados := {	{'BZ_FILIAL'	, FWCodEmp() + FWUnitBusiness() + aFiliais[nCountB]			, NIL},; 
								{'BZ_COD'		, Alltrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')), NIL} }
					aAdd(aMsExecAuto, aDados)
				Next nCountB
				
				aDados := {{'B1_COD', AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')), NIL}}
				aAdd(aMsExecAuto, aDados)
				
			EndIf
		Next nCountA

		If Len(aMsExecAuto) > 0
			Begin Transaction
				For nCountA := 1 To Len(aMsExecAuto)
					nPosCodPrd := aScan(aMsExecAuto[nCountA], {|x| AllTrim(x[1]) == 'B1_COD' .Or. AllTrim(x[1]) == 'BZ_COD'})

					// -----------------------------
					// TRATA EXCLUSAO PRODUTO MASTER
					// -----------------------------
					If Left(aMsExecAuto[nCountA, nPosCodPrd, 1], 2) == 'B1'
						AH1->(DbSetOrder(1)) //--AH1_FILIAL+AH1_PRODUT+AH1_FORNEC+AH1_LOJAFO
						If AH1->(DbSeek(xFilial('AH1')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
							lRet := .F.
							DisarmTransaction()
							Help(,, 'STNA121Env',, 'N�o ser� poss�vel realizar a exclus�o do produto, pois o mesmo est� amarrado � um processo no m�dulo de Controle de Direitos Autorais', 1, 0)
							Exit
						Else
							AH8->(DbSetOrder(1)) //--AH8_FILIAL+AH8_PRODUT
							If AH8->(DbSeek(xFilial('AH8')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
								RecLock('AH8', .F.)
								AH8->(DbDelete())
								AH8->(MsUnLock())
							EndIf
						EndIf
					EndIf

					// -----------------------------------
					// TRATA EXCLUSAO DO PRODUTO/INDICADOR
					// -----------------------------------
					If lRet
		 				lMSErroAuto := .F.
						If Left(aMsExecAuto[nCountA, nPosCodPrd, 1], 2) == 'BZ'

							cFilAnt := aMsExecAuto[nCountA, 1, 2]

							SBZ->(DbSetOrder(1))
							If SBZ->(DbSeek(xFilial('SBZ')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
								MsgRun('Realizando Exclusao dos indicadores... Produto: ' + AllTrim(aMsExecAuto[nCountA, nPosCodPrd, 2]), 'Aguarde...',;
									{|| MSExecAuto({|x, y, z| MATA018(x, y)}, aMsExecAuto[nCountA], 5)} )
							EndIf

							cFilAnt := cFilBkp

						Else
							MsgRun('Realizando Exclusao dos itens... Produto: ' + AllTrim(aMsExecAuto[nCountA, nPosCodPrd, 2]), 'Aguarde...',;
									{|| MSExecAuto({|x, y, z| MATA010(x, y)}, aMsExecAuto[nCountA], 5)} )
 						EndIf
 			
						If lMsErroAuto
							lRet := .F.
							DisarmTransaction()
							Help(,, 'STNA121Env',, 'Ocorreu um erro ao efetivar o cadastro do produto!', 1, 0)
							MostraErro()
							Exit
						Else
							//--EXCLUI RESTRICOES x PRODUTOS x CANAIS
							If Left(aMsExecAuto[nCountA, 1, 1], 2) == 'B1'
								SZ8->(DbSetOrder(1)) //--Z8_FILIAL+Z8_PRODUTO+Z8_CANAL
								If SZ8->(DbSeek(xFilial('SZ8')+aMsExecAuto[nCountA, nPosCodPrd, 2]))
									While !SZ8->(Eof()) .And. SZ8->(Z8_FILIAL+Z8_PRODUTO) == xFilial('SZ8')+PadR(aMsExecAuto[nCountA, nPosCodPrd, 2], Len(SB1->B1_COD), ' ')
										RecLock('SZ8', .F.)
										SZ8->(DbDelete())
										SZ8->(MsUnLock())
									
										SZ8->(DbSkip())
									End
								EndIf
							EndIf
						EndIf
					EndIf
				Next nCountA

				//-- TRATA EXCLUSAO DOS PROCESSOS DE ATUALIZACAO CADASTRAL
				If lRet
					For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
						oModel:GetModel('mdlItens'):GoLine(nCountA)
						If oModel:GetModel('mdlItens'):GetValue('ZP_STATUS') <> '0' //-- 0=Em Desenvolvimento;1=Cadastro em Atualizacao;2=Item Efetivado
							MsgRun('Excluindo dados Atualiza��o Cadastral... ', 'Aguarde...',;
									{|| STNA120ExcPrc(oModel:GetModel('mdlCabec'):GetValue('ZO_IDPROC'), oModel:GetModel('mdlItens'):GetValue('ZP_ITEM'))})
						EndIf
					Next nCountA
				EndIf
			End Transaction
		EndIf
	Else
		lRet := .F.	
	EndIf

ElseIf oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE 
	// ----------------------------------
	// TRATA INCLUSAO/ALTERACAO DA MATRIZ
	// ----------------------------------
	For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
		oModel:GetModel('mdlItens'):GoLine(nCountA)
		If Empty(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO'))
			cGrupo := oModel:GetModel('mdlItens'):GetValue('ZP_GRUPO')
			oModel:GetModel('mdlItens'):SetValue('ZP_CODPRO', cGrupo + STNA020Seq(cGrupo))
		EndIf 		
	Next nCountA

	If oModel:GetOperation() == MODEL_OPERATION_UPDATE 
		For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
			oModel:GetModel('mdlItens'):GoLine(nCountA)
			If oModel:GetModel('mdlItens'):GetValue('ZP_STATUS') <> '0' //-- 0=Em Desenvolvimento;1=Cadastro em Atualizacao;2=Item Efetivado
				If oModel:GetModel('mdlItens'):IsDeleted()
					//-- TRATA EXCLUSAO DOS ITENS "FILHOS"			
					For nCountB := 1 To 10
						If !Empty(oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')))
							For nCountC := 1 To Len(aFiliais)
								aDados := {	{'BZ_FILIAL'	, FWCodEmp() + FWUnitBusiness() + aFiliais[nCountC], NIL},;
											{'BZ_COD'		, AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')) + '-' + oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')), NIL} }
								aAdd(aMsExecAuto, aDados)
							Next nCountC
						
							aDados := {{'B1_COD', AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')) + '-' + oModel:GetModel('mdlItens'):GetValue('ZP_COR' + If(nCountB < 10, AllTrim(Str(nCountB)), 'A')), NIL}}
							aAdd(aMsExecAuto, aDados)
						EndIf
					Next nCountB
			
					//-- TRATA EXCLUSAO DO PRODUTO "PAI"
					For nCountB := 1 To Len(aFiliais)
						aDados := {	{'BZ_FILIAL'	, FWCodEmp() + FWUnitBusiness() + aFiliais[nCountB]			, NIL},;
									{'BZ_COD'		, AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')), NIL} }
						aAdd(aMsExecAuto, aDados)
					Next nCountB
				
					aDados := {{'B1_COD', AllTrim(oModel:GetModel('mdlItens'):GetValue('ZP_CODPRO')), NIL}}
					aAdd(aMsExecAuto, aDados)
				EndIf
			EndIf
		Next nCountA

		If Len(aMsExecAuto) > 0
			Begin Transaction
				For nCountA := 1 To Len(aMsExecAuto)
					lMSErroAuto := .F.

					If Left(aMsExecAuto[nCountA, 1, 1], 2) == 'BZ'

						cFilAnt := aMsExecAuto[nCountA, 1, 2]

						SBZ->(DbSetOrder(1))
						If SBZ->(DbSeek(xFilial('SBZ')+aMsExecAuto[nCountA, 2, 2]))
							MsgRun('Realizando Exclusao dos indicadores... Produto: ' + AllTrim(aMsExecAuto[nCountA, 2, 2]), 'Aguarde...',;
									{|| MSExecAuto({|x, y, z| MATA018(x, y)}, aMsExecAuto[nCountA], 5)} )
						EndIf				

						cFilAnt := cFilBkp

					Else
						MsgRun('Realizando Exclusao dos itens... Produto: ' + AllTrim(aMsExecAuto[nCountA, 1, 2]), 'Aguarde...',;
								{|| MSExecAuto({|x, y, z| MATA010(x, y)}, aMsExecAuto[nCountA], 5)} )
 					EndIf
 			
					If lMsErroAuto
						lRet := .F.
						DisarmTransaction()
						Help(,, 'STNA121Env',, 'Ocorreu um erro ao efetivar a exclus�o do produto!', 1, 0)
						MostraErro()
						Exit
					Else
						//--EXCLUI RESTRICOES x PRODUTOS x CANAIS
						If Left(aMsExecAuto[nCountA, 1, 1], 2) == 'B1'
							SZ8->(DbSetOrder(1)) //--Z8_FILIAL+Z8_PRODUTO+Z8_CANAL
							If SZ8->(DbSeek(xFilial('SZ8')+aMsExecAuto[nCountA, 1, 2]))
								While !SZ8->(Eof()) .And. SZ8->(Z8_FILIAL+Z8_PRODUTO) == xFilial('SZ8')+aMsExecAuto[nCountA, 1, 2]
									RecLock('SZ8', .F.)
									SZ8->(DbDelete())
									SZ8->(MsUnLock())
									
									SZ8->(DbSkip())
								End
							EndIf
						EndIf
					EndIf
				Next nCountA

				//-- TRATA EXCLUSAO DOS PROCESSOS DE ATUALIZACAO CADASTRAL
				If lRet
					For nCountA := 1 To oModel:GetModel('mdlItens'):Length()
						oModel:GetModel('mdlItens'):GoLine(nCountA)
						If oModel:GetModel('mdlItens'):GetValue('ZP_STATUS') <> '0' //-- 0=Em Desenvolvimento;1=Cadastro em Atualizacao;2=Item Efetivado
							If oModel:GetModel('mdlItens'):IsDeleted()
								MsgRun('Excluindo dados Atualiza��o Cadastral... ', 'Aguarde...',;
										{|| STNA120ExcPrc(oModel:GetModel('mdlCabec'):GetValue('ZO_IDPROC'), oModel:GetModel('mdlItens'):GetValue('ZP_ITEM'))})
							EndIf
						EndIf
					Next nCountA
				EndIf
			End Transaction
		EndIf
	EndIf
EndIf

// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet
	lRet := FwFormCommit(oModel)

Else
	oModel:SetErrorMessage(,,,,, CHR(13)+CHR(10)+'Ocorreram problemas que impedem o processamento da solicita��o realizada ou a opera��o foi cancelada pelo usu�rio')

EndIf

Return(lRet)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120ConPd �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Consulta padrao especifica para os campos Grupo, Categoria, ���
���          �Atributo e Colecao (Consulta padrao                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA120ConPd(cCampo)
Local nCountA   := 0
Local cAliasQry := GetNextAlias()
Local oModel    := FWModelActive()
Local cTipo     := oModel:GetValue('mdlItens', 'ZP_TIPO')
Local cGrupo    := oModel:GetValue('mdlItens', 'ZP_GRUPO')
Local cCateg    := oModel:GetValue('mdlItens', 'ZP_CATEG')
Local aItens    := {}
Local lRet      := .T.
Local nRet      := 0
Local cTitulo   := ''
Default cCampo  := ReadVar()

If cCampo == 'M->ZP_GRUPO'
	If Empty(cTipo)
		lRet := .F.
		Help(,, 'STNA120',, 'Informe o Tipo do Produto para acionar a consulta', 1, 0)
	EndIf

ElseIf cCampo == 'M->ZP_CATEG' .Or. cCampo == 'M->ZP_COLEC'
	If Empty(cTipo) .Or. Empty(cGrupo)
		lRet := .F.
		Help(,, 'STNA120',, 'Informe o Tipo/Grupo do Produto para acionar a consulta', 1, 0)
	EndIf

ElseIf cCampo == 'M->ZP_ATRIB'
	If Empty(cTipo) .Or. Empty(cGrupo) .Or. Empty(cCateg)
		lRet := .F.
		Help(,, 'STNA120',, 'Informe o Tipo, Grupo e Categoria do Produto para acionar a consulta', 1, 0)
	EndIf
	
EndIf
	 
If lRet
	If cCampo == 'M->ZP_GRUPO'
		// --------------------------------
		// TRATA CONSULTA: GRUPO DE PRODUTO
		// --------------------------------
		cTitulo := 'Grupos de Produto'
		BeginSql Alias cAliasQry
			SELECT SZL.ZL_GRUPO CODIGO, SZB.ZB_DESCR DESCR
			  FROM %Table:SZL% SZL
			  JOIN %Table:SZB% SZB
			    ON SZB.ZB_FILIAL = %xFilial:SZB%
			   AND SZB.ZB_CODIGO = SZL.ZL_GRUPO
			   AND SZB.%NotDel%
			 WHERE SZL.ZL_FILIAL = %xFilial:SZL%
			   AND SZL.ZL_TIPO   = %Exp:cTipo%
			   AND SZL.%NotDel% 
		EndSql

	ElseIf cCampo == 'M->ZP_CATEG'
		// ------------------------------------
		// TRATA CONSULTA: CATEGORIA DE PRODUTO
		// ------------------------------------
		cTitulo := 'Categorias de Produto'
		BeginSql Alias cAliasQry
			SELECT SZM.ZM_CATEG CODIGO, SZC.ZC_DESCR DESCR
			  FROM %Table:SZM% SZM
			  JOIN %Table:SZC% SZC
			    ON SZC.ZC_FILIAL = %xFilial:SZC%
			   AND SZC.ZC_CODIGO = SZM.ZM_CATEG
			   AND SZC.%NotDel%
			 WHERE SZM.ZM_FILIAL = %xFilial:SZM%
			   AND SZM.ZM_TIPO   = %Exp:cTipo%
			   AND SZM.ZM_GRUPO  = %Exp:cGrupo%
			   AND SZM.%NotDel% 
		EndSql

	ElseIf cCampo == 'M->ZP_COLEC'
		// ----------------------------------
		// TRATA CONSULTA: COLECAO DE PRODUTO
		// ----------------------------------
		cTitulo := 'Cole��es de Produto'
		BeginSql Alias cAliasQry
			SELECT SZN.ZN_COLEC CODIGO, SZD.ZD_DESCR DESCR
			  FROM %Table:SZN% SZN
			  JOIN %Table:SZD% SZD
			    ON SZD.ZD_FILIAL = %xFilial:SZD%
			   AND SZD.ZD_CODIGO = SZN.ZN_COLEC
			   AND SZD.%NotDel%
			 WHERE SZN.ZN_FILIAL = %xFilial:SZN%
			   AND SZN.ZN_TIPO   = %Exp:cTipo%
			   AND SZN.ZN_GRUPO  = %Exp:cGrupo%
			   AND SZN.%NotDel% 
		EndSql

	ElseIf cCampo == 'M->ZP_ATRIB'
		// ----------------------------------
		// TRATA CONSULTA: ATRIBUTOS
		// ----------------------------------
		cTitulo := 'Atributos de Produto'
		BeginSql Alias cAliasQry
			SELECT SZW.ZW_ATRIB CODIGO, SZY.ZY_DESCR DESCR
			  FROM %Table:SZW% SZW
			  JOIN %Table:SZY% SZY
			    ON SZY.ZY_FILIAL = %xFilial:SZY%
			   AND SZY.ZY_CODIGO = SZW.ZW_ATRIB
			   AND SZY.%NotDel%
			 WHERE SZW.ZW_FILIAL = %xFilial:SZW%
			   AND SZW.ZW_TIPO   = %Exp:cTipo%
			   AND SZW.ZW_GRUPO  = %Exp:cGrupo%
			   AND SZW.ZW_CATEG  = %Exp:cCateg%
			   AND SZW.%NotDel% 
		EndSql
		
	EndIf

	// ------------------------------------
	// MONTA VETOR COM OS DADOS P/ CONSULTA
	// ------------------------------------
	While !(cAliasQry)->(Eof())
		AAdd(aItens, {(cAliasQry)->CODIGO, (cAliasQry)->DESCR})
		(cAliasQry)->(DbSkip())
	End

	(cAliasQry)->(DbCloseArea())

	// ----------------------------
	// ABRE INTERFACE PARA CONSULTA
	// ----------------------------
	If Len(aItens) > 0
		nRet := TmsF3Array({'C�DIGO', 'DESCRI��O'}, aItens, cTitulo, .T.)
		If nRet > 0
			VAR_IXB := aItens[nRet, 1]
		Else
			lRet := .F.
		EndIf
	Else
		lRet := .F.
		If cCampo == 'M->ZP_GRUPO'
			Help(,, 'STNA120',, 'N�o existem Grupos de Produto vinculados � este Tipo de Produto', 1, 0)

		ElseIf cCampo == 'M->ZP_CATEG' 
			Help(,, 'STNA120',, 'N�o existem Categorias vinculadas � este Tipo de Produto', 1, 0)

		ElseIf cCampo == 'M->ZP_COLEC'
			Help(,, 'STNA120',, 'N�o existem Cole��es vinculadas � este Tipo de Produto', 1, 0)

		ElseIf cCampo == 'M->ZP_ATRIB'
			Help(,, 'STNA120',, 'N�o existem Atributos vinculados � este Grupo/Categoria de Produto', 1, 0)

		EndIf

	EndIf
EndIf

Return(lRet)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120Stat  �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Exibe a situacao atual referente a atualizacao cadastral do ���
���          �item                                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA120Stat()
Local oModel    := FWModelActive()
Local cIdProc   := oModel:GetValue('mdlCabec', 'ZO_IDPROC')
Local cItem     := oModel:GetValue('mdlItens', 'ZP_ITEM')
Local cStatus   := oModel:GetValue('mdlItens', 'ZP_STATUS')
Local cSLA      := ''
Local oDlg      := NIL
Local oBrowse   := NIL
Local aHeader   := {}
Local aItens    := {}
Local cAliasQry := GetNextAlias()

If cStatus == '0' .Or. cStatus == '2' 
	Help(,, 'STNA120',, 'O item selecionado n�o est� em processo de atualiza��o cadastral', 1, 0)

ElseIf cStatus == '1'
	//-- CABECALHO
	aHeader := {'Status', 'Area', 'SLA Limite'}
	
	//-- ITENS
	BeginSQL Alias cAliasQry
		SELECT SZT.ZT_STATUS, SZQ.ZQ_DESCR, SZT.ZT_DATALIM 
		  FROM %Table:SZT% SZT
		  JOIN %Table:SZQ% SZQ
		    ON SZQ.ZQ_FILIAL = %xFilial:SZQ%
		   AND SZQ.ZQ_CODIGO = SZT.ZT_CODAREA
		   AND SZQ.%NotDel%
		 WHERE SZT.ZT_FILIAL = %xFilial:SZT%
		   AND SZT.ZT_IDPROC = %Exp:cIdProc%
		   AND SZT.ZT_ITEM   = %Exp:cItem%
		   AND SZT.%NotDel%
	EndSQL

	While !(cAliasQry)->(Eof())
		If (cAliasQry)->ZT_STATUS == '0' .And. StoD((cAliasQry)->ZT_DATALIM) > dDataBase
			cSLA := 'PENDENTE - NO PRAZO'
		ElseIf (cAliasQry)->ZT_STATUS == '0' .And. StoD((cAliasQry)->ZT_DATALIM) <= dDataBase
			cSLA := 'PENDETE - ATRASADO'
		ElseIf (cAliasQry)->ZT_STATUS == '1'
			cSLA := 'ATUALIZ. REALIZADA'
		EndIf

		aAdd(aItens, {	cSLA,; 
						(cAliasQry)->ZQ_DESCR,;
						(cAliasQry)->ZT_DATALIM} )

		(cAliasQry)->(DbSkip())
	End
	(cAliasQry)->(DbCloseArea())


	Define MSDialog oDlg From 000, 000 To 250, 600 Title 'Status: Atualiza��o Cadastral' Of oMainWnd Pixel
		oBrowse := TWBrowse():New(040, 005, 290, 080,, aHeader,, oDlg,,,,,,,,,,,,.T.,,.T.)	
		oBrowse:SetArray(aItens)
		oBrowse:bLine := {|| {	aItens[oBrowse:nAT, 1],;
								aItens[oBrowse:nAT, 2],;
								DtoC(StoD(aItens[oBrowse:nAT, 3])) }}
	Activate MSDialog oDlg On Init EnchoiceBar(oDlg, {|| oDlg:End()}, {|| oDlg:End()}) Centered

EndIf


Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120Vld   �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Validacao para o input dos campos Grupo, Categoria, Atributo���
���          �e Colecao                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNA120Vld(cCampo)
Local lRet      := .T.
Local cAliasQry := GetNextAlias()
Local oModel    := FWModelActive()
Local cTipo     := oModel:GetValue('mdlItens', 'ZP_TIPO')
Local cGrupo    := oModel:GetValue('mdlItens', 'ZP_GRUPO')
Local cCateg    := oModel:GetValue('mdlItens', 'ZP_CATEG')
Local cColec    := oModel:GetValue('mdlItens', 'ZP_COLEC')
Local cAtrib    := oModel:GetValue('mdlItens', 'ZP_ATRIB')
Default cCampo  := ReadVar()

If cCampo == 'M->ZP_GRUPO'
	// ------------------------------------
	// TRATA CAMPO: CATEGORIA DE PRODUTO
	// ------------------------------------
	BeginSql Alias cAliasQry
		SELECT SZL.ZL_GRUPO CODIGO, SZB.ZB_DESCR DESCR
		  FROM %Table:SZL% SZL
		  JOIN %Table:SZB% SZB
		    ON SZB.ZB_FILIAL = %xFilial:SZB%
		   AND SZB.ZB_CODIGO = SZL.ZL_GRUPO
		   AND SZB.%NotDel%
		 WHERE SZL.ZL_FILIAL = %xFilial:SZL%
		   AND SZL.ZL_TIPO   = %Exp:cTipo%
		   AND SZL.ZL_GRUPO  = %Exp:cGrupo%
		   AND SZL.%NotDel% 
	EndSql

	If (cAliasQry)->(Eof())
		lRet := .F.
		Help(,, 'STNA120',, 'O Grupo de Produto informado n�o existe ou n�o est� relacionado ao Tipo de Produto informado!', 1, 0)
	EndIf

ElseIf cCampo == 'M->ZP_CATEG'
	// ------------------------------------
	// TRATA CAMPO: CATEGORIA DE PRODUTO
	// ------------------------------------
	BeginSql Alias cAliasQry
		SELECT SZM.ZM_CATEG CODIGO, SZC.ZC_DESCR DESCR
		  FROM %Table:SZM% SZM
		  JOIN %Table:SZC% SZC
		    ON SZC.ZC_FILIAL = %xFilial:SZC%
		   AND SZC.ZC_CODIGO = SZM.ZM_CATEG
		   AND SZC.%NotDel%
		 WHERE SZM.ZM_FILIAL = %xFilial:SZM%
		   AND SZM.ZM_TIPO   = %Exp:cTipo%
		   AND SZM.ZM_GRUPO  = %Exp:cGrupo%
		   AND SZM.ZM_CATEG  = %Exp:cCateg%
		   AND SZM.%NotDel% 
	EndSql
	
	If (cAliasQry)->(Eof())
		lRet := .F.
		Help(,, 'STNA120',, 'A Categoria informada n�o existe ou n�o est� relacionada ao Grupo de Produto informado!', 1, 0)
	EndIf

ElseIf cCampo == 'M->ZP_COLEC'
	// ----------------------------------
	// TRATA CAMPO: COLECAO DE PRODUTO
	// ----------------------------------
	BeginSql Alias cAliasQry
		SELECT SZN.ZN_COLEC CODIGO, SZD.ZD_DESCR DESCR
		  FROM %Table:SZN% SZN
		  JOIN %Table:SZD% SZD
		    ON SZD.ZD_FILIAL = %xFilial:SZD%
		   AND SZD.ZD_CODIGO = SZN.ZN_COLEC
		   AND SZD.%NotDel%
		 WHERE SZN.ZN_FILIAL = %xFilial:SZN%
		   AND SZN.ZN_TIPO   = %Exp:cTipo%
		   AND SZN.ZN_GRUPO  = %Exp:cGrupo%
		   AND SZN.ZN_COLEC  = %Exp:cColec% 
		   AND SZN.%NotDel% 
	EndSql
		
	If (cAliasQry)->(Eof())
		lRet := .F.
		Help(,, 'STNA120',, 'A Cole��o informada n�o existe ou n�o est� relacionada ao Grupo de Produto informado!', 1, 0)
	EndIf

ElseIf cCampo == 'M->ZP_ATRIB'
	// ----------------------------------
	// TRATA CAMPO: ATRIBUTOS
	// ----------------------------------
	BeginSql Alias cAliasQry
		SELECT SZW.ZW_ATRIB CODIGO, SZY.ZY_DESCR DESCR
		  FROM %Table:SZW% SZW
		  JOIN %Table:SZY% SZY
		    ON SZY.ZY_FILIAL = %xFilial:SZY%
		   AND SZY.ZY_CODIGO = SZW.ZW_ATRIB
		   AND SZY.%NotDel%
		 WHERE SZW.ZW_FILIAL = %xFilial:SZW%
		   AND SZW.ZW_TIPO   = %Exp:cTipo%
		   AND SZW.ZW_GRUPO  = %Exp:cGrupo%
		   AND SZW.ZW_CATEG  = %Exp:cCateg%
		   AND SZW.ZW_ATRIB  = %Exp:cAtrib% 
		   AND SZW.%NotDel% 
	EndSql
		
	If (cAliasQry)->(Eof())
		lRet := .F.
		Help(,, 'STNA120',, 'O Atributo informado n�o existe ou n�o est� relacionado ao Grupo/Categoria informados!', 1, 0)
	EndIf
EndIf

//--Encerra Query
(cAliasQry)->(DbCloseArea())

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA120ExcPrc�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Trata exclusao do processo de atualizacao cadastral vincula_���
���          �do ao item da matriz de desenvolvimento                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA120ExcPrc(cIdProc, cItem)
Local cAliasQry := GetNextAlias()


BeginSql Alias cAliasQry
	SELECT SZT.R_E_C_N_O_ SZTRecno
	  FROM %Table:SZT% SZT
	 WHERE SZT.ZT_FILIAL = %xFilial:SZT%
	   AND SZT.ZT_IDPROC = %Exp:cIdProc%
	   AND SZT.ZT_ITEM   = %Exp:cItem% 
	   AND SZT.%NotDel%
EndSQL 

While !(cAliasQry)->(Eof())
	SZT->(DbGoTo((cAliasQry)->SZTRecno))

	//-- APAGA ITENS
	SZU->(DbSetOrder(1)) //--ZU_FILIAL+ZU_PROCESS
	SZU->(DbSeek(xFilial('SZU')+SZT->ZT_PROCESS))
	While !SZU->(Eof()) .And. SZU->(ZU_FILIAL+ZU_PROCESS) == xFilial('SZU')+SZT->ZT_PROCESS
		RecLock('SZU', .F.)
		SZU->(DbDelete())
		SZU->(MsUnLock())
		SZU->(DbSkip())
	End
					
	//-- APAGA CABECALHO
	RecLock('SZT', .F.)
	SZT->(DbDelete())
	SZT->(MsUnlock())
	
	(cAliasQry)->(DbSkip())
End

//-- RESTAURA AMBIENTE
(cAliasQry)->(DbCloseArea())

Return


  