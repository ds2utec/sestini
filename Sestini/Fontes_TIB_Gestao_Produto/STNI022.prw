#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TBICONN.CH"
#INCLUDE "FILEIO.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI022   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �INTEGRACOES RAKUTEN - ESTOQUES                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI022(cProdDe, cProdAte)
Local cAliasQry     := ''
Local cQuery        := ''
Local cArmEcomm     := GetMV('ES_ARECOMM')
Local aPedidos      := {}
Local nSaldo        := 0
Local nPosA         := 0
Local aSaldos       := {}
Local cQuebra       := ''
Local aRestr        := {}
Local aArea         := GetArea()
Local aAreaSB2      := SB2->(GetArea())

Default cProdDe     := Repl(' ', Len(SB1->B1_COD))
Default cProdAte    := Repl('Z', Len(SB1->B1_COD))

//-- OBTEM OS ITENS COM RESTRICAO AO CANAL E-COMM
If IsBlind()
	aRestr := STNI020Restr(cProdDe, cProdAte)

Else
	MsgRun('Obtendo itens com restri��o ao canal de e-comm...', 'Aguarde...',;
			{|| aRestr := STNI020Restr(cProdDe, cProdAte)})

EndIf


// ------------------------------------------
// OBTEM OS PEDIDOS A SEREM CONSIDERADOS PARA
// CALCULO DO SALDO DISPONIVEL
// ------------------------------------------
If IsBlind()
	aPedidos := STNI022Ped(cProdDe, cProdAte)

Else
	MsgRun('Aguarde, obtendo movimento de pedidos - Rakuten...', 'Aguarde...',;
			{|| aPedidos := STNI022Ped(cProdDe, cProdAte)})

EndIf

// -------------------------------
// APURA SALDO DISPONIVEL PRODUTOS
// -------------------------------
cQuery := "SELECT SB2.R_E_C_N_O_ SB2Recno "
cQuery += "  FROM " + RetSQLTab('SB2')
cQuery += " WHERE SB2.B2_FILIAL = '" + xFilial('SB2') + "' "
cQuery += "   AND SB2.B2_COD BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND SB2.B2_LOCAL IN " + FormatIn(cArmEcomm, ';')
cQuery += "   AND SB2.D_E_L_E_T_ = ' ' "
cQuery := ChangeQuery(cQuery)

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)
While !(cAliasQry)->(Eof())
	SB2->(DbGoTo((cAliasQry)->SB2Recno))
	If aScan(aRestr, SB2->B2_COD) == 0
		nPosA  := aScan(aPedidos, {|x| AllTrim(x[2]) == AllTrim(SB2->B2_COD)})
		nSaldo := SaldoSB2(,,,,, 'SB2') - If(nPosA > 0, aPedidos[nPosA, 4], 0)
		aAdd(aSaldos, {SB2->B2_COD, SB2->B2_LOCAL, nSaldo})
	EndIf			
	(cAliasQry)->(DbSkip())
End
(cAliasQry)->(DbCloseArea())

// -----------------------------
// APURA SALDO DISPONIVEL - KITS
// -----------------------------
cQuery := "SELECT ZB2.ZB2_COD, ZB2.ZB2_COMP "
cQuery += "  FROM " + RetSQLTab('ZB2')
cQuery += " WHERE ZB2.ZB2_FILIAL = '" + xFilial('ZB2') + "' "
cQuery += "   AND ZB2.ZB2_COD BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND ZB2.D_E_L_E_T_ = ' ' "

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)
While !(cAliasQry)->(Eof())
	cQuebra := (cAliasQry)->ZB2_COD		
	While !(cAliasQry)->(Eof()) .And. (cAliasQry)->ZB2_COD == cQuebra			
		nPosA := aScan(aSaldos, {|x| x[1] == (cAliasQry)->ZB2_COMP})
		If nPosA == 0
			SB2->(DbSetOrder(1)) //--B2_FILIAL+B2_COD+B2_LOCAL
			If SB2->(DbSeek(xFilial('SB2')+(cAliasQry)->ZB2_COMP+cArmEcomm))
				nPosA  := aScan(aPedidos, {|x| AllTrim(x[2]) == AllTrim(SB2->B2_COD)})
				nSaldo := SaldoSB2(,,,,, 'SB2') - If(nPosA > 0, aPedidos[nPosA, 4], 0)
			EndIf
		Else
			nSaldo := aSaldos[nPosA, 3]
		EndIf			

		nPosA := aScan(aSaldos, {|x| x[1] == cQuebra})
		If nPosA == 0
			aAdd(aSaldos, {cQuebra, cArmEcomm, nSaldo})
		Else
			If aSaldos[nPosA, 3] > nSaldo
				aSaldos[nPosA, 3] := nSaldo
			EndIf
		EndIf			
		(cAliasQry)->(DbSkip())
	End
End
(cAliasQry)->(DbCloseArea())

// ---------------------------
// ATUALIZA O SALDO NA TABELA
// DE SALDOS - RAKUTEN
// ---------------------------
If IsBlind()
	aEval(aSaldos, {|e| STNI022Sld(e[1], e[3])})
	
Else
	MsgRun('Aguarde, processando atualiza��o dos saldos...', 'Aguarde...',; 
			{|| aEval(aSaldos, {|e| STNI022Sld(e[1], e[3])})})

EndIf

//-- RESTAURA AMBIENTE
RestArea(aArea)
RestArea(aAreaSB2)

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI022Ped�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �OBTEM OS PEDIDOS DE VENDA "EM ABERTO" - RAKUTEN             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Static Function STNI022Ped(cProdDe, cProdAte)
Local cAliasQry  := ''
Local cQuery     := ''
Local cStatus    := GetMV('ES_STRAKUT')
Local aPedidos   := {}
Local cDbRakuten := AllTrim(GetMV('ES_DBRAKUT'))
Local nHandle    := 0

If STNI010Conn(.F., cDbRakuten, @nHandle)
	// ------------------------------------------
	// OBTEM OS PEDIDOS A SEREM CONSIDERADOS PARA
	// CALCULO DO SALDO DISPONIVEL
	// ------------------------------------------
	cQuery := "SELECT IT.PedidoID, IT.ItemPartNumber, CB.PedidoStatus, SUM(IT.ItemQtde) ItemQtde	
	cQuery += "  FROM INTEGRATIONQUEUE.dbo.RK_RECEPCAO_PEDIDO_ITEM IT "
	cQuery += "  JOIN INTEGRATIONQUEUE.dbo.RK_RECEPCAO_PEDIDO CB "
	cQuery += "    ON CB.ID = IT.PedidoID
	cQuery += "   AND CB.PedidoStatus IN " + FormatIn(cStatus, ';')
	cQuery += "   AND CB.DataEnvioProtheus = ' ' "
	cQuery += " WHERE IT.ItemPartNumber BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
	cQuery += " GROUP BY IT.PedidoID, IT.ItemPartNumber, CB.PedidoStatus "

	cAliasQry := GetNextAlias()
	DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)
	(cAliasQry)->(DbEval({|| aAdd(aPedidos, {PedidoID, ItemPartNumber, PedidoStatus, ItemQtde})}, {|| .T.}, {|| !Eof()}))

	//--FECHA CONEXAO COM BANCO DE DADOS EXTERNO
	TCUnlink()

EndIf
	
Return(aPedidos)	


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI022Sld�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �ATUALIZA O SALDO DISPONIVEL - RAKUTEN                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Static Function STNI022Sld(cPartNumber, nSaldo)
Local cQuery     := ''
Local nQtdMin    := 0
Local aDadosTXT  := {}
Local nPosA      := 0
Local cDbRakuten := AllTrim(GetMV('ES_DBRAKUT'))
Local nHandle    := 0

If STNI010Conn(.F., cDbRakuten, @nHandle)
	// ---------------------------------------------------------------
	// REALIZA LEITURA ARQUIVO TEXTO C/ INFORMACOES DAS QTDES. MINIMAS
	// ---------------------------------------------------------------
	If Len(aDadosTXT) == 0
		STNI022TXT(@aDadosTXT)
	EndIf
	
	nPosA := aScan(aDadosTXT, {|x| AllTrim(x[1]) == AllTrim(cPartNumber)})
	If nPosA == 0
		nQtdMin := 0
	Else
		nQtdMin := aDadosTXT[nPosA, 2]
	EndIf

	// -----------------------------------------
	// TRATA INCLUSAO DO SALDO NA TABELA RAKUTEN
	// -----------------------------------------
	cQuery := "INSERT "
	cQuery += "  INTO INTEGRATIONQUEUE.dbo.RK_ENVIO_ESTOQUE "
	cQuery += "      (LojaCodigo, "
	cQuery += "       CodigoProdutoInterno, "
	cQuery += "       PartNumber, "
	cQuery += "       QtdEstoque, "
	cQuery += "       QtdMinima, "
	cQuery += "       TipoAlteracao, "
	cQuery += "       DataCriacao, "
	cQuery += "       DataAtualizacao, "
	cQuery += "       StatusProcessamentoID, "
	cQuery += "       MensagemProcessamento, "
	cQuery += "       Processar) "
	cQuery += "VALUES "
	cQuery += "  (0, "															//--LojaCodigo
	cQuery += "   '" + Left(cPartNumber, (At('-', cPartNumber)-1)) + "', "		//--CodigoProdutoInterno
	cQuery += "   '" + AllTrim(cPartNumber) + "', "								//--PartNumber
	cQuery += "   " + AllTrim(Str(nSaldo)) + ", "								//--QtdEstoque
	cQuery += "   " + AllTrim(Str(nQtdMin)) + ", "								//--QtdMinima
	cQuery += "   3, "															//--TipoAlteracao
	cQuery += "   GETDATE(), "													//--DataCriacao
	cQuery += "   NULL, "														//--DataAtualizacao
	cQuery += "   0, "															//--StatusProcessamentoID
	cQuery += "   '', "															//--MensagemProcessamento
	cQuery += "   1) "															//--Processar

	If TCSQLExec(cQuery) < 0
		ConOut("[RAKUTEN][INTEGRACOES] - ERRO AO ATUALIZAR SALDO - " + TCSQLError())
	EndIf
	
	TCUnlink()
EndIf

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI022TXT�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �LEITURA ARQUIVO TEXTO C/ INFORMACOES DAS QTDES. MINIMAS     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Static Function STNI022TXT(aDadosTXT)
Local ofT          := fT():New()
Local cPathRakuten := GetMV('ES_PATHRAK')
Local cFile        := cPathRakuten + If(Right(AllTrim(cPathRakuten), 1) == '\', '', '\') + 'ESTOQUE_MINIMO.CSV'
Local lFirstLine   := .F.
Local cBuffer      := ''
Local lRet         := .T.

If File(cFile) .And. ofT:ft_fUse(cFile) > 0
	ProcRegua(ofT:ft_fRecCount())
	While !( ofT:ft_fEof() )
		If lFirstLine
			ofT:ft_fSkip()
			lFirstLine := .F.
		Else
			cBuffer := Upper(ofT:ft_fReadLn())
			aAdd(aDadosTXT, StrTokArr(cBuffer, ';'))
		EndIf
		ofT:ft_fSkip()
	End
	ofT:ft_fUse()

Else
	lRet := .T.
	ConOut('[RAKUTEN][INTEGRACOES] - NAO FOI POSSIVEL OBTER ARQUIVO COM INFORMACOES DAS QTDES. MINIMAS')

EndIf

Return(lRet)
