#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TBICONN.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.ENTRADA �MATA010   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �PONTO DE ENTRADA GENERICO MVC - CADASTRO DE PRODUTOS        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
User Function ITEM()
Local aParam        := PARAMIXB
Local oModel        := NIL
Local cIdModel      :=''
Local cIdPonto      := ''
Local aDadosSBZ     := {}
Local nOpc          := 0
Local lRet          := .T.
Local cConteudo     := ''
Local aFiliais      := {}
Local nCountA       := 0
Local cFilBkp       := FWCodEmp() + FWUnitBusiness() + FWFilial()
Local aAreaSX3      := SX3->(GetArea())
Private lMsErroAuto := .F.

If aParam <> NIL
	oModel   := aParam[1]
	cIdPonto := aParam[2]
	cIdModel := aParam[3]	
	If cIdPonto == 'MODELCOMMITTTS' 
		If oModel:GetOperation() == MODEL_OPERATION_INSERT .Or. oModel:GetOperation() == MODEL_OPERATION_UPDATE
			// ----------------------------------------------
			// INCLUSAO AUTOMATICA: SBZ - INDICADORES PRODUTO	
 			// ----------------------------------------------
			nOpc := If(oModel:GetOperation() == MODEL_OPERATION_INSERT, 3, 4)

			aFiliais := FWAllFilial(FWCodEmp(), FWUnitBusiness())
			
			For nCountA := 1 To Len(aFiliais)
				//--MODIFICA A FILIAL CORRENTE:
				cFilAnt := FWCodEmp() + FWUnitBusiness() + aFiliais[nCountA]

				aDadosSBZ := {	{'BZ_FILIAL'	, FWCodEmp() + FWUnitBusiness() + aFiliais[nCountA]	, NIL},;
								{'BZ_COD'		, SB1->B1_COD										, NIL}}

				SZ2->(DbSetOrder(1)) //--Z2_FILIAL+Z2_TIPO+Z2_CAMPO+Z2_GRTRIB
				If SZ2->(DbSeek(xFilial('SZ2')+SB1->(B1_TIPO+B1_GRTRIB)))
					While !SZ2->(Eof()) .And. SZ2->(Z2_FILIAL+Z2_TIPO+Z2_GRTRIB) == xFilial('SZ2')+SB1->(B1_TIPO+B1_GRTRIB)
						SX3->(DbSetOrder(2))
						SX3->(DbSeek(SZ2->Z2_CAMPO))
						If SX3->X3_TIPO == 'C'
							cConteudo := PadR(SZ2->Z2_CONTEUD, SX3->X3_TAMANHO)
		
						ElseIf SX3->X3_TIPO == 'M'
							cConteudo := SZ2->Z2_CONTEUD
			
						ElseIf SX3->X3_TIPO == 'N'
							cConteudo := Val(SZ2->Z2_CONTEUD)
		
						ElseIf SX3->X3_TIPO == 'D'
							cConteudo := CtoD(SZ2->Z2_CONTEUD)

						EndIf

						aAdd(aDadosSBZ, {AllTrim(SZ2->Z2_CAMPO), cConteudo, NIL})
					
						SZ2->(DbSkip())
					End

					MsgRun('Realizando cadastro Ind. Produto... Produto: ' + SB1->B1_COD, 'Aguarde...',;
							{|| MSExecAuto({|x, y, z| MATA018(x, y)}, aDadosSBZ, nOpc)} )

					If lMsErroAuto
						Help(,, 'STNMATA010',, 'Ocorreu um erro ao efetivar o cadastro do Indicador de Produto! - Filial: ' + aFiliais[nCountA], 1, 0)
						MostraErro()
						MsgAlert('O produto foi inclu�do com sucesso! Por�m, a manuten��o de Indicadores de Produto n�o pode ser efetivado')
					EndIf
				EndIf

			Next nCountA

			//--RESTAURA EMPRESA/UNIDADE/FILIAL:
			cFilAnt := cFilBkp

		EndIf
	EndIf
EndIf

//--RESTAURA AMBIENTE
RestArea(aAreaSX3)

Return(lRet)		