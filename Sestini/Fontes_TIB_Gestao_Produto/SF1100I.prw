#INCLUDE "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���P.ENTRADA �SF1100I   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �PONTO DE ENTRADA APOS A INCLUSAO DA NOTA FISCAL DE ENTRADA  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
User Function SF1100I()
Local oDlg         := NIL
Local oGetDados    := NIL
Local oGrpGetDados := NIL
Local lOk          := .T.
Local nCountA      := 0
Local aCposAlter   := {'E2_CODBAR', 'E2_LINDIG', 'E2_XFORMA'}

Private aHeader    := {}
Private aCols      := {}

// ---------------------
// MONTA aHeader E aCols
// ---------------------
MsgRun('Carregando dados do t�tulo...', 'Aguarde...', {|| SE2Load()})

// ---------------------------
// MONTA INTERFACE COM USUARIO
// ---------------------------
Define MSDialog oDlg From 000, 000 To 310, 610 Title 'Informa��es Adicionais' Of oMainWnd Pixel
	oGrpGetDados := TGroup():New(040, 005, 150, 300, 'Dados do T�tulo', oDlg,,, .T.)
	oGetDados    := MSGetDados():New(050, 010, 145, 295, 3, 'U_VldLin()','U_VldTOk','', .F., aCposAlter,,, 99)
Activate MSDialog oDlg On Init EnchoiceBar(oDlg, {|| If(oGetDados:TudoOk(), (lOk := .T., oDlg:End()), NIL)}, {|| lOk := .F., oDlg:End()}) Centered


// -------------------------
// TRATA CONFIRMACAO DA TELA
// -------------------------
If lOk
	For nCountA := 1 To Len(aCols)
		SE2->(DbSetOrder(1)) //--E2_FILIAL+E2_PREFIXO+E2_NUM+E2_PARCELA+E2_TIPO+E2_FORNECE+E2_LOJA
		If SE2->(DbSeek(xFilial('SE2')+GdFieldGet('E2_PREFIXO', nCountA)+GdFieldGet('E2_NUM', nCountA)+GdFieldGet('E2_PARCELA', nCountA)+GdFieldGet('E2_TIPO', nCountA)+GdFieldGet('E2_FORNECE', nCountA)+GdFieldGet('E2_LOJA', nCountA)))
			RecLock('SE2', .F.)
			SE2->E2_CODBAR := GdFieldGet('E2_CODBAR', nCountA) 
			SE2->E2_LINDIG := GdFieldGet('E2_LINDIG', nCountA)     
			SE2->E2_XFORMA := GdFieldGet('E2_XFORMA', nCountA)
			SE2->(MsUnLock())
		EndIf
	Next nCountA			     

	MsgInfo('Grava��o realizada com sucesso!', 'Informa��es Adicionais')

Else
	MsgAlert('Preenchimento das informa��es adicionais cancelada pelo usu�rio...', 'ATEN��O')

EndIf

Return
				

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �VldLin    �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �FUNCAO PARA REALIZAR A VALIDACAO DA LINHA DA MSGETDADOS     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function VldLin()
Local lRet := .T.


Return(lRet)



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �VldTOk    �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �FUNCAO PARA REALIZAR A VALIDACAO DA TELA                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
User Function VldTOk()
Local lRet    := .T.
Local nCountA := 0

lRet := aScan(aCols, {|x| Empty(x[GdFieldPos('E2_XFORMA')])}) == 0
If !lRet
	If MsgYesNo('Existem parcelas sem as informa��es referente ao boleto ou forma de pagamento! Deseja continuar?', 'ATEN��O')
		lRet := .T.
	EndIf
EndIf

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �SE2Load   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �FUNCAO PARA CARREGAR DADOS PARA TELA                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function SE2Load()
Local cQuery    := ''
Local cAliasQry := ''
Local aCpos     := {'E2_PREFIXO', 'E2_NUM', 'E2_PARCELA', 'E2_TIPO', 'E2_FORNECE', 'E2_LOJA', 'E2_VENCREA', 'E2_VALOR', 'E2_CODBAR', 'E2_LINDIG', 'E2_XFORMA'}
Local nCountA   := 0
Local aAreaSX3  := SX3->(GetArea())

For nCountA := 1 To Len(aCpos)
	SX3->(DbSetOrder(2)) //--X3_CAMPO
	If SX3->(DbSeek(aCpos[nCountA]))
		Aadd(aHeader, {})
		Aadd(aHeader[Len(aHeader)], AllTrim(SX3->X3_TITULO))  // 1
		Aadd(aHeader[Len(aHeader)], AllTrim(SX3->X3_CAMPO))   // 2
		Aadd(aHeader[Len(aHeader)], SX3->X3_PICTURE)          // 3
		Aadd(aHeader[Len(aHeader)], SX3->X3_TAMANHO)          // 4
		Aadd(aHeader[Len(aHeader)], SX3->X3_DECIMAL)          // 5
		Aadd(aHeader[Len(aHeader)], SX3->X3_VALID)            // 6
		Aadd(aHeader[Len(aHeader)], SX3->X3_USADO)            // 7
		Aadd(aHeader[Len(aHeader)], SX3->X3_TIPO)             // 8
		Aadd(aHeader[Len(aHeader)], SX3->X3_F3)               // 9
		Aadd(aHeader[Len(aHeader)], SX3->X3_CONTEXT)          // 10
		Aadd(aHeader[Len(aHeader)], SX3->X3_CBOX)             // 11
		Aadd(aHeader[Len(aHeader)], SX3->X3_RELACAO)          // 12
		Aadd(aHeader[Len(aHeader)], SX3->X3_WHEN)             // 13
		Aadd(aHeader[Len(aHeader)], SX3->X3_VISUAL)           // 14
		Aadd(aHeader[Len(aHeader)], SX3->X3_VLDUSER)          // 15
		Aadd(aHeader[Len(aHeader)], SX3->X3_PICTVAR)          // 16
		Aadd(aHeader[Len(aHeader)], X3Obrigat(SX3->X3_CAMPO)) // 17
	EndIf
Next nCountA	

cQuery := "SELECT SE2.E2_PREFIXO, SE2.E2_NUM, SE2.E2_PARCELA, SE2.E2_PARCELA, SE2.E2_TIPO, SE2.E2_FORNECE, SE2.E2_LOJA, " 
cQuery += "       SE2.E2_VENCREA, SE2.E2_VALOR, SE2.E2_CODBAR, SE2.E2_LINDIG, SE2.E2_XFORMA, SE2.R_E_C_N_O_ SE2Recno"
cQuery += "  FROM " + RetSQLTab('SE2')
cQuery += " WHERE SE2.E2_FILIAL   = '" + xFilial("SE2") + "' " 
cQuery += "   AND SE2.E2_NUM      = '" + SF1->F1_DUPL + "' "
cQuery += "   AND SE2.E2_TIPO     = 'NF' "
cQuery += "   AND SE2.E2_PREFIXO  = '" + SF1->F1_PREFIXO + "' "
cQuery += "   AND SE2.E2_FORNECE  = '" + SF1->F1_FORNECE + "' "
cQuery += "   AND SE2.E2_LOJA     = '" + SF1->F1_LOJA + "' "
cQuery += "   AND SE2.D_E_L_E_T_  = ' '"

//-- PROCESSA QUERY
cQuery    := ChangeQuery(cQuery)
cAliasQry := GetNextAlias()
dbUseArea(.T., "TOPCONN", TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

(cAliasQry)->(dbEval({|| aAdd(aCols, {E2_PREFIXO, E2_NUM, E2_PARCELA, E2_TIPO, E2_FORNECE, E2_LOJA, E2_VENCREA, E2_VALOR, E2_CODBAR, E2_LINDIG, E2_XFORMA, SE2Recno, .F.})},; 
				{|| .T.},;
				{|| !Eof()}))
	

(cAliasQry)->(dbCloseArea())


RestArea(aAreaSX3)
Return
