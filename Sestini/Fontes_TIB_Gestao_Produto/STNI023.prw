#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "TBICONN.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI023   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �INTEGRACOES RAKUTEN - PRECOS DE VENDA                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI023(cProdDe, cProdAte, cTabPrc)
Local cDbCorp   := GetMV('ES_DBCORP')
Local nHandle   := 0
Local aRestr    := {}
Local aCores    := {}
Local cQuery    := ''
Local cAliasQry := ''
Local nCountA   := 0
Local nPrcCheio := 0
Local nPrcPor   := 0

Default cProdDe     := Repl(' ', Len(SB1->B1_COD))
Default cProdAte    := Repl('Z', Len(SB1->B1_COD))
Default cTabPrc     := GetMV('ES_TABPRRK')

//-- OBTEM OS ITENS COM RESTRICAO AO CANAL E-COMM
If IsBlind()
	aRestr := STNI020Restr(cProdDe, cProdAte)

Else
	MsgRun('Obtendo itens com restri��o ao canal de e-comm...', 'Aguarde...',;
			{|| aRestr := STNI020Restr(cProdDe, cProdAte)})

EndIf

//-- OBTEM OS PRODUTOS A SEREM ENVIADOS
cQuery := "SELECT SZP.*, SB1.* "
cQuery += "  FROM " + RetSQLTab('SZP')
cQuery += "  JOIN " + RetSQLTab('SB1')
cQuery += "    ON SB1.B1_FILIAL = '" + xFilial('SB1') + "' "
cQuery += "   AND SB1.B1_COD = SZP.ZP_CODPRO "
cQuery += "   AND SB1.D_E_L_E_T_ = ' ' "
cQuery += " WHERE SZP.ZP_FILIAL = '" + xFilial('SZP') + "' "
cQuery += "   AND SZP.ZP_CODPRO BETWEEN '" + cProdDe + "' AND '" + cProdAte + "' "
cQuery += "   AND SZP.D_E_L_E_T_ = ' ' " 
cQuery := ChangeQuery(cQuery)

cAliasQry := GetNextAlias()
DbUseArea(.T., 'TOPCONN', TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

While !(cAliasQry)->(Eof())
	If aScan(aRestr, (cAliasQry)->ZP_CODPRO) == 0
		// ----------------------------------------------------
		// OBTEM OS PRODUTOS "FILHOS" A PARTIR DO PRODUTO "PAI" 	
		// ----------------------------------------------------
		aCores := {	(cAliasQry)->ZP_COR1,;
					(cAliasQry)->ZP_COR2,;
					(cAliasQry)->ZP_COR3,;
					(cAliasQry)->ZP_COR4,;
					(cAliasQry)->ZP_COR5,;
					(cAliasQry)->ZP_COR6,;
					(cAliasQry)->ZP_COR7,;
					(cAliasQry)->ZP_COR8,;
					(cAliasQry)->ZP_COR9,;
					(cAliasQry)->ZP_CORA }			

		// -----------------------------------------
		// TRATA GRAVACAO/ATUALIZACAO DO PRODUTO
		// -----------------------------------------
		For nCountA := 1 To Len(aCores)
			If !Empty(aCores[nCountA])

				//-- OBTEM PRECO DE VENDA (PRECO "CHEIO" E PRECO "POR")
				nPrcCheio := 0
				nPrcPor   := 0
				DA1->(DbSetOrder(2)) //-- DA1_FILIAL+DA1_CODPRO+DA1_CODTAB+DA1_ITEM
				If DA1->(DbSeek(xFilial('DA1')+AllTrim((cAliasQry)->ZP_CODPRO) + '-' + aCores[nCountA]+cTabPrc))
					nPrcCheio := DA1->DA1_XPRCOR
					nPrcPor   := DA1->DA1_PRCVEN
				EndIf

				cQuery := "INSERT "
				cQuery += "  INTO " + cDbCorp + ".dbo.K_R_RK_APROVACAO_PRECO "
				cQuery += "       (HANDLE, "
				cQuery += "        Z_GRUPO, "
				cQuery += "        CODIGOREFERENCIA, "
				cQuery += "        PRECOCHEIO, "
				cQuery += "        PRECOPOR, "
				cQuery += "        DATAALTERACAO, "
				cQuery += "        DATAAPROVACAO, "
				cQuery += "        DATAREPROVACAO, "
				cQuery += "        USUARIO, "
				cQuery += "        STATUS) "
				cQuery += " VALUES ("
				cQuery += AllTrim(Str(STNI020Prox('K_R_RK_APROVACAO_PRECO', 'HANDLE'))) + ", "		//-- <HANDLE, int>
				cQuery += "'', "																	//-- <Z_GRUPO, int>
				cQuery += "'" + AllTrim((cAliasQry)->ZP_CODPRO) + '-' + aCores[nCountA] + "', "		//-- <CODIGOREFERENCIA, varchar(20)>
				cQuery += AllTrim(Str(nPrcCheio)) + ", "											//-- <PRECOCHEIO, decimal(15,2)>
				cQuery += AllTrim(Str(nPrcPor)) + ", "												//-- <PRECOPOR, decimal(15,2)>
				cQuery += "GETDATE(), "																//-- <DATAALTERACAO, datetime>
				cQuery += "NULL, "																	//-- <DATAAPROVACAO, datetime>
				cQuery += "NULL, "																	//-- <DATAREPROVACAO, datetime>
				cQuery += "'" + cUserName + "', "													//-- <USUARIO, varchar(20)>
				cQuery += "1) "																		//-- <STATUS, int>

				If STNI010Conn(.F., cDbCorp, @nHandle)
					If TCSQLExec(cQuery) < 0
						ConOut("[RAKUTEN][INTEGRACOES] - ERRO AO ATUALIZAR CADASTRO PRODUTO FILHO - " + TCSQLError())
					EndIf
					TCUnlink()
				EndIf

			EndIf
		Next nCountA
	EndIf
	(cAliasQry)->(DbSkip())
End

(cAliasQry)->(DbCloseArea())

Return