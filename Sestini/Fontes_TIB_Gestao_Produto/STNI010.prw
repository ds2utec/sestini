#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE "APWIZARD.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI010   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �COCKPIT PROCESSOS DE INTEGRACAO                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI010()
Local oWizard    := NIL
Local cMsgIntro  := ''
Local cMsgFinal  := ''
Local oGrpIntegr := NIL
Local oOptIntegr := NIL
Local nIntegr    := 0
Local nPanelBack := 0
Local lOk        := .F.
Local oGrpParam01 := NIL
Local oGrpParam02 := NIL
Local oGrpParam03 := NIL
Local oGrpParam04 := NIL
Local oGrpParam05 := NIL
Local aGets       := {{NIL, NIL}, {NIL, NIL}, {NIL, NIL, NIL}, {NIL, NIL}, {NIL, NIL, NIL, NIL, NIL}, {}}
Local aParams     := {'', '', '', '', '', '', '', '', '', ''}

// ----------------------------------------
// TESTA CONEXAO COM BANCO DE DADOS RAKUTEN
// ----------------------------------------
If STNI010Conn(.T.)

	// ---------------------------------------
	// PAINEL PRINCIPAL - BOAS VINDAS
	// ---------------------------------------
	cMsgIntro := 'Esta rotina t�m o objetivo de auxiliar no processo de execu��o das integra��es com o Portal Rakuten.' + CRLF
	cMsgIntro += 'Siga os passos at� a conclus�o do assistente.....'

	oWizard := ApWizard():New(	'Cockpit - Integra��es Rakuten',;														//-- TITULO DO CABECALHO
								'Assistente Integra��es Rakuten',; 														//-- MENSAGEM DO CABECALHO
								'Siga o assistente at� o fim para processar as integra��es com o Portal Rakuten',;		//-- TITULO DO PAINEL DE APRESENTACAO
								cMsgIntro,;																				//-- TEXTO DO PAINEL DE APRESENTACAO
								{|| .T.},;																				//-- BLOCO DE CODIGO DO BOTAO "AVANCAR"
								{|| .T.},;																				//-- BLOCO DE CODIGO P/ VALIDAR O BOTAO "FINALIZAR"
								.T.,,,;																					//-- HABILITA (.T.) PAINEL OU SCROLL
								.F.)


	// ---------------------------------------
	// PAINEL: SELECAO DA INTEGRACAO
	// ---------------------------------------
	aIntegr := {'Inbound - Cadastro de Produtos - Pai',;
				'Inbound - Cadastro de Produtos - Filho',;
				'Inbound - Pre�o de Venda',;
				'Inbound - Saldos em Estoque',;
				'Inbound - Status Pedido',;
				'Outbound - Pedidos de Venda' }

	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;																	//-- TITULO 
						'Selecione a integra��o � ser executada...',;														//-- MENSAGEM
						{|| .T.},; 																							//-- ACAO DO BOTAO "VOLTAR"
 						{|| If(nIntegr == 0, .F., (STNI010SetVar(nIntegr, @aParams), oWizard:SetPanel(nIntegr+1), .T.))},;	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 																							//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 																								//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )																							//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO


	oGrpIntegr := TGroup():New(005, 005, 100, 290, 'Selecione os processos para Integra��o', oWizard:GetPanel(2),,, .T.)
	oOptIntegr := TRadMenu():New(020, 010, aIntegr, {|x| If(PCount() == 0, nIntegr, nIntegr := x)}, oGrpIntegr,,,,,,,, 120, 12,,,, .T.) 

	// ---------------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO CADASTRO DE PRODUTOS PAI
	// ---------------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;					//-- TITULO 
						'Integra��o Cadastro de Produtos Pai (Inbound)',; 	//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 					//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 4, oWizard:SetPanel(8), .T.)},; 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 											//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 												//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )											//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO


	oGrpParam01 := TGroup():New(005, 005, 135, 295, 'Informe os par�metros para processamento da integra��o - Cadastro de Produtos Pai', oWizard:GetPanel(3),,, .T.)

	aGets[1, 1] := TGet():New(020, 010, {|u| If(PCount() > 0, aParams[1] := u, aParams[1])}, oGrpParam01, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[1]',,,,,,, 'Produto Inicial: ', 1)

	aGets[1, 2] := TGet():New(045, 010, {|u| If(PCount() > 0, aParams[2] := u, aParams[2])}, oGrpParam01, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Produto Final: ', 1)

	aGets[1, 1]:cF3 := 'SB1'
	aGets[1, 2]:cF3 := 'SB1'



	// ------------------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO CADASTRO DE PRODUTOS FILHOS
	// ------------------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;						//-- TITULO 
						'Integra��o Cadastro de Produtos Filhos (Inbound)',; 	//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 						//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 5, oWizard:SetPanel(8), .T.)},;	 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 												//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 													//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )												//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO


	oGrpParam02 := TGroup():New(005, 005, 135, 295, 'Informe os par�metros para processamento da integra��o - Cadastro de Produtos Filho', oWizard:GetPanel(4),,, .T.)

	aGets[2, 1] := TGet():New(020, 010, {|u| If(PCount() > 0, aParams[1] := u, aParams[1])}, oGrpParam02, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[1]',,,,,,, 'Produto Inicial: ', 1)

	aGets[2, 2] := TGet():New(045, 010, {|u| If(PCount() > 0, aParams[2] := u, aParams[2])}, oGrpParam02, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Produto Final: ', 1)

	aGets[2, 1]:cF3 := 'SB1'
	aGets[2, 2]:cF3 := 'SB1'



	// -----------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO PRECOS DE VENDA
	// -----------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;					//-- TITULO 
						'Integra��o Pre�os de Venda (Inbound)',; 			//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 					//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 6, oWizard:SetPanel(8), .T.)},; 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 											//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 												//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )											//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO

	oGrpParam03 := TGroup():New(005, 005, 135, 295, 'Informe os par�metros para processamento da integra��o - Pre�os de Venda', oWizard:GetPanel(5),,, .T.)

	aGets[3, 1] := TGet():New(020, 010, {|u| If(PCount() > 0, aParams[1] := u, aParams[1])}, oGrpParam03, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[1]',,,,,,, 'Produto Inicial: ', 1)

	aGets[3, 2] := TGet():New(045, 010, {|u| If(PCount() > 0, aParams[2] := u, aParams[2])}, oGrpParam03, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Produto Final: ', 1)

	aGets[3, 3] := TGet():New(070, 010, {|u| If(PCount() > 0, aParams[3] := u, aParams[3])}, oGrpParam03, 030, 010, PesqPict('DA0', 'DA0_CODTAB'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Tabela de Pre�o: ', 1)


	aGets[3, 1]:cF3 := 'SB1'
	aGets[3, 2]:cF3 := 'SB1'
	aGets[3, 3]:cF3 := 'DA0'

	// -----------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO SALDOS EM ESTOQUE
	// -----------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;					//-- TITULO 
						'Integra��o Saldos em Estoque (Inbound)',; 			//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 					//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 7, oWizard:SetPanel(8), .T.)},; 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 											//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 												//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )											//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO

	oGrpParam04 := TGroup():New(005, 005, 135, 295, 'Informe os par�metros para processamento da integra��o - Saldos em Estoque', oWizard:GetPanel(6),,, .T.)

	aGets[4, 1]   := TGet():New(020, 010, {|u| If(PCount() > 0, aParams[1] := u, aParams[1])}, oGrpParam04, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[1]',,,,,,, 'Produto Inicial: ', 1)

	aGets[4, 2]   := TGet():New(045, 010, {|u| If(PCount() > 0, aParams[2] := u, aParams[2])}, oGrpParam04, 100, 010, PesqPict('SB1', 'B1_COD'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Produto Final: ', 1)

	aGets[4, 1]:cF3 := 'SB1'
	aGets[4, 2]:cF3 := 'SB1'


	// -----------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO STATUS DOS PEDIDOS
	// -----------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;					//-- TITULO 
						'Integra��o Status dos Pedidos (Inbound)',;			//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 					//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 8, oWizard:SetPanel(8), .T.)},; 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 											//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 												//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )											//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO

	oGrpParam05 := TGroup():New(005, 005, 135, 295, 'Informe os par�metros para processamento da integra��o - Status dos Pedidos', oWizard:GetPanel(7),,, .T.)

	aGets[5, 1]   := TComboBox():New(020, 010, {|u| If(PCount() > 0, aParams[1] := u, aParams[1])}, {'1=Remessa', '2=Retorno'}, 085, 013, oGrpParam05,,,,,,; 
		                 				.T.,,,,,,,,, 'aParams[1]', 'Tipo Processamento:', 1)

	aGets[5, 2]   := TGet():New(020, 100, {|u| If(PCount() > 0, aParams[2] := u, aParams[2])}, oGrpParam05, 180, 010, '@!',;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[2]',,,,,,, 'Pasta/Arquivo: ', 1)

	aGets[5, 3]   := TGet():New(050, 010, {|u| If(PCount() > 0, aParams[3] := u, aParams[3])}, oGrpParam05, 030, 010, PesqPict('SF2', 'F2_SERIE'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[3]',,,,,,, 'Serie Nota Fiscal: ', 1)

	aGets[5, 4]   := TGet():New(075, 010, {|u| If(PCount() > 0, aParams[4] := u, aParams[4])}, oGrpParam05, 050, 010, PesqPict('SF2', 'F2_DOC'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[4]',,,,,,, 'Da Nota Fiscal: ', 1)

	aGets[5, 5]   := TGet():New(100, 010, {|u| If(PCount() > 0, aParams[5] := u, aParams[5])}, oGrpParam05, 050, 010, PesqPict('SF2', 'F2_DOC'),;
								{|| .T.},,,, .F.,, .T.,, .F., {|| .T.}, .F., .F.,, .F., .F.,, 'aParams[5]',,,,,,, 'At� a Nota Fiscal: ', 1)

	aGets[5, 2]:cF3 := 'DIR'


	// -----------------------------------------------------
	// PAINEL: PARAMETROS P/ INTEGRACAO PEDIDOS DE VENDA
	// -----------------------------------------------------
	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;					//-- TITULO 
						'Integra��o Pedidos de Venda (Outbound)',;			//-- MENSAGEM
						{|| (oWizard:SetPanel(3), .T.)},; 					//-- ACAO DO BOTAO "VOLTAR"
 						{|| (nPanelBack := 9, oWizard:SetPanel(8), .T.)},; 	//-- ACAO DO BOTAO "AVANCAR"
						{|| .T.},; 											//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 												//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )											//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO



	// ---------------------------------------
	// PAINEL: ULTIMO PAINEL, CONFIRMACAO
	// ---------------------------------------
	cMsgFinal := 'Estamos prontos para processar a integra��o.' + CRLF
	cMsgFinal += 'Clique no bot�o "Finalizar" para iniciar o processamento...' + CRLF + CRLF
	cMsgFinal += 'IMPORTANTE: Dependendo das configura��es dos par�metros, esse processo pode ser demorado.'

	oWizard:NewPanel(	'Cockpit - Integra��es Rakuten',;				//-- TITULO 
						'Pronto para processar....',;					//-- MENSAGEM
						{|| oWizard:SetPanel(nPanelBack), .T.},;		//-- ACAO DO BOTAO "VOLTAR"
 						{|| .F.},; 										//-- ACAO DO BOTAO "AVANCAR"
						{|| lOk := .T.},; 								//-- VALIDACAO DO BOTAO "FINALIZAR"
						.F.,; 											//-- HABILITA (.T.) PAINEL OU SCROLL 
						{|| .T.} )										//-- BLOCO DE CODIGO QUANDO O PAINEL FOR SELECIONADO

	TSay():New( 010, 005, {|| cMsgFinal}, oWizard:GetPanel(9),,,,,, .T.,,, 290, 100,,,,,, .F.)



	//-- ATIVA WIZARD:
	oWizard:Activate( .T., {|| .T. }, /*<bInit>*/, /*<bWhen>*/)


	// -----------------------------
	// TRATA CONFIRMACAO DO PROCESSO
	// -----------------------------
	If lOk
		If nIntegr == 1
			//-- INTEGRACAO INBOUND: CADASTRO DE PRODUTOS PAI
				MsgRun('Realizando Integra��o - Cad. Produtos (PAI)', 'Aguarde....',;
					{|| STNI020(aParams[1], aParams[2])})
	
		ElseIf nIntegr == 2
			//-- INTEGRACAO INBOUND: CADASTRO DE PRODUTOS FILHO
				MsgRun('Realizando Integra��o - Cad. Produtos (FILHO)', 'Aguarde....',;
					{|| STNI021(aParams[1], aParams[2])})	

		ElseIf nIntegr == 3
			//-- INTEGRACAO INBOUND: PRECO DE VENDA
			MsgRun('Realizando Integra��o - Pre�os de Venda', 'Aguarde....',;
					{|| STNI023(aParams[1], aParams[2], aParams[3])})
	
		ElseIf nIntegr == 4
			//-- INTEGRACAO INBOUND: SALDOS EM ESTOQUE
			MsgRun('Realizando Integra��o - Saldos em Estoque', 'Aguarde....',;
					{|| STNI022(aParams[1], aParams[2])})
	
		ElseIf nIntegr == 5
			//-- INTEGRACAO INBOUND: STATUS PEDIDO
	
		ElseIf nIntegr == 6
			//-- INTEGRACAO OUTBOUND: PEDIDOS DE VENDA

		EndIf

	EndIf

Else
	MsgAlert('N�o foi poss�vel estabelecer a comunica��o com o banco de dados Rakuten!', 'ATEN��O')

EndIf

Return


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI010Conn �Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �DETERMINA OS PARAMETROS (SETA VARIAVEIS)                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Static Function STNI010SetVar(nIntegr, aParams)

aParams := {}

If nIntegr == 1
	//-- INTEGRACAO INBOUND: CADASTRO DE PRODUTOS PAI
	aAdd(aParams, CriaVar('B1_COD', .F.))
	aAdd(aParams, CriaVar('B1_COD', .F.))
aAdd(aParams, ' ')
aAdd(aParams, ' ')
aAdd(aParams, ' ')

ElseIf nIntegr == 2
	//-- INTEGRACAO INBOUND: CADASTRO DE PRODUTOS FILHOS
	aAdd(aParams, CriaVar('B1_COD', .F.))
	aAdd(aParams, CriaVar('B1_COD', .F.))
aAdd(aParams, ' ')
aAdd(aParams, ' ')
aAdd(aParams, ' ')

ElseIf nIntegr == 3
	//-- INTEGRACAO INBOUND: PRECO DE VENDA
	aAdd(aParams, CriaVar('B1_COD', .F.))
	aAdd(aParams, CriaVar('B1_COD', .F.))
	aAdd(aParams, CriaVar('DA0_CODTAB', .F.))
aAdd(aParams, ' ')
aAdd(aParams, ' ')

ElseIf nIntegr == 4
	//-- INTEGRACAO INBOUND: SALDOS EM ESTOQUE
	aAdd(aParams, CriaVar('B1_COD', .F.))
	aAdd(aParams, CriaVar('B1_COD', .F.))
aAdd(aParams, ' ')
aAdd(aParams, ' ')
aAdd(aParams, ' ')

ElseIf nIntegr == 5
	//-- INTEGRACAO INBOUND: STATUS PEDIDO
	aAdd(aParams, ' ')							//-- TIPO DO PROCESSAMENTO: 1-ENVIO | 2-RECEPCAO
	aAdd(aParams, Space(100))					//-- DIRETORIO PARA PROCESSAMENTO
	aAdd(aParams, CriaVar('F2_SERIE', .F.))
	aAdd(aParams, CriaVar('F2_DOC', .F.))
	aAdd(aParams, CriaVar('F2_DOC', .F.))

ElseIf nIntegr == 6
	//-- INTEGRACAO OUTBOUND: PEDIDOS DE VENDA

EndIf


Return



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNI010Conn �Autor  �V. Raspa                               ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �FUNCAO RESPONSAVEL PELA CONEXAO BD RAKUTEN                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNI010Conn(lEncerra, cDatabase, nHandle)
Local cDriver     := GetSrvProfString('TOPDATABASE', 'MSSQL')
Local cDbServer   := AllTrim(GetMV('ES_IPRAKUT'))
Local nPorta      := GetMV('ES_PTRAKUT')
Local lRet        := .T.
Default lEncerra  := .F.
Default cDatabase := AllTrim(GetMV('ES_DBRAKUT'))

//-- TRATA O DRIVER P/ CONEXAO
cDataBase := cDriver + '/' + cDataBase

nHandle := TcLink(cDatabase, cDbServer, nPorta)
If nHandle < 0
	lRet := .F.
	ConOut("[RAKUTEN][INTEGRACOES ] - ERRO (" + cValToChar(nHandle) + ") AO CONECTAR COM " + cDatabase + " EM " + cDbServer + ":" + cValToChar(nPorta) + " AS " + Time() + " " + DtoC(Date()))
Else
	lRet := .T.
	If lEncerra
		TCUnlink() //--FECHA CONEXAO COM BANCO DE DADOS EXTERNO
	EndIf
EndIf

Return(lRet)





