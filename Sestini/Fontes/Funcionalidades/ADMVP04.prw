#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "AP5MAIL.CH"

Static cMarcado		:= "LBTIK"
Static cDesmark		:= "LBNO"
Static oMotivos		:= ADMVMotivosBlq():New()
Static cCanaisADMV	:= U_ADMVXFUN( 15 , "6")

//-----------------------------------------------
/*/{Protheus.doc} ADMVP04
Rotina para Analise de Credito do cliente

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVP04()

    Local aParamBox := {}
    Local aRet      := {}
    Local aCampos   := {}
    Local aCliente  := {}
    Local lOk       := .F.
    Local cSql      := ""
    Local clAlias   := ""
    Local cCombo    := "1"
    Local aCombo    := Str2Arr("1=Bloquear;2=Liberar",";")
    Local cTpOper	:= "" 
    Local cMotivo	:= ""
        
    aadd( aCampos , "A1_COD"    )
    aadd( aCampos , "A1_LOJA"   )
    aadd( aCampos , "A1_CGC"    )
    aadd( aCampos , "A1_NOME"   )
    aadd( aCampos , "A1_GRPVEN"	)
    aadd( aCampos , "ACY_DESCRI")

    aAdd(aParamBox,{1,"Grupo Matriz De:"	, Space(TamSx3("A1_GRPVEN")[1])	,"@!", "NaoVazio()", GetSx3Cache( "A1_GRPVEN" ,"X3_F3"), , 3, .T.})
    aAdd(aParamBox,{1,"Grupo Matriz At�:"	, Space(TamSx3("A1_GRPVEN")[1])	,"@!", "NaoVazio()", GetSx3Cache( "A1_GRPVEN" ,"X3_F3"), , 3, .T.})
    aAdd(aParamBox,{2,"Bloquear/Liberar", cCombo, aCombo, 60, "" ,.T.})
    aAdd(aParamBox,{9,"Motivo:", 30 , 10 , .T.})
    aAdd(aParamBox,{11,"", "" , "" , "" , .T.})

    If ( lOk := ParamBox(aParamBox, "Compet�ncias", @aRet) )

        cSql	:= ADP4QrCl( aRet[1] , aRet[2] , aCampos )
        cTpOper	:= aRet[3] 
        cMotivo	:= aRet[5]

        clAlias :=  GetNextAlias()

        DbUseArea( .T. , "TOPCONN" , TcGenQry( , , cSql ) , clAlias , .F. , .T. )

        dbSelectArea(clAlias)

        (clAlias)->(dbGoTop())

        If (clAlias)->(!Eof())

            Processa( {|| lOk := ADP4DlgC( clAlias, aCampos , @aCliente ) }, "Consultando Dados..." )

            //----------------------------------------------
            //Processamento dos clientes selecionados
            //----------------------------------------------
            If lOk .And. Len( aCliente ) > 0
                
                FwMsgRun(,{|| ADP4PrcT( aCliente , cTpOper , cMotivo )  },"Aguarde...","Processando...")
                
            EndIf

        Else

            Aviso("ADMVP04-01", "N�o foi localizado nenhum cliente vinculado ao grupo matriz informado "+ aRet[1] +".", {"Fechar"})

        EndIf
        (clAlias)->( dbCloseArea() )

    EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4QrCl
Lista os clientes que pertencem ao grupo martiz
para que o usu�rio escolha quais clientes ser�o
processados

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4QrCl( cGrpMtrDe , cGrpMtrAte, aCampos )

    Local cSql      	:= ""
    Local nC        	:= 0
    
    cSql := " SELECT "

    For nC := 1 To Len(aCampos)
		If nC > 1
			cSql += " , "
		EndIf
		cSql += aCampos[ nC ]
	Next nC

    cSql += " FROM " + RetSqlName("SA1") + " SA1 "
    
    cSql += " INNER JOIN " + RetSqlName("ACY")+ " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"'"
    cSql += " AND ACY_GRPVEN = A1_GRPVEN " 
    
    cSql += " WHERE SA1.A1_FILIAL = '" + FWxFilial("SA1") + "'"
    cSql += " AND SA1.A1_GRPVEN BETWEEN '"+ cGrpMtrDe +"' AND '"+ cGrpMtrAte +"'"
   // cSql += " AND SA1.A1_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cSql += " AND SA1.D_E_L_E_T_ = ' '"
    cSql += " AND ACY.D_E_L_E_T_ = ' '"
    
    cSql += " ORDER BY A1_GRPVEN , A1_COD "

Return cSql

//-----------------------------------------------
/*/{Protheus.doc} ADP4DlgC
ADMV Processamento 4 - Tela para selecionar os
clientes que devem ser atualizados

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4DlgC( clAlias, aCampos , aCliente )

	Local aCoord	:= MsAdvSize(.F.,.F.,0)
	Local oOdlg		:= Nil
	Local olDlg		:= Nil
	Local oGrid		:= Nil
	Local aHeader	:= {}
	Local aCols		:= {}
	Local aButtons	:= {}
	Local nY		:= 0
	Local lOrd		:= .T.
	Local nCtrlLoop	:= 0
	Local cMrkAtu	:= cMarcado
	Local oLayer	:= FWLayer():New()
	Local lRet		:= .F.

	ProcRegua( 0 )

	AAdd(aHeader,{		;
	""					;	// X3_TITULO
	,"MARQUE"			;	// X3_CAMPO
	,"@BMP"				;	// X3_PICTURE
	,4					;	// X3_TAMANHO
	,0000				;	// X3_DECIMAL
	,""					;	// X3_VALID
	,""					;	// X3_USADO
	,"C"				;	// X3_TIPO
	,""					;	// X3_F3
	,"V"				;	// X3_CONTEXT
	,""					;	// X3_CBOX
	,cMarcado			;	// X3_RELACAO
	,""					;	// X3_WHEN
	,""					;	// X3_VISUAL
	,""					;	// X3_VLDUSER
	,""					;	// X3_PICTVAR
	,""					;	// X3_OBRIGAT
	})

	dbSelectArea("SX3")
	SX3->(dbSetOrder(2))
	For nY := 1 To Len( aCampos )

		SX3->( dbSeek( aCampos[nY] ) )

		AAdd(aHeader,{ AllTrim(X3Titulo()), aCampos[nY] , SX3->X3_PICTURE,SX3->X3_TAMANHO,;
		SX3->X3_DECIMAL,SX3->X3_VALID,SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3				,;
		SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO,SX3->X3_WHEN,SX3->X3_VISUAL		,;
		SX3->X3_VLDUSER,SX3->X3_PICTVAR,SX3->X3_OBRIGAT									})

	Next nY

	dbSelectArea( clAlias )

	If !(clAlias)->( Eof() )

		While !( clAlias )->( Eof() )

			IncProc()

			Aadd( aCols, {} )

			For nY := 1 To Len( aHeader )

				If aHeader[nY,2] == 'MARQUE'
					Aadd( aTail( aCols ), cMarcado )
    			Else
					Aadd( aTail( aCols ), (clAlias)->( &(aHeader[nY,2]) ) )
				EndIF

			Next nY

			Aadd( aTail( aCols ), .F.)

			(clAlias)->(dbSkip())

		EndDo

	EndIf

    aAdd(aButtons,{"pesquisa",{||  FPesqGrid(oGrid) 														}, "Pesquisa"           , "Pesquisa"})
    aAdd(aButtons,{"PMSEXCEL",{||  Processa( {|| FExportXls(clAlias, aCampos) }, "Exportando para Excel...")}, "Exportar para Excel", "Exportar para Excel"})

	olDlg := TDialog():New(aCoord[1],aCoord[2],aCoord[6],aCoord[5],"Selecionar Clientes",,,,,,,,,.T.)

		// Layers
		oLayer:Init(olDlg,.F.)

		oLayer:AddLine("UP",95,.F.)
		oLayer:AddCollumn("CABEC",100,.F.,"UP")
		oLayer:AddWindow("CABEC","CLIENTES","Selecionar Clientes",100,.F.,.F.,,"UP",{ || })

		oOdlg	:= oLayer:GetWinPanel("CABEC"		,"CLIENTES"	,"UP"	)

		oGrid := MsNewGetDados():New( 000,000,000,000,0,"AllWaysTrue()","AllWaysTrue()","",,,9999,"AllWaysTrue()",,,oOdlg,aHeader,aCols)
		oGrid:lInsert 				:= .F.
		oGrid:oBrowse:BLDBLCLICK	:= {|x,y| FSClick(x,y,oGrid,.F.) }
		oGrid:OBROWSE:BHEADERCLICK 	:= {|x,y| FOrdGrid(x,y,oGrid,@lOrd, @nCtrlLoop,@cMrkAtu) }
		oGrid:oBrowse:Align 		:= CONTROL_ALIGN_ALLCLIENT

	Activate Msdialog olDlg Centered On Init EnchoiceBar(olDlg, {|| lRet := ADP4CMrk( oGrid , @aCliente ) ,  IIF(lRet, olDlg:End(), Nil )    },{|| olDlg:End() },,aButtons)

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4CMrk
Verifica se foi marcado algum cliente para ser
processado, e retorna os codigos selecionados

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4CMrk( oGrid , aCliente )

	Local nPsChk	:= aScan(oGrid:aHeader,{|x| AllTrim(x[2]) == "MARQUE" 	})
	Local nPsCli	:= aScan(oGrid:aHeader,{|x| AllTrim(x[2]) == "A1_COD" 	})
	Local nPsLj	    := aScan(oGrid:aHeader,{|x| AllTrim(x[2]) == "A1_LOJA" 	})
	Local nPsCdMtrz := aScan(oGrid:aHeader,{|x| AllTrim(x[2]) == "A1_GRPVEN"})
	Local lRet		:= .F.
	Local aCols		:= aClone(oGrid:aCols)
	Local nC		:= 0
	
	aCliente := {}
	
	If aScan( oGrid:aCols, {|x| x[nPsChk] == cMarcado } ) > 0

		lRet := .T.

		For nC := 1 To Len(aCols)

			If aCols[ nC ][ nPsChk ] == cMarcado

                If aScan( aCliente , {|x| x[1] == aCols[ nC ][ nPsCdMtrz ] } ) == 0

                    aadd( aCliente , { aCols[ nC ][ nPsCdMtrz ] , aCols[ nC ][ nPsCli ] + aCols[ nC ][ nPsLj ]  } )

                EndIf

			EndIf

		Next nC

	Else

		Aviso("ADP4CMrk_01","Para confirmar o processamento, � necess�rio ao menos um cliente marcado." ,{"Ok"},3)

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FSClick
Mark na linha do grid

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FSClick(x,y,oGrid , lAll , _ParacMrk )

	Local nPsChk		:= aScan(oGrid:aHeader,{|x| AllTrim(x[2]) == "MARQUE" })
	Default lAll		:= .F.
	Default _ParacMrk	:= cMarcado

	If Len(oGrid:aCols) > 0

		If !lAll

			If Alltrim(oGrid:aCols[oGrid:nAt][nPsChk]) == Alltrim(cMarcado)

				oGrid:aCols[oGrid:nAt][nPsChk] := cDesmark

			Else

				oGrid:aCols[oGrid:nAt][nPsChk] := cMarcado

			EndIf


		Else

			oGrid:aCols[oGrid:nAt][nPsChk] := _ParacMrk

		EndIf

	EndIf

	If !lAll

		oGrid:Refresh(.T.)

	EndIf

Return()

//-----------------------------------------------
/*/{Protheus.doc} FOrdGrid
Ordena as colunas do grid, conforme o click
no header do grid

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FOrdGrid(x,y,oGrid,lOder, nCtrlLoop , cMrkAtu)

	Local nC	:= 0

	If nCtrlLoop >= 1

		nCtrlLoop := 0
		Return()

	EndIf

	nCtrlLoop++

	If ! ( Alltrim(oGrid:aHeader[y][2]) $ "MARQUE")

		If lOder

			aSort( oGrid:aCols,,,{ |w1,w2| w1[y] < w2[y] } )

		Else

			aSort( oGrid:aCols,,,{ |w1,w2| w1[y] > w2[y] } )

		EndIf

		lOder := !lOder

		Eval(oGrid:Obrowse:BChange)

		oGrid:ForceRefresh()

	Else

		// Marca/Desmarca tudo
		If cMrkAtu == cMarcado
			cMrkAtu := cDesmark
		Else
			cMrkAtu := cMarcado
		EndIF

		For nC := 1 To Len(oGrid:aCols)

			oGrid:GoTo(nC)
			FSClick( , 1 , oGrid , .T. , cMrkAtu )

		Next nC

		oGrid:GoTo(1)
		oGrid:Refresh(.T.)

	EndIf

Return()

//-----------------------------------------------
/*/{Protheus.doc} FPesqGrid
Tela de pesquisa do regsitro no Grid

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FPesqGrid( oGrid )

	Local oDlg 			:= Nil
	Local oLayer		:= FWLayer():New()
	Local oSrc			:= Nil
	Local nlX3			:= 0
	Local nlX4			:= 0
	Local oSay			:= Nil
	Local oGet01		:= Nil
	Local oGet02		:= Nil
	Local cOrdem		:= ""
	Local cTpPesq		:= "1"
	Local cFiltro		:= Space(100)
	Local aPesquisa		:= {}
	Local lRet 			:=  .T.
	Local aFields		:= { { "Cod Cliente", "A1_COD"  },;
							 { "CPF/CNPJ"   , "A1_CGC"  },;
							 { "Nome"       , "A1_NOME" } }
	Local aOrders		:= {}
	Local nColPesq		:= 0
	Local nPos			:= 0

	For nPos := 1 To Len(aFields)
		nColPesq := aScan(oGrid:aHeader,{|x| Alltrim(x[2]) == aFields[nPos][2]})
		If nColPesq > 0
			cOrdem := Str(Len(aPesquisa) + 1, 1)
			Aadd(aOrders, { cOrdem, nColPesq } )
			aadd(aPesquisa, cOrdem + "-" + aFields[nPos][1])
		EndIf
	Next

	oDlg := TDialog():New(000,000,200,550,"Pesquisa",,,,,,,,,.T.)

		// Layers
		oLayer:Init(oDlg,.F.)

		oLayer:AddCollumn("ALL",100,.F.)
		oLayer:AddWindow("ALL","PESQUISA","Pesquisa",65, .F., .F.,{||},,{||})

		oSrc	:= oLayer:GetWinPanel("ALL"	,"PESQUISA"		)

		nlX3	:= oSrc:NCLIENTWIDTH * 0.015
		nlX4 	:= ( oSrc:NCLIENTHEIGHT * 0.008 ) + 5

		oSay 	:= tSay():New(nlX4+5,nlX3			,{||"Pesquisar Por:"},oSrc,,,,,,.T.,,,50,)
		oGet01	:= TComboBox():New(nlX4+3,nlX3+ 40	,{|u| If(PCount()>0,Eval({|x| cTpPesq:=x },u),cTpPesq)},aPesquisa,70,10,oSrc,,,,,,.T.,,,,,,,,,"cTpPesq")
		oGet02 	:= TGet():New(nlX4+2,nlX3+ 40 + 80	,{|u| if(PCount()>0,cFiltro:=u,cFiltro)},oSrc ,130,10,"@!",,0,,,.F.,,.T.,,.T.,{|| .T.},.T.,.T.,,.F.,.F.,"","cFiltro",,,, )

	Activate MsDialog oDlg Centered On Init EnchoiceBar(oDlg,{||   oDlg:End()   } , {|| lRet := .F. , oDlg:End()} ,, {}  )

	If lRet

		cFiltro := Alltrim(cFiltro)

		For nPos := 1 To Len(aOrders)
			If Left(cTpPesq,1) == aOrders[nPos, 1]
				nColPesq := aOrders[nPos, 2]
			EndIf
		Next

		nPos := aScan(oGrid:aCols,{|x| Upper(Alltrim(cFiltro)) $ Upper(Alltrim(x[nColPesq]))  })

		If nPos == 0

			lRet := .F.

			Aviso("Pesquisa","N�o foi encontrado nenhum registro." ,{"Ok"},3)

		Else

			oGrid:GoTo(nPos)
			oGrid:Refresh(.T.)

		EndIf

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FExportXls
Exporta os dados da tela para excel

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FExportXls(cTabQry , aCampos )

	Local aAux 			:= {}
	Local nInd			:= 0
	Local aCpos			:= {}
	Local oExcel		:= FWMsExcelEx():New()
	Local cPlan			:= 'Clientes'
	Local cTabPlan		:= 'Clientes'
	Local cArqXml		:= cTabPlan +'_'+ Dtos( dDataBase ) +'_'+ StrTran( Time(), ':', '' ) +'.xml'

    For nInd := 1 To Len( aCampos )
        Aadd( aCpos, { aCampos[nInd] , Alltrim(GetSx3Cache( aCampos[nInd] ,"X3_TITULO")) }  )
    Next nInd

	oExcel:AddWorkSheet( cPlan )
	oExcel:AddTable( cPlan, cTabPlan )
	For nInd := 1 To Len( aCpos )
		oExcel:AddColumn( cPlan, cTabPlan, aCpos[ nInd ][ 2 ], 1 , 1, .F. )
	Next nInd

	(cTabQry)->(dbGoTop())
	While !( cTabQry )->( Eof() )
		aAux := {}

		For nInd := 1 To Len(aCpos)

			Aadd( aAux, (( cTabQry )->( &(aCpos[ nInd ][ 1 ])  )) )

		Next nInd

		oExcel:AddRow( cPlan, cTabPlan, aAux )

		( cTabQry )->( dbSkip() )
	EndDo

	oExcel:Activate()
	oExcel:GetXMLFile( cArqXml )

	If !( CpyS2T ( cArqXml, GetTempPath(.T.), .T. ) )
		ApMsgStop( 'Erro ao Copiar Arquivo Xlm para a Temp da maquina', 'Atencao' )
		lRet := .F.
	Else
		If ( ApMsgYesNo( 'Arquivo gerado com sucesso em: '+ CRLF +'[ '+ GetTempPath(.T.) +' ]'+ CRLF +'Deseja abrir a planilha'+ CRLF + cArqXml + CRLF +'?', 'Arquivo Gerado' ) )
			ShellExecute("Open", GetTempPath(.T.) + "\" + cArqXml, "",  GetTempPath(.T.), 3)
		EndIf
	EndIf

	If ValType( oExcel) == "O"
		FreeObj(oExcel)
		oExcel:= Nil
	EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4PrcT
ADMV Processamento 4 - Processamento da analise
de credito de acordo com os clientes selecionados
na tela de processamento em lote, com filtro
por grupo matriz

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4PrcT( aCliente , cTpOper , cMotivo )

    Local nC        := 0
    Local aAreaSA1  := SA1->(GetArea())

    dbSelectArea("SA1")
    SA1->( dbSetOrder(1) )
    For nC := 1 To Len( aCliente )

        If SA1->( dbSeek( FWxFilial("SA1") + aCliente[ nC ][ 2 ] ) )

        	If cTpOper == "1"

			    //---------------------
			    // Bloquear Clientes
			    //---------------------	
        		U_ADP4ACrd( "CLI" , SA1->A1_COD , SA1->A1_LOJA , /*cFilPed*/ , /*cPedido*/ , cMotivo , /*@nVlrLCD*/ )

        	Else

			    //-------------------------------
			    //Liberar Credito Cliente
			    //-------------------------------
			    U_ADP4BlqC( SA1->A1_COD , SA1->A1_LOJA , cUserName , cMotivo , "L" )
			    	        	
        	EndIf

        EndIf

    Next nC

    RestArea(aAreaSA1)

Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4APV
Chama a Rotina para analise de Credito do Pedido
de Venda

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4APV()

	If Alltrim( SC5->C5_XSITUAC ) <> "S" 

		If Empty( SC5->C5_NOTA ) 

			If U_ADMVXFUN( 16 , Alltrim(SC5->C5_XCANAL ) )  

				FwMsgRun(,{|| U_AvPedADMV( "1" ,  SC5->C5_NUM  )  },"Aguarde...","Realizando an�lise de Cr�dito do Pedido de Venda...")
			
			Else
			
				Help(,, "ADP4APV_03",, 'Canal do Pedidos de Venda n�o permite que seja analisado.', 1, 0)
				
			EndIf

		Else
			
			Help(,, "ADP4APV_02",, 'Somente Pedidos de Venda em aberto podem ser analisados.', 1, 0)
			
		EndIf
	
	Else
	
		Help(,, "ADP4APV_01",, 'O Pedido de Venda est� Suspenso.', 1, 0)
	
	EndIf
	
Return

//-----------------------------------------------
/*/{Protheus.doc} Class ADMVMotivosBlq
Classe com os Motivos de Bloqueio de Credito

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Class ADMVMotivosBlq

    Data LcVencido      As String
    Data LCDExc         As String
    Data VencMaior      As String
    Data Juros          As String
    Data SemLC          As String
    Data ChequeDev      As String
    Data Susp           As String
    Data CondPgto       As String
    Data CliCredBlq		As String
    Data LcVencEnt		As String
    Data VencSemFt		As String
    
	Method New() CONSTRUCTOR

EndClass

//-----------------------------------------------
/*/{Protheus.doc} New ADMVMotivosBlq
Inicializacao do Objeto

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method New() Class ADMVMotivosBlq

    Local cPerc     := U_ADMVXFUN( 6 , "ZC6_BLSVMI" )

    ::LcVencido := "LC VENC"
    ::LCDExc    := "LCD EXC"
    ::VencMaior := "VENC >= "+ cValToChar( cPerc ) +"%"
    ::Juros     := "JUROS"
    ::SemLC     := "SEM LC "
    ::ChequeDev := "CD"
    ::Susp      := "SUSP"
    ::CondPgto  := "COND PGTO"
    ::CliCredBlq:= "CLIE CRED BLQ"
    ::LcVencEnt	:= "LC VC ENT"
    ::VencSemFt	:= "VC SEM FAT"

Return Self
//-----------------------------------------------
/*/{Protheus.doc} ADP4BlqC
ADMV Processamento 4 - Atualiza o cadastro do cliente
para Bloqueado por Credito

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4BlqC( cCodCli , cLoja , cUser ,  cMotivo , cSttBLQ )

    Local cMotACY   := ""
    Local lRet      := .F.
    Local cGrpMatriz:= GetAdvFVal( "SA1" , "A1_GRPVEN" , FWxFilial("SA1") + cCodCli + cLoja  , 1 )

    Default cUser   := __cUserId
    Default cMotivo := "Cliente Bloqueado por Credito"
    Default cSttBLQ	:= "B"

    dbSelectArea("ACY")
    ACY->( dbSetOrder(1) )
    If ACY->( dbSeek( FWxFilial("ACY") + cGrpMatriz  ) )
    	
    	If ACY->ACY_XBLQCR <> "S"
	        
	        cMotACY := IIF( !Empty( cMotACY ) , CRLF + CRLF , "" )
	        cMotACY += "Data: "+ DtoC( MsDate() ) + " Hora: " + Time() + " - " +  cUser  + CRLF
	        cMotACY += " > "+ cMotivo
	
	        If ACY->( RecLock( "ACY" , .F. ) )
				
	            ACY->ACY_XBLQCR  := cSttBLQ
	            ACY->ACY_XOBSBL  := cMotACY
	
	            lRet := .T.
	
	            ACY->( MsUnLock() )
	        EndIf
    	
    	EndIf

    EndIf

Return lRet


//-----------------------------------------------
/*/{Protheus.doc} ADP4JbCh
Caso configure para a atualizado dos pedidos de
venda seja realizado via Job, Fun��o
para preparar o ambiente para executgar a fun��o
que Verifica os Pedidos de Vendas em Aberto do 
Cliente, e Bloqueia

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4JbCh( _cEmp , _cFil , cCliente , cLoja , cMotivo , lAprvMan )
	
	Default _cEmp	:= "01"
	Default _cFil	:= "01010001" 
	
	Default lAprvMan	:= .T.

	RpcClearEnv()
	RpcSetType(3)
	
	If RpcSetEnv( _cEmp , _cFil )
	
		U_ADP4PvAb( cCliente , cLoja , cMotivo , lAprvMan )
	
	EndIf
	
	RpcClearEnv()
	
Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4PvAb
ADMV Processamento 4 - Verifica os Pedidos de
Vendas em Aberto do Cliente, e Bloqueia pela
Analise de Credito do Cliente

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4PvAb( cCodCli , cLoja , cMotivo , lAprvMan )

    Local lret      := .T.
    Local cGrpMatriz:= GetAdvFVal( "SA1" , "A1_GRPVEN" , FWxFilial("SA1") + cCodCli + cLoja , 1 )
    Local cSql      := ""
    Local clAlias   := ""
    Local aAreaSC5  := {}
    Local aBlqCred	:= {}
    Local nC		:= 0
    
    Default lAprvMan:= .T.

    cSql      := FQryPvAber( cGrpMatriz , lAprvMan )
    clAlias   := GetNextAlias()
		
    dbSelectArea("SC5")
    aAreaSC5 := SC5->(GetArea())

    DbUseArea( .T. , "TOPCONN" , TcGenQry( , , cSql ) , clAlias , .F. , .T. )

    dbSelectArea(clAlias)

    (clAlias)->( dbGoTop() )

    If (clAlias)->(! Eof() )
    	
    	aadd( aBlqCred , {"1" , cMotivo })
    	
        While (clAlias)->(! Eof() )

            SC5->( dbGoTo( (clAlias)->RECNOSC5 ) )
			
			u_ADP12Grv( SC5->C5_NUM , "5" /*TpMovimentoBloq*/ , aBlqCred , "C" )            

            (clAlias)->(dbSkip())
        EndDo

    EndIf

    (clAlias)->( dbCloseArea() )

    RestArea(aAreaSC5)

Return lret

//-----------------------------------------------
/*/{Protheus.doc} FQryPvAber
ADMV Processamento 4 - Query para identificar
os pedido de Venda em aberto do cliente

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FQryPvAber( cGrpMatriz , lAprvMan )
    
    Local cSql := ""

    cSql := " SELECT DISTINCT "
    cSql += "  C5_FILIAL "
    cSql += " ,C5_NUM "
    cSql += " ,SC5.R_E_C_N_O_ RECNOSC5 "
    
    cSql += " FROM " + RetSqlName( "SC5" )  + " SC5 "
    
    cSql += " INNER JOIN " + RetSqlName( "SC6" )  + " SC6 "
    cSql += " ON C6_FILIAL = C5_FILIAL "
    cSql += " AND C6_NUM = C5_NUM "
    
    cSql += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSql += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSql += " AND A1_COD = C5_CLIENTE "
    cSql += " AND A1_LOJA = C5_LOJACLI "
     
    cSql += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSql += " AND ACY_GRPVEN = A1_GRPVEN "
    
    
    cSql += " WHERE SC5.C5_FILIAL >= ' ' " // Todas as empresas/Filiais
    If lAprvMan
    	cSql += " AND SC5.C5_XBLQCRE in (' ','A','L','S')"
    Else
    	cSql += " AND SC5.C5_XBLQCRE in ( ' ','L','S')"
    EndIf
    //cSql += " AND C5_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cSql += " AND SC5.C5_XSITUAC <> 'S' "
    cSql += " AND SC6.C6_NOTA = ' '"
    cSql += " AND ACY_GRPVEN = '" + cGrpMatriz +"' "
    
    cSql += " AND SC5.D_E_L_E_T_= ' ' "
    cSql += " AND SC6.D_E_L_E_T_= ' ' "
    cSql += " AND SA1.D_E_L_E_T_= ' ' "
    cSql += " AND ACY.D_E_L_E_T_= ' ' "

    cSql += " ORDER BY C5_FILIAL , C5_NUM "

Return cSql

//-----------------------------------------------
/*/{Protheus.doc} ADP4ACrd
ADMV Processamento 4 - Analise de Credito

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4ACrd( cTipo , cCliente , cLoja , cFilPed , cPedido , cObsRotina , nVlrLCD )

	Local cMotBloq	:= ""
	Local cStatsC5	:= "P"	// Blq.Cred.Ped Venda
	Local cTpBloq	:= "1"
	Local aBlqCred	:= {}
	Local aAreaSA1	:= {}
	Local aAreaSC5	:= {}
	Local lOk		:= .T.
	
	Default cTipo		:= "All"
	Default cCliente	:= ""
	Default cLoja		:= ""
	Default cFilPed		:= FWxFilial("SC5")
	Default cPedido		:= ""
	Default cObsRotina	:= ""
	Default nVlrLCD		:= 0
	
	dbSelectArea("SA1")
	dbSelectArea("SC5")
	
	aAreaSA1 := SA1->( GetArea() )
	aAreaSC5 := SC5->( GetArea() )
	
	SA1->( dbSetOrder( 1 ))
	SC5->( dbSetOrder( 1 ))
	
	If Empty( cCliente ) .Or. Empty(cLoja)
		lOk := .F.
	Else
		If !( SA1->( dbSeek( FWxFilial("SA1") + Padr( cCliente, TamSx3("A1_COD")[1]) + Padr( cLoja , TamSx3("A1_LOJA")[1])   ) ) )
			lOk := .F.
		EndIf
	EndIf
	
	If !Empty( cPedido )
		If !( SC5->( dbSeek( Padr( cFilPed , TamSx3("C5_FILIAL")[1]) + Padr( cPedido , TamSx3("C5_NUM")[1] )  ) ) )
			lOk := .F.
		EndIf
	EndIf


	If lOk

		do case
	
			// -------------------------------------------------------------------------------
			// Avalia��o TIPO 1 - Verifica situa��o do cliente se est� bloqueado por credito
			// -------------------------------------------------------------------------------
			case cTipo == "1"
				
				If ADP4CliB( cCliente , cLoja , @cMotBloq )
					
					cStatsC5	:= "C"	//Blq.Cred Cliente
					cTpBloq		:= "5"
					aadd( aBlqCred , { "1" , cMotBloq })
					
				EndIf

		    //---------------------------------------------------------------------
		    // Avalia��o TIPO 2 - Analisa Condi��o Pagamento do Pedido de Venda
		    //---------------------------------------------------------------------
			case cTipo == "2"
			
				If ADP4Cdpg( SC5->C5_CONDPAG , @cMotBloq  )

					aadd( aBlqCred , { "1" , cMotBloq })

					If SC5->C5_XBLQCRE == "C"
						cStatsC5 := ""
					EndIf

				EndIf
	
		    //------------------------------------------------
		    // Avalia��o TIPO 3 - Analise Pedido Venda 
		    //------------------------------------------------
			case cTipo == "3"
			
				ADP4PedVen( cCliente , cLoja , cFilPed , cPedido , @aBlqCred , @nVlrLCD )
				
				If Len( aBlqCred ) > 0 .And. SC5->C5_XBLQCRE == "C"
					cStatsC5 := ""
				EndIf
			
		    //------------------------------------------------
		    // Avalia��o TIPO 4 - Analises Financeiras 
		    //------------------------------------------------			
			case cTipo == "4"
				
				ADP4AnlsFin( cCliente , cLoja , cObsRotina )

		    //------------------------------------------------
		    // Avalia��o TIPO 5 - Atualiza��o LCD
		    //------------------------------------------------
			case cTipo == "5"

				ADP4CalcLCD( cCliente , cLoja )

		    //------------------------------------------------
		    // Avalia��o TIPO CLI - Analise do Cliente
		    //------------------------------------------------
			case cTipo == "CLI"
			
				U_ADP4ACrd( "5"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )
				U_ADP4ACrd( "4"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )
			
			//--------------------------------------------------------------
			// Analise do Pedido - Verifica Credito do Cliente e Atualiza o PV, e verifica Credito para o Pedido
			//--------------------------------------------------------------			
			case cTipo == "APV"

			    //-------------------------------
			    // Verifica situa��o do cliente
			    //-------------------------------
			    U_ADP4ACrd( "1"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )

			    //----------------------------------------------------
			    //Analisa Condi��o Pagamento do Pedido de Venda
			    //----------------------------------------------------
			    U_ADP4ACrd( "2"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )
			    
			    //--------------------------------
			    // Analise do Pedido de Venda 
			    //--------------------------------
			    U_ADP4ACrd( "3"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )
			    
			    //--------------------------------
			    // Analise Financeira do Pedido
			    //--------------------------------
			    U_ADP4ACrd( "4"  , cCliente , cLoja , cFilPed , cPedido , cObsRotina , @nVlrLCD )
			   
		endcase
		
		If Len( aBlqCred ) > 0 .And. !Empty( cPedido )
			u_ADP12Grv( cPedido , cTpBloq /*TpMovimentoBloq*/ , aBlqCred , cStatsC5 )
		Endif    
	
	EndIf
	
    RestArea( aAreaSA1 )
    RestArea( aAreaSC5 )
    
Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4CliB
ADMV Processamento 4 - Verifica situa��o do
cliente, se est� bloqueado por credito

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4CliB( cCliente , cLoja , cMotBloq )

    Local aAreaSA1  := SA1->(GetArea())
    Local aAreaACY	:= {}
    Local lBloqueia := .F.
    Local cGrpMatriz:= ""

    Default cMotBloq    := ""

    dbSelectArea("SA1")
    SA1->(dbSetOrder(1)) //
    If SA1->( dbSeek( FWxFilial("SA1") + cCliente + cLoja ) )
    
    	cGrpMatriz	:= SA1->A1_GRPVEN
    	If !Empty( cGrpMatriz )
    		
    		dbSelectArea( "ACY" )
    		aAreaACY := ACY->( GetArea() )
    		
    		If ACY->( dbSeek( FWxFilial("ACY") + cGrpMatriz  ) )
    		
    			If Alltrim(ACY->ACY_XBLQCR) $ "B|S"

		            lBloqueia   := .T.
		            cMotBloq	:= ""
		            
		            If ACY->ACY_XBLQCR == "S"
		            	cMotBloq	:= oMotivos:Susp
		            Else
		            	cMotBloq	:= oMotivos:CliCredBlq
		            EndIf

		            cMotBloq    += ": "+ Alltrim( ACY->ACY_XOBSBL)

    			EndIf

    		Else

	            lBloqueia   := .T.
	            cMotBloq    := "N�o foi localizado o cadastro do c�digo grupo matriz ("+cGrpMatriz+"), verifique se o cadastro do cliente."

    		EndIf
    		
    		RestArea( aAreaACY )
    	
    	Else

            lBloqueia   := .T.
            cMotBloq    := "Cliente sem cadastro de cr�dito, verifique o campo grupo matriz no cadastro de cliente ou realize o cadastro de cr�dito para o cliente."
                		
    	EndIf

    EndIf

    RestArea(aAreaSA1)

Return lBloqueia

//-----------------------------------------------
/*/{Protheus.doc} ADP4Cdpg
ADMV Processamento 4 - Verifica a Condi��o
de Pagamento se for a vista ou antecipado
deve bloquear o Pedido

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4Cdpg( cCondPg , cMotBloq )

    Local lBloqueia    := .F.
    Local cCondBlq     := Alltrim( U_ADMVXFUN( 15 , "3")  )

    If Alltrim(cCondPg) $ cCondBlq

        lBloqueia   := .T.
        cMotBloq    := oMotivos:CondPgto

    EndIf

Return lBloqueia

//-----------------------------------------------
/*/{Protheus.doc} ADP4PedVen
Analise do valor do pedido e datas de entrega, 
para comparar com com o cadastro de credito 
do cliente

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4PedVen( cCliente , cLoja , cFilPed , cPedido , aBlqCred , nVlrLCD )

	Local cGrpMatriz:= GetAdvFVal( "SA1" , "A1_GRPVEN" , FWxFilial("SA1") + cCliente + cLoja  , 1 )  
	Local aAreaACY	:= ACY->( GetArea() )
	Local dDtEntreg	:= CtoD("//")
	Local cMotBloq	:= ""
	
	If !Empty( cGrpMatriz )

		dbSelectArea( "ACY" )
		aAreaACY := ACY->( GetArea() )
		
		If ACY->( dbSeek( FWxFilial("ACY") + cGrpMatriz  ) )
		
			nVlrLCD		+= SC5->C5_XVLRNF
			
			dDtEntreg := FMrDtEntrg( cFilPed , cPedido )
		
			//-------------------
			//SEM LC 
			//-------------------
			If ADP4SemLC( @cMotBloq )
				aadd( aBlqCred , { "1" , cMotBloq })
			EndIf

		    //------------------------------------------------
		    // Analise LCD 
		    //------------------------------------------------
			If ADP4PvVsLCD( nVlrLCD , @cMotBloq )
				aadd( aBlqCred , { "1" , cMotBloq })
			EndIf

			//------------------------------------------------
			//Data do limite Vencimento
			//------------------------------------------------
			If ADP4LCVCENT( dDtEntreg , @cMotBloq )
				aadd( aBlqCred , { "1" , cMotBloq })
			EndIf
			
		EndIf
		
		RestArea( aAreaACY)
			
	EndIf
	
Return

//-----------------------------------------------
/*/{Protheus.doc} FMrDtEntrg
Retorna a maior data de entrega do pedido de venda 

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function	FMrDtEntrg( cFilPed , cPedido )

	Local cChavePed := cFilPed + cPedido
	Local dDtEntreg	:= CTod("")
	
	dbSelectArea("SC6")
	SC6->(dbSetOrder(1)) 	//  C6_FILIAL + C6_NUM
	SC6->(dbGoTop())

	If SC6->( dbSeek(  cChavePed  ) )
		
		While SC6->(!EOF()) .AND. ( cChavePed == SC6->(C6_FILIAL+C6_NUM )  )

			//-----------------------------------------------------
			//Retorna a Maior data de Entrega do Pedido de Venda
			//-----------------------------------------------------
			If dDtEntreg < SC6->C6_ENTREG
				dDtEntreg := SC6->C6_ENTREG
			EndIf
			
			SC6->( dbSkip() )
		EndDo
		
	EndIf
	
Return dDtEntreg

//-----------------------------------------------
/*/{Protheus.doc} ADP4SemLC
Verifica se a Matriz do Cliente tem Limite Credito

@author DS2U (HFA)
@since 21/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Stati Function ADP4SemLC( cMotBloq )
	
	Local lRet	:= .F.
	
	cMotBloq := ""
	
	If ACY->ACY_XLC <= 0
		lRet := .T.
		cMotBloq := oMotivos:SemLC
	EndIf
	
Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4PvVsLCD
Verifica se o Limite Credito Disopnivel
da Matriz do Cliente � suficiente

@author DS2U (HFA)
@since 21/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4PvVsLCD( nVlrLCD , cMotBloq )
	Local lRet	:= .F.
	
	cMotBloq := ""
	
	If ACY->ACY_XLCD > 0
		
		If nVlrLCD > ACY->ACY_XLCD
	
			lRet	:= .T.
			cMotBloq:= oMotivos:LCDExc
	
		EndIf
		
	Else
		
		// Ate atingir o Limite de Credito, n�o bloqueia
		If nVlrLCD > ACY->ACY_XLC
	
			lRet	:= .T.
			cMotBloq:= oMotivos:LCDExc
	
		EndIf
		
	EndIf
	
Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4LCVCENT
Verifica o vencimento de Cr�dito do Cliente
e a Maior data de Entrega do Pedido de Venda

@author DS2U (HFA)
@since 22/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4LCVCENT( dDtEntreg , cMotBloq )
	
	Local lRet	:= .F.
	
	cMotBloq := ""
	
	If dDtEntreg  >= ACY->ACY_XVNCLC
		lRet	:= .T.
		cMotBloq:= oMotivos:LcVencEnt
	EndIf
	
Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4AnlsFin
Analises das Movimenta��es Financeiras do Cliente

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4AnlsFin( cCliente , cLoja , cObsRotina )
    
   	Local aRet          := {}
   	Local cGrpMatriz	:= GetAdvFVal( "SA1" , "A1_GRPVEN" , FWxFilial("SA1") + cCliente + cLoja  , 1 )
    Local nVlrMinVen    := U_ADMVXFUN( 6 , "ZC6_VLMINV" ) // Valor Minimo de Vencidos
    Local nVlrMaxJur    := U_ADMVXFUN( 6 , "ZC6_VMAXJU" ) // Valor Maximo de Juros
    Local nPerSupVmn    := U_ADMVXFUN( 6 , "ZC6_BLSVMI" ) // %Bloqueio Sup Valor Minim
    Local nPrdFat       := U_ADMVXFUN( 6 , "ZC6_PERIOF" ) // Periodo de Faturamento
    Local nPercJrs      := U_ADMVXFUN( 6 , "ZC6_PJUROS" ) // Percentual maximo de Juros
    Local nVLJrSFPF		:= U_ADMVXFUN( 6 , "ZC6_VLJRCO" ) // Valor de Juros Sem Faturamento no Periodo de Faturamento
    
    Local dPeriodFat	:= DaySub( MsDate() , nPrdFat )
    Local nVlrTVenc		:= FSldTVnc( cGrpMatriz )	// Valor Total de Saldos Vencidos, independento do periodo de Fatuamento 
    Local nVlrPerFat	:= FVTtCompra( cGrpMatriz , dPeriodFat )	// Valor Total de Titulos , no Periodo de Faturamento
    Local nVrJurPerF	:= FVJurosPF( cGrpMatriz , dPeriodFat )	// Valor Total de Juros , no Periodo de Faturamento	
    Local lRet			:= .F.
    Local cMotBloq		:= ""
    Local aBlqCred		:= {}
    Local nC			:= 0
    
    Default cObsRotina	:= "An�lise de Cr�dito: "
		
    //------------------------------------------------
    // Avalia��o Titulos Vencidos VC SEM FAT 
    //------------------------------------------------			
	If ADP4TtSFt( nVlrTVenc , nVlrPerFat , nVlrMinVen , @cMotBloq )
		aadd( aBlqCred , { "1" , cMotBloq })
	EndIf
					
    //------------------------------------------------
    // Avalia��o Titulos Vencidos VENC > 7%
    //------------------------------------------------			
	If ADP4TtCFt( nVlrTVenc , nVlrPerFat , nPerSupVmn , @cMotBloq )
		aadd( aBlqCred , { "1" , cMotBloq })
	EndIf

    //------------------------------------------------
    // Avalia��o Titulos Vencidos JUROS
    //------------------------------------------------			
	ADP4TtJur( nVrJurPerF , nVlrPerFat , nVlrMaxJur , nPercJrs , nVLJrSFPF , @aBlqCred )

	dbSelectArea("ACY")
	ACY->( dbSetOrder(1) )
	ACY->( dbSeek( FWxFilial("ACY") + cGrpMatriz ) )    

    //------------------------------------------------
    // Atualiza o cadastro do Cliente
    //------------------------------------------------
	If Len( aBlqCred ) > 0

		cMotBloq := ""
		For nC := 1 To Len( aBlqCred )
		
			//If ! ( aBlqCred[ nC ][ 2 ] $ ACY->ACY_XOBSBL )
			
				cMotBloq += IIF( !Empty( cMotBloq ) , CRLF , "") + aBlqCred[ nC ][ 2 ]
			
			//EndIf
			
		Next nC
		
		If !Empty( cMotBloq )
			
			cMotBloq := cObsRotina + CRLF + cMotBloq
			
			U_ADP4BlqC( cCliente , cLoja , cUserName ,  cMotBloq , "B" )

		EndIf
				
	Else
		
		If ACY->ACY_XBLQCR == "B"

			cMotBloq := "Retirado o Bloqueio do Cliente ap�s an�lise de Cr�dito do Cliente"
			U_ADP4BlqC( cCliente , cLoja , cUserName ,  cMotBloq , "L" )
					
		EndIf
		
	EndIf
	
Return

//-----------------------------------------------
/*/{Protheus.doc} ADP4TtSFt
Verifica se tem t�tulos Vencidos sem Faturamento
no Per�odo de Faturamento

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4TtSFt( nVlrTVenc , nVlrPerFat , nVlrMinVen , cMotBloq)
	
	Local lRet	:= .F.
	cMotBloq	:= ""
	
	// Tem Titulos Vencidos e N�o tem faturamento no Ultimo Periodo de Faturamento
	If nVlrTVenc > 0 .And. nVlrPerFat == 0

		// Valor dos Titulos Vencidos Maior ou Igual o Minimo de Vencimento Aceitavel
		If nVlrTVenc >= nVlrMinVen
			
			cMotBloq := oMotivos:VencSemFt
			lRet	:= .T.
			
		EndIf

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4TtCFt
Verifica se tem t�tulos Vencidos � maior
que 7% Titulos no Per�odo de Faturamento

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4TtCFt( nVlrTVenc , nVlrPerFat , nPerSupVmn , cMotBloq )

	Local lRet			:= .F.
	Local nPercAtras	:= 0
	
	cMotBloq := ""
	
	// Se tem Vlr no Periodo de Faturamento
	If nVlrPerFat > 0

        nPercAtras := ( nVlrTVenc / nVlrPerFat ) * 100

        If nPercAtras > nPerSupVmn

            cMotBloq:= oMotivos:VencMaior
            lRet	:= .T. 

        EndIf
	
	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4TtJur
Verifica os Titulos de Juros dos clientes, e 
valida a regra de do ADMV

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4TtJur( nVrJurPerF , nVlrPerFat , nVlrMaxJur , nPercJrs , nVLJrSFPF , aBlqCred )
	
	Local lRet		:= .F.
	Local nPercTit	:= 0
	Local cMotBloq	:= ""

	//---------------------------------------------------------------------------------------------------------
	//Valor total de juros Vencidos: se o saldo total de JR for igual ou maior a % Juros (atualmente 10%) 
	//do total de compras no per�odo Periodo Fat. (atualmente, nos �ltimos 12 meses)
	//---------------------------------------------------------------------------------------------------------
	nPercTit	:= ( nVrJurPerF / nVlrPerFat ) * 100
	If nPercTit >= nPercJrs
		lRet := .T.
		cMotBloq := oMotivos:Juros + " - Saldo de Juros >= " + cValToChar( nPercJrs ) +"%"
		aadd( aBlqCred , { "1" , cMotBloq }) 
	EndIf
	
	//---------------------------------------------------------------------------------------------------------
	//Se os t�tulos em atraso refente a Juros, inclu�do manualmente ou calculado pelo sistema, 
	//seja superior a V.Max.Juros ( Atualmente � R$ 3.000,00)
	//---------------------------------------------------------------------------------------------------------
	If nVrJurPerF >= nVlrMaxJur
		lRet := .T.
		cMotBloq := oMotivos:Juros+ " - Valor de Juros >= V.Max.Juros R$ " + Alltrim( Transform( nVlrMaxJur , PesqPict("ZC6","ZC6_VMAXJU")))
		aadd( aBlqCred , { "1" , cMotBloq })	
	EndIf

	//------------------------------------------------------------------------------------------
	//Se o total de juros for igual ou maior a Vlr. Max Juros sem compras R$ 700,00 .
	//Para clientes sem compras no per�odo Periodo Fat. (atualmente, nos �ltimos 12 meses)
	//------------------------------------------------------------------------------------------
	If nVlrPerFat == 0
	
		If nVrJurPerF >= nVLJrSFPF
			lRet := .T.
			cMotBloq := oMotivos:Juros+ " - Valor de Juros >= ao Vlr. Max Juros sem compras R$ " + Alltrim( Transform( nVLJrSFPF , PesqPict("ZC6","ZC6_VLJRCO")))
			aadd( aBlqCred , { "1" , cMotBloq })			
		EndIf
	
	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADP4CalcLCD
Calculo do LCD ( Limite de credito Disponivel)
do Cliente

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP4CalcLCD( cCliente , cLoja )

	Local cGrpMatriz:= GetAdvFVal( "SA1" , "A1_GRPVEN" , FWxFilial("SA1") + cCliente + cLoja  , 1 )
	Local aAreaACY	:= {}
	Local nVlrLC	:= 0
	Local nVlrPV	:= 0
	Local nVlrTitAb	:= 0
	Local nVlrLCD	:= 0
	
	dbSelectArea( "ACY" )
	aAreaACY := ACY->( GetArea() )
	
	If ACY->( dbSeek( FWxFilial("ACY") + cGrpMatriz  ) )

		/*
		( + ) Limite de Cr�dito (LC) 
		( - ) Pedidos Vendas aprovados no financeiro e n�o faturados ainda (COM IMPOSTOS = IPI + ST) Somente os Pedidos que foram aprovados pela analise de Credito.
		( - ) total de t�tulos em aberto do t�tulo �Saldo�:  soma de saldos de t�tulos: (DP + CD + NP + DC + ND) � (RA + DV + VP + AV + NC)
		( = ) LCD (Limite de cr�dito dispon�vel)
		*/
		
		nVlrLC		:= ACY->ACY_XLC
		nVlrPV		:= FAllPVOpen( cGrpMatriz )
		nVlrTitAb	:= FAllTTOpen( cGrpMatriz )
		nVlrLCD		:= nVlrLC - nVlrPV - nVlrTitAb
		
		If ACY->( RecLock( "ACY" , .F. ) )
			
			ACY->ACY_XPVABE	:= nVlrPV
			ACY->ACY_XTITAB	:= nVlrTitAb 
			ACY->ACY_XLCD	:= nVlrLCD
			ACY->ACY_XDATUA	:= MsDate()
			ACY->ACY_XHRATU	:= Time()
			ACY->( MsUnLock() )
			
		EndIf
		
	EndIf

	RestArea( aAreaACY )

Return



//-----------------------------------------------
/*/{Protheus.doc} FGetEmp
ADMV Processamento 4 - Retorna todas as empresas
cadastrados no sigamat

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FGetEmp()

    Local aRet := {}

    SM0->(dbGoTop())
    While SM0->( !Eof() )

        If aScan( aRet , Alltrim(SM0->M0_CODIGO) ) == 0

            aadd( aRet , Alltrim(SM0->M0_CODIGO) )

        EndIf

        SM0->(dbSkip())

    EndDo

Return aRet

//-----------------------------------------------
/*/{Protheus.doc} FSldTVnc
ADMV Processamento 4 - Retorna o Saldo de titulo
vencidos do clientes, de todas as empresas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FSldTVnc( cGrpMatriz )

	Local nSaldo    := 0
    Local clAlias   := ""
    Local cSql      := ""
    Local cNatCred  := Alltrim( U_ADMVXFUN( 15 , "4")  )

    cSql := " SELECT "
    cSql += " SUM(E1_SALDO) VALOR "
    cSql += " FROM " + RetSqlName( "SE1" ) + " SE1 "

    cSql += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSql += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSql += " AND A1_COD = E1_CLIENTE "
    cSql += " AND A1_LOJA = E1_LOJA "
        
    cSql += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSql += " AND ACY_GRPVEN = A1_GRPVEN "
    
    cSql += " WHERE SE1.E1_FILIAL >= ' ' "
    cSql += " AND SE1.E1_NATUREZ IN " + FormatIn( cNatCred , ";")
    cSql += " AND SE1.E1_VENCREA <  '"+ DtoS( MsDate() ) +"'"
    cSql += " AND SE1.E1_SALDO > 0 "
    cSql += " AND ACY_GRPVEN = '" + cGrpMatriz +"' "
    //cSql += " AND SA1.A1_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cSql += " AND SA1.D_E_L_E_T_= ' ' "
    cSql += " AND ACY.D_E_L_E_T_= ' ' "
    cSql += " AND SE1.D_E_L_E_T_ = ' ' "

    clAlias := GetNextAlias()

    DbUseArea( .T. , "TOPCONN" , TcGenQry( , , cSql ) , clAlias , .F. , .T. )

    If (clAlias)->( !Eof() )
        nSaldo := (clAlias)->VALOR
    EndIf
    (clAlias)->( dbCloseArea() )

Return nSaldo

//-----------------------------------------------
/*/{Protheus.doc} FVTtCompra
ADMV Processamento 4 - Retorna o valor de titulo
(compras)do clientes, de todas as empresas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FVTtCompra( cGrpMatriz , dPeriodFat )

	Local nValor	:= 0
    Local clAlias   := ""
    Local cSql		:= ""
    Local cNatCred  := Alltrim( U_ADMVXFUN( 15 , "4")  )

    //-------------------------------------
	// usando os Notas
	//-------------------------------------
	cSql := " SELECT "
	cSql += " SUM(D2_VALBRUT) VLRVENDA "
	cSql += " ,SUM(D1_TOTAL) VLRDEV  "

	cSql += " FROM "+ RetSqlName( "SD2" ) +" SD2 "

    cSql += " INNER JOIN " + RetSqlName( "SF4" )  + " SF4 "
    cSql += " ON F4_FILIAL = '"+ FWxFilial("SF4") +"' "
    cSql += " AND F4_CODIGO = D2_TES "
    
    cSql += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSql += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSql += " AND A1_COD = D2_CLIENTE "
    cSql += " AND A1_LOJA = D2_LOJA "
    
    cSql += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSql += " AND ACY_GRPVEN = A1_GRPVEN "
    
	cSql += " INNER JOIN "+ RetSqlName( "SC6" ) +" SC6 "
	cSql += " ON C6_FILIAL = D2_FILIAL "
	cSql += " AND C6_NUM = D2_PEDIDO "
	cSql += " AND C6_ITEM = D2_ITEMPV "

	cSql += " INNER JOIN "+ RetSqlName( "SC5" ) +" SC5 "
	cSql += " ON C5_FILIAL = C6_FILIAL "
	cSql += " AND C5_NUM = C6_NUM "

	cSql += " LEFT JOIN "+ RetSqlName( "SD1" ) +" SD1 "
	cSql += " ON D1_FILIAL = SD2.D2_FILIAL "
	cSql += " AND D1_NFORI = SD2.D2_DOC "
	cSql += " AND D1_SERIORI = SD2.D2_SERIE "
	cSql += " AND D1_FORNECE = SD2.D2_CLIENTE "
	cSql += " AND D1_LOJA = SD2.D2_LOJA "
	cSql += " AND D1_ITEMORI = SD2.D2_ITEM "
	cSql += " AND D1_TIPO = 'D' "
	cSql += " AND SD1.D_E_L_E_T_ = ' ' "

	cSql += " WHERE D2_FILIAL >= ' ' "
	cSql += " AND SD2.D2_EMISSAO >= '"+ DtoS( dPeriodFat )+"' "
	cSql += " AND SD2.D2_TIPO = 'N' "
    cSql += " AND SF4.F4_DUPLIC = 'S' "
    //cSql += " AND SC5.C5_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cSql += " AND ACY.ACY_GRPVEN = '" + cGrpMatriz +"' "

    cSql += " AND EXISTS ( SELECT E1_VALOR FROM " + RetSqlName( "SE1" )  + " SE1 "
    cSql += "				WHERE E1_FILIAL = D2_FILIAL "
    cSql += "				AND E1_PREFIXO = D2_SERIE "
    cSql += "				AND E1_NUM =  D2_DOC "
    cSql += "				AND E1_TIPO = 'NF' "
    cSql += "				AND E1_CLIENTE = D2_CLIENTE "
    cSql += "				AND E1_LOJA = D2_LOJA "
    cSql += "				AND SE1.E1_NATUREZ IN " + FormatIn( cNatCred , ";")
    cSql += "				AND SE1.D_E_L_E_T_ = ' ' )"

	cSql += " AND SD2.D_E_L_E_T_ = ' ' "
	cSql += " AND SF4.D_E_L_E_T_ = ' ' "
    cSql += " AND SA1.D_E_L_E_T_ = ' ' "
    cSql += " AND ACY.D_E_L_E_T_ = ' ' "	
	cSql += " AND SC6.D_E_L_E_T_ = ' ' "
	cSql += " AND SC5.D_E_L_E_T_ = ' ' "

    clAlias := GetNextAlias()

    DbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),clAlias,.F.,.T.)
    TcSetField(clAlias,'VLRVENDA'	,'N',TamSx3('E1_VALOR')[1],TamSx3('E1_VALOR')[2])
    TcSetField(clAlias,'VLRDEV'		,'N',TamSx3('E1_VALOR')[1],TamSx3('E1_VALOR')[2])

    If (clAlias)->( !Eof() )
        nValor := (clAlias)->VLRVENDA - (clAlias)->VLRDEV
    EndIf

    (clAlias)->( dbCloseArea() )
    
Return nValor

//-----------------------------------------------
/*/{Protheus.doc} FVJurosPF
ADMV Processamento 4 - Retorna o valor de titulo
de juros dos clientes, de todas as empresas

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FVJurosPF( cGrpMatriz , dPeriodFat )

    Local nValor    := 0
    Local clAlias   := ""
    Local cSql      := ""
    Local cNatCred  := Alltrim( U_ADMVXFUN( 15 , "4") )

    cSql := " SELECT "
    cSql += " SUM(E1_SALDO) VALOR "
    cSql += " FROM " + RetSqlName( "SE1" ) + " SE1 "
    
    cSql += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSql += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSql += " AND A1_COD = E1_CLIENTE "
    cSql += " AND A1_LOJA = E1_LOJA "
        
    cSql += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSql += " AND ACY_GRPVEN = A1_GRPVEN "
    
    cSql += " WHERE SE1.E1_FILIAL >= ' ' "
    cSql += " AND SE1.E1_NATUREZ IN " + FormatIn( cNatCred , ";")
    cSql += " AND SE1.E1_TIPO = 'JR' "
    cSql += " AND SE1.E1_VENCREA <  '"+ DtoS( MsDate() ) +"'"
    cSql += " AND SE1.E1_EMISSAO >= '"+ DtoS( dPeriodFat )+"' "
    cSql += " AND ACY_GRPVEN = '" + cGrpMatriz +"' "
    //cSql += " AND SA1.A1_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    
    cSql += " AND SA1.D_E_L_E_T_= ' ' "
    cSql += " AND ACY.D_E_L_E_T_= ' ' "
    cSql += " AND SE1.D_E_L_E_T_ = ' ' "

    clAlias := GetNextAlias()

    DbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),clAlias,.F.,.T.)

    If (clAlias)->( !Eof() )
        nValor := (clAlias)->VALOR
    EndIf

    (clAlias)->( dbCloseArea() )

Return nValor

//-----------------------------------------------
/*/{Protheus.doc} FAllPVOpen
Retorna o Valor de todos os Pedidos de Vendas
em Aberto

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FAllPVOpen( cGrpMatriz )

	Local nValor	:= 0
    Local clAlias   := ""
    Local cSql      := ""

    cSql += " SELECT " 
    cSql += " SUM(C5_XVLRNF) VALOR "
    cSql += " FROM " + RetSqlName( "SC5" )  + " SC5 "

    cSql += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSql += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSql += " AND A1_COD = C5_CLIENTE "
    cSql += " AND A1_LOJA = C5_LOJACLI "
     
    cSql += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSql += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSql += " AND ACY_GRPVEN = A1_GRPVEN "
    
    cSql += " WHERE SC5.C5_FILIAL >= ' ' " // Todas as empresas/Filiais
    cSql += " AND SC5.C5_XBLQCRE in ('A', 'L', 'S' , ' ' )" // Conforme solicitado no documento, por�m o ideal seria n�o filtrar o status do bloqueio de credito
    cSql += " AND SC5.C5_NOTA = ' '"
    //cSql += " AND SC5.C5_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cSql += " AND ACY_GRPVEN = '" + cGrpMatriz +"' "
    cSql += " AND SC5.D_E_L_E_T_= ' ' "
    cSql += " AND SA1.D_E_L_E_T_= ' ' "
    cSql += " AND ACY.D_E_L_E_T_= ' ' "
    
    clAlias := GetNextAlias()

    DbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),clAlias,.F.,.T.)

    If (clAlias)->( !Eof() )
        nValor := (clAlias)->VALOR
    EndIf

    (clAlias)->( dbCloseArea() )

Return nValor

//-----------------------------------------------
/*/{Protheus.doc} FAllTTOpen
Retorna o Valor de todos os titulos
em Aberto

@author DS2U (HFA)
@since 23/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FAllTTOpen( cGrpMatriz )

	Local nValor	:= 0
    Local clAlias   := ""
    Local cSql		:= ""
    Local cSqlAux	:= ""
    Local cWhere	:= ""
    Local cNatCred  := Alltrim( U_ADMVXFUN( 15 , "4")  )
    Local cNatDeb	:= Alltrim( U_ADMVXFUN( 15 , "5")  )

    cSqlAux += " SELECT " 
    cSqlAux += " SUM(E1_SALDO) VALOR "
    cSqlAux += " FROM " + RetSqlName( "SE1" )  + " SE1 "
    cSqlAux += " INNER JOIN " + RetSqlName( "SA1" )  + " SA1 "
    cSqlAux += " ON A1_FILIAL = '"+ FWxFilial("SA1") +"' "
    cSqlAux += " AND A1_COD = E1_CLIENTE "
    cSqlAux += " AND A1_LOJA = E1_LOJA "
    cSqlAux += " INNER JOIN " + RetSqlName( "ACY" )  + " ACY "
    cSqlAux += " ON ACY_FILIAL = '"+ FWxFilial("ACY") +"' "
    cSqlAux += " AND ACY_GRPVEN = A1_GRPVEN "
    cSqlAux += " WHERE SE1.E1_FILIAL >= ' ' "

    cWhere += " AND SE1.E1_SALDO > 0 "
    cWhere += " AND ACY_GRPVEN = '" + cGrpMatriz +"' "
    //cWhere += " AND SA1.A1_XCANAL IN " + FormatIn( cCanaisADMV , ";")
    cWhere += " AND SA1.D_E_L_E_T_= ' ' "
    cWhere += " AND ACY.D_E_L_E_T_= ' ' "
    cWhere += " AND SE1.D_E_L_E_T_ = ' ' "


    cSql := " SELECT "
    cSql += " ( " + cSqlAux  
    cSql += " AND SE1.E1_NATUREZ IN " + FormatIn( cNatCred , ";")
    cSql += " " + cWhere
    cSql += " ) SLDCRED " 
    cSql += " ,  
    cSql += " ( " + cSqlAux  
    cSql += " AND SE1.E1_NATUREZ IN " + FormatIn( cNatDeb , ";")
    cSql += " " + cWhere
    cSql += " ) SLDDEB "
    
    clAlias := GetNextAlias()

    DbUseArea(.T.,"TOPCONN",TcGenQry(,,cSql),clAlias,.F.,.T.)
    TcSetField(clAlias,'SLDCRED','N',TamSx3('E1_SALDO')[1],TamSx3('E1_SALDO')[2])
    TcSetField(clAlias,'SLDDEB'	,'N',TamSx3('E1_SALDO')[1],TamSx3('E1_SALDO')[2])

    If (clAlias)->( !Eof() )
        nValor := (clAlias)->SLDCRED - (clAlias)->SLDDEB
    EndIf

    (clAlias)->( dbCloseArea() )

Return nValor

//-----------------------------------------------
/*/{Protheus.doc} ADP4MlPV
Verifica se Deve enviar e-mail notificando
do Bloqueio do Pedido De Venda por Credito
Pedido em Stand By 

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP4MlPV( cMotivo )
    
    Local nStandBy		:= U_ADMVXFUN( 6 , "ZC6_STANBY" )
    Local dDtEntrega	:= U_ADMVXFUN( 18 , SC5->( C5_FILIAL + C5_NUM)  )
    Local nDiasEnt		:= dDtEntrega - MsDate()
    
    Default cMotivo		:= "Bloqueio pela Analise de Cr�dito."

    //Envia e-mail de Notifica��o Bloqueio de Credito do PV - Manual
    If nDiasEnt < nStandBy

        FwMsgRun(,{|| FMailCrdPv( SC5->C5_NUM , SC5->C5_CLIENTE , SC5->C5_LOJACLI , cMotivo  )  },"Aguarde...","Enviando e-mail de Bloqueio de Cr�dito...")

    EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} FMailCrdPv
Envia o e-mail de notifica��o do bloqueio
manual do Pedido de Venda, por credito

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FMailCrdPv( cPedido , cCliente , cLoja , cMsgBlq )

    Local cAssunto  := "Bloqueio Pedido de Venda - Analise de Credito"
    Local cMensagem := ""
    Local cEmailTo  := Alltrim( U_ADMVXFUN( 15 , "2") )
    Local cEmailCc  := ""
    Local cErro     := ""
    Local cHtmlLibBl:= GetMV("ES_HTMBLQP",,"\Workflow\PVBlqCredito.html") // ADMV - HTML Bloqueio Pedido de Venda - Analise de Credito
    Local oHtmlModel:= Nil

    If File(cHtmlLibBl)

        oHtmlModel := TWFHTML():New(cHtmlLibBl)
        oHtmlModel:ValByName( "PEDIDO"		, cPedido		                                                                            )
        oHtmlModel:ValByName( "CLIENTE"		, cCliente		                                                                            )
        oHtmlModel:ValByName( "NOME"		, Alltrim( GetAdvFVal( "SA1" , "A1_NOME" , FWxFilial("SA1") + cCliente + cLoja  , 1 ) )		)
        oHtmlModel:ValByName( "CNPJ"		, Alltrim( GetAdvFVal( "SA1" , "A1_CGC"  , FWxFilial("SA1") + cCliente + cLoja  , 1 ) )		)
        oHtmlModel:ValByName( "MOTIVO"		, StrTran(cMsgBlq, CRLF , "<br/>") 		                                                                            )
        cMensagem := oHtmlModel:HtmlCode()

    Else

        cMensagem := "Prezados,"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "Foi realizado a bloqueio manual do pedido de vendas abaixo:"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "<table border='1'> "
        cMensagem += "	<tr> "
        cMensagem += "		<th>Pedido</th> "
        cMensagem += "		<th>Cliente</th> "
        cMensagem += "		<th>Nome Cliente</th> "
        cMensagem += "		<th>CNPJ</th> "
        cMensagem += "		<th>Motivo</th> "
        cMensagem += "	</tr>"
        cMensagem += "	<tr>"
        cMensagem += "		<td>" + cPedido + "</td>"
        cMensagem += "		<td>" + cCliente + "</td>"
        cMensagem += "		<td>" + Alltrim( GetAdvFVal( "SA1" , "A1_NOME" , FWxFilial("SA1") + cCliente + cLoja  , 1 ) ) + "</td>"
        cMensagem += "		<td>" + Alltrim( GetAdvFVal( "SA1" , "A1_CGC"  , FWxFilial("SA1") + cCliente + cLoja  , 1 ) ) + "</td>"
        cMensagem += "		<td>" + cMsgBlq + "</td>"
        cMensagem += "	</tr> "
        cMensagem += " </table> "
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "ADMV Sestini"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"

    EndIf

    U_ADMVSNdMail(cAssunto, cMensagem, {}, cEmailTo, cEmailCc, @cErro , .F. , .T. )

Return