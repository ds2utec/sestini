#Include "Protheus.Ch"
#Include "TopConn.Ch"

#DEFINE nTamC5NUM TamSX3("C5_NUM")[1]
#DEFINE nTamC6ITEM TamSX3("C6_ITEM")[1]

//-------------------------------------------------------------------
/*/{Protheus.doc} ADMVP09
Realizar a c�pia de um Pedido Origem para o Pedido Destino

@author  Irineu Filho
@since   08/10/2018 - 09:45
/*/
//-------------------------------------------------------------------
User Function ADMVP09()

Local aArea := GetArea()

If FValChamada() //Valida a chamada da Tela
	FTelaTranf() //Chamada a fun��o de cria��o da tela.
EndIf

RestArea(aArea)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FValChamada
Fun��o para valida��o antes de abertura da tela.

@author  Irineu Filho
@since   08/10/2018 - 10:36
/*/
//-------------------------------------------------------------------
Static Function FValChamada()

Local lRetorno := .T.

Return lRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FTela
Fun��o para cria��o da tela de transferencia de aloca��o.

@author  Irineu Filho
@since   08/10/2018 - 10:36
/*/
//-------------------------------------------------------------------
Static Function FTelaTranf()

Local oDlgTransf	:= Nil
Local oPedOrig		:= Nil
Local oPedDest		:= Nil
Local oAdicionar	:= Nil
Local oRemover		:= Nil
Local aSizeAut		:= MsAdvSize(,.F.,400)
Local aColsOrigem	:= {}
Local aColsDestino	:= {}
Local cTitTela		:= "Tranfer�ncia de Aloca��o"
Local cBkpCadastro	:= cCadastro
Local cPedOri		:= SC5->C5_NUM
Local cPedDes		:= Space(nTamC5NUM)
Local nStyleOrigem	:= GD_UPDATE
Local nStyleDestino	:= 0
Local bProcOk		:= {|| FConfirma(@oDlgTransf,@cPedOri,@cPedDes,oPedOrig) }
Local bCancel		:= {|| oDlgTransf:End() }
Local bAdiciona		:= {|| FAdcItem() }
Local bRemove		:= {|| FRmvItem() }

Private oGetOrigem	:= Nil
Private oGetDestino	:= Nil
Private aBkpOrigem	:= {}
Private a2BkpOrigem	:= {}

cCadastro := "Tranfer�ncia de Aloca��o"

//--------------------------------------------------------------------------
// Chamada da fun��o para buscar as informa��es de configura��o do GetDados.
//--------------------------------------------------------------------------
aCfgTela 		:= FCfgTela()
aHeadOrigem 	:= AClone(aCfgTela[1])
aHeadDestino	:= AClone(aCfgTela[2])
aAltOrigem		:= AClone(aCfgTela[3])
aAltDestino		:= AClone(aCfgTela[4])
aColsOrigem		:= {}
aColsDestino	:= {}

DEFINE MSDIALOG oDlgTransf FROM 0,0 TO aSizeAut[6],aSizeAut[5] TITLE cTitTela OF oMainWnd PIXEL

oDlgTransf:lMaximized := .T.

oLayer := FWLayer():New()

//--------------------------------------------------------
// Cria as layers e janelas que serão utilizadas na tela.
//--------------------------------------------------------
oLayer:init(oDlgTransf,.F.)

oLayer:addLine("LINHA1",15,.F.)
oLayer:addCollumn("COLUNA1",100,.F.,"LINHA1")
oLayer:addWindow("COLUNA1", "JANELA1","Sela��o de Pedidos",100,.F.,.F.,,"LINHA1")

oLayer:addLine("LINHA2",32,.F.)
oLayer:addCollumn("COLUNA2",100,.F.,"LINHA2")
oLayer:addWindow("COLUNA2", "JANELA2","Itens do Pedido de Origem",100,.F.,.F.,,"LINHA2")

oLayer:addLine("LINHA3",15,.F.)
oLayer:addCollumn("COLUNA3",100,.F.,"LINHA3")
oLayer:addWindow("COLUNA3", "JANELA3","A��es",100,.F.,.F.,,"LINHA3")

oLayer:addLine("LINHA4",31,.F.)
oLayer:addCollumn("COLUNA4",100,.F.,"LINHA4")
oLayer:addWindow("COLUNA4", "JANELA4","Itens do Pedido de Destino",100,.F.,.F.,,"LINHA4")

//------------------------------------------
// Objetos de Paineis de cada Janela criada.
//------------------------------------------
oPnlInfoPed	:= oLayer:GetWinPanel("COLUNA1","JANELA1","LINHA1")
oPnlPedOri	:= oLayer:GetWinPanel("COLUNA2","JANELA2","LINHA2")
oPnlAcoes	:= oLayer:GetWinPanel("COLUNA3","JANELA3","LINHA3")
oPnlPedDes	:= oLayer:GetWinPanel("COLUNA4","JANELA4","LINHA4")

//---------------------------------
// Layer de inforam��o dos Pedidos.
//---------------------------------
TSay():New(05,05,{|| "Pedido Origem:" },oPnlInfoPed,,,,,,.T., , ,50,40)
oPedOrig := TGet():New(04,50, bSetGet(cPedOri) ,oPnlInfoPed,40,10,PESQPICT("SC5","C5_NUM"), {|| FValPedidos(cPedOri,.T.) } ,,,,,,.T.,,,{|| .T. },,,,,,,)
oPedOrig:cF3 := "SC5"
oPedOrig:lHasButton := .F.

TSay():New(05,110,{|| "Pedido Destino:" },oPnlInfoPed,,,,,,.T., , ,50,40)
oPedDest := TGet():New(04,160, bSetGet(cPedDes) ,oPnlInfoPed,40,10,PESQPICT("SC5","C5_NUM"), {|| FValPedidos(cPedDes,.F.) } ,,,,,,.T.,,,{|| .T. },,,,,,,)
oPedDest:cF3 := "SC5"
oPedDest:lHasButton := .F.


//--------------------------------
// Layer do Grid dos Itens Origem.
//--------------------------------
oGetOrigem := MsNewGetDados():New( 0 , 0 , 0 , 0 , nStyleOrigem , , ,  , aAltOrigem , 0 , Len(aColsOrigem) ,  ,  ,  , oPnlPedOri , aHeadOrigem , aColsOrigem , , )
oGetOrigem:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
oGetOrigem:oBrowse:SetBlkBackColor({|| FColorLin( oGetOrigem:ACOLS  , oGetOrigem:nAt , oGetOrigem:AHEADER  ) })

//-----------------
// Layer das A��es.
//-----------------
oAdicionar		:= TButton():New( 004 , 010 , "(+) Adicionar"	, oPnlAcoes , bAdiciona	, 45 , 15 ,  ,  ,  , .T. ,  ,  ,  ,  ,  ,  )
oRemover		:= TButton():New( 004 , 080 , "(-) Remover"		, oPnlAcoes , bRemove	, 45 , 15 ,  ,  ,  , .T. ,  ,  ,  ,  ,  ,  )

//-----------------------------------
// Layer do Grid dos Itens de Origem.
//-----------------------------------
oGetDestino := MsNewGetDados():New( 0 , 0 , 0 , 0 , nStyleDestino , , ,  , aAltDestino , 0 , Len(aColsDestino) ,  ,  ,  , oPnlPedDes , aHeadDestino , aColsDestino , , )
oGetDestino:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

ACTIVATE MSDIALOG oDlgTransf CENTERED ON INIT (EnchoiceBar(oDlgTransf,bProcOk,bCancel,,))

cCadastro := cBkpCadastro

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FValPedidos
Fun��o para valida��o dos pedidos digitados.

@author  Irineu Filho
@since   08/10/2018 - 11:23
/*/
//-------------------------------------------------------------------
Static Function FValPedidos(cPedPar,lOrigem,cPedOrigem)

Local lRetorno := .T.

Default cPedOrigem := ""

//Valida a obrigatoriedade da digita��o do Pedido Origem
If lOrigem
	If Empty(cPedPar)
		lRetorno := .F.
		Aviso("Transfer�ncia de Aloca��o","Preenchimento do Pedido de Origem � obrigat�rio.",{"Fechar"})
	EndIf
EndIf

If lRetorno .AND. !Empty(cPedPar)
	lRetorno := ExistCpo("SC5", cPedPar)
EndIf

//--------------------------------------------------------------------
// Chama fun��o para carregar as informa��es dos itens no grid Origem.
//--------------------------------------------------------------------
If lRetorno .AND. lOrigem
	FCargaOrigem(cPedPar)
	If Len(oGetOrigem:ACOLS) == 0
		Aviso("Transfer�ncia de Aloca��o","N�o h� itens aptos � serem transferidos.",{"Fechar"})
		lRetorno := .F.
	EndIf
EndIf

Return lRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FCargaOrigem
Fun��o para carregar o grid de Origem

@author  Irineu Filho
@since   08/10/2018 - 16:31
/*/
//-------------------------------------------------------------------
Static Function FCargaOrigem(cPedPar)

Local aInfoCabec := ACLONE(oGetOrigem:AHEADER)
Local nX := 1
Local cCampoTab := ""
Local aAuxCols := {}
Local aInfoCols := {}
Local nPosC6QTDVEN := Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_QTDVEN"})
Local nPosC6VALOR := Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_VALOR"})
Local nPosC6PRCVEN := Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_PRCVEN"})
Local nPosC6QTDENT := Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_QTDENT"})

SC6->(dbSetOrder(1))
SC6->(dbSeek( xFilial("SC6") + cPedPar ))
While SC6->(!EOF()) .AND. (SC6->C6_FILIAL + SC6->C6_NUM) == (xFilial("SC6") + cPedPar)
	
	aAuxCols := {}
	For nX := 1 To Len(aInfoCabec)
		cCampoTab := "SC6->" + Alltrim(aInfoCabec[nX][2])
		Aadd(aAuxCols,&(cCampoTab))
	Next nX
	Aadd(aAuxCols,.F.)
	
	Aadd(aInfoCols,ACLONE(aAuxCols))

	SC6->(dbSkip())
	
EndDo

aBkpOrigem := ACLONE(aInfoCols)
Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_ITEM"})

//--------------------------------
// Acerta a quantidade disponivel. Saldo (C6_QTDVEN-C6_QTDENT)
//--------------------------------
For nX := 1 To Len(aInfoCols)
	aInfoCols[nX][nPosC6QTDVEN] := (aInfoCols[nX][nPosC6QTDVEN] - aInfoCols[nX][nPosC6QTDENT])
	aInfoCols[nX][nPosC6VALOR] := (aInfoCols[nX][nPosC6PRCVEN] * aInfoCols[nX][nPosC6QTDVEN])
Next nX

a2BkpOrigem := ACLONE(aInfoCols)

oGetOrigem:ACOLS := ACLONE(aInfoCols)
oGetOrigem:Refresh()

oGetDestino:ACOLS := {}
oGetDestino:Refresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FAdcItem
Adiciona o item do Grid de Origem para o Grid de Destino

@author  Irineu Filho
@since   08/10/2018 - 11:30
/*/
//-------------------------------------------------------------------
Static Function FAdcItem()

Local nLinhaOrigem	:= oGetOrigem:OBROWSE:nAt
Local aInfoAtu		:= {}
Local aDestCols		:= ACLONE(oGetDestino:ACOLS)
Local nPosC6ITEM	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_ITEM"})
Local nPosC6QTDVEN	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_QTDVEN"})
Local nPosC6QTDENT	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_QTDENT"})
Local nPosC6VALOR	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_VALOR"})
Local nPosC6PRCVEN	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_PRCVEN"})
Local nPosC6ENTREG	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_ENTREG"})
Local nPosC6BLQ		:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_BLQ"})
Local nNewQtd		:= 0
Local nNewValor		:= 0
Local nPrcItem		:= 0
Local lContinua		:= .T.

//-----------------------------------------
// Valida��o do item eliminado por Residuo.
//-----------------------------------------
If lContinua
	If Alltrim(oGetOrigem:ACOLS[nLinhaOrigem][nPosC6BLQ]) == "R"
		Aviso("Transfer�ncia de Aloca��o","Item eliminado por res�duo, n�o ser� possivel adicionar!",{"Fechar"})
		lContinua := .F.
	EndIf
EndIf

//-------------------------------------------------------------------
// Valida��o para quando n�o houver saldo.
//-------------------------------------------------------------------
If lContinua
	If oGetOrigem:ACOLS[nLinhaOrigem][nPosC6QTDVEN] == 0
		Aviso("Transfer�ncia de Aloca��o","Item sem saldo! N�o ser� possivel adicionar!",{"Fechar"})
		lContinua := .F.
	EndIf
EndIf

//-------------------------------------------------------------------
// Valida��o se o item j� est� adicionado.
//-------------------------------------------------------------------
If lContinua
	nAchouItem := Ascan(aDestCols,{|x|Alltrim(x[nPosC6ITEM]) == oGetOrigem:ACOLS[nLinhaOrigem][nPosC6ITEM] })

	If nAchouItem == 0
		aInfoAtu := ACLONE(oGetOrigem:ACOLS[nLinhaOrigem])
		Aadd( aDestCols , ACLONE(aInfoAtu) )
	Else
		Aviso("Transfer�ncia de Aloca��o","Item j� adicionado!",{"Fechar"})
		lContinua := .F.
	EndIf
EndIf

//-----------------------------
// Atualiza os Grid de Pedidos.
//-----------------------------
If lContinua
	
	//----------------------------
	// Atualiza o grid do Destino.
	//----------------------------
	oGetDestino:ACOLS := ACLONE(aDestCols)
	oGetDestino:Refresh()
	
	//--------------------------
	// Atualiza o total do item.
	//--------------------------
	If lContinua
		
		nQtdOrigem	:= aBkpOrigem[nLinhaOrigem][nPosC6QTDVEN]
		nEntOrigem	:= aBkpOrigem[nLinhaOrigem][nPosC6QTDENT]
		nQtdDestino	:= oGetDestino:ACOLS[Len(oGetDestino:ACOLS)][nPosC6QTDVEN]
		nPrcItem := aBkpOrigem[nLinhaOrigem][nPosC6PRCVEN]
		
		dDtOrigem := aBkpOrigem[nLinhaOrigem][nPosC6ENTREG]
		
		nNewQtd := (nQtdOrigem-nEntOrigem) - nQtdDestino
		nNewValor := nNewQtd * nPrcItem
		oGetOrigem:ACOLS[nLinhaOrigem][nPosC6QTDVEN] := nNewQtd
		oGetOrigem:ACOLS[nLinhaOrigem][nPosC6VALOR] := nNewValor
		oGetOrigem:ACOLS[nLinhaOrigem][nPosC6ENTREG] := dDtOrigem
		oGetOrigem:Refresh()
	EndIf
	
EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FRmvItem
Remove o item do Grid de Destino

@author  Irineu Filho
@since   08/10/2018 - 11:31
/*/
//-------------------------------------------------------------------
Static Function FRmvItem()

Local aAuxCols		:= {}
Local aColsDest		:= ACLONE(oGetDestino:ACOLS)
Local nLinhaDestino	:= oGetDestino:OBROWSE:nAt
Local nX			:= 0
Local nPosC6ITEM	:= Ascan(oGetDestino:AHEADER,{|x|Alltrim(X[2])=="C6_ITEM"})


If Len(aColsDest)

	For nX := 1 To Len(aColsDest)
		If nX <> nLinhaDestino
			Aadd( aAuxCols , aColsDest[nX] )
		EndIf
	Next nX

	//-----------------------------------------------
	// Volta as informa��es originais do grid Origem.
	//-----------------------------------------------
	cItem := Alltrim(aColsDest[nLinhaDestino][nPosC6ITEM])

	nPosBkp := Ascan(a2BkpOrigem,{|x|Alltrim(X[nPosC6ITEM])==cItem})
	If nPosBkp > 0
		oGetOrigem:ACOLS[nPosBkp] := ACLONE(a2BkpOrigem[nPosBkp])
		oGetOrigem:Refresh()
	EndIf

	//------------------------------------------------------
	// Atualiza o Grid de Destino retirando o item removido.
	//------------------------------------------------------
	oGetDestino:ACOLS := ACLONE(aAuxCols)
	oGetDestino:Refresh()

EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FCfgTela
Fun��o para retorno dos arrays utilizados no GetDados.

@author  Irineu Filho
@since   08/10/2018 - 12:43
/*/
//-------------------------------------------------------------------
Static Function FCfgTela()

Local aRetorno		:= {}
Local aCabecGrid	:= {}
Local aAuxRet		:= {}
Local aAltCpos		:= {}
Local cTabGrid		:= "SC6"
Local cCpoRetira	:= "C6_FILIAL"

SX3->(dbSetOrder(1))
SX3->(dbSeek( cTabGrid ))

While SX3->(!EOF()) .AND. (SX3->X3_ARQUIVO == cTabGrid)
	
	If !(Alltrim(SX3->X3_CAMPO) $ cCpoRetira)
		If SX3->X3_CONTEXT <> "V"
			
			//---------------------------
			// Tratamento para os Valids.
			//---------------------------
			If Alltrim(SX3->X3_CAMPO) == "C6_QTDVEN"
				cValid := "StaticCall(ADMVP09,FVldQtd)"
				Aadd(aAltCpos,SX3->X3_CAMPO)
			ElseIf Alltrim(SX3->X3_CAMPO) == "C6_ENTREG"
				cValid := "StaticCall(ADMVP09,FVldEnt)"
				Aadd(aAltCpos,SX3->X3_CAMPO)
			Else
				cValid := SX3->X3_VALID
			EndIf
			
			aAuxRet := {}
			Aadd( aAuxRet , SX3->X3_TITULO )
			Aadd( aAuxRet , SX3->X3_CAMPO )
			Aadd( aAuxRet , SX3->X3_PICTURE )
			Aadd( aAuxRet , SX3->X3_TAMANHO )
			Aadd( aAuxRet , SX3->X3_DECIMAL )
			Aadd( aAuxRet , cValid )
			Aadd( aAuxRet , SX3->X3_USADO )
			Aadd( aAuxRet , SX3->X3_TIPO )
			Aadd( aAuxRet , SX3->X3_F3 )
			Aadd( aAuxRet , SX3->X3_CONTEXT )
			Aadd( aAuxRet , SX3->X3_CBOX )
			Aadd( aAuxRet , SX3->X3_RELACAO )
			Aadd( aCabecGrid , ACLONE(aAuxRet) )
			
		EndIf
	EndIf
	
	SX3->(dbSkip())
	
EndDo

Aadd(aRetorno , aCabecGrid )	//Cabe�alho Origem
Aadd(aRetorno , aCabecGrid )	//Cabe�alho Destino
Aadd(aRetorno , aAltCpos )		//Altera��o Origem
Aadd(aRetorno , Nil )			//Altera��o Destino

Return aRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FVldQtd
Fun��o para valida��o da Quantidade (Origem)

@author  Irineu Filho
@since   08/10/2018 - 13:05
/*/
//-------------------------------------------------------------------
Static Function FVldQtd()

Local lRetorno		:= .T.
Local nQtdDigit		:= &(ReadVar())
Local nQtdOriginal	:= 0
Local nLinhaOrigem	:= oGetOrigem:OBROWSE:nAt
Local nPosC6QTDVEN	:= Ascan(oGetOrigem:AHEADER,{|x|Alltrim(X[2])=="C6_QTDVEN"})
Local nPosC6VALOR	:= Ascan(oGetOrigem:AHEADER,{|x|Alltrim(X[2])=="C6_VALOR"})
Local nPosC6PRCVEN	:= Ascan(oGetOrigem:AHEADER,{|x|Alltrim(X[2])=="C6_PRCVEN"})
Local nPosC6BLQ		:= Ascan(oGetOrigem:AHEADER,{|x|Alltrim(X[2])=="C6_BLQ"})
Local nPrcItem		:=  oGetOrigem:ACOLS[nLinhaOrigem][nPosC6PRCVEN]
Local nValorTotal	:= 0

lRetorno := Positivo()

If lRetorno
	nQtdOriginal := aBkpOrigem[nLinhaOrigem][nPosC6QTDVEN]
	
	If nQtdDigit > nQtdOriginal
		lRetorno := .F.
		Aviso("Transfer�ncia de Aloca��o","Quantidade digitada (" + cValToChar(nQtdDigit) + ") maior que a quantidade original do item (" + cValToChar(nQtdOriginal) + ")",{"Fechar"})
	EndIf
EndIf

//---------------------------------------------
// Tratamento dos itens eliminados por residuo.
//---------------------------------------------
If lRetorno

	If Alltrim(oGetOrigem:ACOLS[nLinhaOrigem][nPosC6BLQ]) == "R"
		lRetorno := .F.
		Aviso("Transfer�ncia de Aloca��o","Item eliminado por res�duo!",{"Fechar"})
	EndIf

EndIf

//--------------------------
// Atualiza o total do item.
//--------------------------
If lRetorno
	nValorTotal := nQtdDigit * nPrcItem
	oGetOrigem:ACOLS[nLinhaOrigem][nPosC6VALOR] := nValorTotal
	oGetOrigem:Refresh()
EndIf


Return lRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FVldEnt
Fun��o para valida��o da Data de Entrega (Origem)

@author  Irineu Filho
@since   08/10/2018 - 13:06
/*/
//-------------------------------------------------------------------
Static Function FVldEnt()

Local lRetorno := .T.
Local dDtDigit := &(ReadVar())

If (dDtDigit < dDataBase)
	lRetorno := .F.
	Aviso("Transfer�ncia de Aloca��o","Data de entrega: (" + DTOC(dDtDigit) + ") n�o permitida. Informar uma data maior ou igual a data de hoje.",{"Fechar"})
EndIf

Return lRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FConfirma
Fun��o ap�s a confirma��o da Tela.

@author  Irineu Filho
@since   09/10/2018 - 12:12
/*/
//-------------------------------------------------------------------

Static Function FConfirma(oDlgPar,cPedOriPar,cPedDesPar,oGetOriPar)

Local cMensagem := ""
Local nOpcGera := 0
Local aSC5Cabec := {}
Local aSC6Itens := {}
Local nX := 0
Local aExecMata := {}
Local nOpcExec := 0
Local aLogExec := {}
Local cPedExec := ""
Local alItnsAloc := { nil, nil }
Local cNewNumPV	:= ""

If Empty(cPedDesPar)
	cMensagem := "Confirma a gera��o de um Novo Pedido e a Altera��o do Pedido (" + cPedOriPar + ") ?"
	nOpcGera := 1 //Ser� necess�rio criar um novo pedido e alterar o pedido origem.
ElseIf !Empty(cPedDesPar)
	cMensagem := "Confirma a gera��o Altera��o do Pedido Origem (" + cPedOriPar + ") e a Altera��o do Pedido Destino (" + cPedDesPar + ") ?"
	nOpcGera := 2 //Ser� necess�rio criar um novo pedido e alterar o pedido origem.
Else
	cMensagem := "TRATAR"
EndIf

If FValTela()
	If Aviso("Transfer�ncia de Aloca��o",cMensagem,{"Sim","N�o"}) == 1
		
		//------------------------------------
		// Busca os dados para criar o Pedido.
		//------------------------------------
		If nOpcGera == 1

			//Incluir Destino
			aSC5Cabec := FCriaSC5(cPedOriPar,3,@cNewNumPV)
			aSC6Itens := FCriaSC6(oGetDestino,"",3,.F.,cNewNumPV)
			Aadd( aExecMata , { ACLONE(aSC5Cabec) , ACLONE(aSC6Itens) , 3 , "" } )
			
			alItnsAloc[2] := ACLONE( aSC6Itens )
			
			//Alterar Origem
			aSC5Cabec := FCriaSC5(cPedOriPar,4)
			aSC6Itens := FCriaSC6(oGetOrigem,cPedOriPar,4,.T.)
			If Len(aSC6Itens) == 0
				//TRATAR O RESIDUO
			Else
				Aadd( aExecMata , { ACLONE(aSC5Cabec) , ACLONE(aSC6Itens) , 4 , cPedOriPar } )
			EndIf
			
			alItnsAloc[1] := ACLONE( aSC6Itens )
			
		Else

			//Incluir Destino
			aSC5Cabec := FCriaSC5(cPedDesPar,4)
			aSC6Itens := FCriaSC6(oGetDestino,cPedDesPar,4,.F.)
			Aadd( aExecMata , { ACLONE(aSC5Cabec) , ACLONE(aSC6Itens) , 4 , "" } )
			
			alItnsAloc[2] := ACLONE( aSC6Itens )
			
			//Alterar Origem
			aSC5Cabec := FCriaSC5(cPedOriPar,4)
			aSC6Itens := FCriaSC6(oGetOrigem,cPedOriPar,4,.T.)
			If Len(aSC6Itens) == 0
				//TRATAR O RESIDUO
			Else
				Aadd( aExecMata , { ACLONE(aSC5Cabec) , ACLONE(aSC6Itens) , 4 , cPedOriPar } )
			EndIf
			
			alItnsAloc[1] := ACLONE( aSC6Itens )

		EndIf
		
		//--------------------------------------
		// Prepara��o para execu��o do MATA410 -
		//--------------------------------------
		begin transaction
		
			For nX := 1 To Len(aExecMata)
				
				aSC5Cabec := aClone( aExecMata[nX][1] )
				aSC6Itens := aClone( aExecMata[nX][2] )
				nOpcExec := aExecMata[nX][3]
				cPedExec := aExecMata[nX][4]
				
				aRetExec := {}
				aRetExec := FMata410( aSC5Cabec , aSC6Itens , nOpcExec , cPedExec )
				
				If !aRetExec[Len(aRetExec)][1]
					DisarmTransaction()
					Exit
				Else
					Aadd( aLogExec , ACLONE(aRetExec) )
					
				Endif
				
			Next nX
			
			//---------------------
			// Processa alocacoes -
			//---------------------
			procAloc( cPedOriPar, aClone( alItnsAloc ) )
		
		end transaction
		
		If !aRetExec[Len(aRetExec)][1]
			Aviso("Tranfer�ncia de Aloca��o","ERRO NA EXECU��O DO PEDIDO " + CRLF + aRetExec[Len(aRetExec)][2],{"Fechar"}, 3)
		Else
		
			//--------------------------------------------------------------
			// Executa transferencia de alocacoes da fila de processamento -
			//--------------------------------------------------------------
			U_ADMVP02(6)

			cMensagem := "Transferencia realizada com sucesso: " + CRLF + CRLF

			For nX := 1 To Len(aLogExec)
				cMensagem += aLogExec[nX][1][2] + CRLF
			Next nX

			Aviso("Tranfer�ncia de Aloca��o",cMensagem,{"Fechar"})
			
			oGetOrigem:ACOLS	:= {}
			oGetOrigem:Refresh()
			oGetDestino:ACOLS	:= {}
			oGetDestino:Refresh()

			cPedOriPar := Space(nTamC5NUM)
			cPedDesPar := Space(nTamC5NUM)

			oGetOriPar:SetFocus()

		EndIf
		
	EndIf
EndIf

Return Nil

/*/{Protheus.doc} procAloc
Processa transfer�ncia de aloca��es
@author DS2U (SDA)
@since 04/01/2019
@version 1.0
@param clPedOri, characters, Codigo do pedido origem
@param alItnsAloc, array of logical, Array com os itens do pedido origem e itens do pedido destino
@type function
/*/
Static Function procAloc( clPedOri, alItnsAloc )

	local alArea	:= getArea()
	local nlPosPed
	local nlPosDtEm
	local nlPosHrEm
	local nlPosDtEnt
	local nlPosItm
	local nlPosPrd
	local nlPosCanal
	local nlPosQtd
	local nlPosPedD
	local nlPosItmD
	local nlPosQtdD
	local alSC6Ori	:= {}
	local alSC6Des	:= {}
	
	default clPedOri	:= ""
	default alItnsAloc	:= {}
	
	if ( len( alItnsAloc ) > 1 )
	
		alSC6Ori := aClone( alItnsAloc[1] )
		alSC6Des := aClone( alItnsAloc[2] )
	
		dbSelectArea( "SC5" )
		SC5->( dbSetOrder( 1 ) )
		
		if ( SC5->( dbSeek( fwxFilial( "SC5" ) + clPedOri ) ) )
	
			nlPosPed	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_NUM" } )
			nlPosDtEm	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_XDTEMIS" } )
			nlPosHrEm	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_XTIMEST" } )
			nlPosDtEnt	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_ENTREG" } )
			nlPosItm	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_ITEM" } )
			nlPosPrd	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_PRODUTO" } )
			nlPosCanal	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_LOCAL" } )
			nlPosQtd	:= aScan( alSC6Ori[1], {|x| allTrim( x[1] ) == "C6_QTDVEN" } )
			
			nlPosPedD	:= aScan( alSC6Des[1], {|x| allTrim( x[1] ) == "C6_NUM" } )
			nlPosItmD	:= aScan( alSC6Des[1], {|x| allTrim( x[1] ) == "C6_ITEM" } )
			nlPosQtdD	:= aScan( alSC6Des[1], {|x| allTrim( x[1] ) == "C6_QTDVEN" } )
			
			//-------------------------------------------------------------------------
			// Adiciona item do pedido de venda na fila de processamento de aloca��es -
			//-------------------------------------------------------------------------
			U_ADMVP02( 2,{"T",; 
						SC5->C5_FILIAL,;
						alSC6Ori[1][nlPosPed][2],;
						alSC6Ori[1][nlPosItm][2],;
						alSC6Ori[1][nlPosPrd][2],;
						alSC6Ori[1][nlPosQtd][2],;
						SC5->C5_XPRIORI,;
						alSC6Ori[1][nlPosDtEm][2],;
						alSC6Ori[1][nlPosHrEm][2],;
						alSC6Ori[1][nlPosDtEnt][2],; 
						,;
						,;
						,;
						alSC6Des[1][nlPosPedD][2],;
						alSC6Des[1][nlPosItmD][2],;
						alSC6Des[1][nlPosQtdD][2];
						} ) // Acrescenta item na fila para desaloca��o
						
		endif
		
	endif
	
	restArea( alArea )
	
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} FMata410()
Fun��o auxiliar para execu��o do MATA410 de acordo com os parametros e op��o.

@author  Irineu Filho
@since   09/10/2018 - 12:27
/*/
//-------------------------------------------------------------------

Static Function FMata410(aSC5,aSC6,nOpcPar,cPedidoPar)

Local aRetorno	:= {}
Local cErro		:= ""
Local cNomeArq	:= "ERRO_" + DTOS(dDataBase) + "_" + ALLTRIM( strTRAN( TIME(),":" )) + ".txt"
Local cMensagem := "Pedido: " + cPedidoPar

lMsErroAuto := .F.

makedir("\erro_mata410\")

MsExecAuto({|x,y,z| MATA410(x,y,z)},aSC5,aSC6,nOpcPar,.T.)

If lMsErroAuto
	// Grava arquivo com o erro
	cErro := "N�mero Pedido: " + cPedidoPar + CRLF + "Op��o: " + cValToChar(nOpcPar) + CRLF + MostraErro("\erro_mata410\",cNomeArq)
	Aadd(aRetorno,{ .F. , cErro })
Else
	
	cMensagem := "Pedido: " + SC5->C5_NUM
	
	If nOpcPar == 3
		cMensagem += " - Inclu�do com Sucesso!"
	ElseIf nOpcPar == 4
		cMensagem += " - Alterado com Sucesso!"
	ElseIf nOpcPar == 5
		cMensagem += " - Exclu�do com Sucesso!"
	Else
		cMensagem += " - DEU RUIM COM 'SUCESSO' !"
	EndIf
	
	Aadd(aRetorno,{ .T. , cMensagem })
	
EndIf


Return aRetorno


//-------------------------------------------------------------------
/*/{Protheus.doc} FCriaSC5
Cria o array para MATA410 referente ao SC5, baseado no pedido enviado por parametro.

@author  Irineu Filho
@since   09/10/2018 - 13:25
/*/
//-------------------------------------------------------------------

Static Function FCriaSC5(cPedPar,nOpcPar, cNewNumPV)

Local aArea := GetArea()
Local aSC5Strut := SC5->(dbStruct())
Local aRetorno := {}
Local cCampoTab := ""
Local nX := 1

SC5->(dbSetOrder(1))
SC5->(dbSeek( xFilial("SC5") + cPedPar ))

For nX := 1 To Len(aSC5Strut)
	
	SX3->(dbSetOrder(2))
	If SX3->(dbSeek( aSC5Strut[nX][1] ))
		If SX3->X3_CONTEXT <> "V"
			If Alltrim(aSC5Strut[nX][1]) == "C5_NUM"
				If nOpcPar <> 3
					aAdd( aRetorno, {Alltrim(aSC5Strut[nX][1]) , cPedPar , Nil} )
				else
					cNewNumPV := getSxeNum("SC5", "C5_NUM")
					aAdd( aRetorno, {Alltrim(aSC5Strut[nX][1]) , cNewNumPV , Nil} )
				EndIf
			ElseIf Alltrim(aSC5Strut[nX][1]) == "C5_EMISSAO"
				aAdd( aRetorno, {Alltrim(aSC5Strut[nX][1]) , dDataBase , Nil} )
			Else
				cCampoTab := "SC5->" + aSC5Strut[nX][1]
				aAdd( aRetorno, {Alltrim(aSC5Strut[nX][1]) , &(cCampoTab) , Nil} )
			EndIf
		EndIf
	EndIf
	
Next nX

RestArea(aArea)

Return aRetorno


//-------------------------------------------------------------------
/*/{Protheus.doc} FCriaSC6
Cria o array de itens SC6 para execauto MATA410.

@author  Irineu Filho
@since   09/10/2018 - 13:43
/*/
//-------------------------------------------------------------------

Static Function FCriaSC6(oObjTrata,cNumPed,nOpcPar,lOrigem,cNewNumPV)

Local aArea := GetArea()
Local aRetorno := {}
Local aAuxRet := {}
Local aHeadSC6 := ACLONE(oObjTrata:AHEADER)
Local aColsSC6 := IIF(lOrigem,ACLONE(aBkpOrigem),oObjTrata:ACOLS)
Local nPosC6QTDVEN	:= Ascan(aHeadSC6,{|x|Alltrim(X[2])=="C6_QTDVEN"})
Local nPosC6QTDENT	:= Ascan(aHeadSC6,{|x|Alltrim(X[2])=="C6_QTDENT"})
Local nPosC6PRCVEN	:= Ascan(aHeadSC6,{|x|Alltrim(X[2])=="C6_PRCVEN"})
Local nPosC6ITEM	:= Ascan(aHeadSC6,{|x|Alltrim(X[2])=="C6_ITEM"})
Local nPosC6NUM		:= Ascan(aHeadSC6,{|x|Alltrim(X[2])=="C6_NUM"})
Local nX := 1
Local nItem := 0
Local nQtdOrigem := 0
Local nQtdDestino := 0
Local cCampoTab := ""
Local xConteudo := Nil
Local cUltItem := ""

SC5->(dbSetOrder(1))
SC5->(dbSeek( xFilial("SC5") + cNumPed ))

//Carrega os itens do SC6 atual quando altera��o e Pedido de Destino.
If nOpcPar <> 3 .AND. !lOrigem

	SC6->(dbSetOrder(1))
	SC6->(dbSeek( xFilial("SC6") + cNumPed ))
	While SC6->(!EOF()) .AND. (SC6->C6_FILIAL + SC6->C6_NUM) == (xFilial("SC6") + cNumPed)

		aAuxRet := {}
		For nX := 1 To Len(aHeadSC6)
			cCampoTab := "SC6->" + Alltrim(aHeadSC6[nX][2])
			xConteudo := &(cCampoTab)
			If !Empty(xConteudo)
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , xConteudo , Nil} )
			EndIf
		Next nX
		
		Aadd(aRetorno,ACLONE(aAuxRet))
		cUltItem := SC6->C6_ITEM
		SC6->(dbSkip())

	EndDo

	If Empty(cUltItem)
		cUltItem := PADL(1,nTamC6ITEM)
	EndIf

EndIf


For nItem := 1 To Len(aColsSC6)
	
	aAuxRet := {}
	For nX := 1 To Len(aHeadSC6)
		If Alltrim(aHeadSC6[nX][2]) == "C6_NUM" 
		
			if nOpcPar <> 3
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , cNumPed , Nil} )
			else
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , cNewNumPV , Nil} )
			endif
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_QTDVEN" .AND. lOrigem
			nQtdDestino	:= 0
			nPosItem	:= Ascan(oGetDestino:ACOLS,{|x|Alltrim(X[nPosC6ITEM])==Alltrim(aColsSC6[nItem][nPosC6ITEM])})
			If nPosItem > 0
				nQtdDestino	:= oGetDestino:ACOLS[nPosItem][nPosC6QTDVEN]
			EndIf
			nQtdOrigem	:= aBkpOrigem[nItem][nPosC6QTDVEN] - nQtdDestino
			If nQtdOrigem == 0
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , aBkpOrigem[nItem][nPosC6QTDVEN] , Nil} )
				aAdd( aAuxRet, {"C6_BLQ" , "R" , Nil} )
			Else
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , nQtdOrigem , Nil} )
			EndIf
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_VALOR" .AND. lOrigem
			nQtdDestino	:= 0
			nPosItem	:= Ascan(oGetDestino:ACOLS,{|x|Alltrim(X[nPosC6ITEM])==Alltrim(aColsSC6[nItem][nPosC6ITEM])})
			If nPosItem > 0
				nQtdDestino	:= oGetDestino:ACOLS[nPosItem][nPosC6QTDVEN]
			EndIf
			nQtdOrigem	:= aBkpOrigem[nItem][nPosC6QTDVEN] - nQtdDestino
			If nQtdOrigem == 0
				nQtdOrigem := aBkpOrigem[nItem][nPosC6QTDVEN]
			EndIf
			nPrcVen		:= aBkpOrigem[nItem][nPosC6PRCVEN]
			nValorTotal := nQtdOrigem * nPrcVen
			aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , nValorTotal , Nil} )
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_QTDENT" .AND. !lOrigem //Limpa o campo de Quantidade entregue.
			aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , 0 , Nil} )
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_NOTA" .AND. !lOrigem //Limpa o campo de NF.
			aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , "" , Nil} )
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_SERIE" .AND. !lOrigem //Limpa o campo de S�rie.
			aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , "" , Nil} )
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_ITEM" .AND. !lOrigem .AND. nOpcPar <> 3 //Limpa o campo item
			cUltItem := SOMA1(cUltItem)
			aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , cUltItem , Nil} )
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_BLQ"
			If !Empty(aColsSC6[nItem][nX])
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , aColsSC6[nItem][nX] , Nil} )
			EndIf
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_XDTEMIS"
		
			If !Empty(aColsSC6[nItem][nX])
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , aColsSC6[nItem][nX] , Nil} )
			else
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , SC5->C5_EMISSAO , Nil} )
			EndIf
		
		ElseIf Alltrim(aHeadSC6[nX][2]) == "C6_XTIMEST"
		
			If !Empty(aColsSC6[nItem][nX])
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , aColsSC6[nItem][nX] , Nil} )
			else
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , "00:00:00.000" , Nil} )
			EndIf
					
		Else
			If !Empty(aColsSC6[nItem][nX])
				aAdd( aAuxRet, {Alltrim(aHeadSC6[nX][2]) , aColsSC6[nItem][nX] , Nil} )
			EndIf
		EndIf
	Next nX

	Aadd(aRetorno, ACLONE(aAuxRet) )

Next nItem

RestArea(aArea)

Return aRetorno

//-------------------------------------------------------------------
/*/{Protheus.doc} FValTela
Fun��o para valida��o ap�s confirmar a tela.

@author  Irineu Filho
@since   09/10/2018 - 16:32
/*/
//-------------------------------------------------------------------
Static Function FValTela()

Local lRetorno := .T.

If Len(oGetDestino:ACOLS) == 0
	Aviso("Transfer�ncia de Aloca��o","Para prosseguir ser� necess�rio adicionar ao menos um item.",{"Fechar"})
	lRetorno := .F.
EndIf

Return lRetorno


//-------------------------------------------------------------------
/*/{Protheus.doc} FColorLin
Fun��o para colorir as linhas que est�o com C6_BLQ = R (Residuo)

@author  Irineu Filho
@since   11/10/2018 - 14:46
/*/
//-------------------------------------------------------------------

Static Function FColorLin(aDados,nLinAtu,aCabec)

Local nCorRet := Nil
Local nPosC6BLQ := Ascan(oGetOrigem:AHEADER,{|x|Alltrim(X[2])=="C6_BLQ"})

If Alltrim(aDados[nLinAtu][nPosC6BLQ]) == "R"
	nCorRet := RGB(255,196,196)
EndIf

Return nCorRet