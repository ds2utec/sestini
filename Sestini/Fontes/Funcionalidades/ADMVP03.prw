#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} ADMVP03
Rotina para Aprovar/Reprova��o Pedido de Vendas
bloqueio de Credito

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVP03()

    Local cMotivo       := ""
    Local lMotv         := .F.
    Local nOpc          := 0
    Local cMsg          := ""
    Local cBloqueio		:= ""
    Local aBloq			:= {}

    //Somente pedido que n�o possuem nota
    If Empty(SC5->C5_NOTA)

    	If U_FVUsrADV( __cUserId , "2" , 1 )
    	
	        While !lMotv
	
	            nOpc := Aviso( "Aprovar ou Reprovar?",@cMotivo,{"Reprovar","Aprovar","Sair", "Vis. Motivos" },2,"Motivo",,,.T.)
	
	            If nOpc == 3
	
	                Exit
	
	            ElseIf nOpc == 4
	            
	            	U_ADP12View()
	            
	            Else
	
	                If !Empty(cMotivo)
	
	                    lMotv := .T.
	
	                Else
	
	                    Help(,, "ADMVP03",, '� obrigat�rio informar o motivo.' , 1, 0)
	
	                EndIf
	
	            EndIf
	
	        EndDo
	
	        If lMotv
	
	            If nOpc == 1
	                // Reprovado
	                cBloqueio := "R"
	            Else
	            	cBloqueio := "A"
	            EndIf
	
	            // Atualializa o Pedido de Venda - Campos de bloqueio de Credito do Pedido de Venda
	            aadd( aBloq , { IIF(nOpc == 1 , "1" , "2" ) , cMotivo } )
	            u_ADP12Grv( SC5->C5_NUM, "1" /*TpMovimento*/ , aBloq , cBloqueio )
	
	        EndIf
	        
    	EndIf
	       
    EndIf

Return