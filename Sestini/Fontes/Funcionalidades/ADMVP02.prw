#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

//---------------------------------------
// Posicoes do array de disponibilidade -
//---------------------------------------
STATIC DTDISPO	:= 1
STATIC QTDISPO	:= 2
STATIC PEDCOM	:= 3
STATIC ITPEDCOM := 4
STATIC CANALVND	:= 5
STATIC STATUSC6	:= 6
STATIC PEDVEN	:= 7
STATIC ITPEDVEN	:= 8

/*/{Protheus.doc} ADMVP02
Programa responsavel pelas tratativas de aloca��es de pedidos
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@param [llIsJob], logical, Variavel para controlar se .T. processamento em JOB ou .F. Processamento com interface com usuario. Padrao = isBlind()
@type function
/*/
User Function ADMVP02( nlOpc, uParam, llIsJob )

	local uRet		:= nil
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )

	private lpInJob	:= .F.	

	default nlOpc	:= 0
	default uParam	:= nil
	default llIsJob	:= isBlind()
	
	conOut( "..:: DS2U ::.. >> BEGIN ADMVP02" )

	if ( llExecADMV )
	
		lpInJob := llIsJob
		
		do case
		
			case nlOpc == 0
				uRet := stepsAloc( uParam )
				
			case nlOpc == 1
				uRet := desalocar( uParam )
				
			case nlOpc == 2
				uRet := queueAloc( uParam )
				
			case nlOpc == 3
				uRet := alocar( uParam )
				
			case nlOpc == 4
				uRet := alocaZCI( uParam )
				
			case nlOpc == 5
				uRet := alocByCan( uParam )
				
			case nlOpc == 6
				uRet := alocTransf( uParam )
				
			case nlOpc == 7
				uRet := showDispEs( uParam )
				
			case nlOpc == 8
				uRet := chkSituC6( uParam )
				
			case nlOpc == 9
				uRet := cancelPV( { SC5->C5_FILIAL, SC5->C5_NUM } )
				
		endCase
	
	else
		conOut( "..:: DS2U ::.. >> REGRAS ADMV DESLIGADAS - PARAMETRO ES_ADMVON" )
	endif
	
	conOut( "..:: DS2U ::.. >> END ADMVP02" )

Return uRet

/*/{Protheus.doc} showDispEs
Interface de consulta de disponibilidade de estoque
@author DS2U (SDA)
@since 09/01/2019
@version 1.0

@type function
/*/
Static Function showDispEs( clProduto )

	local oDlg
	local oLbx
	local alDisp	:= {}
	local nlPos1	:= 590
	local nlPos2	:= 690
	local nlPos3	:= 340
	local nlPos4	:= 270
	local nlPos5	:= 280
	local nlPos6	:= 320
	
	default clProduto := SB1->B1_COD
	
	fwMsgRun(,{|oSay| alDisp := getDisp( clProduto ) },"Aguarde...","Consultando disponibilidade de estoque..." )

	DEFINE DIALOG oDlg TITLE "Disponibilidade de Estoque - " + clProduto FROM 0,0 TO nlPos1,nlPos2 PIXEL
	
		@ 005,005 LISTBOX oLbx FIELDS HEADER "Data", "Estoque", "Alocado", "Reserva", "Pendente", "Disp Data", "Dispon�vel" OF oDlg PIXEL SIZE nlPos3, nlPos4  
		
		oLbx:setArray( alDisp )
		
		oLbx:bLine := {|| {;
							alDisp[oLbx:nAt][1],;
							alDisp[oLbx:nAt][2],;
							alDisp[oLbx:nAt][3],;
							alDisp[oLbx:nAt][4],;
							alDisp[oLbx:nAt][5],;
							alDisp[oLbx:nAt][6],;
							alDisp[oLbx:nAt][7];
						  }}
		
		DEFINE SBUTTON FROM nlPos5,nlPos6 TYPE 1 ENABLE OF oDlg ACTION oDlg:End()
	
	ACTIVATE DIALOG oDlg CENTERED

Return

/*/{Protheus.doc} getDisp
Funcao auxiliar para alimentar o array de disponibilidade de produto
@author DS2U (SDA)
@since 09/01/2019
@version 1.0
@return alDisp, Array com a disponibilidade de estoque do produto enviado por parametro
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function getDisp( clProduto )

	local alArea	:= getArea()
	local alDisp	:= {}
	local alDispon	:= {}
	local alDisAux	:= {}
	local clAlias	:= ""
	local clAliasSB2:= ""
	local nlQtdDisp	:= 0
	
	default clProduto := ""
	
	//-----------------------------------------
	// Trata conteudo para utilizacao no seek -
	//-----------------------------------------
	clProduto := PADR( clProduto, tamSX3( "B1_COD" )[1] )
	
	//------------------------------
	// Identifica saldo disponivel -
	//------------------------------
	dbSelectArea( "SB1" )
	SB1->( dbSetOrder( 1 ) )
	
	if ( SB1->( dbSeek( fwxFilial( "SB1" ) + clProduto ) ) )
	
		dbSelectArea( "SB2" )
		SB2->( dbSetOrder( 1 ) )
		
		clAliasSB2 := getLocais( clProduto )
		
		while ( .not. ( clAliasSB2 )->( eof() ) )
		
			//------------------------------------------------------------------------------------------------------------------------
			// Soma o saldo de todos os armazens envolvidos, somando ao saldoSB2 os produtos que estao em pedido de venda B2_RESERVA -
			//------------------------------------------------------------------------------------------------------------------------
			if ( SB2->( dbSeek( fwxFilial( "SB2" ) + clProduto + ( clAliasSB2 )->ARMAZEM ) ) )
				nlQtdDisp += ( saldoSB2() + ( clAliasSB2 )->RESERVA )
			endif
			
			( clAliasSB2 )->( dbSkip() )
		endDo
		( clAliasSB2 )->( dbCloseArea() )
	
	endif
	
	AADD( alDisp, { "Imediato", 0, 0, 0, 0, 0, 0 } )
	
	clAlias := saldoFutur( clProduto )
	
	while ( .not. ( clAlias )->( eof() ) )
	
		//------------------------------------------
		// Tratamento para dados em estoque fisico -
		//------------------------------------------
		
		if ( nlQtdDisp > 0 )
			alDisp[1][2] := nlQtdDisp
			nlQtdDisp := 0
		endif
		
		if ( empty( ( clAlias )->DATADISP ) .or. sToD( ( clAlias )->DATADISP ) <= date() )
		
			if ( len( alDisp ) > 0 )
				alDisp[1][3] += ( clAlias )->ALOCADO
				alDisp[1][4] += ( clAlias )->RESERVA
				alDisp[1][5] += ( clAlias )->PENDENTE
			endif
			
		else
		
			//----------------------------------------------------------
			// Tratamento para coluna Disp. Data ( Estoque - Alocado ) -
			//----------------------------------------------------------
			setDispData( @alDisp )
			
			//------------------------------------
			// Tratamento para coluna Disponivel -
			//------------------------------------
			setDisponi( @alDisp )
		
			//------------------------------------------
			// Tratamento para dados em estoque futuro -
			//------------------------------------------
			AADD( alDisp, { sToD( ( clAlias )->DATADISP ), ( clAlias )->QTDISP, ( clAlias )->ALOCADO, ( clAlias )->RESERVA, ( clAlias )->PENDENTE, 0	, 0 } )
		endif
		
		//----------------------------------------------------------
		// Tratamento para coluna Disp. Data ( Estoque - Alocado ) -
		//----------------------------------------------------------
		setDispData( @alDisp )
		
		//------------------------------------
		// Tratamento para coluna Disponivel -
		//------------------------------------
		setDisponi( @alDisp )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	//----------------------------------------------------------------------
	// Tratamento em caso de n�o ter encontrado disponibilidade em estoque -
	//----------------------------------------------------------------------
	if ( len( alDisp ) == 0 )
		AADD( alDisp, { "", 0, 0, 0, 0, 0, 0 } )
	endif

	restArea( alArea )

Return alDisp

/*/{Protheus.doc} setDispData
Funcao auxiliar para controlar a regra de calculo da coluna Disponivel na Data (Disp. Data)
@author DS2U (SDA)
@since 22/01/2019
@version 1.0
@param alDisp, array of logical, Array de disponibildiade a ser alterado
@type function
/*/
Static Function setDispData( alDisp )
		
	//----------------------------------------------------------
	// Tratamento para coluna Disp. Data ( Estoque - Alocado ) -
	//----------------------------------------------------------
	alDisp[len( alDisp )][6] := ( alDisp[len( alDisp )][2] - alDisp[len( alDisp )][3] )
	
Return

/*/{Protheus.doc} setDisponi
Funcao auxiliar para controlar a regra de calculo da coluna Disponivel
@author DS2U (SDA)
@since 22/01/2019
@version 1.0
@param alDisp, array of logical, Array de disponibildiade a ser alterado
@type function
/*/
Static Function setDisponi( alDisp )
		
	local nlPos		:= 0
	
	//------------------------------------
	// Tratamento para coluna Disponivel -
	//------------------------------------
	nlPos := len( alDisp )
	if ( nlPos > 1 )
		nlPos := ( nlPos - 1 )
	endif
	alDisp[len( alDisp )][7] := ( alDisp[nlPos][7] + alDisp[len( alDisp )][6] )
	
Return

/*/{Protheus.doc} getLocais
Funcao responsavel por consultar quais armazens existem para o produto enviado por parametroo, aglutinando a quantidade em pedido de venda B2_RESERVA
@author DS2U (SDA)
@since 17/01/2019
@version 1.0
@return clAlias, Alias da consulta
@param clProduto, characters, Codigo do produto a ser consultado
@type function
/*/
Static Function getLocais( clProduto )

	local clAlias	:= getNextAlias()
	
	default clProduto	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			B2_LOCAL AS ARMAZEM
			, SUM( B2_RESERVA ) AS RESERVA
			
		FROM
			%TABLE:SB2% SB2
			
		WHERE
			B2_FILIAL = %XFILIAL:SB2%
			AND B2_COD = %EXP:clProduto%
			AND SB2.%NOTDEL%
			
		GROUP BY B2_LOCAL
	
	ENDSQL

Return clAlias

/*/{Protheus.doc} saldoFutur
Query para consulta de saldo futuro
@author DS2U (SDA)
@since 14/01/2019
@version 1.0
@return clAlias, Alias da consulta de saldo futuro
@param clProduto, characters, Codigo do produto a ser pesquisado
@type function
/*/
Static Function saldoFutur( clProduto )

	local clAlias	:= getNextAlias()
	
	default clProduto := ""
	
	conOut( "..:: DS2U ::.. >> BEGIN saldoFutur" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			COALESCE( ZCK_DTCLPO, '' ) AS DATADISP
			, B1_COD AS PRODUTO
			, SUM( COALESCE( EIC.QTOTDISP, 0 ) ) AS QTDISP
			, SUM( COALESCE( ALC.QTD, 0 ) ) AS ALOCADO
			, SUM( COALESCE( EST.QTD, 0 ) ) AS RESERVA // ESTIMADO
			, SUM( COALESCE( ESTC.QTD, 0 ) ) AS PENDENTE // ESTIMA CANCELAMENTO
			
		FROM
			%TABLE:SB1% SB1
		
		LEFT JOIN
			(
			SELECT
				SW2.W2_FILIAL AS FILIAL			
				, SW2.W2_PO_NUM AS PO				
				, SW3.W3_COD_I AS PRODUTO
				, SW3.W3_POSICAO AS ITEM_PO
				, SW3.W3_FABR AS FABRICANTE
				, SW3.W3_FABLOJ AS LOJAFAB
				, SUM( SW3.W3_QTDE ) AS QTOTDISP	
								
			FROM 
				%TABLE:SW2% SW2 
					
			INNER JOIN 
				%TABLE:SW3%  SW3 ON
				SW3.W3_FILIAL = SW2.W2_FILIAL	
				AND SW3.W3_PO_NUM = SW2.W2_PO_NUM
				AND SW3.W3_TEC = ' '
				AND SW3.%NOTDEL%
				
			INNER JOIN 
				%TABLE:SW0% SW0 ON
				SW0.W0_FILIAL = SW3.W3_FILIAL	
				AND SW0.W0__NUM = SW3.W3_SI_NUM
				AND SW0.%NOTDEL% 
				
			INNER JOIN 
				%TABLE:SC7% SC7 ON
				SC7.C7_FILIAL = SW2.W2_FILIAL
				AND SC7.C7_PO_EIC = SW2.W2_PO_NUM
				AND SC7.C7_ITEM = SW3.W3_POSICAO
				AND SC7.C7_FORNECE = SW3.W3_FABR
				AND SC7.C7_LOJA = SW3.W3_FABLOJ
				AND C7_QUANT > C7_QUJE
				AND SC7.%NOTDEL%
				
			WHERE
				SW2.%NOTDEL% 
		
			GROUP BY 
				SW2.W2_FILIAL			
				, SW2.W2_PO_NUM				
				, SW3.W3_COD_I
				, SW3.W3_POSICAO
				, SW3.W3_FABR
				, SW3.W3_FABLOJ
		
			) EIC ON
			EIC.FILIAL = %XFILIAL:SW2%
			AND EIC.PRODUTO = SB1.B1_COD
			
		LEFT JOIN 
				%TABLE:ZCK% ZCK ON
				ZCK.ZCK_FILIAL = EIC.FILIAL
				AND ZCK.ZCK_PO = EIC.PO
				AND ZCK.%NOTDEL%
		
		LEFT JOIN
			(
		
				SELECT 
					ZCI_FILIAL
					, ZCI_PRODUT
					, SUM( ZCI_QTALOC ) AS QTD 
				FROM 
					%TABLE:ZCI% ZCI 
				WHERE 
					ZCI.ZCI_PEDCOM = ' ' 
					AND ZCI.%NOTDEL%
		
				GROUP BY
					ZCI_FILIAL
					, ZCI_PRODUT
		
			) ALC ON // ALOCADO
			ALC.ZCI_FILIAL = %XFILIAL:ZCI%
			AND ALC.ZCI_PRODUT = SB1.B1_COD
			
		LEFT JOIN
			(
		
				SELECT 
					ZCI_FILIAL
					, ZCI_PRODUT
					, SUM( ZCI_QTALOC ) AS QTD 
				FROM 
					%TABLE:ZCI% ZCI 
		
				INNER JOIN
					%TABLE:SC6% SC6 ON
					SC6.C6_FILIAL = ZCI.ZCI_FILIAL
					AND SC6.C6_NUM = ZCI.ZCI_PEDVEN
					AND SC6.C6_ITEM = ZCI.ZCI_ITEMPV
					AND SC6.C6_PRODUTO = ZCI.ZCI_PRODUT
					AND SC6.%NOTDEL%
					
				WHERE 
					ZCI.ZCI_PEDCOM <> ' '
					AND ZCI.%NOTDEL%
		
				GROUP BY
					ZCI_FILIAL
					, ZCI_PRODUT
		
			) EST ON // ESTIMADO
			EST.ZCI_FILIAL = %XFILIAL:ZCI%
			AND EST.ZCI_PRODUT = SB1.B1_COD
		
		LEFT JOIN
			(
		
				SELECT 
					C6_FILIAL
					, C6_PRODUTO
					, SUM( C6_QTDVEN ) AS QTD 
				FROM 
					%TABLE:SC6% SC6 
		
				LEFT JOIN
					%TABLE:ZCI% ZCI ON
					SC6.C6_FILIAL = ZCI.ZCI_FILIAL
					AND SC6.C6_NUM = ZCI.ZCI_PEDVEN
					AND SC6.C6_ITEM = ZCI.ZCI_ITEMPV
					AND SC6.C6_PRODUTO = ZCI.ZCI_PRODUT
					AND ZCI.ZCI_PEDCOM <> ' '
					AND ZCI.%NOTDEL%
					
				WHERE 
					C6_XSTALOC = 'EC'
					AND SC6.%NOTDEL%			
		
				GROUP BY
					C6_FILIAL
					, C6_PRODUTO
		
			) ESTC ON // ESTIMA CANCELAMENTO
			ESTC.C6_FILIAL = %XFILIAL:SC6%
			AND ESTC.C6_PRODUTO = SB1.B1_COD
			
		WHERE
			B1_FILIAL = %XFILIAL:SB1%
			AND B1_COD = %EXP:clProduto% 
			AND SB1.%NOTDEL%
			
		GROUP BY COALESCE( ZCK_DTCLPO, '' ), B1_COD
			
		ORDER BY COALESCE( ZCK_DTCLPO, '' )
			
	ENDSQL
	
	conOut( "..:: DS2U ::.. >> END saldoFutur" )

Return clAlias

/*/{Protheus.doc} alocaZCI
Funcao criada para tornar externo a fun��o padr�o de inclusao de registro na tabela de aloca��es
@author DS2U (SDA)
@since 31/10/2018
@version 1.0
@param alParam, array of logical, Array no seguinte formato	alParam[1] = Codigo do pedido de compras
																alParam[2] = Item do pedido de compras
																alParam[3] = Flag para identificar se trata dados via begin transaction.
@type function
/*/
Static Function alocaZCI( alParam )

	local llIsParam	:= valType( alParam ) == "A"
	local clFilial	:= iif( llIsParam .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clPedido	:= iif( llIsParam .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clItemPed	:= iif( llIsParam .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clPedCom	:= iif( llIsParam .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clItPedCom:= iif( llIsParam .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )
	local nlQtd		:= iif( llIsParam .and. len( alParam ) > 5 .and. valType( alParam[6] ) == "N", alParam[6], 0 )
	local dlDtEntr	:= iif( llIsParam .and. len( alParam ) > 6 .and. valType( alParam[7] ) == "D", alParam[7], cToD("//") )
	local clCanal	:= iif( llIsParam .and. len( alParam ) > 7 .and. valType( alParam[8] ) == "C", alParam[8], "" )
	local clProduto	:= iif( llIsParam .and. len( alParam ) > 8 .and. valType( alParam[9] ) == "C", alParam[9], "" )

	alocaC6( clFilial, clPedido, clItemPed, clPedCom, clItPedCom, nlQtd, dlDtEntr, clCanal, clProduto )
	
Return

/*/{Protheus.doc} alocar
Prepara ambiente de aloca��o e executa os passos para aloca��es de pedido
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@type function
/*/
Static Function alocar()

	local clAliasItn := ""
	
	conOut( "..:: DS2U ::.. >> BEGIN alocar" )
	
	//----------------------------------------------------------------
	// Listar os itens de pedido de vendas que precisam ser alocados -
	//----------------------------------------------------------------
	clAliasItn := getC6ToAlc()
	
	if ( .not. empty( clAliasItn ) )

		while ( .not. ( clAliasItn )->( eof() ) )
		
			//-------------------------------------------------------------------------
			// Adiciona item do pedido de venda na fila de processamento de aloca��es -
			//-------------------------------------------------------------------------
			queueAloc(	{"A",; 
						( clAliasItn )->FILIAL,;
						( clAliasItn )->PEDIDO,;
						( clAliasItn )->ITEMPEDIDO,;
						( clAliasItn )->PRODUTO,;
						( clAliasItn )->QUANTIDADE,;
						( clAliasItn )->PRIORIDADE,;
						sToD( ( clAliasItn )->DTEMISSAO ),;
						( clAliasItn )->HREMISSAO,;
						sToD( ( clAliasItn )->DTENTREGA ) } ) // Acrescenta item na fila para desaloca��o
		
			( clAliasItn )->( dbSkip() )
		endDo
		( clAliasItn )->( dbCloseArea() )
	
	endif
	
	conOut( "..:: DS2U ::.. >> END alocar" )
		
Return

/*/{Protheus.doc} execAloca
Executa passo a passo do processo de alocacoes
@author DS2U (SDA)
@since 07/09/2018
@version 1.0

@type function
/*/
Static Function execAloca()

	local clAliasItn	:= ""
	local alDispon		:= {}
	local dlDtEntr		:= cToD("//")
	local alArea		:= getArea()
	
	conOut( "..:: DS2U ::.. >> BEGIN execAloca" )

	//----------------------------------------------------------------
	// Listar os itens de pedido de vendas que precisam ser alocados -
	//----------------------------------------------------------------
	clAliasItn := getQueueA()
	
	if ( .not. empty( clAliasItn ) )
	
		while ( .not. ( clAliasItn )->( eof() ) )
			
			conOut( "Processando produto [" + ( clAliasItn )->PRODUTO + "]" )			
			conOut( "Prioridade [" + ( clAliasItn )->PRIORIDADE + "]" )
			conOut( "Data Emissao do item [" + ( clAliasItn )->DTEMISSAO + "]" )
			conOut( "Hora Emissao do item [" + ( clAliasItn )->HREMISSAO + "]" )
			conOut( "Data de entrega do item [" + ( clAliasItn )->DTENTREGA + "]" )
			conOut( "Nr. Pedido [" + ( clAliasItn )->PEDIDO + "]" )
			conOut( "Item do Pedido [" + ( clAliasItn )->ITEMPEDIDO + "]" )
			
			//-----------------------------------------------
			// Muda status do registro para P = Processando -
			//-----------------------------------------------
			setStZCQ( "P", ( clAliasItn )->RECZCQ )
			
			if ( .not. empty( ( clAliasItn )->DTENTREGA ) )
				dlDtEntr := sToD( ( clAliasItn )->DTENTREGA )
			endif
			
			//------------------------------------------------------------------------------------------
			// Verifica se existe registro relacionado ao produto + local na SB2. Se n�o existir, cria -
			//------------------------------------------------------------------------------------------
			U_ADMVP01( 5, { ( clAliasItn )->PRODUTO, ( clAliasItn )->CANAL } )
			
			//-----------------------------------------------------------------------
			// Busca nos pedidos de compras a previs�o de data / quantidade do item -
			//-----------------------------------------------------------------------
			alDispon := getDispon( ( clAliasItn )->PEDIDO, ( clAliasItn )->PRODUTO, ( clAliasItn )->QUANTIDADE, dlDtEntr, ( clAliasItn )->CANAL )
			
			//--------------------------------------------------------
			// Grava aloca��es conforme a disponibilidade do produto -
			//--------------------------------------------------------
			if ( len( alDispon ) > 0 )
			
				//------------------------------------------------------------------------------------------
				// Verifica se existe registro relacionado ao produto + local na SB2. Se n�o existir, cria -
				//------------------------------------------------------------------------------------------
				aEval( alDispon, {|x| U_ADMVP01( 5, { ( clAliasItn )->PRODUTO, x[CANALVND] } ) } )
				
				setAloca( alDispon, ( clAliasItn )->FILIAL, ( clAliasItn )->PEDIDO, ( clAliasItn )->ITEMPEDIDO, ( clAliasItn )->PRODUTO )

			//-----------------------------------------------------------------------------------
			// Se nao encontrou disponibilidade, altera status do item para Estima Cancelamento -
			//-----------------------------------------------------------------------------------
			else
				setSituSC6( ( clAliasItn )->FILIAL, dlDtEntr, ( clAliasItn )->PEDIDO, ( clAliasItn )->ITEMPEDIDO, ( clAliasItn )->PRODUTO, ( clAliasItn )->QUANTIDADE, "EC", ( clAliasItn )->CANAL )
			endif
			
			//----------------------------------------------
			// Muda status do registro para F = Finalizado -
			//----------------------------------------------
			setStZCQ( "F", ( clAliasItn )->RECZCQ )	
			
			( clAliasItn )->( dbSkip() )
		endDo
		( clAliasItn )->( dbCloseArea() )
	
	endif
	
	restArea( alArea )	
	
	conOut( "..:: DS2U ::.. >> END execAloca" )

Return

/*/{Protheus.doc} setAloca
Realiza aloca��es conforme consulta de disponibilidade do produto
@author DS2U (SDA)
@since 12/09/2018
@version 1.0
@param alDispon, array com informa��es de disponibilidade do produto	alDispon[1] = Data disopnivel
																		alDispon[2] = Quantidade disponivel
																		alDispon[3] = Pedido de compra
																		alDispon[4] = Item do pedido de compras
																		alDispon[5] = Codigo do Canal
																		alDispon[6] = Codigo da legenda do item

@param clFilial, characters, Codigo da filial
@param clPedido, characters, Codigo do pedido de venda
@param clItemPed, characters, Codigo do item do pedido de vendas 
@param clProduto, characters, Codigo do produto
@return llProcOk, se .T., processamento ocorreu com sucesso
@type function
/*/
Static Function setAloca( alDispon, clFilial, clPedido, clItemPed, clProduto )
				
	local nlx			:= 1
	local llProcOk		:= .T.
	local clMsgLog		:= ""
	
	default clFilial	:= ""
	default alDispon	:= {}
	
	conOut( "..:: DS2U ::.. >> BEGIN setAloca" )
 
	//-----------------------------------------------------------------------------------------------------------------------------------------
	// Caso tenha encontrado mais de 1 disponibilidade, o item do pedido de venda precisar� ser quebrado por disponibilidade / status do item -
	//-----------------------------------------------------------------------------------------------------------------------------------------
	if ( len( alDispon ) > 1 )
	
		begin transaction
		
			//-----------------------------------
			// Altera itens do pedido de vendas -
			//-----------------------------------
			llProcOk := changePed( clPedido, clItemPed, @alDispon, @clMsgLog )
			
			if ( llProcOk )
			
				for nlx := 1 to len( alDispon )
			
					if (  alDispon[nlx][STATUSC6] == "EC" ) // Se nao foi achado disponibilidade de produto, seta o item do pedido de venda para Estima Cancelamento
						llProcOk := setSituSC6( clFilial, alDispon[nlx][DTDISPO], alDispon[nlx][PEDVEN], alDispon[nlx][ITPEDVEN], clProduto, alDispon[nlx][QTDISPO], alDispon[nlx][STATUSC6],  alDispon[nlx][CANALVND] )
					else
						alocaC6( clFilial, alDispon[nlx][PEDVEN], alDispon[nlx][ITPEDVEN], alDispon[nlx][PEDCOM], alDispon[nlx][ITPEDCOM], alDispon[nlx][QTDISPO], alDispon[nlx][DTDISPO], alDispon[nlx][CANALVND], clProduto )
						llProcOk := setSituSC6( clFilial, alDispon[nlx][DTDISPO], clPedido, clItemPed, clProduto, alDispon[nlx][QTDISPO], alDispon[nlx][STATUSC6],  alDispon[nlx][CANALVND] )
					endif

				next nlx
			
			else
				disarmTransaction()
			endif
		
		end transaction
	
	else
 
		begin transaction
			
			if (  alDispon[nlx][STATUSC6] == "EC" ) // Se nao foi achado disponibilidade de produto, seta o item do pedido de venda para Estima Cancelamento
				llProcOk := setSituSC6( clFilial, alDispon[nlx][DTDISPO], clPedido, clItemPed, clProduto, alDispon[nlx][QTDISPO], alDispon[nlx][STATUSC6],  alDispon[nlx][CANALVND] )
			else
				alocaC6( clFilial, clPedido, clItemPed, alDispon[nlx][PEDCOM], alDispon[nlx][ITPEDCOM], alDispon[nlx][QTDISPO], alDispon[nlx][DTDISPO], alDispon[nlx][CANALVND], clProduto )
				llProcOk := setSituSC6( clFilial, alDispon[nlx][DTDISPO], clPedido, clItemPed, clProduto, alDispon[nlx][QTDISPO], alDispon[nlx][STATUSC6],  alDispon[nlx][CANALVND] )
			endif
		
		end transaction
		
	endif
	
	//----------------------------
	// Trata libera��o de pedido -
	//----------------------------
	if ( llProcOk )
		chkSC9( clFilial, clPedido, clItemPed, clProduto )
	endif
	
	if ( .not. empty( clMsgLog ) )
		msgAlert( clMsgLog )
	endif
	
	conOut( "..:: DS2U ::.. >> END setAloca" )
	
Return llProcOk

/*/{Protheus.doc} chkSC9
Funcao auxiliar para tratar libera��o de pedido de venda
@author DS2U (SDA)
@since 24/01/2019
@version 1.0
@param clFilial, characters, Codigo da filial
@param clPedido, characters, Codigo do pedido de venda
@param clItemPed, characters, Codigo do item do pedido de venda
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function chkSC9( clFilial, clPedido, clItemPed, clProduto )

	local alArea	:= {}
	local nlQtdLib	:= 0
	
	default clFilial	:= fwxFilial( "SC6" )
	default clPedido	:= ""
	default clItemPed	:= ""
	default clProduto	:= ""
	
	dbSelectArea( "SC9" )
	dbSelectArea( "SC6" )

	alArea := getArea()
	SC9->( dbSetOrder( 1 ) ) // C9_FILIAL, C9_PEDIDO, C9_ITEM, C9_SEQUEN, C9_PRODUTO, C9_BLEST, C9_BLCRED, R_E_C_N_O_, D_E_L_E_T_
	SC6->( dbSetOrder( 1 ) ) // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO, R_E_C_N_O_, D_E_L_E_T_

	if ( SC6->( dbSeek( clFilial + clPedido + clItemPed + clProduto   ) ) )
		
		//-----------------------------------------------
		// Atualizo as liberacoes do item, caso existam -
		//-----------------------------------------------
		if ( SC9->( dbSeek( clFilial + clPedido + clItemPed ) ) )
		
			nlQtdLib := SC9->C9_QTDLIB
			
			while (	.not. SC9->( eof() );
					.and. SC9->C9_PEDIDO == SC6->C6_NUM;
					.and. SC9->C9_ITEM == SC6->C6_ITEM;
				   )
				  
				if ( empty( SC9->C9_NFISCAL ) )
					SC9->( a460Estorna( .T. ) )
				endif
				
				SC9->( dbSkip() )
			endDo
			
			//-----------
			// Gera SC9 -
			//-----------
			MaLibDoFat( SC6->( recNo() ), nlQtdLib,.T.,.T., .F., .F. )
			
		endif
		
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} changePed
Responsavel por alterar o item do pedido de venda conforme sua disponibilidade de entrega
@author DS2U (SDA)
@since 26/09/2018
@version 1.0
@return llProcOk, Se .T., a altera��o do pedido de venda foi alterado com sucesso
@param clPedido, characters, Codigo do pedido de vendas que sera alterado
@param clItemPed, characters, Item do pedido de venda que sera alterado
@param alDispon, array of logical, Array passado por refer�ncia com informa��es de disponibilidade
@param clMsgErro, characters, Variavel passado por refer�ncia que armazena a mensagem de log em caso de erro
@type function
/*/
Static Function changePed( clPedido, clItemPed, alDispon, clMsgErro )

	local alArea	:= getArea()
	local alCab		:= {}
	local alItens	:= {}
	local alItem	:= {}
	local nlx
	local nly
	local llProcOk	:= .T.
	local nlTotItens:= 0
	local alItnAux	:= {}
	local alErroAuto:= {}
	
	private lMsErroAuto	:= .F.
	private lMsHelpAuto    := .T.
	private lAutoErrNoFile := .T. 
	
	default clMsgErro	:= ""
	default alDispon	:= {}
	
	conOut( "..:: DS2U ::.. >> BEGIN changePed" )
	
	if ( len( alDispon ) > 1 )
	
		dbSelectArea( "SC5" )
		nlTamSC5 := SC5->( fCount() )
		SC5->( dbSetOrder( 1 ) )
		
		dbSelectArea( "SC6" )
		SC6->( dbSetOrder( 1 ) ) // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO
		
		if ( SC5->( dbSeek( fwxFilial( "SC5" ) + clPedido ) ) )
		
			for nlx := 1 to nlTamSC5
				if ( .not. empty( SC5->&( fieldName( nlx ) ) ) )
					AADD( alCab,{ SC5->( fieldName( nlx ) ), SC5->&( fieldName( nlx ) ), nil } )
				endif
			next nlx
			
			//----------------------------------------------
			// Ordena o array conforme dicionario de dados -
			//----------------------------------------------
			fwVetByDic( alCab, "SC5")
		
		    if ( SC6->( dbSeek( fwxFilial( "SC6" ) + clPedido ) ) )
		    	
		    	clIdSeq := getMaxItns( clPedido )
		    
		    	while ( .not. SC6->( eof() ); 
		    		  	.and. SC6->C6_FILIAL == fwxFilial( "SC6" );
		    		  	.and. SC6->C6_NUM == clPedido;
		    		  )
		    	
		    		alItem := {}
		    	
		    		//--------------------------------------------------------------------------------------------------
		    		// Se for o item que precisa ser alterado, usa o array de disponibilidade para alimentar os campos -
		    		// C6_QTDVEN / C6_ENTREG / C6_XSTALOC / C6_VALOR                                                   -
		    		//--------------------------------------------------------------------------------------------------
		    		if ( SC6->C6_ITEM == clItemPed )
		    		
		    			for nly := 1 to len( alDispon )
		    		
							setSC6ExAu( @alItem, "C6_FILIAL", SC6->C6_FILIAL )
							if ( nly > 1 )
								clIdSeq := soma1( clIdSeq )
								setSC6ExAu( @alItem, "C6_ITEM", clIdSeq )
							else
								setSC6ExAu( @alItem, "C6_ITEM", SC6->C6_ITEM )
							endif
							setSC6ExAu( @alItem, "C6_PRODUTO", SC6->C6_PRODUTO )
							setSC6ExAu( @alItem, "C6_TES", SC6->C6_TES )
							setSC6ExAu( @alItem, "C6_QTDVEN", alDispon[nly][QTDISPO] )
							setSC6ExAu( @alItem, "C6_PRCVEN", SC6->C6_PRCVEN )
							setSC6ExAu( @alItem, "C6_LOCAL", alDispon[nly][CANALVND] )
							setSC6ExAu( @alItem, "C6_PRUNIT", SC6->C6_PRUNIT )
							setSC6ExAu( @alItem, "C6_ENTREG", alDispon[nly][DTDISPO] )
							setSC6ExAu( @alItem, "C6_XSTALOC", alDispon[nly][STATUSC6] )
		    		
			    			AADD( alDispon[nly], clPedido )
			    			
			    			//----------------------------------------------
							// Ordena o array conforme dicionario de dados -
							//----------------------------------------------
			    			fwVetByDic( alItem, "SC6")
			    			
			    			if ( nly == 1 )
			    				AADD( alDispon[nly], clItemPed )
			    				AADD( alItens, alItem )
			    			else
			    				AADD( alDispon[nly], clIdSeq )
			    				AADD( alItnAux, aClone( alItem ) )
			    			endif
			    			
			    			alItem := {}
			    			
			    		next nly
		    		
		    		else
		    			
		    			setSC6ExAu( @alItem, "C6_FILIAL", SC6->C6_FILIAL )
						setSC6ExAu( @alItem, "C6_ITEM", SC6->C6_ITEM )
						setSC6ExAu( @alItem, "C6_PRODUTO", SC6->C6_PRODUTO )
						setSC6ExAu( @alItem, "C6_TES", SC6->C6_TES )
						setSC6ExAu( @alItem, "C6_QTDVEN", SC6->C6_QTDVEN )
						setSC6ExAu( @alItem, "C6_PRCVEN", SC6->C6_PRCVEN )
						setSC6ExAu( @alItem, "C6_PRUNIT", SC6->C6_PRUNIT )
						setSC6ExAu( @alItem, "C6_ENTREG", SC6->C6_ENTREG )
						setSC6ExAu( @alItem, "C6_XSTALOC", SC6->C6_XSTALOC )
		    			
		    			//----------------------------------------------
						// Ordena o array conforme dicionario de dados -
						//----------------------------------------------
		    			fwVetByDic( alItem, "SC6")
		    			AADD( alItens, alItem )
		    			
		    		endif
		    		
		    		SC6->( dbSkip() )
		    	endDo
		    
		    endif
	
		endif
		
		//------------------------------------------------------------------------------------
		// Adiciona nos itens do pedido de venda, as quebras por disponibilidade de produto. -
		// � feito neste momento para obedecer a sequencia dos itens, que � no final.        -
		//------------------------------------------------------------------------------------
		if ( len( alItnAux ) > 0 )
			
			for nlx := 1 to len( alItnAux )
			
				alItem := aClone( alItnAux[nlx] )
				
				//----------------------------------------------
				// Ordena o array conforme dicionario de dados -
				//----------------------------------------------
				fwVetByDic( alItem, "SC6")
				
				AADD( alItens, aClone( alItem ) )
				
			next nlx
			
		endif
		
		msExecAuto( { |x,y,z| MATA410(x,y,z)}, alCab, alItens, 4 )
		
		if ( lMsErroAuto )
		
			llProcOk := .F.
			
			alErroAuto := getAutoGRLog()
			aEval( alErroAuto, {|x| clMsgErro += allTrim( x ) + '<br/>'})
								
			conout("Falha na altera��o do pedido [" + clPedido + "]")
			conout( clMsgErro )
			
		else
			conout("Pedido [" + clPedido + "] alterado com sucesso!")			
		endif
		
	endif
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END changePed" )

Return llProcOk

/*/{Protheus.doc} setSC6ExAu
Controle de regras para adicionar campos no item da execauto MATA410 de pedidos de vendas
@author DS2U (SDA)
@since 28/09/2018
@version 1.0
@param alItem, array of logical, array com o item atual em montagem para execauto do MATA410
@param clField, characters, Nome do campo da SC6
@param uContent, undefined, Conteudo do campo da SC6
@type function
/*/
Static Function setSC6ExAu( alItem, clField, uContent, llValid )

	default llValid	:= nil

	conOut( "..:: DS2U ::.. >> BEGIN setSC6ExAu" )

	if ( .not. empty( uContent ) )
		AADD( alItem,{ clField, uContent, llValid } )
	endif
	
	conOut( "..:: DS2U ::.. >> END setSC6ExAu" )
	
Return

/*/{Protheus.doc} getMaxItns
Responsavel por contar a quantidade de itens e o maior id do sequencial de itens do pedido de venda
@author sergi
@since 26/09/2018
@version 1.0
@return nlQtLin, Quantidade
@param clPedido, characters, Codigo do pedido de venda
@param clFilial, characters, Codigo da filial
@type function
/*/
Static Function getMaxItns( clPedido, clFilial )

	local alArea	:= getArea()
	local clAlias	:= getNextAlias()
	local clMaxId	:= strZero( 1, tamSX3("C6_ITEM")[1] )
	
	default clPedido	:= ""
	default clFilial	:= fwxFilial( "SC6" )
	
	conOut( "..:: DS2U ::.. >> BEGIN getMaxItns" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			MAX( C6_ITEM ) AS MAXITEM
			
		FROM
			%TABLE:SC6% SC6
			
		WHERE
			SC6.C6_FILIAL = %EXP:clFilial%
			AND SC6.C6_NUM = %EXP:clPedido%
			AND SC6.%NOTDEL%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		clMaxId := ( clAlias )->MAXITEM
	endif
	( clAlias )->( dbCloseArea() )

	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END getMaxItns" )

Return clMaxId

/*/{Protheus.doc} alocaC6
Responsavel por controlar a inclusao de registro na tabela de aloca��es
@author DS2U (SDA)
@since 12/09/2018
@version 1.0
@param clFilial, characters, Codigo da filial
@param clPedido, characters, Codigo do pedido de vendas
@param clItemPed, characters, Codigo do item do pedido de vendas
@param clPedCom, characters, Codigo do Pedido de compras
@param clItPedCom, characters, Codigo do item do pedido de compras
@param nlQtd, numeric, Quantidade a ser alocada
@param dlDtEntr, date, Data prevista  de entrega
@param clCanal, characters, Codigo do canal / armazen de onde esta sendo alocado
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function alocaC6( clFilial, clPedido, clItemPed, clPedCom, clItPedCom, nlQtd, dlDtEntr, clCanal, clProduto )

	conOut( "..:: DS2U ::.. >> BEGIN alocaC6" )
	
	if ( recLock( "ZCI", .T. ) )
	
		ZCI->ZCI_FILIAL	:= clFilial
		ZCI->ZCI_PEDVEN	:= clPedido
		ZCI->ZCI_ITEMPV	:= clItemPed
		ZCI->ZCI_PRODUT	:= clProduto
		ZCI->ZCI_PEDCOM	:= clPedCom
		ZCI->ZCI_ITEMPC	:= clItPedCom
		ZCI->ZCI_QTALOC	:= nlQtd
		ZCI->ZCI_CANAL	:= clCanal
		ZCI->ZCI_DTENTR	:= dlDtEntr
		ZCI->ZCI_DTALOC	:= date()
		ZCI->ZCI_TIMEST	:= timeFull()
		
		ZCI->( msUnLock() )
	
	endif
	
	conOut( "..:: DS2U ::.. >> END alocaC6" )

Return

/*/{Protheus.doc} getC6ToAlc
Responsavel por controlar a consulta de itens que precisam ser alocados
@author DS2U (SDA)
@since 07/09/2018
@version 1.0
@return clAlias, Alias do arquivo temporario de consulta. Se vazio, nao retornou dados

@type function
/*/
Static Function getC6ToAlc()

	local clAlias	:= getNextAlias()
	
	conOut( "..:: DS2U ::.. >> BEGIN getC6ToAlc" )
	
	BEGINSQL ALIAS clAlias
	
		COLUMN C6_XDTEMIS AS DATE
		COLUMN C6_ENTREG AS DATE
	
		SELECT 
			SC5.C5_FILIAL AS FILIAL
			, SC5.C5_XPRIORI AS PRIORIDADE
			, CASE WHEN SC6.C6_XDATAEC = ' ' THEN SC6.C6_XDTEMIS ELSE SC6.C6_XDATAEC END DTEMISSAO // Caso o item j� tenha sido alocado e desalocado por quantidade de dias, o campo C6_XDATAEC � preenchido com uma nova data e deve ser esta a emissao na aloca��o
			, CASE WHEN SC6.C6_XHORAEC = ' ' THEN SC6.C6_XTIMEST ELSE SC6.C6_XHORAEC END HREMISSAO // Caso o item j� tenha sido alocado e desalocado por quantidade de dias, o campo C6_XHORAEC � preenchido com uma nova data e deve ser esta a emissao na aloca��o
			, SC6.C6_ENTREG AS DTENTREGA		
			, SC6.C6_NUM AS PEDIDO
			, SC6.C6_ITEM AS ITEMPEDIDO
			, SC6.C6_PRODUTO AS PRODUTO
			, SC6.C6_QTDVEN AS QUANTIDADE
			, SC6.R_E_C_N_O_ AS RECSC6	
				
		FROM 
			%TABLE:SC6% SC6
			
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = SC6.C6_FILIAL
			AND SC5.C5_NUM = SC6.C6_NUM
			AND SC5.%NOTDEL%
			
		LEFT JOIN
			%TABLE:SC9% SC9 ON
			SC9.C9_FILIAL = SC6.C6_FILIAL				
			AND SC9.C9_PEDIDO = SC6.C6_NUM
			AND SC9.C9_CLIENTE = SC6.C6_CLI
			AND SC9.C9_LOJA = SC6.C6_LOJA
			AND SC9.C9_PRODUTO = SC6.C6_PRODUTO
			AND SC9.C9_ITEM = SC6.C6_ITEM		
			AND SC9.%NOTDEL%
			
		WHERE
			SC6.C6_FILIAL = %XFILIAL:SC6%
			AND ( SC9.C9_NFISCAL IS NULL OR SC9.C9_NFISCAL = ' ' )
			AND C6_XSTALOC IN ( 'NP', 'EC' )		// Somente pedidos que ainda nao foram processados ou que estao como estima cancelamento
			AND C6_XSITUAC NOT IN ( 'C', 'T', 'O' )	// C=Cancelado;T=Transferido;O=Atendido Total
			AND SC6.%NOTDEL%
			
		ORDER BY SC5.C5_XPRIORI DESC, SC6.C6_XDTEMIS, SC6.C6_XTIMEST, SC6.C6_ENTREG, SC6.C6_NUM, SC6.C6_ITEM, SC6.C6_PRODUTO
		
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
	conOut( "..:: DS2U ::.. >> END getC6ToAlc" )

Return clAlias

/*/{Protheus.doc} getDispon
Responsavel por calcular a disponibilidade do produto no contexto do pedido de venda,
onde � considerado a cota do vendedor
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alDispon, array com informa��es de disponibilidade do produto	alDispon[1] = Data disopnivel
																		alDispon[2] = Quantidade disponivel
																		alDispon[3] = Pedido de compra
																		alDispon[4] = Item do pedido de compras
																		alDispon[5] = Codigo do Canal
																		alDispon[6] = Codigo da legenda do item

@param clPedido, characters, Codigo do pedido de venda
@param clCodPrd, characters, Codigo do produto do pedido de venda a ser consultado
@param nlQtdPed, numeric, Quantidade vendida
@param dlDtEntr, date, Data da entrega no item do pedido de vendas
@type function
/*/
Static Function getDispon( clPedido, clCodPrd, nlQtdPed, dlDtEntr, clLocAtu )

	local alDispon		:= {}
	local alAuxDisp		:= {}
	local nlSaldo		:= nlQtdPed
	local alCanais		:= {}
	local alArea		:= getArea()
	
	conOut( "..:: DS2U ::.. >> BEGIN getDispon" )
	
	//------------------------------------------------
	//- Verifica os canais que podem ser consultados -
	//------------------------------------------------
	alCanais	:= canalByPed( clPedido )
	
	if ( len( alCanais ) > 0 )
		
		//-------------------------------------------------
		//- Checa a disponibilidade em compras importadas -
		//-------------------------------------------------
		alAuxDisp := getDispPO( alCanais, clPedido, clCodPrd )
		
		//---------------------------------------------------------------------------------------------------
		//- Atribui as quantidades de compras importadas no array de controle de disponibilidade de produto -
		//---------------------------------------------------------------------------------------------------
		nlSaldo := setQtDisPv( @alDispon, alAuxDisp, nlSaldo )	
	
		//----------------------------------------------------------
		//- Se ainda h� saldo a alocar, checa em compras nacionais -
		//----------------------------------------------------------
		if ( nlSaldo > 0 )
		
			alAuxDisp := getDispPC( alCanais, clPedido, clCodPrd )
			
			//--------------------------------------------------------------------------------------------------
			//- Atribui as quantidades de compras nacionais no array de controle de disponibilidade de produto -
			//--------------------------------------------------------------------------------------------------
			nlSaldo := setQtDisPv( @alDispon, alAuxDisp, nlSaldo )
		
		endif
		
		//--------------------------------------------------------
		//- Se ainda h� saldo a alocar, checa estoque dispon�vel -
		//--------------------------------------------------------
		if ( nlSaldo > 0 )
		
			alAuxDisp := getDispED( alCanais, clPedido, clCodPrd )
			
			//--------------------------------------------------------------------------------------------------
			//- Atribui as quantidades de compras nacionais no array de controle de disponibilidade de produto -
			//--------------------------------------------------------------------------------------------------
			nlSaldo := setQtDisPv( @alDispon, alAuxDisp, nlSaldo )
		
		endif
		
	else
		conOut( "..:: DS2U ::.. >> N�o foi encontrado canais vinculados ao vendedor do pedido " + clPedido )
	endif
	
	//----------------------------------------------------------------------------
	//- Se ainda h� saldo a alocar, altera o pedido de venda, quebrando conforme -
	//- quantidades alocadas e quantidades em Estima canelamento                 -
	//----------------------------------------------------------------------------
	if ( nlSaldo > 0 )
		AADD( alDispon, { dlDtEntr, nlSaldo,,,clLocAtu, "EC" } )
	endif
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END getDispon" )

Return alDispon

/*/{Protheus.doc} setQtDisPv
Funcao para controle de saldo disponivel conforme o item do pedido de vendas
@author DS2U (SDA)
@since 28/09/2018
@version 1.0
@return nlSaldo, Saldo atualizado ap�s as atribuicoes
@param alDispon, array of logical, Array passado por referenia para armazenas as disponibilidades do produto
@param alAuxDisp, array of logical, Array com a consulta de disponibilidade
@param nlSaldo, numeric, Saldo atual a ser distribuido
@type function
/*/
Static Function setQtDisPv( alDispon, alAuxDisp, nlSaldo )

	default alAuxDisp	:= {}
	default nlSaldo		:= 0
	
	conOut( "..:: DS2U ::.. >> BEGIN setQtDisPv" )

	for nlx := 1 to len( alAuxDisp )
	
		if ( nlSaldo > 0 )
		
			AADD( alDispon, aClone( alAuxDisp[nlx] ) )
	
			if ( nlSaldo < alAuxDisp[nlx][QTDISPO] )
				alDispon[len(alDispon)][QTDISPO] := nlSaldo
				nlSaldo := 0
			else
				nlSaldo -= alAuxDisp[nlx][QTDISPO]
			endif
			
		endif
			
	next nlx
	
	conOut( "..:: DS2U ::.. >> END setQtDisPv" )
	
Return nlSaldo

/*/{Protheus.doc} canalByPed
Responsavel por retornar os canais envolvidos conforme vendedor do pedido de venda
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alCanais, Codigo dos canais envolvidos
@param clPedido, characters, Codigo do pedido de vendas
@type function
/*/
Static Function canalByPed( clPedido )

	local alArea	:= getArea()
	local alCanais	:= {}
	local clAlias	:= getNextAlias()
	
	conOut( "..:: DS2U ::.. >> BEGIN canalByPed" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			ZCB_CANAL AS CANAL
		
		FROM 
			%TABLE:ZCB% ZCB
		
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = %XFILIAL:SC5%
			AND C5_VEND1 = ZCB_VEND
			AND SC5.%NOTDEL% 
		
		WHERE 
			ZCB_FILIAL = %XFILIAL:ZCB%
			AND C5_NUM = %EXP:clPedido%
			AND ZCB_ATIVAD = 'S' // Somente canais ativos
			AND ZCB.%NOTDEL% 
		
		ORDER BY ZCB_ORDEM
	
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
	
		AADD( alCanais, ( clAlias )->CANAL )
	
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END canalByPed" )

Return alCanais

/*/{Protheus.doc} getDispPO
Responsavel por consultar a quantidade disponivel do produto nas tabelas do EIC
@author DS2U (SDA)
@since 07/09/2018
@version 1.0
@return alDispon, array com informa��es de disponibilidade do produto	alDispon[1] = Data disopnivel
																		alDispon[2] = Quantidade disponivel
																		alDispon[3] = Pedido de compra
																		alDispon[4] = Item do pedido de compras
																		alDispon[5] = Codigo do Canal
																		alDispon[6] = Codigo da legenda do item

@param alCanais, array, Array de canais envolvidos na consulta de disponibilidade
@param clPedVen, characters, Codigo do pedido de vendas
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function getDispPO( alCanais, clPedVen, clProduto )

	local clAlias	:= getNextAlias()
	local alDispon	:= {}
	local alCotas	:= {}
	local nlx
	local nlQtCalc	:= 0
	local nlQtdAloc	:= 0
	
	default clProduto	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN getDispPO" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			COALESCE( ZCK.ZCK_DTCLPO, '' ) AS DTDISP
			, SW2.W2_PO_NUM AS PO
			, SW3.W3_QTDE AS QTOTDISP
			, COALESCE( C7_NUM, '') AS PEDCOM
			, COALESCE( C7_ITEM, '') AS ITPEDCOM
			, COALESCE( D1_DOC, '') AS NOTA
			
		FROM 
			%TABLE:SW2% SW2 
			
		INNER JOIN 
			%TABLE:SW3%  SW3 ON
			SW3.W3_FILIAL = SW2.W2_FILIAL	
			AND SW3.W3_PO_NUM = SW2.W2_PO_NUM
			AND SW3.W3_TEC = ' '
			AND SW3.%NOTDEL% 
		
		INNER JOIN 
			%TABLE:SW0% SW0 ON
			SW0.W0_FILIAL = SW3.W3_FILIAL	
			AND SW0.W0__NUM = SW3.W3_SI_NUM	
			AND SW0.%NOTDEL% 
		
		LEFT JOIN	
			%TABLE:SC7% SC7 ON
			SC7.C7_FILIAL = SW2.W2_FILIAL
			AND SC7.C7_PO_EIC = SW2.W2_PO_NUM
			AND SC7.C7_ITEM = SW3.W3_POSICAO
			AND SC7.C7_FORNECE = SW3.W3_FABR
			AND SC7.C7_LOJA = SW3.W3_FABLOJ
			AND SC7.%NOTDEL%
		
		LEFT JOIN 
			%TABLE:SD1% SD1 ON
			SD1.D1_FILIAL = SC7.C7_FILIAL
			AND SD1.D1_PEDIDO = SC7.C7_NUM
			AND SD1.D1_ITEMPC = SC7.C7_ITEM
			AND SD1.%NOTDEL% 
		
		LEFT JOIN 
			%TABLE:ZCK% ZCK ON
			ZCK.ZCK_FILIAL = SW2.W2_FILIAL
			AND ZCK.ZCK_PO = SW2.W2_PO_NUM
			AND ZCK.%NOTDEL% 
		
		WHERE 
			SW2.W2_FILIAL = %XFILIAL:SW2%
			AND SW3.W3_COD_I = %EXP:clProduto%
			AND D1_DOC IS NULL
			AND ZCK_DTCLPO <> ' '
			AND SW2.%NOTDEL% 
		
		GROUP BY ZCK.ZCK_DTCLPO, SW2.W2_PO_NUM, SW3.W3_QTDE, C7_NUM, C7_ITEM, D1_DOC

	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		//--------------------------------------------------
		//- Consulta cotas, conforme os canais do vendedor -
		//--------------------------------------------------
		alCotas := getCotas( alCanais, clPedVen, clProduto, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM )
		
		for nlx := 1 to len( alCotas )
		
			nlQtCalc := int( ( clAlias )->QTOTDISP * alCotas[nlx][2] / 100 ) // Calcula a quantidade conforme a cota do canal
			
			if ( nlQtCalc > 0 )
					
				//----------------------------------------------------------------------------------
				// Consulta a quantidade alocada deste armazem para considerar na conta e garantir -
				// que nao sera alocado um item ja reservado do canal / pedido de compra           -
				//----------------------------------------------------------------------------------
				nlQtdAloc	:= getQtAloc( fwxFilial( "SW2" ), alCotas[nlx][1], clProduto, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM )
				
				//----------------------------------
				// Desconsidera quantidade alocada -
				//----------------------------------
				nlQtCalc -= nlQtdAloc
				
			endif
			
			if ( nlQtCalc > 0 )
				AADD( alDispon, { sToD( ( clAlias )->DTDISP ), nlQtCalc, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM, alCotas[nlx][1], "ET" } )
			endif
			
		next nlx
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END getDispPO" )

Return alDispon

/*/{Protheus.doc} getCotas
Responsavel por consultar as cotas para consulta de disponibilidade de produto. 
Esta consulta ja respeita a prioridade de consulto conforme amarra��o de canais x vendedor
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alCotas, Array com informa��es de cotas por canais	[1]: Canal
															[2]: Cota
															
@param alCanais, array of logical, Canais em ordem de importancia para ser consultado
@param clPedVen, characters, Codigo do pedido de vendas
@param clProduto, characters, Codigo do produto
@param clPedCom, characters, Codigo do pedido de compras
@param clItPedCom, characters, Codigo do item dp pedido de compras
@type function
/*/
Static Function getCotas( alCanais, clPedVen, clProduto, clPedCom, clItPedCom )

	local alArea	:= getArea()
	local alCotas	:= {}
	
	default clItPedCom	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN getCotas" )
	
	//-----------------------------------------------------------------------------
	//- Primeiro verificar se foi configurado cotas por item do pedido de compras -
	//-----------------------------------------------------------------------------
	if ( .not. empty( clPedCom ) .and. .not. empty( clItPedCom ) )
		alCotas := cotaByPC( alCanais, clPedVen, clPedCom, clItPedCom )
	endif
	
	//----------------------------------------------------------------------------------
	//- Se nao foi encontrado cotas por item do pedido de compras, verificar se existe -
	//- Cotas por produto.                                                             -
	//----------------------------------------------------------------------------------
	if ( len( alCotas ) == 0 )
		alCotas := cotaByPrd( alCanais, clPedVen, clProduto )
	endif

	//----------------------------------------------------------------------------
	//- Por �ltimo, se ainda n�o foi encontrado cotas, consulta cotas por canais -
	//----------------------------------------------------------------------------
	if ( len( alCotas ) == 0 )
		alCotas := cotaByCan( alCanais, clPedVen )
	endif
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END getCotas" )

Return alCotas

/*/{Protheus.doc} cotaByPC
Responsavel por consultar cotas por item do pedido de compras
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alCotas, Array no formato	[1] = Canal
									[2] = Cota

@param alCanais, array of logical, Array de canais envolvidos
@param clPedVen, characters, Codigo do pedido de vendas
@param clPedCom, characters, Codigo do pedido de compras
@param clItPedCom, characters, Codigo do item do pedido de compras
@type function
/*/
Static Function cotaByPC( alCanais, clPedVen, clPedCom, clItPedCom )

	local clAlias	:= getNextAlias()
	local alCotas	:= {}
	local clCanais	:= "%" + toFormatIn( alCanais ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN cotaByPC" )

	BEGINSQL ALIAS clAlias
	
		SELECT 
			ZCB_ORDEM AS ORDEM
			, ZCA_CANAL AS CANAL
			, ZCA_COTA AS COTA
		
		FROM 
			%TABLE:ZCA% ZCA	
			
		INNER JOIN
			%TABLE:ZCB% ZCB ON
			ZCB_FILIAL = %XFILIAL:ZCB%
			AND ZCB_CANAL = ZCA_CANAL
			AND ZCB_ATIVAD = 'S' // Somente canais ativos
			
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = %XFILIAL:SC5%
			AND C5_VEND1 = ZCB_VEND
			AND C5_NUM = %EXP:clPedVen%
			AND SC5.%NOTDEL% 
			
		WHERE 
			ZCA.ZCA_FILIAL = %XFILIAL:ZCA%
			AND ZCA_PEDCOM = %EXP:clPedCom%
			AND ZCA_ITEMPC = %EXP:clItPedCom%
			AND ZCA_CANAL IN %EXP:clCanais%
			AND ZCA.%NOTDEL%
			
		ORDER BY ZCB_ORDEM
	
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		AADD( alCotas, { ( clAlias )->CANAL, ( clAlias )->COTA } )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END cotaByPC" )

Return alCotas

/*/{Protheus.doc} cotaByPrd
Responsavel por consultar cotas por produto
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alCotas, Array no formato	[1] = Canal
									[2] = Cota

@param alCanais, array of logical, Array de canais envolvidos
@param clPedVen, characters, Codigo do Pedido de Vendas
@param clProduto, characters, Codigo do Produto
@type function
/*/
Static Function cotaByPrd( alCanais, clPedVen, clProduto )

	local clAlias	:= getNextAlias()
	local alCotas	:= {}
	local clCanais	:= "%" + toFormatIn( alCanais ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN cotaByPrd" )

	BEGINSQL ALIAS clAlias
	
		SELECT 
			ZCB_ORDEM AS ORDEM
			, ZC8_CANAL AS CANAL
			, ZC8_COTA AS COTA
		
		FROM 
			%TABLE:ZC8% ZC8	
			
		INNER JOIN
			%TABLE:ZCB% ZCB ON
			ZCB_FILIAL = %XFILIAL:ZCB%
			AND ZCB_CANAL = ZC8_CANAL
			AND ZCB_ATIVAD = 'S' // Somente canais ativos
			
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = %XFILIAL:SC5%
			AND C5_VEND1 = ZCB_VEND
			AND C5_NUM = %EXP:clPedVen%
			AND SC5.%NOTDEL% 
		
		WHERE 
			ZC8.ZC8_FILIAL = %XFILIAL:ZC8%
			AND ZC8_PROD = %EXP:clProduto%
			AND ZC8_CANAL IN %EXP:clCanais%
			AND ZC8.%NOTDEL%
			
		ORDER BY ZCB_ORDEM
	
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		AADD( alCotas, { ( clAlias )->CANAL, ( clAlias )->COTA } )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END cotaByPrd" )

Return alCotas

/*/{Protheus.doc} cotaByCan
Responsavel por consultar cotas por CANAIS
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alCotas, Array no formato	[1] = Canal
									[2] = Cota

@param alCanais, array of logical, Array de canais envolvidos
@param clPedVen, characters, Codigo do Pedido de Vendas
@type function
/*/
Static Function cotaByCan( alCanais, clPedVen )

	local clAlias	:= getNextAlias()
	local alCotas	:= {}
	local clCanais	:= "%" + toFormatIn( alCanais ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN cotaByCan" )

	BEGINSQL ALIAS clAlias
	
		SELECT
		 	ZCB_ORDEM AS ORDEM
			, ZC0_LOCAL AS CANAL
			, ZC0_PERCOT AS COTA
		
		FROM 
			%TABLE:ZC0% ZC0
			
		INNER JOIN
			%TABLE:ZCB% ZCB ON
			ZCB_FILIAL = %XFILIAL:ZCB%
			AND ZCB_CANAL = ZC0_LOCAL
			AND ZCB_ATIVAD = 'S' // Somente canais ativos
			
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = %XFILIAL:SC5%
			AND C5_VEND1 = ZCB_VEND
			AND C5_NUM = %EXP:clPedVen%
			AND SC5.%NOTDEL% 	
			
		WHERE 
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0_LOCAL IN %EXP:clCanais%
			AND ZC0.%NOTDEL%
			
		ORDER BY ZCB_ORDEM
	
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		AADD( alCotas, { ( clAlias )->CANAL, ( clAlias )->COTA } )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END cotaByCan" )

Return alCotas

/*/{Protheus.doc} toFormatIn
Funcao auxiliar para transformar o array de canais na sintaxe IN do Sql
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return formaIn, Sintaxe da funcao formaIn para ser utilizada na clausula IN da consulta SQL
@param alCanais, array of logical, Array com os canais envolvidos
@type function
/*/
Static Function toFormatIn( alCanais )

	local clCanais	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN toFormatIn" )
	
	aEval( alCanais, {|x| clCanais += allTrim( x ) + "/" } )
	clCanais := subs( clCanais, 1, len( clCanais ) - 1 )
	
	conOut( "..:: DS2U ::.. >> END toFormatIn" )

Return formatin( clCanais, "/" )

/*/{Protheus.doc} getDispPO
Responsavel por consultar a quantidade disponivel do produto em compras nacionais
@author DS2U (SDA)
@since 07/09/2018
@version 1.0
@return alDispon, array com informa��es de disponibilidade do produto	alDispon[1] = Data disopnivel
																		alDispon[2] = Quantidade disponivel
																		alDispon[3] = Pedido de compra
																		alDispon[4] = Item do pedido de compras
																		alDispon[5] = Canal
																		alDispon[6] = Codigo da legenda do item

@param alCanais, array of logical, Array de canais envolvidos
@param clProduto, characters, Codigo do produto
@param clPedVen, characters, Codigo do pedido de venda
@type function
/*/
Static Function getDispPC( alCanais, clPedVen, clProduto )

	local clAlias	:= getNextAlias()
	local alDispon	:= {}
	local alCotas	:= {}
	local nlx
	local nlQtCalc	:= 0
	local nlQtdAloc	:= 0
	
	default clProduto	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN getDispPC" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			MAX( SC7.C7_DATPRF ) AS DTDISP	
			, SC7.C7_QUANT AS QTOTDISP	
			, C7_NUM AS PEDCOM
			, C7_ITEM AS ITPEDCOM	
			, COALESCE( D1_DOC, '') AS NOTA
			
		FROM 
			%TABLE:SC7% SC7
		
		LEFT JOIN 
			%TABLE:SD1% SD1 ON
			SD1.D1_FILIAL = SC7.C7_FILIAL
			AND SD1.D1_PEDIDO = SC7.C7_NUM
			AND SD1.D1_ITEMPC = SC7.C7_ITEM
			AND SD1.%NOTDEL%  
		
		WHERE
			SC7.C7_FILIAL = %XFILIAL:SC7%
			AND C7_PRODUTO = %EXP:clProduto%
			AND D1_DOC IS NULL
			AND C7_DATPRF <> ' '
			AND C7_PO_EIC = ' '
			AND C7_QUANT > C7_QUJE
			AND SC7.%NOTDEL% 
		
		GROUP BY C7_DATPRF, C7_QUANT, C7_NUM, C7_ITEM, D1_DOC

	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		//--------------------------------------------------
		//- Consulta cotas, conforme os canais do vendedor -
		//--------------------------------------------------
		alCotas := getCotas( alCanais, clPedVen, clProduto, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM )
		
		for nlx := 1 to len( alCotas )
			
			nlQtCalc := int( ( clAlias )->QTOTDISP * alCotas[nlx][2] / 100 ) // Calcula a quantidade conforme a cota do canal
			
			if ( nlQtCalc > 0 )
					
				//----------------------------------------------------------------------------------
				// Consulta a quantidade alocada deste armazem para considerar na conta e garantir -
				// que nao sera alocado um item ja reservado do canal / pedido de compra           -
				//----------------------------------------------------------------------------------
				nlQtdAloc	:= getQtAloc( fwxFilial( "SC7" ), alCotas[nlx][1], clProduto, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM )
				
				//----------------------------------
				// Desconsidera quantidade alocada -
				//----------------------------------
				nlQtCalc -= nlQtdAloc
				
			endif
			
			if ( nlQtCalc > 0 )
				AADD( alDispon, { sToD( ( clAlias )->DTDISP ), nlQtCalc, ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM, alCotas[nlx][1], "ET" } )
			endif
			
		next nlx
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END getDispPC" )

Return alDispon

/*/{Protheus.doc} getDispED
Responsavel por consultar produto em estoque disponivel
@author DS2U (SDA)
@since 10/09/2018
@version 1.0
@return alDispon, array com informa��es de disponibilidade do produto	alDispon[1] = Data disopnivel
																		alDispon[2] = Quantidade disponivel
																		alDispon[3] = Pedido de compra
																		alDispon[4] = Item do pedido de compras
																		alDispon[5] = Canal
																		
@param alCanais, array of logical, Array de canais envolvidos
@param clProduto, characters, Codigo do produto
@param clPedVen, characters, Codigo do pedido de vendas
@type function
/*/
Static Function getDispED( alCanais, clPedVen, clProduto )

	local alArea	:= getArea()
	local clAlias	:= "TMPSB2DS2U"
	local alDispon	:= {}
	local nlx
	local nlQtCalc	:= 0
	local nlSaldoSB2:= 0
	local nlQtdAloc	:= 0
	local clCanais	:= ""
	local nlPos
	local nlPercCota:= 0
	local clSQL		:= ""
	
	default alCanais	:= {}
	default clProduto	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN getDispED" )
	
	dbSelectArea( "SB2" )
	SB2->( dbSetOrder( 1 ) ) // B2_FILIAL, B2_COD, B2_LOCAL
	
	clCanais	:= toFormatIn( alCanais )
	
	clSQL := "SELECT "
	clSQL += "	DISTINCT "
	clSQL += "	B2_FILIAL AS FILIAL "
	clSQL += "	, B2_LOCAL AS LOCPRD "
	clSQL += "	, B2_COD AS PRODUTO "
	clSQL += "	, R_E_C_N_O_ AS RECSB2 "
		
	clSQL += "FROM "
	clSQL += "	" + retSqlTab( "SB2" )
	
	clSQL += " WHERE "
	clSQL += "	SB2.B2_FILIAL = '" + fwxFilial( "SB2" ) + "' "
	clSQL += "	AND B2_COD = '" + clProduto + "' "
	clSQL += "	AND B2_LOCAL IN " + clCanais
	clSQL += "	AND SB2.D_E_L_E_T_ = ' ' " 
		
	dbUseArea( .T., "TOPCONN", tcGenQry(,,clSQL), clAlias, .T., .T. )
	
	dbSelectArea( clAlias )
	if ( .not. ( clAlias )->( eof() ) )
	
		while ( .not. ( clAlias )->( eof() ) )
			
			if ( SB2->( dbSeek( ( clAlias )->FILIAL + ( clAlias )->PRODUTO + ( clAlias )->LOCPRD ) ) )
			
				nlSaldoSB2 := saldoSB2()
			
				if ( nlSaldoSB2 > 0 )
				
					nlQtCalc := nlSaldoSB2
					
					if ( nlQtCalc > 0 )
					
						//----------------------------------------------------------------------------------
						// Consulta a quantidade alocada deste armazem para considerar na conta e garantir -
						// que nao sera alocado um item ja reservado do canal                              -
						//----------------------------------------------------------------------------------
						nlQtdAloc	:= getQtAloc( ( clAlias )->FILIAL, ( clAlias )->LOCPRD, ( clAlias )->PRODUTO )
						
						//----------------------------------
						// Desconsidera quantidade alocada -
						//----------------------------------
						nlQtCalc -= nlQtdAloc
						
					endif
					
					if ( nlQtCalc > 0 )
						AADD( alDispon, { date(), nlQtCalc,,, ( clAlias )->LOCPRD, "FM" } )
					endif
						
				endif
				
			endif
			
			( clAlias )->( dbSkip() )
		endDo
		
	endif
	( clAlias )->( dbCloseArea() )
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END getDispED" )

Return alDispon

/*/{Protheus.doc} getQtAloc
Consulta a quantidade alocada do produto por canal
@author DS2U (SDA)
@since 04/10/2018
@version 1.0
@return nlQtAloc, Quantidade alocada
@param clFilial, characters, Codigo da filial
@param clCanal, characters, Codigo do canal / armazem
@param clProduto, characters, Codigo do produto
@param clPedCom, characters, Codigo do pedido de compras
@param clItPedCom, characters, Codigo do item do pedido de compras
@type function
/*/
Static Function getQtAloc( clFilial, clCanal, clProduto, clPedCom, clItPedCom )

	local clAlias	:= getNextAlias()
	local nlQtAloc	:= 0
	local clFilPC	:= "%( 1 = 1 )%"
	
	default clFilial	:= fwxFilial( "ZCI" )
	default clCanal		:= ""
	default clProduto	:= ""
	default clPedCom	:= ""
	default clItPedCom	:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN getQtAloc" )
	
	//-------------------------------------
	// Trata filtro por pedido de compras -
	//-------------------------------------
	if ( .not. empty( clPedCom ) .and. .not. empty( clItPedCom ) )
	
		clFilPC := "%"
		clFilPC += "ZCI_PEDCOM = '" + clPedCom + "' "
		clFilPC += "AND ZCI_ITEMPC = '" + clItPedCom + "' "
		clFilPC += "%"
	
	endif
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZCI_FILIAL AS FILIAL	
			, ZCI_CANAL AS CANAL
			, ZCI_PRODUT AS PRODUTO
			, SUM( ZCI_QTALOC ) AS QTALOC
		
		FROM
			%TABLE:ZCI% ZCI
		
		WHERE
			ZCI_FILIAL = %EXP:clFilial%
			AND ZCI_CANAL = %EXP:clCanal%
			AND ZCI_PRODUT = %EXP:clProduto%
			AND %EXP:clFilPC%
		
		GROUP BY
			ZCI_FILIAL
			, ZCI_CANAL
			, ZCI_PRODUT
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		nlQtAloc := ( clAlias )->QTALOC
	endif
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END getQtAloc" )

Return nlQtAloc

/*/{Protheus.doc} setSituSC6
Responsavel por armazenar o status do item do pedido de vendas
@author DS2U (SDA)
@since 07/09/2018
@version 1.0
@param clFilial, characters, Codigo da filial
@param dlDtEntr, date, Data de entrega
@param clPedido, characters, Codigo do pedido de venda
@param clItemPed, characters, Codigo do item do pedido de venda
@param clProduto, characters, Codigo do produto
@param nlQtd, numeric, Quantidade do pedido de venda
@param clStatusIt, characters, Status do item do pedido de venda
@param clCanal, characters, Codigo do canal
@param clSituacao, characters, Situacao do item (A=Aberto;C=Cancelado;T=Transferido;P=Atendido Parcial;O=Atendido Total)
@return llProcOk, se .T., processamento ocorreu com sucesso
@type function
/*/
Static Function setSituSC6( clFilial, dlDtEntr, clPedido, clItemPed, clProduto, nlQtd, clStatusIt, clCanal, clSituacao )

	local alArea		:= getArea()
	local llProcOk		:= .T.
	local clUpd			:= ""
	local alCombo		:= retSX3Box( getSX3Cache("C6_XSTALOC","X3_CBOX"),,,2 )
	local nlQtdLib		:= 0
	
	default clFilial	:= fwxFilial( "SC6" )
	default dlDtEntr	:= cToD("//")
	default clPedido	:= ""
	default clItemPed	:= ""
	default clProduto	:= ""
	default nlQtd		:= 0
	default clStatusIt	:= ""
	default clCanal		:= ""
	default clSituacao	:= "A" // (C6_XSITUAC -> A=Aberto;C=Cancelado;T=Transferido;P=Atendido Parcial;O=Atendido Total)
	
	conOut( "..:: DS2U ::.. >> BEGIN setSituSC6" )
	
	//--------------------------------------------------------------------------------------------
	// Verifica se o status enviado por parametro � um dos configurados no campo do campo na SX3 -
	//--------------------------------------------------------------------------------------------
	if ( aScan( alCombo, {|x| allTrim( x[2] ) == allTrim( clStatusIt ) } ) > 0 )
	
		//---------------------------------------------------------------
		// Atualizo status via update para evitar dead lock de registro -
		//---------------------------------------------------------------
		clUpd := "UPDATE "
		clUpd += retSqlName( "SC6" )
		clUpd += " SET "
		clUpd += " C6_XSTALOC = '" + clStatusIt + "' "
		clUpd += " ,C6_XSITUAC = '" + clSituacao + "' "
		clUpd += " ,C6_LOCAL = '" + clCanal + "' "
		clUpd += " ,C6_ENTREG = '" + dToS( dlDtEntr ) + "' "
		clUpd += " ,C6_XDTENDI = '" + dToS( dlDtEntr ) + "' "
		clUpd += " ,C6_QTDVEN = '" + allTrim( str( nlQtd ) ) + "' "
		clUpd += " ,C6_VALOR = ( C6_QTDVEN * C6_PRCVEN ) "
		clUpd += " WHERE " 
		clUpd += " C6_FILIAL = '" + clFilial + "' "
		clUpd += " AND C6_NUM = '" + clPedido + "' "
		clUpd += " AND C6_ITEM = '" + clItemPed + "' "
		clUpd += " AND C6_PRODUTO = '" + clProduto + "' "
		clUpd += " AND D_E_L_E_T_ = ' ' "
		
		if ( tcSqlExec( clUpd ) < 0 )
			conOut( "*** ATENCAO *** FALHA NA ALTERACAO DE STATUS DO ITEM:" )
			conOut( "Filial: " + clFilial )
			conOut( "Pedido: " + clPedido )
			conOut( "Item: " + clItemPed )
			conOut( "Produto: " + clProduto )
			conOut( "ERRO: " + tcSqlError() )
			llProcOk := .F.
		endif
	
	endif

	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END setSituSC6" )

Return llProcOk

/*/{Protheus.doc} alocTransf
Consulta pedidos de venda que precisam ter suas aloca��es transferidas e as transferem para os pedidos de venda destino
@author DS2U (SDA)
@since 26/11/2018
@version 1.0

@type function
/*/
Static Function alocTransf()

	local clAliasItn := ""
	
	conOut( "..:: DS2U ::.. >> BEGIN alocTransf" )
	
	//-----------------------------------------------------------------------------------
	// Listar os itens de pedido de vendas que precisam ter suas aloca��es transferidas -
	//-----------------------------------------------------------------------------------
	clAliasItn := getQueueT()
	
	if ( .not. empty( clAliasItn ) )

		while ( .not. ( clAliasItn )->( eof() ) )
		
			//-----------------------------------------------
			// Muda status do registro para P = Processando -
			//-----------------------------------------------
			setStZCQ( "P", ( clAliasItn )->RECZCQ )
		
			//--------------------------------------
			//- Processa transferencia de aloca��o -
			//--------------------------------------
			execTrnsf( clAliasItn )
		
			//----------------------------------------------
			// Muda status do registro para F = Finalizado -
			//----------------------------------------------
			setStZCQ( "F", ( clAliasItn )->RECZCQ )
		
		( clAliasItn )->( dbSkip() )
		endDo
		( clAliasItn )->( dbCloseArea() )
	
	endif
	
	conOut( "..:: DS2U ::.. >> END alocTransf" )

Return

/*/{Protheus.doc} execTrnsf
Funcao que executa a transferencia de alocacoes conforme registros gravados na fila de aloca��es
@author DS2U (SDA)
@since 04/01/2019
@version 1.0

@type function
/*/
Static Function execTrnsf( clAlias )

	local alArea	:= getArea()
	local llTotal	:= .F.
	
	default clAlias := ""

	dbSelectArea( "SC6" )
	SC6->( dbSetOrder( 1 ) ) // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO, R_E_C_N_O_, D_E_L_E_T_
	
	if ( SC6->( dbSeek(  ( clAlias )->FILIAL + ( clAlias )->PEDVEN + ( clAlias )->ITEMPV + ( clAlias )->PRODUTO ) ) )

		llTotal	:= ( ( clAlias )->QTDVEN == ( clAlias )->QTDVEND )

		begin transaction

			//---------------------------------------------------
			// Se foi transferencia da quantidade total do item -
			//---------------------------------------------------
			if ( llTotal )
			
				//---------------------------------------
				// Altera status pertinentes a alocacao -
				//---------------------------------------
				setSituSC6( SC6->C6_FILIAL,;
							SC6->C6_ENTREG,;
							SC6->C6_NUM,;
							SC6->C6_ITEM,;
							SC6->C6_PRODUTO,;
							SC6->C6_QTDVEN,;
							SC6->C6_XSTALOC,;
							SC6->C6_LOCAL,;
							"T" ) // Situacao do item (C6_XSITUAC -> A=Aberto;C=Cancelado;T=Transferido;P=Atendido Parcial;O=Atendido Total)
							
			endif
			
			//-------------------------------------------------
			// Negativa a quantidade do item do pedido origem -
			//-------------------------------------------------
			alocaC6(SC6->C6_FILIAL,;
			 		SC6->C6_NUM,;
			 		SC6->C6_ITEM,;
			 		,;
			 		,;
			 		( SC6->C6_QTDVEN * -1 ),;
			 		SC6->C6_ENTREG,;
			 		SC6->C6_LOCAL,;
			 		SC6->C6_PRODUTO )
			 		
			 //---------------------------------------
			// Inclui a alocao para o pedido destino -
			//----------------------------------------
			alocaC6(( clAlias )->FILIAL,;
			 		( clAlias )->PEDVEND,;
			 		( clAlias )->ITEMPVD,;
			 		,;
			 		,;
			 		( clAlias )->QTDVEND,;
			 		SC6->C6_ENTREG,;
			 		SC6->C6_LOCAL,;
			 		SC6->C6_PRODUTO )
			 		
		end transaction
	
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} desalocar
Responsavel por controlar a desaloca��o de itens
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@type function
/*/
Static Function desalocar()

	local clAlias	:= getQueueD()
	
	conOut( "..:: DS2U ::.. >> BEGIN desalocar" )
	
	while ( .not. ( clAlias )->( eof() ) )
	
		//-----------------------------------------------
		// Muda status do registro para P = Processando -
		//-----------------------------------------------
		setStZCQ( "P", ( clAlias )->RECZCQ )
		
		if ( .not. empty( ( clAlias )->PEDCOM ) .and. .not. empty( ( clAlias )->ITEMPC ) )
			
			//-------------------------------------------------
			// Processa desalocacoes por alteracao em compras -
			//-------------------------------------------------
			desalocPC( { ( clAlias )->PEDCOM, ( clAlias )->ITEMPC } )
			
		elseif ( .not. empty( ( clAlias )->PEDVEN ) .and. .not. empty( ( clAlias )->ITEMPV ) )
		
			//----------------------------------------------------------
			// Processa desalocacoes por alteracao em pedido de vendas -
			//----------------------------------------------------------
			desalocPV( { ( clAlias )->PEDVEN, ( clAlias )->ITEMPV } )
		
		endif
		
		//----------------------------------------------
		// Muda status do registro para F = Finalizado -
		//----------------------------------------------
		setStZCQ( "F", ( clAlias )->RECZCQ )
	
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )
	
	conOut( "..:: DS2U ::.. >> END desalocar" )
		
Return

/*/{Protheus.doc} setStZCQ
Funcao auxiliar para alterar status da fila de processamento de aloca��es
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@param clStatus, characters, Status a ser atribuido
@param nlRecno, numeric, Recno do registro a ser alterado
@type function
/*/
Static Function setStZCQ( clStatus, nlRecno )
		
	local clUpd		:= "UPDATE " + retSqlName( "ZCQ" ) + " SET ZCQ_STATUS = '#STATUS' #FIELDS WHERE R_E_C_N_O_ = #RECZCQ"
	local clFieldSet:= ""
	
	default clStatus	:= ""
	default nlRecno		:= 0
	
	conOut( "..:: DS2U ::.. >> BEGIN setStZCQ" )
	
	//--------------------------------------------------------
	// Adiciona campo de data e hora conforme status enviado -
	//--------------------------------------------------------
	if ( clStatus == "A" )
	
		clFieldSet := " ,ZCQ_DTSTA = '" + dToS( date() ) + "' "
		clFieldSet += " ,ZCQ_HRSTA = '" + time() + "' "
	
	elseif ( clStatus == "P" )
	
		clFieldSet := " ,ZCQ_DTSTP = '" + dToS( date() ) + "' "
		clFieldSet += " ,ZCQ_HRSTP = '" + time() + "' "
		
	elseif ( clStatus == "F" )
	
		clFieldSet := " ,ZCQ_DTSTF = '" + dToS( date() ) + "' "
		clFieldSet += " ,ZCQ_HRSTF = '" + time() + "' "
		
	endif
	
	//--------------------------------------------------
	// Monta a expressao de update conforme parametros -
	//--------------------------------------------------
	clUpd := strTran( strTran( strTran( clUpd, "#STATUS", clStatus ), "#FIELDS", clFieldSet ), "#RECZCQ", cValToChar( nlRecno ) )
	
	//------------------
	// Processa update -
	//------------------
	if ( tcSqlExec( clUpd ) < 0 )
		conOut( "Falha na atualiza��o de status do registro " + cValToChar( nlRecno ) + " da tabela ZCQ" )
	else
		conOut( "FUNCTION setStZCQ >> Update aplicado com sucesso:" )
		conOut( clUpd )
	endif
	
	conOut( "..:: DS2U ::.. >> END setStZCQ" )

Return

/*/{Protheus.doc} getQueueD
Consulta na fila de aloca��es os registros que precisam ser desalocados
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho conforme consulta no banco de dados

@type function
/*/
Static Function getQueueD()

	local clAlias	:= getNextAlias()
	local clTime	:= subs( time(), 1, 5 )
	local isOracle	:= ( allTrim( tcGetDB() ) == "ORACLE" )
	local clCond	:= "%" + iif( isOracle, "SUBSTR(ZCQ_HRSTP, 1, 5)", "SUBSTRING(ZCQ_HRSTP, 1, 5)" ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN getQueueD" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZCQ_ID			
			, ZCQ_PEDVEN AS PEDVEN
			, ZCQ_ITEMPV AS ITEMPV
			, ZCQ_PRODUT AS PRODUTO
			, ZCQ_PEDCOM AS PEDCOM
			, ZCQ_ITEMPC AS ITEMPC
			, ZCQ_DTENTR AS DTENTR
			, R_E_C_N_O_ AS RECZCQ
			
		FROM
			%TABLE:ZCQ% ZCQ
			
		WHERE
			ZCQ.ZCQ_FILIAL = %XFILIAL:ZCQ%
			AND ( ZCQ.ZCQ_STATUS = 'A' OR ( ZCQ.ZCQ_STATUS = 'P' AND %EXP:clCond% < %EXP:clTime% ) )
			AND ZCQ.ZCQ_TIPO = 'D'
			AND ZCQ.%NOTDEL%
			
		ORDER BY ZCQ_DTENTR DESC 
	
	ENDSQL
	
	conOut( "..:: DS2U ::.. >> END getQueueD" )

Return clAlias

/*/{Protheus.doc} getQueueT
Consulta na fila de aloca��es os registros que precisam ter suas aloca��es transferidas
@author DS2U (SDA)
@since 26/11/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho conforme consulta no banco de dados

@type function
/*/
Static Function getQueueT()

	local clAlias	:= getNextAlias()
	local clTime	:= subs( time(), 1, 5 )
	local isOracle	:= ( allTrim( tcGetDB() ) == "ORACLE" )
	local clCond	:= "%" + iif( isOracle, "SUBSTR(ZCQ_HRSTP, 1, 5)", "SUBSTRING(ZCQ_HRSTP, 1, 5)" ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN getQueueT" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZCQ_ID			
			, ZCQ_FILIAL AS FILIAL
			, ZCQ_PEDVEN AS PEDVEN
			, ZCQ_ITEMPV AS ITEMPV
			, ZCQ_QTDVEN AS QTDVEN
			, ZCQ_PEDVND AS PEDVEND
			, ZCQ_ITMPVD AS ITEMPVD
			, ZCQ_QTDVEN AS QTDVEND
			, ZCQ_PRODUT AS PRODUTO
			, ZCQ_PEDCOM AS PEDCOM
			, ZCQ_ITEMPC AS ITEMPC
			, ZCQ_DTENTR AS DTENTR
			, R_E_C_N_O_ AS RECZCQ
			
		FROM
			%TABLE:ZCQ% ZCQ
			
		WHERE
			ZCQ.ZCQ_FILIAL = %XFILIAL:ZCQ%
			AND ( ZCQ.ZCQ_STATUS = 'A' OR ( ZCQ.ZCQ_STATUS = 'P' AND %EXP:clCond% < %EXP:clTime% ) )
			AND ZCQ.ZCQ_TIPO = 'T'
			AND ZCQ.%NOTDEL%
			
		ORDER BY ZCQ_DTENTR DESC 
	
	ENDSQL
	
	conOut( "..:: DS2U ::.. >> END getQueueT" )

Return clAlias

/*/{Protheus.doc} getQueueA
Consulta na fila de aloca��es os registros que precisam ser alocados, considerando a ordem que deve ser obedecida dos itens a serem analisados
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho conforme consulta no banco de dados

@type function
/*/
Static Function getQueueA()

	local clAlias	:= getNextAlias()
	local clTime	:= subs( time(), 1, 5 )
	local isOracle	:= ( allTrim( tcGetDB() ) == "ORACLE" )
	local clCond	:= "%" + iif( isOracle, "SUBSTR(ZCQ_HRSTP, 1, 5)", "SUBSTRING(ZCQ_HRSTP, 1, 5)" ) + "%"
	
	conOut( "..:: DS2U ::.. >> BEGIN getQueueA" )
	
	BEGINSQL ALIAS clAlias
	
		SELECT
		
			ZCQ.ZCQ_FILIAL AS FILIAL
			, ZCQ_PEDVEN AS PEDIDO
			, C5_XPRIORI AS PRIORIDADE
			, ZCQ_ITEMPV AS ITEMPEDIDO
			, ZCQ_PRODUT AS PRODUTO
			, ZCQ.ZCQ_DTENTR AS DTENTREGA
			, ZCQ.ZCQ_QTDVEN AS QUANTIDADE
			, SC6.C6_LOCAL AS CANAL
			, ZCQ.ZCQ_DTEMIS AS DTEMISSAO
			, ZCQ.ZCQ_TIMEST AS HREMISSAO
			, ZCQ.R_E_C_N_O_ AS RECZCQ
			
		FROM
			%TABLE:ZCQ% ZCQ
			
		INNER JOIN
			%TABLE:SC6% SC6 ON
			SC6.C6_FILIAL = ZCQ.ZCQ_FILIAL
			AND SC6.C6_NUM = ZCQ.ZCQ_PEDVEN
			AND SC6.C6_ITEM = ZCQ.ZCQ_ITEMPV
			AND SC6.C6_PRODUTO = ZCQ.ZCQ_PRODUT
			AND SC6.%NOTDEL%
			
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = SC6.C6_FILIAL
			AND SC5.C5_NUM = SC6.C6_NUM
			AND SC5.%NOTDEL%
			
		WHERE
			ZCQ.ZCQ_FILIAL = %XFILIAL:ZCQ%
			AND ( ZCQ.ZCQ_STATUS = 'A' OR ( ZCQ.ZCQ_STATUS = 'P' AND %EXP:clCond% < %EXP:clTime% ) )
			AND ZCQ.ZCQ_TIPO = 'A'
			AND ZCQ.%NOTDEL% 
			
		ORDER BY // Ordem definida pela equipe admv para analise de aloca��es
			ZCQ.ZCQ_FILIAL
			, ZCQ.ZCQ_PRIORI DESC
			, ZCQ.ZCQ_DTEMIS
			, ZCQ.ZCQ_TIMEST
			, ZCQ.ZCQ_DTENTR
			, ZCQ.ZCQ_PEDVEN
			, ZCQ.ZCQ_ITEMPV
			, ZCQ.ZCQ_PRODUT
	
	ENDSQL
	
	conOut( "..:: DS2U ::.. >> END getQueueA" )

Return clAlias

/*/{Protheus.doc} desalocPV
Responsavel por realizar a desalocacao por pedido de venda
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@param alParam, array of logical, Array no seguinte formato	alParam[1] = Codigo do pedido de compras
																alParam[2] = Item do pedido de compras
																alParam[3] = Flag para identificar se trata dados via begin transaction.
@type function
/*/
Static Function desalocPV( alParam )

	local clPedido	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clItem	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local llLocTran	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "L", alParam[4], .T. )
	local clAlias	:= alocByPV( {clPedido, clItem} )
	
	conOut( "..:: DS2U ::.. >> BEGIN desalocPV" )
	
	if ( empty( clAlias ) )
		conOut( "Nao foi encontrado pedidos de venda a serem desalocados a partir do pedido de venda/item [" + clPedido + "/" + clItem + "]" )
	else
	
		dbSelectArea( "SC6" )
		dbSelectArea( "ZCI" )
		
		alArea := getArea()
		
		SC6->( dbSetOrder( 1 ) ) // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO, R_E_C_N_O_, D_E_L_E_T_
	
		while ( .not. ( clAlias )->( eof() ) )
			
			if ( SC6->( dbSeek( ( clAlias )->( FILIAL + PEDVEN + ITPEDVEN + PRODUTO ) ) ) )
			
				if ( llLocTran )
				
					begin transaction
				
						setDesaloc( clAlias )
						
					end transaction
					
				else				
					setDesaloc( clAlias )
				endif
				
			endif
		
			( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )
		
		restArea( alArea )
	
	endif
	
	conOut( "..:: DS2U ::.. >> END desalocPV" )

Return

/*/{Protheus.doc} setDesaloc
Funcao auxiliar para setar a desaloca��o de produtos
@author DS2U (SDA)
@since 22/10/2018
@version 1.0
@param clAlias, characters, Alias da consulta referente a funcao alocByPV
@param clPedCom, characters, Codigo do pedido de compras
@param clItPedCom, characters, Item do pedido de compras
@param nlQtd, numeric, Quantidade a ser desalocada
@type function
/*/
Static Function setDesaloc( clAlias, clPedCom, clItPedCom, nlQtd )

	default clPedCom	:= ""
	default clItPedCom	:= ""
	default nlQtd		:= ( ( clAlias )->QTALOC * -1 )
	
	conOut( "..:: DS2U ::.. >> BEGIN setDesaloc" )
				
	if ( recLock( "SC6", .F. ) )
		SC6->C6_XSTALOC := "NP"
		SC6->( msUnLock() )
	endif
	
	if ( ( clAlias )->QTALOC > 0 )
		//----------------------------------------------------------
		// Negativa o total alocado para o item do pedido de venda -
		//----------------------------------------------------------
		alocaC6( ( clAlias )->FILIAL, ( clAlias )->PEDVEN, ( clAlias )->ITPEDVEN, clPedCom, clItPedCom, nlQtd, date(), SC6->C6_LOCAL, ( clAlias )->PRODUTO )
	endif
	
	conOut( "..:: DS2U ::.. >> END setDesaloc" )
	
Return

/*/{Protheus.doc} desalocPC
Responsavel por realizar a desalocacao por pedido de compra
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@param alParam, array of logical, Array no seguinte formato	alParam[1] = Codigo do pedido de compras
																alParam[2] = Item do pedido de compras
																alParam[3] = Flag para identificar se trata dados via begin transaction.
@type function
/*/
Static Function desalocPC( alParam )

	local clPedido	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clItem	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local llLocTran	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .T. )
	local clAlias	:= alocByPC( clPedido, clItem )
	
	conOut( "..:: DS2U ::.. >> BEGIN desalocPC" )
	
	if ( empty( clAlias ) )
		conOut( "Nao foi encontrado pedidos de venda a serem desalocados a partir do pedido de compra/item [" + clPedido + "/" + clItem + "]" )
	else
	
		dbSelectArea( "SC6" )
		dbSelectArea( "ZCI" )
		
		alArea := getArea()
		
		SC6->( dbSetOrder( 1 ) ) // C6_FILIAL, C6_NUM, C6_ITEM, C6_PRODUTO, R_E_C_N_O_, D_E_L_E_T_
	
		while ( .not. ( clAlias )->( eof() ) )
			
			if ( SC6->( ( clAlias )->( FILIAL + PEDVEN + ITPEDVEN + PRODUTO ) ) )
			
				if ( llLocTran )
				
					begin transaction
					
						setDesaloc( clAlias, clPedido, clItem )
						
					end transaction
					
				else
					setDesaloc( clAlias, clPedido, clItem )
				endif
				
			endif
			
		endDo
		( clAlias )->( dbCloseArea() )
		
		restArea( alArea )
	
	endif
	
	conOut( "..:: DS2U ::.. >> END desalocPC" )

Return

/*/{Protheus.doc} alocByPC
Responsavel por consultar as alocacoes envolvidas pelo pedido de compras + Item do PC
@author DS2U (SDA)
@since 15/10/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho da consulta feita no banco de dados. Se preenchido, retornou dados. Se vazio, nada foi encontrado
@param clPedido, characters, Codigo do pedido de compras
@param clItem, characters, Codigo do item do pedido de compreas.
@type function
/*/
Static Function alocByPC( clPedido, clItem )
	
	local clAlias	:= getNextAlias()
	
	default clPedido	:= ""
	default clItem		:= ""
	
	conOut( "..:: DS2U ::.. >> BEGIN alocByPC" )

	BEGINSQL ALIAS clAlias
	
		SELECT	
			SC6.C6_FILIAL AS FILIAL
			, SC6.C6_NUM AS PEDVEN
			, SC6.C6_ITEM AS ITPEDVEN
			, SC6.C6_PRODUTO AS PRODUTO
			,COALESCE( SUM( ZCI.ZCI_QTALOC ), 0 ) AS QTALOC
		
		FROM
			%TABLE:ZCI% ZCI
		
		INNER JOIN 
			%TABLE:SC6% SC6 ON
			SC6.C6_FILIAL = ZCI.ZCI_FILIAL
			AND SC6.C6_NUM = ZCI.ZCI_PEDVEN
			AND SC6.C6_ITEM = ZCI.ZCI_ITEMPV
			AND SC6.%NOTDEL%
		
		WHERE
			ZCI.ZCI_FILIAL = %XFILIAL:ZCI%
			AND ZCI.ZCI_PEDCOM = %EXP:clPedido%	
			AND ZCI.ZCI_ITEMPC = %EXP:clItem%
			AND ZCI.%NOTDEL%	
		
		GROUP BY
			SC6.C6_FILIAL
			, SC6.C6_NUM
			, SC6.C6_ITEM
			, SC6.C6_PRODUTO
			
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
	conOut( "..:: DS2U ::.. >> END alocByPC" )
	
Return clAlias

/*/{Protheus.doc} alocByCan
Responsavel por consultar as alocacoes envolvidas pelo pedido de vendas + Item do PV + canal
@author DS2U (SDA)
@since 15/10/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho da consulta feita no banco de dados. Se preenchido, retornou dados. Se vazio, nada foi encontrado
@param alParam[1], characters, Codigo do pedido de vendas
@param alParam[2], characters, Codigo do item do pedido de vendas.
@type function
/*/
Static Function alocByCan( alParam )
	
	local clAlias	:= getNextAlias()
	local clCond	:= "% 1 = 1 %"
	local clPedido	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clItem	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	
	if ( .not. empty( clItem ) )
		clCond := "% ZCI.ZCI_ITEMPV = '" + clItem + "' %"
	endif
	
	conOut( "..:: DS2U ::.. >> BEGIN alocByCan" )

	BEGINSQL ALIAS clAlias
	
		SELECT	
			ZCI.ZCI_FILIAL AS FILIAL 
			, ZCI.ZCI_PEDVEN AS PEDVEN 
			, ZCI.ZCI_ITEMPV AS ITPEDVEN 
			, ZCI.ZCI_PRODUT AS PRODUTO 
			, ZCI.ZCI_CANAL AS CANAL 
			, COALESCE( SUM( ZCI.ZCI_QTALOC ), 0 ) AS QTALOC
		
		FROM
			%TABLE:ZCI% ZCI
		
		WHERE
			ZCI.ZCI_FILIAL = %XFILIAL:ZCI%
			AND ZCI.ZCI_PEDVEN = %EXP:clPedido%	
			AND %EXP:clCond%
			AND ZCI.%NOTDEL%	
		
		GROUP BY
			ZCI.ZCI_FILIAL 
			, ZCI.ZCI_PEDVEN 
			, ZCI.ZCI_ITEMPV 
			, ZCI.ZCI_PRODUT 
			, ZCI.ZCI_CANAL
			
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
	conOut( "..:: DS2U ::.. >> END alocByCan" )
	
Return clAlias

/*/{Protheus.doc} alocByPV
Responsavel por consultar as alocacoes envolvidas pelo pedido de vendas + Item do PV
@author DS2U (SDA)
@since 15/10/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho da consulta feita no banco de dados. Se preenchido, retornou dados. Se vazio, nada foi encontrado
@param alParam[1], characters, Codigo do pedido de vendas
@param alParam[2], characters, Codigo do item do pedido de vendas.
@type function
/*/
Static Function alocByPV( alParam )
	
	local clAlias	:= getNextAlias()
	local clCond	:= "% 1 = 1 %"
	local clPedido	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clItem	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	
	if ( .not. empty( clItem ) )
		clCond := "% ZCI.ZCI_ITEMPV = '" + clItem + "' %"
	endif
	
	conOut( "..:: DS2U ::.. >> BEGIN alocByPV" )

	BEGINSQL ALIAS clAlias
	
		SELECT	
			SC6.C6_FILIAL AS FILIAL
			, SC6.C6_NUM AS PEDVEN
			, SC6.C6_ITEM AS ITPEDVEN
			, SC6.C6_PRODUTO AS PRODUTO
			, COALESCE( SUM( ZCI.ZCI_QTALOC ), 0 ) AS QTALOC
		
		FROM
			%TABLE:ZCI% ZCI
		
		INNER JOIN 
			%TABLE:SC6% SC6 ON
			SC6.C6_FILIAL = ZCI.ZCI_FILIAL
			AND SC6.C6_NUM = ZCI.ZCI_PEDVEN
			AND SC6.C6_ITEM = ZCI.ZCI_ITEMPV
			AND SC6.%NOTDEL%
		
		WHERE
			ZCI.ZCI_FILIAL = %XFILIAL:ZCI%
			AND ZCI.ZCI_PEDVEN = %EXP:clPedido%	
			AND %EXP:clCond%
			AND ZCI.%NOTDEL%	
		
		GROUP BY
			SC6.C6_FILIAL
			, SC6.C6_NUM
			, SC6.C6_ITEM
			, SC6.C6_PRODUTO
			
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
	conOut( "..:: DS2U ::.. >> END alocByPV" )
	
Return clAlias

/*/{Protheus.doc} queueAloc
Responsavel por acrescentar registros na entidade de controle de fila de processamento de aloca��es
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@param alParam, array of logical, Array no seguinte formato:	alParam[1] = Tipo do registro ( A=Alocar;D=Desalocar )
																alParam[2] = Codigo da filial 
																alParam[3] = Codigo do Pedido de venda
																alParam[4] = Codigo do item do pedido de venda
																alParam[5] = Codigo do produto
																alParam[6] = Prioridade do pedido de vendas
																alParam[7] = Data de emissao do item do pedido de vendas
																alParam[8] = Hora de emissao do item do pedido de vendas
																alParam[9] = Data de entrega do item do pedido de vendas
																alParam[10] = Codigo do pedido de compras amarrado ao item do pedido de vendas
																alParam[11] = Codigo do item do pedido de compras amarrado ao item do pedido de vendas
																alParam[12] = Codigo do status do registro da fila de processamento ( A=Aguardando;P=Processando;F=Finalizado )
@type function
/*/
Static Function queueAloc( alParam )

	local clType	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "A" )
	local clFilial	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], fwxFilial( "ZCQ" ) )
	local clPedVen	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clItemPV	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )
	local nlQtdPV	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 5 .and. valType( alParam[6] ) == "N", alParam[6], 0 )
	local clPriori	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 6 .and. valType( alParam[7] ) == "C", alParam[7], "00" )
	local dlDtEmis	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 7 .and. valType( alParam[8] ) == "D", alParam[8], cToD( "//" ) )
	local clTimeSt	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 8 .and. valType( alParam[9] ) == "C", alParam[9], "" )
	local dlDtEntr	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 9 .and. valType( alParam[10] ) == "D", alParam[10], cToD( "//" ) )
	local clPedCom	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 10 .and. valType( alParam[11] ) == "C", alParam[11], "" )
	local clItemPC	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 11 .and. valType( alParam[12] ) == "C", alParam[12], "" )	
	local clStatus	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 12 .and. valType( alParam[13] ) == "C", alParam[13], "A" )
	local clPedVenD	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 13 .and. valType( alParam[14] ) == "C", alParam[14], "" )
	local clItemPVD	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 14 .and. valType( alParam[15] ) == "C", alParam[15], "" )
	local nlQtdPVD	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 15.and. valType( alParam[16] ) == "N", alParam[16], 0 )
	local llPutQueue:= .T.
	local llProcOk	:= .T.
	
	conOut( "..:: DS2U ::.. >> BEGIN queueAloc" )
	
	dbSelectArea( "ZCQ" )
	ZCQ->( dbSetOrder( 3 ) )
	
	// Verifica se a aloca��o j� est� na fila para ser alocado
	if ( ZCQ->( dbSeek( fwxFilial( "ZCQ" ) + PADR( clPedVen, tamSX3("C6_NUM")[1] ) + PADR( clItemPV, tamSX3("C6_ITEM")[1] ) ) ) )
	
		if ( ZCQ->ZCQ_STATUS == "A" ) // Se esta na fila e aguardando, permite alteracao do registro
			llPutQueue := .F.
		elseif ( ZCQ->ZCQ_STATUS == "P" ) // Se esta na fila e processando, n�o permite alteracao / inclusao do registro
			llProcOk := .F.
		endif
		
	endif 
	
	if ( llProcOk )
	
		if ( recLock( "ZCQ", llPutQueue ) )
		
			ZCQ->ZCQ_FILIAL	:= clFilial
			if ( llPutQueue )
				ZCQ->ZCQ_ID		:= getSXeNum( "ZCQ", "ZCQ_ID" )
			endif
			ZCQ->ZCQ_TIPO	:= clType // "A=Alocar;D=Desalocar;T=Transferir"
			ZCQ->ZCQ_PEDVEN	:= clPedVen
			ZCQ->ZCQ_ITEMPV	:= clItemPV
			ZCQ->ZCQ_PRODUT	:= clProduto
			ZCQ->ZCQ_QTDVEN	:= nlQtdPV
			ZCQ->ZCQ_PRIORI	:= clPriori
			ZCQ->ZCQ_DTEMIS	:= dlDtEmis
			ZCQ->ZCQ_TIMEST	:= clTimeSt
			ZCQ->ZCQ_DTENTR	:= dlDtEntr
			ZCQ->ZCQ_PEDCOM	:= clPedCom
			ZCQ->ZCQ_ITEMPC	:= clItemPC
			ZCQ->ZCQ_STATUS	:= clStatus // "A=Aguardando;P=Processando;F=Finalizado"
			if ( clStatus == "A" )
				ZCQ->ZCQ_DTSTA	:= date() // Data do status A
				ZCQ->ZCQ_HRSTA	:= time() // Hora do status A
			elseif ( clStatus == "P" )
				ZCQ->ZCQ_DTSTP	:= date() // Data do status P
				ZCQ->ZCQ_HRSTP	:= time() // Hora do status P
			elseif ( clStatus == "F" )
				ZCQ->ZCQ_DTSTF	:= date() // Data do status F
				ZCQ->ZCQ_HRSTF	:= time() // Hora do status F
			endif
			
			//---------------------------------------------------------------------
			// Tratamento para inclusao de registro de transferencia de alocacoes -
			//---------------------------------------------------------------------
			if ( clType == "T" )
				ZCQ->ZCQ_PEDVND	:= clPedVenD
				ZCQ->ZCQ_ITMPVD	:= clItemPVD
				ZCQ->ZCQ_QTDVND	:= nlQtdPVD
			endif
			
			ZCQ->( msUnLock() )
		
		endif
		
	endif
	
	conOut( "..:: DS2U ::.. >> END queueAloc" )
	
Return

/*/{Protheus.doc} stepsAloc
Rotina de passo a passo para execucao do JOB de aloca��es
@author DS2U (SDA)
@since 22/10/2018
@version 1.0

@type function
/*/
Static Function stepsAloc( alParam )

	private cpIDAloc	:= timeFull()
	
	conOut( "..:: DS2U ::.. >> BEGIN stepsAloc" )
	
	conOut( "ID: [" + cpIDAloc + "] - Iniciando processanmento de aloca��es" )
	conOut( "Empresa: [" + cEmpAnt + "]" )
	conOut( "Filial: [" + cFilAnt + "]" )
	
	//----------------------------------------------------------------
	// Limpa fila de aloca��es, retirando tudo que j� foi finalizado -		
	//----------------------------------------------------------------
	if ( cleanZCQ() )
	
		//----------------------------------------------------------------------------------
		// Checa itens de pedido de venda atendidos parcial ou total e altera sua situa��o -
		//----------------------------------------------------------------------------------
		chkSituC6()
	
		//----------------------------------------------------------------------------------------------------------------------------
		// Checa itens do pedido de venda que est�o a mais de ZC6_RESERV dias como Firme e mudam seu status para Estima Cancelamento -
		//----------------------------------------------------------------------------------------------------------------------------
		firToEc()
		
		//----------------------------------------------------------------------------------------------------------------------------
		// Checa itens pedido de venda que est�o a mais de ZC6_INTCOM dias como Estima Cancelamento e muda seu status para Cancelado -
		//----------------------------------------------------------------------------------------------------------------------------
		ecToCan()
	
		//----------------------------------------------------------
		// Processa tudo que esta na fila e precisa ser desalocado -
		//----------------------------------------------------------
		desalocar()
		
		//----------------------------------------------------------------
		// Limpa fila de aloca��es, retirando tudo que j� foi finalizado -
		//----------------------------------------------------------------
		if ( cleanZCQ() )
		
			//----------------------------------------------------------------
			// Processa os pedidos que precisam ter sua aloca��o transferida -
			//----------------------------------------------------------------
			alocTransf()
		
			//-------------------------------------------------------------------------------------------------------
			// Consulta todos os pedidos de venda com necessidade de aloca��o e acrescenta na fila de processamento -
			//-------------------------------------------------------------------------------------------------------
			alocar()
			
			//-------------------------------------------------------
			// Processa tudo que esta na fila e precisa ser alocado -
			//-------------------------------------------------------
			execAloca()
			
		endif
		
	endif
		
	conOut( "ID: [" + cpIDAloc + "] - Finalizando processanmento de aloca��es" )
	
	conOut( "..:: DS2U ::.. >> END stepsAloc" )

Return

/*/{Protheus.doc} chkSituC6
Funcao auxiliar para alterar o status dos itens conforme seu faturamento
@author DS2U (SDA)
@since 15/01/2019
@version 1.0

@type function
/*/
Static Function chkSituC6()

	local clUpd := ""
	
	clUpd += "UPDATE " 
	clUpd += 	retSqlName( "SC6" )  
	clUpd += " SET " 
	clUpd += "	C6_XSITUAC = CASE " 
	clUpd += "					WHEN " 
	clUpd += "						C6_QTDVEN = C6_QTDENT " 
	clUpd += "					THEN "
	clUpd += "						'O' " // Atendimento Total
	clUpd += "					ELSE  "
	clUpd += "						CASE " 
	clUpd += "							WHEN " 
	clUpd += "								C6_QTDENT > 0 " 
	clUpd += "							THEN "
	clUpd += "								'P' " // Atendimento Parcial 
	clUpd += "							ELSE "
	clUpd += "							 	C6_XSITUAC " // Mantem a situacao
	clUpd += "						END "
	clUpd += "				END "
	clUpd += "	WHERE "
	clUpd += "		C6_FILIAL = '" + fwxFilial( "SC6" ) + "' "
	clUpd += "		AND C6_XSITUAC NOT IN ('O', 'T', 'C') "
	clUpd += "		AND D_E_L_E_T_ = ' ' "
	
	if ( tcSqlExec( clUpd ) < 0 )
		conOut( "..:: DS2U ::.. >> Falha no update situa��o do item do pedido de venda: " + tcSqlError() )
	endif

Return

/*/{Protheus.doc} firToEc
Checa itens do pedido de venda que est�o a mais de ZC6_RESERV dias como Firme e mudam seu status para Estima Cancelamento//TODO Descri��o auto-gerada.
@author DS2U (SDA)
@since 03/01/2019
@version 1.0

@type function
/*/
Static Function firToEc()

	local clUpd := ""
	
	clUpd += "UPDATE " 
	clUpd += 	retSqlName( "SC6" )  
	clUpd += " SET " 
	clUpd += "	C6_XSTALOC = 'EC' "
	clUpd += "	,C6_XSITUAC = 'A' "
	clUpd += "	,C6_XDATAEC = '" + dToS( date() ) + "' "
	clUpd += "	,C6_XHORAEC = '" + timeFull() + "' " 
	clUpd += "	WHERE "
	clUpd += "		R_E_C_N_O_ IN ( "
	clUpd += "			SELECT "
	clUpd += "				R_E_C_N_O_ " 
	clUpd += "			FROM "
	clUpd += 				retSqlName( "SC6" ) + " SC6 " 
	clUpd += "			WHERE "
	clUpd += "				SC6.C6_FILIAL = '" + fwxFilial( "SC6" ) + "' " 
	clUpd += "				AND C6_XSTALOC = 'FM' "
	clUpd += "				AND C6_XDTEMIS <> ' ' "
	clUpd += "				AND SC6.D_E_L_E_T_ = ' ' "
	clUpd += "				AND DATEDIFF (DAY , C6_XDTEMIS , '" + dToS( date() ) + "' ) > ( "
	
	//---------------------------------------------------------------------------------------------------------------------
	// Query para checar o parametro Reserva ou Dias Alocacao a ser considerado para regra de altera��o de status do item -
	// Ordem a ser seguida: Pedido de venda / Cliente / Parametros ADMV                                                   -
	//---------------------------------------------------------------------------------------------------------------------
	clUpd += " 																				SELECT "
	clUpd += " 																					CASE WHEN RESERVPV > 0 THEN RESERVPV ELSE "
	clUpd += " 																						CASE WHEN RESERVCLI > 0 THEN RESERVCLI ELSE "
	clUpd += " 																							RESERVADMV "
	clUpd += " 																						END "
	clUpd += " 																					END AS RESERV "
	
	clUpd += " 																				FROM "
	clUpd += " 																				( 
	
	clUpd += " 																					SELECT " 
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							C5_XRESERV "
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "SC5" ) + " SC5 "
	clUpd += " 																						WHERE "
	clUpd += " 																							SC5.C5_FILIAL = C6_FILIAL "
	clUpd += " 																							AND C5_NUM = C6_NUM "
	clUpd += " 																							AND SC5.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS RESERVPV "
	clUpd += " 																					, "
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							A1_XRESERV "
	
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "SC5" ) + " SC5 "
	
	clUpd += " 																						INNER JOIN "
	clUpd += 																							retSqlName( "SA1" ) + " SA1 ON "
	clUpd += " 																							A1_FILIAL = '" + fwxFilial( "SA1" ) + "' "
	clUpd += " 																							AND A1_COD = C5_CLIENTE "
	clUpd += " 																							AND A1_LOJA = C5_LOJACLI "
	clUpd += " 																							AND SA1.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																						WHERE "				
	clUpd += " 																							SC5.C5_FILIAL = C6_FILIAL "
	clUpd += " 																							AND C5_NUM = C6_NUM "
	clUpd += " 																							AND SC5.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS RESERVCLI "
	clUpd += " 																					, "
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							ZC6_RESERV " 
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "ZC6" ) + " ZC6 " 
	clUpd += " 																						WHERE "
	clUpd += " 																							ZC6.ZC6_FILIAL = '" + fwxFilial( "SC6" ) + "' "
	clUpd += " 																							AND ZC6.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS RESERVADMV "
	
	clUpd += " 																				) AS TMP "
	
	clUpd += " 																			) "
	clUpd += "				) "
	
	if ( tcSqlExec( clUpd ) < 0 )
		conOut( "..:: DS2U ::.. >> Falha no update da funcao firToEc: " + tcSqlError() )
	endif

Return

/*/{Protheus.doc} ecToCan
Checa itens pedido de venda que est�o a mais de ZC6_INTCOM dias como Estima Cancelamento e muda seu status para Cancelado
@author DS2U (SDA)
@since 03/01/2019
@version 1.0

@type function
/*/
Static Function ecToCan()

	local clUpd := ""
	
	clUpd += "UPDATE " 
	clUpd += retSqlName( "SC6" )  
	clUpd += " SET " 
	clUpd += "	C6_XSITUAC = 'C' " 
	clUpd += "	WHERE "
	clUpd += "		R_E_C_N_O_ IN ( "
	clUpd += "			SELECT "
	clUpd += "				R_E_C_N_O_ " 
	clUpd += "			FROM "
	clUpd += 				retSqlName( "SC6" ) + " SC6 " 
	clUpd += "			WHERE "
	clUpd += "				SC6.C6_FILIAL = '" + fwxFilial( "SC6" ) + "' " 
	clUpd += "				AND C6_XSTALOC = 'EC' "
	clUpd += "				AND C6_XDATAEC <> ' ' "
	clUpd += "				AND SC6.D_E_L_E_T_ = ' ' "
	clUpd += "				AND DATEDIFF (DAY , C6_XDATAEC , '" + dToS( date() ) + "' ) > ( "
	
	//---------------------------------------------------------------------------------------------------------------
	// Query para checar o parametro Inten��o de Compra a ser considerado para regra de altera��o de status do item -
	// Ordem a ser seguida: Pedido de venda / Cliente / Parametros ADMV                                             -
	//---------------------------------------------------------------------------------------------------------------
	clUpd += " 																				SELECT "
	clUpd += " 																					CASE WHEN INTCOMPV > 0 THEN INTCOMPV ELSE "
	clUpd += " 																						CASE WHEN INTCOMCLI > 0 THEN INTCOMCLI ELSE "
	clUpd += " 																							INTCOMADMV "
	clUpd += " 																						END "
	clUpd += " 																					END AS INTCOM "
	
	clUpd += " 																				FROM "
	clUpd += " 																				( 
	
	clUpd += " 																					SELECT " 
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							C5_XINTCOM "
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "SC5" ) + " SC5 "
	clUpd += " 																						WHERE "
	clUpd += " 																							SC5.C5_FILIAL = C6_FILIAL "
	clUpd += " 																							AND C5_NUM = C6_NUM "
	clUpd += " 																							AND SC5.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS INTCOMPV "
	clUpd += " 																					, "
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							A1_XINTCOM "
	
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "SC5" ) + " SC5 "
	
	clUpd += " 																						INNER JOIN "
	clUpd += 																							retSqlName( "SA1" ) + " SA1 ON "
	clUpd += " 																							A1_FILIAL = '" + fwxFilial( "SA1" ) + "' "
	clUpd += " 																							AND A1_COD = C5_CLIENTE "
	clUpd += " 																							AND A1_LOJA = C5_LOJACLI "
	clUpd += " 																							AND SA1.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																						WHERE "				
	clUpd += " 																							SC5.C5_FILIAL = C6_FILIAL "
	clUpd += " 																							AND C5_NUM = C6_NUM "
	clUpd += " 																							AND SC5.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS INTCOMCLI "
	clUpd += " 																					, "
	clUpd += " 																					( "
	clUpd += " 																						SELECT "
	clUpd += " 																							ZC6_INTCOM " 
	clUpd += " 																						FROM "
	clUpd += 																							retSqlName( "ZC6" ) + " ZC6 " 
	clUpd += " 																						WHERE "
	clUpd += " 																							ZC6.ZC6_FILIAL = '" + fwxFilial( "SC6" ) + "' "
	clUpd += " 																							AND ZC6.D_E_L_E_T_ = ' ' "
	
	clUpd += " 																					) AS INTCOMADMV "
	
	clUpd += " 																				) AS TMP "
	
	clUpd += " 																			) "
	clUpd += "				) "
	
	if ( tcSqlExec( clUpd ) < 0 )
		conOut( "..:: DS2U ::.. >> Falha no update da funcao ecToCan: " + tcSqlError() )
	endif

Return

/*/{Protheus.doc} cleanZCQ
Funcao responsavel por limpar a fila de aloca��es, gravando seu historico na tabela de historico de fila de aloca��es ZCP
@author DS2U (SDA)
@since 29/10/2018
@version 1.0
@return llRet, Se .T., processamento realizado com sucesso. Se .F., houve alguma falha no processamento das instru��es de insert e delete

@type function
/*/
Static Function cleanZCQ()

	local alArea	:= {}
	local clInsert	:= ""
	local clDelete	:= ""
	local llRet		:= .T.
	
	conOut( "..:: DS2U ::.. >> BEGIN cleanZCQ" )
	
	dbSelectArea( "ZCQ" )
	dbSelectArea( "ZCP" )
	
	alArea	:= getArea()
	
	//------------------------------
	// Monta a instrucao de Insert -
	//------------------------------
	clInsert += "INSERT INTO " + retSqlName( "ZCP" )
	clInsert += " ( ZCP_FILIAL,ZCP_ID,ZCP_TIPO,ZCP_PEDVEN,ZCP_ITEMPV,ZCP_PRODUT,ZCP_QTDVEN,ZCP_PRIORI,ZCP_DTEMIS,ZCP_DTSTF,ZCP_TIMEST,ZCP_DTENTR,ZCP_PEDCOM,ZCP_ITEMPC,ZCP_STATUS,ZCP_DTSTA,ZCP_HRSTA,ZCP_DTSTP,ZCP_HRSTP,ZCP_HRSTF,R_E_C_N_O_ ) "
	clInsert += " SELECT ZCQ_FILIAL,ZCQ_ID,ZCQ_TIPO,ZCQ_PEDVEN,ZCQ_ITEMPV,ZCQ_PRODUT,ZCQ_QTDVEN,ZCQ_PRIORI,ZCQ_DTEMIS,ZCQ_DTSTF,ZCQ_TIMEST,ZCQ_DTENTR,ZCQ_PEDCOM,ZCQ_ITEMPC,ZCQ_STATUS,ZCQ_DTSTA,ZCQ_HRSTA,ZCQ_DTSTP,ZCQ_HRSTP,ZCQ_HRSTF,R_E_C_N_O_  "
	clInsert += " FROM " + retSqlName( "ZCQ" )
	clInsert += " WHERE D_E_L_E_T_ = ' ' AND ZCQ_STATUS = 'F' "
	
	//------------------------------
	// Monta a instrucao de delete -
	//------------------------------
	clDelete += " DELETE FROM " + retSqlName( "ZCQ" )
	clDelete += " WHERE D_E_L_E_T_ = ' ' AND ZCQ_STATUS = 'F' "

	begin transaction
	
		//-------------------------------
		// Processa instrucao de insert -
		//-------------------------------
		if ( tcSqlExec( clInsert ) < 0 )
			llRet := .F.
			conOut( "Erro na limpeza da fila de aloca��es. Processamento interrompido!" )
			conOut( "Instru��o SQL: " + clInsert )
			conOut( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		if ( llRet )
		
			//-------------------------------
			// Processa instrucao de delete -
			//-------------------------------
			if ( tcSqlExec( clDelete ) < 0 )
				llRet := .F.
				conOut( "Erro na limpeza da fila de aloca��es. Processamento interrompido!" )
				conOut( "Instru��o SQL: " + clDelete )
				conOut( "Erro retornado do banco de dados: " + tcSqlError() )
			endif
		
		endif
		
		if ( .not. llRet )
			disarmTransaction()
		endif
	
	end Transaction
	
	restArea( alArea )
	
	conOut( "..:: DS2U ::.. >> END cleanZCQ" )

Return llRet

Static Function cancelPV( alParam )

	local clUpd		:= ""
	local llProcOk	:= .T.
	local llParamOk	:= valType( alParam ) == "A"
	local clFilial	:= iif( llParamOk .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], fwxFilial( "SC6" ) )
	local clPedido	:= iif( llParamOk .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	
	if ( .not. empty( clFilial ) .and. .not. empty( clPedido ) .and. msgYesNo( "Deseja cancelar todos os itens do pedido " + clPedido + " ?" ) )
	
		begin transaction
		
			clUpd := "UPDATE " + retSqlName( "SC5" )
			clUpd += " SET "
			clUpd += " C5_XSITUAC = 'C' "
			clUpd += " WHERE "
			clUpd += " C5_FILIAL = '" + clFilial + "' "
			clUpd += " AND C5_NUM = '" + clPedido + "' "
			clUpd += " AND D_E_L_E_T_ = ' ' "
			
			if ( tcSqlExec( clUpd ) < 0 )
				llProcOk := .F.
				disarmTransaction()
			endif
			
			clUpd := "UPDATE " + retSqlName( "SC6" )
			clUpd += " SET "
			clUpd += " C6_XSITUAC = 'C' "
			clUpd += " WHERE "
			clUpd += " C6_FILIAL = '" + clFilial + "' "
			clUpd += " AND C6_NUM = '" + clPedido + "' "
			clUpd += " AND D_E_L_E_T_ = ' ' "
			
			if ( tcSqlExec( clUpd ) < 0 )
				llProcOk := .F.
				disarmTransaction()
			endif
		
		end transaction
		
		if ( .not. llProcOk )
			alert( "Falha no cancelamento dos itens >> " + tcSqlError() )
		endif
	
	endif
	
Return