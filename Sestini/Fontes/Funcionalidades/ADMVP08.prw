#INCLUDE 'PROTHEUS.CH'
#INCLUDE "TOPCONN.CH"
#INCLUDE "TOTVS.CH"

//-----------------------------------------------
/*/{Protheus.doc} ADMVP08
Al�ada de desconto do pedido de venda

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVP08()
Return

//-----------------------------------------------
/*/{Protheus.doc} FItnsVldAlc
Verifica se existem itens com status Firme ou
Estimado, caso tenha pelo menos um item, dever�
ser avaliado o pedido para Al�ada de desconto

@author DS2U (HFA)
@since 10/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FItnsVldAlc( lGravado , clFilial , cPedido )

	Local lRet			:= .F.
	Local nY			:= 0
	Local nPos			:= 0
	Local aAreaSC6      := SC6->(GetArea())
	Local cSttItAlc		:= Alltrim(GetMV("ES_STITALC",,"FM|ET")) // ADMV - Status do item do Pedido de venda que deve ser avaliado a al�ada.
	//FM=Firme;ET=Estimado

	If lGravado

	    dbSelectArea("SC6")
	    SC6->( dbSetOrder(1) ) //C6_FILIAL+C6_NUM+C6_ITEM+C6_PRODUTO

	    If SC6->( dbSeek( clFilial + cPedido ) )

	        While SC6->(! Eof() ) .And. SC6->( C6_FILIAL + C6_NUM ) == clFilial + cPedido

	            If !Empty( SC6->C6_XSTALOC ) .And. Alltrim( SC6->C6_XSTALOC ) $ cSttItAlc

	            	lRet := .T.
	            	Exit

	            EndIf

	            SC6->( dbSkip() )
	        EndDo

	    EndIf

	Else
	
		// Quando ainda o Pedido de vendas n�o foi gravado
		If ( nPos := aScan( aHeader , {| x | Alltrim( x[ 2 ] ) == "C6_XSTALOC" } ) ) > 0

			For nY := 1 To Len( aCols )

				If !Empty( aCols[ nY ][ nPos ] ) .And. Alltrim( aCols[ nY ][ nPos ] ) $ cSttItAlc

	            	lRet := .T.
	            	Exit

				Endif

			Next nY

		EndIf

	EndIf

    RestArea( aAreaSC6 )

Return lRet
//-----------------------------------------------
/*/{Protheus.doc} FAlcPREA
Rotina de Pre Aprova��o de Al�ada de
Pedido de Venda

@author DS2U (HFA)
@since 08/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FAlcPREA()

	Local lPreAprov := .F.
	Local cJustif	:= ""
	Local nOpc		:= 0
	Local cObs		:= ""
	Local cVendUsr	:= Alltrim( GetAdvFVal( "SA3" , "A3_COD" , FWxFilial("SA3")+ __cUserId , 7 ) )
	Local aRetBloq	:= { }

	If Alltrim( SC5->C5_VEND1 ) == Alltrim( cVendUsr )

		If SC5->C5_XSITUAC == "A" .And. SC5->C5_XALCADA == "N"

			While !lPreAprov

		        nOpc := Aviso( "Justificativa?",@cJustif,{"Pr�-Aprovar","Sair"},1,"Justificativa",,,.T.)

		        If nOpc == 1

		            If !Empty(cJustif)

		            	cObs	:= "Pr�-Aprova��o de Al�ada: " + cJustif
		                lPreAprov := .T.

		            Else

		                Help(,, "FAlcPREA_01",, '� obrigat�rio informar o justificativa.', 1, 0)

		            EndIf

		        Else
		        	Exit
		        EndIf

		    EndDo

		    If lPreAprov

		    	aadd( aRetBloq , {"4", cObs } )
		    	
		    	u_ADP12Grv( SC5->C5_NUM, "2" /*TpMovimento*/ , aRetBloq , "P" )

		    EndIf

		Else

			Help(,, "FAlcPREA_02",, 'Somente pedidos N�o Aprovados por Al�ada e Ativos podem ser pr�-aprovados.' , 1, 0)

		EndIf

	Else

		Help(,, "FAlcPREA_03",, 'Somente o vendedor que incluiu o pedido de venda pode pr�-aprovar a al�ada.', 1, 0)

	EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} FAlcAPRO
Rotina de Aprova��o/ Reprova��o de Al�ada
de Pedido de Venda

@author DS2U (HFA)
@since 08/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FAlcAPRO()

	Local lAprov	:= .F.
	Local cJustif	:= ""
	Local nOpc		:= 0
	Local cObs		:= ""
	Local cSitAlc	:= ""
	Local aEventZCG	:= {}
	Local lUsrSuperv:= FIsSuperv( __cUserId , SC5->C5_VEND1 )
	Local cMailVend	:= Alltrim( GetAdvFVal( "SA3" , "A3_EMAIL" , FWxFilial("SA3")+ SC5->C5_VEND1 , 1 ) )

	If lUsrSuperv .Or. U_FVUsrADV( __cUserId , "2" , 2 )

		If SC5->C5_XSITUAC == "A" .And. SC5->C5_XALCADA $ "N|P|R|"

			While !lAprov

		        nOpc := Aviso( "Aprova��o de Al�ada",@cJustif,{"Aprovar","Reprovar","Vis.Pre-Aprov.", "Sair" },1,"Aprova��o de Al�ada",,,.T.)

		        If nOpc == 1 .Or. nOpc == 2

		            If !Empty(cJustif)

		            	cObs		:= IIf( nOpc == 1 , "Aprova��o" , "Reprova��o") + " de Al�ada (" + cUserName + "): " + cJustif
		                lAprov		:= .T.
		                cSitAlc		:= IIf( nOpc == 1 , "A" , "R")

		            Else

		                Help(,, "FAlcAPRO_02",, '� obrigat�rio informar o justificativa.' , 1, 0)

		            EndIf

		        ElseIf nOpc == 3

		        	U_ADP12View()

		        Else

		        	Exit

		        EndIf

		    EndDo

		    If lAprov
		    	
		    	
		    	aadd( aEventZCG , { IIF( cSitAlc == "R" , "1", "2" ) , cJustif } )
		    	u_ADP12Grv( SC5->C5_NUM, "2" /*TpMovimento*/ , aEventZCG , cSitAlc )

	    		//Em caso de Reprova��o, envia e-mail de notifica��o
	    		If cSitAlc == "R"

		    		If !IsBlind()
		    			FWMsgRun( ,{|| U_ADP8Mail( 2 , SC5->C5_VEND1 , cMailVend , "" , SC5->C5_NUM , SC5->C5_CLIENTE , SC5->C5_LOJACLI , cObs ) },"Aguarde...", "Enviando e-mail de Al�ada")
		    		Else
		    			U_ADP8Mail( 2 , SC5->C5_VEND1 , cMailVend , "" , SC5->C5_NUM , SC5->C5_CLIENTE , SC5->C5_LOJACLI , cObs )
		    		EndIf

	    		EndIf

		    EndIf

		Else

			Help(,, "FAlcAPRO_01",, 'Somente pedidos N�o Aprovados, Pr�-Aprovados, Reprovados e Ativos podem ser Aprovados/Reprovados.' , 1, 0)

		EndIf

	EndIf

Return


//-----------------------------------------------
/*/{Protheus.doc} FIsSuperv
Verifica se o usu�rio logado no sistema
� o supervisor de vendas, do vendedor
que inseriu o Pedido de venda

@author DS2U (HFA)
@since 10/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FIsSuperv( cUserAcess , cVendedor )

	Local lRet 		:= .F.
	Local aAreaSA3	:= {}
	Local cSuperv	:= ""

	dbSelectArea("SA3")
	aAreaSA3 := SA3->( GetArea() )

	SA3->( dbSetOrder(1) )
	If SA3->( dbSeek( FWxFilial("SA3") + cVendedor ) )

		cSuperv := SA3->A3_SUPER

		If !Empty( cSuperv )

			If SA3->( dbSeek( FWxFilial("SA3") + cSuperv ) )

				If Alltrim(SA3->A3_CODUSR) == Alltrim( cUserAcess )

					lRet  := .T.

				EndIf

			EndIf

		EndIf

	EndIf

	RestArea( aAreaSA3 )

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} GetSuperv
Retorna o  supervisor de vendas, do vendedor
que inseriu o Pedido de venda

@author DS2U (HFA)
@since 10/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function GetSuperv( cVendedor )

	Local aAreaSA3	:= {}
	Local cSuperv	:= ""

	dbSelectArea("SA3")
	aAreaSA3 := SA3->( GetArea() )

	SA3->( dbSetOrder(1) )
	If SA3->( dbSeek( FWxFilial("SA3") + cVendedor ) )

		cSuperv := SA3->A3_SUPER

	EndIf

	RestArea( aAreaSA3 )

Return cSuperv


//-----------------------------------------------
/*/{Protheus.doc} ADP8Mail
Envia o email para Supervisor/Vendedor informando
que o pedido de venda foi bloqueado,Liberado,
Reprovado por Al�ada

nOpc| 1 = Bloqueado por Al�ada
	| 2 = Reprovado por Al�ada
	| 3 = Aprovado a Al�ada

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP8Mail( nOpc , cSuperv , cEmailTo , cEmailCc , cPedido , cCliente, cLoja , cMotivo )

    Local cAssunto  := "Alcada de Pedido de Venda"
    Local cHtmlAlc	:= GetMV("ES_HTMALCA",,"\Workflow\PVBlqAlcada.html") // ADMV - HTML Alcada de Pedido de Venda
    Local cMensagem := ""
    Local cErro     := ""
    Local oHtmlModel:= Nil

    If nOpc == 1
    	cAcaoAlc := "Bloqueado por Al�ada"
    ElseIf nOpc == 2
    	cAcaoAlc := "Reprovado por Al�ada"
    Else
    	cAcaoAlc := "Aprovado a Al�ada"
    EndIf

    If File(cHtmlAlc)

        oHtmlModel := TWFHTML():New(cHtmlAlc)
        oHtmlModel:ValByName( "ALCADA"		, cAcaoAlc		                                                                            )
        oHtmlModel:ValByName( "PEDIDO"		, cPedido		                                                                            )
        oHtmlModel:ValByName( "CLIENTE"		, cCliente		                                                                            )
        oHtmlModel:ValByName( "NOME"		, Alltrim( GetAdvFVal( "SA1" , "A1_NOME" , FWxFilial("SA1") + cCliente + cLoja  , 1 ) )		)
        oHtmlModel:ValByName( "CNPJ"		, Alltrim( GetAdvFVal( "SA1" , "A1_CGC"  , FWxFilial("SA1") + cCliente + cLoja  , 1 ) )		)
        oHtmlModel:ValByName( "MOTIVO"		, StrTran( cMotivo , CRLF , "<br/>") 		                                                                            )
        cMensagem := oHtmlModel:HtmlCode()

    Else

        cMensagem := "Prezado(a),"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "O pedido de venda abaixo foi " + cAcaoAlc + ":"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "<table border='1'> "
        cMensagem += "	<tr> "
        cMensagem += "		<th>Pedido</th> "
        cMensagem += "		<th>Cliente</th> "
        cMensagem += "		<th>Nome Cliente</th> "
        cMensagem += "		<th>CNPJ</th> "
        cMensagem += "		<th>Motivo</th> "
        cMensagem += "	</tr>"
        cMensagem += "	<tr>"
        cMensagem += "		<td>" + cPedido + "</td>"
        cMensagem += "		<td>" + cCliente + "</td>"
        cMensagem += "		<td>" + Alltrim( GetAdvFVal( "SA1" , "A1_NOME" , FWxFilial("SA1") + cCliente + cLoja  , 1 ) ) + "</td>"
        cMensagem += "		<td>" + Alltrim( GetAdvFVal( "SA1" , "A1_CGC"  , FWxFilial("SA1") + cCliente + cLoja  , 1 ) ) + "</td>"
        cMensagem += "		<td>" + cMotivo + "</td>"
        cMensagem += "	</tr> "
        cMensagem += " </table> "
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"
        cMensagem += "ADMV Sestini"
        cMensagem += "<Br></Br>"
        cMensagem += "<Br></Br>"

    EndIf

    U_ADMVSNdMail(cAssunto, cMensagem, {}, cEmailTo, cEmailCc, @cErro , .F. , .T. )

Return


//-----------------------------------------------
/*/{Protheus.doc} LdItensSC6
Carrega os itens do Pedido de Venda, no array
para calculo da al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function LdItensSC6( lGravado , clFilial , cPedido )

	Local nC		:= 0
	Local nY		:= 0
	Local nPos		:= 0
	Local aRet		:= {}
	Local aAux		:= {}
	Local aCmpSC6	:= LdCmpSC6()
	Local xConteud	:= Nil
		
	If lGravado

	    dbSelectArea("SC6")
	    SC6->( dbSetOrder(1) ) //C6_FILIAL+C6_NUM+C6_ITEM+C6_PRODUTO

	    If SC6->( dbSeek( clFilial + cPedido ) )

	        While SC6->(! Eof() ) .And. SC6->( C6_FILIAL + C6_NUM ) == clFilial + cPedido

	        	aAux := {}
	        	For nC	:= 1 To Len( aCmpSC6 )
	        	
	        		If SubStr( aCmpSC6[ nC ][1] , 1 , 3) == "C6_"
	        		
	        			xConteud := SC6->( &( aCmpSC6[ nC ][1] ) )
	        		
	        		Else
	        		
	        			xConteud := 0
	        		
	        		EndIf

	        		aadd( aAux , { aCmpSC6[ nC ][1] , xConteud , aCmpSC6[ nC ][2]	} )

	        	Next nC

	        	aadd( aRet , aAux )

	            SC6->( dbSkip() )
	        EndDo

	    EndIf

	Else

		// Quando ainda o Pedido de vendas n�o foi gravado
		For nY := 1 To Len( aCols )

			aAux := {}

			For nC	:= 1 To Len( aCmpSC6 )
			
				xConteud := 0

				If ( nPos := aScan( aHeader , {| x | Alltrim( x[ 2 ] ) == Alltrim( aCmpSC6[ nC ][ 1 ] ) } ) ) > 0

					xConteud :=  aCols[ nY ][ nPos ]

				EndIf
				
				aadd( aAux , { aCmpSC6[ nC ][ 1 ] ,  xConteud	, aCmpSC6[ nC ][ 2 ] } )

			Next nC

			aadd( aRet , aAux )

		Next nY

	EndIf

Return aRet

//-----------------------------------------------
/*/{Protheus.doc} LdCmpSC6
Campos dos itens do pedido de venda, usados para
o Calculo da Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function LdCmpSC6()

	Local aRet	:= {}

	aadd( aRet , {"C6_ITEM"		, "ZCM_ITEM"	})
	aadd( aRet , {"C6_PRODUTO"	, "ZCM_PRODUT"	})
	aadd( aRet , {"C6_QTDVEN"	, "ZCM_QTDVEN"	})
	aadd( aRet , {"C6_PRCVEN"	, "ZCM_PRCVEN"	})
	aadd( aRet , {"C6_VALOR"	, "ZCM_VALOR"	})
	aadd( aRet , {"C6_DESCONT"	, "ZCM_DESCON"	})
	aadd( aRet , {"C6_VALDESC"	, "ZCM_VALDES"	})
	aadd( aRet , {"C6_COMIS1"	, "ZCM_COMIS1"	})
	aadd( aRet , {"C6_COMIS2"	, "ZCM_COMIS2"	})
	aadd( aRet , {"C6_COMIS3"	, "ZCM_COMIS3"	})
	aadd( aRet , {"C6_COMIS4"	, "ZCM_COMIS4"	})
	aadd( aRet , {"C6_COMIS5"	, "ZCM_COMIS5"	})
	aadd( aRet , {"C6_XCOMBS1"	, "ZCM_COMBS1"	})
	aadd( aRet , {"C6_XCOMBS2"	, "ZCM_COMBS2"	})
	aadd( aRet , {"C6_XCOMBS3"	, "ZCM_COMBS3"	})
	aadd( aRet , {"C6_XCOMBS4"	, "ZCM_COMBS4"	})
	aadd( aRet , {"C6_XCOMBS5"	, "ZCM_COMBS5"	})
	aadd( aRet , {"C6_XPRCTAB"	, "ZCM_PRCTAB"	})
	aadd( aRet , {"C6_XSTALOC"	, "ZCM_STALOC"	})
	aadd( aRet , {"C6_XPRCNEG"	, "ZCM_PRCNEG"	})
	aadd( aRet , {"C6_XVALDES"	, "ZCM_DESAGI"	})
	aadd( aRet , {"C6_XIDPROM"	, "ZCM_IDPROM"	})
	aadd( aRet , {"C6_XPTROCA"	, "ZCM_PTROCA"	})
	aadd( aRet , {"C6_XDSCPRM"	, "ZCM_DSCPRM"	})
	aadd( aRet , {"C6_XDSCTRC"	, "ZCM_DSCTRC"	})
	aadd( aRet , {"C6_XUTPRZP"	, "ZCM_UTPRZP"	})
	aadd( aRet , {"C6_XUTDSCP"	, "ZCM_UTDSCP"	})
	aadd( aRet , {"C6_XPRAZOP"	, "ZCM_PRAZOP"	})
	aadd( aRet , {"ZCM_PSPOND"	, "ZCM_PSPOND"	})
	aadd( aRet , {"ZCM_BRTITM"	, "ZCM_BRTITM"	})
	aadd( aRet , {"ZCM_PRZPNP"	, "ZCM_PRZPNP"	}) //PRAZO PONDERADO PERMITIDO
	aadd( aRet , {"ZCM_PRZPNC"	, "ZCM_PRZPNC"	}) //PRAZO PONDERADO CALCULADO - UTILIZADO
	aadd( aRet , {"ZCM_DSCPNP"	, "ZCM_DSCPNP"	}) //PercDescPromocionalPerm
	aadd( aRet , {"ZCM_DSCPNU"	, "ZCM_DSCPNU"	}) //PercDescPromocionalUtili
	aadd( aRet , {"ZCM_TRCPNP"	, "ZCM_TRCPNP"	}) //PercLimiteTrocaPermitida
	aadd( aRet , {"ZCM_MARGEM"	, "ZCM_MARGEM"	}) //PercMargemTab
	aadd( aRet , {"ZCM_DIFCOM"	, "ZCM_DIFCOM"	}) //nDifComiss	
	

Return aRet


//-----------------------------------------------
/*/{Protheus.doc} ADP8VAlc
Visualiza��o Calculo da Al�ada do Pedido de Vendas
chamado pelas op��es: 
Bot�o do Browse do pedido de venda
Bot�o Dentro da tela de manuten��o do Pedido de vendas
Bot�o de consulta no Log de Calculos de Al�ada

//lPVGravado  = False - Visualiza��o dentro da tela de Pedidos
//lPVGravado  = True  - Pedido j� Gravado

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADP8VAlc( lCacular , lPVGravado , cPedido , cFilPed  , lGravarAlc , lView )

	Local oAlcada	:= Nil
	Local clFilial	:= ""
	Local cObs		:= ""
	Local cTipo		:= ""
	Local clPedVen	:= ""

	Default cPedido		:= ""
	Default cFilPed		:= ""
	Default lCacular	:= .T.
	Default lPVGravado	:= .T.
	Default lView		:= .T.
	Default lGravarAlc	:= .T.

	clFilial	:= cFilPed
	clPedVen	:= cPedido 

	If Empty(clFilial)
		clFilial	:= IIF( lPVGravado , SC5->C5_FILIAL , FWxFilial("SC5") )
	EndIf
	
	If Empty(clPedVen)
		clPedVen	:= IIF( lPVGravado , SC5->C5_NUM , M->C5_NUM )
	EndIf
	
	oAlcada:= STNAlcada():New( clPedVen , clFilial , lPVGravado )
		
	If lCacular

		//--------------------------------------
		//Calcula os dados da al�ada na hora
		//--------------------------------------
		oAlcada:AvaliaAlcadaPV( lGravarAlc )

	Else

		//--------------------------------------
		//Carrega dados da al�ada gravada
		//--------------------------------------
    	oAlcada:LoadAlcada()

	EndIf

	//------------------------------------
	//Mostra Tela de Al�ada
	//------------------------------------
	If lView
	
		If Valtype( oAlcada ) == "O" .And. oAlcada:ExisteAlcada
	
			oAlcada:ViewAlcada()
	
		Else
	
			Help(,, "ADP8VAlc_01",, 'N�o existe al�ada para este pedido de venda.', 1, 0)
	
		EndIf

	EndIf

	If Valtype( oAlcada ) == "O"

		FreeObj( oAlcada )
		oAlcada:= Nil

	EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada
Class para guardar os dados de calculo da al�ada
Metodos para grava��o, view, visualiza��o dos
dados j� gravas (loadal�ada), e calculo de al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Class STNAlcada

	//Variaveis Pedido
	Data NumPedido					As String
	Data CodCliente					As String
	Data LojaCliente				As String
	Data cNumPVExterno				As String
	Data TabelaPedido				As String
	Data PercDescComercial			As Float
	Data PercDescCliente			As Float
	Data PercDescNegociado			As Float
	Data PercDescTotal				As Float
	Data VlrDescTotal				As Float
	Data VlrFrete					As Float
	Data TabelaPadCliente			As String
	Data PedidoMae					As String
	Data UtilizaPromocao			As String
	Data ClienteGrupo				As String
	Data TipoFrete					As String
	Data Prioridade					As String
	Data Reserva					As Float
	Data IntCompra					As Float
	Data VlrBrutoPedido				As Float
	Data VlrLiquidoPedido			As Float
	Data ObsAlcada					As String
	Data SituacaoAlcada				As String
	Data SituacaoEstoque			As String
	Data SituacaoComericial			As String
	Data SituacaoCredito			As String

	//Variaveis Al�ada
	Data PercAlcadaPermitida		As Float
	Data PercAlcadaUtilizada		As Float
	Data PercDebCredAlcada			As Float
	Data VlrDebCredAlcada			As Float

	//Variaves Descontos
	Data PercMargemTab				As Float
	Data PercComissaoPermitida		As Float
	Data PercCustoFrete				As Float
	Data PercFinancPermitida		As Float
	Data PercPagAntecipado			As Float
	Data PercDescClientePermitida	As Float
	Data PercDescClienteUtilizada	As Float
	Data PercDescComercPolitPerm	As Float
	Data PercDescComercPolitUtili	As Float
	Data PercDescPromocionalPerm	As Float
	Data PercDescPromocionalUtili	As Float
	Data PercLimiteTrocaPermitida	As Float
	Data PercLimiteTrocaUtilizada	As Float
	Data PercDisponTrocaPermitida	As Float

	//Variaveis Prazo
	Data PrazoPoliticaPermitida		As Float
	Data PrazoPoliticaUtilizada		As Float
	Data PrazoAdicionalPolitica		As Float
	Data PrazoAdicionalComercial	As Float
	Data PrazoPromoPonderadoPerm	As Float
	Data PrazoPromoPonderadoUtili	As Float
	Data PrazoUtilizadoCalculo		As Float
	Data PrazoSaldoPromoc			As Float
	Data PrazoPercSaldoPromoc		As Float
	Data PrazoCliente				As Float
	Data PrazoLivreEncargoPermit	As Float
	Data PrazoLivreEncargoUtiliz	As Float
	Data PrazoLiquido				As Float
	Data DiasFinanceiros			As Float
	Data DiasCondPagamento			As Float

	//Itens do Pedido que compoem Al�ada
	Data ItensPedido				As Array

	//Variaveis auxiliares para calculo
	Data ExisteAlcada				As Boolean
	Data lGravado					As Boolean
	Data CodCondicaoPag				As String
	Data cFilPed					As String
	Data cStatusAlcada				As String
	Data ObsCalcAlcada				As String
	Data DtEmissaoPV				As Date

	Method new( cPedido , clFilial , lGravado ) Constructor

	Method CalculaAlcada()
	Method LoadAlcada()
	Method GravaAlcada()
	Method AvaliaAlcadaPV( lGrava )
	Method ViewAlcada()
	Method CampVariav()
	Method GridAlcada()
	Method GridDesconto()
	Method GridPrazo()
	Method GridVlrPedido()
	Method CalcItensPedido( )
	Method NumDiasCondPag( cCodCondic , dDataCalc )
	Method CalcCustoFrete()
	Method CalcFinanceiro()

EndClass


//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:New
Metodo para criar a class de Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method New( cPedido , clFilial , lGravado ) Class STNAlcada

	Default lGravado	:= .T.
	Default clFilial	:= FWxFilial("SC5")

	If lGravado

		dbSelectArea("SC5")
		SC5->( dbSetOrder( 1 ) )
		SC5->( dbSeek( clFilial + cPedido  ) )

	EndIf

	dbSelectArea("ZC6")
	ZC6->( dbSetOrder(1) )
	ZC6->( dbSeek( FWxFilial("ZC6") + PADR( getMv("ES_ADMVPAR",,"000001"), TamSX3("ZC6_CODIGO")[1] ) ) )
	
	::lGravado			:= lGravado
	::NumPedido			:= cPedido
	::cFilPed			:= clFilial
	::cStatusAlcada		:= IIF( lGravado , SC5->C5_XALCADA	, M->C5_XALCADA	)
	::CodCliente		:= IIF( lGravado , SC5->C5_CLIENTE	, M->C5_CLIENTE	)
	::LojaCliente		:= IIF( lGravado , SC5->C5_LOJACLI	, M->C5_LOJACLI	)
	::cNumPVExterno		:= IIF( lGravado , SC5->C5_XPVEXT  	, M->C5_XPVEXT	)
	::SituacaoAlcada	:= IIF( lGravado , SC5->C5_XALCADA	, M->C5_XALCADA	)
	::SituacaoEstoque	:= IIF( lGravado , SC5->C5_XESTOQU	, M->C5_XESTOQU	)
	::SituacaoComericial:= IIF( lGravado , SC5->C5_XSTCOAD	, M->C5_XSTCOAD	)
	::SituacaoCredito	:= IIF( lGravado , SC5->C5_XBLQCRE	, M->C5_XBLQCRE	)
	::CodCondicaoPag	:= IIF( lGravado , SC5->C5_CONDPAG	, M->C5_CONDPAG	)
	::DtEmissaoPV		:= IIF( lGravado , SC5->C5_EMISSAO	, M->C5_EMISSAO	)
	::ObsAlcada			:= GHistAlc( ::SituacaoAlcada , ::NumPedido ) 
	
	::ObsCalcAlcada		:= ""

	dbSelectArea("SA1")
	SA1->( dbSetOrder(1) )
	SA1->( dbSeek( FWxFilial("SA1") + ::CodCliente + ::LojaCliente  ) )
	
	::ClienteGrupo		:= SA1->A1_XGRPEMP

Return Self

//-----------------------------------------------
/*/{Protheus.doc} GHistAlc
Fun��o para retornar a Observa��o da al�ada de 
desconto

@author DS2U (HFA)
@since 19/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function GHistAlc( cSitAlc , cPedido )

	Local cSttAlc		:= "'"+ X3COMBO('C5_XALCADA', cSitAlc ) +"'"
	Local cObsAlc		:= U_GAD12Memo( cPedido	 , "2" , cSttAlc )
				
Return cObsAlc 

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:CalculaAlcada
Metodo para calcular a al�ada do pedido de venda

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method CalculaAlcada() Class STNAlcada

	Local nPrzClie		:= ZC6->ZC6_PRZPLI
	Local nPrAdicPol	:= ZC6->ZC6_PRZADP
	Local nVlrMinPV		:= ZC6->ZC6_VMINDS
	Local nPrzCom		:= ZC6->ZC6_PRZCOM
	Local nReserva		:= ZC6->ZC6_RESERV
	Local nIntComp		:= ZC6->ZC6_INTCOM
	Local cCndPagAnt	:= U_ADMVXFUN( 15 , "1")
	Local nDescPAnt		:= ZC6->ZC6_DESPGA
	Local nDsCliParc	:= ZC6->ZC6_PCLIPA
	Local nDsComrPol	:= ZC6->ZC6_DESCOM
	Local nAlcAceit		:= ZC6->ZC6_PALCAD
	
	::ExisteAlcada 		:= .T.
	::cStatusAlcada		:= "L"
	
	//------------------------------------------------------------
	//Dados do Pedido de Venda
	//------------------------------------------------------------
	::TabelaPedido				:= IIF( ::lGravado , SC5->C5_TABELA	, M->C5_TABELA	)
	::PercDescComercial			:= IIF( ::lGravado , SC5->C5_DESC3	, M->C5_DESC3	)
	::PercDescCliente			:= IIF( ::lGravado , SC5->C5_DESC1	, M->C5_DESC1	)
	::PercDescNegociado			:= IIF( ::lGravado , SC5->C5_DESC2	, M->C5_DESC2	)
	::VlrFrete					:= IIF( ::lGravado , SC5->C5_FRETE	, M->C5_FRETE	)
	::TipoFrete					:= IIF( ::lGravado , SC5->C5_TPFRETE, M->C5_TPFRETE	)
	::Prioridade				:= IIF( ::lGravado , SC5->C5_XPRIORI, M->C5_XPRIORI	)
	::PedidoMae					:= IIF( ::lGravado , SC5->C5_XISPM	, M->C5_XISPM	)
	::TabelaPadCliente			:= IIF( Alltrim( ::TabelaPedido ) == Alltrim( SA1->A1_TABELA ) , "1" , "2" )
	::ItensPedido				:= LdItensSC6( ::lGravado , ::cFilPed , ::NumPedido )
	::Reserva					:= nReserva
	::IntCompra					:= nIntComp
	::UtilizaPromocao			:= "2"
	


	//--------------------------------------------------------------------------------------
	// Calculo do valor do Pedido
	//--------------------------------------------------------------------------------------
	//Calculos nos itens do Pedido de Venda Ponderados
	::CalcItensPedido( )

	::PercDescTotal				:= Round( (1 - ( ::VlrLiquidoPedido / ::VlrBrutoPedido ) ) * 100 , 2 )
	::VlrDescTotal				:= ::VlrBrutoPedido  * (::PercDescTotal/100)


	//------------------------------------------------------------
	//Calculos referente ao Grid de Prazos
	//------------------------------------------------------------
	::DiasCondPagamento			:= ::NumDiasCondPag( ::CodCondicaoPag , ::DtEmissaoPV )
	::PrazoPoliticaPermitida	:= nPrzClie
	::PrazoAdicionalPolitica	:= nPrAdicPol
	::PrazoAdicionalComercial	:= IIF( ::VlrBrutoPedido > nVlrMinPV ,  nPrzCom  , 0 )
	::PrazoCliente				:= SA1->A1_XPRZCLI
	::PrazoLivreEncargoPermit	:= ::PrazoAdicionalPolitica + ::PrazoAdicionalComercial + ::PrazoPromoPonderadoPerm + ::PrazoCliente
	::PrazoLivreEncargoUtiliz	:= ::DiasCondPagamento

	If ( ::DiasCondPagamento - ::PrazoUtilizadoCalculo )  <= ( ( ::PrazoAdicionalPolitica + ::PrazoAdicionalComercial + ::PrazoCliente ))
		::PrazoLiquido			:= ( ::PrazoAdicionalPolitica + ::PrazoAdicionalComercial + ::PrazoCliente )
	Else
		::PrazoLiquido			:= ::DiasCondPagamento - ::PrazoUtilizadoCalculo
	EndIf
	
	::PrazoPromoPonderadoUtili	:= ::DiasCondPagamento - ::PrazoLiquido
	::PrazoSaldoPromoc			:= IIF( ( ::PrazoPromoPonderadoPerm  - ::PrazoPromoPonderadoUtili > 0 ) , ( ::PrazoPromoPonderadoPerm - ::PrazoPromoPonderadoUtili) , 0  )
	::PrazoPercSaldoPromoc		:= Round ( ( ::PrazoSaldoPromoc / ::PrazoPromoPonderadoPerm * 100 ) , 2 )  
	::PrazoPoliticaUtilizada	:= ::PrazoLiquido - ( ::PrazoAdicionalPolitica + ::PrazoAdicionalComercial + ::PrazoCliente )
	::DiasFinanceiros			:= ::PrazoLiquido - ( ::PrazoCliente + ::PrazoAdicionalPolitica + ::PrazoAdicionalComercial )
	::DiasFinanceiros			:= Round( ::DiasFinanceiros , 0)


	//------------------------------------------------------------
	//Calculos referente ao Grid de DESCONTOS
	//------------------------------------------------------------
	::PercCustoFrete			:= ::CalcCustoFrete()
	::PercFinancPermitida		:= ::CalcFinanceiro()
	::PercPagAntecipado			:= IIF( Alltrim( ::CodCondicaoPag) $ cCndPagAnt , nDescPAnt , 0 )
	::PercDescClientePermitida	:= IIF( SA1->A1_XPARCEI == "1" ,  SA1->A1_XDESCPV + nDsCliParc , 0)
	::PercDescClienteUtilizada	:= ::PercDescCliente
	::PercDescComercPolitPerm	:= IIF( ::VlrBrutoPedido > nVlrMinPV , nDsComrPol , 0 )
	::PercDescComercPolitUtili	:= ::PercDescComercial
	::PercDisponTrocaPermitida	:= ::PercLimiteTrocaPermitida * ( ::PrazoPercSaldoPromoc / 100 )
	::PercLimiteTrocaUtilizada	:= Round( ::PercLimiteTrocaPermitida - ::PercDisponTrocaPermitida , 2 ) 


	//------------------------------------------------------------
	//Calculos referente ao Grid de Al�ada
	//------------------------------------------------------------
	::PercAlcadaPermitida		:= Round( (   1  - (1-::PercMargemTab/100) 							;
												 * (1-::PercComissaoPermitida/100) 					;
												 * (1-::PercCustoFrete/100)							;
												 * (1-::PercFinancPermitida/100)					;
												 * (1-::PercPagAntecipado/100)						;
												 * (1-::PercDescClientePermitida/100)				;
												 * (1-::PercDescComercPolitPerm/100)				;
												 * (1-::PercDescPromocionalPerm/100)				;
												 * (1-::PercDisponTrocaPermitida/100)  ) * 100 , 2	)

	::PercAlcadaUtilizada		:= Round( ( 1-(1-::PercDescComercial/100)*(1-::PercDescCliente/100)*(1-::PercDescNegociado/100) ) * 100 , 2)

	If ::PercAlcadaUtilizada > ::PercAlcadaPermitida

		::PercDebCredAlcada	:= (  ( (1 - ::PercAlcadaUtilizada/100 ) / (1 - ::PercAlcadaPermitida/100 ) ) - 1  )

	Else

		::PercDebCredAlcada	:= Round( ( (1 - (1 - ::PercAlcadaPermitida/100 ) / ( 1 - ::PercAlcadaUtilizada/100) ) ) * 100 , 2 )

	EndIf

	::VlrDebCredAlcada		:= Round( ::VlrLiquidoPedido * ( ::PercDebCredAlcada/100 ) , 2 )

	If ::PercDebCredAlcada < nAlcAceit

		::cStatusAlcada		:= "N"

	EndIf

Return()

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:LoadAlcada
Metodo para buscar os dados da al�ada do
pedido de venda ja calculado e armazenado
Apenas Consulta

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method LoadAlcada( ) Class STNAlcada

	Local aDePara	:= ::CampVariav()
	Local nC		:= 0
	Local cAux		:= ""
	
	dbSelectArea("ZCH")
	ZCH->( dbSetOrder(1) )

	If ZCH->( dbSeek( ::cFilPed + ::NumPedido ) )
		
		::ExisteAlcada := .T.
		
		For nC := 1 To Len( aDePara )
		
			cAux	:= "self:" + aDePara[ nC ][ 2 ] 
			&cAux 	:= ZCH->&( aDePara[ nC ][ 1 ] )
		
		Next nC

	Else

		::ExisteAlcada	:= .F.

	EndIf

Return()

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:GravaAlcada
Metodo para gravar na tabela os dados da al�ada do
pedido de venda ja calculado que est�o no objeto

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method GravaAlcada() Class STNAlcada

	Local aDePara	:= ::CampVariav()
	Local lLock		:= .T.
	Local nC		:= 0
	Local nY		:= 0
	Local nPsItem	:= {}
	Local cItem		:= ""
	Local aAux		:= {}
	Local cAux		:= ""
	Local cTpMovLg	:= ""
	Local aEventZCG := {}

	dbSelectArea("ZCH")
	ZCH->( dbSetOrder(1) )
	If ZCH->( dbSeek( ::cFilPed + ::NumPedido ) )
		lLock := .F.
	EndIf

	//------------------------------------------
	// Cabe�alho do calculo da al�ada
	//------------------------------------------
	If ZCH->( RecLock( "ZCH" , lLock ))

		ZCH->ZCH_FILIAL	:= ::cFilPed

		For nC := 1 To Len( aDePara )
			cAux	:= "self:" + aDePara[ nC ][ 2 ]
			ZCH->&(aDePara[ nC ][ 1 ]) := &cAux

		Next nC

		ZCH->( MsUnLock() )
	EndIf
	
	
	//------------------------------------------
	// Itens da Al�ada
	//------------------------------------------
	dbSelectArea("ZCM")
	ZCM->( dbSetOrder( 1 ) )
	
	For nC := 1 To Len( ::ItensPedido )
	
		aAux := aClone( ::ItensPedido[ nC ] )
		
		If nC == 1
		
			nPsItem		:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_ITEM"	} )
		
		Endif
		
		cItem := ::ItensPedido[ nC ][ nPsItem ][2]

		If ZCM->( dbSeek(  ::cFilPed + ::NumPedido + cItem ) )
			lLock := .F.
		Else
			lLock := .T.
		EndIf
	
		If ZCM->( RecLock( "ZCM" , lLock ))
	
			If lLock
				ZCM->ZCM_FILIAL	:= ::cFilPed
				ZCM->ZCM_PEDIDO	:= ::NumPedido
			EndIf
			
			For nY := 1 To Len( aAux )
				 
				ZCM->&(aAux[ nY ][ 3 ]) := aAux[ nY ][ 2 ]
	
			Next nY
	
			ZCM->( MsUnLock() )
		EndIf
	
	Next nC

	//----------------------------------------------------------------------
	//Observa��o do calcula da al�ada para atualizar o Pedido de Vendas
	//----------------------------------------------------------------------
	If ::cStatusAlcada == "N"
		
		::ObsCalcAlcada := "Pedido Bloqueado por Al�ada Automaticamente. Al�ada: " +; 
							Alltrim( cValToChar( ::PercDebCredAlcada ) ) +  " Menor que o Permitido: " + Alltrim( cValToChar( ZC6->ZC6_PALCAD ) )
		
	
	ElseIf ::cStatusAlcada == "L"
		
		::ObsCalcAlcada := "Pedido de Venda avaliado por al�ada e liberado automaticamente. Al�ada: " +;
							Alltrim( cValToChar( ::PercDebCredAlcada ) ) +  " Maior que o Permitido: " + Alltrim( cValToChar( ZC6->ZC6_PALCAD ) )
		
	EndIf

	If ::cStatusAlcada == "N"
		cTpMovLg := "1"
	Else
		cTpMovLg := "2"
	EndIf
	aadd( aEventZCG, { cTpMovLg , ::ObsCalcAlcada })    
		
	//----------------------------------------------------------------------
	// Grava o historico de bloqueios
	//----------------------------------------------------------------------
	U_ADP12Grv( ::NumPedido, "2" /*TpMovimentoBloq*/ , aEventZCG , ::cStatusAlcada )
			
	//----------------------------------------------------------------------
	// Verifica se deve Enviar e-mail referente a Al�ada
	//----------------------------------------------------------------------
	FMailAlcPV( ::cStatusAlcada  , ::ObsCalcAlcada )
				
Return()

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:AvaliaAlcadaPV
Refaz os calculos da Al�ada e grava os dados
calculados na tabela de Al�ada

@author DS2U (HFA)
@since 05/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method AvaliaAlcadaPV( lGrava ) Class STNAlcada
	
	Local lAnalisAlc    := FItnsVldAlc( ::lGravado , ::cFilPed , ::NumPedido )
		
	Default lGrava		:= .T.
	
	If !IsBlind()
		CursorWait()
	EndIf
	
	If lAnalisAlc
	
		::CalculaAlcada()
		
		If lGrava
		
			::GravaAlcada()
			
		EndIf
	
	Else
		
		::ExisteAlcada 		:= .F.
		
	EndIf
	
	If !IsBlind()
		CursorArrow()
	EndIf
	
Return

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:ViewAlcada
Metodo para Visualizar a tela de al�ada do
pedido de venda, com os dados ja atualizado no
objeto

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method ViewAlcada() Class STNAlcada

	Local oLayer		:= FWLayer():New()
	Local oFont18N		:= TFont():New( "Arial", Nil, 18, Nil, .T., Nil, Nil, Nil, .F., .F. )
	Local oLyAlc		:= Nil
	Local alCoord		:= MsAdvSize(.T.,.F.,0)
	Local oDlg			:= Nil
	Local oPainAlc		:= Nil
	Local oPanAlc		:= Nil
	Local oPanDesc		:= Nil
	Local oPanPrz		:= Nil
	Local oPanNomAlc	:= Nil
	Local oPanBrwAlc	:= Nil
	Local oPanNomDesc 	:= Nil
	Local oPanBrwDesc 	:= Nil
	Local oPanNomPrz	:= Nil
	Local oPanBrwPrz	:= Nil
	Local oGetAlc		:= Nil
	Local oGetDesc		:= Nil
	Local oGetPrz		:= Nil
	Local oLyPed		:= Nil
	Local oPainPedV		:= Nil
	Local oPainDetPV	:= Nil
	Local oEncDetPV		:= Nil
	Local oPanPed1		:= Nil
	Local oLySitPV 		:= Nil
	Local oColVlrPV		:= Nil
	Local oPanVlrPV		:= Nil
	Local oPanBrwPV		:= Nil
	Local oGetVlrPed	:= Nil
	Local oPanSitPV		:= Nil
	Local oTBarSit		:= Nil
	Local oEncSit		:= Nil
	Local oPanPed2		:= Nil
	Local oPanNomObs	:= Nil
	Local oPanBrwObs	:= Nil
	Local oObsAlcada	:= Nil
	Local aHdAlc		:= FHdAlcada(1)
	Local aHdDesc		:= FHdAlcada(2)
	Local aHdPrz		:= FHdAlcada(3)
	Local aClsAlc		:= ::GridAlcada()
	Local aClsDesc		:= ::GridDesconto()
	Local aClsPrz		:= ::GridPrazo()
	Local aHdVlrPed		:= FHdPedido()
	Local aClsVlrPed	:= ::GridVlrPedido()
	Local cObsAlcada	:= GHistAlc( ::SituacaoAlcada , ::NumPedido ) // Caso tenha recalculado, busco o historico atualizado
	Local aCmpDetPV		:= FCmpDetPed()
	Local aFieldPV		:= FFieldEnc( aCmpDetPV )
	Local aCmpSits		:= FCmpSitPed()
	Local aFielSit		:= FFieldEnc( aCmpSits )
	Local nOpc			:= IIF( ::lGravado , 2 , 3 )
	Local aAbas			:= {}
	Local cNameGrp		:= ""
	
	CursorWait()
	
	If !Empty( ::ClienteGrupo )
		cNameGrp := Alltrim( GetAdvFVal( "SZ0" , "Z0_COD" , FWxFilial("SZ0") + ::ClienteGrupo  , 1 ) )
	EndIf

	dbSelectArea("DA0")
	DA0->( dbSetOrder(1) )
	DA0->( dbSeek( FWxFilial("DA0") + ::TabelaPedido  ) )

	dbSelectArea("ZC5")
	ZC5->( dbSetOrder(1) )
	ZC5->( dbSeek( FWxFilial("ZC5") + ::Prioridade  ) )

	oDlg := TDialog():New(alCoord[1],alCoord[2],alCoord[6],alCoord[5], "Pedido: " + ::NumPedido ,,,,,,,,,.T.)

		oLayer:Init(oDlg,.F.)
		oLayer:AddCollumn("ESQUERDA", 38,.F. )
		oLayer:AddCollumn("DIREITA",  62,.F. )

		//------------------------------------------------------------------
		// Define o layout da aba Esquerda
		//------------------------------------------------------------------
		oPainAlc	:= oLayer:GetColPanel('ESQUERDA')

		oLyAlc := FWLayer():New()
		oLyAlc:Init( oPainAlc, .F. )

		//Define a linha da Al�ada
		oLyAlc:AddLine( 'LnBrwAlc', 20, .F. )
		oLyAlc:AddCollumn( 'ColBrwAlc', 100, .T., 'LnBrwAlc' )

		//Define a linha do Descontos da Al�ada
		oLyAlc:AddLine( 'LnBrwDesc', 42, .F. )
		oLyAlc:AddCollumn( 'ColBrwDesc', 100, .T., 'LnBrwDesc' )

		//Define a linha do Prazo da Al�ada
		oLyAlc:AddLine( 'LnBrwPrz', 38, .F. )
		oLyAlc:AddCollumn( 'ColBrwPrz', 100, .T., 'LnBrwPrz' )

		oPanAlc		:= oLyAlc:GetColPanel('ColBrwAlc'	, 'LnBrwAlc'	)
		oPanDesc	:= oLyAlc:GetColPanel('ColBrwDesc'	, 'LnBrwDesc'	)
		oPanPrz		:= oLyAlc:GetColPanel('ColBrwPrz'	, 'LnBrwPrz'	)

		//Subdivide os painel para o Browse e a descri��o
		oPanNomAlc := TPanel():New( 0, 0, "", oPanAlc, Nil, .F., Nil, Nil, Nil,  0, 11, .F., .F. )
		oPanBrwAlc := TPanel():New( 0, 0, "", oPanAlc, Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )

		oPanNomDesc := TPanel():New( 0, 0, "", oPanDesc, Nil, .F., Nil, Nil, Nil,  0, 11, .F., .F. )
		oPanBrwDesc := TPanel():New( 0, 0, "", oPanDesc, Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )

		oPanNomPrz := TPanel():New( 0, 0, "", oPanPrz, Nil, .F., Nil, Nil, Nil,  0, 11, .F., .F. )
		oPanBrwPrz := TPanel():New( 0, 0, "", oPanPrz, Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )

		oPanNomAlc:Align := CONTROL_ALIGN_TOP
		oPanBrwAlc:Align := CONTROL_ALIGN_ALLCLIENT

		oPanNomDesc:Align := CONTROL_ALIGN_TOP
		oPanBrwDesc:Align := CONTROL_ALIGN_ALLCLIENT

		oPanNomPrz:Align := CONTROL_ALIGN_TOP
		oPanBrwPrz:Align := CONTROL_ALIGN_ALLCLIENT

		TSay():New( 002, 003, { || "Al�ada" }      	, oPanNomAlc	, Nil, oFont18N, Nil, Nil, Nil, .T., RGB(0,96,120), Nil, 200, 015, Nil, Nil, Nil, Nil, Nil, .T. )
		TSay():New( 002, 003, { || "Descontos" }	, oPanNomDesc	, Nil, oFont18N, Nil, Nil, Nil, .T., RGB(0,96,120), Nil, 200, 015, Nil, Nil, Nil, Nil, Nil, .T. )
		TSay():New( 002, 003, { || "Prazo" }		, oPanNomPrz	, Nil, oFont18N, Nil, Nil, Nil, .T., RGB(0,96,120), Nil, 200, 015, Nil, Nil, Nil, Nil, Nil, .T. )

		oGetAlc := MsNewGetDados():New(000,000,000,000, 1  ,"AllWaysTrue()","AllWaysTrue()",, {}   , /*1*/ ,/*9999*/,,,,oPanBrwAlc, aHdAlc , aClsAlc)
		oGetAlc:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
		oGetAlc:lInsert := .F.

		oGetDesc := MsNewGetDados():New(000,000,000,000, 1  ,"AllWaysTrue()","AllWaysTrue()",, {}   , /*1*/ ,/*9999*/,,,,oPanBrwDesc, aHdDesc, aClsDesc)
		oGetDesc:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
		oGetDesc:lInsert := .F.

		oGetPrz := MsNewGetDados():New(000,000,000,000, 1  ,"AllWaysTrue()","AllWaysTrue()",, {}   , /*1*/ ,/*9999*/,,,, oPanBrwPrz, aHdPrz, aClsPrz )
		oGetPrz:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
		oGetPrz:lInsert := .F.


		//------------------------------------------------------------------
		// Define o layout da aba Direito
		//------------------------------------------------------------------
		oPainPedV	:= oLayer:GetColPanel('DIREITA')
		oLyPed := FWLayer():New()
		oLyPed:Init( oPainPedV, .F. )

		//------------------------------------------------
		// Lado Direito Superior - Detalhes Pedido Venda
		//------------------------------------------------
		oLyPed:AddLine( 'LnUPPED'	, 50	, .F. )
		oLyPed:AddCollumn("CABEC"	,100	, .F. ,"LnUPPED" )
		oLyPed:AddWindow("CABEC"	,"PEDIDO"		,"Dados Pedidos"	,100	,.F.,.F.,{||},"LnUPPED",{||})
		oPainDetPV	:= oLyPed:GetWinPanel("CABEC"		,"PEDIDO" , "LnUPPED"	)

	
		aAbas	:= {"Pedido de Venda","Situa��es"}
		oFolder := TFolder():New(000,000,aAbas,Nil,oPainDetPV,,,,.T.,.F.,000,000)
		oFolder:Align := CONTROL_ALIGN_ALLCLIENT

		//------------------------------------------------
		// Folder 1 - "Pedido de Venda"
		//------------------------------------------------
		If nOpc == 3
			private aTELA[0][0]
			private aGETS[0]
		EndIf

		// Inicializa as variaves de Tela - MSMGET 1
		M->C5_NUM		:= ::NumPedido
		M->C5_XPVEXT	:= ::cNumPVExterno
		M->C5_CLIENTE	:= ::CodCliente
		M->C5_LOJACLI	:= ::LojaCliente
		M->A1_NOME		:= SA1->A1_NOME
		M->C5_TABELA	:= ::TabelaPedido
		M->DA0_DESCRI	:= DA0->DA0_DESCRI
		M->C5_DESC1		:= ::PercDescCliente
		M->C5_DESC2		:= ::PercDescNegociado
		M->C5_DESC3		:= ::PercDescComercial
		M->C5_TPFRETE	:= ::TipoFrete
		M->C5_FRETE		:= ::VlrFrete
		M->TOTDSCPERC	:= ::PercDescTotal
		M->TOTDSCVLR	:= ::VlrDescTotal
		M->C5_XPRIORI	:= ::Prioridade
		M->ZC5_DESC		:= ZC5->ZC5_DESC
		M->TABPADRAO	:= IIF(::TabelaPadCliente == "1", .T. , .F.)
		M->PEDIDOMAE	:= IIF(::PedidoMae == "1", .T. , .F.)
		M->UTILIPROMO	:= IIF(::UtilizaPromocao == "1", .T. , .F.)
		M->A1_XGRPEMP	:= ::ClienteGrupo
		M->ACY_DESCRI	:= cNameGrp
		M->ZC6_RESERV	:= ::Reserva
		M->ZC6_INTCOM	:= ::IntCompra

		oEncDetPV:= MSMGet():New(  ,,nOpc,/*aCRA*/,/*cLetras*/,/*cTexto*/,aCmpDetPV,{0,0,0,0},{} ,/*nModelo*/,/*nColMens*/,/*cMensagem*/, /*cTudoOk*/,;
									oFolder:aDialogs[1],/*lF3*/,.T./*lMemoria*/, .T. /*lColumn*/,/*caTela*/,/*lNoFolder*/,/*lProperty*/,aFieldPV,;
									/*aFolder*/,.F. /*lCreate*/,/*lNoMDIStretch*/,/*cTela*/, .T. )
		oEncDetPV:oBox:Align := CONTROL_ALIGN_ALLCLIENT


		//------------------------------------------
		// Folder 2 - Situa��es do Pedido de Venda
		//------------------------------------------
		oPanSitPV	:= TPanel():New( 0, 0, "", oFolder:aDialogs[2], Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )
		oPanSitPV:Align := CONTROL_ALIGN_ALLCLIENT

		oTBarSit := TBar():New( oPanSitPV,,,.T.,,,"",.F. )
		oTBarSit:Align := CONTROL_ALIGN_TOP
		If ::lGravado
			TBtnBmp2():New(00, 0, 32, 32,"bmpvisual"	,,,, {|| U_ADP12View() }	,oTBarSit,"Visualiza Situa��es"		,,.T. )
		EndIf

		If nOpc == 3
			private aTELA[0][0]
			private aGETS[0]
		EndIf
		M->C5_XALCADA := ::SituacaoAlcada
		M->C5_XBLQCRE := ::SituacaoCredito
		M->C5_XESTOQU := ::SituacaoEstoque
		M->C5_XSTCOAD := ::SituacaoComericial
		oEncSit:=  MSMGet():New(  ,,nOpc,/*aCRA*/,/*cLetras*/,/*cTexto*/,aCmpSits,{0,0,0,0},{} ,/*nModelo*/,/*nColMens*/,/*cMensagem*/, /*cTudoOk*/,;
									oPanSitPV,/*lF3*/,.T./*lMemoria*/, .T. /*lColumn*/,/*caTela*/,/*lNoFolder*/,/*lProperty*/,aFielSit,;
									/*aFolder*/,.F. /*lCreate*/,/*lNoMDIStretch*/,/*cTela*/, .T. )
		oEncSit:oBox:Align := CONTROL_ALIGN_ALLCLIENT
		


		//--------------------------------------------------------------------------
		// Lado Direito Inferior
		//--------------------------------------------------------------------------
		oLyPed:AddLine( 'LnDOWNPED', 50, .F. )
		oLyPed:AddCollumn( 'ColPED1', 50, .T., 'LnDOWNPED' )
		oLyPed:AddCollumn( 'ColPED2', 50, .T., 'LnDOWNPED' )


		//----------------------------------------------------------
		// Lado Direito Inferior Coluna Situa��o
		//----------------------------------------------------------
		oPanPed1	:= oLyPed:GetColPanel('ColPED1'	, 'LnDOWNPED'	)
		oLySitPV := FWLayer():New()
		oLySitPV:Init( oPanPed1, .F. )

		//------------------------------------------
		// Lado Direito Inferior - Primeira coluna
		//------------------------------------------
		oLySitPV:AddLine( 'LnVlrPED'	,100	, .F. )
		oLySitPV:AddCollumn("VALORPV"	,100	, .F. ,"LnVlrPED" )

		oColVlrPV	:= oLySitPV:GetColPanel('VALORPV'	, 'LnVlrPED'	)

		//------------------------------------------
		//Valor do Pedido de Venda
		//------------------------------------------
		oPanVlrPV	:= TPanel():New( 0, 0, "", oColVlrPV, Nil, .F., Nil, Nil, Nil,  0, 11, .F., .F. )
		oPanBrwPV	:= TPanel():New( 0, 0, "", oColVlrPV, Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )
		oPanVlrPV:Align := CONTROL_ALIGN_TOP
		oPanBrwPV:Align := CONTROL_ALIGN_ALLCLIENT
		TSay():New( 002, 003, { || "Valor Pedido de Venda" }    , oPanVlrPV	, Nil, oFont18N, Nil, Nil, Nil, .T., RGB(0,96,120), Nil, 200, 015, Nil, Nil, Nil, Nil, Nil, .T. )

		oGetVlrPed := MsNewGetDados():New(000,000,000,000, 1  ,"AllWaysTrue()","AllWaysTrue()",, {}   , /*1*/ ,/*9999*/,,,,oPanBrwPV, aHdVlrPed , aClsVlrPed)
		oGetVlrPed:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
		oGetVlrPed:lInsert := .F.


		//------------------------------------------
		// Lado Direito Inferior - Segunda coluna
		//------------------------------------------
		oPanPed2	:= oLyPed:GetColPanel('ColPED2'	, 'LnDOWNPED'	)
		oPanNomObs	:= TPanel():New( 0, 0, "", oPanPed2, Nil, .F., Nil, Nil, Nil,  0, 11, .F., .F. )
		oPanBrwObs	:= TPanel():New( 0, 0, "", oPanPed2, Nil, .F., Nil, Nil, Nil,  0,  0, .F., .F. )
		oPanNomObs:Align := CONTROL_ALIGN_TOP
		oPanBrwObs:Align := CONTROL_ALIGN_ALLCLIENT
		TSay():New( 002, 003, { || "Observa��es Al�ada" }	, oPanNomObs	, Nil, oFont18N, Nil, Nil, Nil, .T., RGB(0,96,120), Nil, 200, 015, Nil, Nil, Nil, Nil, Nil, .T. )
		
		oObsAlcada := tSimpleEditor():New(0, 0, oPanBrwObs , 0 , 0   )
		oObsAlcada:Load( cObsAlcada )		
		oObsAlcada:Align := CONTROL_ALIGN_ALLCLIENT

	Activate MsDialog oDlg Centered
	
	CursorArrow()

Return()

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:CampVariav
Metodo de De-Para dos atributos da classe para
o nome dos campos na tabela

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method CampVariav() Class STNAlcada

	Local aDePara	:= {}

	aadd( aDePara , { "ZCH_PEDIDO"	,"NumPedido"					})
	aadd( aDePara , { "ZCH_CLIENT"	,"CodCliente"					})
	aadd( aDePara , { "ZCH_LOJA"	,"LojaCliente"					})
	aadd( aDePara , { "ZCH_TABPED"	,"TabelaPedido"					})
	aadd( aDePara , { "ZCH_PDESCM"	,"PercDescComercial"			})
	aadd( aDePara , { "ZCH_PDESCL"	,"PercDescCliente"				})
	aadd( aDePara , { "ZCH_PDESNG"	,"PercDescNegociado"			})
	aadd( aDePara , { "ZCH_PDESTT"	,"PercDescTotal"				})
	aadd( aDePara , { "ZCH_VDESTT"	,"VlrDescTotal"					})
	aadd( aDePara , { "ZCH_VFRETE"	,"VlrFrete"						})
	aadd( aDePara , { "ZCH_TABCLI"	,"TabelaPadCliente"				})
	aadd( aDePara , { "ZCH_PEDMAE"	,"PedidoMae"					})
	aadd( aDePara , { "ZCH_UTLPRO"	,"UtilizaPromocao"				})
	aadd( aDePara , { "ZCH_TPFRET"	,"TipoFrete"					})
	aadd( aDePara , { "ZCH_PRIORI"	,"Prioridade"					})
	aadd( aDePara , { "ZCH_RESERV"	,"Reserva"						})
	aadd( aDePara , { "ZCH_INTCOM"	,"IntCompra"					})
	aadd( aDePara , { "ZCH_VBRUPV"	,"VlrBrutoPedido"				})
	aadd( aDePara , { "ZCH_VLIQPV"	,"VlrLiquidoPedido"				})
	aadd( aDePara , { "ZCH_PALCPR"	,"PercAlcadaPermitida"			})
	aadd( aDePara , { "ZCH_PALCUT"	,"PercAlcadaUtilizada"			})
	aadd( aDePara , { "ZCH_PALCDI"	,"PercDebCredAlcada"			})
	aadd( aDePara , { "ZCH_VLRALC"	,"VlrDebCredAlcada"				})
	aadd( aDePara , { "ZCH_PMARGE"	,"PercMargemTab"				})
	aadd( aDePara , { "ZCH_PCOMIS"	,"PercComissaoPermitida"		})
	aadd( aDePara , { "ZCH_PFRETE"	,"PercCustoFrete"				})
	aadd( aDePara , { "ZCH_PFINPR"	,"PercFinancPermitida"			})
	aadd( aDePara , { "ZCH_PPAGAN"	,"PercPagAntecipado"			})
	aadd( aDePara , { "ZCH_PCLIPR"	,"PercDescClientePermitida"		})
	aadd( aDePara , { "ZCH_PCLIUT"	,"PercDescClienteUtilizada"		})
	aadd( aDePara , { "ZCH_PCMRPP"	,"PercDescComercPolitPerm"		})
	aadd( aDePara , { "ZCH_PCMRPU"	,"PercDescComercPolitUtili"		})
	aadd( aDePara , { "ZCH_PPROMP"	,"PercDescPromocionalPerm"		})
	aadd( aDePara , { "ZCH_PPROMU"	,"PercDescPromocionalUtili"		})
	aadd( aDePara , { "ZCH_PLIMTP"	,"PercLimiteTrocaPermitida"		})
	aadd( aDePara , { "ZCH_PLIMTU"	,"PercLimiteTrocaUtilizada"		})
	aadd( aDePara , { "ZCH_PDISTP"	,"PercDisponTrocaPermitida"		})
	aadd( aDePara , { "ZCH_DPOLIP"	,"PrazoPoliticaPermitida"		})
	aadd( aDePara , { "ZCH_DPOLIU"	,"PrazoPoliticaUtilizada"		})
	aadd( aDePara , { "ZCH_DADIPO"	,"PrazoAdicionalPolitica"		})
	aadd( aDePara , { "ZCH_DADICO"	,"PrazoAdicionalComercial"		})
	aadd( aDePara , { "ZCH_DPROPP"	,"PrazoPromoPonderadoPerm"		})
	aadd( aDePara , { "ZCH_DPROPU"	,"PrazoPromoPonderadoUtili"		})
	aadd( aDePara , { "ZCH_DSALDO"	,"PrazoSaldoPromoc"				})
	aadd( aDePara , { "ZCH_PSLDPN"	,"PrazoPercSaldoPromoc"			})
	aadd( aDePara , { "ZCH_DPRZCL"	,"PrazoCliente"					})
	aadd( aDePara , { "ZCH_DENCAP"	,"PrazoLivreEncargoPermit"		})
	aadd( aDePara , { "ZCH_DENCAU"	,"PrazoLivreEncargoUtiliz"		})
	aadd( aDePara , { "ZCH_DPRZLQ"	,"PrazoLiquido"					})
	aadd( aDePara , { "ZCH_DFINAN"	,"DiasFinanceiros"				})
	aadd( aDePara , { "ZCH_DCNDPG"	,"DiasCondPagamento"			})
	aadd( aDePara , { "ZCH_PRZCLU"	,"PrazoUtilizadoCalculo"		})
	aadd( aDePara , { "ZCH_CLIGRP"	,"ClienteGrupo"					})
	aadd( aDePara , { "ZCH_CONDPG"	,"CodCondicaoPag"				})

Return aDePara

//-----------------------------------------------
/*/{Protheus.doc} FHdAlcada
Monta o Header para os grids de Al�adas para
serem apresentadas na tela de Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FHdAlcada( nOpc )

	Local aHeader 	:= {}
	Local cPict		:= ""
	Local nTam		:= 0
	Local nDec		:= 0
	Local cPermit	:= "Permitido"
	Local cUtiliz	:= "Utilizado"

	If nOpc == 1
		cPict		:= "@E 99,999,999,999.99"
		nTam		:= 14
		nDec		:= 2
	ElseIf nOpc == 2
		cPict		:= "@R 99.99%"
		nTam		:= 5
		nDec		:= 2
		cPermit		:= "Permitido em %"
		cUtiliz		:= "Utilizado em %"
	ElseIf nOpc == 3
		cPict		:= "999.99"
		nTam		:= 5
		nDec		:= 2
	EndIf

	Aadd( aHeader,{ "Variaveis", "XVARIAVEL",;
					"@!", 28 , 0,;
					, ;
					,;
					"C", ,  })


	Aadd( aHeader,{ cPermit, "PERMITIDO",;
					cPict , nTam , nDec ,;
					, ;
					,;
					"N", ,  })

	Aadd( aHeader,{ cUtiliz, "UTILIZADO",;
					cPict, nTam , nDec,;
					, ;
					,;
					"N", ,  })

Retur aHeader

//-----------------------------------------------
/*/{Protheus.doc} FHdPedido
Monta o Header para o grid de valores do pedido
de venda, para serem apresentadas na tela de
Al�ada

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FHdPedido()

	Local aHeader	:= {}

	Aadd( aHeader,{ "Variaveis", "XVARIAVEL",;
					"@!", 20 , 0,;
					, ;
					,;
					"C", ,  })


	Aadd( aHeader,{ "Valor", "PERMITIDO",;
					"@E 99,999,999,999.99" , 14 , 2 ,;
					, ;
					,;
					"N", ,  })

Return aHeader

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:GridAlcada
Monta o Acols do Grid de Al�ada - Tela de Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method GridAlcada() Class STNAlcada

	Local aCols		:= {}
	Local cCredDeb	:= "Cr�dito"
	Local nAlcAceit	:= ZC6->ZC6_PALCAD

	If ::PercDebCredAlcada < nAlcAceit

		cCredDeb := "D�bito"

	EndIf

	Aadd( aCols, { "Al�ada (%)" 				, ::PercAlcadaPermitida , ::PercAlcadaUtilizada , .F. } )
	Aadd( aCols, { cCredDeb+" de Al�ada (%)" 	, ::PercDebCredAlcada 	,						, .F. } )
	Aadd( aCols, { cCredDeb+" Em ($)" 			, ::VlrDebCredAlcada 	,						, .F. } )

Return aCols

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:GridDesconto
Monta o Acols do Grid de Descontos - Tela de Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method GridDesconto() Class STNAlcada
	Local aCols		:= {}
	Local cCusto	:= "Custo"
	Local cDebito	:= "D�bito"
	Local cCredito	:= "Cr�dito"
	Local cVariavel	:= ""

	Aadd( aCols, { "Margem Tabela" 						, ::PercMargemTab 				, 	 							, .F. } )

	cVariavel := IIF(::PercComissaoPermitida < 0 , cDebito , cCredito )
	Aadd( aCols, { cVariavel + " Comiss�o"				, ::PercComissaoPermitida		, 	 							, .F. } )

	cVariavel := IIF(::PercCustoFrete < 0 , cCusto , cCredito )
	Aadd( aCols, { cVariavel + " Frete" 				, ::PercCustoFrete 				, 	 							, .F. } )

	cVariavel := IIF(::PercFinancPermitida < 0 , cCusto , cCredito )
	Aadd( aCols, { cVariavel + " Financeiro" 			, ::PercFinancPermitida 		, 								, .F. } )

	Aadd( aCols, { cCredito + " Pagamento Antecipado"	, ::PercPagAntecipado 			, 	 							, .F. } )

	Aadd( aCols, { "Desconto Cliente" 					, ::PercDescClientePermitida	, ::PercDescClienteUtilizada	, .F. } )

	Aadd( aCols, { "Desc. Comercial Pol�tica" 			, ::PercDescComercPolitPerm 	, ::PercDescComercPolitUtili	, .F. } )

	cVariavel := IIF(::PercDescPromocionalPerm < 0 , cDebito , cCredito )
	Aadd( aCols, { cVariavel + " Desc Promocional"		, ::PercDescPromocionalPerm 	, ::PercDescPromocionalUtili	, .F. } )

	Aadd( aCols, { "Limite p/ Troca Prazo/Desc" 		, ::PercLimiteTrocaPermitida	, ::PercLimiteTrocaUtilizada	, .F. } )

	Aadd( aCols, { "% Disp.  p/ Troca Prazo/Desc." 		, ::PercDisponTrocaPermitida	, 	 							, .F. } )

Return aCols

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:GridPrazo
Monta o Acols do Grid de Prazos - Tela de Al�ada

@author DS2U (HFA)
@since 16/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method GridPrazo() Class STNAlcada

	Local aCols	:= {}

	Aadd( aCols, { "Prazo Pol�tica"					, ::PrazoPoliticaPermitida	, ::PrazoPoliticaUtilizada		, .F. } )
	Aadd( aCols, { "Prazo Adicional da Pol�tica"	, ::PrazoAdicionalPolitica	, 	 							, .F. } )
	Aadd( aCols, { "Prazo Adicional Comercial"		, ::PrazoAdicionalComercial	, 	 							, .F. } )
	Aadd( aCols, { "Prazo Promocional Ponderado"	, ::PrazoPromoPonderadoPerm	, ::PrazoPromoPonderadoUtili	, .F. } )
	Aadd( aCols, { "Saldo Prazo Promocional - Dias"	, ::PrazoSaldoPromoc		, 								, .F. } )
	Aadd( aCols, { "Saldo Prazo Promocional % "		, ::PrazoPercSaldoPromoc	, 								, .F. } )
	Aadd( aCols, { "Prazo do Cliente"				, ::PrazoCliente			, 								, .F. } )
	Aadd( aCols, { "Prazo Livre de Encargos"		, ::PrazoLivreEncargoPermit	, ::PrazoLivreEncargoUtiliz		, .F. } )
	Aadd( aCols, { "Prazo L�quido"					, ::PrazoLiquido			, 								, .F. } )

Return aCols

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:GridVlrPedido
Monta o Acols do Grid de Valores do pedido de venda
- Tela de Al�ada

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method GridVlrPedido() Class STNAlcada

	Local aCols	:= {}

	Aadd( aCols, { "$ Bruto do Pedido"		, ::VlrBrutoPedido		, .F. } )
	Aadd( aCols, { "$ L�quido do Pedido"	, ::VlrLiquidoPedido	, .F. } )

Return aCols

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:CalcItensPedido
Calculos Ponderado da Al�ada:
Prazo
Comiss�o
Margem
Promo��o

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method CalcItensPedido( )  Class STNAlcada

	Loca nC				:= 0
	Local nPsPrcTab		:= 0
	Local nPsVlrTot		:= 0
	Local nPsQuant		:= 0
	Local aAux			:= 0
	Local nPsPrmTrc		:= 0
	Local nPsDscPrm		:= 0
	Local nPsDscTrc		:= 0
	Local nPsUtPrzP		:= 0
	Local nPsUtDscP		:= 0
	Local nPsPrzPrm		:= 0
	Local nPsProd		:= 0
	Local nPesoItem		:= 0
	Local nVlrBruItm	:= 0
	Local nPrazoProm	:= 0
	Local nDescProm		:= 0
	Local nDescTrcP		:= 0
	Local nVlrtab100	:= 0
	Local nVlrMargem	:= 0
	Local nPsComsPV1	:= 0
	Local nPsComsPV2	:= 0
	Local nPsComsCd1	:= 0
	Local nPsComsCd2	:= 0
	Local nDifComs1		:= 0
	Local nDifComs2		:= 0
	Local nDifComiss	:= 0
	Local nPsPeso		:= 0
	Local nPsVlrBru		:= 0
	Local nPsPrzPnP		:= 0
	Local nPsPrzPnC		:= 0
	Local nPsDscPnP		:= 0
	Local nPsDscPnU		:= 0
	Local nPsTrcPnP		:= 0
	Local nPsMargem		:= 0
	Local nPsDifCom		:= 0

	//---------------------------------------------------------------------------------------------
	// Retorna o valor do Pedido Bruto de acordo com a Tabela de Pre�o do Pedido de venda
	// Com base nesse valor, � realizado as demais pondera��es 
	//---------------------------------------------------------------------------------------------
	::VlrBrutoPedido	:= 0
	::VlrLiquidoPedido	:= 0
	For nC := 1 To Len( ::ItensPedido )

		If nC == 1

			aAux := aClone( ::ItensPedido[ nC ] )

			nPsProd		:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_PRODUTO"	} )
			nPsPrcTab	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XPRCTAB"	} )
			nPsVlrTot	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_VALOR"  	} )
			nPsQuant	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_QTDVEN"	} )
			nPsPromo	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XIDPROM"	} )
			nPsPrmTrc	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XPTROCA"	} )
			nPsDscPrm	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XDSCPRM"	} )
			nPsDscTrc	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XDSCTRC"	} )
			nPsUtPrzP	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XUTPRZP"	} )
			nPsUtDscP	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XUTDSCP"	} )
			nPsPrzPrm	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XPRAZOP"	} )
			nPsComsPV1	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_COMIS1"	} )
			nPsComsPV2	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_COMIS2"	} )
			nPsComsCd1	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XCOMBS1"	} )
			nPsComsCd2	:= aScan( aAux , {|x| Alltrim( x[1] ) == "C6_XCOMBS2"	} )			
			nPsPeso		:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_PSPOND"	} )
			nPsVlrBru	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_BRTITM"	} )
			nPsPrzPnP	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_PRZPNP"	} )
			nPsPrzPnC	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_PRZPNC"	} )
			nPsDscPnP	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_DSCPNP"	} )
			nPsDscPnU	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_DSCPNU"	} )
			nPsTrcPnP	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_TRCPNP"	} )
			nPsMargem	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_MARGEM"	} )
			nPsDifCom	:= aScan( aAux , {|x| Alltrim( x[1] ) == "ZCM_DIFCOM"	} )
			
		EndIf

		::VlrBrutoPedido	+= ::ItensPedido[ nC ][ nPsPrcTab ][ 2 ] * ::ItensPedido[ nC ][ nPsQuant ][ 2 ]
		::VlrLiquidoPedido	+= ::ItensPedido[ nC ][ nPsVlrTot ][ 2 ]

		If !Empty( ::ItensPedido[ nC ][ nPsPromo ][ 2 ] ) 
			::UtilizaPromocao			:= "1"
		EndIf

	Next nC

	
	//-------------------------------------
	// Calculos Ponderados
	//-------------------------------------
	::PrazoPromoPonderadoPerm	:= 0
	::PercDescPromocionalPerm	:= 0
	::PercDescPromocionalUtili	:= 0
	::PercLimiteTrocaPermitida	:= 0
	::PercMargemTab				:= 0
	::PrazoUtilizadoCalculo		:= 0
	::PercComissaoPermitida		:= 0	
	For nC := 1 To Len( ::ItensPedido )

		nVlrBruItm	:= ( ::ItensPedido[ nC ][ nPsPrcTab ][ 2 ] * ::ItensPedido[ nC ][ nPsQuant ][ 2 ] ) 
		nPesoItem	:= ( nVlrBruItm / ::VlrBrutoPedido )
		::ItensPedido[ nC ][ nPsVlrBru ][ 2 ]	:= nVlrBruItm
		::ItensPedido[ nC ][ nPsPeso ][ 2 ] 	:= nPesoItem
		
		//----------------------------------------------------------------
		// ID da Promo��o estiver Preenchido
		//----------------------------------------------------------------
		If !Empty( ::ItensPedido[ nC ][ nPsPromo ][ 2 ] )

			//------------------------------
			// Prazo Promo��o
			//------------------------------
			nPrazoProm	:= ::ItensPedido[ nC ][ nPsPrzPrm ][ 2 ]
			
			// Permitido
			::ItensPedido[ nC ][ nPsPrzPnP ][ 2 ]	:= nPrazoProm * nPesoItem 
			::PrazoPromoPonderadoPerm				+= ::ItensPedido[ nC ][ nPsPrzPnP ][ 2 ]
			

			// Utilizado
			If ::ItensPedido[ nC ][ nPsUtPrzP ][ 2 ] == "S"
				::ItensPedido[ nC ][ nPsPrzPnC ][ 2 ]	:= nPrazoProm * nPesoItem
				::PrazoUtilizadoCalculo					+= ::ItensPedido[ nC ][ nPsPrzPnC ][ 2 ]
			EndIf			
		

			//------------------------------
			// Desconto Promo��o
			//------------------------------
			nDescProm	:= ::ItensPedido[ nC ][ nPsDscPrm ][ 2 ]
			
			//Permitido
			::ItensPedido[ nC ][ nPsDscPnP ][ 2 ]	:= nDescProm * nPesoItem
			::PercDescPromocionalPerm				+= ::ItensPedido[ nC ][ nPsDscPnP ][ 2 ]
		
			
			// Utilizado
			If ::ItensPedido[ nC ][ nPsUtDscP ][ 2 ] == "S"
				::ItensPedido[ nC ][ nPsDscPnU ][ 2 ]	:= nDescProm * nPesoItem 
				::PercDescPromocionalUtili				+= ::ItensPedido[ nC ][ nPsDscPnU ][ 2 ]
			EndIf
			
			//------------------------------
			// Troca Promo��o
			//------------------------------
			nDescTrcP	:= ::ItensPedido[ nC ][ nPsDscTrc ][ 2 ]
			
			If ::ItensPedido[ nC ][ nPsPrmTrc ][ 2 ] == "S"
				::ItensPedido[ nC ][ nPsTrcPnP ][ 2 ]	:= nDescTrcP * nPesoItem
				::PercLimiteTrocaPermitida				+= ::ItensPedido[ nC ][ nPsTrcPnP ][ 2 ]
			EndIf
							
		EndIf
		
		//------------------------------
		// Margem
		//------------------------------
		nVlrtab100	:= U_ADMVXFUN( 13 , ::ItensPedido[ nC ][ nPsProd ][ 2 ] )
		nVlrMargem	:= ( 1 - ( nVlrtab100 / ::ItensPedido[ nC ][ nPsPrcTab ][ 2 ] ) )
		
		::ItensPedido[ nC ][ nPsMargem ][ 2 ]	:= ( nVlrMargem * nPesoItem ) * 100
		::PercMargemTab	+= ::ItensPedido[ nC ][ nPsMargem ][ 2 ]
		
		//------------------------------
		// Comiss�o
		//------------------------------
		nDifComs1	:= ::ItensPedido[ nC ][ nPsComsCd1 ][ 2 ] - ::ItensPedido[ nC ][ nPsComsPV1 ][ 2 ]
		nDifComs2	:= ::ItensPedido[ nC ][ nPsComsCd2 ][ 2 ] - ::ItensPedido[ nC ][ nPsComsPV2 ][ 2 ]
		
		::ItensPedido[ nC ][ nPsDifCom ][ 2 ]	:= ( nDifComs1 + nDifComs2 ) * nPesoItem
		nDifComiss	+= ::ItensPedido[ nC ][ nPsDifCom ][ 2 ] 


	Next nC	

	::PrazoPromoPonderadoPerm	:= Round( ::PrazoPromoPonderadoPerm	, 0 ) 
	::PrazoUtilizadoCalculo		:= Round( ::PrazoUtilizadoCalculo 	, 0 )
	::PercDescPromocionalPerm	:= Round( ::PercDescPromocionalPerm	, 2 )
	::PercDescPromocionalUtili	:= Round( ::PercDescPromocionalUtili, 2 )	
	::PercLimiteTrocaPermitida	:= Round( ::PercLimiteTrocaPermitida, 2 )	
	::PercMargemTab				:= Round( ::PercMargemTab			, 2 )
	::PercComissaoPermitida		:= Round( nDifComiss * U_ADMVXFUN( 6, "ZC6_PDSCOM" ) , 2 )

Return

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:NumDiasCondPag
Retorna a quantidade de dias referente a uma
condi��o de pagamento

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method NumDiasCondPag( cCodCondic , dDataCalc )  Class STNAlcada

	Local nDias 	:= 0
	Local nC		:= 0
	Local aVencto	:= {}
	Local dtUltima	:= CtoD('  /  /  ')

	Default dDataCalc	:= MsDate()

	aVencto := Condicao( 10000 , cCodCondic , 0 , dDataCalc )

	If Len(aVencto) > 0
		
		For nC	:= 1 To Len(aVencto) 
			dtUltima:= aVencto[ nC ][1]
			nDias	+= DateDiffDay( dtUltima , dDataCalc -1 )
		Next nC
		
		 // Considerar o dia da emiss�o
		nDias	:= nDias / Len( aVencto )

	EndIf

Return nDias


//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:CustoFrete
Calculo o Custo/Credito da opera��o de Frete
do Pedido de Venda

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method CalcCustoFrete() Class STNAlcada

	Local nValor	:= 0

	dbSelectArea("ZC1")
	ZC1->( dbSetOrder( 1 ) ) //ZC1_FILIAL+ZC1_UF+ZC1_REGIAO

	If ZC1->( dbSeek( FWxFilial("ZC1") +  SA1->( A1_EST + A1_REGIAO ) ) )

		If Alltrim( ::TipoFrete ) == Alltrim( ZC1->ZC1_TPFRET ) .And. Alltrim( ::TipoFrete ) == "C"

			nValor := ZC1->ZC1_FRETE * (-1)

		ElseIf Alltrim( ::TipoFrete ) == Alltrim( ZC1->ZC1_TPFRET ) .And. Alltrim( ::TipoFrete ) == "F"

			nValor := 0

		ElseIf Alltrim( ::TipoFrete ) <> Alltrim( ZC1->ZC1_TPFRET ) .And. Alltrim( ::TipoFrete ) == "C"

			nValor := ZC1->ZC1_FRETE *(-1)

		ElseIf Alltrim( ::TipoFrete ) <> Alltrim( ZC1->ZC1_TPFRET ) .And. Alltrim( ::TipoFrete ) = "F"

			nValor := ZC1->ZC1_FRETE

		EndIf


	EndIf

Return nValor

//-----------------------------------------------
/*/{Protheus.doc} STNAlcada:CalcFinanceiro
Calculo o Custo/Credito Financeiro do
do Pedido de Venda

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Method CalcFinanceiro() Class STNAlcada

	Local nValor	:= 0
	Local nFator	:= ZC6->ZC6_FATDIA
	Local nDescFin	:= ZC6->ZC6_PMAXFI
	Local nFinPed	:= 0

	nFinPed := ( ( nFator ^ IIF( ::DiasFinanceiros < 0, ::DiasFinanceiros * ( -1 ),  ::DiasFinanceiros ) -1 ) * 100 )

	If ::DiasFinanceiros < 0

		nFinPed := nFinPed * ( -1 )

	Endif

	nValor := Round( nDescFin - nFinPed , 2)

Return nValor

//-----------------------------------------------
/*/{Protheus.doc} FCmpSitPed
Campos que devem aparecer na enchoice de Situa��o
do pedido de venda - Tela de Al�ada

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FCmpSitPed()

	Local aCmpSits	:= {}

	aadd( aCmpSits , "C5_XALCADA"	)
	aadd( aCmpSits , "C5_XBLQCRE"	)
	aadd( aCmpSits , "C5_XESTOQU"	)
	aadd( aCmpSits , "C5_XSTCOAD"	)
	aadd( aCmpSits , "NOUSER"		)

Return aCmpSits

//-----------------------------------------------
/*/{Protheus.doc} FCmpDetPed
Campos que devem aparecer na enchoice de Detalhes
do pedido de venda - Tela de Al�ada

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FCmpDetPed()
	Local aCmpDetPV	:= {}

	aadd( aCmpDetPV , "C5_NUM"		)
	aadd( aCmpDetPV , "C5_XPVEXT"	)
	aadd( aCmpDetPV , "C5_CLIENTE"	)
	aadd( aCmpDetPV , "C5_LOJACLI"	)
	aadd( aCmpDetPV , "A1_NOME"		)
	aadd( aCmpDetPV , "C5_TABELA"	)
	aadd( aCmpDetPV , "DA0_DESCRI"	)
	aadd( aCmpDetPV , "C5_DESC1"	)
	aadd( aCmpDetPV , "C5_DESC2"	)
	aadd( aCmpDetPV , "C5_DESC3"	)
	aadd( aCmpDetPV , "C5_TPFRETE"	)
	aadd( aCmpDetPV , "C5_FRETE"	)
	aadd( aCmpDetPV , "TOTDSCPERC"	)
	aadd( aCmpDetPV , "TOTDSCVLR"	)
	aadd( aCmpDetPV , "C5_XPRIORI"	)
	aadd( aCmpDetPV , "ZC5_DESC"	)
	aadd( aCmpDetPV , "TABPADRAO"	)
	aadd( aCmpDetPV , "PEDIDOMAE"	)
	aadd( aCmpDetPV , "UTILIPROMO"	)
	aadd( aCmpDetPV , "A1_XGRPEMP"	)
	aadd( aCmpDetPV , "ACY_DESCRI"	)
	aadd( aCmpDetPV , "ZC6_RESERV"	)
	aadd( aCmpDetPV , "ZC6_INTCOM"	)
	aadd( aCmpDetPV , "NOUSER"		)

Retur aCmpDetPV

//-----------------------------------------------
/*/{Protheus.doc} FFieldEnc
Tratamento para os campos que v�o aparecer nas
enchoice da tela, pois a enchoice � de campos
especificos, e n�o de um Alias especifico
- Tela de Al�ada

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FFieldEnc( aCampos )

	Local nC			:= 0
	Local aField		:= {}
	Local cTitulo		:= ""
	Local cTipo			:= ""
	Local nTamanho		:= 0
	Local nDecimal		:= 0
	Local cPict			:= ""
	Local cIniPad		:= ""
	Local cF3			:= ""
	Local cWhen			:= ""
	Local cCombBox		:= ""
	Local cPictVar		:= ""
	Local cTrigger		:= ""
	Local aCmpEsp		:= {}
	Local nPos			:= 0

	aadd( aCmpEsp ,{ "TOTDSCPERC"	,"Total Desc %:"		,"N",05,2,PesqPict("SC6","C6_DESCONT")	,""	})
	aadd( aCmpEsp ,{ "TOTDSCVLR"	,"Total Desc $:"		,"N",14,2,PesqPict("SC6","C6_VALDESC")	,""	})
	aadd( aCmpEsp ,{ "PEDIDOMAE"	,"Pedido M�e"			,"L",01,0,""							,""	})
	aadd( aCmpEsp ,{ "TABPADRAO"	,"Tabela Padr�o Cliente","L",01,0,""							,""	})
	aadd( aCmpEsp ,{ "UTILIPROMO"	,"Utiliza Promo��o"		,"L",01,0,""							,""	})
	aadd( aCmpEsp ,{ "CLIEGRUPO"	,"Cliente do Grupo"		,"L",01,0,""							,""	})

	DbSelectArea("SX3")
	SX3->( dbSetOrder(2) )

	For nC := 1 To ( Len(aCampos)-1 )
		If SX3->( dbSeek( aCampos[ nC ] ) )

			Aadd(aField, {X3TITULO()			,;
							SX3->X3_CAMPO		,;
							SX3->X3_TIPO		,;
							SX3->X3_TAMANHO		,;
							SX3->X3_DECIMAL		,;
							SX3->X3_PICTURE		,;
							Nil					,;
							.F.					,;
							SX3->X3_NIVEL		,;
							SX3->X3_RELACAO		,;
							SX3->X3_F3			,;
							SX3->X3_WHEN		,;
							.F.					,;
							.F.					,;
							SX3->X3_CBOX		,;
							Nil					,;
							.F.					,;
							SX3->X3_PICTVAR		,;
							SX3->X3_TRIGGER		})

		Else

			nPos := aScan( aCmpEsp, {|x| x[1] == aCampos[ nC ] } )

			cTitulo		:= aCmpEsp[ nPos ][ 2 ]
			cTipo		:= aCmpEsp[ nPos ][ 3 ]
			nTamanho	:= aCmpEsp[ nPos ][ 4 ]
			nDecimal	:= aCmpEsp[ nPos ][ 5 ]
			cPict		:= aCmpEsp[ nPos ][ 6 ]
			cCombBox	:= aCmpEsp[ nPos ][ 7 ]

			Aadd(aField, {	cTitulo				,;
							aCampos[ nC ]		,;
							cTipo				,;
							nTamanho			,;
							nDecimal			,;
							cPict				,;
							Nil					,;
							.F.					,;
							0					,;
							cIniPad				,;
							cF3					,;
							cWhen				,;
							.F.					,;
							.F.					,;
							cCombBox			,;
							Nil					,;
							.F.					,;
							cPictVar			,;
							cTrigger			})

		EndIf

	NExt nC

Return aField



//-----------------------------------------------
/*/{Protheus.doc} FMailAlcPV
Atualiza a Al�ada no Pedido de vendas e envia
WF caso necess�rio

@author DS2U (HFA)
@since 17/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FMailAlcPV( cSitAlc  , cObs )

    Local aAreaSC5      := SC5->(GetArea())
    Local cSuperv		:= ""
    Local cMailSup		:= ""
    Local cMailCC		:= ""
    Local oAlcada		:= Nil

	//Envia e-mail para o Supervisor do vendedor que incluiu o pedido de venda
	If cSitAlc == "N" // N�o Aprovado
		
		cSuperv		:= GetSuperv( SC5->C5_VEND1 )
		cMailSup	:= Alltrim( GetAdvFVal( "SA3" , "A3_EMAIL" , FWxFilial("SA3")+ cSuperv , 1 ) )
		cMailCC		:= Alltrim( GetAdvFVal( "SA3" , "A3_EMAIL" , FWxFilial("SA3")+ SC5->C5_VEND1 , 1 ) )
	
		If !IsBlind()
			FWMsgRun( ,{|| U_ADP8Mail( 1 , cSuperv , cMailSup , cMailCC , SC5->C5_NUM , SC5->C5_CLIENTE , SC5->C5_LOJACLI , cObs ) },"Aguarde...", "Enviando e-mail de bloqueio de Al�ada")
		Else
			U_ADP8Mail( 1 , cSuperv , cMailSup , cMailCC , SC5->C5_NUM , SC5->C5_CLIENTE , SC5->C5_LOJACLI , cObs )
		EndIf
	
	Endif

    RestArea( aAreaSC5 )

Return    