#include 'protheus.ch'
#include 'parmtype.ch'
#include "fileio.ch"
#include "TopConn.ch"


/*/{Protheus.doc} ADMVP04
//Rotina para gera��o do Pedido M�e de Vendas. 
@author DS2U [L. Fini]
@since 25/01/2019
@version 1.0
@type function
/*/
User Function ADMVP12()

Local aSC5Area 		:= SC5->(GetArea())
Local cArqCSV 		:= "" 
Local lOk			:= .F. 

// ------------------------------------------------------------------------------------
// Rotina que validar� se o pedido � m�e e/ou pode ser distribuido
lOk := FVldPm()

If lOk 
	If MsgYesNo("Deseja realizar Distribui��o deste Pedido M�e: " + AllTrim(SC5->C5_FILIAL ) + " - " + AllTrim(SC5->C5_NUM )  ) 
		cArqCSV := AllTrim(cGetFile( "Arquivo csv|*.csv",  "Procurar" , 0,, .T., GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE, .T. )) 
		If !Empty(cArqCSV)
			
			FWMsgRun(, {||  FPrepPeds(cArqCSV) }, "Pedido M�e de Vendas", "Aguarde... Lendo arquivo CSV...")
			
		EndIf 
		
	EndIf 
EndIf


SC5->(RestArea(aSC5Area)) 

return nil 


/*/{Protheus.doc} FGeraPeds
//TODO Descri��o auto-gerada.
@author DS2U [L. Fini]
@since 25/01/2019
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FPrepPeds(cArqCSV)

Local oMemo		:= NIL 
Local oDlg		:= NIL 
Local cTexto	:= "" 
Local cMask     := "Arquivos Texto" + "(*.TXT)|*.txt|"
Local lContinua	:= .T. 
Local cMsgErro 	:= "" 
Local aDadosCSV	:= {} 
Local cArqLog 	:= "" 

Default cArqCSV	:= "" 

// ------------------------------------------------
// arquivo de Log 
cArqLog := SubStr(AllTrim(cArqCSV),1,At(".csv",cArqCSV))+"_log_"+StrTran(AllTrim(Time()),":","")+"_.csv"


// ------------------------------------------------------------------
// Validacoes 
// ------------------------------------------------------------------
If !File(cArqCSV) 
	lContinua 	:= !lContinua 
	cMsgErro 	:= "N�o encontrado o Arquivo " + AllTrim(cArqCSV) + ". Verifique o Arquivo selecionado e tente novamente." 
EndIf 



// --------------------------
// Processa 
If lContinua 

	lContinua := LeArqCSV(cArqCSV,cArqLog)
	
	If lContinua 
		FWMsgRun(, {||  GeraPVs(SC5->C5_NUM) }, "Pedido M�e de Vendas", "Aguarde... Gerando Pedidos Filhos...")
	EndIf 

EndIf 

// --------------------------------------------
// Exibe mensagem de Erro 
If !lContinua .and. !Empty(cMsgErro)
	Aviso("Aviso", cMsgErro , {"Voltar"}, 2)	
EndIf 

Return Nil 



/*/{Protheus.doc} LeArqCSV
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}
@param cArqCSV, characters, descricao
@type function
/*/
Static Function LeArqCSV(cArqCSV,cArqLog)

Local aSA1Area		:= SA1->(GetArea())
Local aSB1Area 		:= SB1->(GetArea())
Local cCpoGrpVen	:= "A1_XGRPEMP" 			// Campo que defini grupo de vendas, grupo empresarial, antigo A1_GRPVEN - Substituido na rotina em 23/07/18  
Local aDados 		:= {} 
Local aRet 			:= {} 
Local nHD1 			:= 0  
Local cCNPJ 		:= ""
Local cProdut 		:= ""
Local nQtd  		:= 0
Local cPedCli 		:= ""
Local cPedMae		:= ""
Local cIdZCR		:= ""
Local cLinha		:= "" 
Local cSeparador	:= ";"
Local cLogLine		:= "" 
Local cGrpCliPM		:= AllTrim(Posicione("SA1",1,xFilial("SA1")+ SC5->(C5_CLIENTE+C5_LOJACLI) , cCpoGrpVen ))
Local cFlagNotOk	:= "ERRO"
Local cFlagOk		:= "SUCESSO"
Local nTamProd		:= TamSX3("B1_COD")[1]
Local nTamCGC		:= TamSX3("A1_CGC")[1]
Local aDados		:= {}

Local cCodCli		:= ""									// Codigo do cliente encontrado pelo CNPJ do arquivo CSV 
Local cLojaCli		:= "" 									// Loja do cliente encontrado pelo CNPJ do arquivo CSV 
Local nRecnoSC6		:= 0 									// Recno do item encontrado no pedido de vendas original 
	
// ---------------------------------------------------------
// Posicionamento  das colunas - Posi��o arq. texto 
Local nPosCGC	:= 1
Local nPosProd	:= 2
Local nPosQtd	:= 3 
Local nPosPCli	:= 4 

nHdl := FT_FUSE(cArqCSV)

If nHdl <> -1

	//nBytes := FSeek(nHdl, 0, 2)
	//FSeek(nHdl, 0)
	ProcRegua(FT_FLASTREC())
	FT_FGOTOP()
		While !FT_FEOF()
			
			If FT_FRecno() = 1 //Pula o cabe�alho
				FT_FSKIP()
			EndIf
			
			cLinha := FT_FREADLN()
			
			aDados 	:= Separa(cLinha,cSeparador,.T.)
			
			cCNPJ   := aDados[nPosCGC]
			cProdut := aDados[nPosProd]
			nQtd    := aDados[nPosQtd]
			cPedCli := aDados[nPosPCli]
			cPedMae	:= SC5->C5_NUM
			
			//----- EFETUA VALIDA�OES DE CLIENTE, PRODUTO E QUANTIDADES ----//
			lContinua := Valid(@cCNPJ,@cProdut,@nQtd,@cPedCli,cSeparador,cArqLog, @cCodCli, @cLojaCli)
			
			If lContinua 
				Begin Transaction
					DbSelectArea("ZCR")
					ZCR->(DbSetOrder(1))//ZCR_FILIAL + ZCR_CGC + ZCR_PRODUT
					Reclock("ZCR", .T.)
						ZCR->ZCR_FILIAL := xFilial("SC5")
						cIdZCR := GetSx8Num("ZCR","ZCR_ID")
						ConfirmSX8()
						ZCR->ZCR_ID := cIdZCR
						ZCR->ZCR_CGC := cCNPJ
						ZCR->ZCR_CODCLI := cCodCli
						ZCR->ZCR_LOJCLI	:= cLojaCli
						ZCR->ZCR_PRODUT := cProdut
						ZCR->ZCR_QUANTI := nQtd
						ZCR->ZCR_PEDMAE := cPedMae
						ZCR->ZCR_PEDCLI := cPedCli
						ZCR->ZCR_PROCES := "N"
						ZCR->ZCR_DTINCL := dDataBase
					ZCR->(MsUnlock())
				End Transaction
			EndIf 
		FT_FSKIP()
		EndDo				
	ZCR->(DbCloseArea())
	FT_FUSE()
EndIf 


SB1->(RestArea(aSB1Area))
SA1->(RestArea(aSA1Area))

Return lContinua  


/*/{Protheus.doc} FVldPm
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FVldPm()	

Local lRet 		:= .T. 
Local cMsg		:= "" 
Local cCpoGrpVen	:= "A1_XGRPEMP" 
Local cGrpCliPM		:= AllTrim(Posicione("SA1",1,xFilial("SA1")+ SC5->(C5_CLIENTE+C5_LOJACLI) ,cCpoGrpVen ))

// ----------------------------------------------------------------
// Verifica se o pedido posicionado � o pedido m�e 
If !("1" $ AllTrim(SC5->C5_XISPM)) 
	lRet := .F. 
	cMsg := "O Pedido posicionado " + AllTrim(SC5->C5_FILIAL) + " - " + AllTrim(SC5->C5_NUM) + " n�o � um pedido m�e. " + CRLF + "Verifique a propriedade do pedido."
EndIf 


// -----------------------------------------------------------
// Verifica se o cliente do PM tem grupo de venda amarrado. 
If lRet .and. Empty(cGrpCliPM)
	lRet := .F. 
	cMsg := "O Cliente " + AllTrim( SC5->(C5_CLIENTE+C5_LOJACLI) ) + " n�o possui um grupo de venda atrelado." + CRLF + "Verifique o campo " + cCpoGrpVen + " no cadastro de Clientes."
EndIf 

// ---------------------------------------
// Msg de retorno 
If !lRet .and. !Empty(cMsg)
	Aviso("Aviso", cMsg , {"Voltar"}, 2)	
EndIf 



Return lRet 


/*/{Protheus.doc} FGetSc6
//NOPC = 1 -> Verifica se o produto existe no pedido m�e
  NOPC = 2 -> Verifica se saldo do txt � maior que no pedido m�e
@author DS2U [L. FINI]
@since 24/01/2019
@version 1.0
@return lRet

@type function
/*/
Static Function FGetSc6(nOpc,cPedido, cProduto, nQtdVen)

Local lRet 		:= .T.
Local aItensSC6 := {}
Local cQuery	:= ""
Local cAliasSC6 := GetNextAlias()

If nOpc == 1

	DbSelectArea("SC6")
	SC6->(DbSetOrder(2)) //C6_FILIAL + C6_PRODUTO + C6_NUM
	If !( SC6->( dbSeek( xFilial("SC6") + cProduto + cPedido ) ) )
		lRet := .F.
	EndIf

SC6->(DbCloseArea())

ElseIf nOpc == 2
	
	cQuery := "SELECT C6_PRODUTO, SUM(C6_QTDVEN) AS QTDVEN, SUM(C6_QTDENT) AS QTDENT FROM "+RetSqlName('SC6')+" SC6 "
	cQuery += "WHERE C6_PRODUTO = '" + cProduto +" ' "
	cQuery += "AND C6_NUM = '" + cPedido +" ' "
	cQuery += "AND C6_FILIAL = '" + xFilial("SC6") +" ' "
	cQuery += "AND D_E_L_E_T_ = ' ' "
	cQuery += "GROUP BY C6_PRODUTO, C6_NUM "
	
	DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasSC6,.T.,.T.)
	
	While (cAliasSC6)->(!EOF())
		If ((cAliasSC6)->QTDVEN - (cAliasSC6)->QTDENT) < nQtdven
			lRet := .F.
		EndIf
		(cAliasSC6)->(DbSkip())
	EndDo
	
	(cAliasSC6)->(DbCloseArea())
EndIf

Return lRet

/*/{Protheus.doc} Valid
//Valida CNPJ, Produto e Quantidade digitada no arquivo.
@author DS2U [L. Fini]
@since 24/01/2019
@version 1.0
@return lContinua

@type function
/*/
Static Function Valid(cCNPJ,cProdut,nQtd,cPedCli,cSeparador,cArqLog,cCodCli,cLojaCli)

Local cCpoGrpVen	:= "A1_XGRPEMP" 
Local cGrpCliPM		:= AllTrim(Posicione("SA1",1,xFilial("SA1")+ SC5->(C5_CLIENTE+C5_LOJACLI) ,cCpoGrpVen ))
Local cFlagNotOk	:= "ERRO"
Local cFlagOk		:= "SUCESSO"
Local cLogLine		:= ""
Local nLinha		:= 1
Local nTamProd		:= TamSX3("B1_COD")[1]
Local nTamCGC		:= TamSX3("A1_CGC")[1]
Local lContinua		:= .T.
Local lRetSC6		:= .T.

//------VALIDA CNPJ-----//
cCNPJ	 := PadR(AllTrim(cCNPJ),nTamCGC) 
SA1->(dbSetOrder(3)) // A1_FILIAL + A1_CGC 
SA1->(dbGoTop())

If !SA1->( MsSeek( xFilial("SA1") +  AllTrim(cCNPJ) )  ) 

	lContinua 	:= .F.
	cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
	cLogLine	+= "Cliente com CNPJ: " +  AllTrim(cCNPJ) + " n�o encontrado na base de dados."
	u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	

Else

	// Valida se o registro no arquivo CSV � igual ao Grupo de venda do cliente do PM 
	If AllTrim(cGrpCliPM) <> Alltrim(SA1->&cCpoGrpVen)
	
		lContinua 	:= .F.
		cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
		cLogLine	+= "Cliente com CNPJ: " +  AllTrim(cCNPJ) + " possui um grupo "
		u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	

	Else 
	
		cCodCli		:= SA1->A1_COD 
		cLojaCli 	:= SA1->A1_LOJA
		
	EndIf 
EndIf 

//-----VERIFICA SE PRODUTO EXISTE NA BASE------//
If lContinua
	cProdut	 := PadR(AllTrim(cProdut),nTamProd) 
	SB1->(dbSetOrder(1)) // B1_FILIAL + B1_COD 
	SB1->(dbGoTop())
	If lContinua .And. !(SB1->(MsSeek( xFilial("SB1") + cProdut     ))) 
		lContinua 	:= .F.
		cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
		cLogLine	+= "Produto " +  AllTrim(cProdut) + " n�o encontrado na base de dados."
		u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
	EndIf  
EndIf

// -----Valida Quantidade informada no arquivo ser V�lida ----//
If lContinua 


	nQtd	:= Val(nQtd) 
	If nQtd <= 0 
		lContinua 	:= .F.
		cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
		cLogLine	+= "Valor < = zero(0). N�o necess�rio realizar a transferencia" 
		u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
	EndIf		

EndIf

//---- Validando se o produto do arquivo CSV existe no pedido de venda -----/
If lContinua  
	lRetSC6 := FGetSc6(1,SC5->C5_NUM, cProdut,nQtd) // SC6 - indice 2 - C6_FILIAL + C6_PRODUTO + C6_NUM 
	If !lRetSC6
	
		lContinua 	:= .F.
		cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
		cLogLine	+= "N�o existe itens no pedido de vendas M�e " + AllTrim(SC5->C5_NUM) + " com o produto " + cProdut 
		u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
	
	EndIf 
EndIf 
	

// Valida Saldo  - Caso a quantidade no arquivo csv seja maior que a quantidade disponivel n�o distribui nada
If lContinua 
	lRetSC6 := FGetSc6(2,SC5->C5_NUM, cProdut,nQtd) // SC6 - indice 2 - C6_FILIAL + C6_PRODUTO + C6_NUM 
	If !lRetSC6
	
		lContinua 	:= .F.
		cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
		cLogLine	+= "Saldo no pedido de vendas M�e " + AllTrim(SC5->C5_NUM) + " com o produto " + cProdut + "Menor do que no arquivo."
		u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
	
	EndIf 

EndIf 

Return lContinua



/*/{Protheus.doc} GeraPVs
Gera o pedido de venda conforme fila de processamento (ZCR)
@author DS2U [L. Fini]
@since 25/01/2019
@version 1.0
@return ${return}, ${return_description}
@param cPedMae, characters, descricao
@type function
/*/
Static Function GeraPVs(cPedMae)

Local cQuery := ""
Local cQryPV := ""
Local cAliasZCR := GetNextAlias()
Local cAliasPV	:= GetNextAlias()
Local nQtd		:= 0
Local aRecnoZCR	:= {}
Local aCabec := {}
Local aItens := {}
Local aReg	 := {}
local cItem	 := "01"
Local nPrcVen  := 0
Local nX	   := 1
Local nI	   := 1
Local aAux	   := {}
Local aProdQtd := {}
Local aSetPed  := {}
Local cPedMsg  := ""
Local cPedido  := ""

PRIVATE lMsErroAuto := .F.

cQuery := "SELECT ZCR_FILIAL, ZCR_CGC, ZCR_CODCLI, ZCR_LOJCLI, ZCR_PEDMAE, R_E_C_N_O_ FROM "+RetSqlName('ZCR')+" ZCR "
cQuery += "WHERE ZCR_PEDMAE = '" + cPedMae +" ' "
cQuery += "AND ZCR_PROCES = 'N' "
cQuery += "AND ZCR_DTINCL = '" + DtoS(dDataBase) +" ' "
cQuery += "AND D_E_L_E_T_ = '' "
cQuery += "AND ZCR_FILIAL = '" + xFilial("ZCR") +" ' "
cQuery += "ORDER BY ZCR_CGC, ZCR_CODCLI, ZCR_LOJCLI "
	
DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasZCR,.T.,.T.)

While (cAliasZCR)->(!EOF())

	If aScan(aRecnoZCR,(cAliasZCR)->R_E_C_N_O_) == 0
		
		aCabec := {	{"C5_FILIAL"    ,xFilial("SC5")	     		,Nil},;
				{"C5_TIPO"      , "N"          					,Nil},;
				{"C5_CLIENTE"   , (cAliasZCR)->ZCR_CODCLI			,Nil},;
				{"C5_LOJACLI"   , (cAliasZCR)->ZCR_LOJCLI			,Nil},;
				{"C5_CONDPAG"   , Posicione("SC5",1,xFilial("SC5") +cPedMae ,"C5_CONDPAG")	,Nil},;
				{"C5_TIPOCLI" 	, Posicione("SC5",1,xFilial("SC5") +cPedMae ,"C5_TIPOCLI")	,Nil},;
				{"C5_XCANAL" 	, ""   ,Nil},;
				{"C5_CLIENT" 	, (cAliasZCR)->ZCR_CODCLI									,Nil},;
				{"C5_LOJAENT"   , (cAliasZCR)->ZCR_LOJCLI									,Nil},; 
				{"C5_XISPF"     , "1"															,Nil} } //PEDIDO FILHO = SIM
	
		cQryPV := "SELECT ZCR_FILIAL, ZCR_CGC, ZCR_CODCLI, ZCR_LOJCLI, ZCR_PEDMAE, ZCR_PEDCLI, ZCR_PRODUT, ZCR_QUANTI, R_E_C_N_O_ FROM "+RetSqlName('ZCR')+" ZCR "
		cQryPV += "WHERE ZCR_PEDMAE = '" + cPedMae +" ' "
		cQryPV += "AND ZCR_PROCES = 'N' "
		cQryPV += "AND ZCR_DTINCL = '" + DtoS(dDataBase) +" ' "
		cQryPV += "AND ZCR_CGC = '" + (cAliasZCR)->ZCR_CGC +" ' "
		cQryPV += "AND ZCR_FILIAL = '" + xFilial("ZCR") +" ' "
		cQryPV += "AND D_E_L_E_T_ = '' "
		cQryPV += "ORDER BY ZCR_PRODUT "
		
		DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQryPV),cAliasPV,.T.,.T.)
		
		While (cAliasPV)->(!EOF())
			aAdd(aRecnoZCR, (cAliasPV)->R_E_C_N_O_)
			aAdd(aSetPed, (cAliasPV)->R_E_C_N_O_)
			
			 aReg := {}
			 aAux := {}
			 
			 nPrcVen := Posicione("SC6",2,xFilial("SC6") + (cAliasPV)->ZCR_PRODUT +cPedMae,"C6_PRCVEN")
	         aAdd(aReg,{"C6_ITEM"   ,cItem          																,NIL}) // Item sequencial
	         aAdd(aReg,{"C6_PRODUTO",(cAliasPV)->ZCR_PRODUT 														,nil})    // Cod.Item
	         aAdd(aReg,{"C6_UM"     ,Posicione("SB1",1,xFilial("SB1")+ (cAliasPV)->ZCR_PRODUT,"B1_UM")				,nil})    // Unidade
	         aAdd(aReg,{"C6_QTDVEN" ,(cAliasPV)->ZCR_QUANTI 														,nil})                  // Quantidade
	         aAdd(aReg,{"C6_PRCVEN" ,nPrcVen 																		,nil})    // Preco Unit.
	         aAdd(aReg,{"C6_PRUNIT" ,nPrcVen																		,nil})    // Preco Unit.
	         aAdd(aReg,{"C6_VALOR"  ,nPrcVen * (cAliasPV)->ZCR_QUANTI      											,nil})    // Valor Tot.  
	         aAdd(aReg,{"C6_TES" 	,Posicione("SC6",2,xFilial("SC6") + (cAliasPV)->ZCR_PRODUT +cPedMae,"C6_TES") 	,Nil})  // Tipo de Saida ...      
	         aAdd(aReg,{"C6_CF"  	,Posicione("SC6",2,xFilial("SC6") + (cAliasPV)->ZCR_PRODUT +cPedMae,"C6_CF")  	,Nil})  // Codigo Fiscal ...                  
	         aAdd(aReg,{"C6_LOCAL"  ,Posicione("SC6",2,xFilial("SC6") + (cAliasPV)->ZCR_PRODUT +cPedMae,"C6_LOCAL"),nil}) // Almoxarifado
	         aAdd(aReg,{"C6_XNUMPM" ,cPedMae   																		,nil}) // NF. Origem.
	         aAdd(aReg,{"C6_XITEMPM",Posicione("SC6",2,xFilial("SC6") + (cAliasPV)->ZCR_PRODUT +cPedMae,"C6_ITEM")	,nil}) // Serie Origem.
	         aAdd(aReg,{"C6_PEDCLI" ,(cAliasPV)->ZCR_PEDCLI															,nil}) // Serie Origem.
	         aAdd(aReg,{"C6_XQTORIG",(cAliasPV)->ZCR_QUANTI															,nil}) // Serie Origem.
	         
	         aAdd(aAux,{(cAliasPV)->ZCR_PRODUT,(cAliasPV)->ZCR_QUANTI}) // Serie Origem.
	         
	         aAdd(aProdQtd,aClone(aAux))
	         aAdd(aItens,aClone(aReg))
	         cItem := SomaIt(cItem)	
			
			(cAliasPV)->(DbSkip())
		EndDo
			(cAliasPV)->(DbCloseArea())
			
		Begin Transaction
		
			msExecAuto({|x,y,z|MATA410(x,y,z)},aCabec,aItens,3)

			ROLLBACKSX8()

			If lMSErroAuto
				MOSTRAERRO()
			Else
				
				cPedMsg += " [" + SC5->C5_NUM + "] "
				cPedido := SC5->C5_NUM 
				
				DbSelectArea("ZCR")
				For nX := 1 to Len(aSetPed)
					DbGoto(aSetPed[nX]) 
					ZCR->(RecLock("ZCR", .F.))
						ZCR->ZCR_PEDGER	:= cPedido
						ZCR->ZCR_PROCES	:= "S"
					ZCR->(MsUnLock())
				Next nX
				
				
				FWMsgRun(, {||  AbateQtd(cPedMae, aProdQtd) }, "Pedido M�e de Vendas", "Aguarde... Abatendo Quantidade no Pedido M�e...")
				
			EndIf
			
		End Transaction
	EndIf
	
	
	cItem  	 := "01"
	aItens 	 := {}
	aProdQtd := {}
	aSetPed  := {}
	(cAliasZCR)->(DbSkip())
EndDo

ZCR->(DbCloseArea())

(cAliasZCR)->(DbCloseArea())

Aviso("Processamento" , "Foram gerados os pedidos: "+ CRLF + cPedMsg, {"Ok"} , 2 )

Return


/*/{Protheus.doc} AbateQtd
//Atualiza a quantidade entregue no pedido de venda.
@author DS2U [L. Fini]
@since 25/01/2019
@version 1.0
@param cPedMae, characters, descricao
@param aaProdQtd, array of array, descricao
@type function
/*/
Static Function AbateQtd(cPedMae, aProdQtd)

Local lRet := .F.
Local nX := 1

For nX := 1 to Len(aProdQtd)

	DbSelectArea("SC6")
	SC6->(DbSetOrder(2)) //C6_FILIAL + C6_PRODUTO + C6_NUM
	If ( SC6->( dbSeek( xFilial("SC6") + aProdQtd[nX][1][1] + cPedMae ) ) )
		RecLock("SC6", .F.)
			SC6->C6_QTDENT := SC6->C6_QTDENT + aProdQtd[nX][1][2]
		SC6->(MsUnlock())
	EndIf

Next nX

//VERIFICA SE DEVE ENCERRAR
//O PEDIDO DE VENDA.
u_EncerPed(cPedMae)

SC6->(DbCloseArea())
Return

/*/{Protheus.doc} EncerPed
//Verifica se deve encerrar o pedido de venda
@author DS2U [L. Fini]
@since 25/01/2019
@version 1.0
@return lRet = .T., pedido deve ser encerrado.
@param cPedMae, characters, descricao
@type function
/*/
User Function EncerPed(cPedMae)

Local lRet := .F.

//VERIFICA SE TODOS OS ITENS FORAM ATENDIDOS, 
//SE PELO MENOS UM PRODUTO N�O FOI 100% ATENDIDO
//RETORNAR� FALSO, N�O SENDO NECESS�RIO ENCERRAR O PEDIDO.
DbSelectArea("SC6")
SC6->(DbSetOrder(1)) //C6_FILIAL + C6_NUM
If ( SC6->( dbSeek( xFilial("SC6") + cPedMae ) ) )
	While SC6->(!EOF()) .AND. SC6->C6_NUM == cPedMae
		If SC6->C6_QTDVEN == SC6->C6_QTDENT
			lRet := .T. 
		Else
			lRet := .F.
		EndIf
		SC6->(DbSkip())
	EndDo
EndIf

If lRet //SE LRET = .T., SIGNIFICA QUE TODOS OS ITENS FORAM ATENDIDOS.
	DbSelectArea("SC5")
	SC5->(DbSetOrder(1)) //C6_FILIAL + C6_NUM
	If ( SC5->( dbSeek( xFilial("SC5") + cPedMae ) ) )
		RecLock("SC5",.F.)
			SC5->C5_NOTA := "XXXXXX"
		SC5->(MsUnlock())
	EndIf
Else
	DbSelectArea("SC5")
	SC5->(DbSetOrder(1)) //C6_FILIAL + C6_NUM
	If ( SC5->( dbSeek( xFilial("SC5") + cPedMae ) ) )
		RecLock("SC5",.F.)
			SC5->C5_NOTA := " " //Possibilidade de reabrir o pedido, pois pode ser chamado do A410EXC.prw
		SC5->(MsUnlock())
	EndIf
EndIf

SC6->(DbCloseArea())
SC5->(DbCloseArea())
Return