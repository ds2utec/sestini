/*/{Protheus.doc} jobAloc
Funcao responsavel por prepara os parametros para executar o JOB de aloca��es
@author DS2U (SDA)
@since 02/11/2018
@version 1.0
@param clEmp, characters, Codigo da Empresa que esta configurado o job
@param clFil, characters, Codigo da Filial que esta configurado o job
@type function
/*/
User Function jobAloc( clEmp, clFil )

	if ( empty( clEmp ) .or. empty( clFil ) )
	
		conOut( "ID: [" + cpIDAloc + "] - Parametros de empresa e filiais incorretos. Processamento abortado!" )
	
	else
	
		RpcSetType( 3 )
		RpcSetEnv( clEmp, clFil )

		U_ADMVP02( 0,, .T. )
		
		RpcClearEnv()
		
	endif
	
Return