#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} ADMVXFUN
Bilioteca de funcoes referente ao projeto de ADM de Vendas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return uRet, retorno da funcao executada
@param nlOpc, numeric, Parametro para identificar a funcao a ser executada
@param uParam, undefined, Parametro a ser enviado para a funcao a ser executada
@type function
/*/ 
User Function ADMVXFUN( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil

	do case

		case nlOpc == 1
			uRet := helpAdmv( uParam )

		case nlOpc == 2
			uRet := vldIdDup( uParam )

		case nlOpc == 3
			uRet := vldPerc( uParam )

		case nlOpc == 4
			uRet := getCanais( uParam )

		case nlOpc == 5
			uRet := getCtByIt( uParam )

		case nlOpc == 6
			uRet := getParAdmv( uParam )

		// Valida��o de mudan�a do campo Cliente Novo - A1_XNEWCLI
		case nlOpc == 7
			uRet := vldNewCli( uParam )

		// Valida��o de mudan�a do campo Status Comercial do Pedido de Venda - C5_XSTCOAD
		case nlOpc == 8
			uRet := vldStComAd( uParam )

		// Valida��o Desconto Cliente C5_DESC1
		case  nlOpc == 9
			uRet := VDscCliePV( &(ReadVar()) )

		//Valida��o Desconto Negociado C5_DESC2
		case  nlOpc == 10
			uRet := VDscNegoc( &(ReadVar()) )

		//Valida��o Desconto Comercial
		case  nlOpc == 11
			uRet := VDscComerc( @uParam )

		case  nlOpc == 12
			uRet := FWhenDscC6()
			
		case  nlOpc == 13
			uRet := GVlrT100( uParam )
			
		case  nlOpc == 14
			uRet := VAMDVLbFat()
		
		case  nlOpc == 15
			uRet := GZCLAdmv( uParam )

		case  nlOpc == 16
			uRet := PVAvalADMV( uParam )		

		case  nlOpc == 17
			uRet := FVldMatriz( uParam )		

		case  nlOpc == 18
			uRet := GetPrxEntg( uParam )
			
	endCase

Return uRet


/*/{Protheus.doc} getCanais
Reponsavel por retornar os canais do ADMV, que sao controlados por cotas
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return alCanais,	alCanais[1][1] = Codigo do Armazem em que o canal esta vinculado
					alCanais[1][2] = Descricao do Armazem em que o canal esta vinculado
					alCanais[1][3] = Cota do Armazem em que o canal esta vinculado
					alCanais[1][4] = Linha de Produtos do Armazem em que o canal esta vinculado
									 1=1� Linha;2=2� Linha;3=2� Linha Grave;4=2� Linha Medio;5=2� Linha Leve
@param llOnlyPlin, boolean, Identifica se a query ira retornar somente canais ADMV de primeira linha
@type function
/*/
Static Function getCanais( llOnlyPlin )

	local alCanais	:= {}
	local clAlias	:= getNextAlias()
	local clOnlyPlin:= "% 1 = 1 %"

	default llOnlyPlin	:= .F.

	if ( llOnlyPlin )
		clOnlyPlin := "% ZC0_LINHA = '1' %"
	endif

	BEGINSQL ALIAS clAlias

		SELECT
			ZC0_LOCAL AS ARMAZEM
			, ZC0_DESCAN AS DESCRICAO
			, ZC0_PERCOT AS COTA
			, ZC0_LINHA AS LINHA

		FROM
			%TABLE:ZC0% ZC0

		WHERE
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND %EXP:clOnlyPlin%
			AND ZC0.%NOTDEL%

	ENDSQL

	while ( .not. ( clAlias )->( eof() ) )

		AADD( alCanais, { ( clAlias )->ARMAZEM, ( clAlias )->DESCRICAO, ( clAlias )->COTA, ( clAlias )->LINHA } )

		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )

Return alCanais

/*/{Protheus.doc} getCtByIt
Retorna a soma de percentual de cota por produto. Utilizado para validar o nivel, nao deixando ultrapassar 100%
@author DS2U (SDA)
@since 06/08/2018
@version 1.0
@return nlCota, Soma das cotas por produto
@param clProduto, characters, Codigo do produto a ser analisado
@type function
/*/
Static Function getCtByIt( clProduto )

	local nlCota	:= 0
	local clAlias	:= getNextAlias()

	default clProduto	:= ""

	BEGINSQL ALIAS clAlias

		SELECT
			SUM( ZC8_COTA ) AS TOTALCOTA

		FROM
			%TABLE:ZC8% ZC8

		WHERE
			ZC8.ZC8_FILIAL = %XFILIAL:ZC8%
			AND ZC8.ZC8_PROD = %EXP:clProduto%
			AND ZC8.%NOTDEL%

	ENDSQL

	if ( .not. ( clAlias )->( eof() ) )
		nlCota :=  ( clAlias )->TOTALCOTA
	endif
	( clAlias )->( dbCloseArea() )

Return nlCota

/*/{Protheus.doc} existZC0
Responsavel por checar se ID existe no cadastro de cotas
@author DS2U (SDA)
@since 11/06/2018
@version 1.0
@return llRet, .T. = Existe / .F. = N�o existe
@param clIdCota, characters, ID da cota��o a ser checada
@type function
/*/
Static Function existZC0( clIdCota )

	local alArea	:= getArea()
	local llRet		:= .T.

	dbSelectArea( "ZC0" )
	ZC0->( dbSetOrder( 1 ) )

	llRet := ZC0->( dbSeek( xFilial( "ZC0" ) + PADR( clIdCota, tamsx3("ZC0_ID")[1] ) ) )

	if ( .not. llRet )
		helpAdmv( {"ZC0_ID", "Registro n�o encontrado no cadastro de cotas. Informe um registro v�lido!"} )
	endif

	restArea( alArea )

Return llRet

///*/{Protheus.doc} getChvSBE
//Responsavel por definir regras de filtro para a consulta padrao de cotas ZC0
//@author DS2U (SDA)
//@since 07/05/2018
//@version 1.0
//@return llRet, Retorno booleano conforme regra aplicada ao filtro da tabela ZC0
//
//@type function
///*/
//Static Function getChvSBE( clIdAloc )
//
//	local clChave	:= XFILIAL("SBE")+SUBS(clIdAloc,1,2)+clIdAloc
//
//Return clChave

/*/{Protheus.doc} helpAdmv
Funcao customizada para apresenta��o de alerta
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@param clMsg, characters, Mensagem do usuario a ser apresentada
@type function
/*/
Static Function helpAdmv( alParam )

	local clCampo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clMsg		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local llConout	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .T. )

	default alParam	:= {}

	if ( .not. empty( clCampo ) .and. .not. empty( clMsg ) )

		if ( llConout )
			conout( clCampo )
			conout( clMsg )
		endif

		fwClearHLP()
		help('', 1, getSX3Cache(clCampo, "X3_TITULO"), clCampo, clMsg, 1, 0)

	endif

Return

/*/{Protheus.doc} getAlmxPai
Funcao para buscar o codigo do almoxafirado do codigo pai enviado por parametro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return clAlmoxPai, Codigo do almoxarifado conforme o codigo pai

@type function
/*/
Static Function getAlmxPai( clCodPai )

	local alArea	:= getArea()
	local clAlmoxPai:= ""

	default clCodPai	:= ""

	if ( .not. empty( clCodPai ) )

		dbSelectArea("ZC0")
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_

		if ( ZC0->( dbSeek( xFilial("ZC0") + clCodPai ) ) )
			clAlmoxPai := ZC0->ZC0_ALMOX
		else
			helpAdmv( {"ZC0_ALMOX", "C�digo Pai n�o existe!"} )
		endif

	endif

	restArea( alArea )

Return clAlmoxPai

/*/{Protheus.doc} vldPerc
Funcao para validacao de preenchimento de percentual entre 0,01 e 100 %
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retorna verdadeiro ou falso identificando se o preenchimento do percentual est� v�lido
@param nlPerc, numeric, percentual a ser avaliado
@type function
/*/
Static Function vldPerc( nlPerc )

	local llRet	:= .T.

	default nlPerc := 0

	if ( nlPerc <= 0 .or. nlPerc > 100 )

		llRet := .F.
		helpAdmv( {"ZC0_PERCOT", "O percentual deve ser preenchido entre 0.01% e 100% !"} )

	endif

Return llRet

/*/{Protheus.doc} getParAdmv
Responsavel por controlar o uso da entidade de parametros do ADMV
@author DS2U (SDA)
@since 22/08/2018
@version 1.0
@return uRet, Conteudo do campo da entidade ZC6, referente ao campo enviado por parametro
@param clField, characters, Nome do campo refente ao parametro da entidade que deve ser retornado
@type function
/*/
Static Function getParAdmv( clField )
Return getAdvFVal( "ZC6" , clField , FWxFilial("ZC6") + PADR( getMv("ES_ADMVPAR",,"000001"), tamSX3("ZC6_CODIGO")[1] ) , 1, criaVar( clField ) )




/*/{Protheus.doc} vldNewCli
//Funcao para validar se pode alterar campo que identifica clientes novos
@author DS2U (Dema)
@since 24/08/2018
@version 1.0
@return ${return}, ${return_description}
@param uParam, undefined, descricao
@type function
/*/
static Function vldNewCli( aParam )

Local aArea			:= GetArea()
Local aAreaSA1 		:= SA1->(GetArea())
Local lRet 			:= .T.
Local lAchouSA1		:= .F.
Local xConteudo 	:= &(aParam[3])

dbSelectArea("SA1")
SA1->(dbSetOrder())
SA1->(dbGoTop())
lAchouSA1	:= SA1->( MsSeek( xFilial("SA1") + aParam[1] + aParam[2]  )  )

// ---------------------------------------------------------------------------------------------------------------------------
// Sen�o encontrar cliente Loja no SA1 e o conteudo que est� sendo alterado � "N=N�o" - N�o permitir a altera��o do campo
lRet := Iif(  ( xConteudo $ " |N"  )  ,  lAchouSA1   ,  !lAchouSA1     )

If !lRet
	Alert("Altera��o do campo 'Cliente Novo' Inv�lida" + CRLF + "Deve preencher Cliente Novo = 'N�o' somnente para clientes j� cadastrados na base de dados " + CRLF + "Avalie novamente o cen�rio" )
EndIF

SA1->(RestArea(aAreaSA1))
RestArea(aArea)

Retur lRet





/*/{Protheus.doc} vldStComAd
//TODO Descri��o auto-gerada.
@author DS2U (Dema)
@since 03/09/2018
@version 1.0
@return ${return}, ${return_description}
@param aParam, array, descricao
@type function
/*/
static Function vldStComAd( aParam )

Local lUser 		:= !IsBlind()
Local lRet 			:= .T.
Local cNewStatus	:= ""
Local cError		:= ""

Default aParam 		:= {}

If Len(aParam)>1 .AND. lUser

	cNewstatus := aParam[1]

	// N�o permite que usu�rio altere  o Status Comercial ADMV do pedido para L=Liberado e nem B=Bloqueado
	// Status alterados somente pela rotina de Avalia��o ADMV
	If cNewstatus $ "B|L"
		lRet := .F.
		cError := "Os status - B=Bloqueado e L=Liberado, n�o podem ser selecionados manualmente." + CRLF + " S�o alimentados pela An�lise Comercial ADMV (sist�mica)."
	ElseIf  cNewstatus $ "B|A"

		lRet := MsgYesNo("Essa op��o A=Aprovado FOR�A a libera��o Comercial do Pedido. " + CRLF + "Tem certeza que deseja alterar o Pedido de Vendas para o Status Comercial ADMV A=Aprovado?","Aten��o")

	EndIf

// --------------------------------------------------
// Caso tenha erro - Apresenta Help.
If !lRet .and. !Empty(cError)
	Help(,, "ADMV_COM01",, cError, 1, 0)
EndIf

EndIF


Retur lRet

/*/{Protheus.doc} SesWrLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 20/04/2018
@version 1.0
@return ${return}, ${return_description}
@param cArq, characters, descricao
@param uTexto, undefined, descricao
@param lConsole, logical, descricao
@param lDateTime, logical, descricao
@param tInicio, , descricao
@param tFim, , descricao
@param lPtinternal, logical, descricao
@type function
/*/
User Function SesWrLog( cArq , uTexto , lConsole , lDateTime , tInicio, tFim , lPtinternal )

Local nHandle 	:= 0
Local nAuxFor 	:= 0
Local cTexto  	:= ""
Local cTempo	:= ''

Local cDrive		:= ""
Local cDir			:= ""
Local cNomeArq		:= ""
Local cExt			:= ""
Local tDuracao		:= ''

Default tInicio		:= ''
Default tFim		:= ''
Default lConsole	:= .T.
Default lDateTime	:= .T.
Default lPtinternal	:= .F.

IF !Empty(tInicio)
	MV_PAR60	:= tInicio
Endif

IF !Empty(tFim)
	tDuracao 	:= ElapTime(MV_PAR60,Time())	
	cTempo 		:= '   ' + SubStr(tDuracao,1,2)+" h: "+SubStr(tDuracao,4,2)+" m: "+SubStr(tDuracao,7,2)+" s: "
Endif

cArq := StrTran(Alltrim(cArq)," ","")

If ! ( "." $ cArq )
	cArq	+= ".log"
Endif   

If lPtInternal
	PtInternal( 1, uTexto )
Endif

If !File( cArq )

	// -- Tratamento para diretorios
	
	SplitPath( cArq , @cDrive, @cDir, @cNomeArq, @cExt )
	MontaDir(cDir)

	nHandle := FCreate( cArq )
	FClose( nHandle )	

Endif

If File( cArq )


	nHandle := FOpen( cArq, 2 )
	FSeek ( nHandle, 0, 2 )			// Posiciona no final do arquivo.
	
	If ValType(uTexto) == "C"
		
		If lDateTime
			cTexto := dtoc(dDataBase) + ";" + Time() + ";" + uTexto
		Else
			cTexto := uTexto
		Endif
		
		IF !Empty( cTempo )
			cTexto	+= cTempo
		Endif
		
		FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
		
		If lConsole
			CONOUT(cTexto)
		Endif
	
	ElseIf ValType(uTexto) == "A"
	
		For nAuxFor := 1 to len(uTexto)
		
			cTexto := dtoc(dDataBase) + " " + Time() + " " + uTexto[nAuxFor]

			IF !Empty( cTempo )
				cTexto	+= cTempo
			Endif
			
			FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
			
			If lConsole
				CONOUT(cTexto)
			Endif
			
		Next nAuxFor
	
	EndIf
	
	FClose( nHandle )
	
EndIf

Return Nil

/*/{Protheus.doc} SesSwLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
User Function SesSwLog(cArqLog)

Local oMemo		:= NIL 
Local oDlg		:= NIL 
Local cFile		:= "" 
Local cTexto	:= "" 
Local cMask     := "Arquivos Texto" + "(*.csv)|*.csv|"

// --------------------------------------------
// Le arquivo de Log 
cTexto := LeLog(cArqLog)

Define MsDialog oDlg Title "Atualiza��o concluida." From 3, 0 to 340, 417 Pixel

	@ 5, 5 Get oMemo Var cTexto Memo Size 200, 145 Of oDlg Pixel
	oMemo:bRClicked := { || AllwaysTrue() }
	
	Define SButton From 153, 175 Type  1 Action oDlg:End() Enable Of oDlg Pixel // Apaga
	Define SButton From 153, 145 Type 13 Action ( cFile := cGetFile( cMask, "" ), If( cFile == "", .T., ;
	MemoWrite( cFile, cTexto ) ) ) Enable Of oDlg Pixel

Activate MsDialog oDlg Center


Return Nil

/*/{Protheus.doc} AvPedADMV
//Fun��o Gen�rica para Avaliar os pedidos ADMV e retornar
@author DS2U (Dema)
@since 27/08/2018
@version 1.0
@return ${return}, ${cStatus} - Retornar com a flag
@param nTipo, numeric, Define o tipo de valida��o que dever� ser realizada
	1 = Analise cr�dito do pedido de venda
	2 = Analise al�ada do pedido de venda
	3 = Analise comercial do pedido de venda
	4 = Analise estoque do pedido de venda
@param cPedido, characters, Numero do pedido a ser avaliado
@param cObs, characters, Para retornar todo e qualquer observa��o do(s) motivo(s) do bloqueio ou libera��o - *** DEVE SER PASSADO COMO @REFERENCIA pela chamada original
@type function
/*/
User Function AvPedADMV( cTipo ,  cPedido  )

Local aArea 		:= GetArea()
Local aC5Area		:= SC5->(GetArea())
Local aC6Area		:= SC6->(GetArea())
Local aDA0Area		:= DA0->(GetArea())
Local aA1Area		:= SA1->(GetArea())
Local aRetBloq 		:= {}
Local cStatsC5		:= ""
Local llExecADMV 	:= GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado

Default cTipo		:= "ALL"						// Caso nao passado via parametro qual o tipo de bloqueio, 1,2,3 ou 4 - Realizar todos com All
Default cPedido 	:= ""

If ( llExecADMV )

	dbSelectArea("SC5")
	SC5->(dbSetOrder(1)) 	// C5_FILIAL + C5_NUM
	SC5->(dbGoTop())
	IF SC5->( MsSeek(  xFilial("SC5") +  cPedido  )   )
		
		//------------------------------------------------------------------------------------------
		//Valida de o pedido de vendas deve ser analisado pelas Regras ADMV - Depende do Canal
		//------------------------------------------------------------------------------------------
		If U_ADMVXFUN( 16 , Alltrim(SC5->C5_XCANAL ) )
		
			/*
			cTipo = 1 = BLOQUEIO / LIBERA��O - CREDITO
			cTipo = 2 = BLOQUEIO / LIBERA��O - AL�ADA
			cTipo = 3 = BLOQUEIO / LIBERA��O - COMERCIAL
			cTipo = 4 = BLOQUEIO / LIBERA��O - ESTOQUE
			*/
			
			// ----------------------------------------------------------------------------------
			// ***** IMPLEMENTAR AQUI AS REGRAS DE ANALISE DE BLOQUEIO OU LIBERA�AO DO ADMV
			// ----------------------------------------------------------------------------------
			do case
		
				// -----------------------------------------------------
				// Avalia��o TIPO 1 - CR�DITO do pedido de Venda
				case cTipo == "1"
				
					ADMAvalCrd()
		
				// -----------------------------------------------------
				// Avalia��o TIPO 2 - AL�ADA do pedido de Venda
				case cTipo == "2"
					
					ADMAvalAlc( cPedido )
		
				// -----------------------------------------------------
				// Avalia��o TIPO 3 - COMERCIAL do pedido de Venda
				case cTipo == "3"
		
					// Funcao que avalia as valida��es - Bloqueios Comerciais
					ADMAvalCom( cPedido , @aRetBloq , @cStatsC5 )
		
				// -----------------------------------------------------
				// Avalia��o TIPO 4 - ESTOQUE do pedido de Venda
				case cTipo == "4"
				
					//ADMAvalEst
		
		
		
				// -----------------------------------------------------
				// Avalia��o de todos
				case cTipo == "ALL"
		
					u_AvPedADMV( "1" ,  cPedido  )		// Avalia Credito
					u_AvPedADMV( "2" ,  cPedido  )		// Avalia Al�ada
					u_AvPedADMV( "3" ,  cPedido  )		// Avalia Comercial
					u_AvPedADMV( "4" ,  cPedido  ) 		// Avalia Estoque
		
		
					If !IsBlind()
						// Fun��o para alerta de Warnings para o usuario
						u_ADMVAviso( 1, fRulesWar(cPedido) )
					EndIf
		
			endcase
		
			// ----------------------------------------------------------------------------------
			// ***** FIM DAS ANALISES
			// ----------------------------------------------------------------------------------
			
			// Grava na ZCG as ocorrencias, caso existam.
			If Len(aRetBloq) > 0
				u_ADP12Grv( cPedido, cTipo /*TpMovimentoBloq*/ , aRetBloq , cStatsC5 )
			Endif
		
		EndIf
		
	EndIf
	
	SA1->(RestArea(aA1Area))
	DA0->(RestArea(aDA0Area))
	SC6->(RestArea(aC6Area))
	SC5->(RestArea(aC5Area))
	RestArea(aArea)
	
EndIf

Return aRetBloq



/*/{Protheus.doc} fRulesWar
//TODO Descri��o auto-gerada.
@author DS2U (Dema)
@since 03/09/2018
@version 1.0
@return ${return}, ${return_description}
@param cPedido, characters, descricao
@type function
/*/
Static Function fRulesWar(cPedido)

Local aDados 		:= {}
Local cTipo 		:= "" 					// Deve definir o tipo do Aviso - 1=Credito / 2=Al�ada / 3=Comercial 4=Cr�dito
Local cMSg 			:= ""
Local llExecADMV 	:= GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado

If ( llExecADMV )

	// --------------------------------------------------------
	// --- Regras Warning - Pedido de Venda --- 1 - Credito ---
	// --------------------------------------------------------
	cTipo := '1'
	// ...
	
	
	// --------------------------------------------------------
	// --- Regras Warning - Pedido de Venda --- 2 - Al�ada  ---
	// --------------------------------------------------------
	cTipo := '2'
	
	
	// -----------------------------------------------------------
	// --- Regras Warning - Pedido de Venda --- 3 - Comercial  ---
	// -----------------------------------------------------------
	cTipo := '3'
	
	If SA1->(MsSeek( xFilial("SA1") + SC5->C5_CLIENTE  + C5_LOJACLI    ) )
	
		// -----------------------------------------------------------------------------------------------------
		// AlertCom01 - Transportadora do pedido de venda difere do cadastro do cliente, somente quando for FOB;
		// -----------------------------------------------------------------------------------------------------
		If AllTrim(SC5->C5_TPFRETE) == "F" .AND. ( SC5->C5_TRANSP <> SA1->A1_TRANSP )  // se for FOB e Transp. <> do cadastro do cliente
			cMsg := "AlertCom01 - Este pedido utiliza Tipo de Frete F=Fob e a transportadora informada no pedido de venda ["  + AllTrim(SC5->C5_TRANSP) + "] � diferente da informada no cadastro do cliente [" + AllTrim(SA1->A1_TRANSP ) + "]."
			aAdd(aDados, { cTipo, cMsg } )
		EndIf
	
		// ----------------------------------------------------------------------------------------------------
		// Alert02 - Tabela de pre�o do cliente diferente da tabela de pre�os do pedido de venda;
		// ----------------------------------------------------------------------------------------------------
		If AllTrim(SC5->C5_TABELA) <> AllTrim(SA1->A1_TABELA)
			cMsg := "AlertCom02 - Este pedido utiliza tabela de pre�o  ["  + AllTrim(SC5->C5_TABELA) + "] diferente da informada no cadastro de clientes [" + AllTrim(SA1->A1_TABELA ) + "]. "
			aAdd(aDados, { cTipo, cMsg } )
		EndIf
	
	
	EndIf
	
	// ----------------------------------------------------------------------------------------------------
	// Alert03 - UF do Cadastro de Frete x UF diferente da UF do cadastro do cliente:
	// ----------------------------------------------------------------------------------------------------
	
	
	// --------------------------------------------------------
	// --- Regras Warning - Pedido de Venda --- 4 - Estoque ---
	// --------------------------------------------------------
	cTipo := '4'
	// ...

EndIf

Return aDados



/*/{Protheus.doc} ADMVAviso
//TODO Descri��o auto-gerada.
@author DS2U (Dema)
@since 04/09/2018
@version 1.0
@return ${return}, ${return_description}
@param nTipo, numeric, 1 = Aviso / Warning  - 2=Erro
@param aDados, array, descricao
@type function
/*/
User Function ADMVAviso( nTipo,  aDados )

Local cTitulo 		:= "ATEN��O - Alertas & Valida��es - ADMV"
Local cMsg 			:= ""
Local nCont 		:= 0
Local nPos			:= 0
Local aTipoAlet 	:= {{"1","Cr�dito"},{"2","Al�ada"},{"3","Comercial"},{"4","Cr�dito"}}
Local cNumPed 		:= ""

Default nTipo 		:= 1
Default aDados 		:= {}

If Len(aDados)>0

	cNumPed 	:= Iif( nTipo==1  , SC5->C5_NUM , M->C5_NUM  )
	cMsg := "Ol� " + UsrRetName(__cUserID) + "! " + CRLF + Iif( nTipo==1 , "Existem ALERTAS ADMV para o pedido " , "Existem VALIDA��ES ADMV para o pedido ") + cNumPed + CRLF + CRLF
	For nCont:=1 to  Len(aDados)
		npos := aScan( aTipoAlet , {|z| z[1]== aDados[nCont,1]} )
		If nPos > 0
			cMsg += CRLF + Iif( nTipo==1 , "Aviso "  , "Valida��o " ) + AllTrim(StrZero(nCont,3)) + " - Tipo: " + aDados[nCont,1] + " - " + aTipoAlet[nPos,2] + CRLF + AllTrim(aDados[nCont,2]) + CRLF + Replicate("-",80)
		EndIF
	Next nCont

	Aviso(cTitulo,cMsg,{"Fechar"},3)

EndIf

Return NIL



/*/{Protheus.doc} ADMAvalCom
//Fun��o Especifica para avaliar o Pedido Comercialmente.
@author DS2U (Dema)
@since 27/08/2018
@version 1.0
@return ${return}, ${cStatus} - Retornar com a flag
@param cPedido, characters, Numero do pedido a ser avaliado
@param cObs, characters, Para retornar todo e qualquer observa��o do(s) motivo(s) do bloqueio ou libera��o - *** DEVE SER PASSADO COMO @REFERENCIA pela chamada original
@type function
/*/
Static Function ADMAvalCom( cPedido , aRet , cStatsC5 )

Local aRet 				:= {}	 									// Retorno da valia�ao = L-Liberado / B-Bloqueado
Local lA1_XNEWCLI		:= SA1->(FieldPos("A1_XNEWCLI")) > 0 		// Para avaliar se o campo existe na base de dados
Local lA1_XDMAXCO		:= SA1->(FieldPos("A1_XDMAXCO")) > 0 		// Para avaliar se o campo existe na base de dados
Local lC5_XOBCOBL		:= SC5->(FieldPos("C5_XOBCOBL")) > 0 		// Para avaliar se o campo existe na base de dados
Local lC5_XSTCOAD		:= SC5->(FieldPos("C5_XSTCOAD")) > 0 		// Para avaliar se o campo existe na base de dados
Local lC6_XSTALOC		:= SC6->(FieldPos("C6_XSTALOC")) > 0 		// Para avaliar se o campo existe na base de dados
Local cObs				:= ""
Local cTabelaBase		:= ""
Local cChavePed 		:= ""
Local cCodProd 			:= ""
Local cIdPromo			:= ""
Local nVUnitPed			:= 0
Local nVUnitTabP		:= 0
Local nTotPed 			:= 0
Local nTotDescP			:= 0
Local nPercDescP		:= 0
Local nCont 			:= 0
Local aDatasEnt 		:= {}
Local dDtAnterior 		:= CToD("//")
Local cPrazoMedio		:= ""
Local nDifDias			:= 0
Local cItTravaEst		:= ""

// SC5 - Pedido de vendas
dbSelectArea("SC5")
SC5->(dbSetOrder(1)) 	// C5_FILIAL + C5_NUM
SC5->(dbGoTop())

// SC6 - Itens pedido de vendas
dbSelectArea("SC6")
SC6->(dbSetOrder(1)) 	//  C6_FILIAL + C6_NUM
SC6->(dbGoTop())

// DA0 - Tabela de Pre�os
dbSelectArea("DA0")
DA0->(dbSetOrder(1))	// DA0_FILIAL + DA0_CODTAB
DA0->(dbGoTop())

// DA1 - Itens tabela de Pre�os
dbSelectArea("DA1")
DA1->(dbSetOrder(1))	// DA1_FILIAL + DA1_CODTAB
DA1->(dbGoTop())

// SA1 - Cadastro de Clientes
dbSelectArea("SA1")
SA1->(dbSetOrder(1)) 		// A1_FILIAL + A1_COD + A1_LOJA
SA1->(dbGoTop())

IF lA1_XNEWCLI .AND. lC5_XOBCOBL .AND. lC5_XSTCOAD .AND. lA1_XDMAXCO .AND. lC6_XSTALOC


	IF SC5->( MsSeek(  xFilial("SC5") +  cPedido  )   )

		// ------------------------------------------------------------------------------------------------
		// Verifica se o pedido est� A=Aprovado = N�o passar pela an�lise, caso for�ado pelo usuario
		If !(SC5->C5_XSTCOAD == "A")



			// =========================================
			// *** INICIO DAS VALIDA��ES


			// -------------------------------------------------------------------------------------------------------------------------------------------
			// >>>> Avalia se o cliente � novo
			// -------------------------------------------------------------------------------------------------------------------------------------------
			If SA1-> (MsSeek( xFilial("SA1") + SC5->C5_CLIENTE  + C5_LOJACLI    ) )
				If  ("S" $ SA1->A1_XNEWCLI)
					cObs	:= "COM01 - Cliente novo - Avaliar cadastro do cliente " + SA1->(A1_COD+A1_LOJA)  + " - " + AllTrim(SA1->A1_NREDUZ)
					AAdd(aRet , { "1" , cObs })
					cStatsC5 := "R"
					
				Endif
			Else
			   cObs	:=  "COM02 - Cliente n�o encontrado na base de dados - " + SC5->(C5_CLIENTE+C5_LOJACLI)
			   AAdd(aRet , { "1" , cObs })
			   cStatsC5 := "R"
			   
			EndIF

			// -------------------------------------------------------------------------------------------------------------------------------------------
			// >>>> Avalia se existe Bloqueio de Pedido Preenchido
			// -------------------------------------------------------------------------------------------------------------------------------------------
			If !Empty(SC5->C5_XOBCOBL)
				cObs	:=  "COM03 - O pedido possui observa��es de bloqueio de pedido conforme texto a seguir: " + CRLF + AllTrim(SC5->C5_XOBCOBL)
				AAdd(aRet , { "1" , cObs })
				cStatsC5 := "R"

			EndIF

			// -------------------------------------------------------------------------------------------------------------------------------------------
			// >>>> Tipo de Frete FOB -  Validar Transportadora
			// -------------------------------------------------------------------------------------------------------------------------------------------
			IF  "F" $ AllTrim(SC5->C5_TPFRETE) .AND. Empty(SC5->C5_TRANSP) // F = FOB sem transportadora preenchida
				cObs	:=  "COM04 - O tipo de Frete � F=FOB. Com isso a Transportadora deve ser informada obrigatoriamente. Verifique o Campo Transp: (C5_TRANSP) no pedido de vendas."
				AAdd(aRet , { "1" , cObs })
				cStatsC5 := "R"

			EndIF

			// -------------------------------------------------------------------------------------------------------------------------------------------
			// >>>> Valida se h� algum item do pedido de venda - SC6 com valor de venda < menor que tabela Base
			// Primeiro verifica se haver� tabela base
			// -------------------------------------------------------------------------------------------------------------------------------------------
			cTabelaBase 	:= u_ADMVXFUN( 6, "ZC6_TABBAS" )

			If !Empty(cTabelaBase)

				// ---------------------------------------------------------------------------------------------------
				// *** Aten��o - Neste momento n�o est� sendo considerado conceito de Dt. Vigencia de tabela
				// Ser� sempre a tabela parametrizada nos parametros ADMV
				If DA0->(MsSeek( xFilial("DA0") + cTabelaBase ))

					cChavePed := xFilial("SC6") + SC5->C5_NUM

					// -------------------------------------------------------------------------------------------------------------------------------------------
					// Roda os itens para verificar as valida��es por item
					// -------------------------------------------------------------------------------------------------------------------------------------------
					If SC6->( MsSeek(  cChavePed  ) )


						While SC6->(!EOF()) .AND. ( cChavePed == SC6->(C6_FILIAL+C6_NUM )  )

							cCodProd	:= SC6->C6_PRODUTO						// Codigo do produto do item
							nVUnitPed	:= SC6->C6_PRCVEN 						// Valor unitario do item - vendido
							cIdPromo	:= SC6->C6_XIDPROMO						// C�digo do ID de promo��o
							nTotPed 	+= nVUnitPed + SC6->C6_VALDESC			// Totalizador do pedido de Vendas - Deve somar os valores dados de desconto
							nTotDescP	+= SC6->C6_VALDESC						// Totalizador de Desconto do Pedido

							If !Empty(SC6->C6_ENTREG)
								aAdd(aDatasEnt ,{  SC6->C6_ENTREG , ("Item: " + SC6->C6_ITEM ) } )				// Alimenta com datas de entrega para vaida��o de Prazo M�dio
							EndIf

							// Encontrar o item/produto na tabela 100 - base
							If DA1->( MsSeek( xFilial("DA1") +  cTabelaBase + SC6->C6_PRODUTO  ) )
								nVUnitTabP := DA1->DA1_PRCVEN

								// -------------------------------------------------------------------------------------------------------------------------------------------
								// Valida se o pre�o vendido no pedido de venda � < MENOR que o pre�o na tabela base - 100
								// --------------------------------------------------	-----------------------------------------------------------------------------------------
								If nVUnitPed < nVUnitTabP .AND. Empty(cIdPromo)

									// **** Aten��o - N�o considerado regras de Promocoes neste momento
									cObs	:=  "COM009 - O pre�o vendido para este item: " + AllTrim(SC6->C6_ITEM) + " / Produto: " + AllTrim(SC6->C6_PRODUTO) + " est� menor que a tabela Base."
									AAdd(aRet , { "1" , cObs })
									cStatsC5 := "R"

								EndIf

							Else

								cObs	:=  "COM08 - N�o exite pre�o cadastrado para o item " + AllTrim(SC6->C6_PRODUTO) + " na tabela base - " + cTabelaBase + ". " + CRLF + "Cadastre um pre�o para este item na tabela base."
								AAdd(aRet , { "1" , cObs })
								cStatsC5 := "R"

							EndIF

							// -------------------------------------------------------------------------------------------------------------------------------------------
							// >>>> Valida bloqueio de estoque Comercial
							// -------------------------------------------------------------------------------------------------------------------------------------------
							If !(AllTrim(SC6->C6_XSTALOC) $ "NP|FM" )  //NP=Nao Processado;EC=Estima Cancelamento;FM=Firme;ET=Estimado
								cItTravaEst += SC6->C6_ITEM + " / "
							EndIf


							SC6->(dbSkip())
						EndDo


						// -------------------------------------------------------------------------------------------------------------------------------------------
						// >>>> Valida bloqueio de estoque Comercial
						// -------------------------------------------------------------------------------------------------------------------------------------------
						If !Empty(cItTravaEst)
							cObs	:=  "COM013 - Bloqueio Comercial por Estoque - Existem itens com Status de Aloca��o - C6_XSTALOC <> diferente de NP=Nao Processado e/ou FM=Firme - " + "Itens: " + cItTravaEst
							AAdd(aRet , { "1" , cObs })
							cStatsC5 := "R"
							
						EndIF

						// -------------------------------------------------------------------------------------------------------------------------------------------
						// >>>> Valida desconto - Desconto total do pedido n�o pode ser maiior do que o estipulado no cadastro do cliente A1_XDMAXCO
						// -------------------------------------------------------------------------------------------------------------------------------------------
						nPercDescP		:=  ( 100 * (nTotDescP / nTotPed ) )
						If nPercDescP > SA1->A1_XDMAXCO
							cObs	:=  "COM010 - O % de Desconto dado no total do pedido � > MAIOR que o % Desc. M�ximo permitido ao cliente atrav�s do campo A1_XDMAXCO."
							AAdd(aRet , { "1" , cObs })
							cStatsC5 := "R"
							
						EndIF


						// -----------------------------------------------------------------------------------
						// >>>> Valida prazo m�dio
						// -------------------------------------------------------------------------------------------------------------------------------------------
						cPrazoMedio := AllTrim(u_ADMVXFUN( 6, "ZC6_PMEDCO" ))
						If !Empty(cPrazoMedio)
							If Len(aDatasEnt)>0
								aSort(aDatasEnt , , , {|x,y| x[1] < y[1]  }  )
								cObs := ""
								dDtAnterior	:= aDatasEnt[1,1]
								For nCont := 1 To Len(aDatasEnt)
									If nCont <>1
									
										nDifDias := (aDatasEnt[nCont,1] - dDtAnterior)
										If nDifDias < Val(cPrazoMedio) .AND. nDifDias > 0
											cObs += aDatasEnt[nCont,2] + " - "
										EndIf
									dDtAnterior	:= aDatasEnt[nCont,1]
									EndIF
								Next nCont
								If !Empty(cObs)
									cObs	:=  "COM12 - Existem itens com diferen�a de Data de entrega > MAIOR que o Prazo m�dio de entrega definido nos par�metros ADMV, atrav�s do campo ZC6_PMEDCO." + cObs
									AAdd(aRet , { "1" , cObs })
									cStatsC5 := "R"
									
								EndIF
							EndIf
						Else
							cObs	:=  "COM11 - N�o existe prazo m�dio cadastrado nos parametros ADMV. Verifique o cadastro atrav�s do campo ZC6_PMEDCO."
							AAdd(aRet , { "1" , cObs })
							cStatsC5 := "R"
							
						EndIf


					EndIf


				Else

					cObs	:=  "COM07 - A tabela " + cTabelaBase + " parametrizada nos parametros ADMV, n�o existe na tabela "
					AAdd(aRet , { "1" , cObs })
					cStatsC5 := "R"

				EndIf

			Else

				cObs	:=  "COM06 - N�o h� tabela base parametrizada no ADMV - Verifique os parametros ADMV."
				AAdd(aRet , { "1" , cObs })
				cStatsC5 := "R"

			EndIF



			// *** FIM DAS VALIDA��ES
			// =========================================



			// ---------------------------------------------------------------------------
			// Case n�o tenha encontrado inconsostencias - Inclui a de libera��o
			If Len(aRet)==0
				cObs 	:=  "N�o encontrada inconsist�ncias. Pedido liberado."
				AAdd(aRet , { "2" , cObs })
				cStatsC5 := "L"
				
			EndIf


		Else

			cObs 	:=  "Pedido de Venda APROVADO MANUALMENTE pelo usu�rio"
			AAdd(aRet , { "3" , cObs })
			cStatsC5 := "A"

		EndIf

	EndIF

Else

	cObs	:=  "COM05 - N�o foi possivel realizar avalia��o Comercial do pedido de venda por falta de campos. Contate o Administrador do sistema. - Avaliar UPDADMV"
	AAdd(aRet , { "1" , cObs })
	cStatsC5 := "R"

EndIf

Return aRet

//-----------------------------------------------
/*/{Protheus.doc} VDscCliePV
Valida��o do Percentual de desconto Comercial
Por defini��o do ADMV da Sestini, o percentual
n�o pode ultrapassar o Percentual cadastrado
no Cliente

@author DS2U (HFA)
@since 24/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function VDscCliePV( nDescCom )

	Local lRet		:= .T.
	Local nDescCli	:= GetAdvFVal( "SA1" , "A1_XDESCPV" , FWxFilial( "SA1" ) +  M->C5_CLIENTE + M->C5_LOJACLI , 1 )

	If nDescCom > nDescCli

		lRet := .F.
		Help(,, "VDscCliePV_01",, 'O percentual m�ximo de desconto para o este cliente � ' + cValToChar( nDescCli ) + '%.', 1, 0)

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} VDscNegoc
Valida��o do Percentual de desconto Negociado
Por defini��o do ADMV da Sestini, o percentual
n�o pode ultrapassar o Percentual cadastrado
na tabela de Parametriza��o ADMV

@author DS2U (HFA)
@since 24/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function VDscNegoc( nDescNeg )

	Local lRet		:= .T.
	Local nDsNegADMV:= U_ADMVXFUN( 6 , "ZC6_DESNEG" )

	If nDescNeg > nDsNegADMV

		lRet := .F.
		Help(,, "VDscNegoc_01",, 'O percentual m�ximo de desconto negociado definido pelas regras de ADMV � ' + cValToChar( nDsNegADMV ) + '%.', 1, 0)

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} VDscComerc
Valida��o do Percentual de desconto Comercial.
Por defini��o do ADMV da Sestini, o percentual
n�o pode ultrapassar o Percentual cadastrado
na tabela de Parametriza��o ADMV, e s� pode
ser aplicado caso o total do pedido de vendas
Seja superior ao valor minimo parametrizado pelo
ADMV - Inicialmente o Default � R$ 1.000,00

@author DS2U (HFA)
@since 24/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function VDscComerc( aDados )

	Local lRet			:= .T.
	Local nDsComrADMV	:= U_ADMVXFUN( 6 , "ZC6_DESCOM" )
	Local nVlrMinPed	:= U_ADMVXFUN( 6 , "ZC6_VMINDS" )
	Local nValorPed		:= 0
	Local nPsQtd		:= 0
	Local nPsVlTab		:= 0
	Local nC			:= 0
	Local cMsg			:= ""
	Local cTipo			:= "2" // Al�ada

	Default aDados		:= {}

	If Type("aHeader") == "A" .And. Type("aCols") == "A"

		nPsQtd 		:= aScan( aHeader , {| x | Alltrim( x[ 2 ] ) == "C6_QTDVEN"	} )
		nPsVlTab	:= aScan( aHeader , {| x | Alltrim( x[ 2 ] ) == "C6_XPRCTAB"} )

		If nPsQtd > 0 .And. nPsVlTab > 0

			// Retorna o valor total do pedido de venda, com base no pre�o original de tabela
			For nC := 1 To Len( aCols )
			
				If ( .not. aTail( aCols[ nC ] ) )

					nValorPed +=  aCols[ nC ][ nPsQtd ] * aCols[ nC ][ nPsVlTab ]
	
					If nValorPed >= nVlrMinPed
						Exit
					EndIf

				EndIf

			Next nC

		EndIf

	EndIf

	If nValorPed >= nVlrMinPed

		If M->C5_DESC3 > nDsComrADMV

			lRet := .F.
			cMsg := "VDscComerc_01 - O percentual m�ximo de desconto comercial definido pelas regras de ADMV � " + cValToChar( nDsComrADMV ) + "%."
			aAdd(aDados, { cTipo, cMsg } )

		EndIf

	Else

		lRet := .F.
		cMsg := "VDscComerc_02 - O Pedido de Vendas n�o pode ter desconto comercial, pois o pedido de vendas n�o possui o valor m�nimo de acordo com as regras ADMV - " + Alltrim( Transform(  nVlrMinPed , PesqPict("ZC6","ZC6_VMINDS") ) )
		aAdd(aDados, { cTipo, cMsg } )
			
	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FWhenDscC6
tratamento no When dos campos C6_DESCONT e
C6_VALDESC

@author DS2U (HFA)
@since 24/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FWhenDscC6()
	Local lRet 		:= .T.
	Local nPsNeg:= 0

	If Type("aHeader") == "A" .And. Type("aCols") == "A"

		If ( nPsNeg	:= aScan( aHeader , {| x | Alltrim( x[ 2 ] ) == "C6_XPRCNEG" } ) ) > 0

			lRet := aCols[ n ][ nPsNeg ] <> 'S'

		EndIf

	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} GVlrT100
Retorna o Valor de um produto informado no
cadastro da Tabela 100

@author DS2U (HFA)
@since 02/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function GVlrT100( cProduto )

	Local clTab100		:= PADR( U_ADMVXFUN( 6, "ZC6_TABBAS" ) , tamSX3("DA1_CODTAB")[1] )
	Local nPreco		:= 0

	Default cProduto	:= ""

	dbSelectArea( "DA1" )
	DA1->( dbSetOrder( 1 ) ) // DA1_FILIAL+DA1_CODTAB+DA1_CODPRO+DA1_INDLOT+DA1_ITEM

	If ( DA1->( dbSeek( xFilial( "DA1" ) + clTab100 + cProduto ) ) )

		nPreco := DA1->DA1_PRCVEN

	Endif

Return nPreco



//----------------------------------------------------------------------
/*/{Protheus.doc} ADMAvalCrd
Analises de credito para bloquear o pedido de vendas caso necess�rio,

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//----------------------------------------------------------------------
Static Function ADMAvalCrd()

    //----------------------------------------------------
    //Analises Credito para Novos Pedido de Venda
    //----------------------------------------------------
    U_ADP4ACrd( "APV"  , SC5->C5_CLIENTE , SC5->C5_LOJACLI , SC5->C5_FILIAL , SC5->C5_NUM , /*cObsRotina*/ ,/*nVlrLCD*/ )
        
Return

//----------------------------------------------------------------------
/*/{Protheus.doc} ADMAvalAlc
Analises de al�ada para bloquear o pedido de vendas caso necess�rio

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//----------------------------------------------------------------------
Static Function ADMAvalAlc( cPedido )
        
    U_ADP8VAlc( .T./*lCacular*/ , .T./*lPVGravado*/ , cPedido , FWxFilial("SC5")  , .T. /*lGravarAlc*/ , .F. )

Return


//-----------------------------------------------
/*/{Protheus.doc} ADMVSNdMail
ADMV Processamento 4 - FUN��O GEN�RICA PARA
ENVIO DE E-MAIL.

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVSNdMail(cAssunto, cMensagem, aAnexos, cEmailTo, cEmailCc, cErro , lAlert , lFormat )

    Local lRetorno 	:= .T.
    Local cServer   := GetMv("MV_RELSERV",,"")
    Local cAccount	:= GetMv("MV_RELACNT",,"")
    Local cPassword	:= GetMv("MV_RELAPSW",,"")
    Local lMailAuth := GetMv("MV_RELAUTH",,.F.)
    Local nTimeOut  := GetMv("MV_RELTIME",,120)
    Local lEnvMilSnt:= GetMV("ES_SNSNDML",,.F.) // ADMV - Envia e-mail pela Fun��o da Sestini, ou pela fun��o ADMV

    Default cEmailTo 	:= ""
    Default cEmailCc 	:= ""
    Default aAnexos		:= {}
    Default cMensagem	:= ""
    Default cAssunto	:= ""
    Default cErro		:= ""
    Default lAlert      := .T.
    Default lFormat   := .T.

    If lEnvMilSnt
    	
    	STNXSendMail(cEmailTo, cEmailCc, Nil/*cCCo*/, cAssunto , cMensagem )
    	
    Else
    
	    If !Empty(cEmailTo) .And. !Empty(cAssunto) .And. !Empty(cMensagem)
	
	        If MailSmtpOn( cServer, cAccount, cPassword , nTimeOut )
	
	            If lMailAuth
	
	                If ! ( lRetorno := MailAuth(cAccount,cPassword) )
	
	                    lRetorno := MailAuth(SubStr(cAccount,1,At("@",cAccount)-1),cPassword)
	
	                EndIf
	
	            Endif
	
	            If lRetorno
	
	                If !MailSend(cAccount,{cEmailTo},{cEmailCc},{},cAssunto,cMensagem,aAnexos, lFormat )
	
	                    cErro := "Erro na tentativa de e-mail para " + cEmailTo + ". ErrorMail: " + Mailgeterr()
	                    lRetorno := .F.
	
	                EndIf
	
	            Else
	
	                cErro := "Erro na tentativa de autentica��o da conta " + cAccount + ". "
	                lRetorno := .F.
	
	            EndIf
	
	            MailSmtpOff()
	
	        Else
	
	            cErro := "Erro na tentativa de conex�o com o servidor SMTP: " + cServer + " com a conta " + cAccount + ". "
	            lRetorno := .F.
	
	        EndIf
	
	    Else
	
	        If Empty(cEmailTo)
	
	            cErro := "� neess�rio fornecedor o destin�tario para o e-mail. "
	            lRetorno := .F.
	
	        EndIf
	
	        If Empty(cAssunto)
	
	            cErro := "� neess�rio fornecedor o assunto para o e-mail. "
	            lRetorno := .F.
	
	        EndIf
	
	        If Empty(cMensagem)
	
	            cErro := "� neess�rio fornecedor o corpo do e-mail. "
	            lRetorno := .F.
	
	        EndIf
	
	    Endif
	
	    If lAlert .And. !IsBlind()
	        If !Empty(cErro)
	            Help(,, "ADMVSNdMail",, cErro , 1, 0)
	        Else
	            Help(,, "ADMVSNdMail",, "e-mail enviado com sucesso!", 1, 0)
	        EndIf
	    Endif

    Endif
    	    
Return(lRetorno)

//-----------------------------------------------
/*/{Protheus.doc} VAMDVLbFat
Valida se o pedido de vendas est� liberado 
para faturamento de acordo com as valida��es/Regras
ADMV

@author DS2U (HFA)
@since 25/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function VAMDVLbFat()

	Local lRet		:= .T.
	Local aAreaSC5	:= SC5->( GetArea() )
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

	    //-------------------------------------------------
	    // Valida se o Pedido est� suspenso -  ADMV
	    //-------------------------------------------------
	    If ( lRet := FADPVSusp() )
	
		    //-----------------------------------
		    // Valida Analise de Credito Sestini
		    //-----------------------------------
		    If ( lRet := FADCredLib() )
	
		    	//---------------------------
		    	// Valise Analise de Al�ada
		    	//---------------------------
		    	lRet := FADAlcLib()
		    	
		    EndIf
		    
	    EndIf
	    
	endif

    RestArea( aAreaSC5 )

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FADPVSusp
Verificar se o Pedido est� Suspenso

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FADPVSusp()

	Local lRet	:= .T.

    If Alltrim( SC5->C5_XSITUAC ) == "S"

        lRet := .F.
        Help(,, "FADPVSusp_01",, "O Pedido "+ Alltrim( SC5->C5_NUM ) +" est� Suspenso, e n�o pode ser faturado."  , 1, 0)

    EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FADCredLib
Verificar se o Pedido est� com bloqueio de
Credito Sestini

@author DS2U (HFA)
@since 25/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FADCredLib()

	Local lRet	:= .T.

    //Verifica se o pedido esta bloqueado por Credito
    //P=Blq.Cred.Ped Venda;C=Blq.Cred Cliente;A=Aprovado Manual;R=Reprovado;S=Sem Analise;L=Liberado Sem Bloqueios
    If !( Alltrim(SC5->C5_XBLQCRE) $ "A|L" )

        lRet := .F.
        Help(,, "FADCredLib_01",, "Analise de Cr�dito do Pedido de Venda "+ Alltrim( SC5->C5_NUM ) +" est� com o status: "+  X3COMBO('C5_XBLQCRE',SC5->C5_XBLQCRE)  , 1, 0)

    EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FADAlcLib
Verificar se o Pedido est� com bloqueio de
Al�ada de Desconto - Sestini

@author DS2U (HFA)
@since 13/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FADAlcLib()

	Local lRet := .T.

    //Verifica se o pedido esta bloqueado por Al�ada
    //L=Liberado;A=Aprovado;P=Pr�-Aprovado;N=N�o Aprovado;R=Reprovado
    If !( Alltrim(SC5->C5_XALCADA) $ "A|L" )

        lRet := .F.
        Help(,, "FADAlcLib_01",, "An�lise de Al�ada de Desconto do Pedido de Venda "+ Alltrim( SC5->C5_NUM ) +" est� com o status: "+  X3COMBO('C5_XALCADA',SC5->C5_XALCADA)  , 1, 0)

    EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} GZCLAdmv
Retorna o conteudo da ZCL -  Complemento Regra
ADMV

@author DS2U (HFA)
@since 13/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function GZCLAdmv( uParam )
	
	Local cChave	:= FWxFilial("ZCL") + PADR( GetMv("ES_ADMVPAR",,"000001") , TamSX3("ZC6_CODIGO")[1] ) + uParam
	Local cRet		:= "" 
	
	dbSelectArea("ZCL")
	ZCL->( dbSetOrder( 1 ) ) // ZCL_FILIAL+ZCL_CODIGO+ZCL_TIPO+ZCL_CONTEU
	If ZCL->( dbSeek( cChave ) )
		
		While ZCL->(!EOF()) .And. ZCL->ZCL_FILIAL+ZCL_CODIGO+ZCL_TIPO == cChave
			
			cRet += IIF( !Empty(cRet) , ";", "") + Alltrim(ZCL->ZCL_CONTEU)
			
			ZCL->( dbSkip() )
		EndDo

	EndIf

Return cRet

//-----------------------------------------------
/*/{Protheus.doc} PVAvalADMV
Verifica se o Canal do pedido de venda est�
configurado nas regras ADMV para ser avaliado

@author DS2U (HFA)
@since 13/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function PVAvalADMV( cCanalPV )
	
	Local cCanaisADMV	:= U_ADMVXFUN( 15 , "6")		//Canais que s�o Avaliados pelas Regras ADMV
	Local lRet			:= .F.

	If cCanalPV $ cCanaisADMV
		
		lRet := .T.
		
	EndIf
	
Return lRet


//-----------------------------------------------
/*/{Protheus.doc} FVldMatriz
Valida��o no cadastro de Grupo Matriz - No campo
Matriz.

@author DS2U (HFA)
@since 13/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FVldMatriz( cMatriz )

	Local aAreaSA1	:= {}
	Local aAreaACY	:= ACY->(GetArea())
	Local lRet		:= .T.

	dbSelectArea("SA1")
	aAreaSA1 := SA1->( GetArea() )
	
	SA1->( dbSetOrder( 1 ) )
	If !( SA1->( dbSeek( FWxFilial("SA1") + cMatriz ) ) )

		lRet :=  .F.
		Help(,, "FVldMatriz_01",, 'C�digo Cliente Matriz n�o existe no cadastro de cliente.', 1, 0)

	Else
	
		ACY->( dbOrderNickName( 'ACYMATRIZ' ) ) //ACY_FILIAL+ACY_XMATRI+ACY_XLJMAT
		If ACY->( dbSeek( FWxFilial("ACY") + cMatriz ) )

			lRet :=  .F.
			Help(,, "FVldMatriz_02",, 'C�digo Cliente Matriz j� informado no Grupo: '+ Alltrim( ACY->ACY_GRPVEN)+' - '+ Alltrim( ACY->ACY_DESCRI ) +' .', 1, 0)

		EndIf
		
	EndIf
	
	RestArea( aAreaSA1 )
	RestArea( aAreaACY )

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} GetPrxEntg
Retorna a Proxima data mais proxima de entrega

@author DS2U (HFA)
@since 26/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function GetPrxEntg( cChaveSC6 )

	Local aAreaSC6		:= {}
	Local dDtEntrega	:= CtoD("  /  /  ")

	dbSelectArea( "SC6" )
	aAreaSC6:= SC6->( GetArea() )
	
	SC6->( dbSetOrder(1) )
	SC6->( dbGoTop() )
	
	If SC6->( dbSeek( cChaveSC6 ) )
		
		While SC6->( !Eof() ) .And. SC6->( C6_FILIAL + C6_NUM ) == cChaveSC6
		
			If Empty( SC6->C6_NOTA )
				
				If dDtEntrega < SC6->C6_ENTREG
					dDtEntrega := SC6->C6_ENTREG
				EndIf
				
			EndIf
			
			SC6->( dbSkip() )
		EndDo
		
	EndIf
	
	RestArea( aAreaSC6 )
	
Return dDtEntrega
