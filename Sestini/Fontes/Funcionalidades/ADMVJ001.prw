#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

//------------------------------------------------------------------
/*/{Protheus.doc}ADMVJ001
Job para suspender os Pedidos de Vendas que passaram
a quantidade de dias para aprova��o (quantidade de dias definido nos
parametros ADMV)

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//------------------------------------------------------------------
User Function ADMVJ001( aParam )

	Local cJob		:= "ADMVJ001"
	Local clEmp		:= ""
	Local clFil 	:= ""
	Local cUser 	:= ""
	Local cMsg		:= ""
	Local lOk		:= .T.

	Default aParam	:= { "","",""}

	clEmp	:= aParam[1]
	clFil	:= aParam[2]
	cUser	:= If(!Empty(aParam[3]),aParam[3],"000000")

	//--------------------------------------
	// Prepara o ambiente
	//--------------------------------------
	RpcSetType(3)

	If !RpcSetEnv(clEmp,clFil)

		cMsg := cJob + " ["+Alltrim(clEmp)+ "/" +Alltrim(clFil)+"] - ERRO - Nao foi possivel iniciar a empresa e filial!"
		PtInternal(1, cMsg )
		Conout(cMsg)
		lOk := .F.

	EndIf

	If lOk

		PtInternal(1, cJob + " ["+Alltrim(clEmp)+ "/" +Alltrim(clFil)+"] Processamento Iniciado - " + Time() )

		FProcJ01()

		PtInternal(1, cJob + " ["+Alltrim(clEmp)+ "/" +Alltrim(clFil)+"] Processamento Finalizado - " + Time() )

	EndIf

	//--------------------------------------
	// Finaliza a thread do Job
	//--------------------------------------
	RpcClearEnv()

Return

//---------------------------------------------
/*/{Protheus.doc}FProcJ01
Processamento do Job, executa a query
e atualiza os Pedidos de venda

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//---------------------------------------------
Static Function FProcJ01()

	Local nDias		:= U_ADMVXFUN( 6 , "ZC6_DMXALC" )
	Local cDtCorte	:= DtoS( MsDate() - nDias )
	Local cSql		:= FQryJ01( cDtCorte )
	Local clAlias	:= GetNextAlias()

    DbUseArea( .T. , "TOPCONN" , TcGenQry( , , cSql ) , clAlias , .F. , .T. )

    dbSelectArea(clAlias)

    ( clAlias )->(dbGoTop())

    If ( clAlias )->( !Eof() )

		dbSelectArea("SC5")
		SC5->( dbSetOrder( 1 ) )

    	While ( clAlias )->( !Eof() )

    		If SC5->( dbSeek( ( clAlias )->( C5_FILIAL + C5_NUM ) ) )

    			If SC5->( RecLock( "SC5" , .F. ) )
    				SC5->C5_XSITUAC	:= "S"
    				SC5->C5_XDTSUSP	:= MsDate()
    				SC5->C5_XHRSUSP	:= Time()
    				SC5->( MsUnLock() )
    			EndIf

    		EndIf

    		( clAlias )->( dbSkip() )
    	EndDo
    EndIf

	( clAlias )->( dbCloseArea() )

Return

//---------------------------------------------
/*/{Protheus.doc}FQryJ01
Monta a express�o SQL para identificar os
pedidos de vendas que devem ser suspenso

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//---------------------------------------------
Static Function FQryJ01( cDtCorte )

	Local cSql 			:= ""
	Local cCanaisADMV	:= U_ADMVXFUN( 15 , "6")

	cSql := " SELECT "
	cSql += " C5_FILIAL "
	cSql += " ,C5_NUM "
	cSql += " FROM " + RetSqlName("SC5") + " SC5 "
	cSql += " WHERE C5_FILIAL = '"+ FWxFilial("SC5") +"' "
	cSql += " AND C5_EMISSAO < '" + cDtCorte +  "'"
	cSql += " AND C5_XSITUAC <> 'S' "
	cSql += " AND C5_XALCADA IN ( 'P','N' ,' ' ) " // P=Pr�-Aprovado;N=N�o Aprovado
	cSql += " AND C5_XCANAL IN " + FormatIn( cCanaisADMV , ";")
	cSql += " AND SC5.D_E_L_E_T_ = ' '"
	cSql += " ORDER BY C5_FILIAL "
	cSql += " ,C5_NUM "

Return cSql