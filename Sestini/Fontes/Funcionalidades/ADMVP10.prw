#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"

//------------------------------------------------------------------->
/*/{Protheus.doc} ADMVP10(nOpc)

Respons�vel por realizar as chamadas:
nOpc = 1 -> chkRoy() - Gatilha se o cliente tem Royalties sim ou n�o.
nOpc = 2 -> getRoy() - Gatilha o pre�o de venda calculado com Royalties.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function ADMVP10(nOpc)

	Local uRet	   	 := NIL
	Local nTamCols 	 := Len(aCols)
	Local llExecADMV := getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )
    	Do Case
    		Case nOpc == 1
    			uRet := chkRoy()
    		Case nOpc == 2
    			uRet := getRoy(Nil, nTamCols)
    	EndCase
    endif
    
Return uRet

//------------------------------------------------------------------->
/*/{Protheus.doc} chkRoy

Gatilha no cabe�alho do pedido de venda se o cliente tem Royalties. (C5_XDESAG)

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function chkRoy()

Local cRet := ""

	cRet := Posicione("SA1",1,xFilial("SA1")+M->C5_CLIENTE+M->C5_LOJACLI,"SA1->A1_XDESAG")                      

Return cRet

//------------------------------------------------------------------->
/*/{Protheus.doc} getRoy

Respons�vel por realizar o c�lculo dos Royalties no momento
do preenchimento do pre�o de venda (C6_PRCVEN).

@author TOTVS Protheus
@since  30/07/2018
@return nPrcVen, Pre�o calculado de Royalties ou Pre�o digitado.
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function getRoy(cCampo, nVezes)

Local nPerDes 		:= Posicione("SA1",1,xFilial("SA1")+M->C5_CLIENTE+M->C5_LOJACLI,"SA1->A1_XPERDES")
Local cRoy			:= Posicione("SA1",1,xFilial("SA1")+M->C5_CLIENTE+M->C5_LOJACLI,"SA1->A1_XDESAG")
Local nPosQTDVen	:= GDFIELDPOS("C6_QTDVEN")  
Local nPosProdut	:= GDFIELDPOS("C6_PRODUTO")
Local cProdutRoy	:= SuperGetMV("ES_PRODLIC")
Local nPosVlrVen	:= GDFIELDPOS("C6_VALOR")  
Local nPosVlrTot	:= GDFIELDPOS("C6_VALOR")  
Local nPosPrunit	:= GDFIELDPOS("C6_PRUNIT")  
Local nPosValDes	:= GDFIELDPOS("C6_XVALDES") // 'Valor Total do Des�gio do Item'  
Local nPosVlItDs	:= GDFIELDPOS("C6_XVLITDS") // 'Valor Unit�rio do Des�gio do Item'
Local nPrcVen		:= M->C6_PRCVEN 
Local llExecADMV	:= getMv( "ES_ADMVON",, .T. )
    
Default nVezes := 1

if ( llExecADMV )
	
	If cRoy == "S"
		If aCols[n,nPosProdut] <> Alltrim(cProdutRoy)
		
			aCols[n,nPosValDes]	:= aCols[n,nPosVlrVen] * (nPerDes/100)
			aCols[n,nPosVlItDs]	:= aCols[n,nPosValDes] / IIf(aCols[n,nPosQTDVen]>0,aCols[n,nPosQTDVen],1)	// C6_XVALDES / C6_QTDVEN
					
			nPrcVen 			:= nPrcVen - aCols[n,nPosVlItDs]
			aCols[n,nPosVlrTot] := nPrcVen * aCols[n,nPosQTDVen]
			aCols[n,nPosPrunit] := nPrcVen
			
		EndIf
	EndIf
endif


Return nPrcVen