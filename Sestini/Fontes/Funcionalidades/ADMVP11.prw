#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWADAPTEREAI.CH"

#DEFINE COR_VERDE	"BR_VERDE"
#DEFINE COR_AMARE	"BR_AMARELO"

//------------------------------------------------------------------->
/*/{Protheus.doc} ADMVP11

Rotina respons�vel por gerar os pedidos de licenciamentos.

parametros envolvidos: ES_PRODLIC = c�digo do produto de licenciamento
					   ES_CODEFL  = c�digo da filial de licenciamento
@author DS2U
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function ADMVP11()

	local _lErro	:=.F.
	local aAdvSize  := FWGetDialogSize(oMainWnd)
	local olLayer
	local olPainelNF
	local olPainelCl
	local oLbxNF
	local oLbxCPF
	local oLbxGRP
	local olBtnSair
	local olSplit1
	local olBtnProc
	local _cPerg	:= "ADMVP11"
	local _aMatPE	:= {}
	local _aMatA1	:= {}
	local _aMatA2	:= {}
	local _cIniFim	:= ""
	local _cValTot	:= ""  
	local _cValDes	:= ""
	local oSayP
	local oFolderD
	
	private _nValTot := 0
	private _nValDes := 0
	
	if (  pergunte(_cPerg, .T.) )
	
		Define Font oFont12B Name "Arial" Size 0,-12 Bold
		Define Font oFont14B Name "Arial" Size 0,-14 Bold
	
		_aMatPE := aClone(NFS(mv_par01, mv_par02))
		_aMatA1 := aClone(CPF(mv_par01, mv_par02))
		_aMatA2 := aClone(GRP(mv_par01, mv_par02))
		
		If Empty(_aMatPE) .OR. Empty(_aMatA1) .OR. Empty(_aMatA2)
			_lErro := .T.
			Aviso("Apura��o de Royalties" , "Nenhum registro foi encontrado com os parametros informados.", {"Ok"} , 2 )
		EndIf
	
		If !_lErro
	
			DEFINE MSDIALOG oDlg TITLE "Royalties - Gera��o de Licenciamento" FROM aAdvSize[1],aAdvSize[2] To aAdvSize[3],aAdvSize[4] OF oMainWnd PIXEL
			
				olLayer	:= fwLayer():new()
				olLayer:init( oDlg, .F. )
				
				olLayer:addLine("CABECALHO",90, .T. )
				olLayer:addLine("RODAPE",10, .T. )
				
				olLayer:addCollumn( "CENTER1" , 50, .F., "CABECALHO" )
				olLayer:addCollumn( "CENTER2" , 50, .F., "CABECALHO" )
				olLayer:addCollumn( "CENTERR" , 100, .F., "RODAPE" )
				
				olLayer:addWindow( "CENTER1", "olPainelNF", "Por Nota Fiscal", 100, .F., .F.,,"CABECALHO",{|| })
				olPainelNF := olLayer:getWinPanel("CENTER1","olPainelNF","CABECALHO")
				
				olLayer:addWindow( "CENTER2", "olPainelCl", "Por Cliente", 100, .F., .F.,,"CABECALHO",{|| })
				olPainelCl := olLayer:getWinPanel("CENTER2","olPainelCl","CABECALHO")
				
				@ 1, 1 Listbox  oLbxNF Var  cVar Fields Header "DOC", "SERIE", "CLIENTE", "NOME", "TOTAL", "ROYALTIES" Size 330, 180 Of olPainelNF Pixel
				oLbxNF:SetArray(  _aMatPE )
				oLbxNF:bLine := {|| { _aMatPE[oLbxNF:nAt,1],_aMatPE[oLbxNF:nAt,2],_aMatPE[oLbxNF:nAt,3], _aMatPE[oLbxNF:nAt,4], _aMatPE[oLbxNF:nAt,5], _aMatPE[oLbxNF:nAt,6]}}
				oLbxNF:align := CONTROL_ALIGN_ALLCLIENT
				
				//-------------------------------------------------
				@ 10,02 Folder oFolderD Prompts "CNPJ","Grupo Empresarial" Size 333,202 Pixel Of olPainelCl
				oFolderD:align := CONTROL_ALIGN_ALLCLIENT
				
				@ 1, 1 Listbox  oLbxCPF Var  cVar Fields Header "CNPJ", "TOTAL", "ROYALTIES" Size 328, 180 Of oFolderD:aDialogs[1] Pixel
				oLbxCPF:SetArray(  _aMatA1 )
				oLbxCPF:bLine := {|| { _aMatA1[oLbxCPF:nAt,1],_aMatA1[oLbxCPF:nAt,2],_aMatA1[oLbxCPF:nAt,3]}}
				oLbxCPF:align := CONTROL_ALIGN_ALLCLIENT
				
				@ 1, 1 Listbox  oLbxGRP Var  cVar Fields Header "GRP EMPRESARIAL", "TOTAL", "ROYALTIES" Size 328, 180 Of oFolderD:aDialogs[2] Pixel
				oLbxGRP:SetArray(  _aMatA2 )
				oLbxGRP:bLine := {|| { _aMatA1[oLbxGRP:nAt,1],_aMatA2[oLbxGRP:nAt,2],_aMatA2[oLbxGRP:nAt,3]}}
				oLbxGRP:align := CONTROL_ALIGN_ALLCLIENT
				
				//-------------------------------------------------
				_cIniFim := dToC(mv_par01) + " a " + DTOC(mv_par02)
				_cValTot := allTrim( transform(_nValTot , "@E 999,999,999,999.99") )  
				_cValDes := allTrim( transform(_nValDes , "@E 999,999,999,999.99") )
		
				@ 10,05 SAY oSayP PROMPT "Per�odo : " + _cIniFim  SIZE 200, 010 OF olLayer:getColPanel( "CENTERR", "RODAPE" ) PIXEL Font oFont14B COLOR CLR_BLUE
				@ 22,05 SAY oSayP PROMPT "Total Faturamento ( Per�odo ): R$ " + _cValTot  SIZE 200, 010 OF olLayer:getColPanel( "CENTERR", "RODAPE" ) PIXEL Font oFont14B COLOR CLR_BLUE
				@ 34,05 SAY oSayP PROMPT "Total Royalties ( Per�odo ) R$ : " + _cValDes 	SIZE 200, 010 OF olLayer:getColPanel( "CENTERR", "RODAPE" ) PIXEL Font oFont14B COLOR CLR_BLUE
				
				olBtnSair := TButton():New(0,0,"Sair", olLayer:getColPanel( "CENTERR", "RODAPE" ),{|| oDlg:End() },040,010,,oFont12B,,.T.,,"",,,,)
				olBtnSair:align := CONTROL_ALIGN_RIGHT
				
				olSplit1 := tPanel():New(01,01,"",olLayer:getColPanel( "CENTERR", "RODAPE" ),,,,,CLR_WHITE,10,10)
				olSplit1:align := CONTROL_ALIGN_RIGHT
				
				olBtnProc := TButton():New(0,0,"Processar", olLayer:getColPanel( "CENTERR", "RODAPE" ),{|| oDlg:End(), Processar(_aMatPE), Gerar() },040,010,,oFont12B,,.T.,,"",,,,)
				olBtnProc:align := CONTROL_ALIGN_RIGHT
				
				olLayer:show()
				
			ACTIVATE MSDIALOG oDlg CENTERED
			
		endif
		
	endif

Return

//------------------------------------------------------------------->
/*/{Protheus.doc} NFS

Query respons�vel por encontrar as NFs de sa�da e devolu��o
que contenham Royalties.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function NFS(DtIni, DtFim)
Local aRet		:={}
Local _xFilSA1	:= xFilial("SA1")
Local _xFilSD1	:= xFilial("SD1")
Local _xFilSD2	:= xFilial("SD2")
Local cAliasNFS := GetNextAlias()
Local _cDoc		:= ""
Local _cSer   	:= ""
Local _cCli   	:= ""
Local _cNom		:= ""
Local _cTot   	:= ""
Local _cDes   	:= ""
Local _cQuery   := ""	
	
_cQuery := "SELECT DOC, SERIE, CLIENTE, NOME, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO FROM "
_cQuery += "(SELECT D2_DOC DOC, D2_SERIE SERIE, D2_CLIENTE+'/'+D2_LOJA CLIENTE, A1_NOME NOME, D2_EMISSAO, SUM(D2_TOTAL) TOTAL, SUM(D2_XVALDES) DESAGIO "
_cQuery += "FROM " + RetSQLName("SD2") + " SD2 "
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + " ON D2_CLIENTE=A1_COD "
_cQuery += "AND D2_LOJA=A1_LOJA "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D2_TIPO='N' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D2_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY D2_DOC, D2_SERIE, D2_CLIENTE, D2_LOJA, A1_NOME, D2_EMISSAO "
_cQuery += "UNION SELECT D1_NFORI DOC, D1_SERIORI SERIE, D1_FORNECE+'/'+D1_LOJA CLIENTE, A1_NOME NOME, D2_EMISSAO, (SUM(D2_TOTAL)*-1) TOTAL, SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
_cQuery += "FROM " + RetSQLName("SD1") + " SD1 "
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + "ON D1_FORNECE=A1_COD "
_cQuery += "AND D1_LOJA=A1_LOJA "
_cQuery += "INNER JOIN " + RetSqlName( "SD2" ) + " SD2 " + "ON D1_NFORI=D2_DOC "
_cQuery += "AND D1_SERIORI=D2_SERIE "
_cQuery += "AND D1_ITEMORI=D2_ITEM "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD1.D1_TIPO='D' "
_cQuery += "AND SD1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D1_FILIAL = '" + _xFilSD1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D1_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY D1_NFORI, D1_SERIORI, D1_FORNECE, D1_LOJA, A1_NOME, D2_EMISSAO) TST "
_cQuery += "WHERE DESAGIO<>0 "
_cQuery += "GROUP BY DOC, SERIE, CLIENTE, NOME "

_cQuery := ChangeQuery(_cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),cAliasNFS,.F.,.T.)

	While (cAliasNFS)->(!Eof())
		_cDoc	:= (cAliasNFS)->DOC
		_cSer   := (cAliasNFS)->SERIE
		_cCli   := (cAliasNFS)->CLIENTE
		_cNom	:= (cAliasNFS)->NOME
		_cTot   := TransForm((cAliasNFS)->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm((cAliasNFS)->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		_nValTot += (cAliasNFS)->TOTAL
		_nValDes += (cAliasNFS)->DESAGIO

		aAdd(aRet,{_cDoc, _cSer, _cCli, _cNom, _cTot, _cDes})

		(cAliasNFS)->(DbSkip())
	Enddo

	(cAliasNFS)->(DbCloseArea()) 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} CPF

Query respons�vel por agrupar os valores por CNPJ.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function CPF(DtIni, DtFim)

Local aRet		:={}
Local _xFilSA1	:= xFilial("SA1")
Local _xFilSD1	:= xFilial("SD1")
Local _xFilSD2	:= xFilial("SD2")
Local cAliasCPF := GetNextAlias()
Local _cCgc		:= ""
Local _cTot   	:= ""
Local _cDes   	:= ""
Local _cQuery	:= ""

_cQuery := "SELECT GRPCGC, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO "
_cQuery += "FROM "
_cQuery += "(SELECT A1_CGC GRPCGC, D2_EMISSAO, SUM(D2_TOTAL) TOTAL, SUM(D2_XVALDES) DESAGIO "
_cQuery += "FROM " + RetSQLName("SD2") + " SD2 " 
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + " ON D2_CLIENTE=A1_COD "
_cQuery += "AND D2_LOJA=A1_LOJA "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D2_TIPO='N' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D2_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY A1_CGC, D2_EMISSAO "
_cQuery += "UNION SELECT A1_CGC GRPCGC, D2_EMISSAO, (SUM(D2_TOTAL)*-1) TOTAL, SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
_cQuery += "FROM "+ RetSQLName("SD1") + " SD1 " 
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + " ON D1_FORNECE=A1_COD "
_cQuery += "AND D1_LOJA=A1_LOJA "
_cQuery += "INNER JOIN " + RetSqlName( "SD2" ) + " SD2 " + "ON D1_NFORI=D2_DOC "
_cQuery += "AND D1_SERIORI=D2_SERIE "
_cQuery += "AND D1_ITEMORI=D2_ITEM "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD1.D1_TIPO='D' "
_cQuery += "AND SD1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D1_FILIAL = '" + _xFilSD1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D1_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY A1_CGC, D2_EMISSAO) TST "
_cQuery += "WHERE DESAGIO<>0 "
_cQuery += "GROUP BY GRPCGC "
	
_cQuery := ChangeQuery(_cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),cAliasCPF,.F.,.T.)

	While (cAliasCPF)->(!Eof())
		_cCgc	:= (cAliasCPF)->GRPCGC
		_cTot   := TransForm((cAliasCPF)->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm((cAliasCPF)->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		aAdd(aRet,{_cCgc, _cTot, _cDes})
		(cAliasCPF)->(DbSkip())
	Enddo

	(cAliasCPF)->(DbCloseArea()) 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} GRP

Query respons�vel por agrupar os valores por grupo empresarial.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->


Static Function GRP(DtIni, DtFim)
	Local aRet:={}

Local _xFilSA1	:= xFilial("SA1")
Local _xFilSD1	:= xFilial("SD1")
Local _xFilSD2	:= xFilial("SD2")
Local cAliasGRP := GetNextAlias()
Local _cEmp		:= ""
Local _cTot   	:= ""
Local _cDes   	:= ""
Local _cQuery	:= ""

_cQuery := "SELECT GRPEMP, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO "
_cQuery += "FROM "
_cQuery += "(SELECT A1_XGRPEMP GRPEMP, D2_EMISSAO, SUM(D2_TOTAL) TOTAL, SUM(D2_XVALDES) DESAGIO "
_cQuery += "FROM " + RetSQLName("SD2") + " SD2 "
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + " ON D2_CLIENTE=A1_COD "
_cQuery += "AND D2_LOJA=A1_LOJA "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D2_TIPO='N' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D2_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY A1_XGRPEMP, D2_EMISSAO "
_cQuery += "UNION SELECT A1_XGRPEMP GRPEMP, D2_EMISSAO, (SUM(D2_TOTAL)*-1) TOTAL, SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
_cQuery += "FROM " + RetSQLName("SD1") + " SD1 "
_cQuery += "INNER JOIN " + RetSqlName( "SA1" ) + " SA1 " + "ON D1_FORNECE=A1_COD "
_cQuery += "AND D1_LOJA=A1_LOJA "
_cQuery += "INNER JOIN SD2010 SD2 ON D1_NFORI=D2_DOC "
_cQuery += "AND D1_SERIORI=D2_SERIE "
_cQuery += "AND D1_ITEMORI=D2_ITEM "
_cQuery += "WHERE SA1.D_E_L_E_T_=' ' "
_cQuery += "AND SD1.D1_TIPO='D' "
_cQuery += "AND SD1.D_E_L_E_T_=' ' "
_cQuery += "AND SD2.D_E_L_E_T_=' ' "
_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' "
_cQuery += "AND D1_FILIAL = '" + _xFilSD1 + "' "
_cQuery += "AND D2_FILIAL = '" + _xFilSD2 + "' "
_cQuery += "AND D1_EMISSAO BETWEEN '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' "
_cQuery += "GROUP BY A1_XGRPEMP, D2_EMISSAO) TST "
_cQuery += "WHERE DESAGIO<>0 "
_cQuery += "GROUP BY GRPEMP ""

_cQuery := ChangeQuery(_cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),cAliasGRP,.F.,.T.)


	While (cAliasGRP)->(!Eof())
		_cEmp	:= (cAliasGRP)->GRPEMP
		_cTot   := TransForm((cAliasGRP)->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm((cAliasGRP)->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		aAdd(aRet,{_cEmp, _cTot, _cDes})

		(cAliasGRP)->(DbSkip())
	Enddo

	(cAliasGRP)->(DbCloseArea()) 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} PROCESSAR

Faz c�pia dos resultados das querys para tabela ZC7.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Processar(_aMat)
	Local _aArea  := GetArea()
	Local _nTam  := Len(_aMat)
	Local _lFez  := .F.
	Local _nI
	Local cIdRoy := ""
	Local cTipoNF := ""

	If  _nTam > 0 
		ProcRegua( _nTam )

		Begin Transaction
			For _nI:=1 to _nTam
				Incproc("Gera��o de Fila: " + _aMat[_nI,1]) 
				dbSelectArea("ZC7")
				ZC7->(dbSetOrder(1))
				cTipoNF := IIF(Val(_aMat[_nI,6]) > 0, "N", "D")

				If ZC7->(!dbSeek(xFilial("ZC7")+(_aMat[_nI,1]+_aMat[_nI,2]+cTipoNF))) 
					ZC7->(RecLock("ZC7", .T.))
						cIdRoy := GetSx8Num("ZC7","ZC7_IDPDES")
						ConfirmSX8()
						ZC7->ZC7_FILIAL		:= xFilial("ZC7")
						ZC7->ZC7_STATUS		:= " "
						ZC7->ZC7_CODCLI  	:= SubStr(Alltrim(_aMat[_nI,3]),1,8)	// "CLIENTE"
						ZC7->ZC7_LOJCLI  	:= SubStr(Alltrim(_aMat[_nI,3]),10,4)	// "LOJA" 
						ZC7->ZC7_NOME  		:= _aMat[_nI,4]							// "NOME" 
						ZC7->ZC7_DTEMIS     := dDataBase
						ZC7->ZC7_VALDES		:= Val(_aMat[_nI,6])					// "DESAGIO"
						ZC7->ZC7_NOTA  		:= _aMat[_nI,1]							// "DOC"
						ZC7->ZC7_SERIE  	:= _aMat[_nI,2]							// "SERIE"
						ZC7->ZC7_IDPDES		:= cIdRoy
						ZC7->ZC7_TIPONF		:= cTipoNF
						If cTipoNF == "N"
							ZC7->ZC7_EMISNF		:= Posicione("SF2",1,xFilial("SF2")+ _aMat[_nI,1] + _aMat[_nI,2],"F2_EMISSAO")
						Else
							ZC7->ZC7_EMISNF		:= Posicione("SD1",19,xFilial("SD1")+ _aMat[_nI,1] + _aMat[_nI,2] + SubStr(Alltrim(_aMat[_nI,3]),1,8) + SubStr(Alltrim(_aMat[_nI,3]),10,4),"D1_EMISSAO")
						EndIf
												ZC7->ZC7_CONDPG		:= Posicione("SF2",1,xFilial("SF2")+ _aMat[_nI,1] + _aMat[_nI,2],"F2_COND")
					ZC7->(MsUnLock())
					_lFez  := .T.
				EndIf
			Next _nI   

			If _lFez 
				MsgAlert("Gera��o de Fila de Pedidos de Licenciamento Realizada com Sucesso!")
			Else
				MsgAlert("N�o houve nenhum Pedido de Licenciamento gerado!")
			EndIf

		End Transaction
	Else
		MsgAlert("N�o h� nenhuma Nota Fiscal para a Gera��o de Fila de Pedidos de Licenciamento!")
	EndIf

	RestArea(_aArea)
Return 


//------------------------------------------------------------------->
/*/{Protheus.doc} Gerar

Tela para leitura dos dados gerados na ZC7.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Gerar()

	Local aAdvSize  := FWGetDialogSize(oMainWnd)
	Local oDlg
	Local aHeadSize := {20,20,40,20,20} 
	Local oPanel
	Local _aMatP  	:= {}
	Local olLayer
	Local olPainelNF
	Local oButtonA
	Local oButtonB
	Local olBtnSair
	Local olSplit1
	Local olBtnProc

	_aMatP := aClone(ZC7(mv_par01, mv_par02))
	
	If Len(_aMatP) > 0

		Define Font oFont12B Name "Arial" Size 0,-12 Bold
	
		DEFINE MSDIALOG oDlg TITLE "Royalties - Gera��o de Licenciamento" FROM aAdvSize[1],aAdvSize[2] To aAdvSize[3],aAdvSize[4] OF oMainWnd PIXEL
			
			olLayer	:= fwLayer():new()
			olLayer:init( oDlg, .F. )
			
			olLayer:addLine("CABECALHO",90, .T. )
			olLayer:addLine("RODAPE",10, .T. )
			
			olLayer:addCollumn( "CENTER1" , 100, .F., "CABECALHO" )
			olLayer:addCollumn( "CENTERR" , 100, .F., "RODAPE" )
			
			olLayer:addWindow( "CENTER1", "olPainelNF", "", 100, .F., .F.,,"CABECALHO",{|| })
			olPainelNF := olLayer:getWinPanel("CENTER1","olPainelNF","CABECALHO")
			
			oLBx := TWBrowse():New(010,000,665,220,Nil,{"STATUS", "CLIENTE", "NOME", "EMISSAO", "DESAGIO"}, aHeadSize, olPainelNF ,,,,,,,,,,,,,,.T.)
			oLBx:SetArray(_aMatP)
			oLBx:bLine:={|| {	_aMatP[oLBx:nAt,1],;
			_aMatP[oLBx:nAt,2],;
			_aMatP[oLBx:nAt,3],;
			_aMatP[oLBx:nAt,4],;
			_aMatP[oLBx:nAt,5] } }
			oLBx:align := CONTROL_ALIGN_ALLCLIENT
			
			olBtnSair := TButton():New(0,0,"Executar", olLayer:getColPanel( "CENTERR", "RODAPE" ),{|| oDlg:End(), FWMsgRun(, {|| Executar() }, "Processando", "Gerando Pedidos e Notas Fiscais...") },040,010,,oFont12B,,.T.,,"",,,,)
			olBtnSair:align := CONTROL_ALIGN_RIGHT
			
			olSplit1 := tPanel():New(01,01,"",olLayer:getColPanel( "CENTERR", "RODAPE" ),,,,,CLR_WHITE,10,10)
			olSplit1:align := CONTROL_ALIGN_RIGHT
			
			olBtnProc := TButton():New(0,0,"Cancelar", olLayer:getColPanel( "CENTERR", "RODAPE" ),{||  oDlg:End() },040,010,,oFont12B,,.T.,,"",,,,)
			olBtnProc:align := CONTROL_ALIGN_RIGHT
			
			olLayer:show()
	
		ACTIVATE MSDIALOG oDlg CENTERED
		
	else
		alert( "Nao existe fila de processamento pendente!" )
	endif


Return


//------------------------------------------------------------------->
/*/{Protheus.doc} ZC7

Procura os registros de NF 

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function ZC7(DtIni, DtFim)
	Local aRet		:={}
	Local _xFilZC7	:= xFilial("ZC7")
	Local _cSta		:= ""
	Local _cCli		:= ""
	Local _cNom		:= ""
	Local _cEmi     := ""
	Local _cIDP
	Local _cDes   	:= ""

	Local _cQuery := "SELECT ZC7_STATUS, ZC7_CODCLI, ZC7_LOJCLI, ZC7_NOME, ZC7_DTEMIS, ZC7_VALDES, ZC7_NOTA, ZC7_SERIE, ZC7_IDPDES, R_E_C_N_O_ ZC7_RECNO  " 

	_cQuery += "FROM " + RetSQLName("ZC7") + " ZC7 "
	_cQuery += "WHERE ZC7.ZC7_FILIAL = '" + _xFilZC7 + "' AND " 
	_cQuery += "ZC7.ZC7_EMISNF BETWEEN  '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' AND " 
	_cQuery += "ZC7.D_E_L_E_T_ =' '  " 
	_cQuery += "ORDER BY ZC7.ZC7_NOTA, ZC7.ZC7_SERIE "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)

	Dbselectarea("TMP")
	dbGotop()
	If !Eof()
		While !Eof() 

			If !Empty(TMP->ZC7_STATUS)
				_cSta := LoadBitmap( GetResources(), COR_VERDE ) 		// "BR_VERDE"
			Else
				_cSta := LoadBitmap( GetResources(), COR_AMARE ) 		// "BR_AMARELO"
			EndIf	

			_cCli	:= TMP->ZC7_CODCLI+"/"+TMP->ZC7_LOJCLI
			_cNom	:= TMP->ZC7_NOME
			_cEmi   := DTOC(STOD(TMP->ZC7_DTEMIS))
			_cDes   := TransForm(TMP->ZC7_VALDES,PesqPict("SD2","D2_XVALDES"))		
			_cIDP   := TMP->ZC7_IDPDES
			_cRec   := TMP->ZC7_RECNO
			aAdd(aRet,{_cSta, _cCli, _cNom, _cEmi, _cDes, _cIDP, _cRec})
			dbSelectArea("TMP")
			DbSkip()
		Enddo
	EndIf
	dbSelectArea("TMP")
	DbCloseArea() 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} Executar

Fun��o respons�vel por realizar as chamadas:
1) Gera��o pedido de venda (GERAPV())
2) Gera��o de faturamento autom�tico (GERANFS)
3) Gera��o de NCC (GERANCC())

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Executar()

Local _cProduto 	:= SuperGetMV("ES_PRODLIC") // "000000123456789"
Local _cFilDes  	:= SuperGetMV("ES_CODEFL") 	// xFilial("SC5")
Local _cTes 		:= MV_PAR03
Local dDataDe		:= MV_PAR01
Local dDataAte		:= MV_PAR02
Local cCondPag		:= "" 	
Local _cFilOri		:= xFilial("SC5")
Local cNumPed	 	:= ""
Local _cSerie       := mv_par04
Local _aArea  		:= GetArea()
Local _cIDPDES      
Local nRoyDesc		:= 0
Local cNF			:= ""
Local cPedido		:= ""
Local cPedMsg		:= ""
Local cNFMsg		:= ""
Local _dVencto      := dDataBase
Local cAliasQry		:= GetNextAlias()
Local cAliasVlr		:= GetNextAlias()
Local cQryCli		:= ""
Local cQryVlr		:= ""
Local nValorRoy 	:= 0
Local aRecnoZC7		:= {}
Local cNfZc7		:= ""
Local cNCCMsg		:= ""
Local nX			:= 0
Local cCodCli		:= ""
Local cLojCli		:= ""

cQryCli	:= "SELECT ZC7_FILIAL, ZC7_STATUS, ZC7_CODCLI, ZC7_LOJCLI, ZC7_DTEMIS, ZC7_CONDPG, ZC7_NOTA, ZC7_SERIE, ZC7_VALDES, R_E_C_N_O_ FROM " + RetSQLName("ZC7") + " ZC7 "
cQryCli += "WHERE ZC7.D_E_L_E_T_ = ' '
cQryCli += "AND ZC7_STATUS = ' '
cQryCli	+= "AND ZC7_VALDES > 0 "
cQryCli += "AND ZC7_FILIAL = '" + xFilial("ZC7") + "' "
cQryCli += "AND ZC7_EMISNF BETWEEN '" + DtoS(dDatade)+ "' AND '" + DtoS(dDataAte) + "'
cQryCli += "ORDER BY  ZC7_CODCLI, ZC7_LOJCLI, ZC7_CONDPG, ZC7_NOTA, ZC7_SERIE "

cQryCli := ChangeQuery(cQryCli)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQryCli),cAliasQry,.F.,.T.)
	
While (cAliasQry)->(!Eof()) 
		
	cCodCli := Alltrim((cAliasQry)->ZC7_CODCLI)
	cLojCli	 := Alltrim((cAliasQry)->ZC7_LOJCLI) 
	cCondPag := Alltrim((cAliasQry)->ZC7_CONDPG)
	nRoyDesc := Posicione("SA1",1,xFilial("SA1") + cCodCli + cLojCli,"A1_XDSVDES") //Perc de Desconto de Royalties
	
	If aScan(aRecnoZC7,(cAliasQry)->R_E_C_N_O_) == 0
	
		cQryVlr	:= "SELECT ZC7_FILIAL, ZC7_VALDES, ZC7_CODCLI, ZC7_LOJCLI, ZC7_CONDPG, R_E_C_N_O_, ZC7_NOTA, ZC7_SERIE FROM " + RetSQLName("ZC7") + " ZC7 "
		cQryVlr += "WHERE ZC7.D_E_L_E_T_ = ' '
		cQryVlr += "AND ZC7_STATUS = ' '
		cQryVlr	+= "AND ZC7_VALDES > 0 "
		cQryVlr += "AND ZC7_FILIAL = '" + xFilial("ZC7") + "' "
		cQryVlr += "AND ZC7_CODCLI = '" + cCodCli + "' "
		cQryVlr += "AND ZC7_LOJCLI = '" + cLojCli + "' "
		cQryVlr += "AND ZC7_CONDPG = '" + cCondPag + "' "
		cQryVlr += "AND ZC7_EMISNF BETWEEN '" + DtoS(dDatade)+ "' AND '" + DtoS(dDataAte) + "'
	
		cQryVlr := ChangeQuery(cQryVlr)
		dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQryVlr),cAliasVlr,.F.,.T.)
		
		While (cAliasVlr)->(!Eof()) 
		
			nValorRoy += (cAliasVlr)->ZC7_VALDES //SOMA VALOR DAS NFS PARA GERAR PEDIDO �NICO
			aAdd(aRecnoZC7,(cAliasVlr)->R_E_C_N_O_)//GUARDA RECNO PARA ATUALIZAR STATUS DEPOIS.
			cNfZc7	 := Alltrim((cAliasVlr)->ZC7_NOTA) + Alltrim((cAliasVlr)->ZC7_SERIE)
			
			(cAliasVlr)->(DbSkip())
		EndDo
		
		Begin Transaction
			
			nValorRoy := nValorRoy - (nValorRoy * (nRoyDesc/100)) //Aplica desconto de Royalties
			cNfZc7	 := Alltrim((cAliasVlr)->ZC7_NOTA) + Alltrim((cAliasVlr)->ZC7_SERIE)
			
			//---GERANDO PEDIDO DE VENDA---//
			aPvlNfs := GeraPV( cCodCli, cLojCli, nValorRoy, _cProduto, _cTes, cCondPag, _cFilDes, cNfZc7)
			
			//---GERANDO NF DE SA�DA---//
			If !Empty(aPvlNfs)
				cNF := GeraNFS(aPvlNfs[1], _cSerie, _cFilDes, _cFilOri, aPvlNfs)
				
				cNFMsg += " [" + cNF + "] "
				cPedMsg += " [" + aPvlNfs[1][1] + "] "
			EndIf
			
			
			//---ATUALIZA OS STATUS DA TABELA ZC7 (FILA DE PROCESSAMENTO)---//	
			For nX := 1 to Len(aRecnoZC7)
				DbSelectArea("ZC7")
				DbGoto(aRecnoZC7[nX]) 
				ZC7->(RecLock("ZC7", .F.))
					ZC7->ZC7_STATUS		:= "*"
				ZC7->(MsUnLock())
			Next nX
		
		End Transaction	
		
		(cAliasVlr)->(DbCloseArea())
		nValorRoy := 0
	Else
	
		(cAliasQry)->(DbSkip())
	
	EndIf
Enddo
	
	FWMsgRun(, {|| GeraNCC(dDatade,dDataAte, @cNCCMsg) }, "Processando", "Gerando NCC...")
	
	If !Empty(aRecnoZC7) .AND. Empty(cNCCMsg)//PROCESSADOS PEDIDOS + NF, SEM NCC.
		Aviso("Apura��o de Royalties" , "Foram gerados os pedidos: "+ CRLF + cPedMsg + CRLF + CRLF + "Notas Fiscais: "+ CRLF + cNFMsg + CRLF , {"Ok"} , 2 )
	ElseIf Empty(aRecnoZC7) .AND. !Empty(cNCCMsg)//SOMENTE NCC GERADA
		Aviso("Apura��o de Royalties" , "Foram gerados as NCCs: "+ CRLF + cNCCMsg , {"Ok"} , 2 )
	ElseIf !Empty(aRecnoZC7) .AND. !Empty(cNCCMsg)//PEDIDO + NF + NCC GERADOS.
		Aviso("Apura��o de Royalties" , "Foram gerados os pedidos: "+ CRLF + cPedMsg + CRLF + CRLF + "Notas Fiscais: "+ CRLF + cNFMsg + CRLF + "NCCs: "+ CRLF + cNCCMsg + CRLF, {"Ok"} , 3 )
	Else
		Aviso("Apura��o de Royalties" , "Nenhum registro foi processado.", {"Ok"} , 2 )
	EndIf
	(cAliasQry)->(DbCloseArea())
Return 


//------------------------------------------------------------------->
/*/{Protheus.doc} GeraPV

Gera pedido de venda.

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->


Static Function GeraPV(cCODCLI, cLOJCLI, nVal, cProduto, cTes, cCondPag, cFilDes, cNfzc7)

Local _aArea  	   := GetArea()
Local aCab		   := {}
Local aItens 	   := {}
Local _aSC6 	   := {}
Local _aSC9 	   := {}
Local cNumped	   := ""
Local cFilBkp	   := cFilAnt
Local aPvlNfs	   := {}
Local cPedOrig	   := Posicione("SD2",3,xFilial("SD2")+cNfzc7+cCodCli+cLojCli,"D2_PEDIDO")
Local cCanal	   := Posicione("SC5",1,xFilial("SC5")+cPedOrig,"C5_XCANAL")

PRIVATE lMsErroAuto := .F.

cFilAnt := cFilDes

	aCab := {	{"C5_FILIAL"    ,xFilial("SC5")	     		,Nil},;
	{"C5_TIPO"      , "N"            					,Nil},;
	{"C5_CLIENTE"   , cCODCLI							,Nil},;
	{"C5_LOJACLI"   , cLOJCLI  	  						,Nil},;
	{"C5_CONDPAG"   , cCondPag							,Nil},;
	{"C5_XCANAL" 	, cCanal							,Nil},;
	{"C5_CLIENT" 	, cCODCLI							,Nil},;
	{"C5_LOJAENT"   , cLOJCLI							,Nil} } 

	aAdd(aItens,{  	{"C6_FILIAL"	, xFilial("SC6")				, nil},;
	{"C6_ITEM"   	, "01" 		 							, nil},;
	{"C6_PRODUTO"	, cProduto			   					, nil},; // TEM DE AJUSTAR Produto, Vendedor, TES, Data
	{"C6_TES"    	, cTes			 						, nil},;
	{"C6_QTDVEN" 	, 1	 									, nil},;
	{"C6_PRCVEN" 	, nVal	 								, nil},;
	{"C6_PRCUNIT" 	, 0										, nil},;
	{"C6_QTDLIB" 	, 1										, nil},;
	{"C6_ENTREG" 	, dDataBase								, nil}})

	msExecAuto({|x,y,z|MATA410(x,y,z)},aCab,aItens,3)

	ROLLBACKSX8()

	If lMSErroAuto
		MOSTRAERRO()
	Else
		cNumped := SC5->C5_NUM

		MaLiberOk({ cNumped },.T.)
		

		DbSelectArea("SC9")
		SC9->(DbSetOrder(1))
		If SC9->(DbSeek(xFilial("SC9")+cNumped))
			While SC9->(!EOF()) .And. (C9_FILIAL+C9_PEDIDO = xFilial("SC9")+cNumped)
			          
		          aAdd(aPvlNfs,{ SC9->C9_PEDIDO,;
		          SC9->C9_ITEM,;
		          SC9->C9_SEQUEN,;
		          SC9->C9_QTDLIB,;
		          SC9->C9_PRCVEN,;
		          SC9->C9_PRODUTO,;
		          SF4->F4_ISS=="S",;
		          SC9->(RecNo()),;
		          SC5->(Recno(Posicione("SC5",1,xFilial("SC5")+SC9->C9_PEDIDO,""))),;
		          SC6->(Recno(Posicione("SC6",1,xFilial("SC6")+SC9->C9_PEDIDO+SC9->C9_ITEM,""))),;
		          SE4->(Recno(Posicione("SE4",1,xFilial("SE4")+cCondPag,""))),;
		          SB1->(Recno(Posicione("SB1",1,xFilial("SB1")+SC9->C9_PRODUTO,""))),;
		          SB2->(Recno(Posicione("SB2",1,xFilial("SB2")+SC9->C9_PRODUTO,""))),;
		          SF4->(Recno(Posicione("SF4",1,xFilial("SF4")+cTes,""))),;
		          Posicione("SB2",1,xFilial("SB2")+SC9->C9_PRODUTO,"B2_LOCAL"),;
		          0,;
		          SC9->C9_QTDLIB2})
		          	  
			    SC9->(DbSkip())
		     EndDo
		 EndIf	
	EndIf

cFilAnt := cFilBkp
RestArea(_aArea)
	
Return(aPvlNfs)


//------------------------------------------------------------------->
/*/{Protheus.doc} GeraNFS

Gera a Nota fiscal de sa�da

@author TOTVS Protheus
@since  30/07/2018
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function GeraNFS(cPedido, cSerie, cFilDes, cFilOri, aPvlNfs)

	Local _aArea  := GetArea()
	Local cFilBkp := cFilAnt
	
	cFilAnt := cFilDes

	If Len(aPvlNfs) <> 0   

		cNota := MaPvlNfs(aPvlNfs,cSerie , .F.       , .F.      , .F.       , .F.      , .F.      , 0      , 0           , .F.    , .F.)	                           

	EndIf

	cFilAnt := cFilBkp
	RestArea(_aArea)

Return cNota

//------------------------------------------------------------------->
/*/{Protheus.doc} GeraNCC

Gera NCC

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function GeraNCC(dDatade,dDataAte, cNCCMsg)

Local cAliasNCC := GetNextAlias()
Local cQryNCC	:= ""
Local nValor	:= 0
Local cFilDes  := SuperGetMV("ES_CODEFL") 
Local aSE1	   := {}
Local cFilBkp   := cFilAnt
Local aRecnoZC7 := {}

PRIVATE lMsErroAuto := .F.

cQryNCC	:= "SELECT ZC7_FILIAL, ZC7_STATUS, ZC7_CODCLI, ZC7_LOJCLI, ZC7_DTEMIS, ZC7_CONDPG, ZC7_NOTA, ZC7_SERIE, ZC7_VALDES, R_E_C_N_O_ FROM " + RetSQLName("ZC7") + " ZC7 "
cQryNCC += "WHERE ZC7.D_E_L_E_T_ = ' '
cQryNCC += "AND ZC7_STATUS = ' '
cQryNCC	+= "AND ZC7_VALDES < 0 "
cQryNCC += "AND ZC7_FILIAL = '" + xFilial("ZC7") + "' "
cQryNCC += "AND ZC7_EMISNF BETWEEN '" + DtoS(dDatade)+ "' AND '" + DtoS(dDataAte) + "'
cQryNCC += "ORDER BY  ZC7_CODCLI, ZC7_LOJCLI, ZC7_CONDPG, ZC7_NOTA, ZC7_SERIE "

cQryNCC := ChangeQuery(cQryNCC)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,cQryNCC),cAliasNCC,.F.,.T.)

cFilAnt := cFilDes
DbSelectArea("ZC7")

While (cAliasNCC)->(!Eof()) 
	
	nValor := ((cAliasNCC)->ZC7_VALDES * -1) 
	
	aadd(aSE1,{'E1_PREFIXO'		, (cAliasNCC)->ZC7_SERIE	, NIL })
	aadd(aSE1,{'E1_NUM    '		, (cAliasNCC)->ZC7_NOTA		, NIL }) 
	aadd(aSE1,{'E1_PARCELA'		, "  " 						, NIL })
	aadd(aSE1,{'E1_TIPO   '		, "NCC"   					, NIL })
	aadd(aSE1,{'E1_CLIENTE'		, (cAliasNCC)->ZC7_CODCLI  	, NIL })
	aadd(aSE1,{'E1_LOJA   '		, (cAliasNCC)->ZC7_LOJCLI  	, NIL })
	aadd(aSE1,{'E1_EMISSAO'  	, dDatabase					, NIL })
	aadd(aSE1,{'E1_VENCTO '   	, dDataBase					, NIL })
	aadd(aSE1,{'E1_VENCREA'  	, dDataBase					, NIL })
	aadd(aSE1,{'E1_VALOR  '		, nValor					, NIL })
	
	Begin Transaction
	
		MSExecAuto({|x,y| Fina040(x,y)}, aSE1, 3) // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
	
		If (lMsErroAuto)
			MostraErro()
			DisarmTransaction()
		Else
			cNCCMsg += " [" + SE1->E1_NUM + "] "
			
			//ATUALIZA STATUS DO REGISTRO NA ZC7
			DbGoto((cAliasNCC)->R_E_C_N_O_) 
			ZC7->(RecLock("ZC7", .F.))
				ZC7->ZC7_STATUS		:= "*"
			ZC7->(MsUnLock())
			
		Endif 
	
	End Transaction
	
	nValor := 0
	(cAliasNCC)->(DbSkip())
EndDo

cFilAnt := cFilBkp
ZC7->(DbCloseArea())

Return

/*/{Protheus.doc} VldTES
//Valida se a TES inserida no pergunte 'ADMVP11' movimenta estoque.
@author DS2U [L. FINI]
@since 16/01/2019
@version 1.0
@return lRet

@type function
/*/
User Function VldTES(cCodTES)

Local lRet := .T.
Local aArea	:= getArea()

DbSelectArea("SF4")
SF4->( dbSetOrder(1) )

if ( SF4->( dbSeek( fwxFilial("SF4") + PADR( cCodTES, tamSX3("F4_CODIGO")[1] ) ) ) .and. SF4->F4_ESTOQUE == "S" )
	lRet := .F.
	MsgBox("Selecione uma TES que N�O movimente estoque.")
endif  

restArea( aArea )

Return lRet