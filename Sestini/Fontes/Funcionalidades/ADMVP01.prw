#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVP01
Programa responsavel pelas tratativas de distribuicao de cotas
@author DS2U (SDA)
@since 14/05/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@param [llIsJob], logical, Variavel para controlar se .T. processamento em JOB ou .F. Processamento com interface com usuario. Padrao = isBlind()
@type function
/*/
User Function ADMVP01( nlOpc, uParam, llIsJob )

	local uRet		:= nil

	private lpInJob	:= .F.

	default nlOpc	:= 0
	default uParam	:= nil
	default llIsJob	:= isBlind()

	lpInJob := llIsJob

	do case

		case nlOpc == 0
			uRet := dsCotaByAr( uParam ) // Distribuicao de cotas por armazem 

		case nlOpc == 1 // Mapa de distribuicao por armazem
			uRet := getMapDist( uParam )

		case nlOpc == 2 // Adiciona item no modelo de transfer�ncia multipla e retorno o array pronto para ser adicionado no arrau aAuto do MATA261
			uRet := adTransf( uParam )
			
		case nlOpc == 3 // Filtro de armazens que podem ser utilizados para consulta de produto por vendedores
			uRet := getLocVend( uParam )
			
		case nlOpc == 4
			uRet := dsCotaByNf( uParam )
			
		case nlOpc == 5
			uRet := chkSB2( uParam[1], uParam[2] )
			
	endCase

Return uRet

/*/{Protheus.doc} dsCotaByNf
Responsavel por distribuir os produtos da NF entre armazens do ADMV
@author DS2U (SDA)
@since 29/08/2018
@version 1.0
@param alParam, array of logical, description
@type function
/*/
Static Function dsCotaByNf( alParam )

	local clDoc		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clSerie	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clFornec	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clLoja	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clTipo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )

	local alArea	:= getArea()
	local llRet		:= .F.
	local clLocaliz	:= ""
	local clAlias	:= ""
	local llLocaliz	:= .F.
	local nlReg		:= 0
	local alAreaD1	:= {}

	dbSelectArea("SF1")
	SF1->( dbSetOrder( 1 ) ) // F1_FILIAL + F1_DOC + F1_SERIE + F1_FORNECE + F1_LOJA

	if ( SF1->( dbSeek( FWxFilial( "SF1" ) + clDoc + clSerie + clFornec + clLoja + clTipo ) ) )

		if ( SF1->F1_TIPO == "N" )

			dbSelectArea("SD1")
			SD1->( dbSetOrder( 1 ) ) // D1_FILIAL + D1_DOC + D1_SERIE + D1_FORNECE + D1_LOJA

			dbSelectArea("SB1")
			SB1->( dbSetOrder( 1 ) )

			dbSelectArea("SF4")
			SF4->( dbSetOrder( 1 ) )

			if ( SD1->( dbSeek( FWxFilial("SD1") + SF1->F1_DOC + SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA ) ) )

				llRet		:= .T.

				while	(	.not. SD1->( eof() );
							.and. SD1->D1_FILIAL == SF1->F1_FILIAL;
							.and. SD1->D1_DOC == SF1->F1_DOC;
							.and. SD1->D1_SERIE == SF1->F1_SERIE;
							.and. SD1->D1_FORNECE == SF1->F1_FORNECE;
							.and. SD1->D1_LOJA == SF1->F1_LOJA;
						)

					// Flag que identifica se o item da nota ja foi distribuido
					if ( .not. empty( SD1->D1_TES ) .and. .not. ( SD1->D1_XDISTOK == "S" ) )

						if ( SF4->( dbSeek( FWxFilial( "SF4" ) + SD1->D1_TES ) ) .and. SF4->F4_ESTOQUE == 'S' )

							if ( SB1->( dbSeek( FWxFilial( "SB1" ) + PADR( SD1->D1_COD, tamSX3("B1_COD")[1] ) ) ) )

								// Checa se o produto controla endereco

								//								llLocaliz := U_ADMVXFUN( 8, SB2->B2_COD )
								//
								//								/**
								//								  * Checa se o produto tem rastreamento habilitado
								//								  * para realizar procedimento de enderecamento automatico
								//								  */
								//								if ( llLocaliz )
								//
								//
								//								endif

								clAlias	:= getMapDist( {SD1->D1_PEDIDO, SD1->D1_ITEMPC, SD1->D1_COD} ) // Buscao os armazens do ADMV

								// Realiza transfer�ncia entre armaz�ns / endere�os de tudo que existe no endereco A TRANSFERIR
								begin transaction

									if ( recLock( "SD1", .F. ) )
										SD1->D1_XDISTOK := "S"
										SD1->( msUnLock() )
									endif

									// Cria SB2 nos armazens para o produto em questao, caso ainda nao exista
									( clAlias )->( dbGoTop() )

									while ( .not. ( clAlias )->( eof() ) )

										// Verifica se o almoxafirado j� existe para o produto
										chkSB2( SD1->D1_COD, ( clAlias )->ALMOXARIF )

										( clAlias )->( dbSkip() )
									endDo

									( clAlias )->( dbGoTop() )

									llRet := ( llRet .and. endBetLoc( SD1->D1_LOCAL, SD1->D1_COD, clLocaliz, clAlias ) )

									if ( .not. llRet )
										disarmTransaction()
									endif

								end transaction

							endif

						endif

					endif

					SD1->( dbSkip() )
				endDo

			endif

		endif

	endif

Return llRet

/*/{Protheus.doc} dsCotaByAr
Responsavel por organizar os dados para transferencia entre os armazens a distribuir e os canais do ADMV
@author DS2U (SDA)
@since 29/08/2018
@version 1.0
@return llRet, Se .T., a transfer�ncia ocorreu com sucesso
@param alParam, array of logical, parametro da funcao. alParam[1] = Codigo do produto
@type function
/*/
Static Function dsCotaByAr( alParam )

	local alArea	:= getArea()
	local clLocAdis	:= allTrim( getMv( "ES_LOCDSCT",,"" ) )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local llRet 	:= .F.
	local clAlias	:= getMapDist( { /*Pedido de Compras*/, /*Item do Pedido de compras*/, clProduto} ) // Busca os armazens do ADMV
	
	llRet := endBetLoc( clLocAdis, clProduto,, clAlias )

	restArea( alArea )

Return llRet
 
/*/{Protheus.doc} endBetLoc
Responsavel por realizar transferencias entre armazens conforme o cadastro de cotas
@author DS2u (SDA)
@since 18/05/2018
@version undefined
@return llRet, .T. = Permite continuacao do processamento se nao houve erros no processamento ou .F. = Nao permite continuacao do processamento se ouve erros no processamento
@param clAlmox, characters, Codigo do armazem origem a ser distribuido
@param clEndereco, characters, Codigo do endereco origem a ser distribuido
@param clProduto, characters, Codigo do produto
@param [clAlias], characters, Alias do arquivo de trabalho da View de consulta do mapa de distribuicao entre armazens. Deve ser passado por parametro quando esta em begin transaction
@type function
/*/
Static Function endBetLoc( clAlmox, clProduto, clEndereco, clAlias )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clFilAtu	:= FWxFilial( "SB2" )
	local clAlmoxTrf:= PADR( clAlmox, tamSX3("B2_LOCAL")[1] )
	local nlQtDisp	:= 0

	private	lMsErroAuto := .F.

	default clEndereco	:= ""
	default clAlias		:= ""

	dbSelectArea( "SB2" )
	SB2->( dbSetOrder( 2 ) ) // B2_FILIAL, B2_LOCAL, B2_COD

	if ( SB2->( dbSeek( clFilAtu + clAlmoxTrf + clProduto ) ) )

		while (	.not. ( SB2->( eof() ) );
		.and. SB2->B2_FILIAL == clFilAtu;
		.and. SB2->B2_LOCAL == clAlmoxTrf;
		.and. SB2->B2_COD == clProduto;
		)

			// Saldo disponivel no endereco
			//			nlQtDisp := U_ADMVP02( 2, {clAlmoxTrf, clEndereco, SB2->B2_COD} )

			//Saldo disponivel
			nlQtDisp := saldoSb2()

			if ( nlQtDisp > 0 )

				// Realiza a distribuicao do saldo para os armaz�ns e endere�os
				if ( .not. distSldLoc( SB2->B2_COD, clAlmoxTrf, nlQtDisp, clEndereco, clAlias ) )
					llRet := .F.
					exit
				endif

			endif

			SB2->( dbSkip() )
		endDo

	else
		U_ADMVXFUN( 1, {"B2_LOCAL", "Nao foi encontrado o armazem [" + clAlmoxTrf + "] de transfer�ncia para iniciar a distribui��o de cotas" } )
	endif

Return llRet

/*/{Protheus.doc} distSldLoc
Responsavel pela distribuicao de cotas entre armazens
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return llRet, .T. = Distribuicao entre armazens ok / .F. = Houve falhas na distribuicao
@param clProduto, characters, descricao
@param clAlmoxTrf, characters, descricao
@param nlQtTransf, numeric, descricao
@param clEndOri, characters, descricao
@param [clAlias], characters, Alias do arquivo de trabalho da View de consulta do mapa de distribuicao entre armazens. Deve ser passado por parametro quando esta em begin transaction
@type function
/*/
Static Function distSldLoc( clProduto, clAlmoxTrf, nlQtTransf, clEndOri, clAlias )

	local alArea	:= getArea()
	local clLocDest	:= ""
	local clAlmox	:= ""
	local nlTamEnder:= tamSX3("BE_LOCALIZ")[1]
	local clLocaliz	:= ""
	local clEndAte	:= ""
	local alItem	:= {}
	local alAuto	:= {}
	local nlQtdToCot:= 0
	local llRet		:= .F.

	default clProduto	:= ""
	default clAlmoxTrf	:= ""
	default nlQtTransf	:= 0
	default clAlias		:= ""

	if ( .not. empty( clAlias ) )

		while ( .not. ( clAlias )->( eof() ) )

			// Armazem
			clLocDest := PADR( ( clAlias )->ALMOXARIF, tamSX3("BF_LOCAL")[1] )

			// Endereco % Cota
			//			clLocaliz	:= PADR( allTrim( U_ADMVXFUN( 7, clId )[1] ), nlTamEnder )

			// Calcula as quantidades a serem transferidas
			nlQtdToCot	:= calcQtsPer( nlQtTransf, ( clAlias )->COTA, clProduto )

			if ( nlQtdToCot > 0 )

				// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
				alItem := adTransf( { nlQtdToCot, clProduto, clAlmoxTrf, clLocDest, clEndOri, clEndAte, "DISTR. % COTAS ENTRE ARMAZENS"} )
				AADD( alAuto, alItem )

			endif

			( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )

		// Monta dados e executa a transferencia multipla
		llRet := execTransf( alAuto )

	else
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_CODPAI", "Falha na identificacao dos armazens do ADMV!" } )
	endif

	restArea( alArea )

Return llRet

/*/{Protheus.doc} execTransf
Reponsavel por montar dados e executar transferencia multipla conforme itens enviados por parametro
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return llRet, .T. = transferencia multipla OK / .F. = houve falhas na transferencia multipla 
@param alItens, array of logical, Array no formato de execauto da rotina MATA261 >> Exemplo:

//Origem				
AADD( alItem, SB1->B1_COD )		// D3_COD		
AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
AADD( alItem, SB1->B1_UM )		// D3_UM		
AADD( alItem, clLocalOri )		// D3_LOCAL		
AADD( alItem, clEndOri  )		// D3_LOCALIZ

// Destino
AADD( alItem, SB1->B1_COD )		// D3_COD		
AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
AADD( alItem, SB1->B1_UM )		// D3_UM	
AADD( alItem, clLocalDes )		// D3_LOCAL		
AADD( alItem, clEndDes )		// D3_LOCALIZ		
AADD( alItem, "")				// D3_NUMSERI		
AADD( alItem, "")				// D3_LOTECTL  		
AADD( alItem, "")				// D3_NUMLOTE		
AADD( alItem, cToD("//") )		// D3_DTVALID		
AADD( alItem, 0 )				// D3_POTENCI		
AADD( alItem, nlQtTransf )		// D3_QUANT		
AADD( alItem, 0 )				// D3_QTSEGUM		
AADD( alItem, "" )				// D3_ESTORNO		
AADD( alItem, "" )				// D3_NUMSEQ 		
AADD( alItem, "" )				// D3_LOTECTL		
AADD( alItem, cToD("//") )		// D3_DTVALID		
AADD( alItem, "" )				// D3_ITEMGRD
AADD( alItem, "" )				// D3_IDDCF
AADD( alItem, clObsrv )			// D3_OBSERVA

@type function
/*/
Static Function execTransf( alItens )
				
	local clDoc		:= ""
	local alAuto	:= {}
	local nlOpcAuto	:= 3
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local llRet		:= .F.
	
	private lMsHelpAuto := .T.
	private lMsErroAuto := .F.
	
	if ( len( alItens ) > 0 )
	
		// Cria cabe�alho da transfer�ncia m�ltipla
		clDoc := getSxENum( "SD3", "D3_DOC", 1 )
		AADD( alAuto,{ clDoc, dDataBase } )
		
		// Adiciona itens enviado por parametro
		aEval( alItens, {|x| AADD( alAuto, x ) } )
	
		MSExecAuto({|x,y| MATA261(x,y) }, alAuto, nlOpcAuto )				
	
		if ( lMsErroAuto )
		
			llRet := .F.
			
			clArqErro  := NomeAutoLog()
			if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
				clMsgErro += "Falha no documento [" + clDoc + "] de transfer�ncia m�ltipla." + CRLF
				clMsgErro += memoRead( clArqErro ) + CRLF + replicate("=", 30) + CRLF
				memoWrite(clArqErro," ") // Limpa log automatico
			endif
			
			rollbackSx8()
			
		else
			llRet := .T.
			confirmSx8()
			conout("Incluido doc [" + clDoc + "] de transferencia multipla com sucesso!")			
		endif
		
		if ( .not. empty( clMsgErro ) )
			U_ADMVXFUN( 1, {"BF_LOCALIZ", clMsgErro } )
		endif
	
	endif
	
Return llRet

/*/{Protheus.doc} getMapDist
Responsavel por montar a consulta do mapa de distribuicao de cotas, conforme cadastro de cotas - tabela ZC0
@author DS2U (SDA)
@since 18/05/2018
@version 1.0
@return clAlias, Alias da projecao do banco de dados com a consulta ja ordenada de como sera feito a distribuicao de cotas
@param alParam, array,	alParam[1] = Codigo do pedido de compra
alParam[2] = Item do pedido de compra
alParam[3] = Produto do pedido de compra
@type function
/*/
Static Function getMapDist( alParam )

	local clAlias	:= ""
	local clSql		:= ""
	local clPedCom	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clItemPC	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )

	default alParam	:= {}

	// Primeiro verifica se existe distribuicao por pedido de compras
	if ( .not. empty( clPedCom ) .and. .not. empty( clItemPC ) )

		clSql := "SELECT "
		clSql += "	ZCA_CANAL AS ALMOXARIF "
		clSql += "	, ZCA_COTA AS COTA "

		clSql += "FROM
		clSql += "	" + retSqlName( "ZCA" ) + " ZCA "

		clSql += "WHERE "
		clSql += "	ZCA.ZCA_FILIAL = '" + xFilial("ZCA") + "' "
		clSql += "	AND ZCA.ZCA_PEDCOM = '" + clPedCom + "' "
		clSql += "	AND ZCA.ZCA_ITEMPC = '" + clItemPC + "' "
		clSql += "	AND ZCA.D_E_L_E_T_ = ' ' "

		clAlias	:= getNextAlias()
		dbUseArea( .T., "TOPCONN", tcGenQry(,,clSql), clAlias, .F., .T. )

		if ( ( clAlias )->( eof() ) )
			( clAlias )->( dbCloseArea() )
			clAlias := ""
		endif
		
	// Se nao foi configurado cotas por pedido de compras, entao verifica se existe cotas por produto
	elseif ( empty( clAlias ) .and. .not. empty( clProduto ) )

		clSql := "SELECT "
		clSql += "	ZC8_CANAL AS ALMOXARIF "
		clSql += "	, ZC8_COTA AS COTA "

		clSql += "FROM
		clSql += "	" + retSqlName( "ZC8" ) + " ZC8 "

		clSql += "WHERE "
		clSql += "	ZC8.ZC8_FILIAL = '" + xFilial("ZC8") + "' "
		clSql += "	AND ZC8.ZC8_PROD = '" + clProduto + "' "
		clSql += "	AND ZC8.D_E_L_E_T_ = ' ' "

		clAlias	:= getNextAlias()
		dbUseArea( .T., "TOPCONN", tcGenQry(,,clSql), clAlias, .F., .T. )

		if ( ( clAlias )->( eof() ) )
			( clAlias )->( dbCloseArea() )
			clAlias := ""
		endif

	// Se nao foi identificado cota por pedido de compra e nem por produto, entao considera as cotas por canais
	elseif ( empty( clAlias ) )

		clSql := "SELECT "
		clSql += "	ZC0_LOCAL AS ALMOXARIF "
		clSql += "	, ZC0_PERCOT AS COTA "

		clSql += "FROM
		clSql += "	" + retSqlName( "ZC0" ) + " ZC0 "

		clSql += "WHERE "
		clSql += "	ZC0.ZC0_FILIAL = '" + xFilial("ZC0") + "' "
		clSql += "	AND ZC0.ZC0_LINHA = '1' " // Apenas armazens de primeira linha
		clSql += "	AND ZC0.D_E_L_E_T_ = ' ' "

		clAlias	:= getNextAlias()
		dbUseArea( .T., "TOPCONN", tcGenQry(,,clSql), clAlias, .F., .T. )

		if ( ( clAlias )->( eof() ) )
			( clAlias )->( dbCloseArea() )
			clAlias := ""
		endif

	endif

Return clAlias

/*/{Protheus.doc} calcQtsPer
Responsavel por calcular as quantidades a serem tansferidas conforme percentual de cota e percentual retido
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return nlQtdToCot, Quantidade a ser transferida conforme percentual de cotas
@param nlQtCalc, numeric, Saldo total a ser transferido
@param nlPerCot, numeric, Percentual de distribuicao
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function calcQtsPer( nlQtCalc, nlPerCot, clProduto )

	local nlQtdToCot	:= 0

	default nlQtCalc	:= 0
	default nlPerCot		:= ""

	// Calcula a quantidade a ser transferida conforme o percentual de cotas do cadastro
	nlQtdToCot	:= round( ( nlQtCalc * ( nlPerCot / 100 ) ), 0 ) // Conforme regra definida com Paty, as quantidades devem ser arredonadas conforme regras matematicas

Return nlQtdToCot

/*/{Protheus.doc} adTransf
Funcao auxiliar para adicionar os itens da transferencia multipla
@author DS2U (SDA)
@since 21/05/2018
@version 1.0
@return alItem, array com o item para ser adicionado na transfer�ncia multplia entre enderecos
@param alParam, array,	[1] Quantidade a ser transferida
[2] Codigo do produto
[3] Armazem Origem
[4] Armazem Destino
[5] Endereco Origem
[6] Endereco Destino
[7] Observacao a ser adicionada na movimentacao da SD3
@type function
/*/
Static Function adTransf( alParam )

	local alArea	:= getArea()
	local alItem	:= {}
	local nlQtTransf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "N", alParam[1], 0 )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clLocalOri:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clLocalDes:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clEndOri	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )
	local clEndDes	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 5 .and. valType( alParam[6] ) == "C", alParam[6], "" )
	local clObsrv	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 6 .and. valType( alParam[7] ) == "C", alParam[7], "" )

	if ( nlQtTransf > 0 )

		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )

		if ( SB1->( dbSeek( FwxFilial( "SB1" ) + clProduto ) ) )

			// Valor padrao para armazem origem
			if ( empty( clLocalOri ) )
				clLocalOri := SB1->B1_LOCPAD
			endif

			// Valor padrao para armazem destino
			if ( empty( clLocalDes ) )
				clLocalDes := clLocalOri
			endif

			//Origem
			AADD( alItem, SB1->B1_COD )		// D3_COD
			AADD( alItem, SB1->B1_DESC )	// D3_DESCRI
			AADD( alItem, SB1->B1_UM )		// D3_UM
			AADD( alItem, clLocalOri )		// D3_LOCAL
			AADD( alItem, clEndOri  )		// D3_LOCALIZ

			// Verifica se o almoxafirado j� existe para o produto
			chkSB2(  SB1->B1_COD, clLocalDes )

			// Destino
			AADD( alItem, SB1->B1_COD )		// D3_COD
			AADD( alItem, SB1->B1_DESC )	// D3_DESCRI
			AADD( alItem, SB1->B1_UM )		// D3_UM
			AADD( alItem, clLocalDes )		// D3_LOCAL
			AADD( alItem, clEndDes )		// D3_LOCALIZ
			AADD( alItem, "")				// D3_NUMSERI
			AADD( alItem, "")				// D3_LOTECTL
			AADD( alItem, "")				// D3_NUMLOTE
			AADD( alItem, cToD("//") )		// D3_DTVALID
			AADD( alItem, 0 )				// D3_POTENCI
			AADD( alItem, nlQtTransf )		// D3_QUANT
			AADD( alItem, 0 )				// D3_QTSEGUM
			AADD( alItem, "" )				// D3_ESTORNO
			AADD( alItem, "" )				// D3_NUMSEQ
			AADD( alItem, "" )				// D3_LOTECTL
			AADD( alItem, cToD("//") )		// D3_DTVALID
			AADD( alItem, "" )				// D3_ITEMGRD
			AADD( alItem, clObsrv )			// D3_OBSERVA

		else
			U_ADMVXFUN( 1, {"D3_COD", "Produto [" + clProduto + "] n�o encontrado no sistema" } )
		endif

	else
		U_ADMVXFUN( 1, {"D3_QUANT", "Quantidade a ser transferida [" + allTrim( str( nlQtTransf ) ) + "] deve ser positivo e maior que zero!" } )
	endif

	restArea( alArea )

Return alItem

/*/{Protheus.doc} chkSB2
Funcao auxilizar para checar e criar, caso nao exista, almoxarifado para o produto

@author DS2u (SDA)
@since 18/05/2018
@version undefined
@param clProduto, characters, codigo do produto
@param clAlmox, characters, codigo do almoxarifado
@type function
/*/
Static Function chkSB2( clProduto, clAlmox )

	local alArea	:= getArea()

	if ( .not. empty( clProduto ) .and. .not. empty( clAlmox ) )

		dbSelectArea("SB2")
		SB2->( dbSetOrder( 1 ) )
	
		if ( .not. SB2->( dbSeek( xFilial("SB2") + clProduto + clAlmox ) ) )
			criaSB2(clProduto, clAlmox)
		endif
		
	endif

	restArea( alArea )

Return

/*/{Protheus.doc} jobDisCota
Reponsavel pelo processamento do JOB de distribuicao automatica de cotas
@author DS2U (SDA)
@since 13/08/2018
@version 1.0

@type function
/*/
Static Function jobDisCota()

	local alArea	:= getArea()
	local nlQtdDis	:= 0
	local clLocAdis	:= allTrim( getMv( "ES_LOCDSCT",,"" ) )
	
	if ( .not. empty( clLocAdis ) )
	
		dbSelectArea( "SB2" )
		SB2->( dbSetOrder( 2 ) ) // B2_FILIAL, B2_LOCAL, B2_COD, R_E_C_N_O_, D_E_L_E_T_
	
		clLocAdis := PADR( clLocAdis, tamSX3("B2_LOCAL")[1] )
	
		if ( SB2->( fwxFilial("SB2") + clLocAdis ) )
	
			while (	.not. SB2->( eof() );
			 		.and. SB2->B2_FILIAL == fwxFilial("SB2");
			 		.and. SB2->B2_LOCAL == clLocAdis;
				  )
			
				nlQtdDis	:= saldoSB2()
			
				if ( nlQtdDis > 0 )
			
					dsCotaByAr( { SB2->B2_COD, nlQtdDis } )
					
				endif
				
				SB2->( dbSkip() )
			endDo
			
		endif
		
	endif
		
	restArea( alArea )

Return

/*/{Protheus.doc} prdAdist
Responsavel por consultar produtos a distribuir
@author DS2U (SDA)
@since 13/08/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho com os registros a enderecar

@type function
/*/
Static Function prdAdist()

	local clAlias	:= getNextAlias()
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			DISTINCT
			D1_FILIAL AS FILIAL
			, D1_DOC AS DOC
			, D1_SERIE AS SERIE
			, D1_FORNECE AS FORNECEDOR
			, D1_LOJA AS LOJA
			, D1_TIPO AS TIPO
			 
		FROM 
			%TABLE:SD1% SD1
			
		INNER JOIN
			%TABLE:SF4% SF4 ON
			SF4.F4_FILIAL = %XFILIAL:SF4%
			AND SF4.F4_CODIGO = SD1.D1_TES
			AND SF4.%NOTDEL%
			 
		WHERE 
			SD1.D1_FILIAL = %XFILIAL:SD1%
			AND SD1.D1_XDISTOK <> 'S'			
			AND SD1.%NOTDEL%
			AND F4_ESTOQUE = 'S'
	
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
Return clAlias

Static Function getLocVend( alParam )

	local clVend	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )

Return