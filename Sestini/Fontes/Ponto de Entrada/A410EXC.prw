#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} A410EXC
//Ponto de entrada acionado confirma��o da exclus�o do pedido de venda.
//http://tdn.totvs.com/pages/releaseview.action?pageId=6784034
@author DS2U [L. Fini]
@since 27/01/2019
@version 1.0
@return lRet, se .T. autoriza a exclus�o.

@type function
/*/
User Function A410EXC()

Local lRet := .T.
Local cPedido := SC5->C5_NUM
Local cIsPM    := SC5->C5_XISPM //� pedido m�e? 1= SIM, 2= N�O
Local cIsPF    := SC5->C5_XISPF //� pedido filho? 1= SIM, 2= N�O
Local llExecADMV := GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado
	
If ( llExecADMV )
	lRet := chkExcl(cPedido, cIsPM, cIsPF)
EndIf
	
Return lRet


/*/{Protheus.doc} chkExcl
Checa se � pedido m�e ou filho e faz as devidas valida��es.
@author DS2U [L. Fini]
@since 27/01/2019
@version 1.0
@return lRet, se .T. n�o tem pedidos filhos e pode ser excluido.

@type function
/*/
Static Function chkExcl(cPedido, cIsPM, cIsPF)

Local lRet := .T.
Local cAliasQuery := GetNextAlias()
Local cPedMae	:= ""

If cIsPM == "1" //� pedido m�e
	cQuery := "SELECT C6_XNUMPM FROM "+RetSqlName('SC6')+" SC6 "
	cQuery += "WHERE C6_XNUMPM = '" + cPedido +" ' "
	cQuery += "AND C6_FILIAL = '" + xFilial("SC6") +" ' "
	cQuery += "AND D_E_L_E_T_ = ' ' "
	
	DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQuery,.T.,.T.)
	
	While (cAliasQuery)->(!EOF())
		lRet := .F.
		Exit
	EndDo
	
	If !lRet 
		Aviso("A410EXC" , "Este pedido m�e n�o pode ser exclu�do pois existe pedidos filhos atrelados a ele.", {"Ok"} , 2 )
	EndIf
	
	(cAliasQuery)->(DbCloseArea())
ElseIf cIsPF == "1" //� pedido filho

	DbSelectArea("SC9")
	SC9->(DbSetOrder(1))//C9_FILIAL + C9_PEDIDO
	If ( SC9->( dbSeek( xFilial("SC9") + cPedido ) ) )
		lRet := .F.
	EndIf
	
	If !lRet 
		Aviso("A410EXC" , "Para excluir este Pedido Filho, deve-se primeiro estornar a libera��o em: " + CRLF + "Docto de Sa�da-> Posicione no Pedido -> Outras A��es -> Est. Libera��o", {"Ok"} , 2 )
	Else
		cQuery := "SELECT C6_FILIAL, C6_NUM, C6_PRODUTO, C6_XNUMPM, C6_XITEMPM, C6_XQTORIG FROM "+RetSqlName('SC6')+" SC6 "
		cQuery += "WHERE C6_NUM = '" + cPedido +" ' "
		cQuery += "AND C6_FILIAL = '" + xFilial("SC6") +" ' "
		cQuery += "AND D_E_L_E_T_ = ' ' "
		
		DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasQuery,.T.,.T.)
	
		While (cAliasQuery)->(!EOF())
			cPedMae := (cAliasQuery)->C6_XNUMPM
			DbSelectArea("SC6")
			SC6->(DbSetOrder(1)) //C6_FILIAL+C6_NUM+C6_ITEM+C6_PRODUTO
			If ( SC6->( dbSeek( xFilial("SC6") + (cAliasQuery)->C6_XNUMPM + (cAliasQuery)->C6_XITEMPM + (cAliasQuery)->C6_PRODUTO ) ) )
				Reclock("SC6",.F.)
					SC6->C6_QTDENT := SC6->C6_QTDENT - (cAliasQuery)->C6_XQTORIG
				SC6->(MsUnlock())
			EndIf
			(cAliasQuery)->(DbSkip())
		EndDo
		
		FWMsgRun(, {||  u_EncerPed(cPedMae) }, "Aguarde...", "Verificando reabertura do Pedido M�e...")
		(cAliasQuery)->(DbCloseArea())
	EndIf
	
EndIf

Return lRet