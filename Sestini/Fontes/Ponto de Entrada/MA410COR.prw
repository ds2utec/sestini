#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MA410COR
Ponto de entrada para incluir novas legendas/cores
no browse do pedido de vendas

Link TDN: http://tdn.totvs.com/display/public/PROT/1161230+DSERFAT-1866+DT+Ponto+de+entrada+MA410COR

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MA410COR()

    Local aCores    := aClone( Paramixb )
    local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

    	// Adiciona legendas referente ao ADMV
    	addLegADMV( aCores )
    	
    endif

Return aCores


//-----------------------------------------------
/*/{Protheus.doc} addLegADMV
Tratamento para as novas legendas, de bloqueio
de credito Sestini

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function addLegADMV( aCores )

    Local aAuxCor   := {}
    
    aAdd(aCores, NIL)
    aIns(aCores, 1)
    aCores[1] := { " C5_XSITUAC == 'S' ","BR_MARROM","Suspenso"}

    aAdd(aCores, NIL)
    aIns(aCores, 2)
    aCores[2] := { " C5_XSITUAC == 'C' ","BR_PRETO","Cancelado"}
	
Return aCores