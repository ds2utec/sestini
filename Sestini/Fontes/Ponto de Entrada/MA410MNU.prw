#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MA410MNU
P.E para Inclus�o de botoes no browse da tela
de pedido de vendas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MA410MNU()

	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )
	
		AADD(aRotina, {"Reenvio Bonificado"    , 'U_STNI010RK()' , 0 , 2 , 0 , NIL})
		AADD(aRotina, {"Reenvio Cobrado"       , 'U_STNI020RK()' , 0 , 2 , 0 , NIL})
		AADD(aRotina, {"Alterar Esp"           , "U_410ALTE"     , 0 , 2 , 0 , NIL})
	
		// Adiciona itens referente ao ADMV
		addBtnADMV()
		
	endif

Return

//-----------------------------------------------
/*/{Protheus.doc} addBtnADMV
Inclus�o de botoes para:
Analise de credito
Al�ada de pedido de vendas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function addBtnADMV()

	Local aAlcada	:= {}
	Local aCredito	:= {}
	Local aAlocac	:= {}

    If Type("aRotina") == "A"

        aAdd( aCredito, { "An�lise Cr�dito"          ,	"U_ADMVP04()"			, 0, 6, 0, .F.} )
        aAdd( aCredito, { "Libera��o Blq. Credito"   ,	"U_ADMVP03()"			, 0, 4, 0, .F.} )
        Aadd(aRotina, {"Cr�dito Sestini",aCredito	, 0 , 8 , 3 	, .T.	})
        
        aAdd( aAlcada, { "Pr�-Aprovar Al�ada"       ,	"U_FAlcPREA()"			, 0, 4, 0, .F.} )
        aAdd( aAlcada, { "Aprova��o Al�ada Desc."   ,	"U_FAlcAPRO()"			, 0, 4, 0, .F.} )
        aAdd( aAlcada, { "Vis. Al�ada"   	  		,	"U_ADP8VAlc(.F.,.T.)"	, 0, 2, 0, .F.} )
        aAdd( aAlcada, { "Recalc. Al�ada"   		,	"U_ADP8VAlc(.T.,.T.)"	, 0, 2, 0, .F.} )
        Aadd(aRotina, {"Al�ada Desconto",aAlcada	, 0 , 8 , 3 	, .T.	})
        
        aAdd( aAlocac, { "Transf. Aloca��es"       ,	"U_ADMVP09()"			, 0, 4, 0, .F.} )
        Aadd(aRotina, {"Aloca��es",aAlocac	, 0 , 8 , 3 	, .T.	})
        
        aAdd( aRotina, { "Situa��o ADMV"   	        ,	"U_ADP12View()"			, 0, 6, 0, .F.} )
        
        aAdd( aRotina, { "Pedido M�e"      			,	"U_ADMVP12()", 0, 6, 0, .F.} )
        aAdd( aRotina, { "Log Sestini"     		 	,	"Alert('Fun��o Indispon�vel')", 0, 6, 0, .F.} )
        aAdd( aRotina, { "Cancelar"     		 	,	"U_ADMVP02(9)", 0, 4, 0, .F.} )
        aAdd( aRotina, { "Relatorio Gerencial"     	,	"U_ADMVR01()", 0, 2, 0, .F.} )        
        
    EndIf

Return