#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} IPO400MNU
Ponto de entrada para adicionar novas op��es na Browse de POs

Link: TDN: http://tdn.totvs.com/display/public/mp/IPO400MNU

@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return alRetorno, array com informa��es de novas op��es na browse de PO

@type function
/*/
User Function IPO400MNU()

	local alRetorno := {}

	//------------------------------------------------------------------------------------------
	// Funcao para cadastrar a disponibilidade dos itens do PO para consulta de estoque futuro -
	//------------------------------------------------------------------------------------------
	addAdmv( @alRetorno )
	
Return alRetorno

/*/{Protheus.doc} addAdmv
Responsavel por adicionar novas opcoes referente ao ADMV
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@param alRetorno, array of logical, array que esta sendo utilizado para adicionar novas opcoes na browse de POs
@type function
/*/
Static Function addAdmv( alRetorno )

	default alRetorno	:= {}

	if ( fwIsInCallStack("EICPO400") )

		//------------------------------------------------------------------------------------------
		// Funcao para cadastrar a disponibilidade dos itens do PO para consulta de estoque futuro -
		//------------------------------------------------------------------------------------------
		AADD( alRetorno, {"Disponibilidade ADMV", "U_ADMVA13(1)", 0, 4} )
		
	endif

Return