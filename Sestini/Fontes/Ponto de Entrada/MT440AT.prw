#INCLUDE "PROTHEUS.CH"

//-----------------------------------------------
/*/{Protheus.doc} MT440AT
P.E na rotina de libera��o manual do pedido
de vendas

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MT440AT()

    Local lRet	:= .T.
    local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

	    //-------------------------------------------------------------------------------------
	    // Verifica se o pedido de vendas est� liberado pelas regras ADMV para ser faturado
	    //-------------------------------------------------------------------------------------
	    lRet := U_ADMVXFUN( 14 )
	    
	endif

Return lRet

