#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MA030ROT
P.E para Inclus�o do bot�es no browse do cadastro
de Clientes

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MA030ROT()

    // Adiciona itens referente ao ADMV
	addBtnADMV()

Return

//-----------------------------------------------
/*/{Protheus.doc} addBtnADMV
Inclus�o do bot�o de Analise de Credito no browse
do cadastro de Clientes

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function addBtnADMV()

    If Type("aRotina") == "A"

        aAdd( aRotina, { "Cadastro Cr�dito Cliente/Matriz"	,	"U_ADMVA16()", 0, 6, 0, .F.} )
        aAdd( aRotina, { "An�lise Cr�dito Cliente"			,	"U_ADMVP04()", 0, 6, 0, .F.} )

    EndIf

Return
