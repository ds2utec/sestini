#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'PARMTYPE.CH'

/*/{Protheus.doc} M410STTS
//Este ponto de entrada pertence � rotina de pedidos de venda, MATA410().
Est� em todas as rotinas de altera��o, inclus�o, exclus�o e devolu��o de compras.
Executado ap�s todas as altera��es no arquivo de pedidos terem sido feitas.
@author DS2U (Dema) / TIB
@since 27/08/2018
@version 1.0

@type function
/*/
User Function M410STTS()

	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

    	execAdmv()
    	
    endif

	execComis()

Return

Static Function execComis()
Local cChave	:= ''
Local cPedido 	:= SC5->C5_NUM
Local lContinua	:= .F.
Local cAliasSZ4	:= GetNextAlias()
	
DbSelectarea('SA1')
SA1->(DbSetorder(1))
SA1->(DBseek(xfilial('SA1') + SC5->(C5_CLIENTE + C5_LOJACLI)))

dbselectarea('SC6')
SC6->(dbsetorder(1))
If SC6->(dbseek(xFilial('SC6') + cPedido)) 
	while xFilial('SC6') + cPedido == SC6->(C6_FILIAL + C6_NUM)
	
		If Select(cAliasSZ4) > 0
			(cAliasSZ4)->(DbCloseArea())
		Endif	
		
		BeginSql Alias cAliasSZ4
			
			SELECT	Z4_COMIS
			FROM %Table:SZ4% SZ4
			WHERE
			SZ4.Z4_FILIAL = %xFilial:SZ4% AND
			(SZ4.Z4_REGIAO = %Exp:SA1->A1_XREGIAO% OR SZ4.Z4_REGIAO = ' ') AND
			(SZ4.Z4_SEDE = %Exp:SA1->A1_REGIAO% OR SZ4.Z4_SEDE = ' ') AND
			(SZ4.Z4_GRPVEN = %Exp:SA1->A1_GRPVEN% OR SZ4.Z4_GRPVEN = ' ') AND
			(SZ4.Z4_CLIENTE = %Exp:SA1->A1_COD% OR SZ4.Z4_CLIENTE = ' ') AND
			(SZ4.Z4_LOJA = %Exp:SA1->A1_LOJA% OR SZ4.Z4_LOJA = ' ') AND
			(SZ4.Z4_PRODUTO = %Exp:SC6->C6_PRODUTO% OR SZ4.Z4_PRODUTO = ' ') AND
			%Exp:DTOS(ddatabase)% BETWEEN SZ4.Z4_DTINI AND SZ4.Z4_DTFIM AND
			SZ4.%notdel%
		EndSql
		
		If (cAliasSZ4)->(!EOF())
			Reclock('SC6',.F.)
			SC6->C6_COMIS1 	:= (cAliasSZ4)->Z4_COMIS
			SC6->C6_XCOMIS1 := (cAliasSZ4)->Z4_COMIS
			SC6->C6_COMIS2 	:= (cAliasSZ4)->Z4_COMIS
			SC6->C6_XCOMIS2 := (cAliasSZ4)->Z4_COMIS			
			MsUnlock()
			lContinua := .F.
		Else
			lContinua := .T.
		EndIf
		
		(cAliasSZ4)->(DbCloseArea())
		
		DbSelectArea('SZ4')
		SZ4->(DbSetOrder(3))// Z4_VEND + Z4_REGIAO + Z4_SEDE + Z4_GRPVEN + Z4_CLIENTE + Z4_LOJA + Z4_PRODUTO                                                                                  
		If SZ4->(DbSeek(xFilial('SZ4') + SA1->A1_VEND)) .And. lContinua
			SetComis(SA1->A1_VEND,'1')
		EndIf
		If SZ4->(DbSeek(xFilial('SZ4') + SA1->A1_XVEND2)) .And. lContinua
			SetComis(SA1->A1_XVEND2,'2')
		EndIf		
		
		SC6->(DbSkip())
	Enddo
EndIf

Return 

/*/{Protheus.doc} SetComis
(long_description)
@author mynam
@since 23/08/2018
@version 1.0
@param cVend, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetComis(cVend,cNumVend)
Local cChave := xFilial('SZ4') + cVend
Local cComis := 'C6_COMIS' + cNumVend 
Local cXComis := 'C6_XCOMIS' + cNumVend

If SZ4->(DbSeek(cChave + SC6->C6_PRODUTO))
	cChave +=  SC6->C6_PRODUTO
Else
	cChave +=  Space(Tamsx3('C6_PRODUTO')[1])
EndIf		

If SZ4->(DbSeek(cChave + SA1->(A1_COD+A1_LOJA)))
	cChave += SA1->(A1_COD+A1_LOJA)
Else
	cChave += Space(Tamsx3('A1_COD')[1]) + Space(Tamsx3('A1_LOJA')[1])
EndIf	
		
If SZ4->(DbSeek(cChave + SA1->A1_GRPVEN))
	cChave += SA1->A1_GRPVEN
Else
	cChave += Space(Tamsx3('A1_GRPVEN')[1])
EndIf	

If SZ4->(DbSeek(cChave + SA1->A1_REGIAO))
	cChave += SA1->A1_REGIAO
Else
	cChave += Space(Tamsx3('A1_REGIAO')[1])
EndIf							
								
If SZ4->(DbSeek(cChave + SA1->A1_XREGIAO))
	cChave += SA1->A1_XREGIAO
Else
	cChave += Space(Tamsx3('A1_XREGIAO')[1])
EndIf
		
If SZ4->(DbSeek(cChave))
	Reclock('SC6',.F.)
	SC6->&cComis  := SZ4->Z4_COMIS
	SC6->&cXComis := SZ4->Z4_COMIS
	MsUnlock()			
EndIf	

Return


User Function AX_SZ4()
AXCADASTRO('SZ4','Teste')
Return

Static Function execAdmv()

	Local cPedNum		:= SC5->C5_NUM
	Local cTipoPed		:= SC5->C5_TIPO
	Local nVlrNF		:= 0
	
	// ----------------------------------------------------------------
	// Chama fun��o de Avalia�ao ADMV									|
	// ---------------------------------------------------------------
	If "N" $ cTipoPed // Aplicar Analise ADMV apenas para pedidos normais,
		u_AvPedADMV( /*nTipoBloq - SE NAO INFORMAR, realiza TODOS */ ,  cPedNum  ) 
	EndIf

	If SC5->(FieldPos("C5_XVLRNF")) > 0
		
		nVlrNF	:= Ma410Impos( 4 , .T. , {} )
		
		If SC5->( RecLock( "SC5", .F. ) )
			SC5->C5_XVLRNF	:= nVlrNF
			SC5->( MsUnLock() )
		EndIf
	
	EndIf
	
	if ( !ALTERA .and. !INCLUI )
		
		//----------------------------------------
		// Se for exclusao, zera saldos alocados -
		//----------------------------------------
		zeraSlAloc( cPedNum )
		
	endif
	
return nil

/*/{Protheus.doc} zeraSlAloc
Funcao executada na exclus�o do pedido de venda, para zerar o saldo de aloca��es dos itens do pedido
@author DS2U (SDA)
@since 02/11/2018
@version 1.0
@param clPedido, characters, Codigo do pedido de venda
@type function
/*/
Static Function zeraSlAloc( clPedido )

	local alArea	:= getArea()
	local clAlias	:= U_ADMVP02( 5, { clPedido } )
	
	if ( .not. empty( clAlias ) )
	
		while ( .not. ( clAlias )->( eof() ) )
			
			//--------------------------------------------
			// Inclui em aloca��es a quantidade excluida -
			//--------------------------------------------
			U_ADMVP02( 4, { ( clAlias )->FILIAL, ( clAlias )->PEDVEN, ( clAlias )->ITPEDVEN, "", "", ( ( clAlias )->QTALOC * -1), date(), ( clAlias )->CANAL, ( clAlias )->PRODUTO } )
			
			( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )
	
	endif
	
	restArea( alArea )
	
Return