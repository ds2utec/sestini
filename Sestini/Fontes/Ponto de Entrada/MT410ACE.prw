#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

//--------------------------------------------------------------
/*/{Protheus.doc} MT410ACE
Ponto de Entrada utilizado para verificar se o pedido est� sus-
penso. Pedido suspensos n�o podem ser alterados e nem excluidos
caso necess�rio pode ser realizado a copia do pedido de venda

@author DS2U (HFA)
@since 19/09/2018
@version 1.0
@type function
/*/
//--------------------------------------------------------------
User Function MT410ACE()

	Local nOpc	:= ParamIXB[1]
	Local lRet	:= .T.
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )
	
		//1 - Excluir / 2 - Visualizar / Residuo / 3 - Copiar / 4 - Alterar
		If nOpc == 4 .Or. nOpc == 1 
		
			If SC5->C5_XSITUAC == "S"
	
				lRet := .F. 
				Help(,, "MT410ACE",, 'Pedido de venda suspenso n�o pode ser '+ IIF( nOpc == 4, 'Alterado', 'Exclu�do' ) +'.', 1, 0)
	
			EndIf
		
		EndIf
		
	endif
	
Return lRet