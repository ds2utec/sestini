#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} A410CONS
SERVE P/INCLUIR BOTOES NA ENCHOICEBAR
� chamada no momento de montar a enchoicebar do pedido de vendas, e serve para incluir mais bot�es com rotinas de usu�rio.

Link: TDN: http://tdn.totvs.com/pages/releaseview.action?pageId=6784033

@author DS2U (SDA)
@since 30/07/2018
@version 1.0
@return alNewBt, Array com novos botoes

@type function
/*/
User Function A410CONS()

	local alNewBt := {}
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
	
	if ( llExecADMV )
	
		// Consulta de Promocoes
		addBtProm( @alNewBt )
		
	endif
	
Return alNewBt

/*/{Protheus.doc} addBtProm
Funcao para controlar a inclusao de novo botao na enchoice do pedido de vendas, referente a consulta de promocoes do iten posicionado
@author DS2U (SDA)
@since 30/07/2018
@version 1.0
@return alNewBt, Array com novos botoes
@param alNewBt, array of logical, description
@type function
/*/
Static Function addBtProm( alNewBt )

	local nlPosIdPrm	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XIDPROM" } ) // ID da Promocao vinculado ao item

	default alNewBt := {}
	
	if ( nlPosIdPrm > 0 )
		AADD( alNewBt, {"BMPVISUAL", {|| U_ADMVA05( 2, aCols[N][nlPosIdPrm] ) }, "Promo��es", "Promo��es" } )
	else
		conout( "Campo C6_XIDPROM (ID da Promocao) nao identificado nos itens do pedido de venda!" )
	endif

	AAdd(alNewBt,{ "UPDWARNING", {|| U_ADP8VAlc( .T. , .F. ,,,.F. )	}, "Al�ada Desc."			, "Al�ada Desc." 					})
	
Return alNewBt
