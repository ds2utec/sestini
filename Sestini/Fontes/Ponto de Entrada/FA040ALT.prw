#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} FA040ALT
P.E. Altera��o do Titulo a Receber.

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FA040ALT()

    Local cMotivo   := ""
    local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

	    If !IsBlind()
	
	    	//Tratamento ADMV - Credito
	    	lRet := FChqADMV()
	
	    EndIf
	    
	endif

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FChqADMV
P.E. na altera��o do Titulo a Receber.
Verificar se realizar a altera��o do campo
Alinea de Devolu��o de Cheque

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FChqADMV()

    Local oMotivos  := Nil
    Local lRet      := .T.
    Local lAtuJob	:= .F.

    //---------------------------------
    // Tratamento ADMV
    //---------------------------------
    If Alltrim(M->E1_TIPO) == "CH"

        If !Empty( M->E1_XALINEA ) .And. M->E1_XALINEA <> SE1->E1_XALINEA

            oMotivos    := ADMVMotivosBlq():New()

            cMotivo     := oMotivos:ChequeDev + ": - Titulo a Receber (" + Alltrim(SE1->E1_PREFIXO) +"/"+ Alltrim(SE1->E1_NUM) + ")"
            cMotivo     += " ch devolvido pela Alinea ("+ M->E1_XALINEA+")."

            // Bloqueia o Cliente
            If ( lRet := U_ADP4BlqC( SE1->E1_CLIENTE , SE1->E1_LOJA , cUserName , cMotivo , "B"  ) )
            	
            	lAtuJob := Alltrim( U_ADMVXFUN( 6 , "ZC6_CHONAT" ) ) == "S" 
	            
	            If lAtuJob            

	            	StartJob( "U_ADP4JbCh", GetEnvServer(), .F. , cEmpAnt , cFilAnt , SE1->E1_CLIENTE , SE1->E1_LOJA , cMotivo , .T. )
	            	
	            Else
	            	
	            	// Bloqueia os Pedidos de Venda em Aberto do Cliente
	            	FwMsgRun(,{|| U_ADP4PvAb( SE1->E1_CLIENTE , SE1->E1_LOJA , cMotivo , .T. )  },"Aguarde...","Bloqueando Pedidos de Venda em Aberto...")
	            		            	
	            EndIf

            EndIf

        EndIf

    EndIf

Return lRet