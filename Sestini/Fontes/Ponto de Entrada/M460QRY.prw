#include "protheus.ch"

//-----------------------------------------------
/*/{Protheus.doc} M460QRY
P.E na rotina de Documento de Sa�da. N�o considerar
Pedidos de Venda Suspenso

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function M460QRY()

	Local cQry		:= ParamIxb[1]
	Local nOpc		:= ParamIxb[2]
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

    	cQry := PVSuspADMV( cQry , nOpc )
    	
    endif

Return cQry

//-----------------------------------------------
/*/{Protheus.doc} PVSuspADMV
N�o considerar Pedidos de Venda Suspenso

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function PVSuspADMV( cQry , nOpc )

//	If nOpc == 2
//
//		cQry += " AND SC5.C5_XSITUAC <> 'S' "
//		
//		cQry += " AND SC5.C5_XBLQCRE IN ('A','L') "
//		cQry += " AND SC5.C5_XALCADA IN ('A','L') "
//
//	Else

		cQry += " AND EXISTS ("
		cQry += " 				SELECT DISTINCT "
		cQry += " 								1 "
		cQry += " 				FROM " + RetSqlName("SC5") + " SC5 "
		cQry += " 				WHERE SC5.C5_FILIAL = C9_FILIAL "
		cQry += " 				AND SC5.C5_NUM = C9_PEDIDO "
		cQry += " 				AND SC5.C5_XSITUAC <> 'S' "
		cQry += " 				AND SC5.C5_XBLQCRE IN ('A','L') "
		cQry += " 				AND SC5.C5_XALCADA IN ('A','L') "
		cQry += " 				AND SC5.D_E_L_E_T_ = ' ' "
		cQry += " 			) "

//	EndIf

Return cQry