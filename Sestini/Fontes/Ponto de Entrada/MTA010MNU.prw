/*/{Protheus.doc} MTA010MNU
Ponto de entrada para adicionar novas op��es no cadastro de produtos
@author DS2U (SDA)
@since 25/07/2018
@version 1.0

@type function
/*/
User Function MTA010MNU()

	// Adiciona itens referente ao ADMV
	addBtnADMV()
	
Return

/*/{Protheus.doc} addBtnADMV
Controla novas opcoes do cadastro de produtos referente ao ADMV
@author DS2U (SDA)
@since 25/07/2018
@version 1.0

@type function
/*/
Static Function addBtnADMV()

	//--------------------------------------------
	// Chamada MVC de inclus�o de Cotas por Item -
	//--------------------------------------------
	AADD(aRotina, { "Cotas" ,"VIEWDEF.ADMVA07", 0 , 3, 2, nil} )
	
	//--------------------------------------------
	// Rotina de importa��o de cotas por produto -
	//--------------------------------------------
	AADD(aRotina, { "Importar Cotas" ,"U_ADMVA07(1)", 0 , 3, 2, nil} ) 
	
	//---------------------------------------------------
	// Rotina de consulta de disponibilidade de estoque -
	//---------------------------------------------------
	AADD(aRotina, { "Disp. Estoque" ,"U_ADMVP02(7)", 0 , 3, 2, nil} )
	
Return