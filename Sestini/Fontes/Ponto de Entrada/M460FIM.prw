#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"


/*/{Protheus.doc} M460FIM
Grava��o da NF saida

Link TDN: http://tdn.totvs.com/pages/releaseview.action?pageId=6784180

@since 09/01/2019
@version 1.0

@type function
/*/
User Function M460FIM()

	local lRet := .T.
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

    	lRet := execRoy()
    	
    	//----------------------------------------------
    	// Trata coluna de situa��o do item C6_XSITUAC -
    	//----------------------------------------------
    	U_ADMVP02(8)
    	
    endif

Return lRet

/*/{Protheus.doc} execRoy
Fun�ao que Grava as informa��es do des�gio do SC6 em SD2
@author Andy
@since 09/01/2019
@version 1.0

@type function
/*/
Static Function execRoy()

Local lRet   := .T.
Local aArea  := GetArea()
Local cDoc   := SF2->F2_DOC
Local cSer   := SF2->F2_SERIE
Local cChave := ""

SD2->(DbSetOrder(3))
If SD2->(DbSeek(xFilial("SD2")+cDoc+cSer))
	While !SD2->(Eof()) .AND. SD2->D2_DOC == cDoc .AND. AllTrim(SD2->D2_SERIE) == AllTrim(cSer)
		cChave:= SD2->D2_PEDIDO+SD2->D2_ITEM+SD2->D2_COD
		DbselectArea("SC6")
		dbSetOrder(1)
		If DbSeek(xFilial("SC6")+cChave) 
			SD2->(Reclock("SD2",.F.))
				SD2->D2_XVALDES := SC6->C6_XVALDES	// 'Valor Total do Des�gio do Item'
				SD2->D2_XVLITDS := SC6->C6_XVLITDS	// 'Valor Unit�rio do Des�gio do Item' 
			SD2->(MsUnlock())
		EndIf
		SD2->(DbSkip())
	End
Endif


RestArea(aArea)
Return lRet