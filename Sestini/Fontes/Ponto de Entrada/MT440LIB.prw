#INCLUDE "PROTHEUS.CH"

//-----------------------------------------------
/*/{Protheus.doc} MT440LIB
P.E na rotina de libera��o automatica do pedido
de vendas
 
@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MT440LIB()

	Local nQtdLib 	:= ParamIxb
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )
    	nQtdLib := chkADMV() 
    endif
	
Return nQtdLib 
	
/*/{Protheus.doc} chkADMV
Aplica regras do ADMV
@author DS2U (SDA)
@since 09/01/2019
@version 1.0
@return nQtdLib, Quantidade liberada

@type function
/*/
Static Function chkADMV()

	local nQtdLib	:= 0

	//---------------------------------------------------------------------
	// Garante que sera liberado somente a quantidade alocada para o item -
	//---------------------------------------------------------------------
	nQtdLib := libByAloc( SC6->C6_FILIAL, SC6->C6_NUM, SC6->C6_ITEM, SC6->C6_XSTALOC )

	if ( nQtdLib > 0 )
	
		//---------------------------------
		// Valida se pedido esta suspenso -
		//---------------------------------
		nQtdLib := LbPvADMV( nQtdLib )
	endif

Return nQtdLib

//-----------------------------------------------
/*/{Protheus.doc} LbPvADMV
Pedido de Venda com a Situa��o Suspenso n�o pode
ser liberado.

@author DS2U (HFA)
@since 19/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function LbPvADMV( nQtdLib )

	Local aAreaSC5	:= {}

	dbSelectArea( "SC5" )
	aAreaSC5 := SC5->( GetArea() )

	SC5->( dbSetOrder( 1 ) )
	If SC5->( dbSeek( SC5->( C5_FILIAL + C5_NUM ) ) )

		If Alltrim( SC5->C5_XSITUAC ) == "S" .Or. !( Alltrim(SC5->C5_XALCADA) $ "A|L" ) .Or. !( Alltrim(SC5->C5_XBLQCRE) $ "A|L" )

			nQtdLib := 0

		EndIf

	EndIf
	RestArea( aAreaSC5 )

Return nQtdLib

/*/{Protheus.doc} libByAloc
Responsavel por verificar a quantidade a ser liberada por item, conforme alocacoes
@author DS2U (SDA)
@since 13/10/2018
@version 1.0
@return nlQtAloc, Quantidade alocada para o item
@param clFilial, characters, Codigo da filial
@param clPedido, characters, Codigo do pedido de venda
@param clItem, characters, Codigo do item do pedido de venda
@param clStAloc, characters, Codigo da situa��o de aloca��o do item
@type function
/*/
Static Function libByAloc( clFilial, clPedido, clItem, clStAloc )

	local alArea	:= getArea()
	local nlQtAloc	:= 0
	local clAlias	:= ""
	
	//--------------------------------------------------------------
	// Somente retorna quantidade se o status do item � firme (FM) -
	//--------------------------------------------------------------
	if ( clStAloc == "FM" )
	
		clAlias	:= getNextAlias()
		
		BEGINSQL ALIAS clAlias
	
			SELECT
				SC6.C6_FILIAL AS FILIAL
				, SC6.C6_NUM AS PEDVEN
				, SC6.C6_ITEM AS ITPEDVEN
				,COALESCE( SUM( ZCI.ZCI_QTALOC ), 0 ) AS QTALOC
			
			FROM
				%TABLE:SC6% SC6
			
			INNER JOIN 
				%TABLE:ZCI% ZCI ON
				ZCI.ZCI_FILIAL = SC6.C6_FILIAL
				AND ZCI.ZCI_PEDVEN = SC6.C6_NUM
				AND ZCI.ZCI_ITEMPV = SC6.C6_ITEM
				AND ZCI.%NOTDEL%
			
			WHERE
				SC6.C6_FILIAL = %EXP:clFilial%
				AND SC6.C6_NUM = %EXP:clPedido%
				AND SC6.C6_ITEM = %EXP:clItem%
				AND SC6.%NOTDEL%	
			
			GROUP BY
				SC6.C6_FILIAL
				, SC6.C6_NUM
				, SC6.C6_ITEM
		
		ENDSQL
		
		if ( .not. ( clAlias )->( eof() ) )
			nlQtAloc := ( clAlias )->QTALOC
		endif
		( clAlias )->( dbCloseArea() )
		
	endif
	
	restArea( alArea )

Return nlQtAloc