#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} MSD2520
Esse ponto de entrada est� localizado na fun��o A520Dele(). � chamado antes da exclus�o do registro no SD2.  

Link TDN: http://tdn.totvs.com/display/public/PROT/MSD2520

@author DS2U (SDA)
@since 31/10/2018
@version 1.0

@type function
/*/
User Function MSD2520()

	local alArea	:= getArea()
	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )

		//---------------------------------------------
		// Inclui em aloca��es a quantidade cancelada -
		//---------------------------------------------
		U_ADMVP02( 4, { SD2->D2_FILIAL, SD2->D2_PEDIDO, SD2->D2_ITEMPV, "", "", SD2->D2_QUANT, date(), SD2->D2_LOCAL, SD2->D2_COD } )
		
	endif

	restArea( alArea )

Return