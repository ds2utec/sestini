/*/{Protheus.doc} MA410BOM
Ponto de entrada para inclus�o dos produto da estrutura do produtos no pedido de venda.

@author DS2U (SDA)
@since 08/06/2018
@version 1.0

@type function
/*/
User Function MA410BOM()

	local llExecADMV:= getMv( "ES_ADMVON",, .T. )
    
    if ( llExecADMV )
    	chkKit()
    endif

Return

/*/{Protheus.doc} chkKit
Checagem de Kit do ADMV
@author DS2U (SDA)
@since 09/01/2019
@version 1.0

@type function
/*/
Static Function chkKit()

	local alArea	:= getArea()
	local alKit		:= PARAMIXB[1]
	local alKitZCE	:= {}
	local nlx
	local nly
	local clItem
	local nlPosPrd	:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_PRODUTO"})
	local nlPosTes	:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_TES"})
	local nlPosQtVen:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_QTDVEN"})
	local nlPosPrVen:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_PRCVEN"})
	local nlPosPrLis:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_PRUNIT"})
	local nlPosItem	:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_ITEM"})
	local nlPosTot	:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_VALOR"})
	local nlPosKit	:= aScan(aHeader,{|x| AllTrim(x[2]) == "C6_XCODKIT"})
	local clCodKit	:= aCols[N][nlPosPrd]
	local nlQtdKit	:= iif( aCols[N][nlPosQtVen] > 0, aCols[N][nlPosQtVen], 1 )
	local nlPosDel	:= N
	local llProcOk	:= .F.
	local nlFirst	:= 0
	
	// Verifica se � um kit valido
	aEval( alKit, {|x| AADD( alKitZCE, { clCodKit, x[1] } ) } )
	if ( U_ADMVA11( 3, alKitZCE ) )
	
		dbSelectArea( "SG1" )
		SG1->( dbSetOrder( 1 ) ) // G1_FILIAL, G1_COD, G1_COMP, G1_TRT, R_E_C_N_O_, D_E_L_E_T_
	
		dbSelectArea( "ZCF" )
		ZCF->( dbSetOrder( 2 ) ) // ZCF_FILIAL, ZCF_CODKIT, ZCF_PROD, ZCF_ITEM, R_E_C_N_O_, D_E_L_E_T_
		
		// Inclui Componentes
		for nlx := 1 To Len( alKit )
		
			if ( SG1->( dbSeek( xFilial("SG1") + clCodKit + alKit[nlx][1] ) ) .and. ZCF->( dbSeek( xFilial("ZCF") + clCodKit + alKit[nlx][1] ) ) )
		
				llProcOk := .T.
		
				clItem := aCols[Len(aCols)][nlPosItem]
				
				AADD(aCols,Array(Len(aHeader)+1))
				
				for nly	:= 1 To Len( aHeader )
				
					if ( allTrim( aHeader[nly][2] ) == "C6_ITEM" )
						aCols[Len(aCols)][nly] := soma1(clItem)
					else
						
						if (aHeader[nly,2] <> "C6_REC_WT") .And. (aHeader[nly,2] <> "C6_ALI_WT")				
							aCols[Len(aCols)][nly] := CriaVar(aHeader[nly][2])
						endif
							
					endif
					
				next nly
		
				N := Len( aCols )
				
				if ( nlFirst == 0 )
					nlFirst := N
				endif
				
				aCols[N][Len(aHeader)+1] := .F.
				
				A410Produto( alKit[nlx][1], .F. )
				
				aCols[N][nlPosPrd] := alKit[nlx][1]
				A410MultT("M->C6_PRODUTO",alKit[nlx][1])
				
				if ( existTrigger("C6_PRODUTO"))
					RunTrigger(2,N,Nil,,"C6_PRODUTO")
				endif
		
				A410SegUm(.T.)
				A410MultT("M->C6_QTDVEN",alKit[nlx][2])
				
				If ( existTrigger("C6_QTDVEN ") )
					RunTrigger(2,N,Nil,,"C6_QTDVEN ")
				endif
				
				// Especifico ADMV
				aCols[N][nlPosKit]		:= clCodKit
				aCols[N][nlPosQtVen]	:= ( SG1->G1_QUANT * nlQtdKit )
				aCols[N][nlPosPrVen]	:= ZCF->ZCF_PRCESP
				aCols[N][nlPosPrLis]	:= ZCF->ZCF_PRCESP
				aCols[N][nlPosTot]		:= round( ( SG1->G1_QUANT * nlQtdKit ) * ZCF->ZCF_PRCESP, 2 )
				
				if ( empty(aCols[N][nlPosTot]) .or. empty(aCols[N][nlPosTes] ) )
					aCols[N][Len(aHeader)+1] := .T.
				endif
				
			else
				llProcOk := .F.
				exit
			endif
			
		next nlx
		
		// Deleta pai
		if ( llProcOk .and. len( alKit ) > 0 )
			aDel( aCols, nlPosDel )
			aSize( aCols, len( aCols ) - 1 )
		endif
		
		/*oGetDad:oBrowse:nAt := nlFirst 
		getDRefresh() // Atualiza getDados da janela atual
		oGetDad:oBrowse:Refresh()*/
	
	else
		U_ADMVXFUN( 1,	{"ZCF_CODKIT", "N�o foi cadastrado pre�o para o Kit. Verificar com o respons�vel por cadastro de pre�os do Kit!" } )
	endif

	restArea( alArea )

Return