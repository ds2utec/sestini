#Include "Protheus.Ch"
#Include "TopConn.Ch"

//--------------------------------------------------
/*/{Protheus.doc} ADMVR01
Relatorio Gerencial de Pedido de Vendas

@author Irineu Filho
@since 15/10/2018 - 12:00
@param param, param_type, param_descr
@return Nil
/*/
//--------------------------------------------------
User Function ADMVR01(cAlias,nReg,nOpc)

    Local aArea := GetArea()

    Processa({|| FImprime(SC5->C5_NUM) }, "Imprimindo Relat�rio Gerencial do Pedido" , "Aguarde..." )

    RestArea(aArea)

Return Nil

//--------------------------------------------------
/*/{Protheus.doc} FImprime
Fun��o auxiliar para impress�o do Pedido de Vendas.

@author Irineu Filho
@since 15/10/2018 - 13:14
@param param, param_type, param_descr
@return Nil
/*/
//--------------------------------------------------
Static Function FImprime(cPedidoPar)

Local cDirRel1   := "C:\temp\"
Local cDirRel2   := "C:\temp\PV_Gerencial\"
Local cNomeArq  := cPedidoPar + "_" + DTOS(dDataBase) + "_" + StrTran(Time(),":","")
Local cArquivo  := cDirRel2 + cNomeArq + ".xls"
Local aColunas  := {}
Local nX        := 0
Local nY		:= 0
Local nColunas	:= 23
Local aTexto	:= {}
Local aStyle	:= {}
Local cNumPed	:= ""

Local oCab1		:= Nil
Local oCab2		:= Nil
Local oCab3		:= Nil

Local cUnidade	:= ""
Local dDtInc	:= ""
Local dDtPrev	:= ""
Local cMoeda	:= ""
Local cStatus	:= ""
Local cOrigem	:= ""
Local cPedRep	:= ""
Local cPedCli	:= ""

ProcRegua(-1)

SC5->(dbSetOrder(1))
SC5->(dbSeek( xFilial("SC5") + cPedidoPar ))

SA1->(dbSetOrder(1))
SA1->(dbSeek( xFilial("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI ))

oCab1 := CellStyle():New("oCab1")
oCab1:SetFont("Arial",14,"#000000",.T.)
oCab1:SetHAlign("Center")
oCab1:SetBorder("All","Dot",1,.T.)

oCab2 := CellStyle():New("oCab2")
oCab2:SetHAlign("Center")
oCab2:SetBorder("All","Dot",1,.T.)
oCab2:SetFont("Arial",16,"#000000",.T.)

oCab3 := CellStyle():New("oCab3")
oCab3:SetHAlign("Center")
oCab3:SetBorder("All","Dot",1,.T.)
oCab3:SetFont("Arial",14,"#000000",.T.)

oCab4 := CellStyle():New("oCab4")
oCab4:SetHAlign("Left")
oCab4:SetVAlign("Center")
oCab4:SetBorder("All","Dot",1,.T.)
oCab4:SetFont("Arial",10,"#000000",.T.)

oCab4Num := CellStyle():New("oCab4Num")
oCab4Num:SetHAlign("Right")
oCab4Num:SetVAlign("Center")
oCab4Num:SetBorder("All","Dot",1,.T.)
oCab4Num:SetFont("Arial",10,"#000000",.T.)
oCab4Num:SetNumberFormat("Fixed")

oCab4Center := CellStyle():New("oCab4Center")
oCab4Center:SetHAlign("Center")
oCab4Center:SetVAlign("Center")
oCab4Center:SetBorder("All","Dot",1,.T.)
oCab4Center:SetFont("Arial",10,"#000000",.T.)

oCab4Cinza := CellStyle():New("oCab4Cinza")
oCab4Cinza:SetHAlign("Center")
oCab4Cinza:SetVAlign("Center")
oCab4Cinza:SetBorder("All","Dot",1,.T.)
oCab4Cinza:SetFont("Arial",10,"#000000",.T.)
oCab4Cinza:SetInterior("#E1E1E1","Solid")

oCab5 := CellStyle():New("oCab5")
oCab5:SetHAlign("Left")
oCab5:SetVAlign("Center")
oCab5:SetFont("Arial",10,"#000000",.T.)
oCab5:SetBorder("All","Dot",1,.T.)

oTxt1 := CellStyle():New("oTxt1")
oTxt1:SetHAlign("Left")
oTxt1:SetVAlign("Center")
oTxt1:SetFont("Arial",10,"#000000",.F.)
oTxt1:SetBorder("All","Dot",1,.T.)
oTxt1:SetWrapText(.T.)

oTxt1Num := CellStyle():New("oTxt1Num")
oTxt1Num:SetHAlign("Right")
oTxt1Num:SetVAlign("Center")
oTxt1Num:SetFont("Arial",10,"#000000",.F.)
oTxt1Num:SetBorder("All","Dot",1,.T.)
oTxt1Num:SetNumberFormat("Fixed")

oData1 := CellStyle():New("oData1")
oData1:SetHAlign("Right")
oData1:SetVAlign("Center")
oData1:SetFont("Arial",10,"#000000",.F.)
oData1:SetNumberFormat("Short Date")
oData1:SetBorder("All","Dot",1,.T.)

MakeDir(cDirRel1)
MakeDir(cDirRel2)

oExcel := ExcelXML():New()

oExcel:SetPageSetup(2,.T.,.T.)
oExcel:SetPrintSetup(9)

aColunas := {}
For nX := 1 To nColunas
    Aadd( aColunas , "60" )
Next nX
oExcel:SetColSize(aColunas)

//------------
// Aba Pedido.
//------------
oExcel:SetFolder(1)
oExcel:SetFolderName("Pedido")

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "PEDIDO DE VENDA"
aStyle[1] := oCab1
aTexto[12] := "SESTINI MERCANTIL"
aStyle[12] := oCab2
oExcel:AddRow( "21" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 12 ,   , 5 )


cNumPed := Alltrim(SC5->C5_NUM)
fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "N� " + cNumPed
aStyle[1] := oCab3
oExcel:AddRow( "21" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Unidade"
aStyle[1] := oCab4
aTexto[4] := "Dt. Inclus�o"
aStyle[4] := oCab4
aTexto[5] := "Prev. Fat."
aStyle[5] := oCab4
aTexto[6] := "Moeda"
aStyle[6] := oCab4
aTexto[11] := "Status"
aStyle[11] := oCab4
aTexto[14] := "Origem"
aStyle[14] := oCab4
aTexto[15] := "Ped. Representante"
aStyle[15] := oCab4
aTexto[16] := "Ped. Cliente"
aStyle[16] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 4 )
oExcel:SetMerge(   , 11 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
cUnidade	:= SM0->M0_CODIGO + "/" + SM0->M0_CODFIL + "-" + Alltrim(SM0->M0_FILIAL) + "/" + Alltrim(SM0->M0_NOME)
aTexto[1] := cUnidade
aStyle[1] := oTxt1

dDtInc		:= SC5->C5_EMISSAO
aTexto[4] := dDtInc
aStyle[4] := oData1

dDtPrev		:= STOD("20181016") //--TODO
aTexto[5] := dDtPrev
aStyle[5] := oData1

cVarMoeda 	:= "MV_MOEDA" + Alltrim(Str(SC5->C5_MOEDA))
cMoeda		:= GetMV(cVarMoeda,,"NAO ENCONTRADO")
aTexto[6] := cMoeda
aStyle[6] := oTxt1

cStatus := "-----" //--TODO
aTexto[11] := cStatus
aStyle[11] := oTxt1

cOrigem := "-----" //--TODO
aTexto[14] := cOrigem 
aStyle[14] := oTxt1

cPedRep := "-----" //--TODO
aTexto[15] := cPedRep
aStyle[15] := oTxt1

cPedCli := FRetPedCli(SC5->C5_FILIAL,SC5->C5_NUM)
aTexto[16] := cPedCli
aStyle[16] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 4 )
oExcel:SetMerge(   , 11 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Cond. Pagamento"
aStyle[1] := oCab5
aTexto[4] := "Prazo M�dio"
aStyle[4] := oCab5
aTexto[5] := "CFO"
aStyle[5] := oCab5
aTexto[9] := "Transportadora"
aStyle[9] := oCab5
aTexto[13] := "Redespacho"
aStyle[13] := oCab5
aTexto[15] := "Tab. Pre�o"
aStyle[15] := oCab5
aTexto[16] := "Tipo Frete"
aStyle[16] := oCab5
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 5 ,   , 3 )
oExcel:SetMerge(   , 9 ,   , 3 )
oExcel:SetMerge(   , 13 ,   , 1 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)

cCondPagto := Alltrim(SC5->C5_CONDPAG) + " - " + Alltrim(Posicione("SE4",1,xFilial("SE4") + SC5->C5_CONDPAG ,"E4_DESCRI"))
aTexto[1] := cCondPagto
aStyle[1] := oTxt1

cPrazoMedio := SA1->A1_XPRZCLI
aTexto[4] := cPrazoMedio
aStyle[4] := oTxt1

cDescCFO := FRetCFOP(SC5->C5_FILIAL,SC5->C5_NUM)
aTexto[5] := cDescCFO
aStyle[5] := oTxt1

cTransp := Alltrim(SC5->C5_TRANSP) + " - " + Alltrim(Posicione("SA4",1,xFilial("SA4") + SC5->C5_TRANSP ,"A4_NOME"))
aTexto[9] := cTransp
aStyle[9] := oTxt1

cRedespacho := "-----" //--TODO
aTexto[13] := cRedespacho
aStyle[13] := oTxt1

cTabPreco := Alltrim(SC5->C5_TABELA) + " - " + Alltrim(Posicione("DA0",1,xFilial("DA0") + SC5->C5_TABELA ,"DA0_DESCRI"))
aTexto[15] := cTabPreco
aStyle[15] := oTxt1

cTipoFrete := IIF(SC5->C5_TPFRETE == "C", "CIF" , "FOB")
aTexto[16] := cTipoFrete
aStyle[16] := oTxt1

oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 5 ,   , 3 )
oExcel:SetMerge(   , 9 ,   , 3 )
oExcel:SetMerge(   , 13 ,   , 1 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Cliente"
aStyle[1] := oCab4
aTexto[2] := "Raz�o Social"
aStyle[2] := oCab4
aTexto[5] := "Nome Abreviado"
aStyle[5] := oCab4
aTexto[6] := "CGC"
aStyle[6] := oCab4
aTexto[10] := "Insc. Estadual"
aStyle[10] := oCab4
aTexto[13] := "Contato"
aStyle[13] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 3 )
oExcel:SetMerge(   , 10 ,   , 2 )
oExcel:SetMerge(   , 13 ,   , 2 )

fCriaArray(@aTexto,@aStyle,nColunas)

cCodCli := Alltrim(SC5->C5_CLIENTE) + "-" + Alltrim(SC5->C5_LOJACLI)
aTexto[1] := cCodCli
aStyle[1] := oTxt1

cNomeRazao := Alltrim(SA1->A1_NOME)
aTexto[2] := cNomeRazao
aStyle[2] := oTxt1

cNomeAbrev := Alltrim(SA1->A1_NREDUZ)
aTexto[5] := cNomeAbrev
aStyle[5] := oTxt1

cCGC := Alltrim(Transform(SA1->A1_CGC,PesqPict("SA1","A1_CGC")))
aTexto[6] := cCGC
aStyle[6] := oTxt1

cInscEst := Alltrim(Transform(SA1->A1_INSCR,PesqPict("SA1","A1_INSCR")))
aTexto[10] := cInscEst
aStyle[10] := oTxt1

cContato := Alltrim(SA1->A1_CONTATO)
aTexto[13] := cContato
aStyle[13] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 3 )
oExcel:SetMerge(   , 10 ,   , 2 )
oExcel:SetMerge(   , 13 ,   , 2 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[15] := "Situa��o do Pedido"
aStyle[15] := oCab5
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 15 ,   , 2 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[3] := "Logradouro"
aStyle[3] := oCab5
aTexto[6] := "Cidade/UF"
aStyle[6] := oCab5
aTexto[12] := "CEP"
aStyle[12] := oCab5
aTexto[15] := "Al�ada"
aStyle[15] := oCab5

cStatAlcada := SC5->C5_XALCADA
aTexto[16] := cStatAlcada
aStyle[16] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 3 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 5 )
oExcel:SetMerge(   , 12 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "End. Faturamento"
aStyle[1] := oCab5

cEnd1 := Alltrim(SA1->A1_END)
aTexto[3] := cEnd1
aStyle[3] := oTxt1

cCidade := Alltrim(SA1->A1_MUN) + "/" + Alltrim(SA1->A1_EST)
aTexto[6] := cCidade
aStyle[6] := oTxt1

cCEP := Alltrim(Transform(SA1->A1_CEP,PesqPict("SA1","A1_CEP")))
aTexto[12] := cCEP
aStyle[12] := oTxt1

aTexto[15] := "Estoque"
aStyle[15] := oCab5

cStatEstoque := SC5->C5_XESTOQU
aTexto[16] := cStatEstoque
aStyle[16] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 3 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 5 )
oExcel:SetMerge(   , 12 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "End. Entrega"
aStyle[1] := oCab5

cEnd1 := Alltrim(SA1->A1_ENDENT)
aTexto[3] := cEnd1
aStyle[3] := oTxt1

cCidade :=  Alltrim(Posicione("CC2",1,xFilial("CC2") + SA1->A1_ESTE + SA1->A1_CODMUNE ,"CC2_MUN")) + "/" + Alltrim(SA1->A1_ESTE)
aTexto[6] := cCidade
aStyle[6] := oTxt1

cCEP := Alltrim(Transform(SA1->A1_CEPE,PesqPict("SA1","A1_CEPE")))
aTexto[12] := cCEP
aStyle[12] := oTxt1

aTexto[15] := "Comercial"
aStyle[15] := oCab5

cStatComercial := SC5->C5_XSITUAC
aTexto[16] := cStatComercial
aStyle[16] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 3 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 5 )
oExcel:SetMerge(   , 12 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "End. Cobran�a"
aStyle[1] := oCab5

cEnd1 := Alltrim(SA1->A1_ENDCOB)
aTexto[3] := cEnd1
aStyle[3] := oTxt1

cCidade :=  Alltrim(SA1->A1_MUNC) + "/" + Alltrim(SA1->A1_ESTC)
aTexto[6] := cCidade
aStyle[6] := oTxt1

cCEP := Alltrim(Transform(SA1->A1_CEPC,PesqPict("SA1","A1_CEPC")))
aTexto[12] := cCEP
aStyle[12] := oTxt1

aTexto[15] := "Cr�dito"
aStyle[15] := oCab5

cStatCredito := SC5->C5_XBLQCRE
aTexto[16] := cStatCredito
aStyle[16] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 3 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 5 )
oExcel:SetMerge(   , 12 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Observa��es"
aStyle[1] := oCab4
aTexto[5] := "Configura��es"
aStyle[5] := oCab4
aTexto[13] := "Entrega"
aStyle[13] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 3 )
oExcel:SetMerge(   , 5 ,   , 7 )
oExcel:SetMerge(   , 13 ,   , 4 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := oTxt1
aTexto[5] := ""
aStyle[5] := oTxt1
aTexto[13] := ""
aStyle[13] := oTxt1
oExcel:AddRow( "60" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 3 )
oExcel:SetMerge(   , 5 ,   , 7 )
oExcel:SetMerge(   , 13 ,   , 4 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Produtos"
aStyle[1] := oCab4Center
aTexto[18] := "Promo��o"
aStyle[18] := oCab4Cinza
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 16 )
oExcel:SetMerge(   , 18 ,   , 4 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "C�digo"
aStyle[1] := oCab4
aTexto[2] := "Descri��o"
aStyle[2] := oCab4
aTexto[5] := "Situa��o"
aStyle[5] := oCab4
aTexto[6] := "Prev. Fatur."
aStyle[6] := oCab4
aTexto[8] := "ICMS"
aStyle[8] := oCab4
aTexto[9] := "IPI"
aStyle[9] := oCab4
aTexto[10] := "ST"
aStyle[10] := oCab4
aTexto[11] := "Qtde."
aStyle[11] := oCab4
aTexto[12] := "VL Unit."
aStyle[12] := oCab4
aTexto[14] := "% Desc."
aStyle[14] := oCab4
aTexto[15] := "VL Final"
aStyle[15] := oCab4
aTexto[16] := "Valor NF"
aStyle[16] := oCab4
aTexto[18] := "C�d. Promo��o"
aStyle[18] := oCab4
aTexto[19] := "Descri��o"
aStyle[19] := oCab4
aTexto[20] := "Prazo"
aStyle[20] := oCab4
aTexto[21] := "Desc. Prazo"
aStyle[21] := oCab4
aTexto[22] := "Troca Prazo"
aStyle[22] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 2 )
oExcel:SetMerge(   , 6 ,   , 1 )
oExcel:SetMerge(   , 12 ,   , 1 )
oExcel:SetMerge(   , 16 ,   , 1 )

aInfoValores := fRetValores(SC5->C5_FILIAL,SC5->C5_NUM)

aInfoItens := ACLONE(aInfoValores[1])
aInfoResumo := ACLONE(aInfoValores[2])
aTotais := ACLONE(aInfoValores[3])
aInfoUsuario := ACLONE(aInfoValores[4])

For nX := 1 To Len(aInfoItens)

	fCriaArray(@aTexto,@aStyle,nColunas)
	aTexto[1] := aInfoItens[nX][1]
	aStyle[1] := oTxt1
	aTexto[2] := aInfoItens[nX][2]
	aStyle[2] := oTxt1
	aTexto[5] := aInfoItens[nX][3]
	aStyle[5] := oTxt1
	aTexto[6] := aInfoItens[nX][4]
	aStyle[6] := oData1
	aTexto[8] := aInfoItens[nX][5]
	aStyle[8] := oTxt1Num
	aTexto[9] := aInfoItens[nX][6]
	aStyle[9] := oTxt1Num
	aTexto[10] := aInfoItens[nX][7]
	aStyle[10] := oTxt1Num
	aTexto[11] := aInfoItens[nX][8]
	aStyle[11] := oTxt1Num
	aTexto[12] := aInfoItens[nX][9]
	aStyle[12] := oTxt1Num
	aTexto[14] := aInfoItens[nX][10]
	aStyle[14] := oTxt1Num
	aTexto[15] := aInfoItens[nX][11]
	aStyle[15] := oTxt1Num
	aTexto[16] := aInfoItens[nX][12]
	aStyle[16] := oTxt1Num
	aTexto[18] := aInfoItens[nX][13]
	aStyle[18] := oTxt1
	aTexto[19] := aInfoItens[nX][14]
	aStyle[19] := oTxt1
	aTexto[20] := aInfoItens[nX][15]
	aStyle[20] := oTxt1Num
	aTexto[21] := aInfoItens[nX][16]
	aStyle[21] := oTxt1Num
	aTexto[22] := aInfoItens[nX][17]
	aStyle[22] := oTxt1Num
	oExcel:AddRow( "17" , aTexto , aStyle )
	oExcel:SetMerge(   , 2 ,   , 2 )
	oExcel:SetMerge(   , 6 ,   , 1 )
	oExcel:SetMerge(   , 12 ,   , 1 )
	oExcel:SetMerge(   , 16 ,   , 1 )

Next nX

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Resumo das Programa��es de Entrega"
aStyle[1] := oCab4Center
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 21 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Previs�o Fatur."
aStyle[1] := oCab4
aTexto[3] := "Revenda"
aStyle[3] := oCab4
aTexto[4] := "IPI"
aStyle[4] := oCab4
aTexto[5] := "Total"
aStyle[5] := oCab4
aTexto[6] := "Canceladas"
aStyle[6] := oCab4
aTexto[8] := "Suspensas"
aStyle[8] := oCab4
aTexto[11] := "Faturadas"
aStyle[11] := oCab4
aTexto[14] := "IPI"
aStyle[14] := oCab4
aTexto[15] := "Total"
aStyle[15] := oCab4
aTexto[16] := "Aberto"
aStyle[16] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 6 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 2 )
oExcel:SetMerge(   , 11 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )

For nX := 1 To Len(aInfoResumo)

	fCriaArray(@aTexto,@aStyle,nColunas)
	aTexto[1] := aInfoResumo[nX][1]
	aStyle[1] := oData1
	aTexto[3] := aInfoResumo[nX][2]
	aStyle[3] := oTxt1Num
	aTexto[4] := aInfoResumo[nX][3]
	aStyle[4] := oTxt1Num
	aTexto[5] := aInfoResumo[nX][4]
	aStyle[5] := oTxt1Num
	aTexto[6] := aInfoResumo[nX][5]
	aStyle[6] := oTxt1Num
	aTexto[8] := aInfoResumo[nX][6]
	aStyle[8] := oTxt1Num
	aTexto[11] := aInfoResumo[nX][7]
	aStyle[11] := oTxt1Num
	aTexto[14] := aInfoResumo[nX][8]
	aStyle[14] := oTxt1Num
	aTexto[15] := aInfoResumo[nX][9]
	aStyle[15] := oTxt1Num
	aTexto[16] := aInfoResumo[nX][10]
	aStyle[16] := oTxt1Num
	oExcel:AddRow( "17" , aTexto , aStyle )
	oExcel:SetMerge(   , 1 ,   , 1 )
	oExcel:SetMerge(   , 6 ,   , 1 )
	oExcel:SetMerge(   , 8 ,   , 2 )
	oExcel:SetMerge(   , 11 ,   , 2 )
	oExcel:SetMerge(   , 16 ,   , 1 )

Next nX


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "TOTAIS"
aStyle[1] := oCab4
aTexto[3] := aTotais[2]
aStyle[3] := oCab4Num
aTexto[4] := aTotais[3]
aStyle[4] := oCab4Num
aTexto[5] := aTotais[4]
aStyle[5] := oCab4Num
aTexto[6] := aTotais[5]
aStyle[6] := oCab4Num
aTexto[8] := aTotais[6]
aStyle[8] := oCab4Num
aTexto[11] := aTotais[7]
aStyle[11] := oCab4Num
aTexto[14] := aTotais[8]
aStyle[14] := oCab4Num
aTexto[15] := aTotais[9]
aStyle[15] := oCab4Num
aTexto[16] := aTotais[10]
aStyle[16] := oCab4Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 6 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 2 )
oExcel:SetMerge(   , 11 ,   , 2 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Usu�rio"
aStyle[1] := oCab4
aTexto[3] := "Respons�vel"
aStyle[3] := oCab4
aTexto[4] := "Visto"
aStyle[4] := oCab4
aTexto[5] := "Desc. Total"
aStyle[5] := oCab4
aTexto[6] := "Frete"
aStyle[6] := oCab4
aTexto[7] := "Total Produtos"
aStyle[7] := oCab4
aTexto[10] := "ST"
aStyle[10] := oCab4
aTexto[12] := "IPI"
aStyle[12] := oCab4
aTexto[14] := "Total Pedido"
aStyle[14] := oCab4
aTexto[16] := "% Comiss�o"
aStyle[16] := oCab4
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 7 ,   , 2 )
oExcel:SetMerge(   , 10 ,   , 1 )
oExcel:SetMerge(   , 12 ,   , 1 )
oExcel:SetMerge(   , 14 ,   , 1 )
oExcel:SetMerge(   , 16 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := aInfoUsuario[1]
aStyle[1] := oTxt1
aTexto[3] := aInfoUsuario[2]
aStyle[3] := oTxt1
aTexto[4] := aInfoUsuario[3]
aStyle[4] := oTxt1
aTexto[5] := aInfoUsuario[4]
aStyle[5] := oTxt1Num
aTexto[6] := aInfoUsuario[5]
aStyle[6] := oTxt1Num
aTexto[7] := aInfoUsuario[6]
aStyle[7] := oTxt1Num
aTexto[10] := aInfoUsuario[7]
aStyle[10] := oTxt1Num
aTexto[12] := aInfoUsuario[8]
aStyle[12] := oTxt1Num
aTexto[14] := aInfoUsuario[9]
aStyle[14] := oTxt1Num
aTexto[16] := aInfoUsuario[10]
aStyle[16] := oTxt1Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 1 )
oExcel:SetMerge(   , 7 ,   , 2 )
oExcel:SetMerge(   , 10 ,   , 1 )
oExcel:SetMerge(   , 12 ,   , 1 )
oExcel:SetMerge(   , 14 ,   , 1 )
oExcel:SetMerge(   , 16 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Pedido sujeito a aprova��o mediante valida��o da disponibilidade de estoque, data de entrega e avalia��o de cr�dito. Mais informa��es poder�o ser confirmadas com o"
aStyle[1] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 21 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Representante Comercial / Consultor de Vendas respons�vel por este pedido."
aStyle[1] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 21 )

//------------
// Aba Al�ada.
//------------

aInfoAlcada := FRetAlcada(SC5->C5_FILIAL,SC5->C5_NUM,SC5->C5_CLIENTE,SC5->C5_LOJACLI)

nColunas := 12
oExcel:SetFolder(2)
oExcel:SetFolderName("Al�ada")
oExcel:SetPageSetup(2,.T.,.T.)

aColunas := {}
For nX := 1 To nColunas
    Aadd( aColunas , "60" )
Next nX
oExcel:SetColSize(aColunas)

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Pedido:"
aStyle[1] := oCab4
aTexto[2] := SC5->C5_NUM
aStyle[2] := oTxt1
aTexto[4] := "Data Palm:"
aStyle[4] := oCab4
aTexto[5] := "-----" //--TODO
aStyle[5] := oTxt1
aTexto[7] := "Hora Palm:"
aStyle[7] := oCab4
aTexto[8] := "-----" //--TODO
aStyle[8] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 1 )
oExcel:SetMerge(   , 5 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Cliente:"
aStyle[1] := oCab4
aTexto[2] := Alltrim(SC5->C5_CLIENTE) + "-" + Alltrim(SC5->C5_LOJACLI) + " - " + Alltrim(SA1->A1_NREDUZ) + " - " + Alltrim(SA1->A1_NOME)
aStyle[2] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 10 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Nat. Opera��o:"
aStyle[1] := oCab4
aTexto[2] := cDescCFO
aStyle[2] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 10 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Tabela de Pre�o:"
aStyle[1] := oCab4
aTexto[2] := cTabPreco
aStyle[2] := oTxt1
aTexto[9] := "Tab. Padr�o Cliente:"
aStyle[9] := oCab4
aTexto[10] := IIF(SA1->A1_TABELA <> SC5->C5_TABELA, "Sim", "N�o")
aStyle[10] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 6 )
oExcel:SetMerge(   , 10 ,   , 2 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Desc. Comercial:"
aStyle[1] := oCab4
nDescComercial := 0 //--TODO
aTexto[2] := nDescComercial
aStyle[2] := oTxt1Num

aTexto[4] := "Desc. Cliente:"
aStyle[4] := oCab4
nDescCliente := 0 //--TODO
aTexto[5] := nDescCliente
aStyle[5] := oTxt1Num

aTexto[7] := "Considera RI:"
aStyle[7] := oCab4
cConsideraRI := "" //--TODO
aTexto[8] := cConsideraRI
aStyle[8] := oTxt1Num

aTexto[10] := "Reserva:"
aStyle[10] := oCab4
nReserva := 0 //--TODO
aTexto[11] := nReserva
aStyle[11] := oTxt1Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 1 )
oExcel:SetMerge(   , 5 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Desc. Negociado:"
aStyle[1] := oCab4
nDescNegociado := 0 //--TODO
aTexto[2] := nDescNegociado
aStyle[2] := oTxt1Num

aTexto[4] := "Desc. Financ:"
aStyle[4] := oCab4
nDescFinanceiro := 0 //--TODO
aTexto[5] := nDescFinanceiro
aStyle[5] := oTxt1Num

aTexto[7] := "Desconto RI:"
aStyle[7] := oCab4
nDescontoRI := 0 //--TODO
aTexto[8] := nDescontoRI
aStyle[8] := oTxt1Num

aTexto[10] := "Baixa Prior.:"
aStyle[10] := oCab4
nBaixaPrior := 0 //--TODO
aTexto[11] := nBaixaPrior
aStyle[11] := oTxt1Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 1 )
oExcel:SetMerge(   , 5 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Total Desc.:"
aStyle[1] := oCab4
nTotalDesc := aInfoAlcada[1][2]
aTexto[2] := nTotalDesc
aStyle[2] := oTxt1Num

aTexto[4] := "Total Desc. $:"
aStyle[4] := oCab4
nValTotalDesc := aInfoAlcada[2][2]
aTexto[5] := nValTotalDesc
aStyle[5] := oTxt1Num

aTexto[7] := "RI:"
aStyle[7] := oCab4
nRI := 0 //--TODO
aTexto[8] := nRI
aStyle[8] := oTxt1Num

aTexto[10] := "Int. Compra:"
aStyle[10] := oCab4
nIntCompra := 0 //--TODO
aTexto[11] := nIntCompra
aStyle[11] := oTxt1Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 1 )
oExcel:SetMerge(   , 5 ,   , 1 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Prioridade:"
aStyle[1] := oCab4
cPrioridade := SC5->C5_XPRIORI
aTexto[2] := cPrioridade
aStyle[2] := oTxt1

aTexto[7] := "Aloca Pedido:"
aStyle[7] := oCab4
cAlocaPedido := "" //--TODO
aTexto[8] := cAlocaPedido
aStyle[8] := oTxt1

aTexto[10] := "Reativa��o:"
aStyle[10] := oCab4
nReativa��o := 0 //--TODO
aTexto[11] := nReativa��o
aStyle[11] := oTxt1Num
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 2 ,   , 4 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := ""
aStyle[1] := Nil
oExcel:AddRow( "10" , aTexto , aStyle )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Al�ada"
aStyle[1] := oCab4Center

aTexto[7] := "Tipo Frete:"
aStyle[7] := oCab4
aTexto[8] := aInfoAlcada[3][2]
aStyle[8] := oTxt1

aTexto[10] := "Pedido M�e:"
aStyle[10] := oCab4
cPedMae := "" //--TODO
aTexto[11] := cPedMae
aStyle[11] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 5 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Vari�vel"
aStyle[1] := oCab4Center

aTexto[4] := "Valor"
aStyle[4] := oCab4Center

aTexto[7] := "Cli do Grupo:"
aStyle[7] := oCab4
cCliGrupo := aInfoAlcada[4][2]
aTexto[8] := cCliGrupo
aStyle[8] := oTxt1

aTexto[10] := "Restri��o:"
aStyle[10] := oCab4
cRestricao := "" //--TODO
aTexto[11] := cRestricao
aStyle[11] := oTxt1
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , 2 )
oExcel:SetMerge(   , 4 ,   , 2 )
oExcel:SetMerge(   , 8 ,   , 1 )
oExcel:SetMerge(   , 11 ,   , 1 )

nLinha := 12

For nX := 5 To Len(aInfoAlcada)
	
	fCriaArray(@aTexto,@aStyle,nColunas)
	aTexto[1] := aInfoAlcada[nX][1]
	aStyle[1] := oCab4

	aTexto[4] := aInfoAlcada[nX][2]
	aStyle[4] := oTxt1Num

	If nX == 5
		aTexto[7] := "Situa��o do Pedido"
		aStyle[7] := oCab4Center
	ElseIf nX == 6
		aTexto[7] := "Al�ada:"
		aStyle[7] := oCab4Center
		aTexto[8] := SC5->C5_XALCADA
		aStyle[8] := oTxt1
		nLinhaIni := nLinha
	ElseIf nX == 12
		aTexto[7] := "Estoque:"
		aStyle[7] := oCab4Center
		aTexto[8] := SC5->C5_XESTOQU
		aStyle[8] := oTxt1
		nLinhaIni := nLinha
	ElseIf nX == 18
		aTexto[7] := "Comercial:"
		aStyle[7] := oCab4Center
		aTexto[8] := SC5->C5_XSITUAC
		aStyle[8] := oTxt1
		nLinhaIni := nLinha
	ElseIf nX == 24
		aTexto[7] := "Cr�dito:"
		aStyle[7] := oCab4Center
		aTexto[8] := SC5->C5_XBLQCRE
		aStyle[8] := oTxt1
		nLinhaIni := nLinha
	ElseIf nX == 30
		aTexto[7] := ""
		aStyle[7] := oTxt1
		nLinhaIni := nLinha
	EndIf

	oExcel:AddRow( "17" , aTexto , aStyle )
	oExcel:SetMerge(   , 1 ,   , 2 )
	oExcel:SetMerge(   , 4 ,   , 2 )
	
	If nX == 5
		oExcel:SetMerge(   , 7 ,   , 5 )
	ElseIf nX == 10
		oExcel:SetMerge(  nLinhaIni , 8 , (nLinha-nLinhaIni) , 4 )
	ElseIf nX == 16
		oExcel:SetMerge(  nLinhaIni , 8 , (nLinha-nLinhaIni) , 4 )
	ElseIf nX == 22
		oExcel:SetMerge(  nLinhaIni , 8 , (nLinha-nLinhaIni) , 4 )
	ElseIf nX == 28
		oExcel:SetMerge(  nLinhaIni , 8 , (nLinha-nLinhaIni) , 4 )
	ElseIf nX == 34
		oExcel:SetMerge(  nLinhaIni , 7 , (nLinha-nLinhaIni) , 5 )
	EndIf

	nLinha++

Next nX


//----------------
// Aba Duplicatas.
//----------------

aInfoDuplicatas := FRetDuplicatas(SC5->C5_FILIAL,SC5->C5_NUM)
nColunas := 0

oExcel:SetFolder(3)
oExcel:SetFolderName("Duplicatas")
oExcel:SetPageSetup(2,.T.,.T.)

For nX := 1 To Len(aInfoDuplicatas)
	nTotColunas := 2 + ( Len(aInfoDuplicatas[nX][3]) * 2 )
	If nTotColunas > nColunas
		nColunas := nTotColunas
	EndIf
Next nX

aColunas := {}
For nX := 1 To nColunas
    Aadd( aColunas , "60" )
Next nX
oExcel:SetColSize(aColunas)

fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "DUPLICATAS"
aStyle[1] := oCab4Center
oExcel:AddRow( "17" , aTexto , aStyle )
oExcel:SetMerge(   , 1 ,   , (nColunas-1) )


fCriaArray(@aTexto,@aStyle,nColunas)
aTexto[1] := "Entrega"
aStyle[1] := oCab4Center
aTexto[2] := "Vl.Ent. s/ IPI"
aStyle[2] := oCab4Center

cContador := 0
For nX := 3 To nColunas
	If (nX % 2) == 1
		cContador++
		aTexto[nX] := "Dt. Vencto " + cValToChar(cContador)
		aStyle[nX] := oCab4Center
	Else
		aTexto[nX] := "Valor " + cValToChar(cContador)
		aStyle[nX] := oCab4Center
	EndIf
Next nX
oExcel:AddRow( "17" , aTexto , aStyle )

For nX := 1 To Len(aInfoDuplicatas)

	aInfoAux := ACLONE(aInfoDuplicatas[nX])

	fCriaArray(@aTexto,@aStyle,nColunas)
	aTexto[1] := aInfoAux[1]
	aStyle[1] := oData1
	aTexto[2] := aInfoAux[2]
	aStyle[2] := oTxt1Num

	nColIni := 3
	For nY := 1 To Len(aInfoAux[3])

		aTexto[nColIni] := aInfoAux[3][nY][1]
		aStyle[nColIni] := oData1
		aTexto[nColIni+1] := aInfoAux[3][nY][2]
		aStyle[nColIni+1] := oTxt1Num

		nColIni += 2

	Next nY

	oExcel:AddRow( "17" , aTexto , aStyle )

Next nX





//-------------------------------------
// Cria o arquivo XLS e abre o arquivo.
//-------------------------------------
oExcel:GetXML(cArquivo)

If Aviso( "Relat�rio Gerencial Pedido de Vendas" , "Deseja abrir o arquivo: " + cArquivo  , {"Sim","N�o"} , 3 )
    ShellExecute("open", AllTrim(cArquivo), "", "", 1)
EndIf

Return Nil


//--------------------------------------------------
/*/{Protheus.doc} FCriaArray
Fun��o auxiliar para zerar os arrays de Texto e Estilo.

@author Irineu Filho
@since 15/10/2018 - 13:34
@param param, param_type, param_descr
@return Nil
/*/
//--------------------------------------------------
Static Function FCriaArray(aTxtPar,aStlPar,nColsPar)

Local nX := 1

aTxtPar := {}
aStlPar := {}
For nX := 1 To nColsPar
	Aadd( aTxtPar , Nil )
	Aadd( aStlPar , Nil )
Next nX

Return Nil

//--------------------------------------------------
/*/{Protheus.doc} FRetPedCli
Fun��o para retornar o pedido do cliente de acordo com o pedido.

@author Irineu Filho
@since 16/10/2018 - 10:18
@param cFilPed, Filial do Pedido de Venda
@param cPedPar, Pedido de Venda
@return cRetorno - String com os pedidos.
/*/
//--------------------------------------------------
Static Function FRetPedCli(cFilPed,cPedPar)

Local cQuery := ""
Local cRetorno := ""

cQuery := ""
cQuery += " SELECT DISTINCT C6_PEDCLI "
cQuery += " FROM " + RetSqlName("SC6") + " SC6 (NOLOCK) "
cQuery += " WHERE SC6.C6_FILIAL = '" + cFilPed + "' "
cQuery += " AND SC6.C6_NUM = '" + cPedPar + "' "
cQuery += " AND SC6.C6_PEDCLI <> '' "
cQuery += " AND SC6.D_E_L_E_T_ = ' ' "
cQuery += " ORDER BY SC6.C6_PEDCLI "

If Select("TMPSC6") > 0
	TMPSC6->(dbCloseArea())
EndIf

TcQuery cQuery New Alias "TMPSC6"

While TMPSC6->(!EOF())
	cRetorno += Alltrim(TMPSC6->C6_PEDCLI) + ","
	TMPSC6->(dbSkip())
EndDo

TMPSC6->(dbCloseArea())

If !Empty(cRetorno)
	cRetorno := SubStr(cRetorno,1,Len(cRetorno)-1)
EndIf

Return cRetorno

//--------------------------------------------------
/*/{Protheus.doc} FRetCfop
Retorna o CFOP e a Descri��o de acordo com o pedido.

@author Irineu Filho
@since 16/10/2018 - 12:35
@param cFilPed, Filial do Pedido de Venda
@param cPedPar, Pedido de Venda
@return cRetorno - String com a descri��o do CFOP.
/*/
//--------------------------------------------------
Static Function FRetCfop(cFilPed,cPedPar)

Local cQuery := ""
Local cRetorno := ""

cQuery += " SELECT DISTINCT C6_CF, X5_DESCRI "
cQuery += " FROM " + RetSqlName("SC6") + " SC6 (NOLOCK) "
cQuery += " 
cQuery += " INNER JOIN " + RetSqlName("SX5") + " SX5 (NOLOCK) "
cQuery += " ON X5_FILIAL = '" + xFilial("SX5") + "' "
cQuery += " AND X5_TABELA = '13' "
cQuery += " AND X5_CHAVE = C6_CF "
cQuery += " AND SX5.D_E_L_E_T_ = ' ' "
cQuery += " 
cQuery += " WHERE SC6.C6_FILIAL = '" + cFilPed + "' "
cQuery += " AND SC6.C6_NUM = '" + cPedPar + "' "
cQuery += " AND SC6.C6_CF <> '' "
cQuery += " AND SC6.D_E_L_E_T_ = ' ' "

If Select("TMPSC6") > 0
	TMPSC6->(dbCloseArea())
EndIf

TcQuery cQuery New Alias "TMPSC6"

While TMPSC6->(!EOF())
	cRetorno += Alltrim(TMPSC6->C6_CF) + "-" + Alltrim(TMPSC6->X5_DESCRI) + ","
	TMPSC6->(dbSkip())
EndDo

TMPSC6->(dbCloseArea())

If !Empty(cRetorno)
	cRetorno := SubStr(cRetorno,1,Len(cRetorno)-1)
EndIf

Return cRetorno


//--------------------------------------------------
/*/{Protheus.doc} fRetValores
Retorna as informa��es de Item e Resumo Programa��o

@author Irineu Filho
@since 16/10/2018 - 19:46
@param cFilPed, Filial do Pedido de Venda
@param cPedPar, Pedido de Venda
@return aRetorno - Array contendo os arrays de informa��o.
/*/
//--------------------------------------------------

Static Function fRetValores(cFilPed,cPedPar)

Local aAux		:= {}
Local aItens	:= {}
Local aRetorno	:= {}
Local aResumo	:= {}
Local aTotal	:= {"TOTAL",0,0,0,0,0,0,0,0,0}
Local aUsuario	:= {"","","",0,0,0,0,0,0,0}
Local cSituacao := ""
Local dDtPrev := CTOD("")
Local nValST := 0
Local nValorNF := 0
Local cCodPromo := "-----"
Local cDescPromo := "-----"
Local nPrazo := 0 
Local nDescPromo := 0
Local nTrocaPrazo := 0
Local nValorRevenda := 0
Local nValorIPI := 0
Local nValorCancelada := 0
Local nValorSuspensas := 0
Local nValorFaturada := 0
Local nValorAberto := 0
Local nValorFrete := 0

SC6->(dbSetOrder(1))
SC6->(dbSeek( cFilPed + cPedPar ))

While SC6->(!EOF()) .AND. Alltrim(SC6->C6_FILIAL + SC6->C6_NUM) == Alltrim(cFilPed + cPedPar)

	IncProc("Item: " + SC6->C6_ITEM + " - " + SC6->C6_PRODUTO)

	MaFisIni(SC5->C5_CLIENTE,SC5->C5_LOJACLI,"C",SC5->C5_TIPO,SC5->C5_TIPOCLI,MaFisRelImp("MTR700",{"SC5","SC6"}),,,"SB1","MTR700")
	MaFisAdd(	SC6->C6_PRODUTO,;
	SC6->C6_TES,;
	SC6->C6_QTDVEN,;
	SC6->C6_QTDVEN,;
	SC6->C6_VALDESC,;
	"",;
	"",;
	0,;
	0,;
	0,;
	0,;
	0,;
	(SC6->C6_QTDVEN*SC6->C6_QTDVEN),;
	0,;
	0,;
	0)

	SB1->(dbSetOrder(1))
	SB1->(dbSeek( xFilial("SB1") + SC6->C6_PRODUTO ))

	cSituacao := "-----" //--TODO
	dDtPrev := CTOD("") //--TODO
	nValST := MaFisRet(1,"NF_VALSOL")
	nValorNF := MaFisRet(,"NF_VALMERC")
	cCodPromo := SC6->C6_XIDPROM
	cDescPromo := "-----" //--TODO
	nPrazo := SC6->C6_XPRAZOP
	nDescPromo := SC6->C6_XDSCPRM
	nTrocaPrazo := SC6->C6_XDSCTRC

	nValorRevenda := 0 //--TODO
	nValorIPI := MaFisRet(1,"NF_VALIPI")
	nValorCancelada := 0 //--TODO
	nValorSuspensas := 0 //--TODO
	nValorFaturada := 0 //--TODO
	nValorAberto := 0 //--TODO

	nValorFrete += MaFisRet(1,"NF_FRETE")

	aAux := {}
	Aadd(aAux,SC6->C6_PRODUTO)
	Aadd(aAux,SC6->C6_DESCRI)
	Aadd(aAux,cSituacao)
	Aadd(aAux,dDtPrev)
	Aadd(aAux,SB1->B1_PICM)
	Aadd(aAux,SB1->B1_IPI)
	Aadd(aAux,nValST)
	Aadd(aAux,SC6->C6_QTDVEN)
	Aadd(aAux,SC6->C6_PRCVEN)
	Aadd(aAux,SC6->C6_VALDESC)
	Aadd(aAux,SC6->C6_VALOR)
	Aadd(aAux,nValorNF)
	Aadd(aAux,cCodPromo)
	Aadd(aAux,cDescPromo)
	Aadd(aAux,nPrazo)
	Aadd(aAux,nDescPromo)
	Aadd(aAux,nTrocaPrazo)

	Aadd(aItens,ACLONE(aAux))


	nPosDtPrev := aScan(aResumo,{|x| DTOS(x[1]) == DTOS(dDtPrev) })
	
	If nPosDtPrev == 0
		aAux := {}
		Aadd( aAux , dDtPrev )
		Aadd( aAux , nValorRevenda )
		Aadd( aAux , nValorIPI )
		Aadd( aAux , SC6->C6_VALOR )
		Aadd( aAux , nValorCancelada )
		Aadd( aAux , nValorSuspensas )
		Aadd( aAux , nValorFaturada )
		Aadd( aAux , nValorIPI )
		Aadd( aAux , SC6->C6_VALOR )
		Aadd( aAux , nValorAberto )
		Aadd(aResumo,ACLONE(aAux))
	Else
		aResumo[nPosDtPrev][2] += nValorRevenda
		aResumo[nPosDtPrev][3] += nValorIPI
		aResumo[nPosDtPrev][4] += SC6->C6_VALOR
		aResumo[nPosDtPrev][5] += nValorCancelada
		aResumo[nPosDtPrev][6] += nValorSuspensas
		aResumo[nPosDtPrev][7] += nValorFaturada
		aResumo[nPosDtPrev][8] += nValorIPI
		aResumo[nPosDtPrev][9] += SC6->C6_VALOR
		aResumo[nPosDtPrev][10] += nValorAberto
	EndIf

	aTotal[2] += nValorRevenda
	aTotal[3] += nValorIPI
	aTotal[4] += SC6->C6_VALOR
	aTotal[5] += nValorCancelada
	aTotal[6] += nValorSuspensas
	aTotal[7] += nValorFaturada
	aTotal[8] += nValorIPI
	aTotal[9] += SC6->C6_VALOR
	aTotal[10] += nValorAberto

	aUsuario[4] += SC6->C6_VALDESC
	aUsuario[5] += nValorFrete
	aUsuario[6] += SC6->C6_VALOR
	aUsuario[7] += nValST
	aUsuario[8] += nValorIPI
	aUsuario[9] += SC6->C6_VALOR
	aUsuario[10] := (SC5->C5_COMIS1 + SC5->C5_COMIS2 + SC5->C5_COMIS3 + SC5->C5_COMIS4 + SC5->C5_COMIS5)


	MaFisEnd()

	SC6->(dbSkip())
EndDo

Aadd(aRetorno,aItens)
Aadd(aRetorno,aResumo)
Aadd(aRetorno,aTotal)
Aadd(aRetorno,aUsuario)

Return aRetorno


//--------------------------------------------------
/*/{Protheus.doc} FRetAlcada
Retornas as informa��es de Al�ada.

@author Irineu Filho
@since 17/10/2018 - 11:01
cFilPed - Filial do Pedido
cNumPed - Numero do Pedido
cCliPar - Cliente
cLojaPar - Loja
@return aAlcada
/*/
//--------------------------------------------------

Static Function FRetAlcada(cFilPed,cNumPed,cCliPar,cLojaPar)

Local aAlcada := {}

ZCH->(dbSetOrder(1)) //ZCH_FILIAL, ZCH_PEDIDO, ZCH_CLIENT, ZCH_LOJA, R_E_C_N_O_, D_E_L_E_T_
If ZCH->(dbSeek( cFilPed + cNumPed + cCliPar + cLojaPar ))

	Aadd(aAlcada , { "Total Desc %" , ZCH->ZCH_PDESTT})
	Aadd(aAlcada , { "Total Desc $" , ZCH->ZCH_VDESTT})
	Aadd(aAlcada , { "tipo Frete" , ZCH->ZCH_TPFRET})
	Aadd(aAlcada , { "Cli do Grupo" , ZCH->ZCH_CLIGRP})
	Aadd(aAlcada , { "Total Categoria Depend." , 0 }) //--TODO
	Aadd(aAlcada , { "Total Venda Depend." , 0 }) //--TODO
	Aadd(aAlcada , { "Total Categoria" , 0 }) //--TODO
	Aadd(aAlcada , { "Total Venda" , 0 }) //--TODO
	Aadd(aAlcada , { "Al�ada Ultilizada" , ZCH->ZCH_PALCUT })
	Aadd(aAlcada , { "Al�ada Permitida" , ZCH->ZCH_PALCPR })
	Aadd(aAlcada , { "Cr�dito de Al�ada" , ZCH->ZCH_PALCDI })
	Aadd(aAlcada , { "Cr�dito em $" , ZCH->ZCH_VLRALC })
	Aadd(aAlcada , { "Margem Tabela" , ZCH->ZCH_PMARGE })
	Aadd(aAlcada , { "Cr�dito Comiss�o" , ZCH->ZCH_PCOMIS })
	Aadd(aAlcada , { "Cr�dito de Frete" , ZCH->ZCH_PFRETE })
	Aadd(aAlcada , { "Cr�dito Financeiro" , ZCH->ZCH_PFINPR })
	Aadd(aAlcada , { "Cr�dito Pagto Antecipado" , ZCH->ZCH_PPAGAN })
	Aadd(aAlcada , { "Desc. Cliente Permitido" , ZCH->ZCH_PCLIPR })
	Aadd(aAlcada , { "Desc. Comercial Pol�tica" , ZCH->ZCH_PCMRPP })
	Aadd(aAlcada , { "Cr�dito Desc. Promocional" , ZCH->ZCH_PPROMP })
	Aadd(aAlcada , { "% Disp. p/ Troca Prazo/Desc." , ZCH->ZCH_PDISTP })
	Aadd(aAlcada , { "Limite p/ Troca Prazo/Desc." , ZCH->ZCH_PLIMTP })
	Aadd(aAlcada , { "Prazo Pol�tica" , ZCH->ZCH_DPOLIP })
	Aadd(aAlcada , { "Prazo Adicional da Pol�tica" , ZCH->ZCH_DADIPO })
	Aadd(aAlcada , { "Prazo Adicional Comercial" , ZCH->ZCH_DADICO })
	Aadd(aAlcada , { "Prazo Promocional Ponderado" , ZCH->ZCH_DPROPP })
	Aadd(aAlcada , { "Prazo Promocional Utilizado" , ZCH->ZCH_DPROPU })
	Aadd(aAlcada , { "Saldo Prazo Promocional %" , ZCH->ZCH_PSLDPN })
	Aadd(aAlcada , { "Saldo Prazo Promocional Dias" , ZCH->ZCH_DSALDO })
	Aadd(aAlcada , { "Prazo do Cliente" , ZCH->ZCH_DPRZCL })
	Aadd(aAlcada , { "Prazo Total" , 0 }) //--TODO
	Aadd(aAlcada , { "Prazo Pedido" , ZCH->ZCH_DPROPU })
	Aadd(aAlcada , { "Prazo L�quido" , ZCH->ZCH_DPRZLQ })
	Aadd(aAlcada , { "Sobra de Prazo" , 0 }) //--TODO
	Aadd(aAlcada , { "Dias Financeiro" , ZCH->ZCH_DFINAN })

Else

	Aadd(aAlcada , { "Total Desc %" , 0})
	Aadd(aAlcada , { "Total Desc $" , 0})
	Aadd(aAlcada , { "tipo Frete" , 0})
	Aadd(aAlcada , { "Cli do Grupo" , 0})
	Aadd(aAlcada , { "Total Categoria Depend." , 0 })
	Aadd(aAlcada , { "Total Venda Depend." , 0 })
	Aadd(aAlcada , { "Total Categoria" , 0 })
	Aadd(aAlcada , { "Total Venda" , 0 })
	Aadd(aAlcada , { "Al�ada Ultilizada" , 0 })
	Aadd(aAlcada , { "Al�ada Permitida" , 0 })
	Aadd(aAlcada , { "Cr�dito de Al�ada" , 0 })
	Aadd(aAlcada , { "Cr�dito em $" , 0 })
	Aadd(aAlcada , { "Margem Tabela" , 0 })
	Aadd(aAlcada , { "Cr�dito Comiss�o" , 0 })
	Aadd(aAlcada , { "Cr�dito de Frete" , 0 })
	Aadd(aAlcada , { "Cr�dito Financeiro" , 0 })
	Aadd(aAlcada , { "Cr�dito Pagto Antecipado" , 0 })
	Aadd(aAlcada , { "Desc. Cliente Permitido" , 0 })
	Aadd(aAlcada , { "Desc. Comercial Pol�tica" , 0 })
	Aadd(aAlcada , { "Cr�dito Desc. Promocional" , 0 })
	Aadd(aAlcada , { "% Disp. p/ Troca Prazo/Desc." , 0 })
	Aadd(aAlcada , { "Limite p/ Troca Prazo/Desc." , 0 })
	Aadd(aAlcada , { "Prazo Pol�tica" , 0 })
	Aadd(aAlcada , { "Prazo Adicional da Pol�tica" , 0 })
	Aadd(aAlcada , { "Prazo Adicional Comercial" , 0 })
	Aadd(aAlcada , { "Prazo Promocional Ponderado" , 0 })
	Aadd(aAlcada , { "Prazo Promocional Utilizado" , 0 })
	Aadd(aAlcada , { "Saldo Prazo Promocional %" , 0 })
	Aadd(aAlcada , { "Saldo Prazo Promocional Dias" , 0 })
	Aadd(aAlcada , { "Prazo do Cliente" , 0 })
	Aadd(aAlcada , { "Prazo Total" , 0 })
	Aadd(aAlcada , { "Prazo Pedido" , 0 })
	Aadd(aAlcada , { "Prazo L�quido" , 0 })
	Aadd(aAlcada , { "Sobra de Prazo" , 0 })
	Aadd(aAlcada , { "Dias Financeiro" , 0 })

EndIf

Return aAlcada

//--------------------------------------------------
/*/{Protheus.doc} FRetDuplicatas
Retorna array com as duplicatas.

@author Irineu Filho
@since 17/10/2018 - 18:08
@return aRetorno
/*/
//--------------------------------------------------
Static Function FRetDuplicatas(aInfoCond) //--TODO

Local aRetorno := {}


Aadd(aRetorno, { STOD("20181013") , 0 , { {CTOD(""),0} , {CTOD(""),0} , {CTOD(""),0} } })
Aadd(aRetorno, { STOD("20181012") , 0 , { {CTOD(""),0} , {CTOD(""),0} } })
Aadd(aRetorno, { STOD("20181011") , 0 , { {CTOD(""),0} } })
Aadd(aRetorno, { STOD("20181014") , 0 , { {CTOD(""),0} , {CTOD(""),0} , {CTOD(""),0} , {CTOD(""),0} } })


/*
Exemplo do array aInfoCond
//Data de Faturamento, Valor, Condi��o, Valor IPI, Valor ST
Aadd(aInfoCond , { STOD("20181013") , nValor , cCond , nValIPI , , nValST } )

Exemplo para pegar as duplicatas.

For nX := 1 To Len(aInfoCond)


aDupl := Condicao(1000,SC5->C5_CONDPAG,0,dDataBase,0,0,,,0)
Condicao(nlValor,M->C5_CONDPAG,MaFisRet(,"NF_VALIPI"),dDataCnd,Iif(SF4->F4_INCSOL<>"N",MaFisRet(,"NF_VALSOL"),0),,,nAcresTot)

VarInfo("aDupl - " , aDupl)

Next nX
*/

Return aRetorno