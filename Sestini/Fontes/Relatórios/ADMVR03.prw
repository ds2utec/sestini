#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVR03
Relatorio de Aloca��es x Produto
@author DS2U (SDA)
@since 08/01/2019
@version 1.0

@type function
/*/
User Function ADMVR03()

	local alArea	:= getArea()
	local olReport

	private cpPerg	:= "ADMVR03"
	private cpAlias	:= getNextAlias()

	if ( pergunte(cpPerg, .T.) )
	
		reportDef( @olReport )
		olReport:PrintDialog()
		
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} reportDef
Monta estrutura de impressao dos dados
@author DS2U (SDA)
@since 08/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@type function
/*/
Static Function reportDef( olReport )

	local olSection1
	local olSection2
	local clReport	:= "ADMVR03"
	local clTitulo	:= "Relat�rio de Aloca��es Por Produto"
	local clDescri	:= "Este programa ir� emitir um relat�rio de Aloca��es Por Produto."
	local blReport	:= { |olReport| reportPrint( @olReport, cpPerg ) }
	
	olReport := TReport():New( clReport, clTitulo, /*cpPerg*/, blReport, clDescri )
	
	if ( MV_PAR10 == 1 )
	
		olSection1 := TRSection():New( olReport, "Produto", {cpAlias}, /*aOrd*/ )
	
		TRCell():New( olSection1, "CODPROD"		, cpAlias, "C�digo"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection1, "DESCPROD"	, cpAlias, "Produto"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	
		olSection2 := TRSection():New( olSection1, "Aloca��es x Clientes", {cpAlias}, /*aOrd*/ )
		
		TRCell():New( olSection2, "CLIENTE"		, cpAlias, "Cliente"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "PEDIDO"		, cpAlias, "Pedido"				, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "DATAIMP"		, cpAlias, "Dt. Implantanta��o"	, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->DATAIMP ) } )
		TRCell():New( olSection2, "HORAIMP"		, cpAlias, "Hr. Implantanta��o"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "ENTORI"		, cpAlias, "Entrega Original"	, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->ENTORI ) } )
		TRCell():New( olSection2, "DTALOC"		, cpAlias, "Aloca��o"			, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->DTALOC ) } )
		TRCell():New( olSection2, "TPENTR"		, cpAlias, "Tipo Entrega"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "SITUACAO"	, cpAlias, "Situa��o"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "PRIORIDADE"	, cpAlias, "Prioridade"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "QUANTIDADE"	, cpAlias, "Quantidade"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "DIAS"		, cpAlias, "Dias"				, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "VENDEDOR"	, cpAlias, "Vendedor"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		
		TRBreak():New(olSection1, olSection1:Cell("CODPROD"),,.F.)
		
	else
	
		olSection1 := TRSection():New( olReport, "Vendedor", {cpAlias}, /*aOrd*/ )
	
		TRCell():New( olSection1, "VENDEDOR"		, cpAlias, "Vendedor"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		
		olSection2 := TRSection():New( olSection1, "Produto", {cpAlias}, /*aOrd*/ )
	
		TRCell():New( olSection2, "CODPROD"		, cpAlias, "C�digo"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection2, "DESCPROD"	, cpAlias, "Produto"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	
		olSection3 := TRSection():New( olSection2, "Aloca��es x Clientes", {cpAlias}, /*aOrd*/ )
		
		TRCell():New( olSection3, "CLIENTE"		, cpAlias, "Cliente"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "PEDIDO"		, cpAlias, "Pedido"				, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "DATAIMP"		, cpAlias, "Dt. Implantanta��o"	, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->DATAIMP ) } )
		TRCell():New( olSection3, "HORAIMP"		, cpAlias, "Hr. Implantanta��o"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "ENTORI"		, cpAlias, "Entrega Original"	, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->ENTORI ) } )
		TRCell():New( olSection3, "DTALOC"		, cpAlias, "Aloca��o"			, "@D", 8,/*lPixel*/, {|| sToD( ( cpAlias )->DTALOC ) } )
		TRCell():New( olSection3, "TPENTR"		, cpAlias, "Tipo Entrega"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "SITUACAO"	, cpAlias, "Situa��o"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "PRIORIDADE"	, cpAlias, "Prioridade"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "QUANTIDADE"	, cpAlias, "Quantidade"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "DIAS"		, cpAlias, "Dias"				, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		TRCell():New( olSection3, "VENDEDOR"	, cpAlias, "Vendedor"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
		
	endif
	
Return

/*/{Protheus.doc} reportPrint
Funcao para tratamento dos dados e impressao do relatorio
@author DS2U (SDA)
@since 08/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@param cpPerg, characters, Alias do pergunte do relat�rio
@type function
/*/
Static Function reportPrint( olReport, cpPerg )

	local olSection1	:= olReport:section( 1 )
	local olSection2	:= olReport:section( 1 ):section( 1 )
	local olSection3	:= olReport:section( 1 ):section( 1 ):section( 1 )
	local nlQtdReg		:= 0
	local clProduto		:= ""
	local clVend		:= ""
	local clProdDe		:= ""
	local clProdAte		:= ""
	local clDtDe
	local clDtAte
	local clOrderBy		:= "% B1_COD, C6_ENTREG %"

	pergunte(cpPerg,.F.)
	
	clDtDe	:= dToS( MV_PAR07 )
	clDtAte	:= dToS( MV_PAR08 )
	
	if ( empty( MV_PAR09 ) )
		clProdDe	:= space( tamSX3( "B1_COD" )[1] )
		clProdAte	:= replicate( "Z", tamSX3( "B1_COD" )[1] )
	else
		clProdDe	:= MV_PAR09
		clProdAte	:= MV_PAR09
	endif
	
	if ( MV_PAR10 == 2 )
		clOrderBy := "% A3_NOME, B1_COD, C6_ENTREG %"
	endif
	
	BEGINSQL ALIAS cpAlias
	
		SELECT
			B1_COD AS CODPROD
			, B1_DESC AS DESCPROD
			, A1_NOME AS CLIENTE
			, C5_NUM AS PEDIDO
			, C6_XDTEMIS AS DATAIMP
			, C6_XTIMEST AS HORAIMP
			, C6_XDTENES AS ENTORI
			, C6_ENTREG AS DTALOC
			, CASE WHEN ( C6_XSTALOC = '  ' OR C6_XSTALOC = 'NP' ) THEN 'Nao Processado'
			  ELSE 
				CASE WHEN ( C6_XSTALOC = 'EC' ) THEN 'Estima Cancelamento'
				ELSE
					CASE WHEN ( C6_XSTALOC = 'FM' ) THEN 'Firme'
					ELSE
						CASE WHEN ( C6_XSTALOC = 'ET' ) THEN 'Estimado'
					ELSE
						'Nao identificado'
					END
					END
				END
			  END  AS TPENTR
			, C6_XSITUAC AS SITUACAO
			, C5_XPRIORI AS PRIORIDADE
			, C6_QTDVEN AS QUANTIDADE
			, DATEDIFF(DAY, CASE WHEN C6_XDTEMIS = ' ' THEN C6_ENTREG ELSE C6_XDTEMIS END, C6_ENTREG) AS DIAS
			, A3_NOME AS VENDEDOR
		
		FROM
			%TABLE:SC6% SC6
		
		INNER JOIN
			%TABLE:SC5% SC5 ON
			SC5.C5_FILIAL = SC6.C6_FILIAL
			AND SC5.C5_NUM = SC6.C6_NUM
			AND SC5.%NOTDEL%
		
		INNER JOIN
			%TABLE:SB1% SB1 ON
			SB1.B1_FILIAL = %XFILIAL:SB1%
			AND SB1.B1_COD = SC6.C6_PRODUTO
			AND SB1.%NOTDEL%
		
		INNER JOIN
			%TABLE:SA1% SA1 ON
			SA1.A1_FILIAL = %XFILIAL:SA1%
			AND SA1.A1_COD = SC5.C5_CLIENTE
			AND SA1.A1_LOJA = SC5.C5_LOJACLI
			AND SA1.%NOTDEL%
		
		INNER JOIN
			%TABLE:SA3% SA3 ON
			SA3.A3_FILIAL = %XFILIAL:SA3%
			AND SA3.A3_COD = SC5.C5_VEND1
			AND SA3.%NOTDEL%
		
		WHERE
			SC6.C6_FILIAL = %XFILIAL:SC6%
			AND SA1.A1_COD BETWEEN %EXP:MV_PAR01% AND %EXP:MV_PAR02%
			AND SC5.C5_NUM BETWEEN %EXP:MV_PAR03% AND %EXP:MV_PAR04%
			AND SA3.A3_COD BETWEEN %EXP:MV_PAR05% AND %EXP:MV_PAR06%
			AND SC6.C6_XDTEMIS BETWEEN %EXP:clDtDe% AND %EXP:clDtAte%
			AND SC6.C6_PRODUTO BETWEEN %EXP:clProdDe% AND %EXP:clProdAte%    
			AND SC6.%NOTDEL%
		
		ORDER BY %EXP:clOrderBy%

	ENDSQL
	
	( cpAlias )->( dbGoTop() )
	( cpAlias )->( dbEval( {|| nlQtdReg++ } ) )
		
	olReport:setMeter( nlQtdReg )
	
	( cpAlias )->( dbGoTop() )
	while ( .not. ( cpAlias )->( eof() ) )
			
		if ( olReport:cancel() )
			alert("Relat�rio foi Cancelado!")
			exit
		endif

		if ( MV_PAR10 == 1 )

			olSection1:init()
			olSection1:PrintLine()
			olSection1:finish()
			
			clProduto := ( cpAlias )->CODPROD
			
			olSection2:init()
			while ( .not. ( cpAlias )->( eof() ) .and. clProduto == ( cpAlias )->CODPROD )
			
				olSection2:PrintLine()
				olReport:incMeter()
				
				( cpAlias )->( dbSkip() )
			endDo
			olSection2:finish()
			
			olReport:skipLine()
			
		else
			
			olSection1:init()
			olSection1:PrintLine()
			
			
			clVend		:= ( cpAlias )->VENDEDOR
			
			while ( .not. ( cpAlias )->( eof() ) .and. clVend == ( cpAlias )->VENDEDOR )
			
				olSection2:init()
				olSection2:PrintLine()
				
				clProduto	:= ( cpAlias )->CODPROD
				
				olSection3:init()
				while ( .not. ( cpAlias )->( eof() ) .and. clVend == ( cpAlias )->VENDEDOR .and. clProduto == ( cpAlias )->CODPROD )
				
					olSection3:PrintLine()
					olReport:incMeter()
				
					( cpAlias )->( dbSkip() )
				endDo
				olSection3:finish()
				olSection2:finish()
				
			endDo
			
			olReport:skipLine()
			olSection1:finish()
			
		endif
		
	endDo
	( cpAlias )->( dbCloseArea() )
	
Return