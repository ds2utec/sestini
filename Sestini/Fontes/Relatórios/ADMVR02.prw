#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVR02
Relatorio de historico de alteracoes de data de disponibilidade
@author DS2U (SDA)
@since 02/01/2019
@version 1.0

@type function
/*/
User Function ADMVR02()

	local alArea	:= getArea()
	local olReport

	reportDef( @olReport )
	olReport:PrintDialog()
	
	restArea( alArea )

Return

/*/{Protheus.doc} reportDef
Monta estrutura de impressao dos dados
@author DS2U (SDA)
@since 02/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@type function
/*/
Static Function reportDef( olReport )

	local olSection1
	local clReport	:= "ADMVR02"
	local clPerg	:= "ADMVR02"
	local clTitulo	:= "Relat�rio de Hist�rico de altera��es"
	local clDescri	:= "Este programa ir� emitir um relat�rio de hist�rico de altera��es."
	local blReport	:= { |olReport| reportPrint( @olReport, clPerg ) }
	
	olReport := TReport():New( clReport, clTitulo, clPerg, blReport, clDescri )
	
	olSection1 := TRSection():New( olReport, "Hist�rico", {"ZCK"}, /*aOrd*/ )
	
	TRCell():New( olSection1, "ZCK_FILIAL"	, "ZCK", "Filial"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_PO"		, "ZCK", "P.O."				, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_DTMOV"	, "ZCK", "Data Movimento"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_HRMOV"	, "ZCK", "Hora Movimento"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_DTORPO"	, "ZCK", "Data P.O."		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_DIAETA"	, "ZCK", "Dias ETA"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_DTCLPO"	, "ZCK", "Disponibilidade"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ZCK_USER"	, "ZCK", "Usu�rio"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	
	// Quebra por data de movimento
	TRBreak():New(olSection1, olSection1:Cell("ZCK_DTMOV"),,.F.)
	
Return

/*/{Protheus.doc} reportPrint
Funcao para tratamento dos dados e impressao do relatorio
@author sergi
@since 02/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@param clPerg, characters, Alias do pergunte do relat�rio
@type function
/*/
Static Function reportPrint( olReport, clPerg )

	local clDtDe     	:= ""
	local clDtAte     	:= ""
	local olSection1	:= olReport:section( 1 )
	local clAlias		:= getNextAlias()
	
	pergunte(clPerg,.F.)
	
	clDtDe	:= dToS( MV_PAR01 )
	clDtAte	:= dToS( MV_PAR02 )
	
	if ( select( clAlias ) )
		( clAlias )->( dbCloseArea() )
	endif
	
	olSection1:beginQuery()
	
	BeginSql Alias clAlias
		
		SELECT 
			ZCK.*
			 
		FROM 
			%TABLE:ZCK% ZCK
			
		WHERE 
			ZCK.ZCK_FILIAL = %XFILIAL:ZCK%
			AND ZCK.ZCK_DTMOV BETWEEN %EXP:clDtDe% AND %EXP:clDtAte%
			AND ZCK.%NOTDEL%
		
		ORDER BY ZCK_DTMOV

	EndSql
	
	olSection1:endQuery()
	olSection1:print()
	
	if ( select( clAlias ) )
		( clAlias )->( dbCloseArea() )
	endif
	
Return