#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVR04
Relatorio de resumo de pedidos
@author DS2U (SDA)
@since 29/01/2019
@version 1.0

@type function
/*/
User Function ADMVR04()

	local alArea	:= getArea()
	local olReport
	
	private cpAlias	:= getNextAlias()

	reportDef( @olReport )
	olReport:PrintDialog()
	
	restArea( alArea )

Return

/*/{Protheus.doc} reportDef
Monta estrutura de impressao dos dados
@author DS2U (SDA)
@since 29/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@type function
/*/
Static Function reportDef( olReport )

	local olSection1
	local clReport	:= "ADMVR04"
	local clPerg	:= "ADMVR04"
	local clTitulo	:= "Relat�rio de Resumo de Pedidos"
	local clDescri	:= "Este programa ir� emitir um relat�rio de resumo de pedidos."
	local blReport	:= { |olReport| reportPrint( @olReport, clPerg ) }
	
	olReport := TReport():New( clReport, clTitulo, clPerg, blReport, clDescri )
	
	olSection1 := TRSection():New( olReport, "Resumo de Pedido", {cpAlias}, /*aOrd*/ )
	
	TRCell():New( olSection1, "FORNECEDOR"	, cpAlias, "Fornecedor"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "GRP_ESTOQUE"	, cpAlias, "Grupo Estoque"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "COD_FAMILI"	, cpAlias, "Cod. Familia"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "DES_FAMILI"	, cpAlias, "Desc. Familia"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ITEM"		, cpAlias, "Item"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "DESC_ITEM"	, cpAlias, "Desc. Item"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "PEDIDO"		, cpAlias, "Pedido"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ENTREGA"		, cpAlias, "Entrega"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "QUANT"		, cpAlias, "Quant"			, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "PRECO_FOB"	, cpAlias, "Preco FOB"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "TOTAL_FOB"	, cpAlias, "Total FOB"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "PRECO_VEND"	, cpAlias, "Preco Venda"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "SITUACAO"	, cpAlias, "Situacao"		, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "DT_PEDIDO"	, cpAlias, "Data Pedido"	, /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	TRCell():New( olSection1, "ALIQ_II"		, cpAlias, "Aliquota II (%)", /*Picture*/, /*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
	
Return

/*/{Protheus.doc} reportPrint
Funcao para tratamento dos dados e impressao do relatorio
@author sergi
@since 29/01/2019
@version 1.0
@param olReport, object, Objeto TReport
@param clPerg, characters, Alias do pergunte do relat�rio
@type function
/*/
Static Function reportPrint( olReport, clPerg )

	local clDtEntDe    	:= ""
	local clDtEntAte   	:= ""
	local clDtPCDe    	:= ""
	local clDtPCAte   	:= ""
	local olSection1	:= olReport:section( 1 )
	
	pergunte(clPerg,.F.)
	
	clDtEntDe	:= dToS( MV_PAR01 )
	clDtEntAte	:= dToS( MV_PAR02 )
	clDtPCDe	:= dToS( MV_PAR03 )
	clDtPCAte	:= dToS( MV_PAR04 )
	
	if ( select( cpAlias ) )
		( cpAlias )->( dbCloseArea() )
	endif
	
	olSection1:beginQuery()
	
	BEGINSQL ALIAS cpAlias
	
		SELECT * FROM (
			
			SELECT 
				COALESCE( A2_NOME, '') AS FORNECEDOR
				, COALESCE(ZB_CODIGO, '') AS CODGRP_EST
				, COALESCE(ZB_DESCR, '') AS GRP_ESTOQUE
				, COALESCE(ZD_CODIGO, '') AS COD_FAMILI
				, COALESCE(ZD_DESCR, '') AS DES_FAMILI
				, COALESCE( B1_COD, '') AS ITEM
				, COALESCE( B1_DESC, '') AS DESC_ITEM
				, COALESCE( C7_NUM, '') AS PEDIDO
				, COALESCE( ZCK_DTCLPO, '') AS ENTREGA
				, COALESCE( C7_QUANT, 0 ) AS QUANT
				, COALESCE( W3_PRECO, 0 ) AS PRECO_FOB
				, COALESCE( ( W3_PRECO * W3_QTDE ), 0 ) AS TOTAL_FOB
				, COALESCE( DA1_PRCVEN, 0 ) AS PRECO_VEND
				, CASE WHEN D1_DOC IS NULL THEN CASE WHEN ( ZCK_DTCLPO IS NULL OR ZCK_DTCLPO = ' ' ) THEN 'SEM DATA' ELSE 'CONFIRMADA' END ELSE 'RECEBIDA' END AS SITUACAO
				, COALESCE( C7_EMISSAO, '') AS DT_PEDIDO
				, COALESCE( YD_PER_II, 0 ) AS ALIQ_II
					
			FROM 
				%TABLE:SW2% SW2 
							
			LEFT JOIN 
				%TABLE:SW3% SW3 ON 
				SW3.W3_FILIAL = SW2.W2_FILIAL 
				AND SW3.W3_PO_NUM = SW2.W2_PO_NUM 
				AND SW3.W3_TEC = ' ' 
				AND SW3.%NOTDEL% 
							
			LEFT JOIN 
				%TABLE:SW0% SW0 ON 
				SW0.W0_FILIAL = SW3.W3_FILIAL 
				AND SW0.W0__NUM = SW3.W3_SI_NUM 
				AND SW0.%NOTDEL%
			
			LEFT JOIN 
				(
			
					SELECT 
						ZCK1.ZCK_FILIAL
						, ZCK1.ZCK_PO
						, ZCK1.ZCK_DTMOV
						, ZCK1.ZCK_HRMOV
						, MAX( ZCK1.ZCK_DTCLPO ) AS ZCK_DTCLPO			
			
					FROM
						%TABLE:ZCK% ZCK1
			
					INNER JOIN
						(
						SELECT 
							ZCK2.ZCK_FILIAL
							, ZCK2.ZCK_PO			
							, MAX( ZCK2.ZCK_DTMOV ) AS ZCK_DTMOV
							, MAX( ZCK2.ZCK_HRMOV ) AS ZCK_HRMOV
			
						FROM
							%TABLE:ZCK% ZCK2
			
						WHERE
							ZCK2.%NOTDEL% 
			
						GROUP BY ZCK2.ZCK_FILIAL, ZCK2.ZCK_PO
						
						) ZCK3 ON 
						ZCK3.ZCK_FILIAL = ZCK1.ZCK_FILIAL
						AND ZCK3.ZCK_PO = ZCK1.ZCK_PO
						AND ZCK3.ZCK_DTMOV = ZCK1.ZCK_DTMOV
						AND ZCK3.ZCK_HRMOV = ZCK1.ZCK_HRMOV
			
					WHERE
						ZCK1.%NOTDEL% 
			
					GROUP BY ZCK1.ZCK_FILIAL, ZCK1.ZCK_PO, ZCK1.ZCK_DTMOV, ZCK1.ZCK_HRMOV
			
				) ZCK ON
				ZCK.ZCK_FILIAL = SW2.W2_FILIAL
				AND ZCK.ZCK_PO = SW2.W2_PO_NUM
				
			
			LEFT JOIN	
				%TABLE:SC7% SC7 ON
				SC7.C7_FILIAL = SW2.W2_FILIAL
				AND SC7.C7_PO_EIC = SW2.W2_PO_NUM
				AND SC7.C7_ITEM = SW3.W3_POSICAO
				AND SC7.C7_FORNECE = SW3.W3_FABR
				AND SC7.C7_LOJA = SW3.W3_FABLOJ
				AND SC7.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SD1% SD1 ON
				D1_FILIAL = C7_FILIAL
				AND D1_PEDIDO = C7_NUM
				AND D1_ITEMPC = C7_ITEM
				AND SD1.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SA2% SA2 ON
				A2_FILIAL = %XFILIAL:SA2%
				AND A2_COD = C7_FORNECE
				AND A2_LOJA = C7_LOJA
				AND SA2.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SB1% SB1 ON
				B1_FILIAL = %XFILIAL:SB1%
				AND B1_COD = C7_PRODUTO	
				AND SA2.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:DA1% DA1 ON
				DA1_FILIAL = %XFILIAL:DA1%
				AND DA1_CODPRO = B1_COD
				AND DA1_CODTAB = ( SELECT ZC6_TABBAS FROM %TABLE:ZC6% ZC6 WHERE ZC6.%NOTDEL% )
			
			LEFT JOIN	
				%TABLE:SYD% SYD ON
				YD_FILIAL = %XFILIAL:SYD%
				AND YD_TEC = B1_POSIPI
				AND SYD.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SZP% SZP ON
				ZP_FILIAL = %XFILIAL:SZP%
				AND ZP_CODPRO = B1_COD
				AND ZP_STATUS <> '0'
				AND SZP.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SZB% SZB ON
				ZB_FILIAL = ZP_FILIAL
				AND ZB_CODIGO = ZP_GRUPO
				AND SZB.%NOTDEL%
			
			LEFT JOIN	
				%TABLE:SZD% SZD ON
				ZD_FILIAL = ZP_FILIAL
				AND ZD_CODIGO = ZP_COLEC
				AND SZD.%NOTDEL%
			
			WHERE  			
				W2_FILIAL = %XFILIAL:SW2%
				AND SW2.%NOTDEL% 
				AND SW2.W2_PO_NUM = 'ALOC01'
				
			) AS TMP
		
		WHERE
			TMP.ENTREGA BETWEEN %EXP:clDtEntDe% AND %EXP:clDtEntAte%
			AND TMP.DT_PEDIDO BETWEEN %EXP:clDtPCDe% AND %EXP:clDtPCAte%
			AND TMP.CODGRP_EST BETWEEN %EXP:MV_PAR05% AND %EXP:MV_PAR06%
			AND TMP.COD_FAMILI BETWEEN %EXP:MV_PAR07% AND %EXP:MV_PAR08%
			AND TMP.ITEM BETWEEN %EXP:MV_PAR09% AND %EXP:MV_PAR10%

	ENDSQL
	
	olSection1:endQuery()
	olSection1:print()
	
	if ( select( cpAlias ) )
		( cpAlias )->( dbCloseArea() )
	endif
	
Return