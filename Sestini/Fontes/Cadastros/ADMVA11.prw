#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA11
Cadastro de Kits
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@return uRet, Retorno da funcao executada
@param nlOpc, numeric, Numero da funcao a ser executada
@param uParam, undefined, Parametro enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA11( nlOpc, uParam )

	default nlOpc	:= 0
	default uParam	:= nil
	
	do Case
	
		case nlOpc == 0
			uRet := browse( uParam )
			
		case nlOpc == 1
			uRet := importCsv( uParam )
			
		case nlOpc == 2
			uRet := vldPrcEsp( uParam )
			
		case nlOpc == 3
			uRet := isKitOk( uParam )
			
	endCase
	
Return uRet

/*/{Protheus.doc} browse
Configura��o da Browse de cadastro
@author DS2U (SDA)
@since 12/08/2018
@version 1.0

@type function
/*/
Static Function browse()
	
	local alArea	:= ZCE->( getArea() )

	private oBrowse

	setKey( VK_F2 , {|| FwMsgRun(,{|oSay| loadItens( oSay ) } ) } )
	
	dbSelectArea("ZCE")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZCE")
	oBrowse:SetDescription("Cadastro de Kits")
	oBrowse:Activate()	
	
	setKey( VK_F2 , {||  })
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA11"	OPERATION MODEL_OPERATION_VIEW ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA11"	OPERATION MODEL_OPERATION_INSERT ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA11"	OPERATION MODEL_OPERATION_UPDATE ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Importar"				ACTION "U_ADMVA11(1)"		OPERATION MODEL_OPERATION_INSERT ACCESS 0 // "Importar Kits"

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZCE	:= FWFormStruct( 2, 'ZCE' )
	local olStruZCF	:= FWFormStruct( 2, 'ZCF' )
	local olModel	:= FWLoadModel( 'ADMVA11' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZCE', olStruZCE, 'ZCEMASTER' )
	olView:AddGrid( 'VIEW_ZCF', olStruZCF, 'ZCFDETAIL')
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 20 )
	olView:CreateHorizontalBox( 'INFERIOR', 80 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZCE', 'SUPERIOR' )
	olView:SetOwnerView('VIEW_ZCF','INFERIOR')
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
	//Adiciona noas a��es
	olView:AddUserButton('Carregar Itens (F2)', 'CLIPS', {|oView| FwMsgRun(,{|oSay| loadItens( oSay, olView ) },"Aguarde...","Carregando itens..." ) } )
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZCE	:= FWFormStruct( 1, 'ZCE', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZCF	:= FWFormStruct( 1, 'ZCF', /*bAvalCampo*/, /*lViewUsado*/ )
	local alRelZCF	:= {}
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA11M', /* bPreValidacao*/,/*bPosValidacao*/,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZCEMASTER', /*cOwner*/, olStruZCE, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	olModel:AddGrid( 'ZCFDETAIL', 'ZCEMASTER', olStruZCF,/*bLinePre*/, /*bLinePost*/,/*bPre - Grid Inteiro*/,/*bPos - Grid Inteiro*/,/*bLoad - Carga do modelo manualmente*/)
	
	 // Fazendo o relacionamento entre o Pai e Filho
    AADD( alRelZCF, {'ZCF_FILIAL', 'xFilial( "ZCF" )'} )
    AADD( alRelZCF, {'ZCF_CODKIT', 'ZCE_CODKIT'} )
	
	olModel:SetRelation('ZCFDETAIL', alRelZCF, ZCF->( indexKey( 1 ) ) )
    olModel:GetModel('ZCFDETAIL'):SetUniqueLine({"ZCF_FILIAL","ZCF_PROD"} )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZCE_FILIAL", "ZCE_CODKIT"} )
	
	olModel:SetDescription('Kits')
	olModel:GetModel('ZCEMASTER' ):SetDescription('Kits')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

/*/{Protheus.doc} loadItens
Responsavel por carregar os itens de kits
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@param oSay, object, objeto fwMsgRun, util para enviar mensagem ao usuario atraves da interface de aguarde
@param olView, object, objeto da view MVC
@type function
/*/
Static Function loadItens( oSay, olView )

	local olModelZCE:= nil
	local olModelZCF:= nil
	local alArea	:= getArea()
	local clTab100	:= PADR( getTab100(), tamSX3("DA1_CODTAB")[1] )
	local llProcOk	:= .T.
	local llClear	:= .F.
	local nlLin		:= 1
	local clAlias
	
	default oSay	:= nil
	default olView	:= fwViewActive()
	
	olModelZCE := olView:getModel("ZCEMASTER")
	olModelZCF := olView:getModel("ZCFDETAIL")
	
	if ( empty( olModelZCE:getValue( "ZCE_CODKIT" ) ) )
		llProcOk := .F.
		U_ADMVXFUN( 1,	{"ZCF_CODKIT", "C�digo do KIT deve ser preenchido!" } )
	else
	
		if ( olModelZCF:length() > 0 .and. .not. empty( olModelZCF:getValue( "ZCF_PROD" ) ) )
			llClear := msgYesNo( "J� existe itens no cadastro. Deseja refazer ?" )
		endif
		
		if ( llProcOk .or. llClear )
		
			clAlias := getItens( clTab100, olModelZCE:getValue( "ZCE_CODKIT" ) )
			
			if ( .not. empty( clAlias ) )
			
				if ( llClear )
				
					if ( olModelZCF:canClearData() )
						olModelZCF:clearData()
					else
						llProcOk := .F.
						U_ADMVXFUN( 1,	{"ZCF_PROD", "Nao � poss�vel excluir os itens. Crie um novo cadastro e bloqueie este!" } )
					endif
				
				endif
			
				if ( llProcOk )
			
					while (	.not. ( clAlias )->( eof() ) )
					 	  
				 		olModelZCF:addLine()
				 		olModelZCF:goLine( nlLin++ )
				 		olModelZCF:loadValue( "ZCF_ITEM", ( clAlias )->ITEM )
				 		olModelZCF:loadValue( "ZCF_PROD", ( clAlias )->PRODUTO )
				 		olModelZCF:loadValue( "ZCF_DESPRO", ( clAlias )->DESCPRD )
				 		olModelZCF:loadValue( "ZCF_PRCORI", ( clAlias )->PRECO )
				 		olModelZCF:loadValue( "ZCF_PRCESP", ( clAlias )->PRECO )
						 	 
					 	( clAlias )->( dbSkip() )
					endDo
					
				endif
				
				( clAlias )->( dbCloseArea() )

				olModelZCF:goLine( 1 )
				olView:Refresh( 'VIEW_ZCF' )
				
			endif
	
		endif
		
	endif

	restArea( alArea )

Return

/*/{Protheus.doc} vldItens
Validando itens do cadastro
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@return llRet, Validando todos os itens do cadastro
@param olModel, object, Objeto de Modelo de dados 
@type function
/*/
Static Function vldItens( olModel )

	local llRet			:= .T.
	local nlx
	
	for nlx := 1 to olModel:getModel("ZCFDETAIL"):length()
		
		olModel:getModel("ZCFDETAIL"):goLine( nlx )
		
		if ( .not. vldPrcEsp( olModel ) )
			llRet := .F.
			exit
		endif
		
	next nlx

Return llRet

/*/{Protheus.doc} getItens
Responsavel por consultar os itens a serem carregados na interface
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@return clAlias, Alias da tabela temporaria da consulta de itens a serem carregados para a interface de kits
@param clTab100, characters, Codigo da tabela 100
@param clCodKit, characters, Codigo do Kit
@type function
/*/
Static Function getItens( clTab100, clCodKit )
	
	local clAlias		:= getNextAlias()
	local clSQL			:= ""
	
	default clTab100	:= ""
	default clCodKit	:= ""
	
	clSQL := "SELECT "
	clSQL += "	B1_COD AS PRODUTO "
	clSQL += "	, B1_DESC AS DESCPRD "
	clSQL += "	, DA1_PRCVEN AS PRECO "
	clSQL += "	, DA1_ITEM AS ITEM "
		
	clSQL += "FROM "
	clSQL += retSqlTab("DA1")
		
	clSQL += " INNER JOIN "
	clSQL += "	" + retSqlTab("SB1") + " ON "
	clSQL += "	SB1.B1_FILIAL = '" + xFilial("SB1") + "' "
	clSQL += "	AND SB1.B1_COD = DA1.DA1_CODPRO "
	clSQL += "	AND SB1.D_E_L_E_T_ = ' ' "
	
	clSQL += " INNER JOIN "
	clSQL += "	" + retSqlTab("SG1") + " ON "
	clSQL += "	SG1.G1_FILIAL = '" + xFilial("SG1") + "' "
	clSQL += "	AND SG1.G1_COD = '" + clCodKit + "' "
	clSQL += "	AND SG1.G1_COMP = SB1.B1_COD "
	clSQL += "	AND SG1.D_E_L_E_T_ = ' ' "
	
	clSQL += "WHERE "
	clSQL += "	DA1.DA1_FILIAL = '" + xFilial("DA1") + "' "
	clSQL += "	AND DA1.DA1_CODTAB = '" + clTab100 + "' "
	clSQL += "	AND DA1.D_E_L_E_T_ = ' ' "
	
	dbUseArea( .T., "TOPCONN", tcGenQry(,,clSQL), clAlias, .F., .T. )
			
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
Return clAlias

/*/{Protheus.doc} getTab100
Resposavel por retornar o codigo da tabela de preco base (100)
@author DS2U (SDA)
@since 10/08/2018
@version 1.0
@return clTab, Codigo da tabela base (100)

@type function
/*/
Static Function getTab100()
Return U_ADMVXFUN( 6, "ZC6_TABBAS" )

/*/{Protheus.doc} vldPrcEsp
Responsavel por validar o campo de pre�o especial
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@return llRet, Se .T., digitacao do campo esta OK, Se .F., houve falha no dado digitado
@param olModel, object, Objeto deo modelo de dados MVC
@type function
/*/
Static Function vldPrcEsp( olModel )

	local llRet		:= .T.
	local clMsg		:= ""

	default olModel	:= fwModelActive()
	
	olModel := olModel:getModel( "ZCFDETAIL" )
	
	// Preco deve ser positivo
	if ( olModel:getValue("ZCF_PRCESP") < 0 )
		llRet := .F.
		clMsg := "Produto [" + allTrim( olModel:getValue("ZCF_PROD") ) + "] - Pre�o deve ser positivo!"
	endif
	
	// Preco nao pode ser menor que preco origem
	if ( olModel:getValue("ZCF_PRCESP") < olModel:getValue("ZCF_PRCORI") )
		llRet := .F.
		clMsg := "Produto [" + allTrim( olModel:getValue("ZCF_PROD") ) + "] - Pre�o [" + allTrim( transform(  olModel:getValue("ZCF_PRCESP"), "999,999,999.99" ) ) + "] deve ser maior ou igual ao pre�o origem [" + allTrim( transform(  olModel:getValue("ZCF_PRCORI"), "999,999,999.99" ) ) + "]"
	endif
	
	if ( .not. llRet )
		U_ADMVXFUN( 1,	{"ZCF_PRCESP", clMsg } )
	endif

Return llRet

/*/{Protheus.doc} importCsv
Responsavel por importar o cadastro de Kits
@author DS2U (SDA)
@since 17/08/2018
@version 1.0

@type function
/*/
Static Function importCsv()

	local clFunc	:= "ADMVA11"
	local clTitle	:= "Importa��o de Kits"
	local clDesc	:= "Este programa tem por objetivo realizar a importa��o de Kits"
	local clPerg	:= "ADMVA11"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execImp( oSelf, MV_PAR01 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} execImp
Responsavel por importar Cotas por Produto baseado em um arquivo CSV
@author sergi
@since 02/08/2018
@version 1.0
@param oSelf, object, objeto do tNewProcess para controle de status
@param clFile, characters, caminho do arquivo CSV a ser lido para a importacao
@type function
/*/
Static Function execImp( oSelf, clFile )

	local nlHandle	:= FOPEN(clFile , FO_READWRITE + FO_SHARED )
	local nlTotLin	:= 0
	local nlCont	:= 0
	local clString	:= ""
	local clLinha	:= ""
	local nlQtdBytes:= 5000
	local nlBitLidos:= 0  
	local nlBits	:= 0
	local nlLinIni	:= 1
	local alDados	:= {}
	local llNoFound	:= .F.
	local nlTamPrd	:= tamSx3("ZCF_PROD")[1]
	local nlPreco	:= 0
	local clKit		:= ""
	local clComp	:= ""
	local llFail	:= .F.
	local alKits	:= {}
	local alKit		:= {}
	local clTab100	:= ""
	local nlx
	local nly
	local nlLinha
	
	if ( ferror() <> 0 )
		MsgAlert("Erro de abertura do arquivo: " + str( ferror() ) )
	else
	
		dbSelectArea( "ZCE" )
		ZCE->( dbSetOrder( 1 ) ) // ZCE_FILIAL, ZCE_CODKIT, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "ZCF" )
		ZCF->( dbSetOrder( 1 ) )

		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		dbSelectArea( "DA1" )
		DA1->( dbSetOrder( 1 ) ) // DA1_FILIAL, DA1_CODTAB, DA1_CODPRO, DA1_INDLOT, DA1_ITEM, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "SG1" )
		SG1->( dbSetOrder( 1 ) )
		
		nlTotLin := fSeek( nlHandle, 0, 2 )	// tamenho maximo de uma variavel  1048576		
		fSeek( nlHandle, 0, 0 )				// Posiciona no Inicio do Arquivo
		
		clTab100 := PADR( getTab100(), tamSX3("DA1_CODTAB")[1] )
		
		oSelf:setRegua1( 2 )
		oSelf:incRegua1( "Lendo arquivo" )
		
		while nlBitLidos <= nlTotLin
		
			nlCont++
		
			clString	:= FReadStr(nlHandle, nlQtdBytes)  
			nlBits		:= (At( CHR(13)+CHR(10),clString ))
			
			if ( nlBits == 0 )
				nlBits := Len( AllTrim( clString ) ) // Se o a ultima linha do arquivo nao conter os caractes chr(13) + chr(10) entao pego a quantidade de caracteres lidos
			endif
			
			if ( nlBits > 0 )
			
				nlBits += 1
				
				if ( nlCont == 1 )
					oSelf:setRegua2( int( nlTotLin / nlBits ) * 2 )
				endif				
				
				if ( nlCont > nlLinIni )
				
					fSeek( nlHandle, nlBitLidos, 0)				  
					clLinha	:= AllTrim( FReadStr(nlHandle, nlBits) )
					
					if ( .not. empty( clLinha ) )				
						
						alDados := separa( clLinha, ";", .T. )
						
						if ( len( alDados ) > 1 )
						
							clKit	:= PADR( alDados[1], nlTamPrd )
							clComp	:= PADR( alDados[2], nlTamPrd )
							nlPreco := val( alDados[3] )
							
							if ( DA1->( dbSeek( xFilial( "DA1" ) + clTab100 + clComp ) ) )
							
								if ( ( nlPos := aScan( alKits, {|x| x[1] == clKit .and. x[2] == clComp } ) ) == 0 )
									AADD( alKits, { clKit, clComp, DA1->DA1_PRCVEN, nlPreco, DA1->DA1_ITEM, nlCont } )
								else
									alKits[nlPos][4] := nlPreco
								endif
								
							endif
							
						else
							
							llFail := .T.
							oSelf:incRegua2("")
							oSelf:saveLog( "N�o foi possivel ler corretamente a linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
														
						endif
										
						alDados := {}
						
					endif
					
				endif
				
				nlBitLidos	+= nlBits
				FSeek( nlHandle, nlBitLidos, 0)
				
			else
				nlBitLidos	+= 1
			endif
		
		endDo
		
		oSelf:incRegua1( "Validando / Registrando dados..." )
		
		if ( .not. llFail )
		
			// Ordena por Kit + Componente
			aSort( alKits,,, { |x,y| x[1] + x[2] < y[1] + y[2] } )
			
			for nlx := 1 to len( alKits )
			
				clKit	:= alKits[nlx][1]
				clComp	:= alKits[nlx][2]
				nlPreco	:= alKits[nlx][4]
				nlLinha	:= alKits[nlx][6]
				
				if ( nlPreco <= 0 )
					
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "Pre�o deve ser positivo! KIT [" + clKit + "] / Componente: [" + clComp + "] / Pre�o: " + allTrim( transform( nlPreco, "@E 999,999,999.99" ) ) + ".  Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada" )
				
				elseif ( .not. SB1->( dbSeek( xFilial( "SB1" ) + clKit ) ) )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "N�o foi encontrado o KIT [" + clKit + "]. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
					
				elseif ( .not. SB1->( dbSeek( xFilial( "SB1" ) + clComp ) ) )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "N�o foi encontrado o componente [" + clComp + "]. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
					
				elseif ( .not. DA1->( dbSeek( xFilial( "DA1" ) + clTab100 + clComp ) ) )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "N�o foi encontrado o componente [" + clComp + "] na tabela de pre�os. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
					
				else
				
					if ( nlPreco < DA1->DA1_PRCVEN )
				
						llFail := .T.
						oSelf:incRegua2("")
						oSelf:saveLog( "Pre�o do componente [" + clComp + "] esta menor que o pre�o da tabela 100. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
				
					else
				
						if ( ZCE->( dbSeek( xFilial("ZCE") + clKit ) ) )
						
							oSelf:incRegua2("Alterando Kit [" + allTrim( clKit ) + "]")
							setKit( clKit, alKits[nlx][2], alKits[nlx][3], alKits[nlx][4], alKits[nlx][5], .F. )
						
						else
						
							// No caso de Kits novos, que ainda n�o teve seus pre�os cadastrados
							// Valido se todos os componentes do Kit foram digitados
							while ( nlx <= len( alKits ) .and. clKit == alKits[nlx][1] )
							
								AADD( alKit, alKits[nlx] )
								clKit := alKits[nlx][1]
							
								nlx++
							endDo
							nlx--
							
							// Add Kit
							if ( isKitOk( alKit ) )
							
								oSelf:incRegua2("Incluindo Kit [" + allTrim( clKit ) + "]")
							
								for nly := 1 to len( alKit )
									setKit( clKit, alKit[nly][2], alKit[nly][3], alKit[nly][4], alKit[nly][5] )
								next nly
							
							else
								
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi possivel incluir o KIT [" + allTrim( clKit ) + "], pois os componentes estao errados!" )
								
							endif
							
							alKit := {}
							
						endif 
						
					endif
					
				endif
				
				if ( llFail )
					exit
				endif
			
			next nlx
		
			oSelf:saveLog( "Arquivo " + allTrim( clFile ) + " processado com sucesso!" )
		endif
		
	endif

Return

/*/{Protheus.doc} isKitOk
Responsavel por validar se o Kit do array enviado por parametro existe no sistema
@author DS2U (SDA)
@since 17/08/2018
@version 1.0
@return llRet, Se .T., Kit existe, se .F. nao existe ou algum componente esta sobrando ou faltando
@param alKit, array of logical, array com as informacoes do KIT:	[1] = Codigo do KIT
																	[2] = Codigo do componente
																	[3] = Preco origenal de tabela de pre�o 100
																	[4] = Pre�o especial
																	[5] = Codigo do Item da tabela de pre�o a que se referente o produto
@type function
/*/
Static Function isKitOk( alKit )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clSQL		:= ""
	local clComps	:= ""
	local clCodKit	:= ""
	local nlx
	local clAlias	:= ""
	local llChkZCF	:= fwIsInCallStack("U_MA410BOM")
	
	default alKit	:= {}
	
	if ( len( alKit ) > 0 )
	
		dbSelectArea( "SG1" )
		SG1->( dbSetOrder( 1 ) ) // G1_FILIAL, G1_COD, G1_COMP, G1_TRT, R_E_C_N_O_, D_E_L_E_T_

		// Verifico se todos os componentes registrados no arquivo CSV pertencem / existem para o kit cadastrado
		// Aproveito o la�o de repeti��o para montar a string com o c�digo dos componentes para serem utilizados na query de busca de componentes faltantes
		for nlx := 1 to len( alKit )
		
			if ( SG1->( dbSeek( xFilial("SG1") + alKit[nlx][1] + alKit[nlx][2] ) ) )
			
				clCodKit := alKit[nlx][1]
				clComps += allTrim( alKit[nlx][2] ) + "/"
				
			else
				
				llRet := .F.
				exit
				
			endif
			
		next nlx
		clComps := subs( clComps, 1, len( clComps ) -1 )
	
		// Caso todos os componentes tenham sido encontrados para o KIT, verifico se tem algum componente faltando, pois
		// para a inclusao de preco de um KIT, � obrigat�rio que seja preenchido pre�o para todos os componentes
		if ( llRet )
	
			if ( llChkZCF )
			
				clSQL := "SELECT " 
				clSQL += "	COUNT(*) AS QTDCOMP " 
					
				clSQL += "FROM "
				clSQL += "	" + retSqlName("ZCF") + " ZCF " 
				
				clSQL += "WHERE "
				clSQL += "	ZCF.D_E_L_E_T_ = ' ' "
				clSQL += "	AND ZCF_CODKIT = '" + clCodKit + "' "
				clSQL += "	AND ZCF_PROD IN " + formatIn( clComps, "/" )
				
			else
				clSQL := "SELECT " 
				clSQL += "	COUNT(*) AS QTDCOMP " 
					
				clSQL += "FROM "
				clSQL += "	" + retSqlName("SG1") + " SG1 " 
				
				clSQL += "WHERE "
				clSQL += "	SG1.D_E_L_E_T_ = ' ' "
				clSQL += "	AND G1_COD = '" + clCodKit + "' "
				clSQL += "	AND G1_COMP NOT IN " + formatIn( clComps, "/" )
			
			endif
				
				clAlias := getNextAlias()
				dbUseArea( .T., "TOPCONN", tcGenQry(,,clSQL), clAlias, .F., .T. )
					
				if	(	.not. ( clAlias )->( eof() );
				 		.and. iif( llChkZCF, .not. ( ( clAlias )->QTDCOMP == len( alKit ) ), ( clAlias )->QTDCOMP > 0 ); 
					)
					llRet := .F.
				endif
				( clAlias )->( dbCloseArea() ) 
				
		endif
		
	else
		llRet := .F.
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} setKit
Responsavel por controlar a inclusao / alteracao de Kit
@author DS2U (SDA)
@since 17/08/2018
@version 1.0
@return llRet, Se .T., Gravou o registro com sucesso
@param clCodKit, characters, Codigo do Kit
@param clComp, characters, Codigo do componente
@param nlPrcOri, numeric, Preco origem, conforme tabela 100
@param clPrcEsp, characters, Preco especifico digitado pelo usuario
@param clItemDA1, characters, Item da tabela de preco a que se referete o componente
@param llInc, boolean, Identifica se .T., Inclusao ou .F. altera��o
@type function
/*/
Static Function setKit( clCodKit, clComp, nlPrcOri, clPrcEsp, clItemDA1, llInc )
	
	local alArea	:= getArea()
	local llFoundZCF:= .T.
	local llFoundZCE:= .T.
	local llRet		:= .F.
	
	default llInc := .T.
	
	dbSelectArea( "ZCE" )
	ZCE->( dbSetOrder( 1 ) ) // ZCE_FILIAL, ZCF_CODKIT, R_E_C_N_O_, D_E_L_E_T_
	llFoundZCE := ZCE->( dbSeek( xFilial("ZCE") + clCodKit ) )
	
	if ( .not. llInc )
		
		dbSelectArea( "ZCF" )
		ZCF->( dbSetOrder( 1 ) ) // ZCF_FILIAL, ZCF_CODKIT, ZCF_ITEM, ZCF_PROD, R_E_C_N_O_, D_E_L_E_T_
		
		llFoundZCF := ZCF->( dbSeek( xFilial("ZCF") + clCodKit + clItemDA1 + clComp ) )
		
	endif
	
	if ( llFoundZCF )
	
		begin transaction 
	
			if ( .not. llFoundZCE .and. recLock( "ZCE", .not. llFoundZCE ) )
								
				ZCE->ZCE_FILIAL	:= xFilial("ZCE")
				ZCE->ZCE_CODKIT	:= clCodKit
				ZCE->( msUnLock() )
				
			endif
			
			if ( recLock( "ZCF", llInc ) )
								
				ZCF->ZCF_FILIAL	:= xFilial("ZCF")
				ZCF->ZCF_CODKIT	:= clCodKit
				ZCF->ZCF_ITEM	:= clItemDA1
				ZCF->ZCF_PROD	:= clComp
				ZCF->ZCF_PRCORI := nlPrcOri
				ZCF->ZCF_PRCESP	:= clPrcEsp
				ZCF->( msUnLock() )
				
			endif
			
			llRet := .T.
			
		end transaction
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de importacao de cotas por produto
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	AADD(aPergunt,{cPerg,"01","Selecione o arquivo  ","MV_CH1","C",99,0,"G","NaoVazio()","MV_PAR01","","","","","","ARQCSV",""})
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return