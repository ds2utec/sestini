#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA07
Rotina de cadastro de Cotas por item
@author DS2U (SDA)
@since 25/07/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA07( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			browse()
			
		case nlOpc == 1
			importar()
			
	endCase

Return uRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 25/07/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= SB1->( getArea() )

	private oBrowse

	dbSelectArea("SB1")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("SB1")
	oBrowse:SetDescription("Cadastro de Cotas x Produto")
	oBrowse:Activate()
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA07"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA07"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA07"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA07"	OPERATION 5 ACCESS 0 // "Excluir" 
	ADD OPTION alRotina TITLE "Importar"				ACTION "U_ADMVA07(1)"		OPERATION 3 ACCESS 0 // "Importar"
	ADD OPTION alRotina TITLE "Log"						ACTION "U_ADMVA07(3)"		OPERATION 2 ACCESS 0 // "Log" 

Return alRotina

/*/{Protheus.doc} ModelDef
Modeldef do cadastro de Cotas por Produto
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@return olModel, Objeto de model do cadastro

@type function
/*/
Static Function ModelDef()

	local olModel
	local oStruSB1	:= FWFormStruct( 1, 'SB1', { |x| allTrim( x ) $ 'B1_COD,B1_DESC' }, /*lViewUsado*/ )
	local oStruZC8	:= FWFormStruct( 1, 'ZC8', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA07M', /*{ |olModel| cotaPreVld( olModel ) }*//* bPreValidacao*/,{ |olModel| cotaTdOk( olModel ) }/*bPosValidacao*/, { |olModel| cotaCommit( olModel ) }/*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'SB1MASTER', /*cOwner*/, oStruSB1, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'ZC8DETAIL', 'SB1MASTER', oStruZC8, /*{|olModel,  nLine,cAction,  cField| PRR13LPre(olModel, nLine, cAction, cField)}*/ /*bLinePre*/, /*{ |olModel| ZC8VldLPos( olModel ) }*/ /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*B*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'ZC8DETAIL', { { 'ZC8_FILIAL', 'xFilial( "ZC8" )' }, { 'ZC8_PROD', 'B1_COD' }}, ZC8->( IndexKey( 1 ) ) )
	
	olModel:SetDescription('Cadastro de Cotas x Produto')
	olModel:getModel('SB1MASTER' ):SetDescription('Cadastro de Cotas x Produto') 
	
	olModel:getModel('ZC8DETAIL'):SetOptional( .F. )
	olModel:getModel('ZC8DETAIL'):SetUniqueLine({"ZC8_CANAL"})
	
	olModel:SetActivate( {|olModel| cotaLoad( olModel ) } )

Return olModel

/*/{Protheus.doc} ViewDef
Viewdef do cadastro de cotas por produto
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@return olView, Objeto de view do cadastro

@type function
/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local oStruSB1	:= FWFormStruct( 2, 'SB1', { |x| allTrim( x ) $ 'B1_COD,B1_DESC' }, /*lViewUsado*/ )
	local oStruZC8	:= FWFormStruct( 2, 'ZC8' )
	
	// Cria a estrutura a ser usada na View
	local olModel    := FWLoadModel( 'ADMVA07' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_SB1', oStruSB1, 'SB1MASTER' )		
		
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_ZC8', oStruZC8, 'ZC8DETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 20 )
	olView:CreateHorizontalBox( 'INFERIOR', 80 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_SB1', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_ZC8', 'INFERIOR' )
	
	olView:EnableTitleView( 'VIEW_ZC8', 'Cotas x Canal' )
	olView:SetViewProperty("VIEW_SB1", "ONLYVIEW")
	olView:HideFolder("VIEW_SB1",1,1)
	olView:SetNoDeleteLine( "VIEW_ZC8" )

Return olView

/*/{Protheus.doc} cotaTdOk
Responsavel por validar todo o cadastro antes de enviar para a gra��o no banco de dados
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@return llRet, Se .T., cadastro validado, se .F. houve falha no preenchimento do cadastro
@param olModel, object, Modelo de dados do cadastro de cotas por produto
@type function
/*/
Static Function cotaTdOk( olModel )

	local llRet			:= .T.
	local nlx
	local nlSumPerc		:= 0
	local olModelZC8	:= olModel:getModel('ZC8DETAIL')

	// Valida se o total do percentual distribuido nao ultrapassou 100%
	for nlx := 1 to olModelZC8:length()
			
		olModelZC8:goLine( nlx )
		
		nlSumPerc += olModelZC8:getValue( "ZC8_COTA" )
		
		if ( nlSumPerc > 100 )
			llRet := .F.
			exit
		endif
		
	next nlx
	
	if ( .not. llRet )
		U_ADMVXFUN( 1,	{"ZC8_COTA", "A soma dos percentuais entre os canais ultrapassou 100%. Por favor, revise o cadastro!" } )
	endif

Return llRet

/*/{Protheus.doc} cotaCommit
Responsavel por realizar a gravacao no banco de dados
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return llRet, Se .T. validacao OK, se nao houve falha no processamento
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function cotaCommit( olModel )

	local llRet			:= .T.
	local nlx
	local olModelZC8	:= olModel:getModel('ZC8DETAIL')

	// Exclui do banco de dados os registro vinculados ao produto / canal para considerar apenas os novos registros
	if ( tcSqlExec( "DELETE FROM " + retSqlName("ZC8") + " WHERE D_E_L_E_T_ = ' ' AND ZC8_PROD = '" + SB1->B1_COD + "' " ) < 0 )
		
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZC8_PROD", "Nao foi possivel excluir os canais vinculados ao produto [" + allTrim( SB1->B1_COD ) + "]" + CRLF + tcSqlError() } )
		
	else
	
		llRet := .T.
		
		for nlx := 1 to olModelZC8:length()
			
			olModelZC8:goLine( nlx )
			
			if ( recLock( "ZC8", .T. ) )
			
				ZC8->ZC8_FILIAL	:= xFilial("ZC8")
				ZC8->ZC8_PROD	:= SB1->B1_COD
				ZC8->ZC8_CANAL	:= olModelZC8:getValue( "ZC8_CANAL" )
				ZC8->ZC8_COTA	:= olModelZC8:getValue( "ZC8_COTA" )
			
				ZC8->( msUnLock() )
				
			endif
			
		next nlx
	
	endif

Return llRet

/*/{Protheus.doc} cotaLoad
Carrega a grid de itens com os canais do ADMV
@author DS2U ( SDA )
@since 01/08/2018
@version 1.0
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function cotaLoad( olModel )

	local alArea		:= getArea()
	local nlOpc			:= olModel:getOperation()
	local olModelSB1	:= olModel:getModel('SB1MASTER' )
	local olModelZC8	:= olModel:getModel('ZC8DETAIL')
	local alCanais		:= {}
	local nlx
	local nly
	local alInfoZC8		:= {}
	local clField		:= ""
	local clContent		:= ""
	local llFound		:= .F.
	local nlLine
	local olStruct
	local nlLinAdd		:= 0
	
	if ( nlOpc == 3 .or.  nlOpc == 4 )
	
		dbSelectArea("ZC8")
		ZC8->( dbSetOrder( 1 ) ) // ZC8_FILIAL, ZC8_PROD, ZC8_CANAL
		
		olStruct	:= olModelZC8:getStruct()
		alInfoZC8	:= olStruct:getFields()
		
		olModelSB1:loadValue("B1_COD"	, SB1->B1_COD )
		olModelSB1:loadValue("B1_DESC"	, SB1->B1_DESC )
	
		alCanais	:= getCanais( .T. )  // Adiciona apenas os canais 1� linha, pois diferente disso, nao controla cotas
		
		// Ponteiro para varrer os canais do ADMV
		for nlx := 1 to len( alCanais )
		
			llFound := ZC8->( dbSeek( xFilial("ZC8") + SB1->B1_COD + alCanais[nlx][1] ) )
			nlLinAdd++

			if ( nlOpc == 3 .or. ( nlOpc == 4 .and. .not. llFound ) )
		
				if (  nlLinAdd > 1 .or. nlOpc == 4 )
					nlLine := olModelZC8:AddLine()
					olModelZC8:goLine( nlLine )
				endif
				
				// Ponteiro para varrer os campos da tabela ZC8 e preencher as informacoes na grid conforme os canais existentes
				for nly := 1 to len( alInfoZC8 )
				
					clField		:= allTrim( alInfoZC8[nly][3] )
					clContent	:= ""
				
					do Case
					
						case clField == "ZC8_FILIAL"
						
							if ( llFound )
								clContent	:= ZC8->ZC8_FILIAL
							else
								clContent	:= xFilial("ZC8")
							endif
						
						case clField == "ZC8_PROD"
						
							if ( llFound )
								clContent	:= ZC8->ZC8_PROD
							else
								clContent	:= SB1->B1_COD
							endif
							
						case clField == "ZC8_CANAL"
						
							if ( llFound )
								clContent	:= ZC8->ZC8_CANAL
							else
								clContent	:= alCanais[nlx][1]
							endif
						
						case clField == "ZC8_DESCAN"
						
							if ( llFound )
								clContent	:= posicione("ZC0", 1, xFilial("ZC0") + ZC8->ZC8_CANAL, "ZC0_DESCAN" )
							else
								clContent	:= alCanais[nlx][2]
							endif
						
						case clField == "ZC8_COTA"
						
							if ( llFound )
								clContent	:= ZC8->ZC8_COTA
							else
								clContent	:= 0
							endif
					
					endCase
					
					if ( .not. empty( clContent ) )
						olModelZC8:loadValue( clField, clContent )
					endif
				
				next nly
				
			endif
				
		next nlx
	
	endif
	
	restArea( alArea )

Return .T.

/*/{Protheus.doc} getCanais
Responsavel por tratar os canais do ADMV
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return U_ADMVXFUN( 4 ), Retorno da funcao U_ADMVXFUN( 4 )
@param llOnlyPLin, boolean, Busca canais somente de primeira linha
@type function
/*/
Static Function getCanais( llOnlyPLin )
Return U_ADMVXFUN( 4, llOnlyPLin )

/*/{Protheus.doc} importar
Responsavel por controlar o status de importacap de Cotas por Produto baseado em um arquivo CSV
@author DS2U (SDA)
@since 02/08/2018
@version 1.0

@type function
/*/
Static Function importar()

	local clFunc	:= "ADMVA07"
	local clTitle	:= "Importa��o de Cotas por Produto"
	local clDesc	:= "Este programa tem por objetivo realizar a importa��o de cotas por produto"
	local clPerg	:= "ADMVA07"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execImp( oSelf, MV_PAR01 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} execImp
Responsavel por importar Cotas por Produto baseado em um arquivo CSV
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@param oSelf, object, objeto do tNewProcess para controle de status
@param clFile, characters, caminho do arquivo CSV a ser lido para a importacao
@type function
/*/
Static Function execImp( oSelf, clFile )

	local nlHandle	:= FOPEN(clFile , FO_READWRITE + FO_SHARED )
	local nlTotLin	:= 0
	local nlCont	:= 0
	local clString	:= ""
	local clLinha	:= ""
	local nlQtdBytes:= 5000
	local nlBitLidos:= 0  
	local nlBits	:= 0
	local nlLinIni	:= 1
	local alDados	:= {}
	local llNoFound	:= .F.
	local llCanCota	:= .F.
	local nlTamPrd	:= tamSx3("ZC8_PROD")[1]
	local nlTamCan	:= tamSx3("ZC8_CANAL")[1]
	local nlCota	:= 0
	local clProduto	:= ""
	local clCanal	:= ""
	local llFail	:= .F.
	local nlCotaAtu	:= 0
	
	if ( ferror() <> 0 )
		MsgAlert("Erro de abertura do arquivo: " + str( ferror() ) )
	else
	
		dbSelectArea( "ZC0" )
		ZC0->( dbSetOrder( 1 ) )
		
		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		dbSelectArea( "ZC8" )
		ZC8->( dbSetOrder( 1 ) )
	
		nlTotLin := fSeek( nlHandle, 0, 2 )	// tamenho maximo de uma variavel  1048576		
		fSeek( nlHandle, 0, 0 )				// Posiciona no Inicio do Arquivo
		
		oSelf:setRegua1( 2 )
		oSelf:incRegua1( "Lendo arquivo" )
		
		while nlBitLidos <= nlTotLin
		
			nlCont++
		
			clString	:= FReadStr(nlHandle, nlQtdBytes)  
			nlBits		:= (At( CHR(13)+CHR(10),clString ))
			
			if ( nlBits == 0 )
				nlBits := Len( AllTrim( clString ) ) // Se o a ultima linha do arquivo nao conter os caractes chr(13) + chr(10) entao pego a quantidade de caracteres lidos
			endif
			
			if ( nlBits > 0 )
			
				nlBits += 1
				
				if ( nlCont == 1 )
					oSelf:setRegua2( int( nlTotLin / nlBits ) )
				endif				
				
				if ( nlCont > nlLinIni )
				
					fSeek( nlHandle, nlBitLidos, 0)				  
					clLinha	:= AllTrim( FReadStr(nlHandle, nlBits) )
					
					if ( .not. empty( clLinha ) )				
						
						alDados := separa( clLinha, ";", .T. )
						
						if ( len( alDados ) > 1 )
						
							clProduto	:= PADR( alDados[1], nlTamPrd )
							clCanal		:= PADR( alDados[2], nlTamCan )
							nlCota := val( alDados[3] )
							
							if ( .not. SB1->( dbSeek( xFilial( "SB1" ) + clProduto ) ) )
							
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi encontrado o produto [" + clProduto + "]. Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
							elseif ( .not. ZC0->( dbSeek( xFilial( "ZC0" ) + clCanal ) ) )
							
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi encontrado o canal [" + clCanal + "]. Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
							else
							
								nlCotaAtu	:= U_ADMVXFUN( 5, clProduto ) // Busca o total de cota distribuida para o produto
							 	llNoFound	:= .not. ZC8->( dbSeek( xFilial("ZC8") + clProduto + clCanal ) )
							 	
							 	if ( llNoFound )
							 		llCanCota := ( ( nlCotaAtu + nlCota ) <= 100 ) 
									oSelf:incRegua2( "Incluindo registro " + allTrim( allToChar( nlCont ) ) + " - Produto: " + allTrim( alDados[1] ) )
								else
									llCanCota := ( ( nlCotaAtu + nlCota - ZC8->ZC8_COTA ) <= 100 ) // Se for alteracao, devo desconsiderar a cota do registro que esta sendo alterado
									oSelf:incRegua2( "Alterando registro " + allTrim( allToChar( nlCont ) ) + " - Produto: " + allTrim( alDados[1] ) )
								endif
								
								if ( llCanCota )
								
									if ( recLock( "ZC8", llNoFound ) )
										
										ZC8->ZC8_FILIAL	:= xFilial("ZC8")
										ZC8->ZC8_PROD	:= PADR( alDados[1], nlTamPrd )
										ZC8->ZC8_CANAL	:= PADR( alDados[2], nlTamCan )
										ZC8->ZC8_COTA	:= nlCota
										ZC8->( msUnLock() )
										
									endif
									
								else
								
									llFail := .T.
									oSelf:saveLog( "Cota [" + allTrim( Str( nlCota ) ) + "%] do canal [" + clCanal + "] ultrapassa o limite de 100% da cota por produto. Cota total distribuida do produto [" + allTrim( Str( nlCotaAtu ) ) + "%].Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
								endif
								
							endif
							
						else
							
							llFail := .T.
							oSelf:incRegua2("")
							oSelf:saveLog( "N�o foi possivel ler corretamente a linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
														
						endif
										
						alDados := {}
						
					endif
					
				endif
				
				nlBitLidos	+= nlBits
				FSeek( nlHandle, nlBitLidos, 0)
				
			else
				nlBitLidos	+= 1
			endif
		
		endDo
		
		oSelf:incRegua1( "Validando registros" )
		
		if ( .not. llFail )
			oSelf:saveLog( "Arquivo " + allTrim( clFile ) + " processado com sucesso!" )
		endif
		
	endif

Return

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de importacao de cotas por produto
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	AADD(aPergunt,{cPerg,"01","Selecione o arquivo  ","MV_CH1","C",99,0,"G","NaoVazio()","MV_PAR01","","","","","","ARQCSV",""})
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return