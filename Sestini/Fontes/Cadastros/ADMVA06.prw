#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA06
Cadastro de Prioridade de Pedido
@author DS2U (SDA)
@since 24/06/2018
@version 1.0

@type function
/*/
User Function ADMVA06( nlOpc )

	default nlOpc := 0
	
	do Case
	
		case nlOpc == 0
			browse()
			
		case nlOpc == 1
			importCsv()
	
	endCase
	

Return

Static Function browse()
	
	local alArea	:= ZC5->( getArea() )

	private oBrowse

	dbSelectArea("ZC5")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC5")
	oBrowse:SetDescription("Cadastro de Prioridade de Pedido")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA06"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA06"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA06"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Importar"				ACTION "U_ADMVA06(1)"		OPERATION 3 ACCESS 0 // "Importar Promo��es"

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC5	:= FWFormStruct( 2, 'ZC5' )
	local olModel	:= FWLoadModel( 'ADMVA06' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC5', olStruZC5, 'ZC5MASTER' )
	
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC5', 'SUPERIOR' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC5	:= FWFormStruct( 1, 'ZC5', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA06M', /* bPreValidacao*/,/*bPosValidacao { |olModel| admvTudoOk( olModel ) }*/,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC5MASTER', /*cOwner*/, olStruZC5, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC5_FILIAL", "ZC5_COD"} )
	
	olModel:SetDescription('Prioridade de Pedido')
	olModel:GetModel('ZC5MASTER' ):SetDescription('Prioridade de Pedido')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel