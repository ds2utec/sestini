#INCLUDE 'PROTHEUS.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TOTVS.CH"

//-----------------------------------------------
/*/{Protheus.doc} ADMVA16
Cadastro de Limite de Credito - Cliente/Matriz - ACY

@author DS2U (HFA)
@since 10/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVA16()

	Local cGrpMatriz	:= SA1->A1_GRPVEN
	Local nOperation	:= 0
	Local oModel		:= Nil
	Local oMdlACY		:= Nil
	Local lOk			:= .T.
	
	If Empty( cGrpMatriz )
	
		nOperation	:= MODEL_OPERATION_INSERT
		oModel		:= FWLoadModel("FATA110")
		oModel:SetOperation(nOperation)
		oModel:Activate()
		oMdlACY 	:= oModel:GetModel("ACYMASTER")
		oMdlACY:LoadValue("ACY_XMATRI",SA1->A1_COD)
		oMdlACY:LoadValue("ACY_XLJMAT",SA1->A1_LOJA)
		oMdlACY:LoadValue("ACY_XNMATR",SA1->A1_NOME)
		
	Else
	
		nOperation	:= MODEL_OPERATION_UPDATE
		dbSelectArea( "ACY" )
		ACY->( dbSetOrder( 1 ) )
		If !( ACY->( dbSeek( FWxFilial( "ACY" ) + cGrpMatriz ) ) )
			lOk :=  .F.
			Help(,, "ADMVA16_01",, 'N�o foi localizado o cadastro do Grupo Matriz:'+ cGrpMatriz +', verifique o cadastro de cliente se est� vinculado ao cadastro correto.', 1, 0)
		EndIf
		
	EndIf

	If lOk

		If FWExecView("Grupo Matriz","FATA110", nOperation ,,,,,,,,, oModel ) == 0
			
			If nOperation == MODEL_OPERATION_INSERT
				
				If SA1->( RecLock("SA1" , .F.) )
					SA1->A1_GRPVEN	:= ACY->ACY_GRPVEN 
					SA1->( MsUnLock() )
				EndIf
				
			EndIf
		
		EndIf

	EndIf
	
	If ValType( oModel) == "O"
		FreeObj(oModel)
		oModel:= Nil
	EndIf

Return