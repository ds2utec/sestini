#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA13
Responsavel por controlar a disponibilidade dos itens para aloca��es, considerando saldo futuro
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return uRet, Informacao retornada pela funcao executada
@param nlOpc, numeric, Opcao para execucao da funcao
@param uParam, undefined, Parametro para a funcao a ser executada
@type function
/*/
User Function ADMVA13( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			browse()
			
		case nlOpc == 1
			uRet := cadastrar( uParam )
			
		case nlOpc == 2
			uRet := whenEdit( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} whenEdit
Funcao para ser executada no When dos campos de digita��o de data e dias ETA
@author DS2U (SDA)
@since 20/12/2018
@version 1.0
@return llRet, Retorna .T. se pode editar ou .F. se estiver bloqueado

@type function
/*/
Static Function whenEdit()

	local alArea	:= getArea()
	local llRet		:= .F.
	local olModel    := fwModelActive()
	local olModelSW2	:= olModel:getModel('SW2MASTER' )
	local olModelZCK	:= olModel:getModel('ZCKDETAIL')

	dbSelectArea( "ZCK" )
	ZCK->( dbSetOrder( 1 ) ) // ZCK_FILIAL, ZCK_PO, ZCK_DTMOV, ZCK_HRMOV, R_E_C_N_O_, D_E_L_E_T_
	
	if ( .not. ( llRet := ( .not. ZCK->( dbSeek( fwxFilial( "ZCK" ) + olModelSW2:getValue( "W2_PO_NUM" ) + dToS( olModelZCK:getValue( "ZCK_DTMOV" ) ) + olModelZCK:getValue( "ZCK_HRMOV" ) ) ) ) ) )
		U_ADMVXFUN( 1, { readVar(), "N�o � permitido alterar registro j� gravado. Por favor, inclua uma nova linha para altera��o de data!" } )
	endif

	restArea( alArea )

Return llRet

/*/{Protheus.doc} cadastrar
Funcao para executar a view de cadastro
@author DS2U (SDA)
@since 20/12/2018
@version 1.0

@type function
/*/
Static Function cadastrar()
	FWExecView( "P.O." , "ADMVA13", 3,, {|| .T.} )
Return

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 03/09/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= SW2->( getArea() )

	private oBrowse

	dbSelectArea("SW2")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("SW2")
	oBrowse:SetDescription("Cadastro de Disponibilidade x P.O.")
	oBrowse:Activate()
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA13"	OPERATION 4 ACCESS 0 // "Alterar"

Return alRotina

/*/{Protheus.doc} ModelDef
Modeldef do cadastro de Cotas por Produto
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return olModel, Objeto de model do cadastro

@type function
/*/
Static Function ModelDef()

	local olModel
	local oStruSW2	:= FWFormStruct( 1, 'SW2', { |x| allTrim( x ) $ 'W2_PO_NUM,W2_PO_DT' }, /*lViewUsado*/ )
	local oStruZCK	:= FWFormStruct( 1, 'ZCK', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA13M',/* { |olModel| preVld( olModel ) } bPreValidacao*/,/*{ |olModel| tudoOK( olModel ) }bPosValidacao*/, { |olModel| commit( olModel ) } /*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'SW2MASTER', /*cOwner*/, oStruSW2, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'ZCKDETAIL', 'SW2MASTER', oStruZCK, {|olModel,nLine,cAction,cField| preVld(olModel, nLine, cAction, cField)} /*bLinePre*/, /*{ |olModel| ZCKVldLPos( olModel ) }*/ /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'ZCKDETAIL', { { 'ZCK_FILIAL', 'xFilial( "ZCK" )' }, { 'ZCK_PO', 'W2_PO_NUM' }}, ZCK->( IndexKey( 1 ) ) )
	
	olModel:SetDescription('Cadastro de Disponibilidade x P.O.')
	olModel:getModel('SW2MASTER' ):SetDescription('Disponibilidade x P.O.') 
	
	olModel:getModel('ZCKDETAIL'):SetOptional( .F. )
	olModel:getModel('ZCKDETAIL'):SetUniqueLine({"ZCK_DTMOV", "ZCK_HRMOV"})
	
	olModel:SetActivate( {|olModel| load( olModel ) } )

Return olModel

/*/{Protheus.doc} ViewDef
Viewdef do cadastro de cotas por produto
@author sergi
@since 02/08/2018
@version 1.0
@return olView, Objeto de view do cadastro

@type function
/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local oStruSW2	:= FWFormStruct( 2, 'SW2', { |x| allTrim( x ) $ 'W2_PO_NUM,W2_PO_DT' }, /*lViewUsado*/ )
	local oStruZCK	:= FWFormStruct( 2, 'ZCK' )
	
	// Cria a estrutura a ser usada na View
	local olModel    := FWLoadModel( 'ADMVA13' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_SW2', oStruSW2, 'SW2MASTER' )		
		
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_ZCK', oStruZCK, 'ZCKDETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 20 )
	olView:CreateHorizontalBox( 'INFERIOR', 80 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_SW2', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_ZCK', 'INFERIOR' )
	
	olView:EnableTitleView( 'VIEW_ZCK', 'Hist�rico' )
	olView:SetViewProperty("VIEW_SW2", "ONLYVIEW")
	olView:HideFolder("VIEW_SW2",1,1)
	olView:SetNoDeleteLine( "VIEW_ZCK" )

Return olView

/*/{Protheus.doc} load
Responsavel por controlar o carregamento do modelo de dados da interface de controle de disponibilidazao de datas do PO
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@param olModel, object, description
@type function
/*/
Static Function load( olModel )

	local alArea		:= getArea()
	local nlOpc			:= olModel:getOperation()
	local olModelSW2	:= olModel:getModel('SW2MASTER' )
	local olModelZCK	:= olModel:getModel('ZCKDETAIL')
	local nlLine
	local nlLinAdd		:= 0

	olModelSW2:loadValue("W2_PO_NUM", SW2->W2_PO_NUM )
	olModelSW2:loadValue("W2_PO_DT"	, SW2->W2_PO_DT )
	
	dbSelectArea( "ZCK" )
	ZCK->( dbSetOrder( 1 ) ) // ZCK_FILIAL, ZCK_PO, ZCK_DTMOV, ZCK_HRMOV, R_E_C_N_O_, D_E_L_E_T_
	
	if ( ZCK->( dbSeek( fwxFilial( "ZCK" ) + olModelSW2:getValue( "W2_PO_NUM" ) ) ) )
	
		while (	.not. ZCK->( eof() );
		 		.and. ZCK->ZCK_FILIAL == fwxFilial( "ZCK" );
		 		.and. ZCK->ZCK_PO == SW2->W2_PO_NUM;
			  )
			  
			nlLinAdd++
		
			if ( nlLinAdd > 1 )
				nlLine := olModelZCK:AddLine()
				olModelZCK:goLine( nlLine )
			endif
			
			olModelZCK:loadValue( "ZCK_FILIAL"	, ZCK->ZCK_FILIAL )
			olModelZCK:loadValue( "ZCK_PO"		, ZCK->ZCK_PO )
			olModelZCK:loadValue( "ZCK_DTMOV"	, ZCK->ZCK_DTMOV )
			olModelZCK:loadValue( "ZCK_HRMOV"	, ZCK->ZCK_HRMOV )
			olModelZCK:loadValue( "ZCK_DTORPO"	, ZCK->ZCK_DTORPO )
			olModelZCK:loadValue( "ZCK_DIAETA"	, ZCK->ZCK_DIAETA )
			olModelZCK:loadValue( "ZCK_DTCLPO"	, ZCK->ZCK_DTCLPO )
			olModelZCK:loadValue( "ZCK_USER"	, ZCK->ZCK_USER )
		
			ZCK->( dbSkip() )
		endDo
	
	endif
	
	restArea( alArea )
	
Return .T.

/*/{Protheus.doc} commit
Responsavel por controlar a grava��o da entidade de controle de disponibilidade
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return llRet, Se .T., processamento OK
@param olModel, object, description
@type function
/*/
Static Function commit( olModel )

	local alArea		:= getArea()
	local nlOpc			:= olModel:getOperation()
	local olModelSW2	:= olModel:getModel('SW2MASTER' )
	local olModelZCK	:= olModel:getModel('ZCKDETAIL')
	local nlx
	local nly
	local llRet			:= .T.
	local alPCs			:= {}
	local llIsDesaloc	:= .F.
	local llProcOk		:= .F.
	local clNumPO		:= olModelSW2:getValue( "W2_PO_NUM" )
	local dlDataAnt		:= cToD("//")
	local dlDataPos		:= cToD("//")
	
	dbSelectArea( "ZCK" )
	ZCK->( dbSetOrder( 1 ) ) // ZCK_FILIAL, ZCK_PO, ZCK_DTMOV, ZCK_HRMOV, R_E_C_N_O_, D_E_L_E_T_
	
	if ( nlOpc == 4 )
		llIsDesaloc := msgYesNo( "Deseja processar as REALOCA��ES dos pedidos de vendas envolvidos ?", "Realocar ?" )
	endif
	
	for nlx := 1 to olModelZCK:length()
			
		olModelZCK:goLine( nlx )
		
		if ( .not. ZCK->( dbSeek( fwxFilial( "ZCK" ) + olModelSW2:getValue( "W2_PO_NUM" ) + dToS( olModelZCK:getValue( "ZCK_DTMOV" ) ) + olModelZCK:getValue( "ZCK_HRMOV" ) ) ) )
		
			if ( llIsDesaloc )
				alPCs := getPcByPO( ZCK->ZCK_PO )
			endif
			
			dlDataPos	:= olModelZCK:getValue( "ZCK_DTCLPO" )
			llProcOk	:= .F.
		
			begin transaction 

				if ( recLock( "ZCK", .T. ) )
				
					ZCK->ZCK_FILIAL	:= fwxFilial("ZCK")
					ZCK->ZCK_PO		:= olModelSW2:getValue( "W2_PO_NUM" )
					ZCK->ZCK_DTMOV	:= olModelZCK:getValue( "ZCK_DTMOV" )
					ZCK->ZCK_HRMOV	:= olModelZCK:getValue( "ZCK_HRMOV" )
					ZCK->ZCK_DTORPO	:= olModelZCK:getValue( "ZCK_DTORPO" )
					ZCK->ZCK_DIAETA	:= olModelZCK:getValue( "ZCK_DIAETA" )
					ZCK->ZCK_DTCLPO	:= olModelZCK:getValue( "ZCK_DTCLPO" )
					ZCK->ZCK_USER	:= olModelZCK:getValue( "ZCK_USER" )
				
					ZCK->( msUnLock() )
					
				endif
				
				if ( llIsDesaloc )
				
					for nly := 1 to len( alPCs )
						U_ADMVP02( 1, { nil, nil, .T., .F., { alPCs[nly][1], alPCs[nly][2], .F. } } )
					next nly
					
				endif
				
				llProcOk := .T.
			
			end transaction
			
		else
			
			llProcOk := .T.
			
			if ( olModelZCK:length() > 1 )
				dlDataAnt := olModelZCK:getValue( "ZCK_DTCLPO" )
			endif
			
		endif
		
		if ( .not. llProcOk )
			exit
		endif
		
	next nlx
	
	if ( llProcOk )
		if ( .not. empty( clNumPO ) .and. .not. empty( dlDataAnt ) .and. .not. empty( dlDataPos ) )
			fwMsgRun(,{|oSay| sendMail( clNumPO, dlDataAnt, dlDataPos ) },"Aguarde...","Enviando notifica��o de altera��o de disponibilidade..." )
		endif
	else
		llRet := .F.
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} sendMail
Funcao responsavel por enviar um e-mail notificando os envolvidos sobre a altera��o de data de disponibilidade
@author DS2U (SDA)
@since 20/12/2018
@version 1.0
@param clNumPO, characters, Nr. do P.O. alterado
@param dlDataAnt, date, Data atual do embarque
@param dlDataPos, date, Data de altera��o do embarque
@type function
/*/
Static Function sendMail( clNumPO, dlDataAnt, dlDataPos )

	local alArea		:= getArea()
	local clAssunto		:= "[SESTINI - EMBARQUE] - Altera��o de data de disponibilidade"
	local clMensagem	:= ""
	local clEmailTo		:= ""
	local clEmailCc		:= ""
	local cErro			:= ""

	//------------------------------------------------------------------------------
	// Busca os e-mails envolvidos na notifica��o de altera��o de data de embarque -
	//------------------------------------------------------------------------------
	clEmailTo := getMailsNt()
	
	//-------------------------------------------------
	// Monta corpo da mensagem de email a ser enviado -
	//-------------------------------------------------
	clMensagem := buildMsg( clNumPO, dlDataAnt, dlDataPos )

	U_ADMVSNdMail( clAssunto, clMensagem, {}, clEmailTo, clEmailCc, @cErro , .F. , .T. )

	restArea( alArea )

Return

Static Function buildMsg( clNumPO, dlDataAnt, dlDataPos )

	local clHtml

	clHtml := "Prezados,"
    clHtml += "<Br></Br>"
    clHtml += "<Br></Br>"
    clHtml += "<Br></Br>"
    clHtml += "Foi realizado altera��o de data do embarque:"
    clHtml += "<Br></Br>"
    clHtml += "<Br></Br>"
    clHtml += "<table border='1'> "
    clHtml += "	<tr> "
    clHtml += "		<th>P.O.</th> "
    clHtml += "		<th>Data Sistema Atual</th> "
    clHtml += "		<th>Nova Data</th> "
    clHtml += "	</tr>"
    clHtml += "	<tr>"
    clHtml += "		<td>" + clNumPO + "</td>"
    clHtml += "		<td>" + dToC( dlDataAnt ) + "</td>"
    clHtml += "		<td>" + dToC( dlDataPos ) + "</td>"
    clHtml += "	</tr> "
    clHtml += " </table> "
    clHtml += "<Br></Br>"
    clHtml += "<Br></Br>"
    clHtml += "Equipe de Importa��es Sestini"
    clHtml += "<Br></Br>"
    clHtml += "<Br></Br>"

Return clHtml

/*/{Protheus.doc} getMailsNt
Responsavel por buscar os e-mail envolvidos na notifica��o de altera��o de data de embarque
@author DS2U (SDA)
@since 20/12/2018
@version 1.0
@return clEmailTo, E-mails dos envolvidos na notifica��o de altera��o de data de embarque

@type function
/*/
Static Function getMailsNt()

	local alArea		:= getArea()
	local clEmailTo		:= ""
	
	dbSelectArea( "ZCN" )
	ZCN->( dbGoTop() )
	
	while ( .not. ZCN->( eof() ) )
		
		clEmailTo += allTrim( ZCN->ZCN_EMAIL ) + ";"
		
		ZCN->( dbSkip() )
	endDo
	
	clEmailTo := subs( clEmailTo, 1, len( clEmailTo ) - 1 )
	
	restArea( alArea )

Return clEmailTo

/*/{Protheus.doc} getPcByPO
Consulta os pedidos de compras envolvidos no PO
@author DS2U (SDA)
@since 16/10/2018
@version 1.0
@return alPCs, Array as seguintes informa��es:	alPCs[1] = Codigo do pedido de compras
												alPCs[2] = Codigo do item do pedido de compras
@param clPO, characters, Codigo do PO
@type function
/*/
Static Function getPcByPO( clPO )

	local clAlias	:= getNextAlias()
	local alPCs		:= {}
	
	default clPO		:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			DISTINCT
			C7_NUM AS PEDCOM
			, C7_ITEM AS ITPEDCOM
			
		FROM 
			%TABLE:SW2% SW2 
			
		INNER JOIN 
			SW3010 SW3 ON
			SW3.W3_FILIAL = SW2.W2_FILIAL	
			AND SW3.W3_PO_NUM = SW2.W2_PO_NUM
			AND SW3.W3_TEC = ' '
			AND SW3.%NOTDEL% 
		
		INNER JOIN 
			%TABLE:SW0% SW0 ON
			SW0.W0_FILIAL = SW3.W3_FILIAL	
			AND SW0.W0__NUM = SW3.W3_SI_NUM	
			AND SW0.%NOTDEL% 
		
		INNER JOIN 
			%TABLE:SC1% SC1 ON
			SC1.C1_FILIAL = SW0.W0_FILIAL	
			AND SC1.C1_NUM = SW0.W0_C1_NUM
			AND SC1.C1_PRODUTO = SW3.W3_COD_I
			AND SC1.%NOTDEL% 
		
		INNER JOIN 
			%TABLE:SC7% SC7 ON
			SC7.C7_FILIAL = SC1.C1_FILIAL
			AND SC7.C7_NUMSC = SC1.C1_NUM
			AND SC7.C7_ITEMSC = SC1.C1_ITEM
			AND SC7.%NOTDEL% 
		
		WHERE 
			SW2.W2_FILIAL = %XFILIAL:SW2%
			AND SW2.W2_PO_NUM = %EXP:clPO%
			AND SW2.%NOTDEL% 
		
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		AADD( alPCs, { ( clAlias )->PEDCOM, ( clAlias )->ITPEDCOM } )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )

Return alPCs

/*/{Protheus.doc} preVld
Valida digitacao de campos antes de efetivar a troca de conteudo
@author DS2U (SDA)
@since 03/09/2018
@version 1.0
@return llRet, Se .T., permite a digita��o do campo da linha posicionada, se .F., n�o permite
@param olModel, object, Modelo de dados do MVC
@type function
/*/
Static Function preVld( olModel )

	local nlOpc			:= olModel:getOperation()
	local olModelSW2	:= olModel:getModel('SW2MASTER' )
	local olModelZCK	:= olModel:getModel('ZCKDETAIL')
	local llRet			:= .T.
	local clField		:= readvar()
	
	//-------------------------------------------------------------
	// Nao � permitido alterar linhas que nao s�o da data de hoje -
	//-------------------------------------------------------------
	if ( .not. olModel:getLine() == olModel:length() )
	
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZCK_DTORPO", "N�o � permitido edi��o de itens do hist�rico!" } )
	
	//-------------------------------------------------------------
	// Nao � permitido alterar linhas que nao s�o da data de hoje -
	//-------------------------------------------------------------
	elseif ( .not. empty( clField ) .and. ( "ZCK_DTORPO" $ clField ).and. .not. empty( &clField ) .and. .not. &clField == date() )
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZCK_DTORPO", "N�o � permitido edi��o de itens anterior a data atual!" } )
	endif
	
Return llRet