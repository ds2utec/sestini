#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"
#Include "Totvs.Ch"

//-----------------------------------------------
/*/{Protheus.doc} ADMVA08
Cadastros Usuarios Permiss�o Libera��o Bloqueios 
ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVA08()

	Local aCoors 		:= FWGetDialogSize(oMainWnd)
	Local cTitle		:= "Cadastros de Usuarios com Permiss�o para Libera��o de Bloqueios ADMV"
	
	Private oBrowse		:= NIL
	Private oDlg		:= NIL
	
	If U_FVLA2Usr( __cUserId )
		
		dbSelectArea('ZC9')
		ZC9->(dbSetOrder( 1 ))
			
		DEFINE MSDIALOG oDlg Title cTitle From aCoors[1], aCoors[2] To aCoors[3], aCoors[4] PIXEL
		
			oBrowse:= FWMBrowse():New()
			oBrowse:SetOwner(oDlg)
			oBrowse:SetDescription(cTitle) 
			oBrowse:SetMenuDef('ADMVA08')
			oBrowse:SetAlias('ZC9')
			oBrowse:Alias('ZC9')
			oBrowse:ForceQuitButton()
			oBrowse:Activate()
			
		ACTIVATE MSDIALOG oDlg CENTER

	EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} ModelDef
Model do cadastro Usuarios Permiss�o Libera��o 
Bloqueios ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ModelDef()
	
	Local oStruZC9	:= FWFormStruct(1,'ZC9')
	Local oModel	:= MPFormModel():New( 'ADMV8MVC',,, )
	
	oModel:SetDescription("Cadastros de Usuarios com Permiss�o para Libera��o de Bloqueios ADMV")
	
	oModel:AddFields( 'ZC9MASTER', , oStruZC9 )

	oModel:SetPrimaryKey({"ZC9_CODUSE"})

Return oModel

//-----------------------------------------------
/*/{Protheus.doc} viewdef
View do cadastro Usuarios Permiss�o Libera��o 
Bloqueios ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function viewdef()
	
	Local oModel		:= FWLoadModel("ADMVA08")
	Local oView			:= FWFormView():New()
	Local oStruZC9		:= FWFormStruct(2,'ZC9' )
	
	oView:SetModel( oModel )
	
	oView:AddField('VIEW_ZC9'	,oStruZC9	,'ZC9MASTER')
	
	oView:CreateHorizontalBox('CABEC'	,100)
	oView:SetOwnerView('VIEW_ZC9'	,'CABEC' 	)
	
	oView:SetCloseOnOk({||.T.})

Return oView

//-----------------------------------------------
/*/{Protheus.doc} viewdef
Menu da rotina de cadastro Usuarios Permiss�o 
Libera��o Bloqueios ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function MenuDef()

	Local aRotina	:= {}

	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.ADMVA08" OPERATION MODEL_OPERATION_VIEW	 ACCESS 0
	ADD OPTION aRotina TITLE "Incluir"    ACTION "VIEWDEF.ADMVA08" OPERATION MODEL_OPERATION_INSERT ACCESS 0
	ADD OPTION aRotina TITLE "Alterar"    ACTION "VIEWDEF.ADMVA08" OPERATION MODEL_OPERATION_UPDATE ACCESS 0
	ADD OPTION aRotina TITLE "Excluir"    ACTION "VIEWDEF.ADMVA08" OPERATION MODEL_OPERATION_DELETE ACCESS 0

Return aRotina

//-----------------------------------------------
/*/{Protheus.doc} FVUsrADV
Valida��o dos usu�rios tem permissao de acessar
os Par�metros ADMV e se pode liberar os
bloqueios dos pedidos de venda

@author DS2U (HFA)
@since 10/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FVUsrADV( cUserAcess , clRotina , nOpcLib )

	Local lRet			:= .F.

	Default clRotina	:= ""
	Default nOpcLib		:= 0

	dbSelectArea("ZC9")
	ZC9->( dbSetOrder(1 ) )//ZC9_FILIAL+ZC9_CODUSE
	If ZC9->( dbSeek( FWxFilial("ZC9") + cUserAcess ) )

		If Alltrim(ZC9->ZC9_TPACES) == "3" .Or. Alltrim(ZC9->ZC9_TPACES) == clRotina

			lRet := .T.

		Else

			Help(,, "FVUsrADV_02",, "Sem Permiss�o de acesso a rotina.", 1, 0)

		EndIf

		// Permiss�es para libera��es do Pedido de Venda
		If lRet .And. clRotina == "2"

			If nOpcLib == 1		// Libera Bloqueio Credito

				If ZC9->ZC9_LCREDI <> "1"
					lRet := .F.
					Help(,, "FVUsrADV_03",, "Sem Permiss�o para Liberar Bloqueio Credito.", 1, 0)
				EndIf

			ElseIf nOpcLib == 2	// Libera Bloqueio de Alcada

				If ZC9->ZC9_LALCAD <> "1"
					lRet := .F.
					Help(,, "FVUsrADV_04",, "Sem Permiss�o para Liberar Bloqueio de Alcada.", 1, 0)
				EndIf

			ElseIf nOpcLib == 3	// Libera Bloqueio Comercial

				If ZC9->ZC9_LCOMER <> "1"
					lRet := .F.
					Help(,, "FVUsrADV_05",, "Sem Permiss�o para Liberar Bloqueio Comercial.", 1, 0)
				EndIf

			ElseIf nOpcLib == 4	// Libera Bloqueio Estoque

				If ZC9->ZC9_LCOMER <> "1"
					lRet := .F.
					Help(,, "FVUsrADV_06",, "Sem Permiss�o para Liberar Bloqueio Estoque .", 1, 0)
				EndIf

			EndIf

		EndIf

	Else

		Help(,, "FVUsrADV_01",, "Usu�rio n�o cadastrado para acessos do ADMV.", 1, 0)

	EndIf

Return lRet
