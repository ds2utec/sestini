#INCLUDE 'PROTHEUS.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TOTVS.CH"

//-----------------------------------------------
/*/{Protheus.doc} ADMVA02
Rotina de cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVA02()

	Local cCodigo	:= "000001"
	Local nOperation:= 0
	Local cTitle	:= "Parametriza��es ADMV"

	dbSelectArea('ZC6')
	ZC6->(dbSetOrder( 1 ))
	If ZC6->( dbSeek( FWxFilial("ZC6") + cCodigo ) )
		nOperation := MODEL_OPERATION_UPDATE
	Else
		nOperation := MODEL_OPERATION_INSERT
	EndIf

    If U_FVUsrADV( __cUserId , "1" , Nil  )

        CursorWait()

        FWExecView( cTitle ,"ADMVA02",nOperation,,{|| .T.})

        CursorArrow()

    EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} ModelDef
Model do cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ModelDef()

	Local oStruZC6	:= FWFormStruct(1,'ZC6')
	Local oStruZCL1	:= FWFormStruct(1,'ZCL')
	Local oStruZCL2	:= FWFormStruct(1,'ZCL')
	Local oStruZCL3	:= FWFormStruct(1,'ZCL')
	Local oStruZCL4	:= FWFormStruct(1,'ZCL')
	Local oStruZCL5	:= FWFormStruct(1,'ZCL')
	Local oStruZCL6	:= FWFormStruct(1,'ZCL')
	Local oModel	:= MPFormModel():New( 'ADMV2MVC',,, )

	oModel:SetDescription("Parametriza��es ADMV")

	oModel:AddFields( 'ZC6MASTER', , oStruZC6 )
	
	oModel:SetPrimaryKey({"ZC6_CODIGO"})

	oStruZCL1:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'1'") )
	oStruZCL1:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("1")' ) )
	oModel:AddGrid("ZCLCDPGALC", "ZC6MASTER",oStruZCL1 )
	oModel:SetRelation('ZCLCDPGALC',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLCDPGALC'):SetLoadFilter( { {'ZCL_TIPO', "'1'" } } )
	oModel:GetModel('ZCLCDPGALC' ):SetDescription( "Cond. Pg. Antecipado" )
	oModel:GetModel('ZCLCDPGALC'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLCDPGALC'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLCDPGALC'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLCDPGALC'):SetOptional( .T. )

	oStruZCL2:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'2'") )
	oStruZCL2:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("2")' ) )
	oModel:AddGrid("ZCLMAILBISB", "ZC6MASTER",oStruZCL2 )
	oModel:SetRelation('ZCLMAILBISB',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLMAILBISB'):SetLoadFilter( { {'ZCL_TIPO', "'2'" } } )
	oModel:GetModel('ZCLMAILBISB' ):SetDescription( "Email Bi. Sb" )
	oModel:GetModel('ZCLMAILBISB'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLMAILBISB'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLMAILBISB'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLMAILBISB'):SetOptional( .T. )
	

	oStruZCL3:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'3'") )
	oStruZCL3:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("3")' ) )
	oModel:AddGrid("ZCLCDPGCRED", "ZC6MASTER",oStruZCL3 )
	oModel:SetRelation('ZCLCDPGCRED',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLCDPGCRED'):SetLoadFilter( { {'ZCL_TIPO', "'3'" } } )
	oModel:GetModel('ZCLCDPGCRED' ):SetDescription( "Cond Pg Blq Credito" )
	oModel:GetModel('ZCLCDPGCRED'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLCDPGCRED'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLCDPGCRED'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLCDPGCRED'):SetOptional( .T. )


	oStruZCL4:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'4'") )
	oStruZCL4:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("4")' ) )
	oModel:AddGrid("ZCLNATCRED", "ZC6MASTER",oStruZCL4 )
	oModel:SetRelation('ZCLNATCRED',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLNATCRED'):SetLoadFilter( { {'ZCL_TIPO', "'4'" } } )
	oModel:GetModel('ZCLNATCRED' ):SetDescription( "Natureza Credito" )
	oModel:GetModel('ZCLNATCRED'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLNATCRED'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLNATCRED'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLNATCRED'):SetOptional( .T. )

	oStruZCL5:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'5'") )
	oStruZCL5:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("5")' ) )
	oModel:AddGrid("ZCLNATDEB", "ZC6MASTER",oStruZCL5 )
	oModel:SetRelation('ZCLNATDEB',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLNATDEB'):SetLoadFilter( { {'ZCL_TIPO', "'5'" } } )
	oModel:GetModel('ZCLNATDEB' ):SetDescription( "Natureza Credito" )
	oModel:GetModel('ZCLNATDEB'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLNATDEB'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLNATDEB'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLNATDEB'):SetOptional( .T. )


	oStruZCL6:SetProperty('ZCL_TIPO'	, MODEL_FIELD_INIT	, FwBuildFeature(STRUCT_FEATURE_INIPAD,"'6'") )
	oStruZCL6:SetProperty('ZCL_CONTEU'	, MODEL_FIELD_VALID	, FWBuildFeature(STRUCT_FEATURE_VALID,'U_FVLDZCLC("6")' ) )
	oModel:AddGrid("ZCLCANAL", "ZC6MASTER",oStruZCL6 )
	oModel:SetRelation('ZCLCANAL',{{'ZCL_FILIAL','FWxFilial("ZCL")'},{'ZCL_CODIGO','ZC6_CODIGO'} },ZCL->(IndexKey(1)))
	oModel:GetModel('ZCLCANAL'):SetLoadFilter( { {'ZCL_TIPO', "'6'" } } )
	oModel:GetModel('ZCLCANAL' ):SetDescription( "Canal" )
	oModel:GetModel('ZCLCANAL'):SetNoInsertLine(.F.)
	oModel:GetModel('ZCLCANAL'):SetNoDeleteLine(.F.)
	oModel:GetModel('ZCLCANAL'):SetNoUpdateLine(.F.)
	oModel:GetModel('ZCLCANAL'):SetOptional( .T. )
	
Return oModel

//-----------------------------------------------
/*/{Protheus.doc} viewdef
View do cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function viewdef()

	Local oModel		:= FWLoadModel("ADMVA02")
	Local oView			:= FWFormView():New()
	Local oStruZC61		:= FWFormStruct(2,'ZC6',{|cCampo| GetSx3Cache(AllTrim(cCampo),"X3_FOLDER") == '1' })
	Local oStruZC62		:= FWFormStruct(2,'ZC6',{|cCampo| GetSx3Cache(AllTrim(cCampo),"X3_FOLDER") == '2' })
	Local oStruZC63		:= FWFormStruct(2,'ZC6',{|cCampo| GetSx3Cache(AllTrim(cCampo),"X3_FOLDER") == '3' })
	Local oStruZCL1		:= FWFormStruct(2,'ZCL' )
	Local oStruZCL2		:= FWFormStruct(2,'ZCL' )
	Local oStruZCL3		:= FWFormStruct(2,'ZCL' )
	Local oStruZCL4		:= FWFormStruct(2,'ZCL' )
	Local oStruZCL5		:= FWFormStruct(2,'ZCL' )
	Local oStruZCL6		:= FWFormStruct(2,'ZCL' )
 
	oView:SetModel( oModel )

	oView:AddField('VIEW_ZC61'	,oStruZC61	,'ZC6MASTER')
	oView:AddField('VIEW_ZC62'	,oStruZC62	,'ZC6MASTER')
	oView:AddField('VIEW_ZC63'	,oStruZC63	,'ZC6MASTER')
	
	oView:aViews[1][3]:oStructView:AFolders := {}
	oView:aViews[2][3]:oStructView:AFolders := {}
	oView:aViews[3][3]:oStructView:AFolders := {}
	
	oStruZC61:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAALO == "1" )
	oStruZC62:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABACOM == "1" )
	oStruZC63:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAFIN == "1" )
	
	oStruZCL1:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABACOM == "1" )
	oStruZCL6:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABACOM == "1" )
	
	oStruZCL2:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAFIN == "1" )
	oStruZCL3:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAFIN == "1" )
	oStruZCL4:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAFIN == "1" )
	oStruZCL5:SetProperty("*", MVC_VIEW_CANCHANGE, ZC9->ZC9_ABAFIN == "1" )
	
		
	oView:AddGrid('VIEW_ZCL1'	,oStruZCL1	,'ZCLCDPGALC')
	oView:AddGrid('VIEW_ZCL2'	,oStruZCL2	,'ZCLMAILBISB')
	oView:AddGrid('VIEW_ZCL3'	,oStruZCL3	,'ZCLCDPGCRED')
	oView:AddGrid('VIEW_ZCL4'	,oStruZCL4	,'ZCLNATCRED')
	oView:AddGrid('VIEW_ZCL5'	,oStruZCL5	,'ZCLNATDEB')
	oView:AddGrid('VIEW_ZCL6'	,oStruZCL6	,'ZCLCANAL')
	
	oStruZCL1:SetProperty('ZCL_CONTEU'	,MVC_VIEW_LOOKUP, ADMF3ZCL("1"))
	oStruZCL3:SetProperty('ZCL_CONTEU'	,MVC_VIEW_LOOKUP, ADMF3ZCL("3"))
	oStruZCL4:SetProperty('ZCL_CONTEU'	,MVC_VIEW_LOOKUP, ADMF3ZCL("4"))
	oStruZCL5:SetProperty('ZCL_CONTEU'	,MVC_VIEW_LOOKUP, ADMF3ZCL("5"))
	oStruZCL6:SetProperty('ZCL_CONTEU'	,MVC_VIEW_LOOKUP, ADMF3ZCL("6"))
	
	oView:CreateHorizontalBox('PRINCIPAL'	,100)
	oView:CreateFolder('ABASPRI','PRINCIPAL')
	oView:AddSheet('ABASPRI','ALOCACAO'		,"Aloca��es")
	oView:AddSheet('ABASPRI','COMERCIAL'	,"Comercial ADMV")
	oView:AddSheet('ABASPRI','FINANCEIRO'	,"Financeiro")

	// Folder 1
	oView:CreateHorizontalBox('ALOC'	,100,,,'ABASPRI','ALOCACAO')
	oView:SetOwnerView('VIEW_ZC61'	,'ALOC' 	)


	// Folder 2
	oView:CreateHorizontalBox('COMERC1'	,50,,,'ABASPRI','COMERCIAL')
	oView:CreateHorizontalBox('COMERC2'	,50,,,'ABASPRI','COMERCIAL')
	oView:SetOwnerView('VIEW_ZC62'	,'COMERC1' 	)

	oView:CreateFolder('DETCOMER','COMERC2')
	oView:AddSheet('DETCOMER','ITCANAL'		,"Canais")
	oView:AddSheet('DETCOMER','ITCNDPGCM'	,"Condi��o Pagamento Antecipada - Al�ada")
	
	oView:CreateHorizontalBox('DETCANAL'	,100,,,'DETCOMER','ITCANAL')
	oView:SetOwnerView('VIEW_ZCL6'	,'DETCANAL'	)

	oView:CreateHorizontalBox('DETCNDPGAN'	,100,,,'DETCOMER','ITCNDPGCM')
	oView:SetOwnerView('VIEW_ZCL1'	,'DETCNDPGAN'	)


	// Folder 3
	oView:CreateHorizontalBox('CRED1'	,50,,,'ABASPRI','FINANCEIRO')
	oView:CreateHorizontalBox('CRED2'	,50,,,'ABASPRI','FINANCEIRO')
	oView:SetOwnerView('VIEW_ZC63'	,'CRED1' 	)

	oView:CreateFolder('DETCRED','CRED2')
	oView:AddSheet('DETCRED','ITMAIL'		,"e-mail Bi Sb")
	oView:AddSheet('DETCRED','ITCNDPGCRE'	,"Condi��o Pagamento Antecipado ou � Vista")
	oView:AddSheet('DETCRED','ITNATCRED'	,"Natureza Credito")
	oView:AddSheet('DETCRED','ITNATDEB'		,"Natureza Debito")
	
	oView:CreateHorizontalBox('DETMAIL'	,100,,,'DETCRED','ITMAIL')
	oView:SetOwnerView('VIEW_ZCL2'	,'DETMAIL'	)

	oView:CreateHorizontalBox('DETCDCRED'	,100,,,'DETCRED','ITCNDPGCRE')
	oView:SetOwnerView('VIEW_ZCL3'	,'DETCDCRED'	)

	oView:CreateHorizontalBox('DETNATCRE'	,100,,,'DETCRED','ITNATCRED')
	oView:SetOwnerView('VIEW_ZCL4'	,'DETNATCRE'	)


	oView:CreateHorizontalBox('DETNATDEB'	,100,,,'DETCRED','ITNATDEB')
	oView:SetOwnerView('VIEW_ZCL5'	,'DETNATDEB'	)
	
	// Botoes Tela
    oView:AddUserButton("Usu�rios Libera Bloqueios",'CLIPS',{|| U_ADMVA08() })

	oView:SetCloseOnOk({||.T.})

Return oView


//-----------------------------------------------
/*/{Protheus.doc} FVLA2Usr
Valida��o dos usu�rios quem possuem acesso
a rotina, para manuten��o do cadastro de 
parametriza��es ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FVLA2Usr( cUserAcess )
	
	Local lRet 		:= .F.
	Local cUsers	:= Alltrim(GetMV("ES_USCADMV",,"")) // ADMV - Usuarios com permiss�o de cadastro dos par�metros ADMV

	If cUserAcess $ cUsers
		lRet := .T.
	Else
		Help(,, "FVLA2USR",, "Usu�rio sem permiss�o para acessar a rotina. Entre em contato com o departamento de TI, para verifica��o do par�metro ES_USCADMV.", 1, 0)
	EndIf
	
Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FVLDZCLC
Valida��o no preenchimento de Grid, de acordo
com a aba

@author DS2U (HFA)
@since 14/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FVLDZCLC( cTipo )
	
	Local lRet		:= .T.
	Local cConteudo	:= &(Readvar())
	
	If cTipo == "1"
		lRet := ExistCpo("SE4",cConteudo)
	
	ElseIf cTipo == "2"
		
		If !Empty( cConteudo ) .And. !("@" $ cConteudo)
		
			lRet := .F.
			Help(,, "FVLDZCLC_01",, "e-mail Invalido.", 1, 0)
		
		EndIf
		
	ElseIf cTipo == "3"
	
		lRet := ExistCpo("SE4",cConteudo)
	
	ElseIf cTipo == "4"
	
		lRet := ExistCpo("SED",cConteudo)
	
	ElseIf cTipo == "5"
	
		lRet := ExistCpo("SED",cConteudo)
	
	ElseIf cTipo == "6"
	
		lRet := ExistCpo("ZC0",cConteudo,2) 
	
	EndIf

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADMF3ZCL
Consulta Padr�o no preenchimento de Grid, de acordo
com a aba

@author DS2U (HFA)
@since 14/09/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADMF3ZCL( cTipo )

	Local cRet		:= ""

	If cTipo == "1"

		cRet := "SE4"
	
	ElseIf cTipo == "3"

		cRet := "SE4"
	
	ElseIf cTipo == "4"

		cRet := "SED"
	
	ElseIf cTipo == "5"

		cRet := "SED"

	ElseIf cTipo == "6"

		cRet := "ZC0SA1"
	
	EndIf

Return cRet
