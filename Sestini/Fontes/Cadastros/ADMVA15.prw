#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA15
Rotina de cadastro de Grupo de e-mails que receber�o a notifica��o de altera��o de embarque
@author DS2U (SDA)
@since 18/04/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA15( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			uRet := browse( uParam )
			
		case nlOpc == 1
			uRet := validEmail( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 17/09/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= ZCN->( getArea() )

	private oBrowse

	dbSelectArea("ZCN")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZCN")
	oBrowse:SetDescription("Grupo de e-mails para notifica��o de altera��o de embarque")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 17/09/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA15"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA15"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA15"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA15"	OPERATION 5 ACCESS 0 // "Excluir" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 17/09/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZCN	:= FWFormStruct( 2, 'ZCN' )
	local olModel	:= FWLoadModel( 'ADMVA15' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZCN', olStruZCN, 'ZCNMASTER' )
	
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZCN', 'SUPERIOR' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 17/09/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZCN	:= FWFormStruct( 1, 'ZCN', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA15M', /* bPreValidacao*/,/*bPosValidacao{ |olModel| admvTudoOk( olModel ) }*/ ,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZCNMASTER', /*cOwner*/, olStruZCN, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZCN_FILIAL", "ZCN_EMAIL"} )
	
	olModel:SetDescription('Grupo de E-mail')
	olModel:GetModel('ZCNMASTER' ):SetDescription('Grupo de E-mail')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

/*/{Protheus.doc} validEmail
Responsavel por validar o email digitado
@author DS2U (SDA)
@since 17/09/2018
@version 1.0
@return llRet, Se .T., e-mail valido. Se .F., email invalido!

@type function
/*/
Static Function validEmail()

	local clEmail	:= allTrim( &( readVar() ) )
	local llRet		:= .T.
	
	if ( .not. isEmail( clEmail ) )
		U_ADMVXFUN( 1, {"ZCN_EMAIL", "E-mail inv�lido!" } )
	endif

Return llRet