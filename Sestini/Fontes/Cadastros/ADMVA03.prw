#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

// *** DEMA  

/*/{Protheus.doc} ADMVA03
Cadastro de UF x Regi�o x Frete
@author DS2U (SDA)
@since 24/06/2018
@version 1.0

@type function
/*/
User Function ADMVA03()
	
	local alArea	:= ZC1->( getArea() )

	private oBrowse

	dbSelectArea("ZC1")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC1")
	oBrowse:SetDescription("Cadastro de UF x Regi�o x Frete")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA03"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA03"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA03"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA03"	OPERATION 5 ACCESS 0 // "Excluir" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC1	:= FWFormStruct( 2, 'ZC1' )
	local olModel	:= FWLoadModel( 'ADMVA03' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC1', olStruZC1, 'ZC1MASTER' )
	
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC1', 'SUPERIOR' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC1	:= FWFormStruct( 1, 'ZC1', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA03M', /* bPreValidacao*/,  { |olModel| FTudoOk( olModel ) } ,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC1MASTER', /*cOwner*/, olStruZC1, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC1_FILIAL", "ZC1_UF"} )
	olModel:GetModel('ZC1MASTER'):SetPrimaryKey( { "ZC1_UF", "ZC1_REGIAO" , "ZC1_TPFRET" } )
	
	olModel:SetDescription('UF x Regi�o x Frete')
	
	olModel:GetModel('ZC1MASTER' ):SetDescription('UF x Regi�o x Frete')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel




/*/{Protheus.doc} admvTudoOk
Responsavel por realizar validacoes e processamentos no commit do MVC
@author DS2U (Dema)
@since 07/05/2018
@version 1.0
@type function
/*/
Static Function FTudoOk( olModel )

	local alArea	:= getArea()
	local lRet		:= .T.
	local olModelZC1:= olModel:getModel('ZC1MASTER')
	local nlOpc		:= olModel:getOperation()
	
	if ( nlOpc == 3 ) .or. ( nlOpc == 4 )
	
		//If !( lRet := !( ExistCpo("ZC1" , M->ZC1_UF +  M->ZC1_REGIAO +  M->ZC1_TPFRET  )  ) )
		If !( lRet := !( ExistCpo("ZC1" , M->ZC1_UF + M->ZC1_TPFRET  )  ) ) 
			Aviso("ATEN��O - Inclus�o ou Altera��o n�o permitida." , "J� existe Frete cadastrado para esta UF + Regi�o + Tipo de Frete " + CRLF + CRLF + "Verifique e tente novamente."  , {"Ok"} , 2 )
		Endif 
		
	endif

	restArea( alArea )

Return lRet