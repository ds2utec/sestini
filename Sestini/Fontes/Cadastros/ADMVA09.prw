#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA09
Rotina de cadastro de Cotas por item do pedido de compras
@author DS2U (SDA)
@since 25/07/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA09( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 1
			cotaByItem()
			
	endCase

Return uRet

/*/{Protheus.doc} cotaByItem
Responsavel por executar a view da entidade de cotas por item do pedido de compras
@author DS2U (SDA)
@since 06/08/2018
@version 1.0

@type function
/*/
Static Function cotaByItem()
	fwExecView("Cotas por Item", 'ADMVA09', MODEL_OPERATION_INSERT,, { || .T. } )
Return

/*/{Protheus.doc} ModelDef
Modeldef do cadastro de Cotas por Produto
@author sergi
@since 02/08/2018
@version 1.0
@return olModel, Objeto de model do cadastro

@type function
/*/
Static Function ModelDef()

	local olModel
	local oStruSB1	:= FWFormStruct( 1, 'SB1', { |x| allTrim( x ) $ 'B1_COD,B1_DESC' }, /*lViewUsado*/ )
	local oStruZCA	:= FWFormStruct( 1, 'ZCA', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA09M', /*{ |olModel| cotaPreVld( olModel ) }*//* bPreValidacao*/,{ |olModel| cotaTdOk( olModel ) }/*bPosValidacao*/, { |olModel| cotaCommit( olModel ) }/*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'SB1MASTER', /*cOwner*/, oStruSB1, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'ZCADETAIL', 'SB1MASTER', oStruZCA, /*{|olModel,  nLine,cAction,  cField| PRR13LPre(olModel, nLine, cAction, cField)}*/ /*bLinePre*/, /*{ |olModel| ZCAVldLPos( olModel ) }*/ /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'ZCADETAIL', { { 'ZCA_FILIAL', 'xFilial( "ZCA" )' }, { 'ZCA_PROD', 'B1_COD' } }, ZCA->( IndexKey( 1 ) ) )
	
	olModel:SetDescription('Cadastro de Cotas x Item Pedido de Compra')
	olModel:getModel('SB1MASTER' ):SetDescription('Cadastro de Cotas x Item Pedido de Compra') 
	
	olModel:getModel('ZCADETAIL'):SetOptional( .F. )
	olModel:getModel('ZCADETAIL'):SetUniqueLine({"ZCA_CANAL"})
	
	olModel:SetActivate( {|olModel| cotaLoad( olModel ) } )

Return olModel

/*/{Protheus.doc} ViewDef
Viewdef do cadastro de cotas por produto
@author sergi
@since 02/08/2018
@version 1.0
@return olView, Objeto de view do cadastro

@type function
/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local oStruSB1	:= FWFormStruct( 2, 'SB1', { |x| allTrim( x ) $ 'B1_COD,B1_DESC' }, /*lViewUsado*/ )
	local oStruZCA	:= FWFormStruct( 2, 'ZCA' )
	
	// Cria a estrutura a ser usada na View
	local olModel    := FWLoadModel( 'ADMVA09' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_SB1', oStruSB1, 'SB1MASTER' )		
		
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_ZCA', oStruZCA, 'ZCADETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 20 )
	olView:CreateHorizontalBox( 'INFERIOR', 80 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_SB1', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_ZCA', 'INFERIOR' )
	
	olView:EnableTitleView( 'VIEW_ZCA', 'Cotas x Canal' )
	olView:SetViewProperty("VIEW_SB1", "ONLYVIEW")
	olView:HideFolder("VIEW_SB1",1,1)
	olView:SetNoDeleteLine( "VIEW_ZCA" )

Return olView

/*/{Protheus.doc} cotaTdOk
Responsavel por validar todo o cadastro antes de enviar para a gra��o no banco de dados
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@return llRet, Se .T., cadastro validado, se .F. houve falha no preenchimento do cadastro
@param olModel, object, Modelo de dados do cadastro de cotas por produto
@type function
/*/
Static Function cotaTdOk( olModel )

	local llRet			:= .T.
	local nlx
	local nlSumPerc		:= 0
	local olModelZCA	:= olModel:getModel('ZCADETAIL')

	// Valida se o total do percentual distribuido nao ultrapassou 100%
	for nlx := 1 to olModelZCA:length()
			
		olModelZCA:goLine( nlx )
		
		nlSumPerc += olModelZCA:getValue( "ZCA_COTA" )
		
		if ( nlSumPerc > 100 )
			llRet := .F.
			exit
		endif
		
	next nlx
	
	if ( .not. llRet )
		U_ADMVXFUN( 1,	{"ZCA_COTA", "A soma dos percentuais entre os canais ultrapassou 100%. Por favor, revise o cadastro!" } )
	endif

Return llRet

/*/{Protheus.doc} cotaCommit
Responsavel por realizar a gravacao no banco de dados
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return llRet, Se .T. validacao OK, se nao houve falha no processamento
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function cotaCommit( olModel )

	local llRet			:= .T.
	local nlx
	local olModelZCA	:= olModel:getModel('ZCADETAIL')
	local nlPosItPC		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C7_ITEM" } )
	local nlPosPrd		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C7_PRODUTO" } )

	// Exclui do banco de dados os registro vinculados ao produto / canal para considerar apenas os novos registros
	if ( tcSqlExec( "DELETE FROM " + retSqlName("ZCA") + " WHERE D_E_L_E_T_ = ' ' AND ZCA_PROD = '" + aCols[N][nlPosPrd] + "' AND ZCA_PEDCOM = '" + cA120Num + "' AND ZCA_ITEMPC = '" + aCols[N][nlPosItPC] + "' " ) < 0 )
		
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZCA_PROD", "Nao foi possivel excluir os canais vinculados ao produto [" + allTrim( SB1->B1_COD ) + "]" + CRLF + tcSqlError() } )
		
	else
	
		llRet := .T.
		
		for nlx := 1 to olModelZCA:length()
			
			olModelZCA:goLine( nlx )
			
			if ( recLock( "ZCA", .T. ) )
			
				ZCA->ZCA_FILIAL	:= xFilial("ZCA")
				ZCA->ZCA_PROD	:= aCols[N][nlPosPrd]
				ZCA->ZCA_CANAL	:= olModelZCA:getValue( "ZCA_CANAL" )
				ZCA->ZCA_COTA	:= olModelZCA:getValue( "ZCA_COTA" )
				ZCA->ZCA_PEDCOM	:= cA120Num
				ZCA->ZCA_ITEMPC	:= aCols[N][nlPosItPC]
			
				ZCA->( msUnLock() )
				
			endif
			
		next nlx
	
	endif

Return llRet

/*/{Protheus.doc} cotaLoad
Carrega a grid de itens com os canais do ADMV
@author DS2U ( SDA )
@since 01/08/2018
@version 1.0
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function cotaLoad( olModel )

	local alArea		:= getArea()
	local nlOpc			:= olModel:getOperation()
	local olModelSB1	:= olModel:getModel('SB1MASTER' )
	local olModelZCA	:= olModel:getModel('ZCADETAIL')
	local alCanais		:= {}
	local nlx
	local nly
	local alInfoZCA		:= {}
	local clField		:= ""
	local clContent		:= ""
	local llFound		:= .F.
	local nlLine
	local olStruct
	local nlLinAdd		:= 0
	local nlPosItPC		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C7_ITEM" } )
	local nlPosPrd		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C7_PRODUTO" } )
	
	if ( nlOpc == 3 .or. nlOpc == 4 )
	
		dbSelectArea("ZCA")
		ZCA->( dbSetOrder( 1 ) ) // ZCA_FILIAL, ZCA_PEDCOM, ZCA_ITEMPC, ZCA_PROD, ZCA_CANAL
		
		dbSelectArea("SB1")
		SB1->( dbSetOrder( 1 ) )
		
		if ( SB1->( dbSeek( xFilial("SB1") + aCols[N][nlPosPrd] ) ) )
	
			olStruct	:= olModelZCA:getStruct()
			alInfoZCA	:= olStruct:getFields()
		
			olModelSB1:loadValue("B1_COD"	, SB1->B1_COD )
			olModelSB1:loadValue("B1_DESC"	, SB1->B1_DESC )
		
			alCanais	:= getCanais( .T. ) // Adiciona apenas os canais 1� linha, pois diferente disso, nao controla cotas
			
			// Ponteiro para varrer os canais do ADMV
			for nlx := 1 to len( alCanais )
			
				llFound := ZCA->( dbSeek( xFilial("ZCA") + cA120Num + aCols[N][nlPosItPC] + SB1->B1_COD + alCanais[nlx][1] ) )
				nlLinAdd++
	
				if ( nlOpc == 3 .or. ( nlOpc == 4 .and. .not. llFound ) )
			
					if (  nlLinAdd > 1 .or. nlOpc == 4 )
						nlLine := olModelZCA:AddLine()
						olModelZCA:goLine( nlLine )
					endif
					
					// Ponteiro para varrer os campos da tabela ZCA e preencher as informacoes na grid conforme os canais existentes
					for nly := 1 to len( alInfoZCA )
					
						clField		:= allTrim( alInfoZCA[nly][3] )
						clContent	:= ""
					
						do Case
						
							case clField == "ZCA_FILIAL"
							
								if ( llFound )
									clContent	:= ZCA->ZCA_FILIAL
								else
									clContent	:= xFilial("ZCA")
								endif
							
							case clField == "ZCA_PROD"
							
								if ( llFound )
									clContent	:= ZCA->ZCA_PROD
								else
									clContent	:= SB1->B1_COD
								endif
								
							case clField == "ZCA_CANAL"
							
								if ( llFound )
									clContent	:= ZCA->ZCA_CANAL
								else
									clContent	:= alCanais[nlx][1]
								endif
								
							case clField == "ZCA_DESCAN"
							
								if ( llFound )
									clContent	:= posicione("ZC0", 1, xFilial("ZC0") + ZC8->ZC8_CANAL, "ZC0_DESCAN" )
								else
									clContent	:= alCanais[nlx][2]
								endif
							
							case clField == "ZCA_PEDCOM"
							
								clContent	:= cA120Num
								
							case clField == "ZCA_ITEMPC"
							
								clContent	:= aCols[N][nlPosItPC]
							
							case clField == "ZCA_COTA"
							
								if ( llFound )
									clContent	:= ZCA->ZCA_COTA
								else
									clContent	:= 0
								endif
						
						endCase
						
						if ( .not. empty( clContent ) )
							olModelZCA:loadValue( clField, clContent )
						endif
					
					next nly
					
				endif
					
			next nlx
			
		endif
	
	endif
	
	restArea( alArea )

Return .T.

/*/{Protheus.doc} getCanais
Responsavel por tratar os canais do ADMV
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return U_ADMVXFUN( 4 ), Retorno da funcao U_ADMVXFUN( 4 )
@param llOnlyPLin, boolean, Busca canais somente de primeira linha
@type function
/*/
Static Function getCanais( llOnlyPLin )
Return U_ADMVXFUN( 4, llOnlyPLin )