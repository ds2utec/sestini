#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA05
Cadastro de Promo��es
@author DS2U (SDA)
@since 24/06/2018
@version 1.0

@type function
/*/
User Function ADMVA05( nlOpc, uParam )

	default nlOpc	:= 0
	default uParam	:= nil
	
	do Case
	
		case nlOpc == 0
			uRet := browse( uParam )
			
		case nlOpc == 1
			uRet := importCsv( uParam )
			
		case nlOpc == 2
			uRet := showPromo( uParam )
			
		case nlOpc == 3
			uRet := vldPrcEsp( uParam )
			
		case nlOpc == 4
			uRet := vldC6promo( uParam )
			
		case nlOpc == 5
			uRet := allPromoC6( uParam )
	
		case nlOpc == 6
			uRet := clearPrPro( uParam )

		case nlOpc == 7
			uRet := setPrcTb( uParam )
			
		case nlOpc == 8
			uRet := clearPromo( uParam )
			
		case nlOpc == 9
			uRet := ChangeVal( uParam )
			
		case nlOpc == 10
			uRet := setValtab( uParam )
			
		endCase
	
Return uRet

/*/{Protheus.doc} showPromo
Funcao para mostrar o cadastro de promocoes

@author DS2U (SDA)
@since 30/07/2018
@version 1.0
@param clIdPromo, characters, ID da Promocao
@type function
/*/
Static Function showPromo( clIdPromo )

	local alArea	:= getArea()
	
	clIdPromo := PADR( clIdPromo, tamSX3("ZC3_ID")[1] )

	dbSelectArea( "ZC3" )
	ZC3->( dbSetOrder( 1 ) )
	
	if ( ZC3->( dbSeek( xFilial("ZC3") + clIdPromo ) ) )

		FWExecView("Visualizar", 'ADMVA05', MODEL_OPERATION_VIEW,, { || .T. } )
		
	else
	
		U_ADMVXFUN( 1,	{"ZC3_ID", "ID [" + clIdPromo + "] n�o encontrado no cadastro de promo��es! " } )
	
	endif
	
	restArea( alArea )
	
Return

/*/{Protheus.doc} browse
Configura��o da Browse de cadastro
@author DS2U (SDA)
@since 11/08/2018
@version 1.0

@type function
/*/
Static Function browse()
	
	local alArea	:= ZC3->( getArea() )

	private oBrowse

	setKey( VK_F2 , {|| FwMsgRun(,{|oSay| loadItens( oSay ) } ) } )

	dbSelectArea("ZC3")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC3")
	oBrowse:SetDescription("Cadastro de Promo��es")
	oBrowse:Activate()	
	
	setKey( VK_F2 , {||  })
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"	ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"	ACTION "VIEWDEF.ADMVA05"	OPERATION MODEL_OPERATION_VIEW ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"		ACTION "VIEWDEF.ADMVA05"	OPERATION MODEL_OPERATION_INSERT ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"		ACTION "VIEWDEF.ADMVA05"	OPERATION MODEL_OPERATION_UPDATE ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Importar"	ACTION "U_ADMVA05(1)"		OPERATION MODEL_OPERATION_INSERT ACCESS 0 // "Importar Promo��es"
	ADD OPTION alRotina TITLE 'Imprimir' 	ACTION 'VIEWDEF.ADMVA05' 	OPERATION 8 ACCESS 0
	ADD OPTION alRotina TITLE 'Copiar' 		ACTION 'VIEWDEF.ADMVA05' 	OPERATION 9 ACCESS 0

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC3	:= FWFormStruct( 2, 'ZC3' )
	local olStruZC4	:= FWFormStruct( 2, 'ZC4' )
	local olModel	:= FWLoadModel( 'ADMVA05' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC3', olStruZC3, 'ZC3MASTER' )
	olView:AddGrid( 'VIEW_ZC4', olStruZC4, 'ZC4DETAIL')
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 40 )
	olView:CreateHorizontalBox( 'INFERIOR', 60 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC3', 'SUPERIOR' )
	olView:SetOwnerView('VIEW_ZC4','INFERIOR')
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
	//Adiciona noas a��es
	olView:AddUserButton('Carregar Itens (F2)', 'CLIPS', {|oView| FwMsgRun(,{|oSay| loadItens( oSay, olView ) },"Aguarde...","Carregando itens..." ) } )
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 04/07/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC3	:= FWFormStruct( 1, 'ZC3', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZC4	:= FWFormStruct( 1, 'ZC4', /*bAvalCampo*/, /*lViewUsado*/ )
	local alRelZC4	:= {}
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA05M', /* bPreValidacao*/,/*bPosValidacao*/,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC3MASTER', /*cOwner*/, olStruZC3, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	olModel:AddGrid( 'ZC4DETAIL', 'ZC3MASTER', olStruZC4,/*bLinePre*/, /*bLinePost*/,/*bPre - Grid Inteiro*/,/*bPos - Grid Inteiro*/,/*bLoad - Carga do modelo manualmente*/)
	
	 // Fazendo o relacionamento entre o Pai e Filho
    AADD( alRelZC4, {'ZC4_FILIAL', 'xFilial( "ZC4" )'} )
    AADD( alRelZC4, {'ZC4_ID', 'ZC3_ID'} )
	
	olModel:SetRelation('ZC4DETAIL', alRelZC4, ZC4->( indexKey( 1 ) ) )
    olModel:GetModel('ZC4DETAIL'):SetUniqueLine({"ZC4_FILIAL","ZC4_PROD"} )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC3_FILIAL", "ZC3_ID"} )
	
	olModel:SetDescription('Promo��es')
	olModel:GetModel('ZC3MASTER' ):SetDescription('Promo��es')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

/*/{Protheus.doc} loadItens
Responsavel por carregar os itens das promo��es
@author DS2U (SDA)
@since 11/08/2018
@version 1.0
@param oSay, object, objeto fwMsgRun, util para enviar mensagem ao usuario atraves da interface de aguarde
@param olView, object, objeto da view MVC
@type function
/*/
Static Function loadItens( oSay, olView )

	local olModelZC3:= nil
	local olModelZC4:= nil
	local alArea	:= getArea()
	local clTab100	:= PADR( getTab100(), tamSX3("DA1_CODTAB")[1] )
	local llProcOk	:= .T.
	local llClear	:= .F.
	local nlLin		:= 1
	local clAlias
	
	default oSay	:= nil
	default olView	:= fwViewActive()
	
	olModelZC3	:= olView:getModel("ZC3MASTER")
	olModelZC4	:= olView:getModel("ZC4DETAIL")
	
	if ( olModelZC4:length() > 0 .and. .not. empty( olModelZC4:getValue( "ZC4_PROD" ) ) )
		llClear := msgYesNo( "J� existe itens no cadastro. Deseja refazer ?" )
	endif
	
	if ( llProcOk .or. llClear )
	
		clAlias := getItens( clTab100, olModelZC3:getValue( "ZC3_GRPROD" ), olModelZC3:getValue( "ZC3_COLPRD" ) )
		
		if ( .not. empty( clAlias ) )
		
			if ( llClear )
			
				if ( olModelZC4:canClearData() )
					olModelZC4:clearData()
				else
					llProcOk := .F.
					U_ADMVXFUN( 1,	{"ZC4_PROD", "Nao � poss�vel excluir os itens. Crie um novo cadastro e bloqueie este!" } )
				endif
			
			endif
		
			if ( llProcOk )
		
				while (	.not. ( clAlias )->( eof() ) )
				 	  
			 		olModelZC4:addLine()
			 		olModelZC4:goLine( nlLin++ )
			 		olModelZC4:loadValue( "ZC4_ITEM", ( clAlias )->ITEM )
			 		olModelZC4:loadValue( "ZC4_PROD", ( clAlias )->PRODUTO )
			 		olModelZC4:loadValue( "ZC4_DESPRO", ( clAlias )->DESCPRD )
			 		olModelZC4:loadValue( "ZC4_PRCORI", ( clAlias )->PRECO )
			 		olModelZC4:loadValue( "ZC4_PRCESP", ( clAlias )->PRECO )
			 		olModelZC4:loadValue( "ZC4_DESCON", 0 )
			 		olModelZC4:loadValue( "ZC4_PTROCA", "N" )
			 		olModelZC4:loadValue( "ZC4_PERDES", 0 )
					 	 
				 	( clAlias )->( dbSkip() )
				endDo
				
			endif
			
			( clAlias )->( dbCloseArea() )
			
			olModelZC4:goLine( 1 )
			olView:Refresh( 'VIEW_ZC4' )
			
		endif

	endif

	restArea( alArea )

Return

/*/{Protheus.doc} vldItens
Validando itens do cadastro
@author DS2U (SDA)
@since 11/08/2018
@version 1.0
@return llRet, Validando todos os itens do cadastro
@param olModel, object, Objeto de Modelo de dados 
@type function
/*/
Static Function vldItens( olModel )

	local llRet			:= .T.
	local nlx
	
	for nlx := 1 to olModel:getModel("ZC4DETAIL"):length()
		
		olModel:getModel("ZC4DETAIL"):goLine( nlx )
		
		if ( .not. vldPrcEsp( olModel ) )
			llRet := .F.
			exit
		endif
		
	next nlx

Return llRet

/*/{Protheus.doc} getItens
Responsavel por consultar os itens a serem carregados na interface
@author DS2U (SDA)
@since 11/08/2018
@version 1.0
@return clAlias, Alias da tabela temporaria da consulta de itens a serem carregados para a interface de promo��es
@param clTab100, characters, Codigo da tabela 100
@param [clCodGrp], characters, Codigo do grupo de produtos (Categoria)
@param [clCodCol], characters, Codigo da cole��o
@type function
/*/
Static Function getItens( clTab100, clCodGrp, clCodCol )
	
	local clAlias		:= getNextAlias()
	local clSQL			:= ""
	
	default clTab100	:= ""
	default clCodGrp	:= ""
	default clCodCol	:= ""
	
	clSQL := "SELECT "
	clSQL += "	B1_COD AS PRODUTO "
	clSQL += "	, B1_DESC AS DESCPRD "
	clSQL += "	, DA1_PRCVEN AS PRECO "
	clSQL += "	, DA1_ITEM AS ITEM "
		
	clSQL += "FROM "
	clSQL += retSqlTab("DA1")
		
	clSQL += " INNER JOIN "
	clSQL += "	" + retSqlTab("SB1") + " ON "
	clSQL += "	SB1.B1_FILIAL = '" + fwxFilial("SB1") + "' "
	clSQL += "	AND SB1.B1_COD = DA1.DA1_CODPRO "
	clSQL += "	AND SB1.D_E_L_E_T_ = ' ' "
	
	if ( .not. empty( clCodGrp ) )
	
		clSQL += " INNER JOIN "
		clSQL += "	" + retSqlTab("SZP") + " ON "
		clSQL += "	SZP.ZP_FILIAL = '" + fwxFilial("SZP") + "' "
		clSQL += "	AND SZP.ZP_GRUPO = '" + clCodGrp + "' "
		clSQL += "	AND SZP.ZP_CODPRO = SB1.B1_COD  "
		clSQL += "	AND SZP.D_E_L_E_T_ = ' ' "
	
	endif
	
	if ( .not. empty( clCodCol ) )
	
		clSQL += " INNER JOIN "
		clSQL += "	" + retSqlTab("SZD") + " ON "
		clSQL += "	SZD.ZD_FILIAL = '" + fwxFilial("SZD") + "' "
		clSQL += "	AND SZD.ZD_CODIGO = '" + clCodCol + "' "
		clSQL += "	AND SZD.ZD_CODIGO = SB1.B1_XCOLEC "
		clSQL += "	AND SZD.D_E_L_E_T_ = ' ' "
	
	endif
	
	clSQL += "WHERE "
	clSQL += "	DA1.DA1_FILIAL = '" + fwxFilial("DA1") + "' "
	clSQL += "	AND DA1.DA1_CODTAB = '" + clTab100 + "' "
	clSQL += "	AND DA1.D_E_L_E_T_ = ' ' "
	
	dbUseArea( .T., "TOPCONN", tcGenQry(,,clSQL), clAlias, .F., .T. )
			
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
Return clAlias

/*/{Protheus.doc} getTab100
Resposavel por retornar o codigo da tabela de preco base (100)
@author DS2U (SDA)
@since 10/08/2018
@version 1.0
@return clTab, Codigo da tabela base (100)

@type function
/*/
Static Function getTab100()
Return U_ADMVXFUN( 6, "ZC6_TABBAS" )

/*/{Protheus.doc} vldPrcEsp
Responsavel por validar o campo de pre�o especial
@author DS2U (SDA)
@since 11/08/2018
@version 1.0
@return llRet, Se .T., digitacao do campo esta OK, Se .F., houve falha no dado digitado
@param olModel, object, Objeto deo modelo de dados MVC
@type function
/*/
Static Function vldPrcEsp( olModel )

	local llRet		:= .T.
	local clMsg		:= ""

	default olModel	:= fwModelActive()
	
	olModel := olModel:getModel( "ZC4DETAIL" )
	
	// Preco deve ser positivo
	if ( olModel:getValue("ZC4_PRCESP") < 0 )
		llRet := .F.
		clMsg := "Produto [" + allTrim( olModel:getValue("ZC4_PROD") ) + "] - Pre�o deve ser positivo!"
	endif
	
	// Preco nao pode ser maior que preco origem
	if ( olModel:getValue("ZC4_PRCESP") > olModel:getValue("ZC4_PRCORI") )
		llRet := .F.
		clMsg := "Produto [" + allTrim( olModel:getValue("ZC4_PROD") ) + "] - Pre�o [" + allTrim( transform(  olModel:getValue("ZC4_PRCESP"), "999,999,999.99" ) ) + "] deve ser menor ou igual ao pre�o origem [" + allTrim( transform(  olModel:getValue("ZC4_PRCORI"), "999,999,999.99" ) ) + "]"
	endif
	
	if ( .not. llRet )
		U_ADMVXFUN( 1,	{"ZC4_PRCESP", clMsg } )
	endif

Return llRet

/*/{Protheus.doc} vldC6promo
Responsavel por validar o preenchimento do codigo da promocao no item do pedido de vendas
@author DS2U (SDA)
@since 12/08/2018
@version 1.0
@return llRet, Se .T., preenchimento da promocao foi validada, se .F., houve falha no preenchimento / contexto da promo��o

@type function
/*/
Static Function vldC6promo()

	local alArea	:= getArea()
	local llRet		:= .T.
	local clIdPromo	:= &( readVar() )
	local dlEmissao	:= M->C5_EMISSAO
	local clCodCli	:= M->C5_CLIENTE
	local clLojaCli	:= M->C5_LOJACLI
	local clMsg		:= ""
	local clProduto	:= ""
	local nlPosPrd	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )
	local nlPosLoc	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_LOCAL" } )
	local llExecADMV := GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado
	
if ( llExecADMV )

	if ( .not. empty( clIdPromo ) )
	
		if ( .not. empty( aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XCODKIT" } )] ) )
			
			llRet	:= .F.
			clMsg	:= "N�o � permitido preencher promo��o para KIT!"
			
		elseif ( .not. isInPromo( clIdPromo, aCols[N][nlPosPrd], clCodCli, clLojaCli ) )
		
			llRet	:= .F.
			clMsg	:= "N�o � permitido promo��o para este item. Verique se o mesmo faz parte da promo��o ou se o cliente pode ter promo��o em seus pedidos!"
		
		else
	
			dbSelectArea( "ZC3" )
			ZC3->( dbSetOrder( 1 ) ) // ZC3_FILIAL, ZC3_ID, R_E_C_N_O_, D_E_L_E_T_
			
			dbSelectArea( "ZC4" )
			ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_ID, ZC4_PROD, ZC4_ITEM, R_E_C_N_O_, D_E_L_E_T_
			
			if ( ZC3->( dbSeek( xFilial("ZC3") + clIdPromo ) ) )
			
				if ( ZC3->ZC3_MSBLQL == '1' )
					llRet	:= .F.
					clMsg	:= "Promo��o bloqueada!"
				elseif ( dlEmissao < ZC3->ZC3_VGEMDE .or. dlEmissao > ZC3->ZC3_VGEMAT )
					llRet	:= .F.
					clMsg	:= "Promo��o fora da vig�ncia de emiss�o de pedido!"
				elseif ZC3->ZC3_CANAL <> aCols[N][nlPosLoc]
					llRet	:= .F.
					clMsg	:= "Essa promo��o � exclusiva do canal ["+ Alltrim(ZC3->ZC3_CANAL) +"]. Verifique o armaz�m preenchido."
				else
				
					clProduto	:= aCols[N][nlPosPrd]
				
					if ( empty( clProduto ) )
						llRet	:= .F.
						clMsg	:= "Preencha o produto antes de informar a promo��o!"
					elseif ( .not. ZC4->( dbSeek( xFilial("ZC4") + clIdPromo + clProduto ) ) )
						llRet	:= .F.
						clMsg	:= "Produto n�o identificado na promo��o [" + clIdPromo + "] - " + allTrim( ZC3->ZC3_DESC )
					endif
				
				endif
			
			else
			
				llRet	:= .F.
				clMsg	:= "ID de promo��o [" + clIdPromo + "] n�o existe!"
			
			endif
			
			if ( llRet )
				
				// Preenche informacoes da promocao
				setC6Promo( clIdPromo, ZC4->ZC4_PRCESP, ZC4->ZC4_DESCON, ZC4->ZC4_PERDES, ZC4->ZC4_PTROCA, ZC3->ZC3_PRAZO, "S", "S" )
					
			endif
			
		endif
	
	endif
else
	
	llRet	:= .F.
	clMsg	:= "Processos ADMV desligados. Verifique o parametro ES_ADMVON."

endif

	if ( .not. llRet .and. .not. empty( clMsg ) )
		U_ADMVXFUN( 1,	{"C6_XIDPROM", clMsg } )
	endif
	
	restArea( alArea ) 

Return llRet

/*/{Protheus.doc} setC6Promo
Responsavel por componentizar a alteracao dos campos referente a desconto no item do pedido de venda
@author DS2U (SDA)
@since 31/08/2018
@version 1.0
@param clIdPromo, characters, ID da Promocao
@param nlPrcEsp, numeric, Preco especifico digitado na promo��o
@param nlDesconto, numeric, % de desconto calculado na promo��o
@param nlPercDes, numeric, Percentual de deconto troca prazo desconto
@param clPerTroca, characters, S=Permite troca de prazo por desconto / N=Nao prmite troca de prazo por desconto
@param nlPrazo, numeric, Prazo em dias cadastrado na promocao
@param clUsaPrz, characters, Identifica se usa prazo da promo��o
@param clUsaDes, characters, Identifica se usa desconto da promo��o
@type function
/*/
Static Function setC6Promo( clIdPromo, nlPrcEsp, nlDesconto, nlPercDes, clPerTroca, nlPrazo, clUsaPrz, clUsaDes )

	default clIdPromo	:= ""
	default nlPrcEsp	:= 0
	default nlDesconto	:= 0
	default nlPercDes	:= 0
	default nlPrazo		:= 0
	default clPerTroca	:= "N"
	default clUsaPrz	:= "N"
	default clUsaDes	:= "N"

	setPrPromo( clIdPromo, aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )] )
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XDSCPRM" } )]	:= nlDesconto
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XDSCTRC" } )]	:= nlPercDes
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XPTROCA" } )]	:= clPerTroca
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XUTPRZP" } )]	:= clUsaPrz
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XUTDSCP" } )]	:= clUsaDes
	aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XPRAZOP" } )]	:= nlPrazo

Return

/*/{Protheus.doc} clearPromo
Responsavel por limpar os campos relacionados a promocao
@author DS2U (SDA)
@since 31/08/2018
@version 1.0
@return clPerDsPro, Conteudo digitado no campo C6_XIDPROMO, pois esta funcao foi projetada para executar no gatilho do campo

@type function
/*/
Static Function clearPromo()

	local clIdPromo	:= &( readVar() ) // C6_XIDPROMO

	if ( empty( clIdPromo ) .and. msgYesNo( "Deseja remover a promo��o deste item ?" ) )
		setC6Promo() // Sem parametros � feito o preenchimento padrao, sem informa��es da promo��o
	endif

Return clIdPromo

/*/{Protheus.doc} clearPrPro
Responsavel por preencher o preco da promocao via gatilho
@author sergi
@since 31/08/2018
@version 1.0
@return clPerDsPro, Retorna o conteudo do readvar, pois a funcao foi projetada para executar via gatilho

@type function
/*/
Static Function clearPrPro()

	local clPerDsPro	:= &( readVar() ) // C6_XUTDSCP
	
	if ( clPerDsPro == "N" )
		setPrcTb()
	elseif ( clPerDsPro == "S" )
		setPrPromo( aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XIDPROM" } )], aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )] )
	endif
	
Return clPerDsPro

/*/{Protheus.doc} setPrcTb
Responsavel por preencher o pre�o conforme a tabela de pre�os
@author DS2U (SDA)
@since 31/08/2018admin	
@version 1.0

@type function
/*/
Static Function setPrcTb()

	local alArea	:= getArea()
	local clCodTabPr:= M->C5_TABELA
	local nlPosPrd
	local nlPosPrV	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRCVEN" } )
	local nlPosPrL	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRUNIT" } )
	local nlPreco	:= 0
	
	if ( .not. empty( clCodTabPr ) )
	
		nlPosPrd	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )
	
		dbSelectArea( "DA1" )
		DA1->( dbSetOrder( 1 ) ) // DA1_FILIAL, DA1_CODTAB, DA1_CODPRO, DA1_INDLOT, DA1_ITEM, R_E_C_N_O_, D_E_L_E_T_
		
		if ( DA1->( dbSeek( fwxFilial("DA1") + clCodTabPr + aCols[N][nlPosPrd] ) ) )
	
			nlPreco	:= DA1->DA1_PRCVEN
			
		endif
			
	endif
	
	aCols[N][nlPosPrV]	:= nlPreco
	aCols[N][nlPosPrL]	:= nlPreco
		
	restArea( alArea )
	
Return

/*/{Protheus.doc} setPrPromo
Responsavel por gatilhar o preco da promocao
@author sergi
@since 31/08/2018
@version 1.0
@param clIdPromo, characters, ID da promo��o
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function setPrPromo( clIdPromo, clProduto )

	local alArea		:= getArea()
	local nlPosPrV		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRCVEN" } )
	local nlPosPrL		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRUNIT" } )
	local nlPosQtVen	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_QTDVEN" } )
	local nlPosTotal	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_VALOR" } )
	local nlPreco		:= 0
	
	if ( .not. empty( clIdPromo ) .and. .not. empty( clProduto ) )
	
		dbSelectArea( "ZC4" )
		ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_ID, ZC4_PROD, ZC4_ITEM, R_E_C_N_O_, D_E_L_E_T_
		
		if ( ZC4->( dbSeek( fwxFilial("ZC4") + clIdPromo + clProduto ) ) )
		
			 nlPreco := ZC4->ZC4_PRCESP
		
		endif
	
	endif

	aCols[N][nlPosPrV]	 := nlPreco
	aCols[N][nlPosPrL]	 := nlPreco
	aCols[N][nlPosTotal] := (nlPreco * aCols[N][nlPosQtVen])

	restArea( alArea )

Return

/*/{Protheus.doc} importCsv
Responsavel por importar o cadastro de promocoes
@author DS2U (SDA)
@since 17/08/2018
@version 1.0

@type function
/*/
Static Function importCsv()

	local clFunc	:= "ADMVA05"
	local clTitle	:= "Importa��o de Promo��es"
	local clDesc	:= "Este programa tem por objetivo realizar a importa��o de promo��es"
	local clPerg	:= "ADMVA05"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execImp( oSelf, MV_PAR01 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} execImp
Responsavel por importar Cotas por Produto baseado em um arquivo CSV
@author sergi
@since 02/08/2018
@version 1.0
@param oSelf, object, objeto do tNewProcess para controle de status
@param clFile, characters, caminho do arquivo CSV a ser lido para a importacao
@type function
/*/
Static Function execImp( oSelf, clFile )

	local nlHandle	:= FOPEN(clFile , FO_READWRITE + FO_SHARED )
	local nlTotLin	:= 0
	local nlCont	:= 0
	local clString	:= ""
	local clLinha	:= ""
	local nlQtdBytes:= 5000
	local nlBitLidos:= 0  
	local nlBits	:= 0
	local nlLinIni	:= 1
	local alDados	:= {}
	local llNoFound	:= .F.
	local llCanCota	:= .F.
	local nlTamPrd	:= tamSx3("ZC4_PROD")[1]
	local nlTamCan	:= tamSx3("ZC3_CANAL")[1]
	local nlTamDes	:= tamSx3("ZC3_DESC")[1]
	local nlPreco	:= 0
	local nlPrcOri	:= 0
	local clProduto	:= ""
	local clCanal	:= ""
	local llFail	:= .F.
	local clTab100	:= ""
	local dlDtVEDe	:= cToD("//")
	local dlDtVEAte	:= cToD("//")
	local dlDtVFDe	:= cToD("//")
	local dlDtVFAte	:= cToD("//")
	local nlPrazo	:= 0
	local alPromocs	:= {}
	local alPromocao:= {}
	local nlx
	local nly
	local nlLinha	:= 0
	local clItmTbPrc:= ""
	local clDesPromo:= ""
	local clDesc	:= ""
	local clPerChang:= "N"
	local nlDescon	:= 0
	local nlDescPrz	:= 0
	
	if ( ferror() <> 0 )
		MsgAlert("Erro de abertura do arquivo: " + str( ferror() ) )
	else
	
		dbSelectArea( "ZC3" )
		ZC3->( dbSetOrder( 2 ) ) // ZC3_FILIAL, ZC3_DESC, ZC3_CANAL, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "ZC4" )
		ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_ID, ZC4_PROD, ZC4_ITEM, R_E_C_N_O_, D_E_L_E_T_

		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		dbSelectArea( "DA1" )
		DA1->( dbSetOrder( 1 ) ) // DA1_FILIAL, DA1_CODTAB, DA1_CODPRO, DA1_INDLOT, DA1_ITEM, R_E_C_N_O_, D_E_L_E_T_
	
		nlTotLin := fSeek( nlHandle, 0, 2 )	// tamenho maximo de uma variavel  1048576		
		fSeek( nlHandle, 0, 0 )				// Posiciona no Inicio do Arquivo
		
		clTab100 := PADR( getTab100(), tamSX3("DA1_CODTAB")[1] )
		
		oSelf:setRegua1( 2 )
		oSelf:incRegua1( "Lendo arquivo" )
		
		while nlBitLidos <= nlTotLin
		
			nlCont++
		
			clString	:= FReadStr(nlHandle, nlQtdBytes)  
			nlBits		:= (At( CHR(13)+CHR(10),clString ))
			
			if ( nlBits == 0 )
				nlBits := Len( AllTrim( clString ) ) // Se o a ultima linha do arquivo nao conter os caractes chr(13) + chr(10) entao pego a quantidade de caracteres lidos
			endif
			
			if ( nlBits > 0 )
			
				nlBits += 1
				
				if ( nlCont == 1 )
					oSelf:setRegua2( int( nlTotLin / nlBits ) )
				endif				
				
				if ( nlCont > nlLinIni )
				
					fSeek( nlHandle, nlBitLidos, 0)				  
					clLinha	:= AllTrim( FReadStr(nlHandle, nlBits) )
					
					if ( .not. empty( clLinha ) )				
						
						alDados := separa( clLinha, ";", .T. )
						
						if ( len( alDados ) > 1 )
						
							clCanal		:= PADR( alDados[1], nlTamCan )
							clDesPromo	:= PADR( alDados[2], nlTamDes )
							clProduto	:= PADR( alDados[3], nlTamPrd )
							nlPreco		:= val( alDados[4] )
							dlDtVEDe	:= cToD( allTrim( alDados[5] ) )
							dlDtVEAte	:= cToD( allTrim( alDados[6] ) )
							dlDtVFDe	:= cToD( allTrim( alDados[7] ) )
							dlDtVFAte	:= cToD( allTrim( alDados[8] ) )
							nlPrazo		:= val( alDados[9] )
							clPerChang	:= alDados[10]
							nlDescPrz	:= val( alDados[11] )
							
							if ( DA1->( dbSeek( xFilial( "DA1" ) + clTab100 + clProduto ) ) )
							
								nlDescon	:= ( nlPreco / DA1->DA1_PRCVEN ) * 100
							
								if ( ( nlPos := aScan( alPromocs, {|x| x[1] == clCanal .and. x[2] == clDesPromo .and. x[3] == clProduto } ) ) == 0 )
									AADD( alPromocs, { clCanal, clDesPromo, clProduto, DA1->DA1_PRCVEN, nlPreco, nlDescon, clPerChang, nlDescPrz, DA1->DA1_ITEM, dlDtVEDe, dlDtVEAte, dlDtVFDe, dlDtVFAte, nlPrazo, nlCont } )
								else
									alPromocs[nlPos][5]		:= nlPreco
									alPromocs[nlPos][10]	:= dlDtVEDe
									alPromocs[nlPos][11]	:= dlDtVEAte
									alPromocs[nlPos][12]	:= dlDtVFDe
									alPromocs[nlPos][13]	:= dlDtVFAte
									alPromocs[nlPos][14]	:= nlPrazo
								endif
								
							endif
							
						else
							
							llFail := .T.
							oSelf:incRegua2("")
							oSelf:saveLog( "N�o foi possivel ler corretamente a linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
														
						endif
										
						alDados := {}
						
					endif
					
				endif
				
				nlBitLidos	+= nlBits
				FSeek( nlHandle, nlBitLidos, 0)
				
			else
				nlBitLidos	+= 1
			endif
		
		endDo
		
		oSelf:incRegua1( "Validando dados..." )
		
		if ( .not. llFail )
		
			// Ordena por Descricao da Promocao + Canal
			aSort( alPromocs,,, { |x,y| x[2] + x[1] < y[2] + y[1] } )
		
			for nlx := 1 to len( alPromocs )
			
				clCanal		:= alPromocs[nlx][1]
				clDesPromo	:= alPromocs[nlx][2]
				clProduto	:= alPromocs[nlx][3]
				nlPrcOri	:= alPromocs[nlx][4]
				nlPreco		:= alPromocs[nlx][5]
				nlDescon	:= alPromocs[nlx][6]
				clPerChang	:= alPromocs[nlx][7]
				nlDescPrz	:= alPromocs[nlx][8]
				clItmTbPrc	:= alPromocs[nlx][9]
				dlDtVEDe	:= alPromocs[nlx][10]
				dlDtVEAte	:= alPromocs[nlx][11]
				dlDtVFDe	:= alPromocs[nlx][12]
				dlDtVFAte	:= alPromocs[nlx][13]
				nlPrazo		:= alPromocs[nlx][14]
				nlLinha		:= alPromocs[nlx][15]
				
				if ( nlPreco <= 0 )
					
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "Pre�o deve ser positivo! Promo��o [" + allTrim( clDesPromo ) + "] / Produto: [" + clProduto + "] / Pre�o: " + allTrim( transform( nlPreco, "@E 999,999,999.99" ) ) + ".  Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada" )
					
				elseif ( nlDescPrz < 0 )
					
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "% Prazo em Desconto n�o deve ser positivo.  Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada" )
					
				elseif ( clPerChang == "S" .and. nlDescPrz <= 0 )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "% Prazo em Desconto n�o foi preenchido para item que permite troca.  Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada" )
				
				elseif ( .not. SB1->( dbSeek( xFilial( "SB1" ) + clProduto ) ) )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "N�o foi encontrado o Produto [" + clProduto + "]. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
					
				elseif ( .not. DA1->( dbSeek( xFilial( "DA1" ) + clTab100 + clProduto ) ) )
				
					llFail := .T.
					oSelf:incRegua2("")
					oSelf:saveLog( "N�o foi encontrado o produto [" + clProduto + "] na tabela de pre�os. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
					
				else
				
					if ( nlPreco > DA1->DA1_PRCVEN )
				
						llFail := .T.
						oSelf:incRegua2("")
						oSelf:saveLog( "Pre�o do produto [" + clProduto + "] esta maior que o pre�o da tabela 100. Linha " + allTrim( Str( nlLinha ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
				
					else
				
						clDesc := clDesPromo
					
						// Aglutino os produtos por promocoes
						while ( nlx <= len( alPromocs ) .and. clDesc == alPromocs[nlx][2] )
						
							AADD( alPromocao, alPromocs[nlx] )
							clDesc := alPromocs[nlx][2]
						
							nlx++
						endDo
						nlx--
						
						oSelf:incRegua2("Incluindo Promo��o [" + allTrim( clDesPromo ) + "]")
					
						for nly := 1 to len( alPromocao )
							setPromo( alPromocs[nly][1], alPromocs[nly][2], alPromocs[nly][3], alPromocs[nly][4], alPromocs[nly][5], alPromocs[nly][6], alPromocs[nly][7], alPromocs[nly][8], alPromocs[nly][9], alPromocs[nly][10], alPromocs[nly][11], alPromocs[nly][12], alPromocs[nly][13], alPromocs[nly][14] )
						next nly
							
						alPromocao := {}
						
					endif
					
				endif
				
				if ( llFail )
					exit
				endif
			
			next nlx
		
			oSelf:saveLog( "Arquivo " + allTrim( clFile ) + " processado com sucesso!" )
			
		endif
		
	endif

Return

/*/{Protheus.doc} setPromo
Responsavel por gravar os dados na entidade de promocoes
@author DS2U (SDA)
@since 19/08/2018
@version 1.0
@return llRet, Se .T., gravacao OK. Se .F., gravacao nao ocorreu
@param clCanal, characters, Codigo do canal
@param clDesPromo, characters, Descricao da promocao
@param clProduto, characters, Codigo do produto
@param nlPrcOri, numeric, Preco origem da tabela de pre�os
@param nlPrcEsp, numeric, Pre�o especial digitado pelo usuario
@param nlDescont, numeric, % De desconto calculado pelo sistema
@param clCanChang, characters, Identifica se 'S', pode trocarr prazo por desconto, ou n�o 'N'
@param nlDescPrz, numeric, % Desconto de troca de prazo por de desconto
@param clItmTbPrc, characters, Item da tabela de pre�os a que se refere o item da promo��o
@param dlDtVEDe, date, Data de emiss�o do pedido de venda inicio de vigencia
@param dlDtVEAte, date, Data de emiss�o do pedido de venda final de vigencia
@param dlDtVFDe, date, Data de faturamento do pedido de venda inicio de vigencia
@param dlDtVFAte, date, Data de faturamento do pedido de venda final de vigencia
@param nlPrazo, numeric, Prazo da promo��o
@type function
/*/
Static Function setPromo( clCanal, clDesPromo, clProduto, nlPrcOri, nlPrcEsp, nlDescont, clCanChang, nlDescPrz, clItmTbPrc, dlDtVEDe, dlDtVEAte, dlDtVFDe, dlDtVFAte, nlPrazo )
	
	local alArea	:= getArea()
	local llFoundZC3:= .F.
	local llFoundZC4:= .F.
	local llRet		:= .F.
	local clID		:= ""
	local nlx
	
	dbSelectArea( "ZC3" )
	ZC3->( dbSetOrder( 2 ) ) // ZC3_FILIAL, ZC3_DESC, ZC3_CANAL, R_E_C_N_O_, D_E_L_E_T_
	llFoundZC3 := ZC3->( dbSeek( xFilial("ZC3") + clDesPromo + clCanal ) )
	
	if ( llFoundZC3 )
		
		clID	:= ZC3->ZC3_ID
		
		dbSelectArea( "ZC4" )
		ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_ID, ZC4_PROD, ZC4_ITEM, R_E_C_N_O_, D_E_L_E_T_
		llFoundZC4 := ZC4->( dbSeek( xFilial("ZC4") + clID + clProduto + clItmTbPrc ) )
		
	else
		clID	:= getSXeNum("ZC3", "ZC3_ID")
	endif
	
	begin transaction 

		if ( recLock( "ZC3", .not. llFoundZC3 ) )
							
			ZC3->ZC3_FILIAL	:= xFilial("ZC3")
			ZC3->ZC3_ID		:= clID
			ZC3->ZC3_DESC	:= clDesPromo
			ZC3->ZC3_VGEMDE	:= dlDtVEDe
			ZC3->ZC3_VGEMAT	:= dlDtVEAte
			ZC3->ZC3_VGFTDE	:= dlDtVFDe
			ZC3->ZC3_VGFTAT	:= dlDtVFAte
			ZC3->ZC3_CANAL	:= clCanal
			ZC3->ZC3_MSBLQL	:= '2'
			ZC3->ZC3_PRAZO	:= nlPrazo
			ZC3->( msUnLock() )
			
		endif
		
		if ( recLock( "ZC4", .not. llFoundZC4 ) )
							
			ZC4->ZC4_FILIAL	:= xFilial("ZC4")
			ZC4->ZC4_ID		:= clID
			ZC4->ZC4_ITEM	:= clItmTbPrc
			ZC4->ZC4_PROD	:= clProduto
			ZC4->ZC4_PRCORI := nlPrcOri
			ZC4->ZC4_PRCESP	:= nlPrcEsp
			ZC4->ZC4_DESCON	:= nlDescont
			ZC4->ZC4_PTROCA	:= clCanChang
			ZC4->ZC4_PERDES	:= nlDescPrz
			ZC4->( msUnLock() )
			
		endif
		
		confirmSX8()
		llRet := .T.
		
	end transaction
	
	if ( .not. llRet )
		rollBackSX8()
	endif
		
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de importacao de cotas por produto
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	AADD(aPergunt,{cPerg,"01","Selecione o arquivo  ","MV_CH1","C",99,0,"G","NaoVazio()","MV_PAR01","","","","","","ARQCSV",""})
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return

/*/{Protheus.doc} allPromoC6
Responsavel por preencher o ID da promo��o nos itens do pedido de venda a partir do preenchimento da promo��o no cabe�alho do pedido de vendas
@author DS2U (SDA)
@since 20/08/2018
@version 1.0

@type function
/*/
Static Function allPromoC6()

	local llJob	     := isBlind()
	local llExecADMV := GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado
	local cMsg		 := "Processos ADMV desligados. Verifique o parametro ES_ADMVON."
	
if ( llExecADMV )
	if ( llJob .or. msgYesNo( "Deseja processar promo��es nos itens do pedido ?" ) )
		fwMsgRun(,{|oSay| execProC6( oSay ) },"Aguarde...","Processando promo��es..." )
	endif
else
	U_ADMVXFUN( 1,	{"C5_XIDPROM", cMsg } )
endif

Return .T.

/*/{Protheus.doc} execProC6
Responsavel por executar o processamento de promocoes para os itens do pedido de vendas
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@param oSay, object, objeto da classe fwMsgRun para incluir mensagens a interface com o usuario
@type function
/*/
Static Function execProC6( oSay )

	local alArea		:= getArea()
	local nlPosPrVen	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRCVEN" } )
	local nlPosPrLis	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRUNIT" } )
	local nlPosKit		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XCODKIT" } )
	local nlPosPrd		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )
	local nlPosIdPro	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XIDPROM" } )
	local nlPosQtVen	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_QTDVEN" } )
	local nlPosTotal	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_VALOR" } )
	local nlPosLoc		:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_LOCAL" } )
	local nlPosDel		:= len( aHeader ) + 1
	local clIdPromo		:= M->C5_XIDPROM
	local clCodCli		:= M->C5_CLIENTE
	local clLojaCli		:= M->C5_LOJACLI
	local nlx
	local clMsg			:= ""
	
	default oSay		:= nil
	
	for nlx := 1 to len( aCols )
	
		if ( .not. aCols[nlx][nlPosDel] ) // Se nao esta deletado
	
			if ( empty( aCols[nlx][nlPosKit] ) .and. empty( aCols[nlx][nlPosIdPro] ) ) // Verifica promocoes somente se o item n�o for KIT e se nao estiver preenchido com algum codigo de promocao
				
				if ( isInPromo( clIdPromo, aCols[nlx][nlPosPrd], clCodCli, clLojaCli ) ) .AND. (ZC3->ZC3_CANAL = aCols[nlx][nlPosLoc]) // Checa se o produto esta em promocao e deixa o cadastro posicionado para uso das informa�oes
						aCols[nlx][nlPosIdPro] := clIdPromo
						aCols[nlx][nlPosPrVen] := ZC4->ZC4_PRCESP
						aCols[nlx][nlPosPrLis] := ZC4->ZC4_PRCESP
						aCols[nlx][nlPosTotal] := (ZC4->ZC4_PRCESP * aCols[nlx][nlPosQtVen])
				else
					clMsg += allTrim( aCols[nlx][nlPosPrd] ) + "/"
				endif
			
			endif
			
		endif
		
	next nlx
	
	oGetDad:oBrowse:nAt := 1 
	getDRefresh() // Atualiza getDados da janela atual
	oGetDad:oBrowse:Refresh()
	
	if ( .not. empty( clMsg ) )
	
		clMsg := subs( clMsg, 1, len( clMsg ) - 1 )
		clMsg := "N�o � permitido promo��o para os produtos abaixo:";
					+ CRLF + clMsg + CRLF + CRLF +;
					"Verique possibilidades: " + CRLF +;
					"1)Produtos fora da promo��o selecionada " + CRLF + "2)Cliente n�o autorizado a ter promo��o em seus pedidos" + CRLF +;
					"3)Promo��o exclusiva de um Canal de Vendas diferente do Armaz�m do preenchido." 
		U_ADMVXFUN( 1,	{"C6_XIDPROM", clMsg } ) 
	
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} isInPromo
Responsavel por verificar se o produto enviado por parametro esta em promocao, conforme ID enviado por parametro.
Se o retorno for verdadeiro, o cadastro de promo��o ficar� posicionado. Entidades: ZC3 e ZC4
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return llRet, Se .T., o produto esta em promocao, se .F., n�o esta
@param clIdPromo, characters, ID da promo��o
@param clProduto, characters, Codigo do produto
@param clCodCli, characters, Codigo do cliente do pedido de venda
@param clLojaCli, characters, Codigo da loja do pedido de venda
@type function
/*/
Static Function isInPromo( clIdPromo, clProduto, clCodCli, clLojaCli )

	local alArea	:= getArea()
	local llRet		:= .F.
	local clMsg		:= ""
	
	dbSelectArea( "SA1" )
	SA1->( dbSetOrder( 1 ) )

	if ( empty( clIdPromo ) )
	
		clMsg += "Preencha o c�digo da promo��o!"
	
	elseif ( empty( clProduto ) )
	
		clMsg += "O produto precisa ser informado para processamento de promo��es!"
	
	elseif ( empty( clCodCli ) .or. empty( clLojaCli ) )
	
		clMsg += "O c�digo/loja do cliente precisa ser informado para processamento de promo��es!
	
	elseif ( .not. SA1->( dbSeek( fwxFilial("SA1") + clCodCli + clLojaCli ) ) .or. .not. ( SA1->A1_XPROMO == "S" ) )
	
		clMsg += "N�o est� permitido o uso de promo��es para este cliente!

	else

		dbSelectArea( "ZC3" )
		ZC3->( dbSetOrder( 1 ) ) // ZC3_FILIAL, ZC3_ID, R_E_C_N_O_, D_E_L_E_T_
		if ( ZC3->( dbSeek( xFilial("ZC3") + clIdPromo ) ) )
		
			dbSelectArea( "ZC4" )
			ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_ID, ZC4_PROD, ZC4_ITEM, R_E_C_N_O_, D_E_L_E_T_
			
			if ( ZC4->( dbSeek( xFilial("ZC4") + clIdPromo + clProduto ) ) )
				llRet := .T.
			endif
			
		endif
		
	endif
	
	if ( .not. empty( clMsg ) )
		U_ADMVXFUN( 1,	{"C6_XIDPROM", clMsg } )
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} ChangeVal
Quando alterado o pre�o do produto com promo��o, verifica o pre�o inserido com o cadastro
caso diferente, o usu�rio tem a op��o de retirar o produto espec�fico da promo��o.
@author DS2U (L. Fini)
@since 04/01/2019
@version 1.0
@return nVlrAtu, pre�o de venda do produto.
@param nVlrAtu, pre�o do produto inserido manualmente.
@param cCodProd, Codigo do produto
@param cIdPromo, ID da promo��o
@param nVlrProm, valor do produto cadastrado na promo��o
@type function
/*/
Static Function ChangeVal()

Local nVlrAtu 	:= M->C6_PRCVEN
Local cCodProd	:= GdFieldGet("C6_PRODUTO",n)
Local cIdPromo	:= GdFieldGet("C6_XIDPROM",n)
Local nVlrProm	:= 0
Local llExecADMV := GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado
	
If ( llExecADMV )
	If !Empty(cIdPromo)			
	
		nVlrProm := Posicione("ZC4",2,xFilial("ZC4")+ cIdPromo + cCodProd,"ZC4_PRCESP")
		
		If nVlrProm <> nVlrAtu
			If msgYesNo( "Pre�o inserido est� diferente do cadastro de promo��o. " + CRLF + "Retirar o item da promo��o ?" ) 
				setC6Promo() // Sem parametros � feito o preenchimento padrao, sem informa��es da promo��o
				aCols[N][aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XIDPROM" } )]	:= ""
			Else
				nVlrAtu := nVlrProm
			Endif
		EndIf
		
	EndIf
EndIf

Return nVlrAtu


/*/{Protheus.doc} setValTab
Responsavel por preencher o pre�o conforme a tabela de pre�os
Criar gatilho C6_QTDVEN / C6_QTDVEN / U_ADMVA05(10)
@author DS2U (L. FINI)
@since 23/01/2019	
@version 1.0

@type function
/*/
Static Function setValTab()

local alArea	:= getArea()
local clCodTabPr:= M->C5_TABELA
local cProduto	:= M->C6_PRODUTO
local nlPosPrd
local nlPosPrV	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRCVEN" } )
local nlPosTab	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_XPRCTAB" } )
local nlPosPrL	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRUNIT" } )
local nlPosTot	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_VALOR" } )
local nlPreco	:= 0
Local llExecADMV := GetMv("ES_ADMVON",, .T. ) //Processos ADMV ativado

If ( llExecADMV )

	if ( .not. empty( clCodTabPr ) )
	
		nlPosPrd	:= aScan( aHeader, {|x| allTrim( x[2] ) == "C6_PRODUTO" } )
	
		dbSelectArea( "DA1" )
		DA1->( dbSetOrder( 1 ) ) // DA1_FILIAL, DA1_CODTAB, DA1_CODPRO, DA1_INDLOT, DA1_ITEM, R_E_C_N_O_, D_E_L_E_T_
		
		if ( DA1->( dbSeek( fwxFilial("DA1") + clCodTabPr + aCols[N][nlPosPrd] ) ) )
	
			nlPreco	:= DA1->DA1_PRCVEN
			
		endif
			
	endif
Endif
	
	//aCols[N][nlPosPrV]	:= nlPreco
	aCols[N][nlPosPrL]	:= nlPreco 
	aCols[N][nlPosTab]	:= nlPreco 
		
	restArea( alArea )
	
Return cProduto
