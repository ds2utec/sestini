------------------------------------
-- VIEW VW_TIC_Varejo_Pagtos
-- Auto:  Mauro Paladini
-- Versao: 3.0
-- Descricao:  Exibe informacoes detalhadas referente o pagamento do cupom
-- Esta viu foi nomeada e criada desta forma pq o cliente n�o enviou a estrutura da VIEW da forma esperada por falta de conhecimento tecnico.


CREATE VIEW [dbo].[VW_TIC_Varejo_Pagtos]
AS    

SELECT * FROM
( 

-- PAGAMENTOS COM DINHEIRO
SELECT
	CRE.IdEntidade,
	VEnt.Cnpj,
	VEnt.Nome,
	CRE.Token Token, 
	CRE.Id ID,   
	CRE.IdTerceiro,	
	CRE.DataEmissao,
    CRE.NumeroDocumento,
	CRE.NumeroFiscal, 
	CRE.NumeroSerie, 
    1 AS Parcela,   
	CRE.DataEmissao As DataVencimento,

 CASE WHEN DIN.Dinheiro IS NOT NULL OR DIN.Dinheiro>0 THEN DIN.Dinheiro ELSE 0.00 END Valor,   
 'R$' Forma_Pagto,   
 'DINHEIRO' Administradora_Cartao, 
 	0 AS IdNegociacaoCartoes,
	'' AS Bandeira,
	'DINHEIRO' TipoCartao,   
    '' Numero_Cartao, 
 '' NSU_transa��o,   
 '' Cod_Autoriza��o_Transa��o,   
 '' Agencia,   
 '' Conta_Cheque,   
 '' RG_Cheque,   
 '' Telefone_Cheque,   
 '' Numero_Cheque  
FROM   
 Varejo_Contas_Receber CRE (NOLOCK)   
  LEFT JOIN Varejo_Entidades VEnt ON VEnt.Id = CRE.IdEntidade AND VEnt.Token = CRE.Token  AND VEnt.Ativa = 1
  INNER JOIN Varejo_Contas_Receber_Composicao DIN (NOLOCK) ON (DIN.VarejoContasReceberId=CRE.Id and CRE.Token=DIN.Token) AND (DIN.Dinheiro IS NOT NULL AND DIN.Dinheiro>0)   
WHERE    
 CRE.Token='00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de' and CRE.TipoDocumento='PEDIDO'   

 UNION

-- PAGAMENTOS COM VALE-TROCAS (CREDITOS NCC)
SELECT 
	CRE.IdEntidade,
	VEnt.Cnpj,
	VEnt.Nome,
	CRE.Token Token, 
	CRE.Id ID,   
	CRE.IdTerceiro,
	CRE.DataEmissao, 
    CRE.NumeroDocumento,
	CRE.NumeroFiscal, 
	CRE.NumeroSerie, 
    1 AS Parcela,   
    CRE.DataEmissao as  DataVencimento,

 CASE WHEN CVA.Outros IS NOT NULL OR CVA.Outros>0 THEN CVA.Outros ELSE 0.00 END Valor,   
 'NC' Forma_Pagto,   
 'NCC' Administradora_Cartao, 
 	0 AS IdNegociacaoCartoes,
	'' AS Bandeira,
	'NCC' TipoCartao,   
    '' Numero_Cartao, 
 '' NSU_transa��o,   
 '' Cod_Autoriza��o_Transa��o,   
 '' Agencia,   
 '' Conta_Cheque,   
 '' RG_Cheque,   
 '' Telefone_Cheque,   
 '' Numero_Cheque   
FROM   
 Varejo_Contas_Receber CRE (NOLOCK)   
   LEFT JOIN Varejo_Entidades VEnt ON VEnt.Id = CRE.IdEntidade AND VEnt.Token = CRE.Token  AND VEnt.Ativa = 1
   INNER JOIN Varejo_Contas_Receber_Composicao CVA (NOLOCK) ON (CVA.VarejoContasReceberId=CRE.Id and CRE.Token=CVA.Token AND CVA.Outros IS NOT NULL AND CVA.Outros>0)   
   
WHERE    
 CRE.Token='00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de' and CRE.TipoDocumento='PEDIDO'   

 UNION

SELECT DISTINCT   

	CRE.IdEntidade,
	VEnt.Cnpj,
	VEnt.Nome,
	CRE.Token Token, 
	CRE.Id ID,   
	CRE.IdTerceiro,
	CRE.DataEmissao,
    CRE.NumeroDocumento,
	CRE.NumeroFiscal, 
	CRE.NumeroSerie, 
    CTO.NumeroParcela Parcela,   
	CTO.DataVencimento,
    CTO.ValorBruto Valor,   
   
  CASE CRN.TipoCartao WHEN 'CREDITO' THEN 'CC' WHEN 'DEBITO' THEN 'CD' END Forma_Pagto,   
  
    CASE  
      WHEN CRN.Descricao = 'CIELO - DEBITO' THEN  '001 - CIELO - DEBITO'  
      WHEN CRN.Descricao = 'CIELO - CRED. VISTA' THEN '002 - CIELO - CRED. VISTA'  
      WHEN CRN.Descricao = 'CIELO - CRED. PARCELADO' THEN '003 - CIELO - CRED. PARCELADO'  
      WHEN CRN.Descricao = '1-REDE - DEBITO' THEN '004 - 1-REDE - DEBITO'  
      WHEN CRN.Descricao = '2-REDE - CRED. VISTA' THEN '010 - 2-REDE - CRED. VISTA'  
      WHEN CRN.Descricao = '3-REDE - CRED. PARCELADO' THEN '006 - 3-REDE - CRED. PARCELADO'  
      WHEN CRN.Descricao = 'AMEX - CRED. VISTA' THEN '007 - AMEX - CRED. VISTA'  
      WHEN CRN.Descricao = 'AMEX - CRED. PARCELADO' THEN '008 - AMEX - CRED. PARCELADO'  
      WHEN CRN.Descricao = '4-HIPER - CRED. VISTA' THEN '009 - 4-HIPER - CRED. VISTA'  
      WHEN CRN.Descricao = '5-HIPER - CRED. PARCELADO' THEN '005 - 5-HIPER - CRED. PARCELADO'  
    END Administradora_Cartao,  

	CRN.Id AS IdNegociacaoCartoes,
	CRN.Bandeira,
	CASE CRN.TipoCartao WHEN 'CREDITO' THEN 'CARTAO DE CREDITO' WHEN 'DEBITO' THEN 'CARTAO DE DEBITO AUTOMATICO' END TipoCartao,
    '' Numero_Cartao,   
    CRT.Nsu NSU_transacao,   
   
    CTO.Autorizacao Cod_Autoriza��o_Transacao,   
    '' Agencia,   
    '' Conta_Cheque,   
    '' RG_Cheque,   
    '' Telefone_Cheque,   
    '' Numero_Cheque   
FROM   
 Varejo_Contas_Receber CRE (NOLOCK)   
  LEFT JOIN Varejo_Entidades VEnt ON VEnt.Id = CRE.IdEntidade AND VEnt.Token = CRE.Token  AND VEnt.Ativa = 1
  LEFT JOIN Varejo_Contas_Receber CRE_EXT (NOLOCK) ON CRE.NumeroFiscal=CRE_EXT.NumeroFiscal AND (ISNULL(CRE.NumeroSerie, 'XXX') = ISNULL(CRE_EXT.NumeroSerie, 'XXX'))  AND CRE_EXT.TipoDocumento='PEDIDO'   
  LEFT JOIN Varejo_Contas_Receber_Composicao CCT ON CCT.VarejoContasReceberId=CRE_EXT.Id AND CCT.Token=CRE.Token   
   LEFT JOIN Varejo_Contas_Receber_Composicao_Cartoes_RecebimentoOperadora CTO ON CTO.IdParcelaOperadoraCartao=CRE.Id AND CTO.Token=CRE.Token   
  LEFT JOIN Varejo_Contas_Receber_Composicao_Cartoes CRT ON CRT.VarejoContasReceberComposicaoId=CCT.Id AND CRE_EXT.Token=CCT.Token   
   LEFT JOIN Varejo_Negociacoes_Cartao CRN ON CRN.Id=CRT.IdNegociacao AND CRN.Token=CRT.Token AND CRN.Ativo = 1
WHERE    
 CRE.Token='00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de'    
 AND CRE.TipoDocumento='EXTRATO'

 ) Pagtos 

GO


