CREATE PROCEDURE [dbo].[SP_TIC_LERCUPONS_01] ( 

@FILIAL			VARCHAR(8),
@CNPJFILIAL		VARCHAR(19)  ) AS

DECLARE @iRecno		INTEGER;
DECLARE @IT_iRecno	INTEGER;
DECLARE @PG_iRecno	INTEGER;

DECLARE @DATABASE	VARCHAR(8);
DECLARE @IDCUPOM	INTEGER;
DECLARE @NUMCUPOM	VARCHAR(15);
DECLARE @CHAVECF	VARCHAR(50);
DECLARE @DATACF		VARCHAR(8);
DECLARE @SERIECF	VARCHAR(10)
DECLARE @VLRTOTAL	DECIMAL(14,2);
DECLARE @IDENTCF	VARCHAR(10);
DECLARE @IDTERCDEST	VARCHAR(10);
DECLARE @IDEMP		VARCHAR(10);
DECLARE @ID			VARCHAR(10);
DECLARE @NOMECF		VARCHAR(15);
DECLARE @NOMEFANCF	VARCHAR(15);
DECLARE @PESSOACF	VARCHAR(2);
DECLARE @DOCCLICF	VARCHAR(20);
DECLARE @DTNASCLI	VARCHAR(8);
DECLARE @IECLICF	VARCHAR(10);
DECLARE @TOTALCUP	DECIMAL(14,2);

DECLARE @IT_PROD	VARCHAR(15);
DECLARE @IT_DESCRI	VARCHAR(100)
DECLARE @IT_CFOP	VARCHAR(4);
DECLARE @IT_QUANT	INTEGER;
DECLARE @IT_VLUNIT	DECIMAL(14,2);
DECLARE @IT_VLDESC	DECIMAL(14,2);
DECLARE @IT_VLITEM	DECIMAL(14,2);
DECLARE @IT_ALQICM	DECIMAL(5,2);
DECLARE @IT_VLICM	DECIMAL(14,2);
DECLARE @IT_ALQPIS	DECIMAL(5,2);
DECLARE @IT_VLPIS	DECIMAL(14,2);
DECLARE @IT_IDCF	INTEGER;
DECLARE @IT_ITEM	VARCHAR(2);
DECLARE @IT_NITEM	INTEGER;

DECLARE @CAB_CUPONS		AS CURSOR; 
DECLARE @ITENS_CUPONS	AS CURSOR;

SET @DATABASE = CONVERT( varchar(8), GETDATE() , 112) ;
SET @CAB_CUPONS = CURSOR FOR

-- Query para ler cabe�alho dos cupons � importar

SELECT  TOP 200
		M.Chave,
		CONVERT( varchar(8), M.Data , 112) as Data,
		CAST( M.Serie  AS varchar(10)) As Serie,
		CAST( M.NumeroNotaFiscal  AS varchar(15)) As NumeroNotaFiscal,
		M.ValorTotal,
		CAST( M.IdEntidade  AS varchar(10)) As IdEntidade, 
		CAST( M.IdTerceiroDestinatario  AS varchar(10)) As IdTerceiroDestinatario,
		CAST( V.IdEmpresa AS varchar(10)) As IdEmpresa,
		CAST( M.Id AS varchar(10)) As Id,
		M.Id,
		ISNULL(T.Nome,'') As Nome ,
		ISNULL(T.NomeFantasia ,'') As NomeFantasia,
		ISNULL(T.TipoPessoa ,'') AS TipoPessoa,
		ISNULL(T.Documento ,'') As Documento,
		CONVERT( varchar(8), T.DataNascimento , 112) as DataNascimento,
		T.Ie

FROM [LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_NotasMercadoria]  M  with (NOLOCK)

inner join [LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_Entidades] V with (NOLOCK)
on	M.Token = V.Token 
	And M.IdEntidade = V.Id 
	And V.Token = '00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de' 

left join  [LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_Terceiros] T with (NOLOCK)
on	T.Id = M.IdTerceiroDestinatario 
and T.Token = '00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de' 

where	M.Data >= GETDATE()-30
		And M.Status = 'EMITIDO'                                
		And Replace(M.ModeloDocumentoFiscal,'NOTA_','') = '59' 
		And ( M.PROCESSADOPROTHEUS = '0' or  M.PROCESSADOPROTHEUS is null ) 
		And V.Cnpj = @CNPJFILIAL 

OPEN @CAB_CUPONS

FETCH NEXT FROM @CAB_CUPONS INTO	@CHAVECF, 
									@DATACF, 
									@SERIECF, 
									@NUMCUPOM, 
									@VLRTOTAL,
									@IDENTCF,	
									@IDTERCDEST	,
									@IDEMP,
									@ID,	
									@IDCUPOM,		
									@NOMECF,		
									@NOMEFANCF,	
									@PESSOACF,	
									@DOCCLICF,	
									@DTNASCLI,	
									@IECLICF ;

SELECT @iRecno = IsNull( Max( R_E_C_N_O_ ), 0 )		FROM [SVBD05].[DADOSMP12_TST].[dbo].[P01010] with (NOLOCK);
SELECT @PG_iRecno = IsNull( Max( R_E_C_N_O_ ), 0 )	FROM [SVBD05].[DADOSMP12_TST].[dbo].[P03010] with (NOLOCK);
									
WHILE ( @@FETCH_STATUS ) = 0

BEGIN

	-- Grava registro do cabecalho

	SET @iRecno = @iRecno + 1 ;
	SET @PG_iRecno = @PG_iRecno + 1 ; 

	SET @NUMCUPOM = LTRIM(@NUMCUPOM) ;
	SET @NUMCUPOM = REPLICATE('0', 9 - LEN(@NUMCUPOM)) + RTRIM(@NUMCUPOM)  ; 

	INSERT INTO [SVBD05].[DADOSMP12_TST].[dbo].[P01010] (P01_FILIAL,
						P01_CHAVE,
						P01_DTEMIS,
						P01_DTIMP,
						P01_CNPJCF,
						P01_SATQUP,
						P01_DOC,
						P01_DOCCOO,
						P01_STOTAL,
						P01_IDCF,
						P01_IDDEST,
						P01_NOMECF,
						P01_STATUS,
						R_E_C_N_O_ )

	VALUES				(	@FILIAL,
							@CHAVECF,
							@DATACF, 
							@DATABASE,
							@CNPJFILIAL, 							
							@SERIECF, 
							@NUMCUPOM, 
							@NUMCUPOM, 
							@VLRTOTAL,
							@IDENTCF,	
							@IDTERCDEST	,
							@NOMECF,
							'0',
							@iRecno );

	-- Marca Flag Processado Protheus

	UPDATE [LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_NotasMercadoria]
	SET PROCESSADOPROTHEUS = 1
	WHERE Id = @IDCUPOM ;

	-- Grava Itens do cupom Fiscal

	SET @ITENS_CUPONS = CURSOR 
	FOR 

	SELECT
		CONVERT( varchar(15), p.CodigoSistema , 112) As CodigoSistema,
		LEFT(p.Descricao,100) As DescProduto,
		I.Cfop AS CFOP, 
		I.Quantidade AS Qtde,
		I.ValorUnitario,
		I.ValorDesconto,
		I.ValorTotal,
		I.IcmsValor,
		I.IcmsAliquota,
		I.PisValor, 
		I.PisAliquota

	FROM [LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_NotasMercadoria] M with (NOLOCK), 
	[LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_NotasMercadoriaItens] I with (NOLOCK),
	[LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_Produtos] p with (NOLOCK),
	[LK_TOTVS].[Sestini_Integracoes].[dbo].[Varejo_Entidades] v with (NOLOCK)

	where I.IdNotasMercadoria = M.Id
	And I.IdProduto = p.Id
	And M.IdEntidade = v.Id 
	And p.Token = v.Token
	And p.Ativo = 1 
	AND M.Id = @IDCUPOM 

    OPEN @ITENS_CUPONS

    FETCH NEXT FROM @ITENS_CUPONS INTO	@IT_PROD,
										@IT_DESCRI,
										@IT_CFOP,
										@IT_QUANT,
										@IT_VLUNIT,	
										@IT_VLDESC,
										@IT_VLITEM,
										@IT_VLICM,	
										@IT_ALQICM,	
										@IT_VLPIS,
										@IT_ALQPIS;
									

	SELECT @IT_iRecno = IsNull( Max( R_E_C_N_O_ ), 0 ) FROM [SVBD05].[DADOSMP12_TST].[dbo].[P02010] with (NOLOCK);

	SET @IT_NITEM = 1 ;
	SET @TOTALCUP = 0 ;

    WHILE ( @@FETCH_STATUS = 0 )

    BEGIN
	
		SET @IT_iRecno = @IT_iRecno + 1 ;

		SET @IT_VLITEM	=  @IT_VLUNIT * @IT_QUANT ;
		SET @TOTALCUP	=  @TOTALCUP + @IT_VLITEM ;
		 
		SET @IT_ITEM	= LTRIM( STR( @IT_NITEM  ) );
		SET @IT_ITEM	= REPLICATE('0', 2 - LEN(@IT_ITEM)) + RTRIM(@IT_ITEM)  ; 
		SET @IT_NITEM	= @IT_NITEM + 1 ;

		INSERT INTO [SVBD05].[DADOSMP12_TST].[dbo].[P02010] (	P02_FILIAL,		
													P02_PROD,
													P02_DESCRI,
													P02_CF,
													P02_QUANT,
													P02_PRUNIT,
													P02_DESCIT,
													P02_VALLIQ,
													P02_VLICMS,
													P02_PICM,
													P02_VLPIS,
													P02_PPIS,																										
													P02_DOCCOO,
													P02_DOC,
													P02_DTEMIS,
													P02_DTIMP,
													P02_ITEM,
													P02_STATUS,
													R_E_C_N_O_ )

		VALUES				(	@FILIAL,
								@IT_PROD,
								@IT_DESCRI,
								@IT_CFOP,
								@IT_QUANT,
								@IT_VLUNIT,
								@IT_VLDESC,
								@IT_VLITEM,
								@IT_VLICM,	
								@IT_ALQICM,	
								@IT_VLPIS,
								@IT_ALQPIS,
								@NUMCUPOM,
								@NUMCUPOM, 
								@DATACF,
								@DATABASE,
								@IT_ITEM,
								'0',
								@IT_iRecno );

		FETCH NEXT FROM @ITENS_CUPONS INTO	@IT_PROD,
											@IT_DESCRI,
											@IT_CFOP,
											@IT_QUANT,
											@IT_VLUNIT,	
											@IT_VLDESC,
											@IT_VLITEM,
											@IT_VLICM,	
											@IT_ALQICM,	
											@IT_VLPIS,
											@IT_ALQPIS;
											

	END

	CLOSE @ITENS_CUPONS ;
	DEALLOCATE @ITENS_CUPONS;

	-- Grava meios de pagamento

	INSERT INTO [SVBD05].[DADOSMP12_TST].[dbo].[P03010] (	P03_FILIAL,	
												P03_STATUS,
												P03_DOC,
												P03_DOCCOO,
												P03_EMISSA,
												P03_FORMPG,
												P03_VALOR,
												P03_DTIMP,
												R_E_C_N_O_ )

	VALUES				(	@FILIAL,
							'0',
							@NUMCUPOM,
							@NUMCUPOM,
							@DATACF,
							'DINHEIRO',
							@TOTALCUP,
							@DATABASE,
							@PG_iRecno );

    
	-- Atualiza Cursor CXAB_CUPONS
	FETCH NEXT FROM @CAB_CUPONS INTO	@CHAVECF, 
										@DATACF, 
										@SERIECF, 
										@NUMCUPOM, 
										@VLRTOTAL,
										@IDENTCF,	
										@IDTERCDEST	,
										@IDEMP,
										@ID,	
										@IDCUPOM,		
										@NOMECF,		
										@NOMEFANCF,	
										@PESSOACF,	
										@DOCCLICF,	
										@DTNASCLI,	
										@IECLICF ;	
END

CLOSE @CAB_CUPONS;
DEALLOCATE @CAB_CUPONS;

