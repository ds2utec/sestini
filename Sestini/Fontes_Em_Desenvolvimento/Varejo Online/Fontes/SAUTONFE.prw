#Include 'Protheus.ch'
#include 'tbiconn.ch'

/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnnfe_integration.log' 
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'


/*/{Protheus.doc} SAUTONFE
Fun��o de usu�rio auxiliar que prepara o objeto da FWExecAuto para integra��o.

Utiliza��o -> Gravada no campo PV9_FUNOPC no momento da inser��o do registro pela classe TransactRows()

@type 		User Function
@author 	Mauro Paladini
@since 		Aug 17, 2018  2:33:31 PM
@version 	1.0
@return 	${return}, ${return_description}

/*/

/* 

VARIAVEOS MSEXECAUTO
~~~~~~~~~~~~~~~~~~~~

Array Cabe�alho		->  "aCabAuto"
Array Itens 		->	"aITAuto"

Array PARAMFUN do Objeto deve receber 6 Parametros dentro do Array 

Exemplo:
~~~~~~~~
oFWExecAuto:ParamFun := { 'aCabAuto' , 'aItAuto' , '3' , '' , '' , '' }


Sintaxe:
~~~~~~~~
oFWExecAuto:ParamFun := { <Array Cab> , [Array Itens] , <nOpc> , '' , '' , '' }

*/

User Function SAUTONFE()

Local   aAreas          := { PV1->(GetArea()) , PV2->(GetArea()) , PV4->(GetArea()) , PV9->(GetArea()) , GetArea() }
Local   cFuncAux        := 'U_SAUTONFE'
Local   cIdMapFld       := GetMv('ES_MNFE2',,'0000000002')

Local 	cKeyPV9         := PV9->PV9_SEQ
Local   cOrigem			:= PV9->PV9_ORIGEM

Local   cMapSeek        := xFilial('PV5') + AvKey( cFuncAux , 'PV5_FUNOPC' ) + cIdMapFld
Local   aAux			:= {}
Local   cTipoNF			:= ''

Private cJobLog         := cDirLog + cCFGLog 

MontaDir(cDirLog)

// -- Valdar Mapa de Campos

If Empty ( cIdMapFld ) .OR. PV5->( ! DbSeek( cMapSeek ) )  
    u_WriteLog( cJobLog  , '[' + cFuncAux + '] Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek  ,.T.,.T. )
    Return .F.
Endif


// -- Tratativa para os varios tipos de nota de entrada
// Envia Cliente ou Fornecedor no array 

PV1->( DbSetOrder(2) )
If PV1->( DbSeek( xFilial("PV1") + cKeyPv9 ) ) 
	cTipoNF	:= PV1->PV1_TIPONF
Endif


// -- Instancia o objeto da classe FwExecauto
oFWExecAuto := FwExecAuto():New( 'PV1' , 'PV2' , cOrigem , cKeyPV9 , cFuncAux , cIdMapFld , .T. /*lRestAreas*/ , /*aSaveAlias*/ , .F. /*lBlankFields*/ )
oFWExecAuto:ParamFun := { 'aCabAuto' , 'aItAuto' , '3' , '' , '' , '' }

If cTipoNF $ 'N'
	oFWExecAuto:aNoCabFields := { 'PV1_CLIENT' , 'PV1_CPFCLI' , 'PV1_LJCLI' }
Else
	oFWExecAuto:aNoCabFields := { 'PV1_FORNEC' , 'PV1_CGCFOR' , 'PV1_LJFOR' }
Endif 


// -- Faz o load dos registros para o objeto e executa a Execauto

IF oFWExecAuto:Load()
	oFWExecAuto:Execute()	
Endif

IF !oFWExecAuto:lResult
	u_WriteLog( cJobLog  , '[SAUTONFE] ' + oFWExecAuto:Message ,.T.,.T. )
Endif

AEval(aAreas, {|x| RestArea(x)})

Return { oFWExecAuto:lResult , oFWExecAuto:Message }