#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE COMERRO		"8"
#DEFINE EMPROCESSA	"9"

/*/{Protheus.doc} TibWFGvBatch
Workflow 
Chama a rotina de finaliza��o de cupons fiscais OFF-LINE
Transforma os registros gravados nas tabelas do loja em venda
Utiliza fun��o padr�o LjGrvBatch e conceito ** MONO THREAD ** para o processamento
Nao deve ser utilizado em multiplas threads por causa da concorrencia

SL1 => SF2-Cabecalho de notas fiscais de saida
SL2 => SD2-Itens de notas fiscais de saida
SL4 => SE1-Titulos a Receber

@author Mauro Paladini
@since 12/02/2015
@version 1.0
/*/

user Function TibWFGvBatch(CRPCEmp, cRPCFil)

Local nQtdThread		:= 0 	// ** ESTE PROCESSO DEVE SER MONO THREAD **
Local nQtdRegistros		:= 0
Local nRegsPorThread	:= 0
Local lCriouAmb			:= .F.
Local lRet				:= .T.

Private cLogT			:= "\system\wfgravabatch.log"
Private nLastRow	:= 0 	

If File(cLogT)
	FErase(cLogT)
Endif

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
	RPCSetType(3) 
	RPCSETENV(cRPCEmp, cRPCFil)
	lCriouAmb	:= .T.
Endif 

ChkFile("NNR")	

// MultiThread
If nQtdThread > 0 

	//Obtem a quantidade de cupons a serem gravados como or�amento;
	nQtdRegistros := u_QtdCupom(ORCAMENTO) 
	
	If nQtdRegistros > 0
		
		//Se o n� de registros for maior que a qtd de threads � necess�rio o tratamento abaixo
		If nQtdRegistros > nQtdThread 
		
			nRegsPorThread := Round( (nQtdRegistros/nQtdThread),0 )
			IF !IsBlind()
				FWMsgRun( , {|| u_PackMT( nQtdThread, "u_TibMTBatch", { nRegsPorThread } , "GRV" ) } ,, "Gerando registros ... "  )
			Else
				U_PackMT( nQtdThread, "u_TibMTBatch", { nRegsPorThread } , "GRV" )			
			Endif
						
		//Se o n� de registros for menor ou igual a qtd de threads, sobe s� a qtd necess�ria.
		ElseIf nQtdRegistros <= nQtdThread 
		
			IF !IsBlind()
				FWMsgRun( , {|| U_PackMT( nQtdRegistros, "u_TibMTBatch", {1} , "GRV"  ) } ,, "Gerando registros ... "  )
			Else
				U_PackMT( nQtdRegistros, "u_TibMTBatch", {1} , "GRV" )		
			Endif

		EndIf

	Else
		If !IsBlind()			
			MsgInfo("N�o existem cupons para serem Gerados!")
			lRet := .T.
		Endif
	EndIf

Else

	// Gera os or�amentos como monothread - Processo normal.
	// Obtem a quantidade de cupons a serem validados;

	CONOUT( "Iniciando QtdCupom .. " ) 

	nQtdRegistros := U_QtdCupom(ORCAMENTO) 

	If nQtdRegistros > 0		
		IF !IsBlind()
			FWMsgRun( , {|| u_TibMTBatch( u_DivRegistros(0, "'" + ORCAMENTO + "'" ) ) } ,, "Gerando registros ... "  )
		Else
			u_TibMTBatch( u_DivRegistros(0, "'" + ORCAMENTO + "'") )		
		Endif
	Else
		If !IsBlind()
			MsgInfo("N�o existem cupons para serem Gerando!")
			lRet := .F.
		Endif
	EndIf
	
EndIf

If !IsBlind() .And. lRet 	
	Aviso("Aviso","Processo finalizado",{"OK"},2)
Endif

If lCriouAmb
	RESET ENVIRONMENT
Endif

Return



/*/{Protheus.doc} TibMTBatch
THREAD que efetivamente finaliza a or�amento como venda 

@author Mauro Paladini
@since 06/03/2015
@version 1.0
@param aDados, 		array, Cupons a serem gravados como orcamento
@param aParamMT, 	array, Reservado MT
/*/

User Function TibMTBatch( aDados , aParamMT )

Local aAreas  	:= { 	DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) ,;
						SA1->(GetArea()) , SF4->(GetArea()) , SX5->(GetArea()) , SE1->(GetArea()) ,;
						SA6->(GetArea()) , SLG->(GetArea()) , SLF->(GetArea()) , P01->(GetArea()) ,;
						P02->(GetArea()) , P03->(GetArea()) , SAE->(GetArea()) , SX2->(GetArea()) ,;
						SX3->(GetArea()) , SM0->(GetArea()) , SX6->(GetArea()) , GetArea() }

Local cArq				:= "" 
Local cCupom			:= ""

Local nL				:= 0
Local nX				:= 0
Local cThread			:= IIF( aParamMT == Nil , "singleuser" , aParamMT[1] )

Local cLog				:= ""
Local cLogOP			:= ""
Local aLogOP			:= {}
Local cLogMsg			:= ""

Local cOpObs			:= ""
Local aCupom			:= { "" , "" , 0 , "" }
Local lGravacao			:= .F.
Local lNaoProduziu		:= .F.

Local lEstneg			:= If( GetMv("MV_ESTNEG",,"S") == "S" , .T. ,.F. )
Local cTMPCP			:= GetMv("FS_TMPCP",,"001")
Local cAlias			:= GetNextAlias()

Local cModAnt			:= cModulo
Local nModAnt			:= nModulo 

Local cPDV				:= ""
Local cSerie			:= ""
Local cNumCx			:= ""
Local cCodEstacao		:= ""			

Local tInicio	
Local tFim	

Private dDataBackup	:= dDatabase

Public cLogMT			:= "\log_pdv\grvbatch_thread_" + cThread + ".log"

If Type("cEstacao") == "U"
	Public cEstacao	:= ""
Endif

MakeDir("\log_pdv") 
If File(cLogMt)
	FErase(cLogMT)
Endif

LjWriteLog(cLogMT,"Iniciando LjGrvBatch .. [" + cThread + "] ")

For nX := 1 To Len(aDados)

	aArea			:= Getarea()
	aSave			:= { "P01" , "P02" , "P03" , "SM0" } 
	aAlias			:= u_xGetArea(aSave)
	
	lGravacao		:= .F.
	lNaoProduziu	:= .F.		
	cArq			:= aDados[nX][1] 
	cCupom			:= aDados[nX][2]
	tInicio			:= Time()

	cPDV			:= ""
	cSerie			:= ""
	
	cNumCx			:= ""
	cEstacao		:= ""			
	
	// Posiciona no CUPOM na tabela P01	
	
	P01->( DbSetOrder(6) )
	
	If P01->( DbSeek( xFilial("P01") + cArq + RTrim(cCupom) ) .And. P01_STATUS <> COMERRO ) 

		SaveInter()		
						
		u_ECFPdv( P01->P01_FILIAL , Rtrim(P01->P01_NSECF) , @cPDV, @cSerie, @cNumCx, @cCodEstacao )
		
		// -- Efetua valida��o das formas de pagamento na SL4
		
		SL4->( DbSetOrder(1) )		
		IF SL4->( DbSeek( P01->P01_FILIAL + P01->P01_NUMORC ) )
		
			cMsgLog 		:= ""
			lErroSL4		:= .F.
			
			While SL4->( !Eof() .And. L4_FILIAL+L4_NUM == P01->P01_FILIAL + P01->P01_NUMORC ) 			
				IF RTrim(SL4->L4_FORMA) $ "CC|CO|CD|FI|VA"				
					IF Empty( RTrim( SL4->L4_ADMINIS ) )					
						cMsgLog	+= "Adm n�o preenchida (SL4) " + RTrim(SL4->L4_FORMA) +  " Valor R$ " + AllTrim( Str(SL4->L4_VALOR,10,2) ) + CRLF
						lErroSL4	:= .T.					
					Endif				
				Endif			
				SL4->( DbSkip() )					
			End
			
			IF lErroSL4						
				RecLock("P01",.F.)
					P01->P01_STATUS	:= COMERRO
					P01->P01_LOGIMP	:= cMsgLog
				MsUnLock()
				
				u_TibDeletaOrc( P01->P01_FILIAL , P01->P01_NUMORC , P01->P01_DOC )				
				Loop		
			Endif
						
		Endif
		
		PtInternal(1, "[VAREJO] Dia " + DTOC(ddatabase) + " Grava��o do Cupom " + AllTrim(P01->P01_DOC) + "  " )
		
		// -- Controle de Erros
		
		aResume			:= {}
		cTextoErro		:= ""
		bOldError 		:= ErrorBlock( { |x| VerIfErro(x, @aResume , @cTextoErro ) } ) // muda code-block de erro
					
		Begin Transaction 
		
		Begin Sequence
					
		// Inicia processo de processamento do cupom fiscal		
		// Begin Transaction 	

		DbSelectArea("SL1")	
		SL1->( DbSetOrder(1) )
		IF SL1->( DbSeek( xFilial("SL1") + P01->P01_NUMORC ) )
				
			RecLock("SL1",.F.)
				SL1->L1_TIPO	:= "V" 		
			MsUnLock()				
						
			If !lNaoProduziu
				
				cModulo := "LOJ"
				nModulo	:= 12
				
				M->LQ_CLIENTE 	:= SL1->L1_CLIENTE
				M->LQ_LOJA		:= SL1->L1_LOJA
				
				// Transforma o orcamento em cupom fiscal	
				RecLock("SL1",.F.)
					SL1->L1_SITUA	:= "RX"	
				MsUnLock()
				
				SetFunName("LOJA701")			
				dDatabase		:= P01->P01_DTEMIS
				
				u_TibGravaBatch( cEmpAnt , cFilAnt , /*cIntervalo*/ , /*cMinReproc*/ , SL1->L1_NUM , @lGravacao )
				
				// -- Caso nao consiga efetivar a venda deve desfazer as OP's criadas
				 	
				IF !lGravacao
				
					dDatabase	:= dDataBackup
				
					LjWriteLog(cLogMT, "Antes do DisarmTransaction() GRAVABATCH" )
				
					DisarmTransaction()	
					RollBackSX8()
				
					LjWriteLog(cLogMT, "Depois do DisarmTransaction() GRAVABATCH" )
				
				Endif
					
				
				// -- Atualiza dados do Orcamento ap�s finaliza��o da venda				
				
				SL1->( DbSetOrder(1) )
				IF SL1->( DbSeek( xFilial("SL1") + P01->P01_NUMORC ) )								

					   RecLock("SL1",.F.)
						   SL1->L1_NUMMOV	:= LJNumMov()
						   SL1->L1_TIPO	:= "V" 		
					   MsUnLock()				

                      // -- Atualiza Situa��o tribut�ria
                      
                      SL2->( DbSetOrder(1) )
                      IF SL2->( DbSeek(SL1->L1_FILIAL+SL1->L1_NUM) )
                        
                          While SL2->(L2_FILIAL+L2_NUM) == SL1->(L1_FILIAL+L1_NUM)
                            
                              SD2->( DbSetOrder(3) )
                              If SD2->( DbSeek( xFilial('SD2') + SL1->(L1_DOC + L1_SERIE + L1_CLIENTE + L1_LOJA) + SL2->L2_PRODUTO + SL2->L2_ITEM ) ) .And. SD2->D2_PICM > 0
                                
                                  cSTrib := 'T' + StrZero( SD2->D2_PICM * 100, 4, 0 )
 
                                  SL2->(RecLock('SL2',.f.))
                                  SL2->L2_SITTRIB := cSTrib
                                  SL2->(MsUnLock())
                                    
                                  RecLock('SD2',.F.)
                                  Replace D2_SITTRIB With cSTrib
                                  MsUnLock()
                                    
                              Endif
                               
                              SL2->(DbSkip())
                                
                          End

                      Endif

				  Endif
				  					
				  // -- Reprocessamento do Livro Fiscal
				  
				  If !IsBlind()
				  	MsAguarde( {|| u_FWFiscais( SL1->L1_FILIAL , 2 , SL1->L1_EMISSAO , SL1->L1_DOC , SL1->L1_SERIE , SL1->L1_CLIENTE , SL1->L1_LOJA )  },'Livros Fiscais')//Gera Titulos provisorios
				  Else
				  	u_FWFiscais( SL1->L1_FILIAL , 2 , SL1->L1_EMISSAO , SL1->L1_DOC , SL1->L1_SERIE , SL1->L1_CLIENTE , SL1->L1_LOJA )
				  Endif
				  						
                  SF2->( DbSetOrder(1) ) 
                  If SF2->( DbSeek( xFilial('SF2') + SL1->(L1_DOC + L1_SERIE + L1_CLIENTE + L1_LOJA) ) )                    
                        RecLock('SF2',.F.)
                        Replace F2_ESPECIE With 'NFCE'
                        MsUnLock()                        
                  Endif
                    
                  SF3->( DbSetOrder( 5 ) )
                  IF SF3->( DbSeek( xFilial('SF3') + SF2->(F2_SERIE + F2_DOC + F2_CLIENTE + F2_LOJA) ) )                    
                      While SF3->( F3_FILIAL + F3_SERIE + F3_NFISCAL + F3_CLIEFOR + F3_LOJA ) == SF2->(F2_FILIAL + F2_SERIE + F2_DOC + F2_CLIENTE + F2_LOJA)                        
                            RecLock('SF3',.F.)
                            Replace F3_ESPECIE With 'NFCE'
                            MsUnlock()
                            SF3->( DbSkip() )                            
                      End
                  Endif
                
                  SFT->( DbSetOrder( 3 ) ) 
                  IF SFT->( DbSeek( xFilial('SFT') + 'S' + SF2->(F2_CLIENTE + F2_LOJA + F2_SERIE + F2_DOC ) ) )                     
                      While SFT->( FT_FILIAL + FT_TIPOMOV + FT_CLIEFOR + FT_LOJA + FT_SERIE + FT_NFISCAL ) == SF2->(F2_FILIAL + 'S' + F2_CLIENTE + F2_LOJA + F2_SERIE + F2_DOC)                    
                            RecLock('SFT', .F. )
                            Replace FT_ESPECIE With 'NFCE'
                            MsUnlock()
                            SFT->( DbSkip() )                        
                      End
                  Endif				
				
				RecLock("P01",.F.)
					P01->P01_STATUS	:= IIF( lGravacao , VENDA , COMERRO )
					P01->P01_LOGIMP	:= IIF( lGravacao , "" , "Erro na finaliza��o da LjGrvBatch" ) 
				MsUnLock()
				
				dDatabase	:= dDataBackup
							
			Endif
			
			cModulo 	:= cModAnt
			nModulo		:= nModAnt			
			dDatabase	:= dDataBackup
			
		Endif
		
		Recover
											
			cModulo 	:= cModAnt
			nModulo		:= nModAnt
			ddatabase	:= dDataBackup
		
			DisarmTransaction()
		
			AEval(aAreas, {|x| RestArea(x)})

			RecLock("P01",.F.)
				P01->P01_STATUS	:= COMERRO
				P01->P01_LOGIMP	:= "ERRO FATAL na finaliza��o da LjGravaBatch " + CRLF + cTextoErro
			MsUnLock()

		End Sequence
		
		ErrorBlock( bOldError )		
		
		End Transaction
	
	Endif

	tFim := ElapTime(tInicio,Time())	
	cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 

	LjWriteLog(cLogMT, "Orcamento " + P01->P01_NUMORC + " Cupom " + cCupom +  IIF( lGravacao , " [OK] " , " [ERRO] " ) )
	
	RestInter()

	AEval(aAreas, {|x| RestArea(x)})
		 
Next nX

dDatabase	:= dDataBackup

If aParamMT <> Nil
	PutGlbValue( aParamMT[1] , "4" ) // STATUS 4 - Finalizado
	GlbUnLock()
EndIf

Return




/*/{Protheus.doc} FWFiscais
Reprocessa os registros do livro fiscal

@type 		User Function
@author 	Mauro Paladini
@since 		Feb 27, 2019  1:46:56 AM
@version 	1.0
@return 	${return}, ${return_description}

/*/

User Function FWFiscais( xFilial , xTipo , xEmisCF , xNumDoc , xSerie , xCliente , xLoja )		

Local aAreas  	:= { 	DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) ,;
						SA1->(GetArea()) , SF4->(GetArea()) , SF2->(GetArea()) , SD2->(GetArea()) ,;
						SE1->(GetArea()) , SE2->(GetArea()) , SF1->(GetArea()) , SD1->(GetArea()) ,;
						SED->(GetArea()) , SFT->(GetArea()) , SF3->(GetArea()) , SA2->(GetArea()) ,;
						SL1->(GetArea()) , SL2->(GetArea()) , SL4->(GetArea()) , GetArea() }




Local aPergA930	:= {}

AAdd( aPergA930, DTOC(xEmisCF) 	)
AAdd( aPergA930, DTOC(xEmisCF) 	)
AAdd( aPergA930, xTipo 			)		
AAdd( aPergA930, xNumDoc 		)
AAdd( aPergA930, xNumDoc 		)
AAdd( aPergA930, xSerie 		)
AAdd( aPergA930, xSerie 		)
AAdd( aPergA930, xCliente 		)
AAdd( aPergA930, xCliente 		)
AAdd( aPergA930, xLoja 			)
AAdd( aPergA930, xLoja 			)

Pergunte("MTA930",.F.)

MATA930( .T. , aPergA930 )			

AEval(aAreas, {|x| RestArea(x)})	  

Return



/*/{Protheus.doc} VeriErro
Retorna erro fatal ocorrido no processo executado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/

Static Function VerIfErro( e , aResume , cTextoErro )

	Local lRet 		:= .F.
	Local cAutoGets	:= {}
		
	If e:gencode > 0 
    
		cTextoErro := "DESCRIPTION: " + e:DESCRIPTION + CRLF
		cTextoErro += "ERRORSTACK:" + CRLF
		cTextoErro += e:ERRORSTACK
	    	
		lRet:=.T.

		Break

	EndIf

Return lRet


/*/{Protheus.doc} FWGetErro
Retorna erro ocorrido no processo executado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/
Static Function FWGetErro( oObjMdl )

Local aErro := {}
Local cRet	:= ""

DEFAULT oObjMdl := FwModelActive()

aErro := oObjMdl:GetErrorMessage()

If Len(aErro) > 0

	cRet	:= '[' + AllToChar( aErro[MODEL_MSGERR_IDFORMERR] ) + ']'   + CRLF
	cRet	+= '[' + AllToChar( aErro[MODEL_MSGERR_IDFIELDERR] ) + ']' + CRLF
	cRet	+= '[' + AllToChar( aErro[MODEL_MSGERR_ID] ) + ']'  + CRLF
	cRet	+= '[' + AllToChar( aErro[MODEL_MSGERR_MESSAGE] ) + '|' + AllToChar( aErro[MODEL_MSGERR_SOLUCTION] ) + ']' + CRLF

EndIf

Return cRet


/*
ShowLog
Exibe detalhes do erro apresentado pela rotina

@author Mauro Paladini
@since 10/04/2015
@version 1.0
*/

Static Function ShowLog( cMemo )

	Local oDlg     := NIL
	Local oFont    := NIL
	Local oMemo    := NIL

	DEFINE FONT oFont NAME "Courier New" SIZE 5,0
	DEFINE MSDIALOG oDlg TITLE 'Log' From 3,0 to 340,617 PIXEL

	@ 5,5 GET oMemo  VAR cMemo MEMO SIZE 300,145 OF oDlg PIXEL

	oMemo:bRClicked := {|| AllwaysTrue()}
	oMemo:oFont:=oFont

	DEFINE SBUTTON  FROM 153,280 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
	ACTIVATE MSDIALOG oDlg CENTER

Return Nil



/*
Log2Array
Abre arquivo de LOG da ExecAuto e retorna em um array
@author Mauro Paladini
@since 10/04/2015
@version 1.0
*/

Static Function Log2Array(cFileLog, lAchaErro )
	
	Local nHdl		:= -1
	Local nPos		:= 0
	Local nBytes	:= 0
	
	Local cLinha	:= ""
	Local cChar		:= ""

	Local cRet		:= ""
	Local aRet		:= {}

	Default cFileLog	:= ""
	Default lAchaErro	:= .T.
	
	IF !Empty(cFileLog)
		nHdl := FOpen(cFileLog)
	Endif

	If !(nHdl == -1)
	
		nBytes := FSeek(nHdl, 0, 2)
		FSeek(nHdl, 0)

		If nBytes > 0

			For nPos := 1 To nBytes
	
				FRead(nHdl, @cChar, 1)
				
				// Quebra de linha (chr(13)+chr(10) = CR+LF)
				If cChar == Chr(10) .Or. nPos == nBytes
					
					If lAchaErro .And. "< -- Invalido" $ cLinha
						cRet	:= "Campo -> " + cLinha
						Exit
					Endif

					IF !Empty(cLinha)
						aAdd(aRet, cLinha)
					Endif
				
					cLinha :=  ""
										 
				ElseIf cChar <> Chr(13)

					cLinha += cChar

				EndIf
	
			Next nPos

		EndIf
	
		FClose(nHdl)
	
	Endif
		
	IF Empty(cRet) .And.  Len(aRet) >= 2
		cRet := RTrim(aRet[1]) + " => " + RTrim(aRet[Len(aRet)])
	Endif
	
Return IIF( lAchaErro , cRet , aRet )