#Include 'Protheus.ch'

#DEFINE TIPOREG		1
#DEFINE TITULO 		2
#DEFINE	CAMPO		3
#DEFINE TAMANHO		4
#DEFINE POSINI		5
#DEFINE POSFIM		6
#DEFINE FORMATO		7
#DEFINE DECIMAL		8 

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE COMERRO   	"8"
#DEFINE EMPROCESSA	"9"

#DEFINE NTRYARQ		15		// Numero de tentativas default de operacoes com arquivo texto

Static lCenVenda	:= SuperGetMv("MV_LJCNVDA",,.F.)

/*/{Protheus.doc} TibGrvOrc
Gera orcamento para um determinado cupom importado
Transforma os registros gravados nas tabelas da familia P  

P01 => SL1-Cabecalho
P02 => SL2-Itens
P03 => SL4-Meios de Pagamento

@author Mauro Paladini
@since 12/02/2015

/*/

User Function TibGrvOrc( cArqImp , cCupom, lProcessou , lRpcClear )

Local aAreas  	:= { 	DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) ,;
						SA1->(GetArea()) , SF4->(GetArea()) , SX5->(GetArea()) , SE1->(GetArea()) ,;
						SA6->(GetArea()) , SLG->(GetArea()) , SLF->(GetArea()) , P01->(GetArea()) ,;
						P02->(GetArea()) , P03->(GetArea()) , SAE->(GetArea()) , SX2->(GetArea()) ,;
						SX3->(GetArea()) , SM0->(GetArea()) , SX6->(GetArea()) , GetArea() }

Local aCabec		:= {}
Local aItem			:= {}
Local aItens		:= {}
Local aSL4			:= {}
Local aSL4Aux		:= {}
Local aRecP02		:= {}
Local aRecP03		:= {}

Local cL4Forma		:= ""
Local cL4AdmFin		:= ""

Local nDescPer		:= 0
Local nL4Valor		:= 0 
Local nX			:= 0
Local nI			:= 0
Local nPosForma		:= 0
Local nTotItem		:= 0
Local nTotPagtos	:= 0
Local nQtdPgtos		:= 0
Local nPagtoDif		:= 0

Local cAliasTRB		:= GetNextAlias()
Local cMsgErro		:= ""
Local cCliente		:= Getmv("MV_CLIPAD",,"000001")
Local cLojaCli		:= GetMv("MV_LOJAPAD",,"01")
Local cCodVend		:= GetMv("MV_VENDPAD",,"01")
Local lAutoTes		:= GetMv("FS_AUTOTES",,.T.)
Local cFS_TesSai	:= GetMv("FS_TESSAI",,"501")  

Local cGrupoOBS		:= GetMv("ES_GRPOBS",,"")
Local cTesVenda		:= ""
Local cLogL4		:= "\lodpdv\wfpagtos.log"
Local dDataAnt		:= ddatabase 

Local lRet              := .T.
Local cCaixa            := ""
Local cNumOrc           := ""
Local cNomeCaixa        := ""
Local cCaixaSup         := ""
Local nNCCVenda         := 0
Local aNCC              := {}
Local aNCCItens         := {}
Local aMDJ              := {}
Local cLGCodSaT			:= ''

Local cSX5              := GetNextAlias()
Local cSE1              := GetNextAlias()

// MsExecAuto 

Private nOpc			:= 3
Private INCLUI 			:= .T.			
Private ALTERA			:= .F.	
Private aRotina   	    := {}
Private lMsHelpAuto     := .T.
Private lMsErroAuto     := .F.
Private cTextoErro  	:= ""
Private aUltTab			:= {}

Default lProcessou	:= .T.
Default cArqImp     := Space(TamSx3("P01_ARQ")[1])
Default lRpcClear	:= .F.

If Type("cEstacao") == "U"
	Public cEstacao	:= ""
Endif

// Guarda variaveis de ambiente

SaveInter()

// -- Ajusta dicionario
AjustaSX3()

// -- Valida confiura��es do ambiente SIGALOJA

IF !u_LjVldAmbiente( .T. /*lOrcamento*/ , @lRet, @cMsgErro , @cCaixa , cArqImp , cCupom , cNumOrc , @cNomeCaixa , @cCaixaSup , @cEstacao  )
	Return lRet
Endif

SA1->( DbSetOrder(1) )
SB0->( DbSetOrder(1) )
SB1->( DbSetOrder(1) )
P02->( DbSetOrder(6) )
P03->( DbSetOrder(6) )
SAE->( DbSetOrder(3) )


cLGCodSaT	  := IIF(LJGetStation("CODSAT",.F.) == Nil,"",LJGetStation("CODSAT",.F.))
ddatabase 	  := P01->P01_DTEMIS
cCupom        := P01->P01_DOCCOO

// Usa o cliente identificado caso o CPF tenha sido informado no arquivo da NF Paulista
IF !Empty(P01->P01_CLIENT)
	cCliente	:= P01->P01_CLIENT
	cLojaCli	:= P01->P01_LOJA 
Endif

// Posiciona registros relacionados

SA1->( DbSeek( xFilial("SA1") + cCliente + cLojaCli ) )

SA3->( DbSeek( xFilial("SA3") + cCodVend ) )        

SA6->( DbSetOrder(1) )

SA6->( DbSeek( xFilial("SA6") + cCaixa ) )

cUsername  := RTrim(SA6->A6_NOME)

u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "Inicio TibOrcamentos.prw -------------- " )
u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "ddatabase 	:= " + DTOC(ddatabase) )
u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "cCaixa 		:= " + cCaixa  )
u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "cUserName 	:= " + cUserName  )

DBSelectArea("SLQ")
DbSetOrder(1)	

// Valida os itens do cupom fiscal
IF P02->( DbSeek( xFilial("P02") + cArqImp + RTrim(cCupom) ) )

    aAdd(aCabec,{"LQ_VEND"	    , SA3->A3_COD							,	Nil })	// Vendedor
    aAdd(aCabec,{"LQ_COMIS"     , 0									,	NIL })	// Comiss�o    
    aAdd(aCabec,{"LQ_CLIENTE"   , SA1->A1_COD							, 	Nil })	// Cliente
    aAdd(aCabec,{"LQ_LOJA"      , SA1->A1_LOJA						     , 	Nil })	// Loja
    aAdd(aCabec,{"LQ_TIPOCLI"   , SA1->A1_TIPO						     ,	NIL })	// Tipo de Cliente
    aAdd(aCabec,{"L1_ORCRES"    , ''                                       ,   NIL })  // Tipo de Cliente

	While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + RTrim(P02_DOC) == xFilial("P02") + cArqImp + RTrim(cCupom) )
									
			
		// -- Validar estorno parcial ou total do item
		 
		If P02->P02_ESTORN <> "1"
		
			If SB1->( DbSeek( xFilial("SB1") + P02->P02_PROD ) )
			
				// -- Avalia Tabela de precos para nao dar erro ao incluir o orcamento
								
				STNTabPreco( P02->P02_PROD , P02->P02_PRUNIT ) 
						 				
				// -- Considera estornos parciais
				
				nQtdEst		:= P02->P02_QTDEST  
				nVlrEst		:= P02->P02_VLREST  
				nVlrItem	:= P02->P02_VALLIQ

				aItem    	:= {}

				// -- Tratamento para utiliza��o da TES inteligente
				
				cTesVenda	:= MaTesInt( 2	, "01"	, SA1->A1_COD , SA1->A1_LOJA ,"C" , PadR(P02->P02_PROD,TamSx3("LR_PRODUTO")[1]) , NIL )
				
				
				// -- Caso n�o encontre utiliza TES do Produto
				
				If Empty(cTesVenda)
					cTesVenda	:= PadR(SB1->B1_TS,TamSx3("LR_TES")[1])							
				Endif
									
				// -- Caso esteja em banco no produto utiliza TES fixa em parametro
				
				If Empty(cTesVenda)
					cTesVenda	:= PadR(cFS_TesSai,TamSx3("LR_TES")[1])					
				Endif
									

				aAdd(aItem,{"LR_PRODUTO"		,	PadR(P02->P02_PROD,TamSx3("LR_PRODUTO")[1])			,	Nil })	// Cod. do Produto				
				aAdd(aItem,{"LR_UM"				,   PadR(SB1->B1_UM,TamSx3("LR_UM")[1])					,	NIL })	// Unidade de Medida 
				aAdd(aItem,{"LR_QUANT"			,	P02->P02_QUANT - nQtdEst							,	Nil })	// Quant. Item				

				nPrUnit 	   	:= ( P02->P02_PRUNIT / 100 )				
				nPerc 	   		:= Round( P02->P02_DESCIT / nPrUnit , 2)
								
				aAdd(aItem,{"LR_VALDESC"		,	P02->P02_DESCIT               						,	NIL })	// Valor de Desconto				
								
				aAdd(aItem,{"LR_DOC"		     ,   RTrim(P01->P01_DOC)               					,	NIL })	
				aAdd(aItem,{"LR_SERIE"			,	P01->P01_LGSERI               						,	NIL })	
				aAdd(aItem,{"LR_PDV" 			,	P01->P01_LGPDV               						,	NIL })										
				aAdd(aItem,{"LR_TABELA"			,	SuperGetMV( "MV_TABPAD",,"1")						,	NIL })	// Tabela Padr�o
				
				// -- N�o utilizado desconto em %
				
				// aAdd(aItem,{"LR_DESCPRO"		,	0													,	NIL })	// Desconto do Produto
				
				aAdd(aItem,{"LR_FILIAL"			,	xFilial("SLR")										,	Nil })	// Filial	
				aAdd(aItem,{"LR_VRUNIT"			,	P02->P02_PRUNIT										,	Nil })	// Valor Unit�rio do Item		
                aAdd(aItem,{"LR_VLRITEM"        ,   nVlrItem                                            ,   Nil })  // Valor Unit�rio do Item       

				aAdd(aItem,{"LR_VEND"			,	PadR(cCodVend,TamSx3("LR_VEND")[1])				   	,	Nil })	// Cod. do Vendedor	
				
				If !Empty(cTesVenda)
					aAdd(aItem,{"LR_TES"		,	cTesVenda	    									,	Nil, })	// Cod. de Entrada e Sa�da
				Endif
			
				aAdd(aItens,aItem)
								
				u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "Produto 		:= " + P02->P02_ITEM + " " + P02->P02_PROD + RTrim(SB1->B1_DESC) )
				u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "TES			:= " + cTesVenda )
								
				nTotItem := A410Arred( nTotItem + nVlrItem  , "LR_VLRITEM" ) 
			
			Endif
		
		Endif
		
		aAdd( aRecP02 , { P02->( Recno() ) , StrZero(Len(aItens),2)  } )
		
		P02->( DbSkip() )
		
	End
	

	// -- Contar a quantidade de pagamentos da venda
	
	BEGINSQL Alias cAliasTRB
	
	SELECT COUNT(*) PGTOS 
	FROM %table:P03%  P03
	WHERE 
			P03_FILIAL = %xFilial:P03% AND 
			P03_ARQ = %Exp:cArqImp%   AND 
			P03_DOCCOO = %Exp:RTrim(cCupom)% AND 
			P03.D_E_L_E_T_ = ''
	
	ENDSQL
	
	(cAliasTRB)->( DbGoTop() )
	
	nQtdPgtos	:= (cAliasTRB)->PGTOS
	
	(cAliasTRB)->( DbCloseArea() )
	
	// -- Fim da contagem de quantidade de pagamentos da venda
	

	// -- Prepara Pagamentos 
	
	IF P03->( DbSeek( xFilial("P03") + cArqImp + RTrim(cCupom) ) )
	
	    nTotPagtos := 0
					
		While P03->( !Eof() .And.  P03_FILIAL + RTrim(P03_DOCCOO) == xFilial("P03") + RTrim(cCupom) )
									
			IF !Empty(RTrim(P03->P03_CODADM)) .And. SAE->( DbSeek( xFilial("SAE") + RTrim(P03->P03_CODADM) ) )
			
				cL4Forma	:= RTrim(SAE->AE_TIPO)
				cL4AdmFin	:= SAE->AE_COD + "-" + SAE->AE_DESC
				nL4Valor	:= P03->P03_VALOR
			
			Else

                   // -- Identifica Meio de Pagamento
                    
				BEGINSQL Alias cSX5					
				SELECT TOP 1 X5_CHAVE, X5_DESCRI
				FROM   %table:SX5% SX5
				       WHERE   X5_FILIAL = %xFilial:SX5% AND
				               X5_DESCRI = %Exp:UPPER(RTrim(P03->P03_FORMPG))% AND
				               SX5.D_E_L_E_T_ = '' 					                 					
				ENDSQL
				
				(cSX5)->( DbGoTop() )
				
				If (cSX5)->( !Eof() )
                    
                        cL4Forma    := RTrim((cSX5)->X5_CHAVE ) 
                        cL4AdmFin   := ""  
                        nL4Valor    := P03->P03_VALOR             				     
				
				Else
				
                        cL4Forma	   := "R$"
                        cL4AdmFin   	:= ""				
                        nL4Valor	   := P03->P03_VALOR
				    
				Endif	
				
				(cSX5)->( DbCloseArea() )		   
			
			Endif
			
			
			nPosForma    := Ascan(aSL4,{|x| AllTrim(x[3]) == "R$"})						
			nTotPagtos   := A410Arred( nTotPagtos + nL4Valor  , "P03_VALOR" )
						
			IF cL4Forma == "R$"  .And. nPosForma > 0
				aSL4[nPosForma][2] +=  A410Arred( aSL4[nPosForma][2] + nL4Valor , "P03_VALOR" )
			
			Else
			
				aSL4Aux := {}				
				nL4Valor := A410Arred( nL4Valor  , "LR_VLRITEM" )
				
				Aadd( aSL4Aux, {"L4_DATA"		, ddatabase							, NIL}	)	// L4_DATA
				Aadd( aSL4Aux, {"L4_VALOR"		, nL4Valor							, NIL}	)	// L4_VALOR				
				Aadd( aSL4Aux, {"L4_FORMA"		, cL4Forma							, NIL}	)	// L4_FORMA
				Aadd( aSL4Aux, {"L4_ADMINIS"	, cL4AdmFin							, NIL}	)	// L4_ADMINIS			
				Aadd( aSL4Aux, {"L4_FORMAID"	, ""									, NIL} )	// L4_CGC
				Aadd( aSL4Aux, {"L4_MOEDA"		, 1									, NIL}	)	// L4_MOEDA				
				Aadd( aSL4Aux, {"L4_NUMCART"	, ""									, NIL}	)	// L4_NUMCART
                   						
				Aadd( aSL4, aSL4Aux )			

			Endif 

			aAdd( aRecP03 , P03->( Recno() ) )
			
			// -- alimenta para a aNCCUsadas
             
             IF 'NCC' $ P03->P03_FORMPG
                nNCCVenda += P03->P03_VALOR
                aAdd( aNCC , { P03->( Recno() ) , P03->P03_VALOR } )         
             Endif 
             
             P03->( DbSkip() )
		
		End 
			
	Endif
	

	// -- Protecao para a diferenca de centavos da gorjeta
	
	IF nTotPagtos > nTotItem
	
		nPagtoDif	:= ABS( nTotPagtos - nTotItem )
		
		P03->( DbGoTo( aRecP03[Len(aRecP03)] ) )
		
		RecLock("P03",.F.)
			P03->P03_VALOR -= nPagtoDif
		MsUnLock() 

		nTotPagtos -= nPagtoDif
			
	Endif
	

    // ### Valida a NCC em caso de pagamentos
    IF nNCCVenda > 0 
                
        BEGINSQL Alias cSE1
        SELECT TOP 1    E1_SALDO, 
                        E1_NUM, 
                        E1_EMISSAO, 
                        R_E_C_N_O_ AS RECSE1,
                        E1_SALDO, 
                        E1_MOEDA, 
                        E1_PREFIXO, 
                        E1_PARCELA, 
                        E1_TIPO
                        
        FROM   %table:SE1% SE1
               WHERE    E1_FILIAL   = %Exp:P01->P01_FILIAL% AND 
                        E1_TIPO     = 'NCC'     AND
                        E1_CLIENTE  = %Exp:P01->P01_CLIENT% AND
                        E1_LOJA     = %Exp:P01->P01_LOJA%   AND
                        E1_VALOR    BETWEEN %Exp:(nNCCVenda)% AND %Exp:(nNCCVenda + 0.03)% AND
                        E1_SALDO    BETWEEN %Exp:(nNCCVenda)% AND %Exp:(nNCCVenda + 0.03)% AND
                        E1_STATUS   = 'A'    AND 
                        SE1.D_E_L_E_T_ = ''   
        ENDSQL
        
        (cSE1)->( DbGoTop() )
        
        If (cSE1)->( !Eof() )
                        
            aNCCItens   := {}
            
            AAdd(aNCCItens,  {    .T.,;
                                   (cSE1)->E1_SALDO ,;
                                   (cSE1)->E1_NUM  ,;
                                   STOD((cSE1)->E1_EMISSAO) ,;
                                   (cSE1)->RECSE1  ,;
                                   (cSE1)->E1_SALDO ,;
                                   SuperGetMV("MV_MOEDA1") ,;
                                   (cSE1)->E1_MOEDA ,;
                                   (cSE1)->E1_PREFIXO ,;
                                   (cSE1)->E1_PARCELA ,;
                                   (cSE1)->E1_TIPO })
                                   
        Endif
        
        (cSE1)->( DbCloseArea() )	
	
	Endif
	
	
	aAdd(aCabec,{"LQ_VLRTOT"	, A410Arred( nTotItem , "LQ_VLRTOT" )   	         , NIL }) 	// Valor Total
	aAdd(aCabec,{"LQ_DESCONT"	, 0													 , NIL })	// Desconto, por enquanto, 0
	aAdd(aCabec,{"LQ_VLRLIQ"	, A410Arred( nTotItem , "LQ_VLRLIQ" )   	         , NIL })	// Valor L�quido	
	aAdd(aCabec,{"LQ_NROPCLI" 	, " " 										, NIL })
	aAdd(aCabec,{"LQ_DTLIM"		, ddatabase									, NIL })	// Data Limite				
	aAdd(aCabec,{"LQ_EMISSAO"	, ddatabase									, Nil })	// Emissao
	
	aAdd(aCabec,{"LQ_DOC"		, RTrim(P01->P01_DOC)							, Nil })
	aAdd(aCabec,{"LQ_SERIE"		, P01->P01_LGSERI								, Nil })
	aAdd(aCabec,{"LQ_PDV"		, P01->P01_LGPDV								, Nil })
	aAdd(aCabec,{"LQ_EMISNF"	, ddatabase									, Nil })	// Emissao		
	aAdd(aCabec,{"LQ_NUMCFIS"	, RTrim(P01->P01_DOC)							, Nil }) 	// Numero do cupom fiscal 			 
	aAdd(aCabec,{"LQ_NUMMOV"  	, "1 "										, Nil })
	aAdd(aCabec,{"LQ_OPERADO"  	, P01->P01_OPERAD                   		            , Nil })
	
	cMsgTxt	:= "Cupom " + P01->P01_DOC + " Valor Liquido " + LTrim(Str(nTotItem,10,2)) + " Pagamentos " + LTrim(Str(nTotPagtos,10,2)) + IIF( nTotPagtos >= nTotItem , "[OK]" , "[ERRO]")

	u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )
		
	Begin Transaction
	
	cTextoErro	:= ''
	bOldError 	:= ErrorBlock( { |x| u_TibVerifBlock()(x, @cTextoErro ) } ) // muda code-block de erro
				
	Begin Sequence
	
	PtInternal(1, "[VAREJO] Dia " + DTOC(ddatabase) + " Filial " + cFilAnt + " Cupom " + AllTrim(P01->P01_DOC) + "  " )

	IF nTotPagtos > 0 .And. nTotPagtos = nTotItem  .And. ( ( nNCCVenda > 0 .And. Len(aNCCItens) > 0 )  .OR.  nNCCVenda = 0  )
		                
		cMsgErro			:= ""
		lMsErroAuto			:= .F.
		lAutoErrNoFile		:= .T.
		lMsHelpAuto			:= .T.
		cNumOrc				:= ''
	    nModulo				:= 12
	        
		If MafisFound()
			MaFisEnd()
		EndIf

		DbSelectArea("SL1")		
		SetFunName("LOJA701")
						
		MSExecAuto({|a,b,c,d,e,f,g,h| LOJA701(a,b,c,d,e,f,g,h)},.F.,3,SA1->A1_COD,SA1->A1_LOJA,{},aCabec,aItens,aSL4)
		
		lProcessou			:= !lMsErroAuto		

		If lMsErroAuto 
		
			cMsgTxt	:= "Cupom " + P01->P01_DOC + " MsExecAuto [ERRO]" 
			u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )
			
			aErro 		:= GetAutoGRLog() 
			cMsgErro	:= ""
			
			For nX := 1 To Len(aErro)			
			   	cMsgErro += alltrim(aErro[nX])+Chr(13)+Chr(10)			
			Next nX 
			
			cMsgErro := StrTran(cMsgErro,"<","")
			cMsgErro := StrTran(cMsgErro,">","") + CRLF 

			DisarmTransaction()
			/*
			FreeUsedCode(.T.)
			ReleaseUser() */
				
		Else
					
			cNumOrc		:= SL1->L1_NUM
			
			If Empty( AllTrim(cNumOrc) )
				DisarmTransaction()
				lProcessou 	:= .F.
				Return .F.			
			Endif
			
			cMsgTxt		:= "Atualizando Or�amento " + cNumOrc + "  Cupom " + P01->P01_DOC + " MsExecAuto executada com sucesso [OK]"			
			u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )
		

			// -- Faz ajuste na tabela SL4 referente o valor da gorjet			

			SL4->( DbSetOrder(1) )
			If SL4->( DbSeek( P01->P01_FILIAL + SL1->L1_NUM	 ) )				
				While SL4->( !Eof() .AND. L4_FILIAL+L4_NUM == P01->P01_FILIAL+SL1->L1_NUM	 )
					Reclock("SL4",.F.)
					SL4->( dbDelete() )
					MsUnlock()
					SL4->( dbSkip() )
				End
			
			EndIf
			
			// -- Cria novos registros na SL4 para ter a referencia do valor da Gorjeta
			
			For nX := 1 To Len(aSL4) 	
				Reclock("SL4",.T.)			
					SL4->L4_FILIAL	:= P01->P01_FILIAL				
					For nI := 1 To Len( aSL4[1] )
						SL4->( FieldPut( FieldPos( aSL4[nX][nI][1] ), aSL4[nX][nI][2] ) )
					Next nI					
					SL4->L4_NUM	:= SL1->L1_NUM 					
				MsUnLock()												
			Next nX
						
			// -- Atualiza Status do CUPOM FISCAL 
					
			RecLock("P01",.F.)
				P01->P01_STATUS	:= ORCAMENTO
				P01->P01_NUMORC	:= SL1->L1_NUM  						
			MsUnLock()
			
			For nX := 1 To Len(aRecP02)				
				P02->( DbGoTo( aRecP02[nX][1]) )			
				RecLock("P02",.F.)
					P02->P02_STATUS	:= ORCAMENTO
					P02->P02_NUMORC	:= SL1->L1_NUM
				MsUnLock()
			Next nX		
		
			For nX := 1 To Len(aRecP03)		
				P03->( DbGoTo( aRecP03[nX]) )			
				RecLock("P03",.F.)
					P03->P03_STATUS	:= ORCAMENTO
					P03->P03_NUMORC	:= SL1->L1_NUM 						
				MsUnLock()
			Next nX
					

				// Atualiza Orcamento
				RecLock("SL1",.F.)		
				
				SL1->L1_DOC		:= P01->P01_DOC
				SL1->L1_NUMCFIS	:= P01->P01_DOC
				SL1->L1_EMISNF	:= P01->P01_DTEMIS 
				SL1->L1_SERIE	:= P01->P01_LGSERI
				SL1->L1_PDV		:= P01->P01_LGPDV
				SL1->L1_ESTACAO	:= P01->P01_LGCOD 
				SL1->L1_SITUA	:= "RX"
				SL1->L1_OPERADO	:= P01->P01_OPERAD		
				SL1->L1_KEYNFCE := P01->P01_CHAVE				
				SL1->L1_ESPECIE	:= IIF( !Empty(RTrim(cLGCodSaT)) , 'SATCE' , '' )  					
				SL1->L1_SERSAT  := LEFT(P01->P01_SATQUP,8)
				SL1->L1_CREDITO := nNCCVenda			
				SL1->L1_ENTRADA := SL1->L1_ENTRADA - nNCCVenda
	
				If P01->P01_CGC <> "00000000000000"
					SL1->L1_CGCCLI	:= P01->P01_CGC
				Endif
			
			MsUnLock()
						
			SL2->( DbSetOrder(1) )
			IF SL2->( DbSeek( xFilial("SL2") + SL1->L1_NUM ) )
				While SL2->( !Eof() .And. L2_FILIAL+L2_NUM == xFilial("SL2")+SL1->L1_NUM ) 
				
					RecLock("SL2",.F.)
						SL2->L2_VENDIDO    := "S"
						SL2->L2_DOC        := P01->P01_DOC
						SL2->L2_SERIE      := P01->P01_LGSERI
						SL2->L2_PDV        := P01->P01_LGPDV 
				       	SL2->L2_ESPECIE    := IIF( !Empty(RTrim(cLGCodSaT)) , 'SATCE' , '' )  	 
					MsUnLock()			
					SL2->( DbSkip() )
					
				End
			Endif
	       
	        // -- Gera registros para Baixa da NCC        
	        IF nNCCVenda > 0 .ANd. Len(aNCCItens) > 0 
	
	            aMDJ := {}
	            Aadd(aMDJ,{"MDJ_FILIAL"     ,SL1->L1_FILIAL     })
	            Aadd(aMDJ,{"MDJ_DOC"        ,SL1->L1_DOC        })
	            Aadd(aMDJ,{"MDJ_SERIE"      ,SL1->L1_SERIE      })
	            Aadd(aMDJ,{"MDJ_CLIENT"     ,SL1->L1_CLIENTE    })
	            Aadd(aMDJ,{"MDJ_LOJA"       ,SL1->L1_LOJA       })
	            Aadd(aMDJ,{"MDJ_OPERAD"     ,SL1->L1_OPERADO    })
	            Aadd(aMDJ,{"MDJ_EMIS"       ,SL1->L1_EMISSAO    })
	            Aadd(aMDJ,{"MDJ_NCCUSA"     ,nNCCVenda          })
	            Aadd(aMDJ,{"MDJ_NCCGER"     ,0                  })
	            Aadd(aMDJ,{"MDJ_CREDIT"     ,nNCCVenda          })
	            Aadd(aMDJ,{"MDJ_NUMORC"     ,SL1->L1_NUM        })
	            Aadd(aMDJ,{"MDJ_OPER"       ,"B"                })
	            Aadd(aMDJ,{"MDJ_SITUA"      ,"NP"               })
	            
	            Frt060GrvNcc( aMDJ , aNCCItens , 1)
	                
	            SL4->( DbSetOrder(1) )
	            If SL4->( DbSeek( SL1->L1_FILIAL + SL1->L1_NUM ) ) 
	                While SL4->( !Eof() .AND. L4_FILIAL+L4_NUM == SL1->L1_FILIAL + SL1->L1_NUM )
	                    IF 'NC' $ SL4->L4_FORMA                 
	                        Reclock("SL4",.F.)
	                        SL4->( dbDelete() )
	                        MsUnlock()
	                    Endif                    
	                    SL4->( dbSkip() )
	                End
	            EndIf        
	    
	        Endif

					
		Endif

							
	Else
	
		AEval(aAreas, {|x| RestArea(x)})

		cMsgTxt	:= "Cupom " + P01->P01_DOC + " MsExecAuto N�O [WARNING]" 
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )
				
		lProcessou	 := .F.
		
	Endif
	
	Recover
								
		ddatabase	:= dDataAnt
				
		If nTotPagtos >= nTotItem

			cError	:= 	"ERRO FATAL " + CRLF + cTextoErro			
		
		Else 

			cError := "1) A soma das parcelas � menor que o total da venda." + CRLF + ;
						"ERRO FATAL " + CRLF + cTextoErro			
		
		Endif
		
		AEval(aAreas, {|x| RestArea(x)})
		
		DisarmTransaction()
			
		lProcessou		:= .F.

		RecLock("P01",.F.)
			P01->P01_STATUS	:= COMERRO
			P01->P01_LOGIMP	:= cError
		MsUnLock()

		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "##### ERRO FATAL ######" )
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cError )

	End Sequence
	
	ErrorBlock( bOldError )		
	
	End Transaction	
	
	
	If !lProcessou
	
		If nTotPagtos == 0
			cMsgErro += CRLF + "Meios de pagamento n�o informados"
		Endif

	    IF nNCCVenda > 0 .And. Len(aNCCItens) == 0
	         cMsgErro  += "N�o encontrato cr�dito Tipo NCC R$ " + AllTrim(Transform(nNCCVenda,PesqPict("SE1","E1_VALOR")))                 
	         cMsgErro  += CRLF + "  Cliente: " + P01->P01_CLIENT + " Loja " + P01->P01_LOJA
	    Endif
	         
	     // -- u_SLDelete( P01->P01_FILIAL , P01->P01_NUMORC , '' )      
	
		If nTotPagtos < nTotItem
			cMsgErro += CRLF + "A soma das parcelas � menor que o total da venda."
		Endif
		
		If nTotPagtos > nTotItem
			cMsgErro += CRLF + "A soma das parcelas � SUPERIOR ao total da venda."
		Endif
		
		RecLock("P01",.F.)
			P01->P01_STATUS	:= COMERRO
			P01->P01_LOGIMP	:= cMsgErro
			P01->P01_NUMORC	:= ""
		MsUnLock()
		
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "##### ERRO DA ROTINA AUTOM�TICA ######" )
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgErro )
		
		For nX := 1 To Len(aRecP02)				
			P02->( DbGoTo( aRecP02[nX][1]) )			
			RecLock("P02",.F.)
				P02->P02_STATUS	:= COMERRO
				P02->P02_NUMORC	:= ""
			MsUnLock()
		Next nX		
	
		For nX := 1 To Len(aRecP03)	
			P03->( DbGoTo( aRecP03[nX]) )			
			RecLock("P03",.F.)
				P03->P03_STATUS	:= COMERRO
			MsUnLock()
		Next nX
		
	Endif
		
Endif

nModulo				:= 05			

// Restaura variaveis de ambiente
RestInter()

ddatabase := dDataAnt

AEval(aAreas, {|x| RestArea(x)})

Return




/*/{Protheus.doc} STNTabPreco
Fun��o para atualizar a tabela de pre�os de venda

@author 		Mauro Paladini
@since 			11/02/2017
/*/

Static Function STNTabPreco( cCodProd , nPreco )

Local aAreas  	:= { DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) , GetArea() }	
Local cTabPad	:= SuperGetMv("MV_TABPAD")
Local cItem		:= ""
Local lRet 		:= .T.
Local CQRY		:= GetNextAlias()

// -- Avalia tabela de pre�o para minimizar cupons com divergencia
// -- Considera se o SIGALOJA est� configurado para o cen�rio de vendas

If lCenVenda

	cTabPad		:= Pad(cTabPad,TamSx3("DA0_CODTAB")[1])
	
	BEGINSQL  Alias CQRY
		SELECT MAX(DA1_ITEM) AS ITEM
		FROM %table:DA1% DA1
		WHERE 	DA1.DA1_FILIAL = %xFilial:DA1%
			  	AND DA1.%NotDel%
			  	AND DA1.DA1_CODTAB = %Exp:cTabPad%
	ENDSQL
	
	If (CQRY)->(!EOF())
		cItem := Soma1( (CQRY)->ITEM ,4 )
	EndIf
	
	(CQRY)->(DbCloseArea())
	
	DA0->( dbSetOrder(1) ) 
	DA0->( dbSeek(xFilial("DA0") + cTabPad ) )
	
	DbSelectArea("DA1")
	
	DA1->( dbSetOrder(2) )
	If DA1->( dbSeek(xFilial("DA1") + AvKey( cCodProd , "DA1_CODPRO" )  + cTabPad ) )

		RecLock("DA1",.F.)
			DA1->DA1_ATIVO  := "1"
			DA1->DA1_TPOPER := "4"
			DA1->DA1_QTDLOT := 999999.99 
			DA1->DA1_INDLOT := "000000999999999.99"
			DA1->DA1_PRCVEN := nPreco
			DA1->DA1_XPRCOR	:= nPreco
		DA1->(MsUnLock())
		
	Else
			
		cItem := Soma1(cItem,4)
		
		RecLock("DA1",.T.)
			DA1->DA1_FILIAL := xFilial("DA1")
			DA1->DA1_ITEM   := cItem
			DA1->DA1_CODTAB := cTabPad
			DA1->DA1_CODPRO := cCodProd
			DA1->DA1_PRCVEN := nPreco
			DA1->DA1_XPRCOR	:= nPreco
			DA1->DA1_ATIVO  := "1"
			DA1->DA1_TPOPER := "4"
			DA1->DA1_QTDLOT := 999999.99 
			DA1->DA1_INDLOT := "000000999999999.99"
			DA1->DA1_MOEDA  := 1
			DA1->DA1_DATVIG := CTOD('')
		DA1->(MsUnLock())

		cMsgTxt	:= "DA1 inclu�da [" + RTrim(cItem) + "-" + RTrim(cCodProd) + "] Produto  := " + P02->P02_ITEM + " " + P02->P02_PROD + " R$ "+  LTrim(Str(nPreco,10,2))
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )

	Endif
	
Else

	IF SB0->( DbSeek( xFilial("SB0") + cCodProd ) )
		RecLock("SB0",.F.)
			SB0->B0_PRV1	:= nPreco  
		SB0->( MsUnLock() ) 	
	Else
		RecLock("SB0",.T.)  
			SB0->B0_FILIAL	:= xFilial("SB0")
			SB0->B0_COD		:= cCodProd
			SB0->B0_PRV1	:= nPreco 
		MsUnlock()

		cMsgTxt	:= "DA1 inclu�da [" + RTrim(cItem) + "-" + RTrim(cCodProd) + "] Produto  := " + P02->P02_ITEM + " " + P02->P02_PROD + " R$ "+  LTrim(Str(nPreco,10,2))
		u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , cMsgTxt )

	Endif

Endif

AEval(aAreas, {|x| RestArea(x)})

Return




/*/{Protheus.doc} TibVerifBlock()
Retorna erro fatal ocorrido no processo executado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/

User Function TibVerifBlock()( e , cTextoErro )

	Local lRet 		:= .F.
	Local cAutoGets	:= {}
		
	If e:gencode > 0 
    
		cTextoErro := "DESCRIPTION: " + e:DESCRIPTION + CRLF
		cTextoErro += "ERRORSTACK:" + CRLF
		cTextoErro += e:ERRORSTACK
	    	
		lRet:=.T.

		Break

	EndIf

Return lRet








/*/{Protheus.doc} CaUpdate()
Atualiza o Status de um Cupom envolvento todas as tabelas relacionadas
 
@author 	Mauro Paladini
@since 	13/02/2015
@version 	1.0
/*/


User Function CaUpdate( lSucesso , cStatus , cArqImp , cCupom , cNumOrc , cMsgLog )

Local aAreas  	:= { 	DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) ,;
						SA1->(GetArea()) , SF4->(GetArea()) , SX5->(GetArea()) , SE1->(GetArea()) ,;
						SA6->(GetArea()) , SLG->(GetArea()) , SLF->(GetArea()) , P01->(GetArea()) ,;
						P02->(GetArea()) , P03->(GetArea()) , SAE->(GetArea()) , GetArea()  }
Local cFilDoc	:= ''


Default lSucesso	:= .T.
Default cStatus		:= ""
Default cArqImp    	:= Space(TamSx3("P01_ARQ")[1])
Default cCupom		:= ""
Default cNumOrc		:= ""
Default cMsgLog		:= ""

Begin Transaction 

	u_WriteLog( "\log_pdv\orcamentos\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(P01->P01_DOC) , "Atualizando status dos registros P01 P02 P03 -> " + IIF( !lSucesso , "[COMERRO]" , "[OK]" ) )

	P01->( DbSetOrder(6) )
	
	IF P01->( DbSeek( xFilial("P01") + cArqImp + RTRim(cCupom) ) )
	
		cFilDoc	:= P01->P01_FILIAL

		RecLock("P01",.F.)
		
			P01->P01_STATUS	:= cStatus
			P01->P01_NUMORC	:= cNumOrc	
			P01->P01_LOGIMP	:= IIF( !lSucesso , cMsgLog , "" ) 		
		
			IF !lSucesso .And. ( cStatus == INVALIDO .Or. cStatus == COMERRO )
				P01->P01_LGPDV	:= "" 
				P01->P01_LGSERI	:= "" 
				P01->P01_LGCOD	:= "" 	
				P01->P01_OPERAD := ""					
			Endif
			
		MsUnLock()
	
		P02->( DbSetOrder(6) )
		IF P02->( DbSeek( cFilDoc + cArqImp + RTrim(cCupom) ) )
		
			While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + RTrim(P02_DOC) == cFilDoc + cArqImp + RTrim(cCupom) )
			
				RecLock("P02",.F.)
					
					P02->P02_NUMORC	:= cNumOrc
					
					IF lSucesso
						P02->P02_LOGIMP	:= ""
						P02->P02_STATUS	:= cStatus
					Endif
					
				MsUnLock()
				
				P02->( DbSkip() ) 						
			End
			
		Endif
		
		P03->( DbSetOrder(6) )
		
		IF P03->( DbSeek( cFilDoc + cArqImp + RTrim(cCupom) ) )
		
			While P03->( !Eof() .And.  P03_FILIAL + P03_ARQ + RTrim(P03_DOC) == cFilDoc + cArqImp + RTrim(cCupom) )
			
				RecLock("P03",.F.)
	
					P03->P03_STATUS	:= cStatus
					P03->P03_NUMORC	:= cNumOrc
	
					If lSucesso 
						P03->P03_LOGIMP	:= ""
					Endif
	
				MsUnLock()
				
				P03->( DbSkip() ) 						
			End
			
		Endif
		
	Endif
	
End Transaction

AEval(aAreas, {|x| RestArea(x)})

Return



Static Function AjustaSX3()

Local aArea := GetArea()

dbSelectArea('SX3')
dbSetOrder(2)

If SX3->( DbSeek("L2_OPC") )
	Reclock("SX3",.F.)
		REPLACE X3_USADO WITH '���������������'
	MsUnlock()
EndIf

If SX3->( DbSeek("LR_OPC") )
	Reclock("SX3",.F.)
		REPLACE X3_USADO WITH '���������������'
	MsUnlock()
EndIf

SX3->( dbSetOrder(1) )

RestArea(aArea)

Return





User Function TibDeletaOrc( cFilOrc , cNumOrc , cNumDoc )

Local aReg		:= { "P01" , "P02" , "P03" , "SM0" } 
Local aAlias	:= u_xGetArea(aReg)

Begin Transaction

SL1->( DbSetOrder(1) )
If SL1->( DbSeek( cFilOrc + cNumOrc  ) )
	Reclock("SL1",.F.)
	SL1->( dbDelete() )
	MsUnlock()	
Endif	

SL2->( DbSetOrder(1) )
If SL2->( DbSeek( cFilOrc + cNumOrc ) )	
	While SL2->( !Eof() .AND. L2_FILIAL+L2_NUM == cFilOrc+cNumOrc )		
		Reclock("SL2",.F.)
		SL2->( dbDelete() )
		MsUnlock()
		SL2->( dbSkip() )
	End
EndIf

SL4->( DbSetOrder(1) )
If SL4->( DbSeek( cFilOrc + cNumOrc ) )	
	While SL4->( !Eof() .AND. L4_FILIAL+L4_NUM == cFilOrc+cNumOrc )
		Reclock("SL4",.F.)
		SL4->( dbDelete() )
		MsUnlock()
		SL4->( dbSkip() )
	End
EndIf
	

P01->( DbSetOrder(3) )

IF P01->( DbSeek( cFilOrc + cNumOrc + cNumDoc ) )

	cArqImp	:= P01->P01_ARQ

	RecLock("P01",.F.)
		P01->P01_NUMORC	:= ""  	
		P01->P01_STATUS	:= COMERRO					
	MsUnLock()
	
	P02->( DbSetOrder(6) )	
		
	IF P02->( DbSeek( cFilOrc + cArqImp + cNumDoc ) )
		While P02->( !Eof() .AND. P02_FILIAL+P02_ARQ+ RTrim(P02_DOC) == cFilOrc + cArqImp + RTrim(cNumDoc) )			
			RecLock("P02",.F.)
				P02->P02_NUMORC	:= ""				
				P02->P02_ITORC	:= ""
			MsUnLock()
			P02->( DbSkip() )			
		End
	Endif


	P03->( DbSetOrder(6) )	
		
	IF P03->( DbSeek( cFilOrc + cArqImp + cNumDoc ) )
		While P03->( !Eof() .AND. P03_FILIAL+P03_ARQ+ RTrim(P03_DOC) == cFilOrc + cArqImp + RTrim(cNumDoc) )			
			RecLock("P03",.F.)
				P03->P03_NUMORC	:= ""
				P03->P03_VALOR	:= P03->P03_VLRPGTO
				P03->P03_RATGOR	:= 0
			MsUnLock()
			P03->( DbSkip() )			
		End
	Endif

Endif	

End Transaction

u_xRestArea(aAlias)

Return Nil