#INCLUDE "PROTHEUS.CH"

User Function SESIMPPV()

	

Return

Static Function procPVs()

	

Return

Static Function putPedVen( clPedido, alCab, alItens, nlOpc )

	local llProcOk	:= .T.
	local clMsgErro	:= ""
	local alErroAuto	:= {}

	default alCab	:= {}
	default alItens	:= {}
	default nlOpc	:= {}
	
	private lMsErroAuto		:= .F.
	private lMsHelpAuto		:= .T.
	private lAutoErrNoFil	:= .T.
	
	msExecAuto( { |x,y,z| MATA410(x,y,z)}, alCab, alItens, nlOpc )
	
	if ( lMsErroAuto )
		
		llProcOk := .F.
		
		alErroAuto := getAutoGRLog()
		aEval( alErroAuto, {|x| clMsgErro += allTrim( x ) + '<br/>'})
							
		conout("Falha na inclus�o do pedido [" + clPedido + "]")
		conout( clMsgErro )
		
	else
		conout("Pedido [" + clPedido + "] incluido com sucesso!")			
	endif
	

Return llProcOk