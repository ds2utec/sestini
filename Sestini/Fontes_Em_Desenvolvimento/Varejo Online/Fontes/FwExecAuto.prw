#include "protheus.CH"
#include "WSMAT110.CH"
#INCLUDE "APWEBSRV.CH"

User Function FwExecAuto()
Return 

Class FwExecAuto FROM LongClassName
    
    Data lMsErroAuto        AS Bolean
    Data lAutoErrNoFile     As Bolean
    Data lRestoreAlias      As Bolean
    Data lBlankFields       As Bolean
    Data lResult            As Bolean

    Data aAutoLog           As Array    
    Data aSaveAlias         As Array
    
    Data aHeader            AS Array
    Data aCols              AS Array

    Data aNoCabFields       As Array
    Data aNoGridFields      As Array    
    Data aCabFields         As Array
    Data aGridFields        As Array
    Data aFinFields			As Array
    
    Data aCabAuto		 	As Array
    Data aIteAuto		 	As Array
    Data aFinAuto			As Array
    Data ParamFun			As Array

    Data CabAlias           As String
    Data GridAlias          As String
    Data FinAlias			As String
    Data lSigaFin			As Bolean
    Data KeyPV9		 	 	As String
    Data FuncName           As String
    Data FuncMap			As String
    Data IdMapFields        As String
    Data Message			As String    

    Method New()      CONSTRUCTOR
    Method Load()
    Method Execute()
    Method Destroy()
      
EndClass



/*/{Protheus.doc} New
Contructor

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method New( cHeadAlias , cGridAlias , cFunUser , cKeyPV9 , cFunMap , cIdMapFld , lRestAreas , aSaveAlias , lBlankFields  )  Class FwExecAuto

Local nA    := 0
Local cAliasFin	:= ::FinAlias

Default cHeadAlias      := Nil
Default cGridAlias      := Nil
Default cFunUser        := Nil
Default cFunMap         := Nil
Default cIdMapFld       := Nil
Default lRestAreas      := .T.
Default aSaveAlias      := { 'PV1' , 'PV2' , 'PV4' , Alias() }
Default lBlankFields    := .F.    

If aSaveAlias[Len(aSaveAlias)] <> Alias()
    aAdd( aSaveAlias , Alias() )
Endif

For nA := 1 To Len(aSaveAlias)
    aSaveAlias[nA]  := (aSaveAlias[nA])->( GetArea() )  
Next nA

::lMsErroAuto        := .F.
::lAutoErrNoFile     := .T.
::lRestoreAlias      := lRestAreas
::lBlankFields       := lBlankFields
::lSigaFin			 := .F.


::aAutoLog           := {}    
::aSaveAlias         := aSaveAlias

::CabAlias           := cHeadAlias
::GridAlias          := cGridAlias
::FinAlias			 := 'PV3'

::FuncName           := cFunUser
::FuncMap            := cFunMap
::IdMapFields        := cIdMapFld

::ParamFun			 := Array(6)

::KeyPV9             := cKeyPV9

::aHeader            := {}
::aCols              := {}

::aNoCabFields       := Nil
::aCabFields         := Nil
::aGridFields        := Nil
::aFinFields		 := Nil
::aNoGridFields      := Nil   

::aCabAuto           := {}
::aIteAuto           := {} 
::aFinAuto			 := {}

::lResult            := .F.
::Message            := ''

Return Self
                    





/*/{Protheus.doc} Metodo Load
Carrega as propriedades do dicionario de campos conforme mapa de equivalencia

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method Load()   Class FwExecAuto

Local oModel    	:= Nil
Local nW			:= 0
Local nE			:= 0
Local aArea 		:= GetArea()
Local cTrb			:= GetNextAlias()
Local xValue		:= Nil
Local xField		:= ''
Local cFuncMap		:= ::FuncMap
Local cIdMapFld		:= ::IdMapFields

Local cAliasCab		:= ''
Local cAliasGrid	:= ''
Local cAliasFin		:= RTrim(::FinAlias)

Local aCposCab	:= {}
Local aCposDet	:= {}
Local aCposFin	:= {}

Local aAux		:= {}
Local aAuxCab	:= {}
Local aAuxDet	:= {}
Local aAuxFin	:= {}
Local aTeste	:= {}

::lResult	:= .T.

IF Select(cTrb) > 0
	(cTrb)->( DbCloseArea() ) 
Endif

BEGINSQL Alias cTrb

	SELECT 	RTrim(PV5_FIELD) 	AS PV5_FIELD, 
		   	RTrim(PV5_QFIELD) 	AS PV5_QFIELD,
		   	PV5_X2TAB
		   
	FROM %Table:PV5% PV5
	
	WHERE	PV5_FILIAL		 = %xFilial:PV5%  	        AND
			PV5_FUNOPC       = %Exp:cFuncMap%         	AND
			PV5_SEQ		     = %Exp:cIdMapFld%  	 	AND
			PV5_QFIELD		<> ''                       AND
			PV5.D_E_L_E_T_ = ''
			ORDER BY PV5_X2TAB

ENDSQL

// --  Alimenta os array com as configura��es de campos

(cTrb)->( DbGoTop() )

While (cTrb)->( !Eof() )

	IF UPPER( RTrim((cTrb)->PV5_X2TAB) )  $ 'PV1-PV4' 

		If ( Ascan( ::aNoCabFields , RTrim((cTrb)->PV5_QFIELD)) == 0 .And. Ascan( ::aNoCabFields , RTrim((cTrb)->PV5_FIELD ) ) == 0 )    
			aAdd( aCposCab , { (cTrb)->PV5_FIELD , (cTrb)->PV5_QFIELD } )
		Endif 				
	Else
		If ( Ascan( ::aNoCabFields , RTrim((cTrb)->PV5_QFIELD)) == 0 .And. Ascan( ::aNoCabFields , RTrim((cTrb)->PV5_FIELD ) ) == 0 )
			aAdd( aCposDet , { (cTrb)->PV5_FIELD , (cTrb)->PV5_QFIELD } )
		Endif
	Endif

	(cTrb)->( DbSkip() )

End

(cTrb)->( DbCloseArea() )

// -- Apos definir quais campos sao para o cabecalho e para os itens alimenta array com os dados  
// -- da salvos na tabela PV1 e PV2

PV1->(DbOrderNickName('PVSEQ00001'))
PV2->(DbOrderNickName('PVSEQ00001'))
PV3->(DbOrderNickName('PVSEQ00001'))
PV4->(DbOrderNickName('PVSEQ00001'))

::aCabFields	:= aClone(aCposCab) 
::aGridFields	:= aClone(aCposDet)

IF Len(aCposCab) > 0  

	cAliasCab	:= LEFT( ::CabAlias , 3 )
	
	IF (cAliasCab)->( DbSeek( xFilial( (cAliasCab) ) + ::KeyPV9 ) )
	
		IF cAliasCab == 'PV1' .And. PV1->PV1_TIPONF == 'N'
			::lSigaFin	:= IIF( PV1->PV1_TITULO == 's' , .T. , .F. )
			::FinAlias := 'PV3"
		Endif
		
		// -- Obtem conteudo da PV1 ou PV4 para alimentar no Array com o De/Para dos campos
		 
		For nW := 1 To Len(aCposCab)	
	
			xValue 	:= (cAliasCab)->( FieldGet( FieldPos( aCposCab[nW][1] ) ) )
			xField	:= RTRim( aCposCab[nW][2] )
			
			If ValType( xValue ) == 'C'
				xValue	:= AllTrim( xValue )
			Endif

			If ValType( xValue ) == 'C'
				xValue	:= AvKey( xValue , xField )
			Endif
					
			IF !Empty(xValue)		
				aAdd( aAuxCab , { xField , xValue , Nil , Nil } )
			Endif			 
	
		Next nW
				
		aEval(aAuxCab ,{|x| x[4] := Posicione("SX3", 2, x[1], "X3_ORDEM") })
	
		aAuxCab := aSort(aAuxCab,,,{|x,y| x[4] < y[4]})

		
		// -- GRID
		
		IF Len(aCposDet) > 0  .And. Len(aCposCab) > 0
		
			cAliasGrid	:= ::GridAlias
			cWhileKey	:= cAliasGrid + '_FILIAL + ' + cAliasGrid + '_SEQ'
				
			// -- Obtem conteudo da PV2 para alimentaro array de itens
			IF (cAliasGrid)->( DbSeek( xFilial( cAliasGrid ) + ::KeyPV9 ) )
		
				While (cAliasGrid)->( !Eof() .And. &(cWhileKey) == xFilial(cAliasGrid) + ::KeyPV9 )
					
					aAux := {}			
					For nW := 1 To Len(aCposDet)	
		
						IF 'PV2_ITEM' == RTrim(aCposDet[nW][1])						

							nTamItem	:= TamSX3(RTrim(aCposDet[nW][2]))[1]
							xValItem	:= Val( (cAliasGrid)->( FieldGet( FieldPos ( cAliasGrid + '_ITEM' ) ) ) )

							xValue 	:= StrZero(xValItem,nTamItem) 
							xField	:= RTRim( aCposDet[nW][2] )

						Else

							xValue 	:= (cAliasGrid)->( FieldGet( FieldPos( aCposDet[nW][1] ) ) )
							xField	:= RTRim( aCposDet[nW][2] )
						
						Endif

								
						If ValType( xValue ) == 'C'
							xValue	:= AvKey( xValue , xField )
						Endif
		
						IF !Empty(xValue)		
							aAdd( aAux , { xField  , xValue , Nil , Nil } )
						Endif			 
		
					Next nW			
						
					
					aEval(aAux ,{|x| x[4] := Posicione("SX3", 2, x[1], "X3_ORDEM") })				
					aAux := aSort(aAux,,,{|x,y| x[4] < y[4]})
					
					aAdd( aAuxDet , aAux  )
					
					(cAliasGrid)->( DbSkip())
					  
				End	
				
				
				// -- Titulos				
				
				IF ::lSigaFin 
				
					aAdd ( aCposFin , { "PV3_CARTEI" 	, "E2_CARTEI" 		} )
					aAdd ( aCposFin , { "PV3_EMISSA" 	, "E2_EMISSAO" 		} )
					aAdd ( aCposFin , { "PV3_DOC" 		, "E2_DOC" 			} )
					aAdd ( aCposFin , { "PV3_SERNF" 	, "E2_SERIE" 	  	} )
					aAdd ( aCposFin , { "PV3_TIPO"		, "E2_TIPO" 		} )
					aAdd ( aCposFin , { "PV3_PARCEL" 	, "E2_PARCELA" 	 	} )
					aAdd ( aCposFin , { "PV3_VALOR"		, "E2_VALOR" 		} )
					aAdd ( aCposFin , { "PV3_VENC" 		, "E2_VENCTO" 		} )
				
					// -- Obtem conteudo da PV2 para alimentaro array de itens

					cWhileKey	:= cAliasFin + '_FILIAL + ' + cAliasFin + '_SEQ'
					
					IF (cAliasFin)->( DbSeek( xFilial( cAliasFin ) + ::KeyPV9 ) )
				
						While (cAliasFin)->( !Eof() .And. &(cWhileKey) == xFilial(cAliasFin) + ::KeyPV9 )
							
							aAux := {}			
							For nW := 1 To Len(aCposFin)	

								xValue 	:= (cAliasFin)->( FieldGet( FieldPos( aCposFin[nW][1] ) ) )
								xField	:= RTRim( aCposFin[nW][2] )
								
								If ValType( xValue ) == 'C'
									xValue	:= AvKey( xValue , xField )
								Endif
				
								IF !Empty(xValue)		
									aAdd( aAux , { xField  , xValue , Nil , Nil } )
								Endif			 
				
							Next nW			
								
							
							aEval(aAux ,{|x| x[4] := Posicione("SX3", 2, x[1], "X3_ORDEM") })				
							aAux := aSort(aAux,,,{|x,y| x[4] < y[4]})
							
							aAdd( aAuxFin , aAux  )
							
							(cAliasFin)->( DbSkip())
							  
						End	

					Else
					
						// -- ERRO : TITULOS
					
						aAuxCab		:= {}
						AAuxDet		:= {}	
						aAuxFin		>= {}	
			
						::Message	:= 'N�o encontrado registros dos titulos ' + cAliasFin + ' Verifique tabelas de integra��o. Sequence ID: ' + ::KeyPV9 			
						::lResult	:= .F.
					
					Endif
					
				Endif				
		
			Else
				
				// -- ERRO : CANE�ALHO SEM ITENS
				// -- N�o encontrou os detalhes do documento por�m tem cabe�alho 
			
				aAuxCab		:= {}
				AAuxDet		:= {}	
				aAuxFin		>= {}	
	
				::Message	:= 'N�o encontrado registros do GRID ' + cAliasGrid + ' relacionados ao cabe�alho ' + cAliasCab + ' da transa��o. Verifique tabelas de integra��o. Sequence ID: ' + ::KeyPV9 			
				::lResult	:= .F.
			
			Endif
		
		Endif
		
	Endif

Endif

RestArea( aArea)

::aCabAuto	:= aClone(aAuxCab)
::aIteAuto	:= aClone(aAuxDet)
::aFinAuto	:= aClone(aAuxFin)


Return ::lResult



/*/{Protheus.doc} Execute
Processa a Execauto 

@type		class
@author	Mauro Paladini
@since	01/10/2018
@version	1.0
/*/

Method Execute()    Class FwExecAuto

Local nX		:= 0
Local cFuncao	:= ::FuncName
Local cMsgErro	:= ''
Local aErro		:= {}
Local cFilSave	:= cFilAnt 

Local aPEResult		:= {}
Local lPEResult		:= .T.
Local cPEMessage	:= ''

Private aPar		:= aClone( ::ParamFun )
Private aCabAuto	:= aClone( ::aCabAuto )
Private aITAuto		:= aClone( ::aIteAuto )
Private bExAuto		:= { || }

Private lMsHelpAuto     	:= .T.
Private lMsErroAuto     	:= .F.
Private lAutoErrNoFile		:= ::lAutoErrNoFile

IF !Empty( PV9->PV9_FILORI )
	cFilAnt	:=	PV9->PV9_FILORI
Endif 

bExAuto := &( "{|| MSExecAuto( {|a,b,c,d,e,f,g| " + AllTrim(cFuncao) + "(a,b,c,d,e,f,g)}," + (aPar[1]) + "," + (aPar[2]) + "," + (aPar[3]) + "," + (aPar[4]) + "," + (aPar[5]) + " ) }" )
Eval( bExAuto ) 

If lMsErroAuto

	// -- Processa retorno
	
	aErro 	:= GetAutoGRLog() 
	cMsgErro	:= ""
	
	For nX := 1 To Len(aErro)			
	   	cMsgErro += alltrim(aErro[nX])+Chr(13)+Chr(10)			
	Next nX 
	
	cMsgErro := StrTran(cMsgErro,"<","")
	cMsgErro := StrTran(cMsgErro,">","") + CRLF
	
	// -- Grava Log do processamento
	
	cAliasCab	:= LEFT( ::aCabFields[1][1] , 3 )	

	(cAliasCab)->(DbOrderNickName('PVSEQ00001'))
	
	IF (cAliasCab)->( DbSeek( xFilial( cAliasCab ) + ::KeyPV9 ) )
		RecLock(cAliasCab,.F.)
			(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_LOGIMP' ) , cMsgErro ) )
		MsUnLock()
	Endif
	
	::lMsErroAuto	:= lMsErroAuto
	::aAutoLog		:= aErro
	::Message  		:= cMsgErro
	::lResult   	:= .F.
			
Else

	::lMsErroAuto	:= lMsErroAuto
	::lResult   	:= .T.
	::Message  		:= ''
	::aAutoLog		:= {}


	// PE FWEXEPOS
	// Executadp apos a execu��o da MsExecAuto
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	If ExistBlock( 'FWEXEPOS' ) 	
	  	  	
	  	aPEResult	:= ExecBlock( 'FWEXEPOS' , .F. , .F. , { Self } )  	

	  	lPEResult 	:= aPEResult[1]	  	 
		cPEMessage	:= aPEResult[2]
		
		If !lPEResult
			::Message  		:= cPEMessage
			::lResult   	:= .F.
		Endif
		  		  		
	Endif

Endif


cFilAnt := cFilSave 

// -- Restaura os ponteiros 
If ::lRestoreAlias
    AEval( ::aSaveAlias , {|x| RestArea(x)}) 
Endif

Return Nil


/*/{Protheus.doc} Destroy
Elimina objeto e libera variaveis

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method Destroy() Class FwExecAuto

::lMsErroAuto        := .F.
::lAutoErrNoFile     := .T.
::lRestoreAlias      := .F.
::lBlankFields       := .F.
::lSigaFin			 := .F.

::aAutoLog           := {}
::aSaveAlias         := {}

::CabAlias           := ''
::GridAlias          := ''
::FinAlias			 := ''
::FuncName           := ''
::FuncMap            := ''
::IdMapFields        := ''
::KeyPV9             := ''

::aHeader            := {}
::aCols              := {}

::aNoCabFields       := Nil
::aCabFields         := Nil
::aGridFields        := Nil
::aFinFields		 := Nil
::aNoGridFields      := Nil

::aCabAuto           := {}
::aIteAuto           := {}
::aFinAuto			 := {}

::lResult            := .F.
::Message            := ''


Return Nil 