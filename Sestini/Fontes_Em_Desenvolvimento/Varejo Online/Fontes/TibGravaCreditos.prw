#INCLUDE 'TOTVS.CH'

/*/{Protheus.doc} LjGrvCred
Long description

@type 	    User Function
@author 	Mauro Paladini
@since 	    Jan 16, 2019  11:01:24 AM
/*/

User Function LjGrvCred( aNccItens , nNccUsada , nNccGerada )

Local cWhileSE1 := "xFilial('SE1') + SF2->F2_PREFIXO + SL1->L1_DOC + cParcela + 'CR '"
Local nTamCodSAE        := TamSx3("AE_COD")[1]                                  // Tamanho do campo AE_COD
Local nTamE1_PREFIXO    := TamSX3("E1_PREFIXO")[01]                             // Tamanho do campo E1_PREFIXO
Local nTamE1_PARCELA    := TamSX3("E1_PARCELA")[01]                             // Tamanho do campo E1_PARCELA
Local nTamA6_AGENCIA    := TamSX3("A6_AGENCIA")[1]                              // Tamanho do campo A6_AGENCIA
Local nRndMoed1         := MsDecimais(1)                                        // Arredondamento para localizacoes
Local cParcela  		:= PadR(SuperGetMV("MV_1DUP"), nTamE1_PARCELA) // Ajusta de acordo com o tamanho do E1_PARCELA
Local cAgenciaCx		:= '.'
Local cContaCx      	:= "."													// Caixa
Local cOrigem 			:= "LOJA701"

Private nMoedaCor 		:= 1
    
//|  1 - Valor pago em Credito


SE1->(DbSetOrder(1))
While SE1->(DbSeek(cWhileSE1))
    cParcela := CHR(ASC(cParcela)+1)
End

SX5->(DbSeek(xFilial("SX5")+"24"+PADR("CR",6)))
cNatureza := If(Empty(SX5->X5_DESCRI),"CREDITO",SX5->X5_DESCRI)

If nMoedaCor > 1
   cAgenciaCx  := PadR(SuperGetMV("MV_SIMB"+Str(nMoedaCor,1)),nTamA6_AGENCIA)
   SA6->(DbSetOrder(1))
   If SA6->(DbSeek(xFilial("SA6")+ SL1->L1_OPERADO + cAgenciaCx))
      cContaCx  := SA6->A6_NUMCON
   EndIf   
EndIf

RecLock( "SE1",.T.)
REPLACE SE1->E1_FILIAL      WITH xFilial("SE1")

REPLACE SE1->E1_PREFIXO     WITH SF2->F2_PREFIXO
REPLACE SE1->E1_NUM         WITH SL1->L1_DOC

REPLACE SE1->E1_PARCELA     WITH cParcela
REPLACE SE1->E1_TIPO        WITH "CR"
REPLACE SE1->E1_NUMNOTA     WITH SL1->L1_DOC

REPLACE SE1->E1_SERIE       WITH SL1->L1_SERIE

REPLACE SE1->E1_FILORIG     WITH cFilAnt
REPLACE SE1->E1_NATUREZ     WITH cNatureza
REPLACE SE1->E1_PORTADO     WITH SL1->L1_OPERADO
REPLACE SE1->E1_AGEDEP      WITH cAgenciaCx
REPLACE SE1->E1_CONTA       WITH cContaCx
REPLACE SE1->E1_MOEDA       WITH nMoedaCor
REPLACE SE1->E1_CLIENTE     WITH SL1->L1_CLIENTE
REPLACE SE1->E1_LOJA        WITH SL1->L1_LOJA
REPLACE SE1->E1_NOMCLI      WITH SA1->A1_NREDUZ

REPLACE SE1->E1_EMISSAO     WITH SL1->L1_EMISSAO
REPLACE SE1->E1_EMIS1       WITH SL1->L1_EMISSAO
REPLACE SE1->E1_VENCTO      WITH SL1->L1_EMISSAO  

REPLACE SE1->E1_VENCREA     WITH DataValida(SE1->E1_VENCTO,.T.)
REPLACE SE1->E1_VENCORI     WITH dDataBase
REPLACE SE1->E1_MOVIMEN     WITH SE1->E1_BAIXA  

REPLACE SE1->E1_VALOR       WITH SL1->L1_CREDITO
REPLACE SE1->E1_SALDO       WITH SE1->E1_VALOR
REPLACE SE1->E1_STATUS      WITH "A"
REPLACE SE1->E1_VLCRUZ      WITH Round(xMoeda(SE1->E1_VALOR, SE1->E1_MOEDA,1, SE1->E1_EMISSAO),nRndMoed1)
REPLACE SE1->E1_VALLIQ      WITH SE1->E1_VALOR
REPLACE SE1->E1_SITUACA     WITH IIf(FindFunction("LjCartTit"),LjCartTit(SL1->L1_NUM,"CR",SE1->E1_PORTADO),"0")
REPLACE SE1->E1_ORIGEM      WITH cOrigem
REPLACE SE1->E1_MULTNAT     WITH "2"
REPLACE SE1->E1_FLUXO       WITH "S"

DbSelectArea("SED")
DbSetOrder(1)
DbSeek(xFilial("SED")+SE1->E1_NATUREZ)            

SE1->(MsUnlock())

cDocCred        := SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_LOJA
nRecnoSE1       := SE1->(Recno())

/*/
?ExpA1  - Array que contem todas as NCC selecionadas na venda
?ExpN2  - Numero de Casas que deve ser arredondada a moeda   
?ExpC3  - Dado a ser gravado no campo documento do se5      
?ExpN4  - Numero do registo que foi incluido a SE1         
?ExpN5  - NCC Usada                                        
?ExpN6  - NCC Gerada                                          
/*/
    
LjXInMovNCC( aNccItens,  nRndMoed1,  cDocCred,;
            nRecnoSE1,  nNccUsada,  nNccGerada )
            
        
        
Return





/*/{Protheus.doc} ${LjXInMovNCC}
Inclui na SE5 os dados da NCC e baixa os titulos da SE1

@type 	User Function
@author Mauro Paladini
@since 	Jan 16, 2019  11:17:23 AM
@version 1.0
@return ${return}, ${return_description}

?ExpA1  - Array que contem todas as NCC selecionadas na venda
?ExpN2  - Numero de Casas que deve ser arredondada a moeda   
?ExpC3  - Dado a ser gravado no campo documento do se5      
?ExpN4  - Numero do registo que foi incluido a SE1         
?ExpN5  - NCC Usada                                        
?ExpN6  - NCC Gerada                                          

/*/

Static Function LjXInMovNCC(    aNccItens,  nRndMoed1,  cDocCred,   nRecnoSE1, ;
                                nNccUsada,  nNccGerada) 

Local nX           
Local cDocNCC                                           // chave para identificacao da NCC no SE1
Local cTipoDoc                                          // Variavel de gravacao na SE5 no campo E5_TIPODOC   
Local cBenef            := Space(1)                     // Variavel de gravacao na SE5 no campo E5_BENEF   
Local nValor                                            // Variavel de gravacao na SE5 no campo E5_VALOR
Local cMoeda                                            // Variavel de gravacao na SE5 no campo E5_MOEDA
Local nSaldo                                            // Variavel de gravacao na SE5 no campo E5_VLMOED2
Local nTPCompNCC        := SuperGetMV("MV_LJCPNCC",,1)  //Tratamento para compensacao de NCC 1-Compensacao atual 2 - Nova Compensacao
Local nDecs             := 2
Local lGravaSE5         := .T.                          // Se deve gravar o SE5
Local lBaixarNCC        := .T.                          // baixa o NCC total ou parcial
Local nValorNCCAtual    := 0                            // Valor da NCC Atual
Local nValorNCC         := 0                            // Valor a ser Baixado nas ncc   
Local cSeqBxCp          := ""                           // Numero sequencial da baixa

// Valor  que deve ser baixado das NCC
nValorNCC = nNccUsada - nNccGerada

For nX := 1 to Len(aNccItens)

    If aNccItens[nX][1]     
    
        SE1->(DbGoTo(aNccItens[nX][5]))
                    
        cDocNCC := SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+SE1->E1_TIPO+SE1->E1_LOJA

        cBenef := Space(1)  
        If SE1->E1_TIPO == "NCC"
            cTipoDoc := "BA"

            //?oloca o nome do cliente no E5_BENE

            SA1->( DbSetOrder( 1 ) )
            If SA1->( DbSeek( xFilial( "SA1" ) + SE1->E1_CLIENTE + SE1->E1_LOJA ) )
                cBenef := SA1->A1_NOME
            EndIf
        Else
            cTipoDoc := "CP"
        EndIf
                
        lGravaSE5 := .T.
        If ( nTPCompNCC == 1 ) .OR. ( nTPCompNCC == 3 ) .OR. ( nTPCompNCC == 4 )         
            nValor  := Round(xMoeda(SE1->E1_SALDO,SE1->E1_MOEDA,1,dDataBase,nRndMoed1+1),nRndMoed1)
        Else    
            If nValorNCC == 0
                lGravaSE5 := .F.
            ElseIf nValorNCC >= aNCCItens[nX][2]
                lBaixarNCC := .T.
                nValorNCC -= aNCCItens[nX][2]   
                If cPaisLoc == "BRA"
                    nValor := Round(xMoeda(aNccItens[nX][6],SE1->E1_MOEDA,1,dDataBase,nRndMoed1+1),nRndMoed1)
                Else 
                    nDecs  := MsDecimais(SE1->E1_MOEDA)         
                    nValor := Round(xMoeda(aNccItens[nX][6],aNccItens[nX][8],1,dDataBase,nRndMoed1+1),nRndMoed1)                    
                EndIf
            Else
                lBaixarNCC := .F.
                If cPaisLoc == "BRA"
                    nValor := Round(xMoeda(nValorNCC,SE1->E1_MOEDA,1,dDataBase,nRndMoed1+1),nRndMoed1)
                Else 
                    nDecs  := MsDecimais(SE1->E1_MOEDA)         
                    nValor := Round(xMoeda(nValorNCC,aNccItens[nX][8],1,dDataBase,nRndMoed1+1),nRndMoed1)                   
                EndIf
                nValorNCCAtual := nValorNCC
                nValorNCC := 0
            EndIf
                
        EndIf

        //? Verifica a proxima sequencia de baixa                     
        cSeqBxCp := FaNxtSeqBx()

        // -- iNSERE REGISTRO NA se5        
        If lGravaSE5
            LjInSE5(SL1->L1_EMISNF,         SE1->E1_TIPO,       cTipoDoc,               cBenef, ;  
                    nValor,                 "01",               SE1->E1_SALDO,          dDataBase, ;
                    SE1->E1_NATUREZ,        SE5->E5_DATA,       SL1->L1_OPERADO,        ".", ;
                    ".",                    "R",                "CMP",                  SE1->E1_PREFIXO, ;      
                    SE1->E1_NUM,            SE1->E1_PARCELA,    SE1->E1_CLIENTE,        SE1->E1_CLIENTE, ;
                    SE1->E1_LOJA,           "COMP.P/ VENDA LOJA",cDocCred,               cSeqBxCp)
        EndIf

                           
        If lGravaSE5
            RecLock("SE1",.F.)

            If lBaixarNCC 
                REPLACE SE1->E1_SALDO   WITH 0
                REPLACE SE1->E1_STATUS  WITH "B"
                REPLACE SE1->E1_VALLIQ  WITH SE1->E1_VALOR
            Else 
                REPLACE SE1->E1_SALDO   WITH aNCCItens[nX][2] - nValorNCCAtual
                REPLACE SE1->E1_VALLIQ  WITH SE1->E1_VALLIQ + nValorNCCAtual
            EndIf  
            
            REPLACE SE1->E1_BAIXA   WITH SL1->L1_EMISNF
            REPLACE SE1->E1_MOVIMEN WITH SL1->L1_EMISNF
            SE1->(MsUnlock())
        EndIf
        
        If nRecnoSE1 > 0
            SE1->(DbGoTo(nRecnoSE1))
        EndIf

        cBenef := Space(1)      
        If SE1->E1_TIPO == "NCC"
            cTipoDoc := "BA"
            // ?oloca o nome do cliente no E5_BENEF            
            SA1->( DbSetOrder( 1 ) )
            If SA1->( DbSeek( xFilial( "SA1" ) + SE1->E1_CLIENTE + SE1->E1_LOJA ) )
                cBenef := SA1->A1_NOME
            EndIf
        Else
            cTipoDoc := "CP"
        EndIf

        If nTPCompNCC == 1
            lGravaSE5 := .T.
            nValor   := xMoeda(aNccItens[nX][2],SE1->E1_MOEDA,1)
        EndIf
        cMoeda   := Nil
        nSaldo   := aNccItens[nX][2]
        

        // --- Grava registro na SE5
        If lGravaSE5
            LjInSE5(dDataBase,          SE1->E1_TIPO,       cTipoDoc,           cBenef, ;  
                    nValor,             cMoeda,             nSaldo,             dDataBase, ;
                    SE1->E1_NATUREZ,    SE5->E5_DATA,       SE1->E1_PORTADO,    ".", ;
                    ".",                "R",                "CMP",              SE1->E1_PREFIXO, ;      
                    SE1->E1_NUM,        SE1->E1_PARCELA,    SE1->E1_CLIENTE,    SE1->E1_CLIENTE, ;      
                    SE1->E1_LOJA,       "COMP.P/ VENDA LOJA",            cDocNCC,            cSeqBxCp)
        EndIf
        
        RecLock("SE1",.F.)
        SE1->E1_SALDO := 0
        If SE1->E1_SALDO = 0
            REPLACE SE1->E1_STATUS  WITH "B"
            REPLACE SE1->E1_BAIXA   WITH dDataBase
            REPLACE SE1->E1_MOVIMEN WITH dDataBase
            REPLACE SE1->E1_VALLIQ  WITH SE1->E1_VALOR
        EndIf
        MsUnlock()
        
    EndIf
Next nX

Return NIL



/*/{Protheus.doc} LjInSE5
Realiza inclus�o da SE5

@type 	    User Function
@author 	Mauro Paladini
@since 	    Jan 16, 2019  11:26:45 AM
@version    1.0
/*/

Static Function LjInSE5(dData,      cTipo,      cTipoDoc,   cBenef, ;  
                        nValor,     cMoeda,     nSaldo,     dDtDigit, ;
                        cNaturez,   dDtDispo,   cBanco,     cAgencia, ;
                        cConta,     cRecPag,    cMotBX,     cPrefixo, ;     
                        cNumero,    cParcela,   cCliFor,    cCliente, ;     
                        cLoja,      cHistor,    cDocument,  cSeq)
Default cSeq := ""

RecLock("SE5",.T.)
SE5->E5_FILIAL  := xFilial("SE5")
SE5->E5_FILORIG := cFilAnt

REPLACE SE5->E5_DATA    WITH dData  
REPLACE SE5->E5_TIPO    WITH cTipo    
REPLACE SE5->E5_BENEF   WITH cBenef
REPLACE SE5->E5_TIPODOC WITH cTipoDoc
REPLACE SE5->E5_VALOR   WITH nValor
REPLACE SE5->E5_MOEDA   WITH cMoeda
REPLACE SE5->E5_VLMOED2 WITH nSaldo  
REPLACE SE5->E5_DTDIGIT WITH dDtDigit
REPLACE SE5->E5_NATUREZ WITH cNaturez
REPLACE SE5->E5_DTDISPO WITH dDtDispo
REPLACE SE5->E5_BANCO   WITH cBanco 
REPLACE SE5->E5_AGENCIA WITH cAgencia
REPLACE SE5->E5_CONTA   WITH cConta 
REPLACE SE5->E5_RECPAG  WITH cRecPag
REPLACE SE5->E5_MOTBX   WITH cMotBX 
REPLACE SE5->E5_PREFIXO WITH cPrefixo
REPLACE SE5->E5_NUMERO  WITH cNumero
REPLACE SE5->E5_PARCELA WITH cParcela
REPLACE SE5->E5_CLIFOR  WITH cCliFor
REPLACE SE5->E5_CLIENTE WITH cCliente
REPLACE SE5->E5_LOJA    WITH cLoja  
REPLACE SE5->E5_HISTOR  WITH cHistor
REPLACE SE5->E5_DOCUMEN WITH cDocument

If cTipo $ "NCC/CR "
    REPLACE SE5->E5_SEQ     WITH cSeq 
EndIf

SE5->(MsUnlock())

Return NIL
