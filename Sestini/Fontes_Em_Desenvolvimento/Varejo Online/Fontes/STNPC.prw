#Include 'Protheus.ch'
#include 'tbiconn.ch'

#DEFINE NARRAY  12

#DEFINE ITEM    1
#DEFINE PRODUTO 2
#DEFINE QTDE    3
#DEFINE PRCVEN  4
#DEFINE PRCUNIT 5
#DEFINE VLRITEM 6
#DEFINE B2LOCAL 7
#DEFINE LOTE    8
#DEFINE IDENTB6 9
#DEFINE REFORI  10
#DEFINE REFITEM 11
#DEFINE TES     12



/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnpc_integration.log'
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'



/*/{Protheus.doc} STNPC
Interface de integra��o de Pedidos

@type   User Function
@author Mauro Paladini
@since  Aug 17, 2018  2:33:31 PM
@version 1.0
@return     ${return}, ${return_description}

/*/

User Function STNPC( CRPCEmp, cRPCFil )

    Local aArea         := GetArea()
    Local aTCLink       := { {0,0} }
    Local lCriouAmb     := .F.
    Local oConnect      := Nil

    Local cQuery        := ''
    Local cAlias        := ''
    Local cSQL          := ''
    Local lSQL          := .T.
    Local lBlqSequen    := .F.
    Local cAgrupador    := ''
    Local cCNPJ         := ''

    Local cCodPva       := "STNPC"
    Local lRunning		:= .T.

	Private cTagLog		:= '[' + cCodPva + ' ' + SM0->M0_CODFIL + ' ' + RTRIM(SM0->M0_FILIAL)  +  ']'
    Private cJobLog		:= cDirLog + cCFGLog


    MontaDir(cDirLog)

    IF cRPCEmp <> Nil .And. cRPCFil <> Nil
        RPCSetType(3)
        RPCSETENV(cRPCEmp, cRPCFil)
        u_WriteLog( cJobLog  , '[PREP-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        lCriouAmb   := .T.
    Endif

    // -- Valida conexao com DBAccess
    u_WriteLog( cJobLog  , '[FWDB000ACCESS] Iniciando conex�o com DbAccess ',.T.,.T. )

    // -- Verifica conexao com DBaccess da Integra��o
    If ! U_ODBCConnect( @oConnect )

        If lCriouAmb
            u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
            RESET ENVIRONMENT
        Endif
        Return

    Endif

    If IsBlind()
    
        u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )
        
        cCNPJ   := RTrim(SM0->M0_CGC)
    
        ProcFilial( @oConnect , cCNPJ )
                            
    Else
    
		u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )

        cCNPJ   := RTrim(SM0->M0_CGC)
    
        FWMsgRun( , {|| ProcFilial( @oConnect , cCNPJ ) } ,, "Processando .. "  )
    
    Endif

    // -- Finaliza conex�es realizadas

    oConnect:CloseConnection()
    oConnect:Finish()
    FreeObj(oConnect)

    If lCriouAmb
        u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        RESET ENVIRONMENT
    Endif

Return





/*/{Protheus.doc} ProcFilial
Fun��o Auxiliar

@type       Function
@author     Mauro Paladini
@since      Aug 17, 2018  2:33:31 PM
@version    1.0
@return     ${return}, ${return_description}
/*/

Static Function ProcFilial( oConnect , cCNPJ )

    Local aAreas    := { PV1->(GetArea()) , PV2->(GetArea()) , PV9->(GetArea()) , GetArea() }
    Local cQuery    := ''
    Local cNFS      := ''   // Cabecalhos
    Local cITE      := ''   // Itens
    Local cQRY      := ''
    Local lRet      := .T.
    Local cSQL      := ''

    // -- Pedido
    Local cNotaFis  		:= ''
    Local cIDNota   		:= ''
    Local cItem     		:= '01'
    Local aProdutos 		:= {}

    Local cQryPath      	:= '\user_procedures\'
    Local cIDCliente    	:= ''
    Local oSestini      	:= Nil
    Local cChave        	:= ''
    Local cOperacao			:= ''
    Local cNumPedido		:= ''

  // -- 1=Inclus�o;2=Altera��o;3=Exclus�o   
    
    Local cIdMapFld     	:= GetMv('ES_MPC1',,'0000000001') 		// -- C�digo do cadasstro 
	Local xST_CONDPG		:= GetMv('ES_STNCND',,'10')				// -- Condicao de pagamento Default
	Local xST_NATURE		:= GetMv('ES_SRNNAT',,'2002')			// -- Natureza Padrao
	Local xST_TESSAI		:= GetMv('ES_TESNFS',,'501')			// -- TES Defauls
	Local cNaturez			:= ''
	
	Local nPosCond			:= 0
	Local nPosNat			:= 0
	
    Local cViewSQL			:= 'VW_STNPC_ITEM.SQL'					// -- Nome do arquivo .SQ� referente a View dos registros    
    Local cAutoFunc			:= 'SAUTOPC'							// -- User Function registrada no Map Field PV5     
    Local cMapSeek			:= xFilial('PV5') + AvKey( 'U_' + cAutoFunc , 'PV5_FUNOPC' ) + cIdMapFld

	Local cToken			:= ''
    Local cKeyOrig      	:= ''
    Local cTabOrig      	:= 'SC7'
    Local nIndOrig      	:= 1
    Local nRecSave			:= ''
    Local nCpos         	:= 0
    Local aItens			:= {}
    Local aAux				:= {}
    Local aAuxItens     	:= {}

    Local aColsCab			:= {}
    Local aColsGrid			:= {}
    
    Local aCposCab			:= {} 
    Local aCposDet			:= {}

    // 1=Sim 2=Nao
    Local cBlqProx  := '1'

    oSestini  := Nil
    cOperacao := '3'
 
	If Empty ( cIdMapFld ) .OR. PV5->( ! DbSeek( cMapSeek ) )  
	    u_WriteLog( cJobLog  , '[' + cTagLog + '] ERRO Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek  ,.T.,.T. )
		u_WriteLog( cJobLog  , '[' + cTagLog + '] Processo cancelado'  ,.T.,.T. )
	    Return .F.
	Endif	
 
    // -- Conex�o realizada com sucesso ( Tudo deve ser feito dentro do Objeto oConnect )
    IF ( oConnect:HasConnection() .or. oConnect:OpenConnection() )
   
		// -- QUERY PED.COMPRA     

        cQuery := u_BIBuildQuery( cViewSQL )
        cQuery := u_DePara( {{ "#CNPJ#" , cCNPJ }} , cQuery )            

	
		// -- FWDBACESS QUERY 
		
        cQRY   := GetNextAlias()
        oConnect:NewAlias( cQuery , cQRY )
       
        If oConnect:HasError()
            u_WriteLog( cJobLog  , oConnect:ErrorMessage()  , .T. , .T. ,,,, .T. /*lFunName*/  )
            Return
        EndIf
        
        IF (cQRY)->( !Eof() )
            u_WriteLog( cJobLog  , '[STNPC] QUERY .. [OK] ' ,.T.,.T. )
        Else
            u_WriteLog( cJobLog  , '[STNPC] QUERY .. [N�o h� dados] ' ,.T.,.T. )
        Endif
        
        // -- La�o para o cabe�alho dos documentos        
        
        (cQRY)->( DbGoTop() )    
        
        While (cQRY)->( !Eof() )
                    
			// -- Tratamento especial para gravar Token
			
			cToken		:= ''
            oSestini    := Nil
            cOperacao   := '3'	
        	cNumPedido	:= ''
        	        
			IF (cQRY)->( FieldPos('TOKEN') > 0 )
				cToken			:= RTrim( (cQRY)->TOKEN )
			Endif 

            nIdOrig		:= (cQRY)->IDREGORIG
            cOrigem		:= 'MATA120'		// ##CFG
            cChave      := xFilial(cFilAnt) + RTrim( (cQRY)->NUM_PEDIDO )      // -- Chave agrupadora
            
            cNumPedido	:= ALLTRIM( (cQRY)->NUM_PEDIDO )
            cKeyOrig    := AllTrim ( cValToChar((cQRY)->IDREGORIG ) )
            nIndOrig    := 1
          
            u_WriteLog( cJobLog  , cTagLog + ' Integrando ' + ALLTRIM((cQRY)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar((cQRY)->( IDREGORIG )) ) ,.T.,.T. )
                    
            
            // -- Instancia o objeto da classe de integra��o        

            oSestini := TransactRows():New( cOrigem ) 
            oSestini:SetVariables( cOrigem , cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBlqProx , Nil , cToken  )
            oSestini:cFunopc  			:=  cAutoFunc
            
            aRetFor						:= u_CliForAuto( ALLTRIM((cQRY)->CGCFORNECE) , 2 )
            oSestini:CGCFornecedor      := ALLTRIM((cQRY)->CGCFORNECE)            
            oSestini:CodFornecedor      := aRetFor[1]
            oSestini:LojaFornecedor     := aRetFor[2]	            
			oSestini:Token				:= cToken

            oSestini:DataEmissao        := CTOD((cQRY)->DTEMISSAO)
            oSestini:NumPedido 		   	:= ALLTRIM((cQRY)->NUM_PEDIDO)

            oSestini:TabelaIntegracao   := ALLTRIM((cQRY)->TABORIGEM)
            oSestini:CampoFlagProcessado:= ALLTRIM((cQRY)->CPOSTATUS)
            oSestini:IDRegistro         := (cQRY)->IDREGORIG
            

			// Base MapField cadastrado na PV5
			
            oSestini:PrepareAuto( "U_" + cAutoFunc , cIdMapFld )
            
            // -- Se tem Mapa de campos cadastrado foi p�ssivel montar os arrays
            
            aCposCab	:= aClone(oSestini:aCposCab)
            aCposDet	:= aClone(oSestini:aCposDet)

			aColsCab	:= {}
		    aColsGrid	:= {}

			aAux		:= {}
		
			// -- 06 Prepara aHeader para cabecalho (PAI) ##CGF

			IF Len(aCposCab) > 0 			
				
				// -- aCposCab[nCpos][2]   -->  Utiliza a posi��o [2] pois neste momento faz a leitura do campo da query  PV5_QFIELD  
				// -- Valida se o campo de relacionamento esta preenchido
	
				For  nCpos := 1 To Len(aCposCab)
					xCpoValue   := (cQRY)->( FieldGet( FieldPos( RTrim( aCposCab[nCpos][2] ) ) ) )
	     			aAdd( aColsCab , xCpoValue )
	 			Next nCpos  
	 			     			
	 			// -- Condicao de Pagamento Padrao
	 			nPosCond := Ascan( aCposCab , 'PV1_CONDPG' )
	
	 			If nPosCond == 0
	 				aAdd( aCposCab , { 'PV1_CONDPG' , 'COND' } )  
	 				aAdd( aColsCab , xST_CONDPG  )
	 			Else
	 				IF Empty( RTrim( aColsCab[nPosCond] ) )
	 					aColsCab[nPosCond]	:= xST_CONDPG
	 				Endif
	 			Endif
	 				     				           			
				If ! Empty(RTrim(aRetFor[3]))
					cNaturez	:= RTrim(aRetFor[2])
				Else
					cNaturez	:= xST_NATURE
				Endif

	 			// -- Natureza Padrao     			
	 			nPosNAT := Ascan( aCposCab , 'PV1_NATURE' )
	
	 			If nPosNAT == 0
	 				aAdd( aCposCab ,  { 'PV1_NATURE' , 'NATUREZ' } )  
	 				aAdd( aColsCab ,   cNaturez  )
	 			Else
	 				IF Empty( RTrim( aColsCab[nPosNAT] ) )
	 					aColsCab[nPosNAT]	:= cNaturez
	 				Endif
	 			Endif
	 				     				           			
			Endif


			// -- Faz laco nos itens do pedido (mesma tabela)
			
			(cQRY)->( DbGoTop() )			
			cUnqKey	:= (cQRY)->FILIAL + (cQRY)->NUM_PEDIDO + Alltrim((cQRY)->CGCFORNECE)
	
			While (cQRY)->( !Eof() .And.  cUnqKey == (cQRY)->FILIAL + (cQRY)->NUM_PEDIDO + Alltrim((cQRY)->CGCFORNECE) )
				
				aAux	:= {}
				
				IF Len(aCposDet) > 0 
	
					// -- aCposDet[nCpos][2]   -->  Utiliza a posi��o [2] pois neste momento faz a leitura do campo da query  PV5_QFIELD  
					// -- Valida se o campo de relacionamento esta preenchido
	
					aAux := {}				
					For  nCpos := 1 To Len(aCposDet)									
						xCpoValue   := (cQRY)->( FieldGet( FieldPos( RTrim( aCposDet[nCpos][2] ) ) ) )
			     			aAdd( aAux , xCpoValue )				     			     
		     		Next nCpos
		     		
					// -- Armazen Padrao - Tratamento Especial para criar o armazen automaticamente antes da MsExecAuto 
										
	     			nPosCodPro := Ascan( aCposDet,{ |x| Alltrim(x[1]) == "PV2_COD" } ) 
	     			nPosLocPad := Ascan( aCposDet,{ |x| Alltrim(x[1]) == 'PV2_LOCAL' } ) 				

	     			If nPosLocPad > 0	     			
	     				IF !Empty( RTrim(aAux[nPosLocPad]) ) .And. !Empty(RTrim(aAux[nPosCodPro]))	     				
	     					SB1->( DbSetOrder(1) )
	     					IF SB1->( MsSeek( FwxFilial("SB1",SM0->M0_CODFIL) + RTrim(aAux[nPosCodPro]) ) )	     		
								SB2->( DbSetOrder(1) )
								If SB2->( !MsSeek( FwxFilial("SB2",SM0->M0_CODFIL) + SB1->B1_COD + RTrim(aAux[nPosLocPad]) ) )
									CriaSB2( SB1->B1_COD , RTrim(aAux[nPosLocPad]) )
								EndIf
	     					Endif	     				
	     				Endif	     				
	     			Endif

					// -- Item do Pedido adicionado no Acols 
		     		
		     		aAdd( aColsGrid  , aAux ) 
									
				Endif
				
				nRecSave	:= (cQRY)->( Recno() )
								
				(cQRY)->( DbSkip() )
			
			End

			                                    
			// -- 08 Alimenta o objeto da integra��o com as informa��es processadas
			
			oSestini:aColsCab     := aClone(aColsCab)
			oSestini:aColsGrid	  := aClone(aColsGrid)			    

			oSestini:aCposCab	  := aClone(aCposCab)
			oSestini:aCposDet	  := aClone(aCposDet)
				
			oSestini:AppendRow()
			oSestini:SaveSource()
			oSestini:SetDoneById( nIdOrig  , @oConnect ) 
			
			oSestini:Destroy()					
			Freeobj(oSestini)
			oSestini := Nil

			u_WriteLog( cJobLog  ,  cTagLog + ' Finalizado ' + ALLTRIM((cQRY)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar( nIdOrig ) ) ,.T.,.T. )

			PCOM2VEN( aColsCab , aColsGrid , cQRY , aCposCab , aCposDet , nIdOrig , cToken , cNumPedido ) 
			
        End
        
        (cQRY)->( DbCloseArea() )
                   
    Endif
    
    AEval(aAreas, {|x| RestArea(x)})
        
Return





/*/{Protheus.doc} PCOM2VEN
Gera Pedido de Venda espelhado ao Pedido de Compras 

@type 		User Function
@author 	Mauro Paladini
@version 	1.0

/*/
Static Function PCOM2VEN( aColsSC5 , aColsSC6 , cQRY , aCposSC5 , aCposSC6 , nIdOrig , cToken , cNumPedido )

Local aAreas    		:= { PV1->(GetArea()) , PV2->(GetArea()) , PV9->(GetArea()) , SM0->( GetArea() ) , GetArea() }
Local cQuery    		:= ''
Local cNFS      		:= ''   // Cabecalhos
Local cITE      		:= ''   // Itens
Local lRet      		:= .T.
Local nD				:= 0
Local nX				:= 0

Local cSQL      		:= ''

// -- Pedido
Local cNotaFis  		:= ''
Local cIDNota   		:= ''
Local cItem     		:= '01'
Local nPosDet			:= 0
Local aProdutos 		:= {}

Local cQryPath      	:= '\user_procedures\'
Local cIDCliente    	:= ''
Local oSestini      	:= Nil
Local cChave        	:= ''

Local cIdMapFld     	:= GetMv('ES_MNFS1'	,,'0000000001')		// -- C�digo do cadasstro 
Local xES_OPEPED		:= GetMv('ES_OPEPED',,'01' )
Local cTesPed			:= ''
Local xST_CONDPG		:= GetMv('ES_STNCND',,'10')				// -- Condicao de pagamento Default
Local xST_NATURE		:= GetMv('ES_SRNNAT',,'1001')		// -- Natureza Padrao
Local cNaturez			:= ''
Local xST_TESSAI		:= GetMv('ES_TESNFS',,'501')			// -- TES Defauls
Local xFilOld			:= cFilAnt
Local xEmpOld			:= cEmpAnt
Local xCNPJ				:= Rtrim( SM0->M0_CGC )
Local xEmpMatris		:= GetMv('ES_EMATRI',,'01')					// -- TES Defauls
Local xFilMatris		:= GetMv('ES_FMATRI',,'01010001')			// -- TES Defauls

Local nPosCond			:= 0
Local nPosNat			:= 0

Local cKeyOrig      	:= ''
Local cTabOrig      	:= 'SC5'
Local nIndOrig      	:= 1
Local nCpos         	:= 0
Local aItens			:= {}
Local aAux				:= {}
Local aAuxItens     	:= {}

Local aColsCab			:= {}
Local aColsGrid			:= {}
Local aCposCab			:= {}
Local aCposDet			:= {}
Local aRetCli			:= {}
Local xCpoValue			
Local cNaturez			:= ''
Local cPvTagLog			:= ''

// 1=Sim 2=Nao
Local oSestini			:= Nil
Local cAutoFunc			:= 'SAUTONFS'							// -- User Function registrada no Map Field PV5     
Local cMapSeek			:= ''
Local cBlqProx  		:= '1'
Local cOperacao 		:= '3'
Local cOrigem			:= 'MATA410'

Default cToken		:= ''

SaveInter()


// -- Instancia filial parametrizada ( SESTINI - CD )

SM0->( DbSeek( FWGrpCompany() + Padr(xFilMatris,8) ))

cEmpAnt		:= xEmpMatris
cFilAnt		:= xFilMatris

cPvTagLog	:=  '[PedCom2Ven ' + SM0->M0_CODFIL + ' ' + RTRIM(SM0->M0_FILIAL)  +  ']'

u_WriteLog( cJobLog  ,  cPvTagLog + ' Processo Iniciado (pedido de transfer�ncia)'  ,.T.,.T. )




// -- PV5 Configura��o de Canais
// -- Verifica se a configura��o de campos existe para esta rotina 

cMapSeek	:= xFilial('PV5') + AvKey( 'U_' + cAutoFunc , 'PV5_FUNOPC' ) + cIdMapFld

If Empty ( cIdMapFld ) .OR. PV5->( ! DbSeek( cMapSeek ) )  
    u_WriteLog( cJobLog  , '[' + cPvTagLog + '] ERRO Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek  ,.T.,.T. )
	u_WriteLog( cJobLog  , '[' + cPvTagLog + '] Processo cancelado'  ,.T.,.T. )
    Return .F.
Endif	



// -- PV9 PV1 PV2 Instancia Objeto de integra��o
// -- Chama classe da integra��o para a grava��o de um Pedido de Venda com Base no Pedido de Compra

cOrigem		:= 'MATA410'		// ##CFG
cChave      := xFilial(cFilAnt) + AllTrim ( cValToChar(nIdOrig) )    	// -- Chave agrupadora	// #CFG
cOperacao	:= '3'
cKeyOrig    := AllTrim ( cValToChar( nIdOrig ) )
nIndOrig    := 1
 
u_WriteLog( cJobLog  , cPvTagLog + ' Integrando ' + 'Ped Compra ' + ' Id: ' +  AllTrim ( cValToChar( nIdOrig ) ) ,.T.,.T. )
        


// -- Classe TransactRows()        

oSestini := TransactRows():New( cOrigem ) 
oSestini:SetVariables( cOrigem , cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBlqProx )
oSestini:cFunopc  			:=  cAutoFunc

oSestini:TipoNF             := 'N'
oSestini:FormProprio        := 'S'
oSestini:NroNotaFiscal      := ''
oSestini:SerieNotaFiscal    := ''
oSestini:DataEmissao        := CTOD( aColsSC5[ Ascan( aCposSC5,{ |x| Alltrim(x[1]) == 'PV1_EMISSA' } ) ] )
oSestini:DataDigitacao      := ddatabase
oSestini:NumPedido			:= cNumPedido 
oSestini:Especie            := ''
oSestini:CPFCliente         := ALLTRIM( xCNPJ )

aRetCli	 := u_CliForAuto( ALLTRIM( xCNPJ ) , 1 )

oSestini:CodCliente			:= aRetCli[1]
oSestini:LojaCliente        := aRetCli[2]

oSestini:NroNotaEletronica  := ''
oSestini:ValorMercadorias   := 0
oSestini:ValorBrutoNF       := 0
oSestini:ChaveNFE           := ''
oSestini:ProtocoloNFE       := ''
oSestini:GeraTitulos        := ''
oSestini:Token				:= cToken
oSestini:IDRegistro         := nIdOrig
oSestini:TabelaIntegracao   := 'Varejo_PedidoCompras'
oSestini:CampoFlagProcessado:= ''

            

// -- M�todo que ir� prepara o aHeader do cabe�alho e itens com 
// -- base no layout de campos cadastrados na PV5

oSestini:PrepareAuto( "U_" + cAutoFunc , cIdMapFld )


// -- Se o Layout 0000000001 de INPUT existe na PV5 o m�todo preparou aheader
// -- das tabelas 

aCposCab	:= aClone(oSestini:aCposCab)
aCposDet	:= aClone(oSestini:aCposDet)

aColsCab	:= {}
aColsGrid	:= {}
aAux		:= {}

SA1->( DbSetOrder(1) )
SA1->( DbSeek( xFilial('SA1') + aRetCli[1] + aRetCli[2] ) )


// -- Alimenta Arrays de Cab e Itens para envio na rotina autom�tica
// -- Serao gravados na PV1 e PV2 e posteriormente na execauto ser�o enviados via array 

IF Len(aCposCab) > 0 	
		
	
	// -- aCposCab[nCpos][2]   -->  Utiliza a posi��o [2] pois neste momento faz a leitura do campo da query  PV5_QFIELD  
	// -- Valida se o campo de relacionamento esta preenchido

	For  nCpos := 1 To Len(aCposCab)
		 
		xCpoValue	:= CriaVar( RTrim(aCposCab[nCpos][1]) )
		
		If RTrim( aCposCab[nCpos][1] ) = 'PV1_CPFCLI'
			xCpoValue	:= ALLTRIM( xCNPJ )
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_DTDIGIT'
			xCpoValue	:= ddatabase
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_TIPONF'
			xCpoValue	:= 'N'

		Elseif RTrim( aCposCab[nCpos][1])  = 'PV1_EMISSA'
			xCpoValue	:= CTOD(aColsSC5[nCpos])
		
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_TXMOED'
			xCpoValue	:= 1
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_MOEDA'
			xCpoValue	:= 1
				
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_IDREG'
			xCpoValue	:= nIdOrig  
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_TABINT'
			xCpoValue	:= 'Varejo_PedidoCompras'
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_PEDIDO'
			xCpoValue	:= cNumPedido
			
		Elseif RTrim( aCposCab[nCpos][1] ) = 'PV1_NUMPED'
			xCpoValue	:= cNumPedido
					
		Endif
		
		aAdd( aColsCab , xCpoValue )

	Next nCpos  
		
	// -- Condicao de Pagamento Padrao
	
	nPosCond := Ascan( aCposCab , 'PV1_CONDPG' )

	If nPosCond == 0
		aAdd( aCposCab , { 'PV1_CONDPG' , 'COND' } )  
		aAdd( aColsCab , xST_CONDPG  )
	Else
		IF Empty( RTrim( aColsCab[nPosCond] ) )
			aColsCab[nPosCond]	:= xST_CONDPG
		Endif
	Endif
		     				           			
	// -- Natureza Padrao     	
			
	nPosNAT := Ascan( aCposCab , 'PV1_NATURE' )

	If ! Empty(RTrim(aRetCli[3]))
		cNaturez	:= RTrim(aRetCli[3])
	Else
		cNaturez	:= xST_NATURE
	Endif

	If nPosNAT == 0
		aAdd( aCposCab ,  { 'PV1_NATURE' , 'NATUREZ' } )  
		aAdd( aColsCab ,   cNaturez  )
	Else
		IF Empty( RTrim( aColsCab[nPosNAT] ) )
			aColsCab[nPosNAT]	:= cNaturez
		Endif
	Endif
		     				           			
Endif



// ## itens

IF Len(aCposDet) > 0

	For nX := 1 To Len(aColsSC6)
	
		cTesPed	:= ''
	
		aAux	:= {} 
	
		For nD	:= 1 To Len(aCposDet)
		
			xCpoValue	:= CriaVar( RTrim(aCposDet[nD][1]) )
			
			If RTrim( aCposDet[nD][1] ) = 'PV2_ITEM'
				xCpoValue	:= StrZero(nD,2)				
		
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_COD'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
			
				SB1->( DbSetOrder(1) )
				SB1->( DbSeek( xFilial('SB1') + RTrim(xCpoValue) ) ) 
		
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_DESPRO'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
				
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_TOTAL'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )				

			Elseif RTrim( aCposDet[nD][1] ) = 'PV2_QUANT'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
				
			Elseif RTrim( aCposDet[nD][1] ) = 'PV2_VUNIT'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
					
			Elseif RTrim( aCposDet[nD][1] ) = 'PV2_PRCVEN'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  'PV2_VUNIT' } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
				
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_EMISSA'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )

			Elseif RTrim( aCposDet[nD][1])  = 'PV2_DTDIGI'
				xCpoValue	:= dDatabase

			Elseif RTrim( aCposDet[nD][1])  = 'PV2_LOCAL'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
				
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_CF'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )
				
			Elseif RTrim( aCposDet[nD][1])  = 'PV2_TES'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )

			Elseif RTrim( aCposDet[nD][1])  = 'PV2_ITEMOR'
				xCpoValue	:= '  '

			Elseif RTrim( aCposDet[nD][1])  = 'PV2_ITEMCT'
				xCpoValue	:= '  '
				
			Elseif RTrim( aCposDet[nD][1] ) = 'PV2_IDREG'
				nPosDet		:= Ascan( aCposSC6,{ |x| Alltrim(x[1]) ==  RTrim( aCposDet[nD][1]) } )
				xCpoValue	:= IIF( nPosDet > 0 , aColsSC6[nX][nPosDet] , xCpoValue )

			Elseif RTrim( aCposDet[nD][1] ) = 'PV2_TABINT'
				xCpoValue	:= 'Varejo_PedidoCompras' 
			
			Endif�
			
			aAdd( aAux , xCpoValue )
	
		Next nD
		
		// -- Armazen Padrao - Tratamento Especial para criar o armazen automaticamente antes da MsExecAuto 
							
		nPosCodPro := Ascan( aCposDet,{ |x| Alltrim(x[1]) == "PV2_COD" } ) 
		nPosLocPad := Ascan( aCposDet,{ |x| Alltrim(x[1]) == 'PV2_LOCAL' } ) 				

		If nPosLocPad > 0	     			
			IF !Empty( RTrim(aAux[nPosLocPad]) ) .And. !Empty(RTrim(aAux[nPosCodPro]))	     				
				SB1->( DbSetOrder(1) )
				IF SB1->( MsSeek( FwxFilial("SB1",SM0->M0_CODFIL) + RTrim(aAux[nPosCodPro]) ) )	     		
					SB2->( DbSetOrder(1) )
					If SB2->( !MsSeek( FwxFilial("SB2",SM0->M0_CODFIL) + SB1->B1_COD + RTrim(aAux[nPosLocPad]) ) )
						CriaSB2( SB1->B1_COD , RTrim(aAux[nPosLocPad]) )
					EndIf
				Endif	     				
			Endif	     				
		Endif

		SB1->( DbSetOrder(1) )
		IF SB1->( MsSeek( FwxFilial("SB1",SM0->M0_CODFIL) + RTrim(aAux[nPosCodPro]) ) )	     		

			cTesPed		:= MaTesInt( 2 ,;
									 xES_OPEPED ,;
									 AvKey( aRetCli[1] ,'A1_COD') ,;
									 AvKey( aRetCli[2] ,'A1_LOJA') ,;
									 "C" ,;
									 SB1->B1_COD  ,;
									 NIL )
								 
		Endif 					

		If Empty( RTrim( cTesPed ))
			cTesPed	:= xST_TESSAI		
		Endif

		// -- TES Padrao (utiliza caso n�o seja informado)					

		nPosTES := Ascan( aCposDet,{ |x| Alltrim(x[1]) == 'PV2_TES' } ) 				
		If nPosTES == 0
			aAdd( aCposDet , { 'PV2_TES' , 'TES' } )
			aAdd( aAux , cTesPed )
		Else	     			
			aAux[nPosTES]	:= cTesPed
		Endif 

		aAdd( aColsGrid , aAux )		

	Next nX

Endif

                        
// -- 08 Alimenta o objeto da integra��o com as informa��es processadas

oSestini:aColsCab     := aClone(aColsCab)
oSestini:aColsGrid	  := aClone(aColsGrid)			    

oSestini:aCposCab	  := aClone(aCposCab)
oSestini:aCposDet	  := aClone(aCposDet)
	
oSestini:AppendRow()
oSestini:SaveSource()

oSestini:Destroy()
Freeobj(oSestini)
oSestini := Nil

DbCommit()

RestInter()

AEval(aAreas, {|x| RestArea(x)})

cEmpAnt	:= xEmpOld
cFilAnt	:= xFilOld

Return