#include "TOTVS.CH"

#DEFINE TIPOREG		1
#DEFINE TITULO 		2
#DEFINE	CAMPO			3
#DEFINE TAMANHO		4
#DEFINE POSINI		5
#DEFINE POSFIM		6
#DEFINE FORMATO		7
#DEFINE DECIMAL		8 


/*/{Protheus.doc} LjGeraSFI
Alimenta tabela de reducoes z com base nos registros recebidos no arquivo da CAT52

@author Mauro Paladini
@since 24/03/2015
@version 1.0
@see http://tdn.totvs.com/pages/releaseview.action;jsessionid=00EA30A74B509D951FDBFEE1C55BF539?pageId=73893303
/*/

User Function LjGeraSFI( cCatFilial )

Local aArea				:= GetArea()
Local cNumero			:= GetSx8Num("SFI","FI_NUMERO")
Local dDataMov		:= P04->P04_DTMOV
Local cTrb				:= GetNextAlias()
Local cCupomIni		:= ""
Local cCupomFim		:= ""
Local cNumRedz		:= LTrim(Str(P04->P04_CRZ,6))

Local cPDV				:= ""
Local cSerie			:= ""
Local cNumCx			:= ""
Local cCodEstacao	:= ""			


// Pega 1o e ultimo cupom do dia

BEGINSQL Alias cTrb
	
	
SELECT MIN(P01_DOC) P01_DOC
FROM %Table:P01% P01
WHERE P01_DTEMIS = %Exp:DTOS(dDataMov)% AND D_E_L_E_T_ = ''
UNION ALL

SELECT MAX(P01_DOC) P01_DOC
FROM %Table:P01% P01
WHERE P01_DTEMIS = %Exp:DTOS(dDataMov)% AND D_E_L_E_T_ = ''
ORDER BY P01_DOC

ENDSQL


(cTrb)->( DbGoTop() )
	
cCupomIni	:= (cTrb)->P01_DOC

(cTrb)->( DbSkip() )
	
cCupomFim	:= (cTrb)->P01_DOC

(cTrb)->( DbCloseArea() )
 
// Fim query

SFI->( DbSetOrder(1) )

IF SFI->( !DbSeek( cCatFilial + DTOS( P04->P04_DTMOV ) + cPDV + cNumRedz ) )

	// Inicializa Variavies da Estacao

	u_ECFPdv( cCatFilial , Rtrim(P04->P04_NSECF) , @cPDV, @cSerie, @cNumCx, @cCodEstacao )
	

	RecLock("SFI",.T.) 
	
		SFI->FI_FILIAL	:= cCatFilial
		SFI->FI_DTMOVTO	:= P04->P04_DTMOV
		SFI->FI_NUMERO	:= cNumero
		SFI->FI_PDV			:= cPDV	
		SFI->FI_SERPDV	:= Rtrim(P04->P04_NSECF)
		SFI->FI_NUMREDZ	:= LTrim(Str(P04->P04_CRZ,6))
		SFI->FI_GTINI		:= ( P04->P04_GT - P04->P04_VLRTOT ) 
		SFI->FI_GTFINAL	:= P04->P04_GT
		SFI->FI_NUMINI	:= Str( Val( cCupomIni ) , 6 )
		SFI->FI_NUMFIM	:= Str( Val( cCupomFim ) , 6 )
		SFI->FI_CANCEL	:= P04->P04_TOVLCA
		SFI->FI_VALCON	:= P04->P04_VLRTOT
		SFI->FI_SUBTRIB	:= P04->P04_TOTVLS
		SFI->FI_DESC		:= P04->P04_TOTVLD
		SFI->FI_ISENTO	:= P04->P04_TOTISE
		SFI->FI_NTRIB		:= P04->P04_TOTNTR
		SFI->FI_SITUA		:= "00"		
		SFI->FI_IMPDEBT	:= ( P04->P04_TOTA01 * ( P04->P04_ALIQ01 / 100 ) ) 		
		SFI->FI_COO			:= StrZero( Val( P04->P04_COOFIM ) , 6 ) 
		SFI->FI_CRO			:= StrZero( Val( P04->P04_CRO ) , 3 ) 	
		SFI->FI_DTREDZ	:= P04->P04_EMISSA
	
		// Grava Campo Base do Imposto
		
		IF P04->P04_ALIQ01 == 3.2
		
			SFI->FI_BAS32	:= P04->P04_TOTA01
			SFI->FI_COD32	:= "T" + StrTran( StrZero( P04->P04_ALIQ01 , 5 , 2 ) , "." , "" )
			
		
		Elseif P04->P04_ALIQ01 == 2
		
			SFI->FI_BAS2	:= P04->P04_TOTA01
			SFI->FI_COD2	:= "T" + StrTran( StrZero( P04->P04_ALIQ01 , 5 , 2 ) , "." , "" ) 
			
		Elseif P04->P04_ALIQ01 == 7
		
			SFI->FI_BAS7	:= P04->P04_TOTA01
			SFI->FI_COD7	:= "T" + StrTran( StrZero( P04->P04_ALIQ01 , 5 , 2 ) , "." , "" ) 
	
		Elseif P04->P04_ALIQ01 == 3
		
			SFI->FI_BAS3	:= P04->P04_TOTA01
			SFI->FI_COD3	:= "T" + StrTran( StrZero( P04->P04_ALIQ01 , 5 , 2 ) , "." , "" ) 
	
		Elseif P04->P04_ALIQ01 == 4
		
			SFI->FI_BAS4	:= P04->P04_TOTA01
			SFI->FI_COD4	:= "T" + StrTran( StrZero( P04->P04_ALIQ01 , 5 , 2 ) , "." , "" )
	
		Endif
		
	MsUnlock()

Endif

Return






/*/
{Protheus.doc} ECFPdv
Pega os dados do PDV


@author Mauro Paladini
@since 24/03/2015
@version 1.0
/*/


User Function ECFPdv( cLJFil , cNumSerie , cPDV, cSerie, cNumCx, cCodEstacao )

Local aArea			:= GetArea()
Local aSave			:= { "SLG" }  
Local aAlias		:= u_xGetArea(aSave)

Local lRet	:= .T.

SLG->( DbOrderNickName("PDV0000001") ) 

IF SLG->( DbSeek( FwxFilial("SLG",cLJFil) + AvKey(cNumSerie,"LG_SERPDV") ) ) 

	cPDV				:= SLG->LG_PDV 
	cSerie			:= SLG->LG_SERIE
	cCodEstacao		:= SLG->LG_CODIGO
	cNumCx 			:= SLG->LG_XNUMCX
	

Else

	// -- Valida os casos do SAT 

	SLG->( DbOrderNickName("SAT0000001") ) 

	IF SLG->( !DbSeek( P01->P01_FILIAL + RTrim(P01->P01_NSECF) ) ) 

		cPDV				:= SLG->LG_PDV 
		cSerie			:= SLG->LG_SERIE
		cCodEstacao		:= SLG->LG_CODIGO
		cNumCx 			:= SLG->LG_XNUMCX
		
	Else

		lRet 		:= .F.	
	
	Endif 

Endif

u_xRestArea(aAlias)
					
Return lRet 