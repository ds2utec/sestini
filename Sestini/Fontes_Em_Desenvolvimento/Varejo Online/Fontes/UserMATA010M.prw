#INCLUDE "TOTVS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} MATA010N
PE nativo da Classe MVC referente ao modelo MATA010N - Cadastro de Produtos
        
@author         Mauro Paladini
@since          18/02/2013
@version        P12      
/*/ 

User Function MATA010()

Local xRet  := .T.
 
If AtIsRotina("FWFORMCOMMIT")
                    
    IF !AtIsRotina("ITCOMNTTS")
        ITCOMNTTS()
    Endif      
        
Endif

Return xRet


/*/{Protheus.doc} ITCOMNTTS
PEa Apos a gravacao total do modelo fora do controle de transa��es
        
@author         Mauro Paladini
@since          18/02/2013
@version        P12      
/*/ 

Static Function ITCOMNTTS()

    Local aAreas            := { SM0->(GetArea()) , GetArea() }
    Local oModel            := FwModelActive()
    Local nOpcao            := oModel:GetOperation()
    Local nX                := 1 
    Local lRetFun           := .T.
    Local cRevisao          := ''
                
    // ## Mauro Paladini - INTEGRACAO VAREJO ONLINE ## 
    // -- Envia regostro para a fila de integracoes
    
    Integracao(nOpcao)
    
    // -- Fim Integracao
    
    AEval(aAreas, {|x| RestArea(x)})

Return (lRetFun)


/*/{Protheus.doc} Integracao()
Funcao auxiliar para enviar o registro atual para a fila de integracao
Prepara o Script e Chama Metodo responsavel pelas atualizacoes

@type function
@author     Mauro Paladini
@since  01/10/2018
@version    1.0
/*/

Static Function Integracao(nOpcao)

Local cQuery    := StatiCCall( STNQUERY , ProdQuery )
Local cChave    := SB1->B1_COD      // -- Chave agrupadora

// -- 1=Inclus�o;2=Altera��o;3=Exclus�o
Local cOperacao := '1'    
Local cKeyOrig  := SB1->B1_FILIAL + SB1->B1_COD
Local cTabOrig  := 'SB1'
Local nIndOrig  := 1

// 1=Sim 2=Nao
Local cBlqProx  := '1'     

// -- Instancia a classe
If !Empty( cQuery )
	oSestini := TransactRows():New('MATA010')
	oSestini:SetVariables( 'MATA010' /*cOrigem */, cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBlqProx )
	oSestini:SetScript( cQuery )
	oSestini:AppendRow()
	oSestini:Destroy()
	
	Freeobj(oSestini)
	oSestini := Nil
Endif

Return