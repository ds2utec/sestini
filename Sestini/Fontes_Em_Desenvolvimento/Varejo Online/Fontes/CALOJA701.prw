#INCLUDE "PROTHEUS.CH" 
#INCLUDE "FWMVCDEF.CH"
#include "apwizard.ch"
#INCLUDE "POSCSS.CH"

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE CANCELADO	"5"
#DEFINE COMERRO		"8"
#DEFINE EMPROCESSA	"9"


/*
CALOJA701
Interface MVC de manuten��o/visualiza��o dos cupons de venda do arquivo CAT52

@author Mauro Paladini
@since 11/02/2015
@version 1.0
*/
User Function CaCupom()

Local oBrowse 	:= Nil

Private aRotina     := MenuDef()
Private aResume		:= {}

                        

// Ajusta campos
AjustaSX3()

oBrowse:= FWMBrowse():New()
oBrowse:SetAlias("P01")
oBrowse:SetDescription(" [SAT-CE] Movimentos Automa��o")

oBrowse:AddLegend( "P01_STATUS == '0'", "BR_BRANCO"		, "Integrado" )
oBrowse:AddLegend( "P01_STATUS == '1'", "BR_AMARELO"	, "Integrado n�o valiado"  )
oBrowse:AddLegend( "P01_STATUS == '2'", "BR_LARANJA"	, "Validado" )
oBrowse:AddLegend( "P01_STATUS == '3'", "BR_AZUL"		, "Or�amento de venda" )
oBrowse:AddLegend( "P01_STATUS == '4'", "BR_VERDE"		, "Venda finalizada" )
oBrowse:AddLegend( "P01_STATUS == '5'", "BR_MARRON"		, "Cupom Cancelado" )
oBrowse:AddLegend( "P01_STATUS == '8'", "BR_VERMELHO" 	, "Registro com inconsistencia" )
oBrowse:AddLegend( "P01_STATUS == '9'", "BR_CINZA"		, "Em processamento" )

oBrowse:Activate()

Return Nil



/*
ModelDef
Funcao MVC para criar o modelo de dados

@author Mauro Paladini
@since 11/02/2015
@version 1.0
*/              

Static Function ModelDef()

Local oModel	:= Nil
Local oStruP01	:= FWFormStruct(1,"P01") 
Local oStruIP02	:= FWFormStruct(1,"P02") 
Local oStruIP03	:= FWFormStruct(1,"P03") 

Local bPreValid     := Nil
Local bPosValid     := Nil
Local bCommit       := {|oModel| CA701Commit(oModel)}
Local bCancel	      := Nil  


// Adiciona um campo manualmente na Model 
SX3->( DbSetOrder(2) )

	If SX3->( DbSeek( "P02_STATUS" ) )
	
		oStruIP02:AddField( ;                     					// Ord. Tipo Desc. 
		"P02_P02BMP"                      						, ; // [01]  C   Nome do Campo 
		Trim(X3Descric())     									, ; // [02]  C   ToolTip do campo 
		"P02_P02BMP"                     						, ; // [03]  C   identificador (ID) do Field 
		SX3->X3_TIPO                       						, ; // [04]  C   Tipo do campo 
		11	                    								, ; // [05]  N   Tamanho do campo 
		SX3->X3_DECIMAL                    						, ; // [06]  N   Decimal do campo 
		FwBuildFeature( STRUCT_FEATURE_VALID , SX3->X3_VALID )	, ; // [07]  B   Code-block de valida��o do campo 
		FwBuildFeature(2, AllTrim(SX3->X3_WHEN) )               , ; // [08]  B   Code-block de valida��o When do campo 
		{} /*StrTokArr( AllTrim(X3CBox()),';')*/           		, ; // [09]  A   Lista de valores permitido do campo 
		X3Obrigat( SX3->X3_CAMPO )                             	, ; // [10]  L   Indica se o campo tem preenchimento	obrigat�rio 
		FwBuildFeature( STRUCT_FEATURE_INIPAD, SX3->X3_RELACAO)	, ; // [11]  B   Code-block de inicializacao do campo 
		NIL                             						, ;	// [12]  L   Indica se trata de um campo chave 
		NIL                              						, ; // [13]  L   Indica se o campo pode receber valor em uma opera��o de update. 
		( SX3->X3_CONTEXT == 'V' )                              ) 	// [14]  L   Indica se o campo � virtual 

	Endif

SX3->( DbSetOrder(1) )


// Criacao da Modelo
oModel:= MpFormMOdel():New("CALOJA701",  /*bPreValid*/ , /*bPosValid*/ , bCommit ,/*bCancel*/ )

// Cabecalho
oModel:AddFields("MdFieldP01",Nil,oStruP01,/*prevalid*/,,/*bCarga*/)
oModel:SetDescription("Cupom SAT-CE : Cabe�alho")

// Produtos
oModel:AddGrid("MdGridP02", "MdFieldP01" /*cOwner*/, oStruIP02 , /*bLinePre*/ , /*bLinePost*/ , /*bPre*/ , /*bPost*/, { |oGrid| LoadCores(oGrid) } /*bLoad*/)
oModel:SetRelation( "MdGridP02" , { { "P02_FILIAL" , 'xFilial("P01")' }, { "P02_ARQ" , "P01_ARQ" } , { "P02_DOCCOO" , "P01_DOCCOO" } } , P02->( IndexKey( 3 ) ) )
oModel:AddCalc( "MdCalcP02", 'MdFieldP01', 'MdGridP02', "P02_VALLIQ"  , "P02__TOTLIQ" , "SUM" , { | oMdl | VldTotLiq( oMdl ) }  ,, "Total de Produtos"   ) 

// Pagamentos
oModel:AddGrid("MdGridP03", "MdFieldP01" /*cOwner*/, oStruIP03 , /*bLinePre*/ , /*bLinePost*/ , /*bPre*/ , /*bPost*/, /*bLoad*/)
oModel:SetRelation( "MdGridP03" , { { "P03_FILIAL" , 'xFilial("P03")' }, { "P03_ARQ" , "P01_ARQ" } , { "P03_DOCCOO" , "P01_DOCCOO" } , { "P03_DOC" , "P01_DOC" } } , P03->( IndexKey( 3 ) ) )
oModel:AddCalc( "MdCalcP03", 'MdFieldP01', 'MdGridP03', "P03_VALOR"  , "P03__TOTFIN" , "SUM" , { | oFW | .T.}  ,, "Pagamentos"   ) 

oModel:SetPrimaryKey({ "P01_FILIAL","P01_ARQ","P01_DOC" })

Return ( oModel )




/*/{Protheus.doc} VldTotLiq
Faz a valida��o dos registros que devem ser considerados para o campo totalizador

@type 	User Function
@author 	Mauro Paladini
@since 	Aug 24, 2018  10:42:23 PM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/
Static Function VldTotLiq( oModel, lPar )

Local lRet := .T.

If oModel:GetValue( 'MdGridP02', 'P02_ESTORN' ) == '1'
    lRet    := .F. 
Endif 

Return lRet 




/*/{Protheus.doc} CA701Commit
Faz a gravacao do formulario

@type   Static Function
@author Mauro Paladini
@since  Dez 12, 2018  10:42:23 PM
@version 1.0

/*/

Static Function CA701Commit(oModel)

Local lRet        := .T.
Default oModel    := FWModelActive()

// -- Caso algo seja modificado o cupom precisa ser validado novamente

If oModel:GetOperation() == MODEL_OPERATION_UPDATE  .OR.  oModel:GetOperation() == MODEL_OPERATION_INSERT 

    RecLock("P01",.F.)    
        P01->P01_STATUS := INVALIDO        
    MsUnLock()

    P02->( DbSetOrder(6) )
    IF P02->( DbSeek( P01->P01_FILIAL + P01->P01_ARQ + P01->P01_DOC ) )  

        While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + P02_DOC == P01->P01_FILIAL + P01->P01_ARQ + P01->P01_DOC )        
            RecLock("P02",.F.)                
                P02->P02_STATUS := INVALIDO               
            MsUnLock()            
            P02->( DbSkip() )                                   
        End        

    Endif
    
    P03->( DbSetOrder(6) )    
    IF P03->( DbSeek( P01->P01_FILIAL + P01->P01_ARQ + P01->P01_DOC ) )
    
        While P03->( !Eof() .And.  P03_FILIAL + P03_ARQ + P03_DOC == P01->P01_FILIAL + P01->P01_ARQ + P01->P01_DOC)
            RecLock("P03",.F.)
                P03->P03_STATUS := INVALIDO
            MsUnLock()            
            P03->( DbSkip() )                                   
        End
        
    Endif

EndIf

// -- COMMIT COMPLETO

If lRet 
    lRet := FwFormCommit(oModel)
EndIf

Return(lRet)



/*
ViewDef
Funcao MVC para criar as Views

@author Mauro Paladini
@since 09/12/2014
@version 1.0                                                        

*/

Static Function ViewDef()

Local oModel 	      := FwLoadModel("CALOJA701")

Local oCalc1        := FWCalcStruct( oModel:GetModel( 'MdCalcP02' ) )
Local oCalc2        := FWCalcStruct( oModel:GetModel( 'MdCalcP03' ) )
 
Local oStruP01      := FWFormStruct(2,"P01") 
Local oStruIP02     := FWFormStruct(2,"P02") 
Local oStruIP03     := FWFormStruct(2,"P03") 
Local oView         := Nil

Private cModelCSS   := "QHeaderView::section { font-family: Arial, Helvetica, sans-serif; font-size: 09px; color: black ; background-image: url(rpo:fw_brw_hdr.png); border: 1px solid #99CCFF; } "+ ;
                    "QTableView{ rowHeight: 5; font-family: Arial, Helvetica, sans-serif; font-size: 10px; alternate-background-color: LightGray ; color: black }" 

/* TFolder => QTabBar */

Private cFldCSS     :=  "QTabBar::tab " + ;
                        "{ " + ;
                        "  color: #FFFFFF; " + /*cor da fonte*/ ;  
                        "  font-size: 14px; " + /*tamanho da fonta*/ ;
                        "  background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #00B0B2, stop: 1 #52CCCE ); " +  /*Cor de fundo*/ ;
                        "  border: 1px solid #3F647E; " + /*cor da borda*/ ;
                        "  border-radius: 10px; " + /*arredondamento borda*/ ; 
                        "  min-width: 150px; " +      /*largura minima botao*/ ;
                        "  min-height: 17px; " +      /*altura minima botao*/ ;
                        "  padding: 2px; " +          /*margin para arredondamento da borda*/ ;
                        "} " +                        /* Acoes quando a aba for selecionada */ ;                        
                        "QTabBar::tab:selected{ background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #52CCCE, stop: 1 #00B0B2); " +  /*Cor de fundo*/ ;
                        "  border-bottom: none; " +  /*cor da borda*/ ;
                        "} " +  /* Acoes quando a aba nao estiver selecionada */ ;                     
                        " QTabBar::tab:!selected{ margin-top: 2px; } "  /*margem superior*/
                                                 


// Adiciona um campo manualmente
SX3->( DbSetOrder(2) )

	If SX3->( DbSeek( "P02_STATUS" ) )

		oStruIP02:AddField( ;
		AllTrim( "P02_P02BMP"  )	   		, ;    	// [01] Campo
		"00"            		   			, ;    	// [02] Ordem
		AllTrim( X3Titulo()  )       		, ;    	// [03] Titulo
		AllTrim( X3Descric() )       		, ;     // [04] Descricao
		NIL                          		, ;     // [05] Help
		SX3->X3_TIPO                    	, ;    	// [06] Tipo do campo   COMBO, Get ou CHECK
		"@BMP"        						, ;    	// [07] Picture
		Nil                    				, ;     // [08] PictVar
		SX3->X3_F3                   		, ;    	// [09] F3
		SX3->X3_VISUAL <> 'V' 				, ;    	// [10] Editavel
		SX3->X3_FOLDER               		, ;    	// [11] Folder
		SX3->X3_FOLDER               		, ;    	// [12] Group
		{} /*StrTokArr(AllTrim(X3CBox()),';') */ , ;    	// [13] Lista Combo
		Nil                    				, ;    	// [14] Tam Max Combo
		SX3->X3_INIBRW               		, ;    	// [15] Inic. Browse
		SX3->X3_CONTEXT <> "R" )    				// [16] Virtual
		
	Endif

SX3->( DbSetOrder(1) )



// Instacia a View
oView := FwFormView():New()
oView:SetModel(oModel)

// Cabecalho
oView:AddField('VwFieldP01', oStruP01 , 'MdFieldP01') 

// Item do cupom fiscal
oView:AddGrid( 'VwGridP02', oStruIP02 , 'MdGridP02')
oView:AddIncrementField( 'VwGridP02' , 'P02_ITEM' )

//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados) 
oView:AddField( 'VwCalcP02', oCalc1, 'MdCalcP02' )
oView:AddField( 'VwCalcP03', oCalc2, 'MdCalcP03' )

// Pagamentos
oView:AddGrid( 'VwGridP03', oStruIP03 , 'MdGridP03')
oView:AddIncrementField( 'VwGridP03' , 'P03_SEQ' )


// Panels
// Cria os panels de forma bonita com margem 
oView:CreateHorizontalBox("SUPERIOR",32)
oView:CreateVerticalBox( 'PANE_CAB' , 0.5 , "SUPERIOR" )
oView:CreateVerticalBox( 'PANM_CAB' ,  98 , "SUPERIOR" )
oView:CreateVerticalBox( 'PAND_CAB' , 0.5 , "SUPERIOR" )


// Detalhe dos produtos, formas de pagamento, impostos e totalizadores
oView:CreateHorizontalBox("MARGEM_SUP",3)
oView:CreateHorizontalBox("MEIO",60)
oView:CreateHorizontalBox("INFERIOR",5)

oView:CreateFolder('FOLDER_01',"MEIO")
oView:AddSheet('FOLDER_01','ABA_01', "Produtos" 	) // "Clientes "
oView:AddSheet('FOLDER_01','ABA_02', "Pagamentos" 	) 

oView:CreateHorizontalBox( 'PAN_F01_TOP' , 85 ,,, "FOLDER_01" , "ABA_01" )
oView:CreateHorizontalBox( 'PAN_F01_BOT' , 15 ,,, "FOLDER_01" , "ABA_01" )

oView:CreateHorizontalBox( 'PAN_F02_TOP' , 85 ,,, "FOLDER_01" , "ABA_02" )
oView:CreateHorizontalBox( 'PAN_F02_BOT' , 15 ,,, "FOLDER_01" , "ABA_02" )


// Define 
oView:SetOwnerView('VwFieldP01',"PANM_CAB")

oView:SetOwnerView('VwGridP02',"PAN_F01_TOP")
oView:SetOwnerView('VwCalcP02',"PAN_F01_BOT")

oView:SetOwnerView('VwGridP03',"PAN_F02_TOP")
oView:SetOwnerView('VwCalcP03',"PAN_F02_BOT")

// Habilita titulos para melhor visualizacao do usuario

oView:EnableTitleView('VwFieldP01',"Cupom SAT-CE : Cabe�alho" )

oView:SetViewProperty("VwGridP02"     , "SETCSS", {cModelCSS})   
oView:SetViewProperty("VwGridP03"     , "SETCSS", {cModelCSS})   
oView:SetViewProperty("VwFieldP01"     , "SETCSS", {cFldCSS})   

oView:SetViewProperty("VwGridP02"     , 'ENABLENEWGRID' )   
oView:SetViewProperty("VwGridP03"     , 'ENABLENEWGRID' )

oView:SetViewProperty("VwGridP02"     , "GRIDVSCROLL", {.F.})   
oView:SetViewProperty("VwGridP03"     , "GRIDVSCROLL", {.F.})

oView:SetViewProperty("VwGridP02"     , "GRIDROWHEIGHT", {5})   
oView:SetViewProperty("VwGridP03"     , "GRIDROWHEIGHT", {5})


Return(oView)


/*
MenuDef
Funcao generica MVC com as opcoes de menu

@author Mauro Paladini
@since 09/12/2014
@version 1.0
*/

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE "Pesquisar"            	ACTION "PesqBrw"            		OPERATION 1 ACCESS 0  
ADD OPTION aRotina TITLE "Visualizar"           	ACTION "VIEWDEF.CALOJA701" 			OPERATION 2 ACCESS 0 
ADD OPTION aRotina TITLE "Incluir"              	ACTION "VIEWDEF.CALOJA701" 			OPERATION 3 ACCESS 0 
ADD OPTION aRotina TITLE "Alterar"              	ACTION "VIEWDEF.CALOJA701" 			OPERATION 4 ACCESS 0 
ADD OPTION aRotina TITLE "Excluir"              	ACTION "VIEWDEF.CALOJA701" 			OPERATION 5 ACCESS 0 
ADD OPTION aRotina TITLE "Validar registros"    	ACTION "u_CaValida" 				OPERATION 4 ACCESS 0
ADD OPTION aRotina TITLE "Gera or�amento"	      	ACTION "u_CaOrcamento" 				OPERATION 4 ACCESS 0 
ADD OPTION aRotina TITLE "Finaliza cupom"	      	ACTION "u_SATFinaliza" 				OPERATION 4 ACCESS 0 

Return ( aRotina )




/*/{Protheus.doc} LoadCores
Carrega as cores da legenda do Grid

@author Mauro Paladini
@since 04/03/2015
@version 1.0

/*/

Static Function LoadCores( oGrid )

Local nOperacao := oGrid:GetModel():GetOperation()
Local aStruct   := oGrid:oFormModelStruct:GetFields()

Local nPosBmp	:= aScan(aStruct , {|x| RTrim(x[3]) == "P02_P02BMP" })
Local nPosStat	:= aScan(aStruct , {|x| RTrim(x[3]) == "P02_STATUS" }) 

Local nX		:= 0
Local aRet      := {}

If nOperacao <> OP_INCLUIR
	
	aRet := FormLoadGrid( oGrid )
	
	For nX := 1 To Len(aRet)		
		If nPosBmp > 0		
			IF RTrim(aRet[nX][2][nPosStat]) $ "18" 		
				aRet[nX][2][nPosBmp] := "BR_VERMELHO"			
			Else
				aRet[nX][2][nPosBmp] := "BR_VERDE"
			Endif
		Endif	
	Next nX 
		
EndIf

Return aRet



/*/{Protheus.doc} CaFinaliza
Finaliza um cupom/orcamento em venda na base de dados utilizando LjGravaBatch OFF-LINE
Deve estar posicionado na tabela P01

@author Mauro Paladini
@since 04/03/2015
@version 1.0            
@return Nil
/*/

User Function CaFinaliza()

Local aArea             := GetArea()
Local aSave             := { "P01" , "P02" , "P03" , "SM0" } 
Local aAlias            := u_xGetArea(aSave)
Local lGravacao     	:= .F.
Local lNaoProduziu  	:= .F.
Local lEstneg           := If( GetMv("MV_ESTNEG",,"S") == "S" , .T. ,.F. )    
Local cTMPCP            := GetMv("FS_TMPCP",,"001")
Local aLogOP            := {}
Local nL

Local cNumOrc       	:= P01->P01_NUMORC
Local cArqImp       	:= P01->P01_ARQ
Local cCupom        	:= P01->P01_DOC
Local aCupom        	:= {}

Local cMsgLog   		:= ""
Local lErroSL4  		:= .F.
Local lRet          	:= .T.

Local cModAnt       := cModulo
Local nModAnt       := nModulo 

Local dDataBackup   := dDatabase

Private cLogT       := "\system\wfgravabatch.log"
Private cLogMT 		 := "\log_pdv\grvbatch_thread_singleuser.log"

Private cTextoErro	:= ''
Private aResume 	:= {}

IF P01->P01_STATUS == CANCELADO  
    Aviso("Aten��o","Este cupom encontra-se estornado",{"OK"},3)
    Return  
Endif

// Validacoes iniciais
IF Empty(P01->P01_NUMORC) 
    Aviso("Aten��o","Or�amento de venda ainda n�o gerado.  Para finalizar o cupom um or�amento de venda deve ser gerado.",{"OK"},3)
    Return  
Endif

IF ( P01->P01_STATUS < ORCAMENTO .And. Empty(P01->P01_NUMORC) )   
    Aviso("Aten��o","Or�amento de venda ainda n�o gerado.  Para finalizar o cupom um or�amento de venda deve ser gerado.",{"OK"},3)
    Return  
Endif

If !IsBlind() .And. !MsgYesNo("Confirma a efetiva��o deste cupom fiscal importado como venda ?")
    Return
Endif

SaveInter()

dDataBackup     := dDatabase
dDatabase       := P01->P01_DTEMIS      

// -- Controle de Erros

aResume     	:= {}
cTextoErro      := ""
bOldError       := ErrorBlock( { |x| VerIfErro(x, @aResume , @cTextoErro ) } ) // muda code-block de erro
            
Begin Transaction 

// Begin Sequence

// Efetua valida��o das formas de pagamento na SL4

SL4->( DbSetOrder(1) )

IF SL4->( DbSeek( P01->P01_FILIAL + P01->P01_NUMORC ) )

    cMsgLog     := ""
    lErroSL4    := .F.
    
    While SL4->( !Eof() .And. L4_FILIAL+L4_NUM == P01->P01_FILIAL + P01->P01_NUMORC ) 
    
        IF RTrim(SL4->L4_FORMA) $ "CC|CO|CD|FI|VA"
        
            IF Empty( RTrim( SL4->L4_ADMINIS ) )
            
                cMsgLog += "Adm n�o preenchida (SL4) " + RTrim(SL4->L4_FORMA) +  " Valor R$ " + AllTrim( Str(SL4->L4_VALOR,10,2) ) 
                
                aAdd( aResume , { .F. , "" , cMsgLog , "" } )
                
                cMsgLog += CRLF
                
                lErroSL4    := .T.
            
            Endif
        
        Endif
    
        SL4->( DbSkip() )
            
    End
    
    IF lErroSL4
    
        DisarmTransaction()

        RecLock("P01",.F.)
            P01->P01_STATUS := COMERRO
            P01->P01_LOGIMP := cMsgLog      
        MsUnLock()

        Aviso("Aten��o","N�o foi poss�vel realizar a efetiva��o do cupom.  Revise os meios de pagamentos selecionados." ,{"OK"},3)
        
        RollBackSX8()       
        RestInter()
        
        u_TibDeletaOrc( P01->P01_FILIAL , P01->P01_NUMORC , P01->P01_DOC )
        
        dDatabase   := dDataBackup
        lRet        := .F.
                
    Endif
    
Endif


SL1->( DbSetOrder(1) )

IF !lErroSL4 .And. SL1->( DbSeek( P01->P01_FILIAL + P01->P01_NUMORC ) )
 
   
    If lRet 
    
        RestInter()
    
        DbSelectArea("SL1")
    
        cModulo := "LOJ"
        nModulo := 12
        
        M->LQ_CLIENTE           := SL1->L1_CLIENTE
        M->LQ_LOJA              := SL1->L1_LOJA
        
        // Transforma o orcamento em cupom fiscal   
        RecLock("SL1",.F.)
            SL1->L1_SITUA   := "RX" 
        MsUnLock()
        
        SetFunName("LOJA701")
    
        IF !IsBlind()   
            FWMsgRun( , {|| u_TibGravaBatch( cEmpAnt , cFilAnt , /*cIntervalo*/ , /*cMinReproc*/ , SL1->L1_NUM , @lGravacao ) } ,, "Processando venda ... " )
        Else
            u_TibGravaBatch( cEmpAnt , cFilAnt , /*cIntervalo*/ , /*cMinReproc*/ , SL1->L1_NUM , @lGravacao )
        Endif   
        
        // Caso nao consiga efetivar a venda deve desfazer as OP's criadas  
        IF !lGravacao
            DisarmTransaction() 
            RollBackSX8()
        Endif
            
        RecLock("P01",.F.)
            P01->P01_STATUS := IIF( lGravacao , VENDA , COMERRO )
            P01->P01_LOGIMP := IIF( lGravacao , "" , "Erro na finaliza��o da LjGrvBatch" ) 
        MsUnLock()
    
    Endif

Endif

cModulo     := cModAnt
nModulo     := nModAnt
dDatabase   := dDataBackup

/*
Recover
                
    aAdd( aResume , { .F. , "" , "ERRO FATAL" , cTextoErro } )
    
    RestArea(aArea)

    cModulo     := cModAnt
    nModulo     := nModAnt
    ddatabase   := dDataBackup

    DisarmTransaction()
    
    u_xRestArea(aAlias)

End Sequence

ErrorBlock( bOldError )     
*/

End Transaction

If Len(aResume) > 0
    WiResume( aResume )
Endif

aResume     := {}
dDatabase   := dDataBackup

u_xRestArea(aAlias)

RestArea(aArea)

Return


/*/{Protheus.doc} SATFinaliza
@type 		User Function
@author 	Mauro Paladini
@since 		Feb 23, 2019  1:44:56 PM
@version 	1.0
@return 	${return}, ${return_description}
/*/

User Function SATFinaliza()

Local aAreas  	:= { 	DA0->(GetArea()) , DA1->(GetArea()) , SB0->(GetArea()) , SB1->(GetArea()) ,;
						SA1->(GetArea()) , SF4->(GetArea()) , SX5->(GetArea()) , SE1->(GetArea()) ,;
						SA6->(GetArea()) , SLG->(GetArea()) , SLF->(GetArea()) , P01->(GetArea()) ,;
						P02->(GetArea()) , P03->(GetArea()) , SAE->(GetArea()) , SX2->(GetArea()) ,;
						SX3->(GetArea()) , SM0->(GetArea()) , SX6->(GetArea()) , GetArea() }

Local aDados		:= { { P01->P01_ARQ , P01->P01_DOC } } 
Local dDataBackup   := dDatabase

Private cLogT       := "\system\wfgravabatch.log"
Private cLogMT 		:= "\log_pdv\grvbatch_thread_singleuser.log"
Private cTextoErro	:= ''
Private aResume 	:= {}

IF P01->P01_STATUS == CANCELADO  
    Aviso("Aten��o","Este cupom encontra-se estornado",{"OK"},3)
    Return  
Endif

// Validacoes iniciais
IF Empty(P01->P01_NUMORC) 
    Aviso("Aten��o","Or�amento de venda ainda n�o gerado.  Para finalizar o cupom um or�amento de venda deve ser gerado.",{"OK"},3)
    Return  
Endif

IF ( P01->P01_STATUS < ORCAMENTO .And. Empty(P01->P01_NUMORC) )   
    Aviso("Aten��o","Or�amento de venda ainda n�o gerado.  Para finalizar o cupom um or�amento de venda deve ser gerado.",{"OK"},3)
    Return  
Endif

IF ( P01->P01_STATUS = VENDA )   
    Aviso("Aten��o","Cupom Fiscal j� efetivado",{"OK"},3)
    Return  
Endif

If !IsBlind() .And. !MsgYesNo("Confirma a efetiva��o deste cupom fiscal importado como venda ?")
    Return
Endif

SaveInter()

FWMsgRun( , {|| u_TibMTBatch( aDados ) } ,, "Processando Cupom Fiscal ... " )

RestInter()

If Len(aResume) > 0
    WiResume( aResume )
Endif

aResume     := {}
dDatabase   := dDataBackup

AEval(aAreas, {|x| RestArea(x)})

Return


/*
WiResume
Rotina para exibi��o dos dados processados

@author Mauro Paladini
@since  27/10/2016
@version 1.0
*/

Static Function WiResume( aResume )

    Local oOk           := LoadBitmap( GetResources(), "ENABLE" )
    Local oNoOk         := LoadBitmap( GetResources(), "DISABLE" )
    Local oWizard       := Nil
    Local oBrw          := Nil
    
    Local nLog      := 0
    
    Private aItens  := {}

    For nLog := 1 To Len(aResume)

        aAdd( aItens , { .F. , aResume[nLog][2] , aResume[nLog][3] , aResume[nLog][4] } )       
    
    Next nLog   


    // - Exibe itens com problema de importa��o

    DEFINE WIZARD oWizard TITLE "TOTVS" HEADER "Relat�rio de processamento" MESSAGE "Os seguintes eventos foram registrados durante o processamento :" PANEL NEXT {|| .F. } FINISH {|| .T.}

    oBrw := TWBrowse():New(05,7,290,115,,{ "","Mensagem"},,oWizard:oMPanel[1],,,,,{||},,,,,,,.F.,,.T.,,.F.,,, )

    oBrw:SetArray(aItens)
    oBrw:bLine := {||{  IIF(    aItens[oBrw:nAt,1] , oOk ,  oNoOk ) ,;
                                    aItens[oBrw:nAt,3] }}

    oBrw:bLDblClick:= {|| ShowLog( aItens[oBrw:nAt,4])  }

//  @ 122, 240 BUTTON oButtonLog PROMPT "Exportar Log"  SIZE 040, 013   ACTION MsgStop("Em desenvolvimento")  OF oWizard:oMPanel[1] PIXEL
  
    ACTIVATE WIZARD oWizard CENTERED

Return





/*/{Protheus.doc} VeriErro
Retorna erro fatal ocorrido no processo executado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/

Static Function VerIfErro( e , aResume , cTextoErro )

    Local lRet      := .F.
    Local cAutoGets := {}
        
    If e:gencode > 0 
    
        cTextoErro := "DESCRIPTION: " + e:DESCRIPTION + CRLF
        cTextoErro += "ERRORSTACK:" + CRLF
        cTextoErro += e:ERRORSTACK
            
        lRet:=.T.

        Break

    EndIf

Return lRet




/*/{Protheus.doc} FWGetErro
Retorna erro ocorrido no processo executado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/
Static Function FWGetErro( oObjMdl )

Local aErro := {}
Local cRet  := ""

DEFAULT oObjMdl := FwModelActive()

aErro := oObjMdl:GetErrorMessage()

If Len(aErro) > 0

    cRet    := '[' + AllToChar( aErro[MODEL_MSGERR_IDFORMERR] ) + ']'   + CRLF
    cRet    += '[' + AllToChar( aErro[MODEL_MSGERR_IDFIELDERR] ) + ']' + CRLF
    cRet    += '[' + AllToChar( aErro[MODEL_MSGERR_ID] ) + ']'  + CRLF
    cRet    += '[' + AllToChar( aErro[MODEL_MSGERR_MESSAGE] ) + '|' + AllToChar( aErro[MODEL_MSGERR_SOLUCTION] ) + ']' + CRLF

EndIf

Return cRet


/*
ShowLog
Exibe detalhes do erro apresentado pela rotina

@author Mauro Paladini
@since 10/04/2015
@version 1.0
*/

Static Function ShowLog( cMemo )

    Local oDlg     := NIL
    Local oFont    := NIL
    Local oMemo    := NIL

    DEFINE FONT oFont NAME "Courier New" SIZE 5,0
    DEFINE MSDIALOG oDlg TITLE 'Log' From 3,0 to 340,617 PIXEL

    @ 5,5 GET oMemo  VAR cMemo MEMO SIZE 300,145 OF oDlg PIXEL

    oMemo:bRClicked := {|| AllwaysTrue()}
    oMemo:oFont:=oFont

    DEFINE SBUTTON  FROM 153,280 TYPE 1 ACTION oDlg:End() ENABLE OF oDlg PIXEL //Apaga
    ACTIVATE MSDIALOG oDlg CENTER

Return Nil




/*
Log2Array
Abre arquivo de LOG da ExecAuto e retorna em um array
@author Mauro Paladini
@since 10/04/2015
@version 1.0
*/

Static Function Log2Array(cFileLog, lAchaErro )
    
    Local nHdl      := -1
    Local nPos      := 0
    Local nBytes    := 0
    
    Local cLinha    := ""
    Local cChar     := ""

    Local cRet      := ""
    Local aRet      := {}

    Default cFileLog    := ""
    Default lAchaErro   := .T.
    
    IF !Empty(cFileLog)
        nHdl := FOpen(cFileLog)
    Endif

    If !(nHdl == -1)
    
        nBytes := FSeek(nHdl, 0, 2)
        FSeek(nHdl, 0)

        If nBytes > 0

            For nPos := 1 To nBytes
    
                FRead(nHdl, @cChar, 1)
                
                // Quebra de linha (chr(13)+chr(10) = CR+LF)
                If cChar == Chr(10) .Or. nPos == nBytes
                    
                    If lAchaErro .And. "< -- Invalido" $ cLinha
                        cRet    := "Campo -> " + cLinha
                        Exit
                    Endif

                    IF !Empty(cLinha)
                        aAdd(aRet, cLinha)
                    Endif
                
                    cLinha :=  ""
                                         
                ElseIf cChar <> Chr(13)

                    cLinha += cChar

                EndIf
    
            Next nPos

        EndIf
    
        FClose(nHdl)
    
    Endif
        
    IF Empty(cRet) .And.  Len(aRet) >= 2
        cRet := RTrim(aRet[1]) + " => " + RTrim(aRet[Len(aRet)])
    Endif
    
Return IIF( lAchaErro , cRet , aRet )

/*/{Protheus.doc} CaValida
Valida as inconsistencias dos registros da venda

@author Mauro Paladini
@since 05/03/2015
@version 1.0
@return ${return}, ${return_description}
/*/
User Function CaValida()

Local aArea		:= GetArea()
Local lCupomOk	:= .T.
Local cMsg		:= ""

IF P01->P01_STATUS == CANCELADO  
	Aviso("Aten��o","Este cupom encontra-se estornado",{"OK"},3)
	Return	
Endif

IF !(P01->P01_STATUS $ INTEGRADO+INVALIDO+COMERRO)
	Aviso("Aten��o","Registro j� validado e n�o poder� sevalidado novamente." ,{"OK"},3)
	Return	
Endif

If !MsgYesNo("Confirma a valida��o dos registros desta venda ?")
	Return
Endif

// Valida estorno
IF P01->P01_ESTORN == "S"

	IF P02->( DbSeek( xFilial("P02") + P01->P01_ARQ + P01->P01_DOC ) )

		While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + P02_DOC == xFilial("P02") + P01->P01_ARQ + P01->P01_DOC )
		
			RecLock("P02",.F.)
				P02->P02_STATUS	:= CANCELADO
				P02->P02_LOGIMP	:= "Cupom cancelado" 						
			MsUnLock()					
			
			P02->( DbSkip() )
			
		End
	
	Endif

	// Atualiza status
	RecLock("P01",.F.)
		P01->P01_STATUS	:= CANCELADO
		P01->P01_LOGIMP	:= "Cupom cancelado"					
	MsUnLock()
	
	Return		

Endif


// Efetua todas as valida��es
FWMsgRun( , {|| u_ValidaCupom( P01->P01_ARQ, P01->P01_DOC, @lCupomOk, @cMsg ) } ,, "Validando registros ..." )

// Atualiza status
RecLock("P01",.F.)
	P01->P01_STATUS	:= IIF( lCupomOk , VALIDADO , INVALIDO )
	P01->P01_LOGIMP	:= cMsg 						
MsUnLock()	

IF !lCupomOk 
	Aviso("Aten��o","Ocorreram erros na valida��o do registro selecionado." ,{"OK"},3)
Endif

RestArea(aArea)

Return









/*/{Protheus.doc} CaOrcamento
Grava como orcamentoo um cupom importado e validado 100%

@author Mauro Paladini
@since 04/03/2015
@version 1.0			
@return ${return}, ${return_description}

/*/

User Function CaOrcamento()

Local aArea	:= GetArea()

IF P01->P01_STATUS == CANCELADO  
	Aviso("Aten��o","Este cupom encontra-se estornado",{"OK"},3)
	Return	
Endif

IF !Empty(P01->P01_NUMORC) 
	Aviso("Aten��o","Or�amento " + SL1->L1_NUM + " j� gerado para este cupom fiscal" ,{"OK"},3)
	Return	
Endif

IF !( P01->P01_STATUS $ VALIDADO )
	Aviso("Aten��o","O cupom deve estar validado para ser gravado como or�amento." ,{"OK"},3)
	Return	
Endif

If !MsgYesNo("Confirma a gera��o do or�amento de venda para este registro ?")
	Return
Endif

FWMsgRun( , {|| u_TibGrvOrc( P01->P01_ARQ , P01->P01_DOCCOO ) } ,, "Gerando or�amento ... " )	

RestArea(aArea)

Return



Static Function AjustaSX3()

Local aArea := GetArea()

dbSelectArea('SX3')
dbSetOrder(2)

If SX3->( DbSeek("P01_STATUS") )
	Reclock("SX3",.F.)
		REPLACE X3_CBOX WITH "0=Integrado;1=Integrado nao validado;2=Validado;3=Importado Orcamento;4=Venda Finalizada;8=Inconsistencias;5=Cancelado"
	MsUnlock()
EndIf

Return