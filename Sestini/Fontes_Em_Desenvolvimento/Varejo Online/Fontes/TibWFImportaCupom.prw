#Include 'Protheus.ch'
#include 'tbiconn.ch'

/*/{Protheus.doc} WfImpCupom

Workflow 
Chama a rotina de importacao dos arquivos gerados com base no layout da Portaria CAT52

@author Mauro Paladini
@since 12/02/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)

/*/

User Function TibWfImpCupom(CRPCEmp, cRPCFil)

Local nX			:= 0

Local aLayout52		:= {}
Local cVerCat52		:= ""
Local cLayout52		:= ""

Local cPath52		:= ""
Local cLidos52		:= ""
Local cErros		:= ""
Local aArquivos		:= {}
Local oHLayout		:= Nil

Local aArqMT		:= {} 
Local lUsaMT		:= .F.
Local nQtdThread	:= 0 
Local lCriouAmb		:= .F. 
Local cExtArq		:= ""

Private aCatFilial	:= {}
Private lXML		:= .F.

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
	RPCSetType(3) 
	RPCSETENV(cRPCEmp, cRPCFil)
	lCriouAmb	:= .T.
Endif 


// Valida a Build atual devido ao uso de HashMap
If RTrim(GetBuild()) <= "7.00.121227P"
	
	If !IsBlind()	
		Aviso("Aten��o","Build incompat�vel para a utiliza��o desta ferramenta.  Atualize seu ambiente para uma Build superior a 7.00.121227P.",{"OK"},3)
	Else
		CONOUT("Build incompat�vel para a utiliza��o desta ferramenta.  Atualize seu ambiente para uma Build superior a 7.00.121227P.")
	Endif
	
	If lCriouAmb
		RESET ENVIRONMENT
	Endif
	
	Return
		
Endif

cVerCat52	:= GetMV("FS_VCAT52",,"V01")
cLayout52	:= "TIBCAT52." + cVerCat52
cPath52		:= GetMv("FS_DIR52",,"\integracao_pdv\importar\")
cLidos52	:= GetMv("FS_LIDOS522",,"\integracao_pdv\lidos\")
cErros		:= GetMv("FS_LIDOS522",,"\integracao_pdv\invalidos\") 

lUsaMT		:= GetMv("FS_CATMT"	,,.F.)
nQtdThread	:= IIF( lUsaMT , GetMv("FS_NTIMP",,5) , 0 )

cExtArq		:= IIF( FindFunction("U_XMLCLI") , "*.XML" , "*.*" )
lXML		:= IIF( FindFunction("U_XMLCLI") , .T. , .F. )

// Carrega empresas e filiais em memoria
u_CatLoadFilial(@aCatFilial)
  
// Verifica arquivos a serem importados
aDir( cPath52 + cExtArq , aArquivos )

If Len(aArquivos) == 0
	If !IsBlind()					
		MsgInfo("N�o existem arquivos a serem processados em " + cPath52)
	EndIf
	Return
Endif

// Carrega em memoria o layout do arquivo mapa de configuracao do layout

IF !lXML

	aLayout52 := u_TibLoadLayout( cLayout52 )
	
	// Valida que exista campos no layout
	IF Len(aLayout52) == 0
		
		If !IsBlind()					
			MsgInfo("Nenhum campo foi retornado para o arquivo de layout")
		EndIf
		Return
		
	Endif
	
	
	// Cria Obj Hash com a estrutura dos campos
	oHLayout := aToHM(aLayout52)

Endif


If lUsaMT .And. nQtdThread > 0 // MultiThread

	//Cria um array com os nomes dos arquivos a serem importados e um espa�o em branco. 
	//Ser� usado para que as threads possam identificar quais arquivos ainda est�o pendentes de importa��o.
	
	For nX := 1 To Len(aArquivos)
		aAdd(aArqMT, { aArquivos[nX] , "" }) 
	Next nX
	
	nX := nX - 1
	
	//Se a n� de arquivos for maior que a qtd de threads � necess�rio o tratamento abaixo
	If nX > nQtdThread 
	
		While nX > nQtdThread		
			FWMsgRun( , {|| U_PackMT( nQtdThread, "u_TibImpArquivo", { aLayout52, aArqMT, aCatFilial  } , "IMP" ) } ,, "Importando arquivos"  )			
			nX := nX - nQtdThread		
		EndDo
		
		If nX > 0 		
			FWMsgRun( , {|| U_PackMT( nX , "u_TibImpArquivo", {aLayout52, aArqMT, aCatFilial } , "IMP" ) } ,, "Importando arquivos"  )							
		Endif

	//Se o n� de arquivos for menor que a qtd de threads, sobe s� a qtd necess�ria.
	ElseIf nX <= nQtdThread 
		FWMsgRun( , {|| U_PackMT( nX, "u_TibImpArquivo", {aLayout52, aArqMT, aCatFilial } , "IMP" ) } ,, "Importando arquivos"  )
	EndIf
	
Else

	// Importa os arquivos como monothread - Processo normal.
	For nX := 1 To Len(aArquivos)
		If !IsBlind()
			FWMsgRun( , {|| u_TibImpArquivo( {aLayout52, aArquivos[nX] , aCatFilial } ) } ,, "Importando arquivo " + aArquivos[nX] )
		Endif 
	Next nX

EndIf

If !IsBlind()	
	Aviso("Aviso","Processo finalizado",{"OK"},2)
Endif

If lCriouAmb
	RESET ENVIRONMENT
Endif

Return





/*/{Protheus.doc} CatLoadFilial
Carrega em memoria todas as filiais da empresa corrente

@author Mauro Paladini 
@since 27/02/2015
@version 1.0
@param aCatFilial, array, Array que recebera as filiais disponiveis no SIGAMAT
@return ${return}, ${return_description}
/*/

User Function CatLoadFilial(aCatFilial)

Local aArea	:= GetArea()
Local aAreaSM0	:= SM0->( GetArea() )

Default aCatFilial	:= {}

SM0->( DbGoTop() )

IF SM0->( DbSeek( cEmpAnt ) )

	While SM0->( !Eof() .And. SM0->M0_CODIGO == cEmpAnt )
	
		aAdd( aCatFilial, { SM0->M0_CODIGO , SM0->M0_CODFIL , SM0->M0_FILIAL , SM0->M0_NOME , SM0->M0_CGC } )
		SM0->( DbSkip() )		
		
	End

Endif

RestArea(aAreaSM0)
RestArea(aArea)

Return 




/*/{Protheus.doc} CatRetFilial
Retorna o codigo da filial equivalente ao CGC cadastrado no SIGAMAT 

@author Mauro Paladini
@since 27/02/2015
@version 1.0
@param cCGC, character, CGC da empresa cadastrado no SIGAMAT
@return ${return}, 		$Codigo da filial correspondente no SIGAMAT
/*/

User Function CatRetFilial(cCGC)

Local nPos	:= aScan( aCatFilial , { |x| Upper( AllTrim( x[5] ) ) == cCGC } )
Local cRet	:= ""
Local nLen	:= Len(xFilial("P01"))

IF nPos > 0
	cRet	:= Padr(aCatFilial[nPos][2],nLen)
Endif

Return cRet