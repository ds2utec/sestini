#include "TOTVS.CH"

/*/{Protheus.doc} MT010INC()
Ponto de entrada na inclus�o do cadastro de produtos

@type function
@author 	Mauro Paladini
@since 	01/10/2018
@version 	1.0
/*/

User Function MT010INC()

CONOUT('MT010INC')

// ## Mauro Paladini - INTEGRACAO VAREJO ONLINE ## 
// -- Envia regostro para a fila de integracoes

Integracao()

// -- Fim Integracao

Return




/*/{Protheus.doc} Integracao()
Funcao auxiliar para enviar o registro atual para a fila de integracao
Prepara o Script e Chama Metodo responsavel pelas atualizacoes

@type function
@author 	Mauro Paladini
@since 	01/10/2018
@version 	1.0
/*/

Static Function Integracao()

Local cQuery    := StatiCCall( STNQUERY , ProdQuery , 1 )
Local cChave    := SB1->B1_COD      // -- Chave agrupadora

// -- 1=Inclus�o;2=Altera��o;3=Exclus�o
Local cTabOrig  := 'SB1'
Local cOperacao := '1'    
Local cKeyOrig  := SB1->B1_FILIAL + SB1->B1_COD
Local nIndOri   := 1

// 1=Sim 2=Nao
Local cBlqProx  := '1'     

IF !AtIsRotina('MATA010M') .and. !Empty( cQuery )

    // -- Instancia a classe
    
    oSestini := TransactRows():New()
    oSestini:SetVariables( 'MATA010' /*cOrigem */, cChave , cOperacao , cKeyOrig , cTabOrig , nIndOri , cBlqProx )
    oSestini:SetScript( cQuery )
    oSestini:AppendRow()
    oSestini:Destroy()
    
    Freeobj(oSestini)
    oSestini := Nil

Endif

Return

