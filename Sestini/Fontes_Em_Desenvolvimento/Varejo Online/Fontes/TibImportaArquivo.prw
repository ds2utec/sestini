#Include 'Protheus.ch'

#DEFINE TIPOREG		1
#DEFINE TITULO 		2
#DEFINE	CAMPO		3
#DEFINE TAMANHO		4
#DEFINE POSINI		5
#DEFINE POSFIM		6
#DEFINE FORMATO		7
#DEFINE DECIMAL		8 

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE EMPROCESSA	"9"

#DEFINE NTRYARQ		15		// Numero de tentativas default de operacoes com arquivo texto


/*/{Protheus.doc} TibImpArquivo
Funcao intermediaria que define para qual funcao ira enviar a thread de importacao de cupons

@author Mauro Paladini
@since 12/02/2015
@version 1.0
/*/

User Function TibImpArquivo( aParamFunc, aParamMT )

Local cNomeArq	:= aParamFunc[2]

Default aParamFunc	:= {} 
Default aParamMT 	:= {}

IF "XML" $ upper(cNomeArq)

	IF FindFunction("U_XMLCLI")
	
		// Chama fun��o personalizada do cliente para a importa��o do arquivo XML
		u_XMLCli( aParamFunc, IIF( aParamMT <> Nil `.And. Len(aParamMT) > 0 , aParamMT , Nil ) )
	
	Else
	
		// Funcao para importar os aruqivos vXML indo do SAT 
		u_SatImpArquivo( aParamFunc, IIF( aParamMT <> Nil `.And. Len(aParamMT) > 0 , aParamMT , Nil ) )
	
	Endif	
	
Else

	// Funcao para importat os cupons com arquivo TXT CAT 52
	u_TibTXTImporta( aParamFunc, IIF( aParamMT <> Nil `.And. Len(aParamMT) > 0 , aParamMT , Nil ) )

Endif

Return




/*/{Protheus.doc} TibTXTImporta

Abre arquivo gerado com base no layout da Portaria CAT52
Importacao do cupom fiscal do arquivo txt para as tabelas da familia P

@author Mauro Paladini
@since 12/02/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)

/*/


User Function TibTXTImporta( aParamFunc, aParamMT )

Local oLayout		:= aToHM(aParamFunc[1])

Local cPath52		:= GetMv("FS_DIR52",,"\integracao_pdv\importar\")
Local cLidos52		:= GetMv("FS_LIDOS522",,"\integracao_pdv\lidos\")
Local cErros		:= GetMv("FS_LIDOS522",,"\integracao_pdv\invalidos\")
Local nTamLin		:= TamSx3("P01_LINHA")[1]

Local cNomeArq		:= aParamFunc[2]
Local cThread		:= IIF( aParamMT == Nil .And. Len(aParamMT) == 0 , "singleuser" , aParamMT[1] )

Local cLogMT		:= "\logpdv\imparquivo_thread_" + cThread + ".log"
Local cTipoReg		:= ""
Local cAliasP		:= ""
Local cArqImp		:= cPath52 + cNomeArq   
Local lDefCampos	:= .F.
Local nX			:= 0
Local nLinha		:= 0

Local aCampos		:= {}
Local aTotais		:= {}  
Local cIDCV8		:= ""
Local cArqCNPJ		:= ""  
Local lReducaoZ		:= .F.
Local dDataMov		:= CTOD("") 		// 

Local tInicio		:= Time()	
Local tFim	

Private cLine		:= ""
Private cCatFilial	:= ""
Private aCatFilial	:= aParamFunc[3]


MakeDir("\logpdv") 
If File(cLogMt)
	FErase(cLogMT)
Endif

// Inicio
ProcLogIni( {}, cNomeArq ,, @cIdCV8)

LjWriteLog(cLogMT,"Iniciando Importacao de arquivos .. [" + cThread + "] ")


// Abre arquivo pa ser importado
If File(cArqImp)

	If FT_FUSE(cArqImp) >= 0
	
		// Identifica a filial do arquivo da CAT52			
		cArqCNPJ 	:= Substr(FT_FREADLN(),99,14)		
		cCatFilial	:= u_CatRetFilial(cArqCNPJ)
		
		
		// Valida se e uma filial valida
		IF Empty(cCatFilial)
		
			FT_FUSE()
			If u_Copia2Lidos( cArqImp , cErros+cNomeArq )			
				u_DeletaLido(cArqImp)				
			Endif
		
			ProcLogAtu( "ERRO", "CNPJ invalida para o cadastro de empresas" , /* detalhes */ ,,.T.)			

			tFim := ElapTime(tInicio,Time())	
			cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 
			
			LjWriteLog(cLogMT, "Arquivo  " + cArqImp + " [ERRO] " + cLog )

			If aParamMT <> Nil
				PutGlbValue( aParamMT[1] , "4" )
				GlbUnLock()
			EndIf
			
			Return				
			
		Endif 	
							
		
		// Valida se o arquivo j� foi importado 
		P01->( DbSetOrder(1) )
		IF P01->( DbSeek( cCatFilial + RTrim(cNomeArq) ) )
		
			FT_FUSE()
			If u_Copia2Lidos( cArqImp , cErros+cNomeArq )			
				u_DeletaLido(cArqImp)				
			Endif

			If aParamMT <> Nil
				PutGlbValue( aParamMT[1] , "4" )
				GlbUnLock()
			EndIf

			ProcLogAtu( "ERRO", "Arquivo j� processado" , /* detalhes */ ,,.T.)

			tFim := ElapTime(tInicio,Time())	
			cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 
			
			LjWriteLog(cLogMT, "Arquivo  " + cArqImp + " [ERRO] " + cLog )

			Return

		Endif 	
	
		PtInternal(1, "Importando arquivo " + cArqImp )
		
		Begin Transaction
					
			ProcLogAtu( "INICIO", "Importacao do arquivo " ,,,.T.)
			
			// Validar a integridade do arquivo
			
			FT_FGOTOP()
			
			While (!FT_FEOF())
			
				nLinha		+= 1
				cLine 		:= Upper(FT_FREADLN())			
				
				IF cTipoReg <> Left(cLine,3)  						
					cTipoReg	:= Left(cLine,3)			
				Endif
						
				// Ignora totalizadores da Reducao Z
				If ! cTipoReg $ "E14|E15|E21"

					IF cTipoReg $ "E01|E12|E13"
						aAdd( aTotais , cLine )    

						// Obtem a data do movimento para controlar os cupons que forem
						// emitidos depois da meia-noite pois fazem parte da mesma data de emissao
						If cTipoReg == "E12"
							IF HMGet( oLayout , cTipoReg , @aCampos )
								dDataMov	:= u_ValorCampo("FI_DTMOVTO" , aCampos[ Ascan( aCampos,{ |x|Trim(x[CAMPO])== "FI_DTMOVTO" } ) ] )
							Endif  
						Endif

					Endif 
				
					If cTipoReg == "E13"
						lReducaoZ 	:= .T.
					Endif					
					
					FT_FSKIP()
					Loop

				Endif
	
				lDefCampos		:= HMGet( oLayout , cTipoReg , @aCampos )
								
				If lDefCampos
				
					cAliasP		:= Left(aCampos[1,CAMPO],3) 					

					RecLock(cAliasP,.T.)
							
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_FILIAL" )	, cCatFilial ) )
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_STATUS" )	, INTEGRADO ) )
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_ARQ" )		, Upper(RTrim(cNomeArq)) ) )
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_DTIMP" )	, Date() ) )
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_HRIMP" )	, Time() ) )
					(cAliasP)->( FieldPut( FieldPos( cAliasP + "_LINHA" )	, StrZero(nLinha,nTamLin) ) )
								
					For nX := 1 To Len(aCampos)					
						IF aCampos[nX,CAMPO] <> "NAOUSADO"
							(cAliasP)->( FieldPut( FieldPos( aCampos[nX,CAMPO] ), u_ValorCampo( aCampos[nX,CAMPO], aCampos[nX] ) ) )											
						Endif											
					Next nX 
					
					// Excessao para gravar nos cupons a data de emissao de acordo com a Reducao Z
					// por causa da situacao onde o cupom eh emitido apos a meia-noite					
					IF cAliasP $ "P01|P02"					
						(cAliasP)->( FieldPut( FieldPos( cAliasP + "_DTEMIS" )	, dDataMov ) )
					Elseif cAliasP $ "P03"
						(cAliasP)->( FieldPut( FieldPos( cAliasP + "_EMISSA" )	, dDataMov ) )
					Endif
					
					MsUnLock()
				
				Endif		
			
				FT_FSKIP()
			
			End
			
			// Grava registro referente a reducao Z
			If lReducaoZ
				u_LjGeraSFI(cCatFilial, aTotais, oLayout)
			Endif
							
		End Transaction
		
		FT_FUSE()
				
		PtInternal(1, "" )
		
		// Move para a estrutura de arquivos lidos		
		If u_Copia2Lidos( cArqImp , cLidos52+cNomeArq )			
			u_DeletaLido(cArqImp)		
		Else
			ProcLogAtu( "ERRO", "Nao foi poss�vel mover arquivo" ,,, .T.)		
		Endif		
		
		ProcLogAtu( "FIM",,,,.T. )
		
		tFim := ElapTime(tInicio,Time())	
		cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 
		
		LjWriteLog(cLogMT, "Arquivo  " + cArqImp + " [OK] " + cLog )
		
	Else
		If !IsBlind()					
			MsgInfo("Imposs�vel abrir o arquivo " + cNomeArq )
		EndIf
		
		tFim := ElapTime(tInicio,Time())	
		cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 
		
		LjWriteLog(cLogMT, "Arquivo  " + cArqImp + " [ERRO] " + cLog )
				
	Endif
	
Endif

If aParamMT <> Nil
	PutGlbValue( aParamMT[1] , "4" ) // STATUS 4 - Finalizado
	GlbUnLock()
EndIf

Return



/*/{Protheus.doc} ValorCampo
Retorna o valor do campo do arquivo de acordo com o formato informado
 
@author Mauro Paladini
@since 13/02/2015
@version 1.0
/*/

User Function ValorCampo( cCampo, aCampos )

Local xValor	:= Nil
Local nDecimais	:= 2

Default cLine	:= ""
Default cCampo	:= ""
Default aCampos	:= {}

xValor := Substr( cLine, Val(aCampos[POSINI]) , Val(aCampos[TAMANHO]) )

If aCampos[FORMATO] == "N"

	// Considera qtd de decimais do layout
	If Len(aCampos) == 8
		nDecimais	:= Val(&(aCampos[DECIMAL])) 	
	Endif
	
	IF Val(aCampos[TAMANHO]) == 1
		xValor	:= Val( Substr( cLine, Val(aCampos[POSINI]) , ( Val(aCampos[TAMANHO])  ) )  )	
	Else 	
		xValor	:= Val( Substr( cLine, Val(aCampos[POSINI]) , ( Val(aCampos[TAMANHO]) - nDecimais ) ) + "." + Right(xValor,nDecimais) )
	Endif  
	
Elseif aCampos[FORMATO] == "D"
	xValor	:= StoD(xValor) 

Elseif aCampos[FORMATO] == "H"
	xValor	:= Left(xValor,2) + ":" + Substr(xValor,3,2) + ":" + Right(xValor,2)

Endif

Return xValor




/*/{Protheus.doc} Copia2Lidos
Move arquivo para uma determinada pasta de arquivos lidos

@author Mauro Paladini
@since 18/02/2015
@version 1.0
/*/
User Function Copia2Lidos( cOrigem , cDestino )

Local nTry		:= 1 

While nTry <= NTRYARQ .Or. !File( cDestino )

	__CopyFile( UPPER(cOrigem) , UPPER(cDestino) )
	Sleep(100)
	nTry += 1
	
End

Return File( cDestino )





/*/{Protheus.doc} DeletaLido
Deleta arquivo lido depois de copiado para pasta de backup

@author Mauro Paladini
@since 18/02/2015
@version 1.0
/*/
User Function DeletaLido( cOrigem )

Local nTry		:= 1 

While nTry <= NTRYARQ .Or. File(cOrigem) 				 				

	Ferase(cOrigem)	
	Sleep(100)
	nTry += 1					

End
		 			
Return File(cOrigem)

