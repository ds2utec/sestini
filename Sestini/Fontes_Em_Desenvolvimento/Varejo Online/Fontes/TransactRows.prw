#include "protheus.CH"
#include "WSMAT110.CH"
#INCLUDE "APWEBSRV.CH"
#INCLUDE 'TBICONN.CH'


User Function TransactRows()
Return 

Class TransactRows FROM LongClassName
    
    DATA TipoNF               AS String    
	DATA FormProprio          AS String 
	DATA NroNotaFiscal        AS String 
	DATA SerieNotaFiscal      AS String
	DATA DataEmissao          AS Date     
	DATA DataDigitacao        AS Date
	DATA Especie              AS String   
	DATA CodFornecedor        AS String
	DATA LojaFornecedor       AS String
	DATA CodCliente           AS String
	DATA LojaCliente          AS String
	DATA CPFCliente           AS String
	DATA CGCFornecedor        AS String
	DATA NroNotaEletronica    AS String
	DATA ValorMercadorias     AS Float 
	DATA ValorBrutoNF         AS Float 
	DATA ChaveNFE             AS String
	DATA ProtocoloNFE         AS String
	DATA GeraTitulos          AS String
	DATA TabelaIntegracao     AS String
	DATA CampoFlagProcessado  AS String
	DATA IDRegistro           AS Float
	
	DATA NumPedido            AS String
	DATA DataPrevista         AS Date
	DATA Observacoes          AS String
	DATA CondicaoPagto        AS String
	DATA ContatoPedido        AS String
	DATA Moeda                AS integer
	DATA TaxaMoeda            AS Float
	DATA NomeSolicitante      AS String
	DATA TipoFrete            AS String
	
	DATA AliqICMS             AS Float
	DATA AliqIPI              AS Float
	DATA AliqPIS              AS Float
	DATA AliqCOF              AS Float
	DATA AliqCSLL             AS Float
	
	DATA BaseICMS             AS Float
	DATA BaseIPI              AS Float
	DATA BasePIS              AS Float
	DATA BaseCOF              AS Float
	DATA BaseCSLL             AS Float
	
	DATA ValorICMS            AS Float
	DATA ValorIPI             AS Float
	DATA ValorPIS             AS Float
	DATA ValorCOF             AS Float
	DATA ValorCSLL            AS Float    
	
	DATA TotalICMS            AS Float
	DATA TotalIPI             AS Float
	DATA TotalPIS             AS Float
	DATA TotalCOF             AS Float
	DATA TotalCSLL            AS Float    
	
	Data aHeader            	AS Array
	Data aCols              	AS Array

	Data aNoCposCab			As Array
	Data aNoCposGrid		As Array
	Data aNoCposFin			As Array
	
	Data aCposCab			As Array
	Data aCposDet			As Array
	Data aCposFin			As Array	
	Data aColsCab			As Array
	Data aColsGrid			As Array
	Data aColsFin			As Array
	
	Data cFil
	Data cSituac
	Data cStatus
	Data cSeq
	Data dData
	Data cHora
	Data cUsuari
	Data cOrigem
	Data cChave
	Data cOperac
	Data cKeyori
	Data cTabori
	Data nIndOri
	Data cScript
	Data cResult
	Data cFunopc
	Data cXMLdat
	Data DDtrun
	Data cHrrun
	Data nQtdrun
	Data cBlqpro
	Data cCateg
	Data cPcori
	Data cPline
	Data cIntver
	Data lRet
	Data cMessage   		As String
	Data cIdSeq
	Data cX2Tab	
	Data Token				As String

	
	Method New(cOrigem) CONSTRUCTOR    
	Method SetVariables()   
	Method SetScript()
	Method PrepareAuto()
	Method AppendRow()
	Method SaveSource()  
	Method SetDoneByID()                
	Method Destroy()
	
  
EndClass



/*/{Protheus.doc} New
Contructor

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method New(cOrigem)  Class TransactRows
    
::TipoNF               := ''    
::FormProprio          := '' 
::NroNotaFiscal        := '' 
::SerieNotaFiscal      := ''
::DataEmissao          := CTOD("//")
::DataDigitacao        := Date()
::Especie              := ''   
::CodFornecedor        := ''
::LojaFornecedor       := ''
::CodCliente           := ''
::LojaCliente          := ''
::CPFCliente           := ''
::CGCFornecedor        := ''
::NroNotaEletronica    := ''
::ValorMercadorias     := 0
::ValorBrutoNF         := 0 
::ChaveNFE             := ''
::ProtocoloNFE         := ''
::GeraTitulos          := ''
::TabelaIntegracao     := ''
::CampoFlagProcessado  := ''
::IDRegistro           := 0

::NumPedido            := ''
::DataPrevista         := CTOD("//")
::Observacoes          := ''
::CondicaoPagto        := ''
::ContatoPedido        := ''
::Moeda                := 1
::TaxaMoeda            := 1
::NomeSolicitante      := ''
::TipoFrete            := ''
    
::AliqICMS             := 0
::AliqIPI              := 0
::AliqPIS              := 0
::AliqCOF              := 0
::AliqCSLL             := 0

::BaseICMS             := 0
::BaseIPI              := 0
::BasePIS              := 0
::BaseCOF              := 0
::BaseCSLL             := 0
    
::ValorICMS            := 0
::ValorIPI             := 0
::ValorPIS             := 0
::ValorCOF             := 0
::ValorCSLL            := 0    
    
::TotalICMS            := 0
::TotalIPI             := 0
::TotalPIS             := 0
::TotalCOF             := 0
::TotalCSLL            := 0  

::aColsCab				:= {}
::aColsGrid				:= {}
::aColsFin				:= {}

::aNoCposCab			:= {}
::aNoCposGrid			:= {}
::aNoCposFin			:= {}

::aCposCab				:= {}
::aCposDet				:= {}
::aCposFin				:= {}


// -- Variaveis utilizadas para a grava��o dos dadps na tabela PV1 e PV2  ( intermedi�rias )
::aCols                 := {}
::aHeader               := {}

// -- Dados passados via Parametro    
::cOrigem   :=  cOrigem
::cChave    := ''
::cOperac   := ''
::cKeyori   := ''
::cTabori   := ''
::nIndOri   := 1
::cBlqpro   := .F.

// -- Log automatico
::cSituac   := '1'  // 1=Pendente
::cStatus   := ''   // 1=Sucesso 2=Falha
::cSeq      := ''
::dData     := Date()
::cHora     := Time()
::cUsuari   := cUserName
::cScript   := ''
::cResult   := ''
::cFunopc   := ''
::cXMLdat   := ''
::DDtrun    := CTOD('')
::cCateg    := ''

// -- Depura��o de execu��o 
::cPcori    := ''
::cPline    := ''
::cIntver   := ''
::lRet      := .F.
::cMessage  := ''
::Token		:= ''
           
Return Self
                    

/*/{Protheus.doc} SetVariables
Define os parametros da transacao que sera replicado

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method SetVariables( cOrigem , cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBloqProx , cCateg , cToken ) Class TransactRows

Default cChave      := ''
Default cOperacao   := ''
Default cKeyOrig    := ''
Default cTabOrig    := ''
Default nIndOrig    := 1
Default cBloqProx   := '1'
Default cOrigem     := ''
Default cCateg		:= ''
Default cToken		:= ''

If !Empty( cOrigem )
    ::cOrigem   := cOrigem
Endif

::cChave    := cChave
::cOperac   := cOperacao
::cKeyori   := cKeyOrig
::cTabori   := cTabOrig
::nIndOri   := nIndOrig
::cBlqpro   := cBloqProx
::cCateg	:= cCateg
::Token		:= cToken
        
Return .T.




/*/{Protheus.doc} SeTransactRows
Define a query SQL que devera ser executada no momento da integra��o

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method SetScript( cQuery ) Class TransactRows

::cScript   := cQuery
        
Return  .T.




/*/{Protheus.doc} LoadModel
instancia um modelo com a estrutura de campos  das tabelas.
Com base na estrutura alimenta o aCols e aHeader corretamente
@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method PrepareAuto( cFunopc , cIdSeq ) Class TransactRows

Local aArea		:= GetArea()
Local cTrb		:= GetNextAlias()

Local aCposCab	:= {}
Local aCposDet	:= {}
Local aCposFin	:= {}


 // -- Estrutura de registros financeiros
 
aAdd ( aCposFin , { "PV3_CARTEI" 	, "E2_CARTEI" 		} )
aAdd ( aCposFin , { "PV3_EMISSA" 	, "E2_EMISSAO" 		} )
aAdd ( aCposFin , { "PV3_DOC" 		, "E2_DOC" 			} )
aAdd ( aCposFin , { "PV3_SERNF" 	, "E2_SERIE" 	  	} )
aAdd ( aCposFin , { "PV3_TIPO"		, "E2_TIPO" 		} )
aAdd ( aCposFin , { "PV3_PARCEL" 	, "E2_PARCELA" 	 	} )
aAdd ( aCposFin , { "PV3_VALOR"		, "E2_VALOR" 		} )
aAdd ( aCposFin , { "PV3_VENC" 		, "E2_VENCTO" 		} )


// -- Estrutura de registros Cabecalho e Itens
 
BEGINSQL Alias cTrb

SELECT 	RTrim(PV5_FIELD) 	AS PV5_FIELD, 
	   	RTrim(PV5_QFIELD) 	AS PV5_QFIELD,
	   	PV5_X2TAB
	   
FROM %Table:PV5% PV5

WHERE	PV5_FILIAL		= %xFilial:PV5%  	AND
		PV5_FUNOPC		= %Exp:cFunopc% 	AND
		PV5_SEQ			= %Exp:cIdSeq%  	AND
		PV5_QFIELD		<> ''				AND
		PV5.D_E_L_E_T_ = ''
		ORDER BY  PV5_FILIAL, PV5_X2TAB

ENDSQL

// --  Alimenta os array com as configura��es de campos
(cTrb)->( DbGoTop() )
While (cTrb)->( !Eof() )

	IF (cTrb)->PV5_X2TAB $ 'PV1|PV4'
		aAdd( aCposCab , { (cTrb)->PV5_FIELD , (cTrb)->PV5_QFIELD } ) 		
	Else
		aAdd( aCposDet , { (cTrb)->PV5_FIELD , (cTrb)->PV5_QFIELD } )
	Endif
	(cTrb)->( DbSkip() )

End
(cTrb)->( DbCloseArea() )

::aCposCab 	:= aClone(aCposCab)  
::aCposDet	:= aClone(aCposDet)
::aCposFin	:= aClone(aCposFin)

RestArea( aArea)
        
Return  .T.


/*/{Protheus.doc} AppendRow
Insere o registro na tabela PV9 para ser replicado posteriormente via JOB

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/
Method AppendRow()  Class TransactRows

Local cProcOri  := ProcName(1)
Local CProcLine := Str(ProcLine(1),7)
Local aPrograms := IIF( FindFunction( "GetPrograms" ) , GetPrograms() , {} ) 

/*
SITUACOES POSSIVEIS

1=Pendente Envio; 
2=Executando Agora; 
3=Executado ; 
4=Pendente Solu��o;
5=Pendente Reenvio  */

Begin Transaction

RecLock("PV9",.T.)

PV9->PV9_FILIAL  := xFilial("PV9")
PV9->PV9_FILORI  := SM0->M0_CODFIL
PV9->PV9_SITUAC  := '1'                 
PV9->PV9_STATUS  := ''
PV9->PV9_SEQ     := StrZero( PV9->( Recno() ) , 20 )
PV9->PV9_DATA    := ::dData
PV9->PV9_HORA    := ::cHora
PV9->PV9_USUARI  := ::cUsuari
PV9->PV9_ORIGEM  := ::cOrigem
PV9->PV9_CHAVE   := ::cChave
PV9->PV9_OPERAC  := ::cOperac
PV9->PV9_KEYORI  := ::cKeyori
PV9->PV9_TABORI  := ::cTabori
PV9->PV9_INDORI  := ::nIndOri
PV9->PV9_SCRIPT  := ::cScript
PV9->PV9_FUNOPC  := ::cFunopc
PV9->PV9_XMLDAT  := ::cXMLdat
PV9->PV9_BLQPRO  := ::cBlqpro
PV9->PV9_CATEG   := ::cCateg
PV9->PV9_PCORI   := cProcOri   
PV9->PV9_PLINE   := CProcLine
PV9->PV9_IDREG   := ::IDRegistro
PV9->PV9_TKVAR 	 := ::Token
PV9->PV9_INTVER  := '99/99/99 00:00:00'

PV9->( MsUnLock() )

End Transaction 

::cSeq		:= PV9->PV9_SEQ

If ( !  Left( ::GeraTitulos , 1 ) $ 'SN' .Or.( ! Left(::FormProprio,1) $ 'SN' ) ) .And.  Rtrim(Upper(::cOrigem)) $ 'MATA410|MATA103' 
	::lRet      := .F.
	::cMessage  := 'Informa��es de origem poss�i layout inconsistente. Revise estrutura de campos com documento MIT044 enviado perla DS2U'	
	Return .F.
Endif

::lRet      := .T.
::cMessage  := 'Sucesso'

Return ::lRet 



/*/{Protheus.doc} SaveSource
Realiza a grava��o dos registros lidos da base intermedi�ria para serem replicados em fila
Este registro ser� processado atrav�s do registro criado na PV9 ( fila de processamento )

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method SaveSource()  Class TransactRows

Local nA        	:= 0
Local nC			:= 0
LOcal nLin	     	:= 0
Local nPosCpo		:= 0
Local cProcOri  	:= ProcName(1)
Local CProcLine 	:= Str(ProcLine(1),7)
Local aPrograms 	:= IIF( FindFunction( "GetPrograms" ) , GetPrograms() , {} )

Local cAliasCab		:= ''
Local cAliasGrid	:= '' 
Local cAliasFin		:= ''

IF Len( ::aCposCab ) > 0 
	cAliasCab	:= LEFT( ::aCposCab[1][1] , 3 )
Endif

Begin Transaction

// -- Re1gistro Lido no processo de integra��o (Cabe�alho)
// -- Indice 01 - PV1_FILIAL+PV1_ORIGEM+PV1_SERNF+PV1_DOC+PV1_TIPONF+PV1_CGCFOR+PV1_CPFCLI

IF cAliasCab == 'PV1'

	RecLock("PV1",.T.)
	PV1->PV1_FILIAL := xFilial("PV1")
	PV1->PV1_SEQ    := ::cSeq
	PV1->PV1_FILORI := SM0->M0_CODFIL
	PV1->PV1_ORIGEM := ::cOrigem                /* Adapter */ 
	PV1->PV1_DOC    := ::NroNotaFiscal
	PV1->PV1_PEDIDO	:= ::NumPedido
	PV1->PV1_NUMPED	:= ::NumPedido

	If Type("::DataDigitacao") <> "U"
		PV1->PV1_DTDIGI := ::DataDigitacao
	Endif

	PV1->PV1_EMISSA := ::DataEmissao
	PV1->PV1_SERNF  := ::SerieNotaFiscal
	
	PV1->PV1_CGCFOR := ::CGCFornecedor 
	PV1->PV1_FORNEC := ::CodFornecedor 
	PV1->PV1_LJFOR 	:= ::LojaFornecedor 	

	PV1->PV1_CPFCLI := ::CPFCliente
	PV1->PV1_CLIENT := ::CodCliente 
	PV1->PV1_LJCLI 	:= ::LojaCliente 		
	
	PV1->PV1_VMERC  := ::ValorMercadorias
	PV1->PV1_VBRUT  := ::ValorBrutoNF
	PV1->PV1_TIPONF := ::TipoNF
	PV1->PV1_NFELET := ::NroNotaEletronica
	PV1->PV1_CHVNFE := ::ChaveNFE
	PV1->PV1_PNFE   := ::ProtocoloNFE
	PV1->PV1_TITULO := ::GeraTitulos
	PV1->PV1_TABINT := ::TabelaIntegracao
	PV1->PV1_FLAG   := ::CampoFlagProcessado
	PV1->PV1_IDREG  := ::IDRegistro
	PV1->PV1_ESPECI := ::Especie
	PV1->PV1_VLFRET := 0
	PV1->PV1_VLDESP := 0
	PV1->PV1_VICM   := 0
	PV1->PV1_VLIPI  := 0
	PV1->PV1_DESCON := 0
	PV1->PV1_PESOLI := 0
	PV1->PV1_VLFOB  := 0
	PV1->PV1_VLSEGU := 0
	PV1->PV1_VLCIF  := 0
	PV1->PV1_PLIQUI := 0
	PV1->PV1_PBRUTO := 0
	
	PV1->( MsUnLock() ) 

Endif

// -- Grava tabelas de cabecalho 

If Len( ::aColsCab )> 0 .And. Len( ::aCposCab ) > 0 

	// -- Complementa eventuais campos na grava��o da PV1
    
    IF cAliasCab == 'PV1'

		RecLock("PV1",.F.)    
	    For nA  := 1 To Len( ::aColsCab )
	    	
			For nC := 1 To Len(::aCposCab)	
				If ! Empty( ::aCposCab[nC][2] )				
					xCpoValue	:= ::aColsCab[nC]										
					IF !Empty(xCpoValue) .And. PV1->( FieldPos( AllTrim( ::aCposCab[nC][1] ) ) ) > 0   .And.  Empty( PV1->( FieldGet( FieldPos( AllTrim(::aCposCab[nC][1]) ) )  ) )
						IF ( Ascan( ::aNoCposCab , Alltrim( ::aCposCab[nC][1] ) ) == 0 )   
							PV1->( FieldPut( FieldPos( Alltrim( ::aCposCab[nC][1] ) ) , xCpoValue ) )						
						Endif 
					Endif 
				Endif
			Next nC
	
	    	/*
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_CLIENT' 	) , Left(RTrim(::CPFCliente),8) 	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_FORNEC'  	) , Left(RTrim(::CGCFornecedor),8)	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_LJCLI' 	) , Right(RTrim(::CPFCliente),4) 	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_LJFOR'  	) , Right(RTrim(::CGCFornecedor),4)	) )
	    	*/
	    		
	    Next nA	    
	    MsUnlock()
    
    Else
    
		RecLock( cAliasCab  ,.T.)
    
	    For nA  := 1 To Len( ::aColsCab )
	    
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_ORIGEM' 	) , ::cOrigem 		) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_SEQ'   	) , ::cSeq 			) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_FILORI'  	) , SM0->M0_CODFIL 	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_CPFCLI' 	) , ::CPFCliente 	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_CGCFOR'  	) , ::CGCFornecedor	) )
	    
			For nC := 1 To Len(::aCposCab)
	
				If ! Empty( ::aCposCab[nC][2] )
				
					xCpoValue	:= ::aColsCab[nC]										
					IF !Empty(xCpoValue) .And. (cAliasCab)->( FieldPos( AllTrim( ::aCposCab[nC][1] ) ) ) > 0 
						(cAliasCab)->( FieldPut( FieldPos( Alltrim( ::aCposCab[nC][1] ) ) , xCpoValue ) ) 
					Endif 
	
				Endif
	
			Next nC		       

	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_CLIENT' 	) 	, u_CliForAuto( ::CPFCliente 		, 1 )[1] 	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_FORNEC'  	) 	, u_CliForAuto( ::CGCFornecedor 	, 2 )[1]	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_LJCLI' 	) 	, u_CliForAuto( ::CPFCliente 		, 1 )[2]	) )
	    	(cAliasCab)->( FieldPut( FieldPos( cAliasCab + '_LJFOR'  	) 	, u_CliForAuto( ::CGCFornecedor 	, 2 )[2]	) )

	
	    Next nA
	    
	    (cAliasCab)->( MsUnLock() )
	    
    Endif
            
Endif


// -- Registro Lido no processo de integra��o (Itens)
// -- Indice 01 - PV2_FILIAL+PV2_ORIGEM+PV2_SERIE+PV2_DOC+PV2_TIPO+PV2_CGCFOR+PV2_CPFCLI

If Len( ::aColsGrid )> 0 .And. Len( ::aCposDet ) > 0 

	cAliasGrid	:= LEFT( ::aCposDet[1][1] , 3 )	

    For nA  := 1 To Len( ::aColsGrid )
    
        RecLock(cAliasGrid,.T.)

    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_ORIGEM' 		) , ::cOrigem 		) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_SEQ'   		) , ::cSeq 			) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_FILORI'  	) , SM0->M0_CODFIL 	) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_CPFCLI' 		) , ::CPFCliente 	) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_CGCFOR'  	) , ::CGCFornecedor	) )

		For nC := 1 To Len(::aCposDet)
		
			If ! Empty( ::aCposDet[nC][2] )
			
				xCpoValue	:= ::aColsGrid[nA][nC]
													
				IF !Empty(xCpoValue) .And. (cAliasGrid)->( FieldPos( AllTrim( ::aCposDet[nC][1] ) ) ) > 0 
					(cAliasGrid)->( FieldPut( FieldPos( Alltrim( ::aCposDet[nC][1] ) ) , xCpoValue ) ) 
				Endif 

			Endif

		Next nC

    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_CLIENT' 	) 	, u_CliForAuto( ::CPFCliente 	, 1  )[1] 	) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_FORNEC'  ) 	, u_CliForAuto( ::CGCFornecedor	, 2  )[1]	) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_LJCLI' 	) 	, u_CliForAuto( ::CPFCliente		, 1  )[2]	) )
    	(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_LJFOR'  	) 	, u_CliForAuto( ::CGCFornecedor	, 2  )[2]	) )

		(cAliasGrid)->( FieldPut( FieldPos( cAliasGrid + '_ITEM' 	) 	, StrZero(nA,4) 	) )

        (cAliasGrid)->( MsUnLock() )

    Next nA
            
Endif

If ::TipoNF	== "N"

	If Len( ::aColsFin )> 0 .And. Len( ::aCposFin ) > 0 
	
		cAliasFin	:= LEFT( ::aCposFin[1][1] , 3 )	
	
	    For nA  := 1 To Len( ::aColsFin )
	    
	        RecLock(cAliasFin,.T.)
	        
	    	(cAliasFin)->( FieldPut( FieldPos( cAliasFin + '_SEQ'   		) , ::cSeq 			) )
	    	(cAliasFin)->( FieldPut( FieldPos( cAliasFin + '_FILORI'  		) , SM0->M0_CODFIL 	) )
	
			For nC := 1 To Len(::aCposFin)
			
				If ! Empty( ::aCposFin[nC][2] )
				
					xCpoValue	:= ::aColsFin[nA][nC]
														
					IF !Empty(xCpoValue) .And. (cAliasFin)->( FieldPos( AllTrim( ::aCposFin[nC][1] ) ) ) > 0 
						(cAliasFin)->( FieldPut( FieldPos( Alltrim( ::aCposFin[nC][1] ) ) , xCpoValue ) ) 
					Endif 
	
				Endif
	
			Next nC
	
	        (cAliasFin)->( MsUnLock() )
	
	    Next nA
	            
	Endif

Endif

End Transaction

// -- Registro Lido no processo de integra��o (Cadastros)
/*
RecLock("PV4",.T.)    
MsUnLock()
*/

::lRet      := .T.
::cMessage  := 'Sucesso'

Return ::lRet 





/*/{Protheus.doc} SetDoneByID
Realiza update e marca registro como processado nas tabelas origem da integra��o

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method SetDoneByID( cIdOrigem , oConnect ) Class TransactRows

Local cQuery	:= ''
Local cTabAux	:= ''
Local aStruct	:= {}
Local nF		:= 0
Local cFIdName	:= ''
Local cSQL		:= ''
Local lSQL		:= .T.
Local cLog		:= ''
Local lRet		:= .T.

Default cIdOrigem	:= ''
Default oConnect	:= Nil

// -- Protege Posicionamento para o registro da PV9

If ! Empty(::cSeq) .And. PV9->PV9_SEQ <> ::cSeq
    PV9->( DbGoTo( Val( ::cSeq ) ) )
Endif


// -- Faz update no SQL Server da integra��o via objeto FWDBACcESS

IF ( oConnect:HasConnection() .or. oConnect:OpenConnection() )

	If ! Empty(::TabelaIntegracao)  .And. ! Empty(::CampoFlagProcessado) .And. ! Empty(::IDRegistro)             
	

	    // -- Consiste campo ID Case Sensitive	    

		cQuery	:= "SELECT	COLUMN_NAME "
		cQuery	+= "FROM	INFORMATION_SCHEMA.COLUMNS "
		cQuery	+= "WHERE	TABLE_NAME = '" + AllTrim(::TabelaIntegracao) + "' AND "
		cQuery	+= "		COLUMN_NAME IN ( 'ID' , 'id' , 'Id' ) "
        
        cTabAux := GetNextAlias()        
        oConnect:NewAlias( cQuery , cTabAux )
        
        (cTabAux)->( DbGoTop() )        
        If (cTabAux)->( !Eof() )
        	cFIdName	:= (cTabAux)->COLUMN_NAME
        Endif 
        (cTabAux)->( DbCloseArea() )
        
	    
		// -- Executa UPDATE 

	    cSQL := 'UPDATE ' 	+ AllTrim(::TabelaIntegracao) 	+ ' SET ' 	+ AllTrim(::CampoFlagProcessado) + ' = 1 '
	    cSQL += 'WHERE ' 	+ Alltrim(cFIdName) 			+ ' = ' 	+ AllTrim( cValToChar(::IDRegistro) ) 
	    cLog := CHR(13) + CHR(10) + Alltrim(cFIdName) + ' ' + AllTrim(cValToChar(::IDRegistro)) + ' em ' + AllTrim(::TabelaIntegracao) + ' -> ' + AllTrim(::CampoFlagProcessado) + ' [OK]'
	        
		oConnect:ClearError()
		lSQL := oConnect:SQLExec( cSQL )
	    
        RecLock('PV9',.F.)
        If !lSQL
        	PV9->PV9_RESULT	+= chr(13) + chr(10) + AllTrim( oConnect:ErrorMessage() )
        	lRet	:= .F.
        Else	       
        	PV9->PV9_RESULT += cLog  
	    Endif            
        PV9->( MsUnLock() )

	Else
	
		cLog	:= chr(13) + chr(10) + 'Falha de conex�o ap temtar acessar SQL ca integra��o' + _EOL + AllTrim( oConnect:ErrorMessage() )
		
		// -- N�o foi possivel estabelecer conex�o com o TOP CONNECT
		RecLock('PV9',.F.)
		PV9->PV9_RESULT +=  cLog
		PV9->( MsUnLock() )

		lRet	:= .F.
	
	Endif

Else

	cLog	:= chr(13) + chr(10) + 'Falha de conex�o ao atualizar campo ' + AllTrim(::CampoFlagProcessado) + ' em ' + AllTrim(::TabelaIntegracao) + _EOL + AllTrim( oConnect:ErrorMessage() )
	
	// -- N�o foi possivel estabelecer conex�o com o TOP CONNECT
	RecLock('PV9',.F.)
	PV9->PV9_RESULT +=  cLog
	PV9->( MsUnLock() )

Endif

Return lRet


/*/{Protheus.doc} Destroy
Elimina objeto e libera variaveis

@type       class
@author     Mauro Paladini
@since      01/10/2018
@version    1.0
/*/

Method Destroy() Class TransactRows

::TipoNF               := ''    
::FormProprio          := '' 
::NroNotaFiscal        := '' 
::SerieNotaFiscal      := ''
::DataEmissao          := CTOD("//")
::DataDigitacao        := Date()
::Especie              := ''   
::CodFornecedor        := ''
::LojaFornecedor       := ''
::CodCliente           := ''
::LojaCliente          := ''
::CPFCliente           := ''
::CGCFornecedor        := ''
::NroNotaEletronica    := ''
::ValorMercadorias     := 0
::ValorBrutoNF         := 0 
::ChaveNFE             := ''
::ProtocoloNFE         := ''
::GeraTitulos          := ''
::TabelaIntegracao     := ''
::CampoFlagProcessado  := ''
::IDRegistro           := 0

::NumPedido            := ''
::DataPrevista         := CTOD("//")
::Observacoes          := ''
::CondicaoPagto        := ''
::ContatoPedido        := ''
::Moeda                := 1
::TaxaMoeda            := 1
::NomeSolicitante      := ''
::TipoFrete            := ''
    
::AliqICMS             := 0
::AliqIPI              := 0
::AliqPIS              := 0
::AliqCOF              := 0
::AliqCSLL             := 0

::BaseICMS             := 0
::BaseIPI              := 0
::BasePIS              := 0
::BaseCOF              := 0
::BaseCSLL             := 0
    
::ValorICMS            := 0
::ValorIPI             := 0
::ValorPIS             := 0
::ValorCOF             := 0
::ValorCSLL            := 0    
    
::TotalICMS            := 0
::TotalIPI             := 0
::TotalPIS             := 0
::TotalCOF             := 0
::TotalCSLL            := 0  

::aColsCab				:= {}
::aCposDet				:= {}
::aCols					:= {}
::aHeader				:= {}
::aCposFin				:= {}
::aColsFin				:= {}

::aNoCposCab			:= {}
::aNoCposGrid			:= {}
::aNoCposFin			:= {}

::cOrigem   			:= ''
::cChave    			:= ''
::cOperac   			:= ''
::cKeyori   			:= ''
::cTabori   			:= ''
::nIndOri   			:= 1
::cBlqpro   			:= .F.

// -- Log automatico
::cSituac   			:= '1'  // 1=Pendente
::cStatus   			:= ''   // 1=Sucesso 2=Falha
::cSeq     				:= ''
::dData     			:= Date()
::cHora     			:= ''
::cUsuari   			:= ''
::cScript   			:= ''
::cResult   			:= ''
::cFunopc   			:= ''
::cXMLdat   			:= ''
::DDtrun    			:= CTOD('')
::cCateg    			:= ''

// -- Depura��o de execu��o 
::cPcori    			:= ''
::cPline    			:= ''
::cIntver  				:= ''
::lRet      			:= .F.
::cMessage  			:= ''
::Token					:= ''
           
Return Nil 



/*/{Protheus.doc} CliForAuto
Tratamento customizado para numero de documento CNPJ ou CPF

@type 	User Function
@author Mauro Paladini
@since 	Jan 21, 2019  1:41:42 PM

/*/

User Function CliForAuto( XCGC , nCliFor )

Local   aAreas          := { SA1->(GetArea()) , SA2->(GetArea()) , GetArea() }
Local 	aRet			:= { '' , '' , '' } 	
Local   cCLIFOR			:= GetNextAlias()

Default nCliFor		:= 0
Default XCGC		:= ''

IF nCliFor == 1	

	BEGINSQL Alias cCLIFOR
		SELECT A1_COD, A1_LOJA, A1_NATUREZ
		FROM %table:SA1% SA1
		WHERE A1_FILIAL = %xFilial:SA1% AND A1_CGC = %Exp:Alltrim(xCGC)% AND D_E_L_E_T_ = ''
	ENDSQL
	
	(cCLIFOR)->( DbGoTop() )
	IF (cCLIFOR)->( !EOF() ) 
		aRet[1]     := (cCLIFOR)->A1_COD
	    aRet[2] 	:= (cCLIFOR)->A1_LOJA
	    aRet[3]		:= (cCLIFOR)->A1_NATUREZ
	Endif
	(cCLIFOR)->( DbCloseArea() )

Else

	BEGINSQL Alias cCLIFOR
		SELECT A2_COD, A2_LOJA, A2_NATUREZ
		FROM %table:SA2% SA2
		WHERE A2_FILIAL = %xFilial:SA2% AND A2_CGC = %Exp:Alltrim(xCGC)% AND D_E_L_E_T_ = ''
	ENDSQL
	
	(cCLIFOR)->( DbGoTop() )
	IF (cCLIFOR)->( !EOF() ) 
		aRet[1]     := (cCLIFOR)->A2_COD
	    aRet[2] 	:= (cCLIFOR)->A2_LOJA
		aRet[3]		:= (cCLIFOR)->A2_NATUREZ
	Endif
	(cCLIFOR)->( DbCloseArea() )

Endif

AEval(aAreas, {|x| RestArea(x)})
		
Return aRet

