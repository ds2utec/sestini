#Include 'Protheus.ch'
#include 'tbiconn.ch'
#include 'tryexception.ch'


/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnlauncher_integration.log' 
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'


/*/{Protheus.doc} STNLAUNCHER
Processa a fila de transa��es pendentes da PV9

@type 		User Function
@author 	Mauro Paladini
@since 		Aug 17, 2018  2:33:31 PM
@version 	1.0
@return 	${return}, ${return_description}

/*/

User Function STNLAUNCHER( CRPCEmp, cRPCFil )

Local aArea         := GetArea()
Local aTCLink       := { {0,0} }
Local lCriouAmb     := .F.
Local oConnect      := Nil

Private cJobLog     := cDirLog + cCFGLog 
Private cTagLog		:= '[STNLAUNCHER ' + SM0->M0_CODFIL + ' ' + RTRIM(SM0->M0_FILIAL)  +  '] '

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
    RPCSetType(3) 
    RPCSETENV(cRPCEmp, cRPCFil)
    u_WriteLog( cJobLog  , '[PREP-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil ,.T.,.T. )
    lCriouAmb   := .T.
Endif 

u_WriteLog( cJobLog  , cTagLog + 'Inicio ..' ,.T.,.T. )


// -- Valida conexao com DBAccess
u_WriteLog( cJobLog  , '[FWDBACCESS] Realiza conex�o com DbAccess ',.T.,.T. )


// -- Verifica conexao com DBaccess da Integra��o
If ! U_ODBCConnect( @oConnect )

    If lCriouAmb
        u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        RESET ENVIRONMENT
    Endif        
    Return

Endif


// -- Processa movimentos por Filial

If IsBlind()

    u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )
    
    cCNPJ   := RTrim(SM0->M0_CGC)

    STNAux( @oConnect , cCNPJ )
                        
Else

	u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )

    cCNPJ   := RTrim(SM0->M0_CGC)

    FWMsgRun( , {|| STNAux( @oConnect , cCNPJ ) } ,, "Processando .. "  )

Endif


// -- Finaliza conex�es realizadas

oConnect:CloseConnection()
oConnect:Finish()
FreeObj(oConnect)

u_WriteLog( cJobLog  , '[STNLAUNCHER] T�rmino' ,.T.,.T. )

If lCriouAmb
    u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil ,.T.,.T. )
    RESET ENVIRONMENT
Endif

Return



/*/{Protheus.doc} STNAux
Funcao Auxiliar de processamento

@type 	User Function
@author Mauro Paladini
@since 	Mar 6, 2019  11:42:12 PM

/*/

Static Function STNAux( oConnect , cCNPJ )

Local cSQL          := ''
Local lSQL          := .T.
Local lBlqSequen    := .F.
Local cAgrupador    := ''
Local lContinue		:= .T.
Local lMaGerNF		:= .T.
Local cUMessage		:= ''
Local aResult		:= {}
Local cAllResult	:= ''
Local bError 		:= ErrorBlock( { |oError| MyError( oError ) } )

Private cAutoError		:= ''
Private oFWExecAuto     := Nil


// -- Conex�o realizada com sucesso ( Tudo deve ser feito dentro do Objeto oConnect )

lContinue := PV9->( DbSeek( xFilial('PV9') + '1' ) )

While  PV9->( !Eof() .And. lContinue  )

	PtInternal(1, "[Running Varejo] ID " +  PV9->PV9_SEQ + ' ' + PV9->PV9_ORIGEM + 'Running ...')        

    // Caso 1=Sim  deve bloquear as proximas atualizacoes do mesmo item caso  
    // Este item apresente erro  ( exemplo:  Mesmo produto )
    
    nRec        := PV9->( Recno() )
    cProcSeq    := PV9->PV9_SEQ
    lBlqSequen  := PV9->PV9_BLQPRO == '1'     // 1=Sim
    cAgrupador  := PV9->PV9_CHAVE
    
    PV9->( DbSetOrder(4) )
    PV9->( DbGoTo( nRec ) )
           
    // -- ### SQL SERVER    
    If Empty( PV9->PV9_FUNOPC )
    
        // -- Executa a o Script SQL       
        cSQL := PV9->PV9_SCRIPT
            
        IF ( oConnect:HasConnection() .or. oConnect:OpenConnection() )
        
            oConnect:ClearError()
            lSQL := oConnect:SQLExec( cSQL )
       
       		RecLock('PV9', .F.)
       		                    
            IF !lSQL 
            
                PV9->PV9_RESULT := oConnect:ErrorMessage()
                PV9->PV9_STATUS := '2'
                PV9->PV9_SITUAC := '4'  
                                    
            Else
                
                PV9->PV9_STATUS := '1'
                PV9->PV9_SITUAC := '3'
                
            Endif
            
            MsUnLock()
            
            u_WriteLog( cJobLog  , '[PV9 ID ' + PV9->PV9_SEQ + '] ' + IIF( !lSQL , 'Falha ao executar' , 'Atualizado com sucesso' ) ,.T.,.T. )
            
        Endif
            
    Else

        // -- ### ADVPL | MSEXECAUTO ADAPTER 
        // -- Executa a fun��o auxiliar indicada no campo PV9_FUNOPC 
        
        oFWExecAuto		:= Nil
        cUMessage		:= ''
        lMaGerNF		:= .T.
        aResult			:= { .F. , '' }
        
        If ExistBlock( RTrim(PV9->PV9_FUNOPC) )
        
			PtInternal(1, "[Running Varejo] ID " +  PV9->PV9_SEQ + ' ' + PV9->PV9_ORIGEM + '[Running..]'  )    
          	
			bError := ErrorBlock( { |oError| MyError( oError ) } )

			BEGIN SEQUENCE

				// -- Atualiza o registro da fila de transa��es com o status do processamento
				// Considera 	Message 	- Resultado do processamento da MsExecAuto
				//				cAutoError	- ERRO FATAL
				//				cUMessage	- Reaultado do processamento da fun��o de usu�rio ( EXECBLOCK )

	          	aResult		:= ExecBlock( RTrim(PV9->PV9_FUNOPC) , .F. , .F. , { oFWExecAuto } ) 
	          	lResult 	:= aResult[1] 
	          	cUMessage	:= aResult[2]
	        	
				 If !oFWExecAuto:lResult .OR. !Empty( Alltrim( cAutoError ) )

					cAllResult	:= cUMessage + Chr(10) + Chr(13)
					cAllResult	+= cAutoError 	+ Chr(10) + Chr(13)
					
					DisarmTransaction()

					RecLock("PV9",.F.)
					PV9->PV9_RESULT := cAllResult
					PV9->PV9_STATUS := '2'
					PV9->PV9_SITUAC := '4'
					MsUnLock()  
					
				Else
					RecLock("PV9",.F.)
					PV9->PV9_RESULT := ''
					PV9->PV9_STATUS := '1'
					PV9->PV9_SITUAC := '3'
					MsUnLock()
				Endif          	  
								
				oFWExecAuto:Destroy()
				Freeobj(oFWExecAuto)
				oFWExecAuto := Nil

			RECOVER
								
				DisarmTransaction()

	        	RecLock("PV9",.F.)
	          	 
					PV9->PV9_RESULT := cAutoError
					PV9->PV9_STATUS := '2'
					PV9->PV9_SITUAC := '4'  					
				
				MsUnlock()
								
				If Type("oFWExecAuto") <> 'U'
					oFWExecAuto:Destroy()
					Freeobj(oFWExecAuto)
					oFWExecAuto := Nil
				Endif
	
			END SEQUENCE
			
			ErrorBlock( bError )
			
        Endif

    Endif
    
    PV9->( DbSetOrder(4) )
    PV9->( DbGoTo( nRec ) )
    
    // -- Atualiza��o do registro da PV9
    RecLock('PV9', .F.)
    
    PV9->PV9_DTRUN  := Date()
    PV9->PV9_HRRUN  := Time()
    PV9->PV9_QTDRUN += 1
    
    MsUnLock()

	PV9->( DbSkip() )
	    	    
    // -- Posiciona para persistir a fila dos registros pendentes 
    
    IF PV9->PV9_SITUAC <> '1'
		PV9->( DbSetOrder(1) )		
        lContinue := PV9->( DbSeek( xFilial('PV9') + '1' ) )
    Endif

End

Return




/*/{Protheus.doc} MyError
Tratamento de erros 

@type 	User Function
@author Mauro Paladini
@since 	Jan 21, 2019  9:27:03 PM

/*/

Static Function MyError( oError )

cAutoError	:= oError:Description + Chr(10) + Chr(13)
cAutoError	:= oError:Errorstack + Chr(10) + Chr(13)

BREAK

Return( NIL )



/*/{Protheus.doc} ODBCConnect
Realiza conexao ODBC com o TOP CONNECT da Integracao 

@type   Static Function
@author Mauro Paladini
@since  Aug 17, 2018  2:33:31 PM
@version 1.0
/*/

User Function ODBCConnect( oConnect )
   
Local cMessage      := ""
Local cIniFile      := GetAdv97()
Local cDBAlias      := RTrim( GetPvProfString('TScript',"TOPAlias","",cIniFile ) )
Local cDBTipo       := RTRIM( GetPvProfString('TScript',"TOPDataBase","",cIniFile ) )
Local cServer       := RTRIM( GetPvProfString('TScript',"TOPServer","",cIniFile ) )
Local nPort         := Val( GetPvProfString('TScript',"TOPPort","0",cIniFile ) )
Local cMsg          := ''
Local lRet          := .T.

Default oConnect    := Nil
 
IF Type("cJobLog") == "U"
    cDirLog := 'log-odbcconnect-' + AllTrim(ProcName(1)) + '.log'
Endif

/*
[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890
*/

__FunName := '[' + AllTrim(ProcName(1)) + ' line: ' + AllTRim(cValToChar(ProcLine(1))) + '] '                

// -- Valida as Configura��es no APPSERVER.INI

IF Empty(cDBAlias)
    u_WriteLog( cJobLog  , 'Chave TopAlias n�o configurada na se��o TScript do APPSERVER.INI'  , .T.,.T.,,,, .T. /*lFunName*/  )
    lRet := .F.
Endif

IF Empty(cDBTipo)
    u_WriteLog( cJobLog  , 'Chave TOPDataBase n�o configurada na se��o TScript do APPSERVER.INI'  , .T.,.T.,,,, .T. /*lFunName*/  )
    lRet := .F.
Endif

IF Empty(cServer)
    u_WriteLog( cJobLog  , 'Chave TOPServer n�o configurada na se��o TScript do APPSERVER.INI'  , .T.,.T.,,,, .T. /*lFunName*/  )
    lRet := .F.
Endif

IF Empty(nPort)
    u_WriteLog( cJobLog  , 'Chave TOPPort n�o configurada na se��o TScript do APPSERVER.INI'  , .T.,.T.,,,, .T. /*lFunName*/  )
    lRet := .F.
Endif


// -- Realiza a conex�o

oConnect := FWDBAccess():New( cDBTipo + '/' + cDBAlias  , cserver , nPort )
oConnect:SetConsoleError( .T. )

If !oConnect:OpenConnection()

    cMsg  := AllTrim( oConnect:ErrorMessage() )    
    u_WriteLog( cJobLog  , 'Falha ao conectar -> ' + cMsg  , .T.,.T.,,,, .T. /*lFunName*/  )

    oConnect:ClearError()
    oConnect:Destroy()
    FreeObj(oConnect)    
    lRet := .F.
    
Else

    u_WriteLog( cJobLog  , 'Conectado com sucesso' , .T.,.T.,,,, .T. /*lFunName*/  )
    
EndIf

Return lRet



/*/
SchedDef
Fun��o para compatibilizar o Scheduler

@author Mauro Paladini
@since 15/04/2015
@version 1.0
/*/
    
Static Function SchedDef()

Local aParam
Local aOrd := {} 

aParam := {;
            "P"                                     ,;  // Tipo: R para relatorio P para processo               
            "PARAMDEF"                                  ,;  // Pergunte do relatorio, caso nao use passar "PARAMDEF"
            ""                                      ,;  // Alias
            aOrd                                    ,;  // Array de ordens
          }   

Return aParam




