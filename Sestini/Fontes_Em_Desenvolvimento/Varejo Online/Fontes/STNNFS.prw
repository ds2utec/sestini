#Include 'Protheus.ch'
#include 'tbiconn.ch'


/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

						
Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnnfsfor_integration.log'
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'


/*/{Protheus.doc} STNNFS
Interface de integra��o Nota Fiscal de Saida

@type   User Function
@author Mauro Paladini
@since  Aug 17, 2018  2:33:31 PM
/*/

User Function STNNFS( CRPCEmp, cRPCFil )

    Local aArea         := GetArea()
    Local aTCLink       := { {0,0} }
    Local lCriouAmb     := .F.
    Local oConnect      := Nil

    Local cQuery        := ''
    Local cAlias        := ''
    Local cSQL          := ''
    Local lSQL          := .T.
    Local lBlqSequen    := .F.
    Local cAgrupador    := ''
    Local cCNPJ         := ''
    Local cCodPva       := "STNNFS"
    Local lRunning		:= .T.

	Private cTagLog		:= '[STNNFS]'
    Private cJobLog		:= cDirLog + cCFGLog

    MontaDir(cDirLog)

    IF cRPCEmp <> Nil .And. cRPCFil <> Nil
        RPCSetType(3)
        RPCSETENV(cRPCEmp, cRPCFil)
        u_WriteLog( cJobLog  , '[PREP-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        lCriouAmb   := .T.
    Endif

    // -- Valida conexao com DBAccess
    u_WriteLog( cJobLog  , '[FWDB000ACCESS] Iniciando conex�o com DbAccess ',.T.,.T. )

    // -- Verifica conexao com DBaccess da Integra��o
    If ! U_ODBCConnect( @oConnect )

        If lCriouAmb
            u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
            RESET ENVIRONMENT
        Endif
        Return

    Endif


	// -- Processa movimentos por Filial
    If IsBlind()
    
        cCNPJ   	:= RTrim(SM0->M0_CGC)

        u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )        
   
        ProcFilial( @oConnect , cCNPJ )
                           
 
    Else
    
        cCNPJ   := RTrim(SM0->M0_CGC)

		u_WriteLog( cJobLog , cTagLog + ' Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )
    
        FWMsgRun( , {|| ProcFilial( @oConnect , cCNPJ ) } ,, "Processando .. "  )
    
    Endif


    
    // -- Finaliza conex�es realizadas

    oConnect:CloseConnection()
    oConnect:Finish()
    FreeObj(oConnect)

    If lCriouAmb
        u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        RESET ENVIRONMENT
    Endif

Return





/*/{Protheus.doc} ProcFilial
Fun��o Auxiliar

@type       Function
@author     Mauro Paladini
@since      Aug 17, 2018  2:33:31 PM
@version    1.0
@return     ${return}, ${return_description}
/*/

Static Function ProcFilial( oConnect , cCNPJ )

    Local aAreas    		:= { PV1->(GetArea()) , PV2->(GetArea()) , PV9->(GetArea()) , GetArea() }
    Local cQuery    		:= ''
    Local cNFS      		:= ''   // Cabecalhos
    Local cITE      		:= ''   // Itens
    Local cQRY      		:= ''
    Local lRet      		:= .T.
    Local cSQL      		:= ''

    // -- Pedido
    Local cNotaFis  		:= ''
    Local cIDNota   		:= ''
    Local cItem     		:= '01'
    Local aProdutos 		:= {}

    Local cQryPath      	:= '\user_procedures\'
    Local cIDCliente    	:= ''
    Local oSestini      	:= Nil
    Local cChave        	:= ''

    // -- 1=Inclus�o;2=Altera��o;3=Exclus�o   
    
    Local cIdMapFld     	:= GetMv('ES_MNFS1'	,,'0000000001')		// -- C�digo do cadasstro 
	Local xST_CONDPG		:= GetMv('ES_STNCND',,'10')				// -- Condicao de pagamento Default
	Local xST_NATURE		:= GetMv('ES_SRNNAT',,'1.1.3001')		// -- Natureza Padrao
	Local xST_TESSAI		:= GetMv('ES_TESNFS',,'501')			// -- TES Defauls
	
	Local nPosCond			:= 0
	Local nPosNat			:= 0
	
    Local cViewSQL			:= 'VW_STNNFS_CAB.SQL'					// -- Nome do arquivo .SQ� referente a View dos registros    
    Local cAutoFunc			:= 'SAUTONFS'							// -- User Function registrada no Map Field PV5     
    Local cMapSeek			:= xFilial('PV5') + AvKey( 'U_' + cAutoFunc , 'PV5_FUNOPC' ) + cIdMapFld

    Local cKeyOrig      	:= ''
    Local cTabOrig      	:= 'SF2'
    Local nIndOrig      	:= 1
    Local nCpos         	:= 0
    Local aItens			:= {}
    Local aAux				:= {}
    Local aAuxItens     	:= {}

    Local aColsCab			:= {}
    Local aColsGrid			:= {}
    
    Local aCposCab			:= {} 
    Local aCposDet			:= {}

    // 1=Sim 2=Nao
    Local cBlqProx  := '1'

    oSestini  := Nil
    cOperacao := '1'
 
	If Empty ( cIdMapFld ) .OR. PV5->( ! DbSeek( cMapSeek ) )  
	    u_WriteLog( cJobLog  , '[' + cTagLog + '] ERRO Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek  ,.T.,.T. )
		u_WriteLog( cJobLog  , '[' + cTagLog + '] Processo cancelado'  ,.T.,.T. )
	    Return .F.
	Endif	

    // -- Conex�o realizada com sucesso ( Tudo deve ser feito dentro do Objeto oConnect )
    
    IF ( oConnect:HasConnection() .or. oConnect:OpenConnection() )
   
        // -- 001 QUERY

        aParam  := { { "#CNPJ#"     , cCNPJ } }
            
        cQuery := u_BIBuildQuery( cViewSQL )
        
		cQuery := u_DePara( aParam , cQuery )  
        cQRY   := GetNextAlias()
        
        oConnect:NewAlias( cQuery , cQRY )
       
        If oConnect:HasError()
            u_WriteLog( cJobLog  , oConnect:ErrorMessage()  , .T. , .T. ,,,, .T. /*lFunName*/  )
            Return
        EndIf
        
        IF (cQRY)->( !Eof() )
            u_WriteLog( cJobLog  ,  cTagLog + ' QUERY .. [OK] ' ,.T.,.T. )
        Else
            u_WriteLog( cJobLog  ,  cTagLog + ' QUERY .. [N�o h� dados] ' ,.T.,.T. )
        Endif
        
       
        // -- 002 Laco para inclusao dos clientes
              
        (cQRY)->( DbGoTop() )    
		
        While (cQRY)->( !Eof() )
                    
            // -- 1=Inclus�o;2=Altera��o;3=Exclus�o        

            oSestini    := Nil
            cOperacao   := '1'
        
            nIdOrig		:= (cQRY)->IDREGORIG
            cOrigem		:= 'MATA410'		// ##CFG
            cChave      := cFilAnt + ALLTRIM( (cQRY)->NOTAFISCAL ) + ALLTRIM( (cQRY)->CODCLIENTE )     // -- Chave agrupadora	// #CFG
            cKeyOrig    := AllTrim ( cValToChar((cQRY)->IDREGORIG ) )
            nIndOrig    := 1
          
            u_WriteLog( cJobLog  , cTagLog + ' Integrando ' + ALLTRIM((cQRY)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar((cQRY)->( IDREGORIG )) ) ,.T.,.T. )
                    
            // -- 03 Instancia o objeto da classe de integra��o        

            oSestini := TransactRows():New( cOrigem ) 
            oSestini:SetVariables( cOrigem , cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBlqProx )
            oSestini:cFunopc  			:=  cAutoFunc
            
            oSestini:TipoNF             := LEFT((cQRY)->TIPONF,1)
            oSestini:FormProprio        := 'S'
            oSestini:NroNotaFiscal      := ALLTRIM((cQRY)->NOTAFISCAL)
            oSestini:SerieNotaFiscal    := ALLTRIM((cQRY)->SERIENF)
            oSestini:DataEmissao        := CTOD((cQRY)->DTEMISSAO)
            oSestini:DataDigitacao      := CTOD((cQRY)->DTDIGITACAO)
            oSestini:Especie            := "SPED"
            oSestini:cODcLIENTE         := LEFT(RTrim((cQRY)->CPFCLIENTE),8) 
            oSestini:LojaCliente        := RIGHT(RTrim((cQRY)->CPFCLIENTE),4) 
            oSestini:CPFCliente         := ALLTRIM((cQRY)->CPFCLIENTE)
            oSestini:NroNotaEletronica  := ALLTRIM((cQRY)->NFELETRON)
            oSestini:ValorMercadorias   := (cQRY)->TOTMERC
            oSestini:ValorBrutoNF       := (cQRY)->TOTBRUTONF
            oSestini:ChaveNFE           := ALLTRIM((cQRY)->CHAVENFE)
            oSestini:ProtocoloNFE       := ALLTRIM((cQRY)->PROTOCNFE)
            oSestini:GeraTitulos        := ALLTRIM((cQRY)->GERADUP)
            oSestini:TabelaIntegracao   := ALLTRIM((cQRY)->TABORIGEM)
            oSestini:CampoFlagProcessado:= ALLTRIM((cQRY)->CPOSTATUS)
            oSestini:IDRegistro         := (cQRY)->IDREGORIG           
            oSestini:TabelaIntegracao   := 'Varejo_Terceiros'
            oSestini:CampoFlagProcessado:= 'ProcessadoProtheus'            
            
			// -- 05 M�todo que ir� prepara o aHeader do cabe�alho e itens com 
			// base no MapField cadastrado na PV5

            oSestini:PrepareAuto( "U_" + cAutoFunc , cIdMapFld )
            
            // -- Se tem Mapa de campos cadastrado foi p�ssivel montar os arrays
            
            aCposCab	:= aClone(oSestini:aCposCab)
            aCposDet	:= aClone(oSestini:aCposDet)

			aColsCab	:= {}
		    aColsGrid	:= {}

			aAux		:= {}
		
			// -- 06 Prepara aHeader para cabecalho (PAI) ##CGF

			IF Len(aCposCab) > 0 			
				
				// -- aCposCab[nCpos][2]   -->  Utiliza a posi��o [2] pois neste momento faz a leitura do campo da query  PV5_QFIELD  
				// -- Valida se o campo de relacionamento esta preenchido

				For  nCpos := 1 To Len(aCposCab)
					xCpoValue   := (cQRY)->( FieldGet( FieldPos( RTrim( aCposCab[nCpos][2] ) ) ) )
	     			aAdd( aColsCab , xCpoValue )
     			Next nCpos  
     			
     			
     			// -- Condicao de Pagamento Padrao
     			nPosCond := Ascan( aCposCab , 'PV1_CONDPG' )

     			If nPosCond == 0
     				aAdd( aCposCab , { 'PV1_CONDPG' , 'COND' } )  
     				aAdd( aColsCab , xST_CONDPG  )
     			Else
     				IF Empty( RTrim( aColsCab[nPosCond] ) )
     					aColsCab[nPosCond]	:= xST_CONDPG
     				Endif
     			Endif
     				     				           			
     			// -- Natureza Padrao     			
     			nPosNAT := Ascan( aCposCab , 'PV1_NATURE' )

     			If nPosNAT == 0
     				aAdd( aCposCab ,  { 'PV1_NATURE' , 'NATUREZ' } )  
     				aAdd( aColsCab ,   xST_NATURE  )
     			Else
     				IF Empty( RTrim( aColsCab[nPosNAT] ) )
     					aColsCab[nPosNAT]	:= xST_NATURE
     				Endif
     			Endif
     				     				           			
			Endif
			
			// -- 07 qUERY ITENS ##CGF

            aParam  := { { "#CNPJ#"     , cCNPJ } ,;
                         { "#NUMDOC#"   , ALLTRIM((CQRY)->NOTAFISCAL) } }

           	cITQuery  := ''
            cITQuery  := u_BIBuildQuery( 'VW_STNNFS_ITEM.SQL' )
            cITQuery  := u_DePara( aParam , cITQuery )                                             
            cQRY_IT	  := GetNextAlias()
            cItem     := "01"
			
	        oConnect:NewAlias( cITQuery , cQRY_IT )
	       	        
	        IF !oConnect:HasError() .And. (cQRY_IT)->( !Eof() )
	            u_WriteLog( cJobLog  ,  cTagLog + ' QUERY .. [OK] ' ,.T.,.T. )
	            
	        Elseif oConnect:HasError() .OR. ( !oConnect:HasError() .AND. (cQRY_IT)->( Eof() ) )
	        
	        	If oConnect:HasError()
	        		u_WriteLog( cJobLog  , oConnect:ErrorMessage()  , .T. , .T. ,,,, .T. /*lFunName*/  )
	        	Endif 
	            
	            IF  (cQRY_IT)->( Eof() )
	            	u_WriteLog( cJobLog  ,  cTagLog + ' QUERY .. [N�o h� dados] ' ,.T.,.T. )
	            Endif

				oSestini:AppendRow()
				oSestini:SetDoneById( nIdOrig  , @oConnect ) 
				oSestini:Destroy()
				
				Freeobj(oSestini)
				oSestini := Nil
				
				RecLock('PV9',.F.)
				PV9->PV9_RESULT := 'Documento poss�i registro de cabe�alho e N�O poss�i os registros de item. SeqId -> ' + PV9->PV9_SEQ
                PV9->PV9_STATUS := '2'
                PV9->PV9_SITUAC := '4'
                MsUnLock()  
				
				(cQRY_IT)->( DbCloseArea() )
				(cQRY)->( DbCloseArea() )								
				Loop

	        Endif

			// #CFG -- 08 Prepara aHeader para itens (FILHOS)   
			
			(cQRY_IT)->( DbGoTop() )
			
			cUnqKey	:= (cQRY_IT)->FILIAL + ALLTRIM((cQRY_IT)->NOTAFISCAL) + Alltrim((cQRY_IT)->CODCLIENTE)

			While (cQRY_IT)->( !Eof() .And.  cUnqKey == (cQRY_IT)->FILIAL + ALLTRIM((cQRY_IT)->NOTAFISCAL) + Alltrim((cQRY_IT)->CODCLIENTE) )
				
				aAux	:= {}
				
				IF Len(aCposDet) > 0 
	
					// -- aCposDet[nCpos][2]   -->  Utiliza a posi��o [2] pois neste momento faz a leitura do campo da query  PV5_QFIELD  
					// -- Valida se o campo de relacionamento esta preenchido
	
					aAux := {}				
					For  nCpos := 1 To Len(aCposDet)									
						xCpoValue   := (cQRY_IT)->( FieldGet( FieldPos( RTrim( aCposDet[nCpos][2] ) ) ) )
			     			aAdd( aAux , xCpoValue )				     			     
		     		Next nCpos
		     		

					// -- Armazen Padrao - Tratamento Especial para criar o armazen automaticamente antes da MsExecAuto 
										
	     			nPosCodPro := Ascan( aCposDet,{ |x| Alltrim(x[1]) == "PV2_COD" } ) 
	     			nPosLocPad := Ascan( aCposDet,{ |x| Alltrim(x[1]) == 'PV2_LOCAL' } ) 				

	     			If nPosLocPad > 0	     			
	     				IF !Empty( RTrim(aAux[nPosLocPad]) ) .And. !Empty(RTrim(aAux[nPosCodPro]))	     				
	     					SB1->( DbSetOrder(1) )
	     					IF SB1->( MsSeek( FwxFilial("SB1",SM0->M0_CODFIL) + RTrim(aAux[nPosCodPro]) ) )	     		
								SB2->( DbSetOrder(1) )
								If SB2->( !MsSeek( FwxFilial("SB2",SM0->M0_CODFIL) + SB1->B1_COD + RTrim(aAux[nPosLocPad]) ) )
									CriaSB2( SB1->B1_COD , RTrim(aAux[nPosLocPad]) )
								EndIf
	     					Endif	     				
	     				Endif	     				
	     			Endif


					// -- TES Padrao (utiliza caso n�o seja informado)					

	     			nPosTES := Ascan( aCposDet,{ |x| Alltrim(x[1]) == 'PV2_TES' } ) 				
	     			If nPosTES == 0
	     				aAdd( aCposDet , { 'PV2_TES' , 'TES' } )
	     				aAdd( aAux , xST_TESSAI )
	     			Else	     			
	     				aAux[nPosTES]	:= xST_TESSAI
	     			Endif 

					
					// -- Item do Pedido adicionado no Acols 
		     		
		     		aAdd( aColsGrid  , aAux ) 
									
				Endif
								
				(cQRY_IT)->( DbSkip() )
			
			End
			
			(cQRY_IT)->( DbCloseArea() )
                                    
			// -- 08 Alimenta o objeto da integra��o com as informa��es processadas
			
			oSestini:aColsCab     := aClone(aColsCab)
			oSestini:aColsGrid	  := aClone(aColsGrid)			    

			oSestini:aCposCab	  := aClone(aCposCab)
			oSestini:aCposDet	  := aClone(aCposDet)
				
			oSestini:AppendRow()
			oSestini:SaveSource()
			oSestini:SetDoneById( nIdOrig  , @oConnect ) 
			oSestini:Destroy()
			
			Freeobj(oSestini)
			oSestini := Nil
			
			u_WriteLog( cJobLog  ,  cTagLog + ' Finalizado ' + ALLTRIM((cQRY)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar( nIdOrig ) ) ,.T.,.T. )
			
			(cQRY)->( DbSkip() )
							    
        End
        
        (cQRY)->( DbCloseArea() )
           
    Endif
    
    AEval(aAreas, {|x| RestArea(x)})
        
Return .T.
