#Include 'Protheus.ch'
#include 'tbiconn.ch'

#DEFINE NARRAY  12

#DEFINE ITEM    1
#DEFINE PRODUTO 2
#DEFINE QTDE    3
#DEFINE PRCVEN  4
#DEFINE PRCUNIT 5
#DEFINE VLRITEM 6
#DEFINE B2LOCAL 7
#DEFINE LOTE    8
#DEFINE IDENTB6 9
#DEFINE REFORI  10
#DEFINE REFITEM 11
#DEFINE TES     12



/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnnfent_integration.log'
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'



/*/{Protheus.doc} STNNFENT
Interface de integra��o de notas fiscais de entrada Sestini

@type 	User Function
@author 	Mauro Paladini
@since 	Aug 17, 2018  2:33:31 PM
@version 1.0
@return 	${return}, ${return_description}

/*/

User Function STNNFENT( CRPCEmp, cRPCFil )

    Local aArea         := GetArea()
    Local aTCLink       := { {0,0} }
    Local lCriouAmb     := .F.
    Local oConnect      := Nil

    Local cQuery        := ''
    Local cAlias        := ''
    Local cSQL          := ''
    Local lSQL          := .T.
    Local lBlqSequen    := .F.
    Local cAgrupador    := ''
    Local cCNPJ         := ''
    Local cCodPva       := "STFNFE"

    Private cJobLog         := cDirLog + cCFGLog

    MontaDir(cDirLog)

    IF cRPCEmp <> Nil .And. cRPCFil <> Nil
        RPCSetType(3)
        RPCSETENV(cRPCEmp, cRPCFil)
        u_WriteLog( cJobLog  , '[PREP-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        lCriouAmb   := .T.
    Endif

// -- Valida conexao com DBAccess
    u_WriteLog( cJobLog  , '[FWDBACCESS] Iniciando conex�o com DbAccess ',.T.,.T. )

// -- Verifica conexao com DBaccess da Integra��o
    If ! U_ODBCConnect( @oConnect )

        If lCriouAmb
            u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
            RESET ENVIRONMENT
        Endif
        Return

    Endif


// -- Processa movimentos por Filial
    If IsBlind()
    
        PVB->( DbSetOrder(1) )
        IF PVB->( DbSeek( xFilial("PVB") + cCodPva ) )
        
            u_WriteLog( cJobLog , '[STNNFENT] Grupo de Filiais encontrado -> ' + cCodPva, .T. , .T. ,,,, .F. /*lFunName*/  )
            
            While PVB->( !Eof() .And.  PVB_FILIAL+PVB_CODGRP == xFilial("PVB") + cCodPva )

                IF PVB->PVB_ATIVO == '1'
                      
                    IF SM0->( DbSeek( PVB->PVB_CODEMP + PVB->PVB_CODFIL ) )
                    
                        u_WriteLog( cJobLog , '[STNNFENT] Integrando registros filial -> ' + SM0->M0_CODFIL , .T. , .T. ,,,, .F. /*lFunName*/  )
                        
                        cFilAnt := SM0->M0_CODFIL
                        cCNPJ   := RTrim(SM0->M0_CGC)
                    
                        ProcFilial( @oConnect , cCNPJ )
                           
                    Endif
            
                Endif
        
                PVB->( DbSkip() )
        
            End
    
        Else
        
            u_WriteLog( cJobLog , 'Grupo de filiais n�o localizado na tabela PV8' , .T. , .T. ,,,, .T. /*lFunName*/  )
                                
        Endif

    Else
    
        cCNPJ   := RTrim(SM0->M0_CGC)
    
        FWMsgRun( , {|| ProcFilial( @oConnect , cCNPJ ) } ,, "Processando .. "  )
    
    Endif


    
    // -- Finaliza conex�es realizadas

    oConnect:CloseConnection()
    oConnect:Finish()
    FreeObj(oConnect)

    If lCriouAmb
        u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.T.,.T. )
        RESET ENVIRONMENT
    Endif

Return





/*/{Protheus.doc} ProcFilial
Fun��o Auxiliar

@type       Function
@author     Mauro Paladini
@since      Aug 17, 2018  2:33:31 PM
@version    1.0
@return     ${return}, ${return_description}
/*/

Static Function ProcFilial( oConnect , cCNPJ )

    Local aAreas    := { PV1->(GetArea()) , PV2->(GetArea()) , PV9->(GetArea()) , GetArea() }
    Local cQuery    := ''
    Local cNFS      := ''   // Cabecalhos
    Local cITE      := ''   // Itens
    Local cNFD      := ''
    Local lRet      := .T.
    Local cSQL      := ''

// -- Pedido
    Local cNotaFis  := ''
    Local cIDNota   := ''
    Local cItem     := '01'
    Local aProdutos := {}

    Local cQryPath      := '\user_procedures\'
    Local cIDCliente    := ''
    Local oSestini      := Nil

    Local cChave        := ''

    // -- 1=Inclus�o;2=Altera��o;3=Exclus�o
    Local cOperacao     := '1'
    Local cKeyOrig      := ''
    Local cTabOrig      := 'SF1'
    Local nIndOrig      := 1
    Local nCpos         := 0
    Local aAuxItens     := {}
    

    // 1=Sim 2=Nao
    Local cBlqProx  := '1'

    oSestini  := Nil
    cOperacao := '1'
 
    // -- Conex�o realizada com sucesso ( Tudo deve ser feito dentro do Objeto oConnect )
    IF ( oConnect:HasConnection() .or. oConnect:OpenConnection() )
   
        // -- Query de entrada com Parse de campos    
        cQuery := u_BIBuildQuery( 'VW_STNNFENT_CAB.SQL' )
        cQuery := u_DePara( {{ "#CNPJ#" , cCNPJ }} , cQuery )
            
        cNFD    := GetNextAlias()
        oConnect:NewAlias( cQuery , cNFD )
       
        If oConnect:HasError()
            u_WriteLog( cJobLog  , cMsg  , .T. , .T. ,,,, .T. /*lFunName*/  )
            Return
        EndIf
        
        IF (cNFD)->( !Eof() )
            u_WriteLog( cJobLog  , '[STNNFENT] QUERY .. [OK] ' ,.T.,.T. )
        Else
            u_WriteLog( cJobLog  , '[STNNFENT] QUERY .. [N�o h� dados] ' ,.T.,.T. )
        Endif
        
        // -- La�o para o cabe�alho dos documentos        
        (cNFD)->( DbGoTop() )    
        While (cNFD)->( !Eof() )
                    
            // Begin Transaction
            
            // -- 1=Inclus�o;2=Altera��o;3=Exclus�o        
            oSestini    := Nil
            cOperacao   := '1'
            cFormulario := IIF( ALLTRIM( (cNFD)->CGCFORNECE ) == cCNPJ .OR. ALLTRIM( (cNFD)->CPFCLIENTE ) == cCNPJ , 'S' , 'N' )
        
            cChave      := (cNFD)->( TIPONF + NOTAFISCAL + SERIENF )      // -- Chave agrupadora
            cKeyOrig    := AllTrim ( cValToChar((cNFD)->( IDREGORIG )) )
            cTabOrig    := 'SF1'
            nIndOrig    := 1
          
            u_WriteLog( cJobLog  , '[STNNFENT] Integrando ' + ALLTRIM((cNFD)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar((cNFD)->( IDREGORIG )) ) ,.T.,.T. )
                    
            // -- Instancia o objeto da classe de integra��o        
            oSestini := TransactRows():New('MATA103')
            oSestini:SetVariables( 'MATA103' /*cOrigem */, cChave , cOperacao , cKeyOrig , cTabOrig , nIndOrig , cBlqProx )
            oSestini:cFunopc  := "U_SAUTONFE()"
        
            // - Prepara informa��es referente ao cabe�alho 
            oSestini:TipoNF             := LEFT((cNFD)->TIPONF,1)
            oSestini:FormProprio        := cFormulario
            oSestini:NroNotaFiscal      := ALLTRIM((cNFD)->NOTAFISCAL)
            oSestini:SerieNotaFiscal    := ALLTRIM((cNFD)->SERIENF)
            oSestini:DataEmissao        := CTOD((cNFD)->DTEMISSAO)
            oSestini:DataDigitacao      := CTOD((cNFD)->DTDIGITACAO)
            oSestini:Especie            := "SPED"
            oSestini:CodFornecedor      := ALLTRIM((cNFD)->CODFORNECE)
            oSestini:LojaFornecedor     := ""
            oSestini:cODcLIENTE         := ALLTRIM((cNFD)->CODCLIENTE)
            oSestini:LojaCliente        := ""
            oSestini:CPFCliente         := ALLTRIM((cNFD)->CPFCLIENTE)
            oSestini:CGCFornecedor      := ALLTRIM((cNFD)->CGCFORNECE)
            oSestini:NroNotaEletronica  := ALLTRIM((cNFD)->NFELETRON)
            oSestini:ValorMercadorias   := (cNFD)->TOTMERC
            oSestini:ValorBrutoNF       := (cNFD)->TOTBRUTONF
            oSestini:ChaveNFE           := ALLTRIM((cNFD)->CHAVENFE)
            oSestini:ProtocoloNFE       := ALLTRIM((cNFD)->PROTOCNFE)
            oSestini:GeraTitulos        := ALLTRIM((cNFD)->GERADUP)
            oSestini:TabelaIntegracao   := ALLTRIM((cNFD)->TABORIGEM)
            oSestini:CampoFlagProcessado:= ALLTRIM((cNFD)->CPOSTATUS)
            oSestini:IDRegistro         := (cNFD)->IDREGORIG
        
            // -- Prepara Query para os itens dp documento         
            If lRet
            
                aParam  := { { "#CNPJ#"     , cCNPJ } ,;
                             { "#NUMDOC#"   , ALLTRIM((cNFD)->NOTAFISCAL) } ,;
                             { "#SERIE#"    , ALLTRIM((cNFD)->SERIENF) } ,;
                             { "#TIPONF#"   , LEFT((cNFD)->TIPONF,1) } }

                cQuery  := ''
                cQuery  := u_BIBuildQuery( 'VW_STNNFENT_ITEM.SQL' )
                cQuery  := u_DePara( aParam , cQuery )                                             
                cITE    := GetNextAlias()
                cItem   := "01"
               
                oConnect:NewAlias( cQuery , cITE )
            
                If oConnect:HasError()
                    cMsg := "Erro query: [" + AllTrim( oConnect:ErrorMessage() ) + "]"
                    u_WriteLog( cJobLog  , cMsg , .T.,.T.,,,, .T. /*lFunName*/  )
                    Return
                EndIf
                                                        
                // -- Prepara array aProdutos para utiliza��o na u_STNIncPedido            
                aAuxCpos    :=  (cITE)->( Dbstruct() ) 
                aItens      := {}
                                        
                (cITE)->( DbGoTop() )
                
                // -- Valida a existencia dos itens do documento
                
                If (cITE)->( Eof() )
                                
                    // -- disparar grava��o do Log com erro
                    Return  .F.

                Endif
                
                While (cITE)->( !Eof() )
            
                    aAuxItens   := {}
                                                            
                    For nCpos := 1 To Len(aAuxCpos)
                    
                        lNumeric    := .F.
                        xCpoValue   := ''
                                                                  
                        If  ALLTRIM( aAuxCpos[nCpos][2] ) == "C"
                                                     
                            IF LEFT(aAuxCpos[nCpos][1],2) <> 'DT'

                                IF 'QUANT' $ aAuxCpos[nCpos][1]
                                    aTamCampo   := TamSx3("C6_QTDVEN")
                                    lNumeric    := .T.                                
                                Elseif LEFT( aAuxCpos[nCpos][1] , 3 ) $  "TOT-BAS-VAL"                                
                                    aTamCampo   := TamSx3("C6_VALOR")
                                    lNumeric    := .T.                                                                    
                                Elseif LEFT( aAuxCpos[nCpos][1] , 2 ) $  "VL"                                
                                    aTamCampo   := TamSx3("C6_VALOR")        
                                    lNumeric    := .T.                        
                                Elseif LEFT( aAuxCpos[nCpos][1] , 2 ) $  "AL"
                                    aTamCampo   := TamSx3("C6_ICMSRET")
                                    lNumeric    := .T.                                
                                Else                            
                                    lNumeric    := .F.                            
                                Endif                        
                            
                                If lNumeric                             
                                    aAuxCpos[nCpos][2]  := 'N'                                
                                    xCpoValue   := Val( (cITE)->( FieldGet( FieldPos( aAuxCpos[nCpos][1]) ) ) )
                                    xCpoValue   := NoRound( xCpoValue , aTamCampo[2] )
                                    aAuxCpos[nCpos][3]  := aTamCampo[1]
                                    aAuxCpos[nCpos][4]  := aTamCampo[2]                                    
                                Else                                
                                     xCpoValue   := AllTrim( (cITE)->( FieldGet( FieldPos( aAuxCpos[nCpos][1]) ) ) )                                 
                                Endif
                                
                                                                
                            Elseif LEFT(aAuxCpos[nCpos][1],2)  == 'DT'
                                xCpoValue   := CTOD( ALLTRIM( (cITE)->( FieldGet( FieldPos( aAuxCpos[nCpos][1]) ) ) ) )
                                aAuxCpos[nCpos][2]  := "D"
                            Endif
                        
                        Elseif ALLTRIM( aAuxCpos[nCpos][2] )  == "D"
                                xCpoValue   := CTOD( ALLTRIM( (cITE)->( FieldGet( FieldPos( aAuxCpos[nCpos][1]) ) ) ) )

                        
                        Elseif  ALLTRIM( aAuxCpos[nCpos][2] ) == "N"
                        
                            xCpoValue   := 0
                            IF 'QUANT' $ aAuxCpos[nCpos][1]
                                aTamCampo   := TamSx3("C6_QTDVEN")                                
                            Elseif LEFT( aAuxCpos[nCpos][1] , 3 ) $  "TOT-BAS-VAL"                                
                                aTamCampo   := TamSx3("C6_VALOR")                                
                            Elseif LEFT( aAuxCpos[nCpos][1] , 2 ) $  "VL"                                
                                aTamCampo   := TamSx3("C6_VALOR")                                
                            Elseif LEFT( aAuxCpos[nCpos][1] , 2 ) $  "AL"
                                aTamCampo   := TamSx3("C6_ICMSRET")                                
                            Else                            
                                aTamCampo   := { 14 , 4 }                            
                            Endif                        
                        
                            xCpoValue   := NoRound( (cITE)->( FieldGet( FieldPos( aAuxCpos[nCpos][1]) ) ) , aTamCampo[2] )
                            aAuxCpos[nCpos][3]  := aTamCampo[1]
                            aAuxCpos[nCpos][4]  := aTamCampo[2]
                        
                        Endif
                        
                        aAdd( aAuxItens , xCpoValue ) 
                                           
                    Next nCpos
                    
                    aAdd( aItens  , aAuxItens )
                    
                    (cITE)->( DbSkip() )
                                    
                End
                
                (cITE)->( DbCloseArea() )

                // -- Alimenta o objeto da integra��o com as informa��es dos itens
                
                oSestini:aCols      := aClone(aItens)
                oSestini:aHeader    := aClone(aAuxCpos)
                
            Endif
            
            oSestini:AppendRow()
            oSestini:SaveSource()                  
            oSestini:Destroy()
            
            Freeobj(oSestini)
            oSestini := Nil

            u_WriteLog( cJobLog  , '[STNNFENT] Finalizando ' + ALLTRIM((cNFD)->TABORIGEM) + ' Id: ' +  AllTrim ( cValToChar((cNFD)->( IDREGORIG )) ) ,.T.,.T. )
            
            // -- Atualiza Flag de lido
            
            If ! EMPTY( PV1->PV1_TABINT ) .And. ! EMPTY( PV1->PV1_FLAG )             

                cSQL := 'UPDATE ' + AllTrim( PV1->PV1_TABINT ) + ' SET ' + AllTrim( PV1->PV1_FLAG ) + ' = 1 '
                cSQL += 'WHERE Id = ' + AllTrim( cValToChar( PV1->PV1_IDREG ) ) 
                    
                If !oConnect:SQLExec( cSQL )
                    RecLock('PV9',.F.)
                    PV9->PV9_RESULT :=  AllTrim( oConnect:ErrorMessage() ) 
                    PV9->( MsUnLock() )
                Endif            

            Endif

            (cNFD)->( DbSkip() )
    
        End
        
        (cNFD)->( DbCloseArea() )
           
    Endif
    
    AEval(aAreas, {|x| RestArea(x)})
        
Return


/*/{Protheus.doc} STNCadCli
Cadastramento automatico do cliente

@type       Function
@author     Mauro Paladini
@since      Aug 17, 2018  2:33:31 P M
@version    1.0
@return     ${return}, ${return_description}
/*/


Static Function STNCadCli( cID , cCNPJ )

    Local aAreas    := { SA1->(GetArea()) , GetArea() }
    Local aCliente  := {}
    Local cNome     := ''
    Local cNReduz   := ''
    Local cCGC      := ''

    Local cEndere   := ''
    Local cBairro   := ''
    Local cUF       := ''
    Local cCep      := ''
    Local cCodMun   := ''
    Local cInscr    := ''
    Local cFone     := ''
    Local cCodCli   := ""
    Local cLojCli   := ""
    Local cQuery    := ''
    Local lRet      := .T.
    Local cNFC      := ''

    Local aErro     := {}
    Local cMsgErro  := ''
    Local nX        := ''

    Private lMsErroAuto := .F.

    cQuery  := u_BIBuildQuery( 'QRY_STNNFENT_CLI' )
    cQuery  := u_DePara( {      { "'#CNPJ#'"        , cCNPJ },;
        { "'#IDTERCEIRO#'"  , cID    }} , cQuery )
        

    cNFC     := GetNextAlias()
    oConnect:NewAlias( cQuery , cNFC )

    If oConnect:HasError()
        cMsg := "Erro query: [" + AllTrim( oConnect:ErrorMessage() ) + "]"
        u_WriteLog( cJobLog  , cMsg , .T.,.T.,,,, .T. /*lFunName*/  )
        Return
    EndIf
    
    (cNFC)->( DbGoTop() )

    cNome     := AvKey( (cNFC)->NOME          , "A1_NOME" )
    cNReduz   := AvKey( (cNFC)->NOMEFANTASIA  , "A1_NREDUZ" )
    cCGC      := StrTran( StrTran( StrTran( (cNFC)->DOCUMENTO , '.' , '' ) , '-' , '' ) , '/' , '' )
    cCGC      := AvKey( cCGC                  , "A1_CGC" )
    cEndere   := AvKey( RTrim((cNFC)->ENDERECO) + ' ' + RTRim((cNFC)->NUMERO)  , "A1_END" )
    cBairro   := AvKey( (cNFC)->BAIRRO        , "A1_BAIRRO" )
    cUF       := AvKey( (cNFC)->ESTADO        , "A1_EST" )
    cCep      := AvKey( (cNFC)->CEP           , "A1_CEP" )
    cCodMun   := AvKey( (cNFC)->CODMUN        , "A1_COD_MUN" )
    cInscr    := StrTran( StrTran( StrTran( (cNFC)->IE , '.' , '' ) , '-' , '' ) , '/' , '' )
    cInscr    := AvKey( cInscr                , "A1_INSCR" )
    cCodCli   := SA1->( U_DXGetSXE( "SA1","A1_COD",1 ) )
    cLojCli   := "01"

    If __lSX8
        ConfirmSX8()
    EndIf

    aCliente :={{"A1_COD"           ,cCodCli                                ,Nil},;
        {"A1_LOJA"          ,cLojCli                                ,Nil},;
        {"A1_NOME"          ,cRazao                                 ,Nil},;
        {"A1_NREDUZ"        ,cNReduz                                ,Nil},;
        {"A1_CGC"           ,cCGC                                   ,Nil},;
        {"A1_PESSOA"        ,If(Len(AllTrim(cCGC))< 14,'F','J')     ,Nil},;
        {"A1_END"           ,cEndere                                ,Nil},;
        {"A1_BAIRRO"        ,cBairro                                ,Nil},;
        {"A1_EST"           ,cUF                                    ,Nil},;
        {"A1_CEP"           ,cCep                                   ,Nil},;
        {"A1_COD_MUN"       ,cCodMun                                ,Nil},;
        {"A1_INSCR"         ,cInscr                                 ,Nil},;
        {"A1_TIPO"          ,"F"                                    ,Nil}}

    lMsErroAuto := .F.

    MSExecAuto({|x,y| MATA030(x,y)},aCliente,3)

    If lMsErroAuto
    
        aErro       := GetAutoGRLog()
        cMsgErro    := ""
    
        For nX := 1 To Len(aErro)
            cMsgErro += alltrim(aErro[nX])+Chr(13)+Chr(10)
        Next nX
    
        cMsgErro := StrTran(cMsgErro,"<","")
        cMsgErro := StrTran(cMsgErro,">","") + CRLF

        u_WriteLog( cJobLog  , '[MATA030] ERRO ao executar MsExecAuto ' ,.T.,.T. )
        u_WriteLog( cJobLog  , '[MATA030] ' + cMsgErro ,.T.,.T. )

    Endif

    AEval(aAreas, {|x| RestArea(x)})

Return lRet
