#Include 'Protheus.ch'

/*/{Protheus.doc} CVRLOJ02
Relatorio de registros importados de cupons fiscais. Vers�es Anal�tico e Sint�tico
@author Giane
@since 12/02/2015
@version 1.0
/*/
User Function CVRLOJ02()

Local oReport := Nil

oReport:= ReportDef()
oReport:PrintDialog()

Return




/*/{Protheus.doc} ReportDef
Monta as celulas e se��es do relat�rio
@author Giane
@since 12/02/2015
@version 1.0
/*/
Static Function ReportDef()

Local oReport    := Nil
Local oSection1  := Nil
Local oSection2  := Nil
Local oBreak     := Nil
Local cPerg      := PadR( 'CVRLOJ02',Len(SX1->X1_GRUPO) )
Local aOrdem     := {"Data+Arquivo+Cupom"}  

Local cAliasP    := GetNextAlias()  

oReport := TReport():New("CVRLOJ02","Relat�rio de registros Importados de Cupom Fiscal.",cPerg, {|oReport| ReportPrint(@oReport,@cAliasP,@aOrdem)},"Este relat�rio exibe os registros importados de cupom fiscal, com ou sem inconsist�ncias.")
oReport:SetLandscape() //Define orienta��o de p�gina do relat�rio como retrato

AjustaSx1(cPerg)
Pergunte(cPerg,.F.)

oSection1 := TRSection():New(oReport,"Cupons fiscais importados",{"P01"},aOrdem/*{Array com as ordens do relat�rio}*/, .F., .F.,,,)
TRCell():New( oSection1,"DATA"        , ,'Data'         ,/*Picture*/,10,/*lPixel*/  ,{ || (cAliasP)->P01_DTIMP  } )
TRCell():New( oSection1,"HORA"        , ,'Hora'         ,/*Picture*/,5,/*lPixel*/  ,{ || (cAliasP)->P01_HRIMP  } )
TRCell():New( oSection1,"ARQUIVO"     , ,'Arquivo'      ,/*Picture*/,TamSX3("P01_ARQ")[1]/*nTam*/,/*lPixel*/  ,{ || (cAliasP)->P01_ARQ } )
TRCell():New( oSection1,"CUPOM"       , ,'Cupom'        ,/*Picture*/,6,/*lPixel*/  ,{ || (cAliasP)->P02_DOC  } )
TRCell():New( oSection1,"NUMORC"      , ,'Or�amento'    ,/*Picture*/,6,/*lPixel*/  ,{ || (cAliasP)->P02_NUMORC  } )
TRCell():New( oSection1,"VLRTOT"      , ,'Valor Total'  ,X3Picture("P01_VALLIQ"),30/*nTam*/,/*lPixel*/  ,{ || (cAliasP)->P01_VALLIQ }, "RIGHT", ,"RIGHT")
TRCell():New( oSection1,"DETCAB"      , ,'Detalhes'          ,,60,/*lPixel*/  ,{ || (cAliasP)->DETCAB  }, , .T. )
TRCell():New( oSection1,"STATCAB"     , ,'Status Registro'   ,,25,/*lPixel*/  ,{ || u_PsRetBox( "P01_STATUS" , (cAliasP)->P01_STATUS )  }, ,.T. )


oSection2 := TRSection():New(oSection1,"Itens Importados",{"P02"},aOrdem/*{Array com as ordens do relat�rio}*/, .F., .F.,,,.F. )
TRCell():New( oSection2,"PROD"         , ,'Produto'           ,/*Picture*/,15,/*lPixel*/ ,{ || (cAliasP)->P02_PROD  } )
TRCell():New( oSection2,"LINHA"        , ,'Linha'             ,/*Picture*/,3,/*lPixel*/  ,{ || (cAliasP)->P02_LINHA  } )
TRCell():New( oSection2,"QTD"          , ,'Qtd'               ,"@E 999,999.999",15,/*lPixel*/  ,{ || (cAliasP)->P02_QUANT },"RIGHT",,"RIGHT" )
TRCell():New( oSection2,"VLRVEND"      , ,'Vlr.Venda'         ,"@E 999,999.99",15,/*lPixel*/  ,{ || (cAliasP)->P02_VALLIQ },"RIGHT",,"RIGHT" )
TRCell():New( oSection2,"DESCRI"       , ,'Descri��o'         ,,30,/*lPixel*/  ,{ || (cAliasP)->P02_DESCRI }, ,.T. )
TRCell():New( oSection2,"DETALHE"      , ,'Detalhes'          ,,90,/*lPixel*/  ,{ || (cAliasP)->DETALHE  }, , .T. )
TRCell():New( oSection2,"STATUS"       , ,'Status Registro'   ,,25,/*lPixel*/  ,{ || u_PsRetBox( "P02_STATUS" , (cAliasP)->P02_STATUS ) }, ,.T. )   

oSection2:SetColSpace(2)
oSection2:SetLeftMargin(10)
 
Return(oReport) 



/*/{Protheus.doc} ReportPrint
Monta dados do relat�rio e manda imprimir
@author Giane
@since 12/02/2015
/*/
Static Function ReportPrint(oReport, cAliasP,aOrdem)

Local oSection1 := oReport:Section(1)
Local oSection2 := oSection1:Section(1)
Local cStatus   := "%%"
Local cDetalhe	:= "%%"

If MV_PAR05 == 1 //somente os inconsistentes
	cStatus := "%AND P01.P01_STATUS IN ('1','8')%"
Endif

If MV_PAR06 == 2 //Sintetica
	oSection1:SetHeaderPage()	
	oSection2:Hide()	
Else
	oSection1:SetHeaderPage(.F.)	
	oSection1:SetHeaderSection(.T.)	
Endif

If MV_PAR07 == 1 //Detalhe Completo
	cDetalhe := "%  CAST(CAST(P02_LOGIMP AS varbinary(8000)) AS varchar(8000)) DETALHE%"
Else
	cDetalhe := "%  CAST(CAST(P02_LOGIMP AS varbinary(8000)) AS varchar(100)) DETALHE%"
Endif

FullRange(oReport:uParam)

MakeSqlExpr(oReport:uParam)

BEGIN REPORT QUERY oSection1
BeginSql Alias cAliasP
	SELECT P01_DOC, P01_ARQ, P01_DTIMP, P01_HRIMP, P01_VALLIQ, P01_STATUS, CAST(CAST(P01_LOGIMP AS varbinary(8000)) AS varchar(100)) DETCAB, 
	       P02_DOC, P02_NUMORC, P02_PROD, P02_LINHA, P02_QUANT, P02_VALLIQ, P02_STATUS ,P02_DESCRI, 
	       %Exp:cDetalhe% 
		FROM %table:P01% P01
	INNER JOIN %table:P02% P02 ON
	    P02.P02_FILIAL  = %xFilial:P02%
	    AND P02.P02_ARQ = P01.P01_ARQ	
	    AND P02.P02_DOC = P01.P01_DOC
	    AND P02.%NotDel%		   	
	WHERE
	    P01.P01_FILIAL = %xFilial:P01%
	    AND P01.P01_DTIMP BETWEEN %Exp:mv_par01% AND  %Exp:mv_par02%
	    AND P01.P01_ARQ >= %Exp:MV_PAR03% AND P01.P01_ARQ <= %Exp:MV_PAR04%	    
	    AND P01.%NotDel%	  
	    %Exp:cStatus%  
	ORDER BY P01_DTIMP, P01_ARQ, P01_DOC //ver com mauro se o indice foi criado na p01 desta forma
EndSql 
END REPORT QUERY oSection1 

//������������������������������������������������������������������������Ŀ
//�Usa Query da Secao Principal nas Secoes Filhas                          �
//��������������������������������������������������������������������������
oSection2:SetParentQuery()
oSection2:SetParentFilter( { |cParam| DTOC((cAliasP)->P01_DTIMP) + (cAliasP)->P01_ARQ + (cAliasP)->P01_DOC == cParam },{ || DTOC((cAliasP)->P01_DTIMP) + (cAliasP)->P01_ARQ + (cAliasP)->P01_DOC } ) 

oSection1:Print()

Return Nil

/*/{Protheus.doc} AjustaSX1
Perguntas necessarias para realizar o filtro dos dados a serem impressos
@author Giane
@since 12/02/2015
@version 1.0
@param cPerg, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function AjustaSX1( cPerg )
Local aHelp  
                                                                                 
aHelp := {}
AAdd( aHelp, 'Informe a data inicial das importa��es  ' )  
PutSx1(cPerg,'01','Data Importa��o de?','Data Importa��o de?','Data Importa��o de?','MV_Ch1','D',08,0,0,'G','','','','',"MV_PAR01","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe a data final das importa��es ' )
PutSx1(cPerg,'02','Data Importa��o At�?','Data Importa��o At�?','Data Importa��o At�?','MV_Ch2','D',08,0,0,'G','(MV_PAR02 >= MV_PAR01)','','','',"MV_PAR02","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe o nome do arquivo inicial' )  
PutSx1(cPerg,'03','Do Arquivo?','Do Arquivo?','Do Arquivo?','MV_Ch3','C',TamSX3("CV8_PROC")[1],0,0,'G','','','','',"MV_PAR03","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe o nome do arquivo final' )  
PutSx1(cPerg,'04','At� o Arquivo?','At� o Arquivo?','At� o Arquivo?','MV_Ch4','C',TamSX3("CV8_PROC")[1],0,0,'G','','','','',"MV_PAR04","","","","","","","","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe o status que deseja imprimir' )
PutSx1(cPerg,'05','Status?','Status?','Status?','MV_Ch5','N',1,0,0,'C','NaoVazio','','','',"MV_PAR05","Inconsistentes","Inconsistentes","Inconsistentes","","Todos","Todos","Todos","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe tipo de layout' )
PutSx1(cPerg,'06','Impress�o?','Impress�o?','Impress�o?','MV_Ch6','N',1,0,0,'C','NaoVazio','','','',"MV_PAR06","Analitica","Analitica","Analitica","","Sint�tica","Sint�tica","Sint�tica","","","","","","","","","",aHelp,aHelp,aHelp)

aHelp := {}
AAdd( aHelp, 'Informe se no relat�rio anal�tico os detalhes ser�o apresentados de forma completa ou resumida.' )
PutSx1(cPerg,'07','Detalhes?','Detalhes?','Detalhes?','MV_Ch7','N',1,0,0,'C','NaoVazio','','','',"MV_PAR07","Completo","Completo","Completo","","Resumido","Resumido","Resumido","","","","","","","","","",aHelp,aHelp,aHelp)

Return




/*/{Protheus.doc} PsRetBox
Retorna conteudo de campo COMBOBOX para impressao em relatorio 

@author Mauro Paladini
@since 20/03/2015
@version 1.0
/*/

User Function PsRetBox( cCampo , cConteudo , lArray )

	Local aArea			:= GetArea()
	Local aAreaSX3		:= SX3->( GetArea() )
	Local nInitCBox		:= 0
	Local nMaxLenCombo	:= 0
    Local cInitValue	:= ""
    Local aRet			:= {}
    Local cRet			:= ""
    
    Default lArray	:= .F.
    
    SX3->( DbSetOrder(2) )
    
    IF SX3->( DbSeek( cCampo ) )        
    
    	aRet := RetSX3Box( SX3->X3_CBOXSPA , @nInitCBox , @nMaxLenCombo , SX3->X3_TAMANHO , @cInitValue )    	
    	nPos := Ascan( aRet , { |x| Upper(AllTrim(x[2])) == Upper(AllTrim(cConteudo)) } ) 
    	If nPos > 0
    		cRet := aRet[nPos,3]
    	Endif
    	
    Endif
    
    If lArray
    	cRet	:= aClone(aRet)
    Endif
    
    RestArea(aAreaSX3)
    RestArea(aArea)

Return cRet
