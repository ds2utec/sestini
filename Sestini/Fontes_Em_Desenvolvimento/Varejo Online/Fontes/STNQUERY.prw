#Include 'TOTVS.CH'

#DEFINE BR   CHR(13) + CHR(10)

// -- ## INTEGRA��O VAREJO ONLINE ##
// Funcoes responsaveis por devolver as querys customizadas do cliente
// No processo de integracao com o banco de dados intermediario

// ## Produtos

/*/{Protheus.doc} ProdQuery()
Integra��o Varejo Online -> Cadastro de Produtos -> Retaguarda para PDV
Prepara Script SQL contendo os principais campos definidos pelo cliente

@type function
@author     Mauro Paladini
@since  01/10/2018
@version    1.0
/*/

Static Function ProdQuery()

Local cQuery    := ''

Local cColecao      := ''
Local cPersonagem   := ''
Local cCategoria    := ''
Local cGrupo        := ''

//---------------------------------------------------------
// Se o produto esta bloqueado, desconsidera a integracao -
//---------------------------------------------------------
If SB1->B1_MSBLQL == '1'
	Return ""
Endif

//------------------------------------------------------------------------------------------------
// Se o produto estiver na tabela de controle de produto em restricao, desconsidera a integracao -
//------------------------------------------------------------------------------------------------
If inRestrict( SB1->B1_COD )
	Return ""
Endif

// -- Grupo
If !Empty( SB1->B1_GRUPO )
    SBM->( DbSetOrder(1) )
    IF SBM->( DbSeek( xFilial('SBM') + SB1->B1_GRUPO ) )
        cGrupo  := RTRIM(SBM->BM_DESC)
    Endif
Endif

// -- Categoria
If !Empty( SB1->B1_XCATEG )
    SZC->( DbSetOrder(1) )
    IF SZC->( DbSeek( xFilial('SZC') + SB1->B1_XCATEG ) )
        cCategoria  := RTRIM(SZC->ZC_DESCR)
    Endif
Endif

// -- Colecao
If !EMPTY( SB1->B1_XCOLEC )
    SZD->( DbSetOrder(1) )
    IF SZD->( DbSeek( xFilial('SZD') + SB1->B1_XCOLEC ) )
        cColecao := RTRIM(SZD->ZD_DESCR)
    Endif
Endif

// -- Personagens
If !Empty( SB1->B1_XPERSON )
    SZG->( DbSetOrder(1) )
    IF SZG->( DbSeek( xFilial('SZG') + SB1->B1_XPERSON ) )
        cPersonagem := RTRIM(SZG->ZG_DESCR)
    Endif
Endif


cQuery := "DECLARE @IdProd INT;   "
cQuery += BR + "SELECT  @IdProd = IsNull( Max( Id ), 0 ) FROM [Sestini_Integracoes].[dbo].[Varejo_Produtos] ; "  

cQuery += BR + "IF NOT EXISTS ( SELECT * FROM [Sestini_Integracoes].[dbo].[Varejo_Produtos]    "
cQuery += BR + "WHERE   CodigoSistema = '" + RTRIM(SB1->B1_COD) + "'"
cQuery += BR + "        And Token = '00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de'  )"  

cQuery += BR + "    BEGIN   "
cQuery += BR + "        SET @IdProd = @IdProd + 1 "   
cQuery += BR + "        INSERT  INTO [Sestini_Integracoes].[dbo].[Varejo_Produtos] "          
cQuery += BR + "                ( "       
cQuery += BR + "                    Descricao, "   
cQuery += BR + "                    Classificacao, "              
cQuery += BR + "                    Origem,    "
cQuery += BR + "                    Fci,  "
cQuery += BR + "                    MetodoControle,  "           
cQuery += BR + "                    Unidade,  "           
cQuery += BR + "                    Preco,  "           
cQuery += BR + "                    CodigoNCM, "          
cQuery += BR + "                    Especificacao,  "           
cQuery += BR + "                    CodigoBarras,  "           
cQuery += BR + "                    CodigoSistema, "           
cQuery += BR + "                    DataCriacao,   "
cQuery += BR + "                    Token, "           
cQuery += BR + "                    Id, "           
cQuery += BR + "                    Ativo   " 
cQuery += BR + "                ) "  
cQuery += BR + "        VALUES        "

cQuery += BR + "                (            "
cQuery += BR + "                    '" + RTRIM(SB1->B1_DESC) + " ', "
cQuery += BR + "                    'REVENDA', "
cQuery += BR + "                    " + SB1->B1_ORIGEM + ", "
cQuery += BR + "                    Null, "  
cQuery += BR + "                    'ESTOCAVEL', "           
cQuery += BR + "                    '" + RTRIM(SB1->B1_UM) + "', "           
cQuery += BR + "                    " + AllTrim(STr(SB1->B1_PRV1,10,2)) + " , "           
cQuery += BR + "                    '" + RTRIM(SB1->B1_POSIPI) + "',   "
cQuery += BR + "                    Null,"
cQuery += BR + "                    '" + RTRIM(SB1->B1_CODBAR) + "',   "
cQuery += BR + "                    '" + RTRIM(SB1->B1_COD) + "', "
cQuery += BR + "                    GetDate(), "           
cQuery += BR + "                    '00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de', "           
cQuery += BR + "                    @IdProd , "           
cQuery += BR + "                    '1' "
cQuery += BR + "                ) "
cQuery += BR + "    END "
cQuery += BR + "    ELSE "
cQuery += BR + "    BEGIN "
cQuery += BR + "        UPDATE [Sestini_Integracoes].[dbo].[Varejo_Produtos] "       
cQuery += BR + "        SET Descricao       = '" + RTRIM(SB1->B1_DESC) +" ', "
cQuery += BR + "            Classificacao   = 'REVENDA', "
cQuery += BR + "            Origem          = " + SB1->B1_ORIGEM + ", "
cQuery += BR + "            Fci             = Null, "
cQuery += BR + "            MetodoControle  = 'ESTOCAVEL', "
cQuery += BR + "            Unidade         = '" + RTRIM(SB1->B1_UM) + "', "     
cQuery += BR + "            Preco           = " + AllTrim(STr(SB1->B1_PRV1,10,2)) + " , "  
cQuery += BR + "            CodigoNCM       = '" + RTRIM(SB1->B1_POSIPI) + "',   "
cQuery += BR + "            Especificacao   = Null, "
cQuery += BR + "            CodigoBarras    = '" + RTRIM(SB1->B1_CODBAR) + "', "   
cQuery += BR + "            DataAlteracao   = CONVERT( DATETIME , '" + DTOS(Date()) + "' ),  " 
cQuery += BR + "            Ativo           = '1' "
cQuery += BR + "        WHERE   CodigoSistema = '" + RTRIM(SB1->B1_COD) + "' And "
cQuery += BR + "                Token = '00bc81d3e397388d3aa341e8ccfc99958c000102dc9eb7c578c00e19a351d3de' "              
cQuery += BR + "    END "

Return cQuery


/*/{Protheus.doc} inRestrict
Funcao para identificar se um produto contem restri��es
@author DS2U (SDA)
@since 01/03/2019
@version 1.0
@return lRet, Se .T., contem restri��es. Se .F., n�o contem restricoes
@param cProduto, characters, Codigo do Produto
@type function
/*/
Static Function inRestrict( cProduto )

Local lRet      := .F.
Local clAlias   := getNextAlias()
Local cCanalVar := formatIn( allTrim( getMv( "ES_CNVAR",,"VAR" ) ), "/" ) // Parametro que indica quais os c�digos de canal do varejo

Default cProduto := ""

BEGINSQL ALIAS clAlias

	SELECT
	 	Z8_PRODUTO
	 	, Z8_CANAL
	 	
	 FROM
	 	%TABLE:SZ8% SZ8
	 	
	 WHERE
	 	SZ8.Z8_FILIA = %XFILIAL:SZ8%
	 	AND SZ8.Z8_PRODUTO = %EXP:cProduto%
	 	AND SZ8.Z8_CANAL IN %EXP:cCanalVar%
	 	AND SZ8.%NOTDEL%

ENDSQL 

lRet := !( clAlias )->( eof() )
( clAlias )->( dbCloseArea() )

Return lRet


// ## Saldos em estoque na matriz

/*/{Protheus.doc} SaldoEstoque()
Integra��o Varejo Online -> Saldo em estoque Matriz -> PDV Varejo ( para consulta )
Prepara Script SQL contendo os principais campos definidos pelo cliente

@type   function
@author Mauro Paladini
@since  01/10/2018
@version    1.0
/*/

Static Function SaldoQuery()

Local cQuery    := ''

cQuery := "IF NOT EXISTS ( SELECT * FROM [Sestini_Integracoes].[dbo].[Varejo_SaldoEstoque] "    
cQuery += BR + "WHERE   SISTEMA = '" + RTRIM(SB1->B1_COD) + "' ) "
cQuery += BR + "    BEGIN "   
cQuery += BR + "        INSERT  INTO [Sestini_Integracoes].[dbo].[Varejo_SaldoEstoque] " 
cQuery += BR + "                ( " 
cQuery += BR + "                    SISTEMA, " 
cQuery += BR + "                    SALDO, " 
cQuery += BR + "                    DATAULTIMAMOVIMENTACAO "
cQuery += BR + "                ) " 
cQuery += BR + "        VALUES        "
cQuery += BR + "                ( "            
cQuery += BR + "                    '" + RTRIM(SB1->B1_COD) + "', " 
cQuery += BR + "                    " + AllTrim(cValToChar(SB2->B2_QATU))+ ", " 
cQuery += BR + "                    CONVERT( DATETIME , '" + DTOS(Date()) + "' ) "  "
cQuery += BR + "                ) " 
cQuery += BR + "    END "  
cQuery += BR + "    ELSE " 
cQuery += BR + "    BEGIN " 
cQuery += BR + "        UPDATE [Sestini_Integracoes].[dbo].[Varejo_SaldoEstoque] " 
cQuery += BR + "        SET SALDO       = " + AllTrim(cValToChar(SB2->B2_QATU))+ ", "
cQuery += BR + "            DATAULTIMAMOVIMENTACAO = CONVERT( DATETIME , '" + DTOS(Date()) + "' ) "      
cQuery += BR + "        WHERE   SISTEMA= '" + RTRIM(SB1->B1_COD) + "' "
cQuery += BR + "    END " 

Return cQuery