#Include 'Protheus.ch'
#include 'tbiconn.ch'

#DEFINE NARRAY  12

#DEFINE ITEM    1 
#DEFINE PRODUTO 2
#DEFINE QTDE    3
#DEFINE PRCVEN  4
#DEFINE PRCUNIT 5   
#DEFINE VLRITEM 6
#DEFINE B2LOCAL 7
#DEFINE LOTE    8
#DEFINE IDENTB6 9
#DEFINE REFORI  10
#DEFINE REFITEM 11
#DEFINE TES     12


/*
---- CONFIGURAÇÃO NO APPSERVER.INI ------
[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890
*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnstoks_integration.log' 
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'

/*/{Protheus.doc} STNSTOKS
Interface de integração de estoques

@type 	User Function
@author Mauro Paladini
@since 	Aug 17, 2018  2:33:31 PM

/*/

User Function STNSTOKS( CRPCEmp, cRPCFil )

Local aArea         := GetArea()
Local aTCLink       := { {0,0} }
Local lCriouAmb     := .F.
Local oSestini      := Nil 

Local cTrb          := ''
Local cQry          := ''
Local cAlias        := ''
Local cCNPJ         := ''
Local cLocPad       := ''

Local cTabOrig  := ''
Local cOperacao := ''    
Local cKeyOrig  := ''
Local nIndOri   := 0
Local cQuery    := ''
Local cChave    := ''

Private cJobLog         := cDirLog + cCFGLog

MontaDir(cDirLog)

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
    RPCSetType(3) 
    RPCSETENV(cRPCEmp, cRPCFil)
    u_WriteLog( cJobLog  , '[PREP-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.F.,.T. )
    lCriouAmb   := .T.
Endif 

// -- Local padrao
cLocPad := GetMv('ES_STNLOC',,'EXP')
cTrb    := GetNextAlias()

BEGINSQL Alias cTrb

SELECT B2_FILIAL, B2_COD, B2_LOCAL, R_E_C_N_O_ AS B2REC
FROM %table:SB2% SB2
WHERE   B2_FILIAL = %xFilial:SB2%    And
        B2_LOCAL  = %Exp:cLocPad%    And
        B2_MSEXP  = '' And
        D_E_L_E_T_ = ''

ENDSQL

(cTrb)->( DbGoTop() )
        

SB1->( DbSetOrder(1) )
SB2->( DbSetOrder(1) )

// -- Envia para a fila de replicacao a atualização de saldos em estoque 

While (cTrb)->( !Eof() )

    SB2->( DbGoTo( (cTrb)->B2REC ) )   
    SB1->( DbSeek( xFilial('SB1') + SB2->B2_COD ) )

    cTabOrig  := 'SB2'
    cOperacao := '1'   
    cChave    := SB2->B2_COD 
    cKeyOrig  := SB2->B2_FILIAL+SB2->B2_COD+SB2->B2_LOCAL
    nIndOri   := 1
    cQry      := StatiCCall( STNQUERY , SaldoQuery )

    oSestini := TransactRows():New()
    oSestini:SetVariables( 'MATA225' /*cOrigem */, cChave , cOperacao , cKeyOrig , cTabOrig , nIndOri , Nil )
    oSestini:SetScript( cQry )
    oSestini:AppendRow()
    oSestini:Destroy()
    
    Freeobj(oSestini)
    oSestini := Nil
    
    RecLock('SB2',.F.)
    SB2->B2_MSEXP := DTOs(Date())
    MsUnLock()
   
    (cTrb)->( DbSkip() )
   
End        

(cTrb)->( DbCloseArea() )

If lCriouAmb
    u_WriteLog( cJobLog  , '[RESET-ENVIRONMENT] Empresa ' + cRPCEmp + ' Filial ' + cRPCFil  ,.F.,.T. )
    RESET ENVIRONMENT
Endif

Return



