#include "protheus.ch"
#include "apwizard.ch"
//#include "apcfg80.ch"

/*/{Protheus.doc} TibCatCfg
Configurador do ambiente 
Template de Importa��o de Cupons Portaria CAT52 

@author Mauro Paladini
@since 18/03/2015
@version 1.0
/*/

User Function TibCatCfg()

Local oWizard
Local lSave 	:= .F.
Local i			:= 0
Local nX		:= 0

Local oAutoSB0	:= Nil
Local oAutoSB1	:= Nil
Local oAutoTES	:= Nil
Local oAutoSB2	:= Nil
Local oAutoALQ	:= Nil
Local oVlPreco	:= Nil
Local oUsaMT	:= Nil
Local oTESSAI	:= Nil

Local lUsaMT	
Local aCombo	:= { "Sim" , "N�o" }

Local cFS_DIR52		:= Padr( GetMv("FS_DIR52"	,,"\integracao_pdv\importar\"), 200 )
Local cFS_ERROS52	:= Padr( GetMv("FS_ERROS52"	,,"\integracao_pdv\invalidos\"), 200 ) 
Local cFS_LIDOS52	:= Padr( GetMv("FS_LIDOS52"	,,"\integracao_pdv\lidos\"), 200 ) 
Local cFS_VCAT52	:= Padr( GetMv("FS_VCAT52"	,,"V01"), 3 )   

Local lUsaMT		:= GetMv("FS_CATMT"	,,.F.)
Local nFS_NTIMP		:= GetMv("FS_NTIMP"	,,0)
Local nFS_NTVLD		:= GetMv("FS_NTVLD"	,,0) 
Local nFS_NTORC		:= GetMv("FS_NTORC"	,,0) 
Local nFS_NTVEN		:= 0

Local aHeadGetD		:= {}
Local aColsGetD		:= {}
Local nOpcGetD 		:= GD_INSERT + GD_UPDATE + GD_DELETE

Local aCposGD		:= { "LG_CODIGO", "LG_NOME" , "LG_IMPFISC" , "LG_SERIE", "LG_PDV" , "LG_TAMSER" , "LG_SERPDV" , "LG_XNUMCX" , "LG_XNOMECX" }
Local nPosEstacao	:= 0 

Private cFS_AUTOB0	:= If( GetMv("FS_AUTOSB0",,.F.), "Sim" , "N�o" )
Private cFS_AUTOB2	:= If( GetMv("FS_AUTOSB2",,.F.), "Sim" , "N�o" )
Private cFS_AUTOB1	:= If( GetMv("FS_AUTOSB1",,.F.), "Sim" , "N�o" )
Private cFS_AUTOTES	:= If( GetMv("FS_AUTOTES",,.F.), "Sim" , "N�o" ) 
Private cFS_AUTOALQ	:= If( GetMv("FS_AUTOALQ",,.F.), "Sim" , "N�o" ) 
Private cFS_VLDPRV	:= If( GetMv("cFS_VLDPRV",,.F.), "Sim" , "N�o" )

Private cFS_TESSAI	:= Padr(GetMv("FS_TESSAI",,""),3)
Private oGetDados	:= Nil

// Prepara aHeader
CriaHeader(aHeadGetD,"SLG",aCposGD)

// Prepara aCols com todas as impressoras cadastradas
IF SLG->( DbSeek( xFilial("SLG") ) )
	While SLG->( !Eof() .And. LG_FILIAL == xFilial("SLG") )
	
		Aadd(aColsGetD,Array(Len(aHeadGetD)+1))
			
		For i := 1 To Len(aHeadGetD)
			IF aHeadGetD[i][10] == "V"
				SX3->( DbSetOrder(2) )
				SX3->( DbSeek( aHeadGetD[i][2] ) ) 				
				aColsGetD[Len(aColsGetD)][i]	:= &(AllTrim(SX3->X3_INIBRW))
			Else
				aColsGetD[Len(aColsGetD)][i]	:= SLG->&(AllTrim(aHeadGetD[i][2]))
			Endif
		Next i
		
		aColsGetD[Len(aColsGetD)][Len(aHeadGetD)+1] := .F.
		SLG->( DbSkip() )
	
	End
Endif


oWizard := APWizard():New( "Importa��o de Cupons Fiscais CAT52 - Filial " + SM0->M0_CODFIL , "Informe abaixo os parametros para a importa��o dos cupons fiscais a partir dos arquivos CAT52" , "Configurador de ambiente" ,, {|| .T.}, {|| .F.}, .T.,,, .T., {0,0,450,650} )

@ 002,005 SAY "Diret�rio de leitura dos arquivos no formato CAT52" OF oWizard:GetPanel(1) PIXEL 

@ 011,005 GET cFS_DIR52 	SIZE 200,009 OF oWizard:GetPanel(1) PIXEL

@ 025,005 SAY "Diret�rio a ser gravado os arquivos processados com erro" OF oWizard:GetPanel(1) PIXEL 

@ 034,005 GET cFS_ERROS52  	SIZE 200,009 OF oWizard:GetPanel(1) PIXEL

@ 048,005 SAY "Diret�rio a ser gravado os arquivos lidos" OF oWizard:GetPanel(1) PIXEL

@ 057,005 GET cFS_LIDOS52 	SIZE 200,009 OF oWizard:GetPanel(1) PIXEL 

@ 071,005 SAY "Vers�o do Layout da Portaria CAT52" OF oWizard:GetPanel(1) PIXEL

@ 080,005 GET cFS_VCAT52 	SIZE 030,009 OF oWizard:GetPanel(1) PIXEL


CREATE PANEL oWizard HEADER "Valida��o das informa��es" MESSAGE "Informe abaixo os parametros relacionados ao processo de valida��o dos cupons fiscais"; 
	BACK {|| .T.} NEXT {|| .T.} FINISH {|| .F.} PANEL
	

@ 002,005 SAY "Cadastramento autom�tico do produto caso n�o exista" OF oWizard:GetPanel(2) PIXEL 

@ 011,005 COMBOBOX oAutoSB1 VAR cFS_AUTOB1 ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL

@ 025,005 SAY "Utiliza TES padr�o caso a TES no produto esteja em branco" OF oWizard:GetPanel(2) PIXEL 

@ 034,005 COMBOBOX oAutoTES VAR cFS_AUTOTES ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL

@ 048,005 SAY "Cadastra Aliquota de ICMS e ISS no produto caso esteja em branco" OF oWizard:GetPanel(2) PIXEL

@ 057,005 COMBOBOX oAutoALQ VAR cFS_AUTOALQ ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL

@ 071,005 SAY "TES de venda padr�o"   OF oWizard:GetPanel(2) PIXEL

@ 080,005 MSGET oTESSAI VAR cFS_TESSAI  F3 "SF4" VALID ( IIF( Left(cFS_AUTOTES,1) == "S" , ExistCpo("SF4") .And. M->cFS_TESSAI >= "500" , .T. ) ) SIZE 040,009  WHEN ( Left(cFS_AUTOTES,1) == "S" ) OF oWizard:GetPanel(2) PIXEL

@ 094,005 SAY "Cria almoxarifado inicial automaticamente" OF oWizard:GetPanel(2) PIXEL 

@ 103,005 COMBOBOX oAutoSB2 VAR cFS_AUTOB2 ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL

@ 117,005 SAY "Cadastramento autom�tico da tabela de pre�os caso n�o exista" OF oWizard:GetPanel(2) PIXEL 

@ 126,005 COMBOBOX oAutoSB0 VAR cFS_AUTOB0 ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL

@ 140,005 SAY "Rejeita produtos sem  pre�o de venda do produto" OF oWizard:GetPanel(2) PIXEL 

@ 149,005 COMBOBOX oVlPreco VAR cFS_VLDPRV ITEMS aCombo SIZE 040,009 OF oWizard:GetPanel(2) PIXEL



CREATE PANEL oWizard HEADER "Processamento e mem�ria" MESSAGE "As informa��es abaixo ser�o utilizadas para a otimiza��o de processamento e mem�ria" ;
	BACK {|| .T.} NEXT {|| .T.} FINISH {|| .F. } PANEL
	
@ 002, 005 CHECKBOX oUsaMT VAR lUsaMT PROMPT "Utilizar processamento Multi-Threads " OF oWizard:GetPanel(3);
	SIZE 150, 009 PIXEL ON CHANGE ( oUsaMT:SetFocus(),  )

@ 016,015 SAY "N�mero de threads para o processo de importa��o de arquivos" OF oWizard:GetPanel(3) PIXEL 

@ 025,015 GET nFS_NTIMP PICTURE "9999" SIZE 030,009 OF oWizard:GetPanel(3) PIXEL WHEN lUsaMT

@ 039,015 SAY "N�mero de threads para o processo de valida��o dos registros" OF oWizard:GetPanel(3) PIXEL

@ 048,015 GET nFS_NTVLD PICTURE "9999" SIZE 030,009 OF oWizard:GetPanel(3) PIXEL WHEN lUsaMT

@ 062,015 SAY "N�mero de threads para o processo de gera��o de or�amentos" OF oWizard:GetPanel(3) PIXEL

@ 071,015 GET nFS_NTORC PICTURE "9999" SIZE 030,009 OF oWizard:GetPanel(3) PIXEL WHEN lUsaMT

@ 084,015 SAY "N�mero de threads para o processo de efetiva��o das vendas" OF oWizard:GetPanel(3) PIXEL

@ 093,015 GET nFS_NTVEN PICTURE "9999" SIZE 030,009 OF oWizard:GetPanel(3) PIXEL WHEN .F.  // Nao disponivel multi thread nesta versao


CREATE PANEL oWizard HEADER "Equipamentos Fiscais - Filial " + SM0->M0_CODFIL MESSAGE "Informe o n�mero de s�rie do equipamento fiscal em cada esta��o desta filial" ;
	BACK {|| .T.} NEXT {|| .F.} FINISH {|| IIF( U_WiTudoOk() , lSave := MsgYesNo("Confirma a grava��o das configura��es para esta filial ?" ) , lSave := .F. ) } PANEL 
	
oGetDados := MsNewGetDados():New(05,7,153,320,nOpcGetD,"u_WiLinok()","u_WiTudoOk()",/*"+_ITEM"*/,,,50000,,,/*"u_PsLinDel20()"*/,oWizard:GetPanel(4),aHeadGetD,aColsGetD)	

ACTIVATE WIZARD oWizard CENTERED

If lSave

	// Cria estruturas de diretorio	
	MontaDir(cFS_DIR52)
	MontaDir(cFS_ERROS52)
	MontaDir(cFS_LIDOS52)
	
	// Grava��o e atualiza��o dos parametros
	If !PutMV("FS_TESSAI",cFS_TESSAI)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_TESSAI"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := "Tes de Saida padrao vendas CAT52"
		MsUnLock()
		PutMV("FS_TESSAI",cFS_TESSAI)
	Endif

	If !PutMV("FS_DIR52",cFS_DIR52)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := xFilial("SX6")
			SX6->X6_VAR     := "FS_DIR52"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := "Pasta de leitura dos arquivos"
		MsUnLock()
		PutMV("FS_DIR52",cFS_DIR52)
	Endif
	
	If !PutMV("FS_ERROS52",cFS_ERROS52)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := xFilial("SX6")
			SX6->X6_VAR     := "FS_ERROS52"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := "Pasta de arquivos com erro"
		MsUnLock()
		PutMV("FS_ERROS52",cFS_ERROS52)
	Endif	

	If !PutMV("FS_LIDOS52",cFS_LIDOS52)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := xFilial("SX6")
			SX6->X6_VAR     := "FS_LIDOS52"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := "Pasta de arquivos processados"
		MsUnLock()
		PutMV("FS_LIDOS52",cFS_LIDOS52)
	Endif
	
	If !PutMV("FS_VCAT52",cFS_VCAT52)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_VCAT52"
			SX6->X6_TIPO    := "C"
			SX6->X6_DESCRIC := "Versao do layout CAT52"
		MsUnLock()
		PutMV("FS_VCAT52",cFS_VCAT52)
	Endif	
	 
	If !PutMV("FS_VLDPRV",Left(cFS_VLDPRV,1)=="S")
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_VLDPRV"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Rejeita prod sem preco de venda CAT52"
		MsUnLock()
		PutMV("FS_VLDPRV",Left(cFS_VLDPRV,1)=="S")
	Endif	

	If !PutMV("FS_AUTOSB0",Left(cFS_AUTOB0,1)=="S")
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_AUTOSB0"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Inclui tab de pre�o do arquivo CAT52"
		MsUnLock()
		PutMV("FS_AUTOSB0",Left(cFS_AUTOB0,1)=="S")
	Endif	
	
	If !PutMV("FS_AUTOSB1",Left(cFS_AUTOB1,1)=="S")
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_AUTOSB1"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Inclui produto do arquivo CAT52"
		MsUnLock()
		PutMV("FS_AUTOSB1",Left(cFS_AUTOB1,1)=="S")
	Endif	
	
	If !PutMV("FS_AUTOSB2",Left(cFS_AUTOB2,1)=="S")
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_AUTOSB2"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Cria almoxarifado inicial CAT52"
		MsUnLock()
		PutMV("FS_AUTOSB2",Left(cFS_AUTOB2,1)=="S")
	Endif	

	If !PutMV("FS_AUTOTES",Left(cFS_AUTOTES,1)=="S")
		RecLock("SX6",.T.)
		
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_AUTOTES"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Usa TES padrao em produtos s/ TES"
		MsUnLock()
		PutMV("FS_AUTOTES",Left(cFS_AUTOTES,1)=="S")
	Endif	

	If !PutMV("FS_AUTOALQ",Left(cFS_AUTOALQ,1)=="S")
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_AUTOALQ"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Atualiza a Aliq CAT52 nos produtos"
		MsUnLock()
		PutMV("FS_AUTOALQ",Left(cFS_AUTOALQ,1)=="S")
	Endif
	
	If !PutMV("FS_CATMT",lUsaMT)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_CATMT"
			SX6->X6_TIPO    := "L"
			SX6->X6_DESCRIC := "Indica se usa Multi-Threads CAT52"
		MsUnLock()
		PutMV("FS_CATMT",lUsaMT)
	Endif
	
	If !PutMV("FS_NTIMP",nFS_NTIMP)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_NTIMP"
			SX6->X6_TIPO    := "N"
			SX6->X6_DESCRIC := "No Threads ao ler arquivos CAT52"
		MsUnLock()
		PutMV("FS_NTIMP",nFS_NTIMP)
	Endif	
	
	If !PutMV("FS_NTVLD",nFS_NTVLD)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_NTVLD"
			SX6->X6_TIPO    := "N"
			SX6->X6_DESCRIC := "No Threads ao validar regs CAT52"
		MsUnLock()
		PutMV("FS_NTVLD",nFS_NTVLD)
	Endif	
	
	If !PutMV("FS_NTORC",nFS_NTORC)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_NTORC"
			SX6->X6_TIPO    := "N"
			SX6->X6_DESCRIC := "No Threads ao gerar orcamentos"
		MsUnLock()
		PutMV("FS_NTORC",nFS_NTORC)
	Endif	

	If !PutMV("FS_NTVEN",nFS_NTVEN)
		RecLock("SX6",.T.)
			SX6->X6_FIL     := ""
			SX6->X6_VAR     := "FS_NTVEN"
			SX6->X6_TIPO    := "N"
			SX6->X6_DESCRIC := "No Threads ao efetivar orctos"
		MsUnLock()
		PutMV("FS_NTVEN",nFS_NTVEN)
	Endif
	

	Begin Transaction
		
	// Gravacao do cadastro de estacoes	
	nPosEstacao	:= Ascan( oGetDados:aHeader,{ |x|Trim(x[2])=="LG_CODIGO" } )
	
	For nX := 1 To Len(oGetDados:aCols)
	
		lCria := SLG->( DbSeek( xFilial("SLG") + oGetDados:aCols[nX,nPosEstacao] ) )
	
		IF !oGetDados:aCols[nX,Len(oGetDados:aHeader)+1]
		
			SLG->( RecLock("SLG", !lCria ) )
			
			If lCria
				SLG->LG_FILIAL	:= xFilial("SLG")
			Endif
			
			For i := 1 To Len(oGetDados:aHeader)				
				IF SLG->( FieldPos(oGetDados:aHeader[i,2]) ) <> 0
					SLG->( FieldPut( FieldPos(oGetDados:aHeader[i,2]) , oGetDados:aCols[nX,i] ) )
				Endif				
			Next i

			SLG->( MsUnLock() )
		
		Else

			If !lCria
				SLG->( RecLock("SLG", lCria ) )
				SLG->( DbDelete() )
				SLG->(MsUnlock())
			Endif		
		
		Endif
	
	Next nX
	
	End Transaction 
		
EndIf
		
Return






/*/{Protheus.doc} CriaHeader
Prepara o aHeader para a MsNewGetdados

@author Mauro Paladini
@since 18/03/2015
@version 1.0
/*/

Static Function CriaHeader(aHeadGetD,cAliasGD,aCposGD)

	Local aArea		:= GetArea()
	Local lField	:= .F.
	Local lDelField	:= .F.

	Default aCposGD		:= {}
	
	lField 		:= !Empty(aCposGD)

	SX3->(dbSetOrder(1))
	SX3->(dbSeek(cAliasGD))

	While SX3->(!Eof()) .And. SX3->X3_ARQUIVO == cAliasGD

		If lField .And. Ascan(aCposGD,RTrim(SX3->X3_CAMPO)) == 0
			SX3->( DbSkip() )
			Loop
		Endif

		Aadd(aHeadGetD,	{	Trim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,;
							SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
							SX3->X3_USADO,SX3->X3_TIPO,;
							SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOXSPA,,SX3->X3_WHEN,	;
							SX3->X3_VISUAL,SX3->X3_VLDUSER, SX3->X3_PICTVAR,SX3->X3_OBRIGAT	})

		SX3->(dbSkip())

	EndDo

	RestArea(aArea)

Return



/*/
WiTudoOk()
Faz a valida��o do TudoOk da GetDados

@author Mauro Paladini
@since 10/08/2015
@version 1.0
/*/

User Function WiTudoOk()

Local lRet		:= .T.
Local nX		:= 0
Local aArea		:= GetArea()
Local nAt		:= oGetDados:nAt

For nX := 1 To Len(oGetDados:aCols)

	If lRet 	
		lRet := u_WiLinok( nX )
	Endif

Next nX

RestArea(aArea)

Return lRet



/*/
WiLinok()
Faz a valida��o da linha na GetDados para n�o permitir duplicidades

@author Mauro Paladini
@since 10/08/2015
@version 1.0
/*/

User Function WiLinok( nAt )

Local lRet		:= .T.
Local nX		:= 0
Local aArea		:= GetArea()

Local nPosPDV	:= aScan( oGetDados:aHeader, { |x| AllTrim( x[2] ) == "LG_PDV" } )
Local nPosCod	:= aScan( oGetDados:aHeader, { |x| AllTrim( x[2] ) == "LG_CODIGO" } ) 
Local nPosSerie	:= aScan( oGetDados:aHeader, { |x| AllTrim( x[2] ) == "LG_SERIE"  } )
Local nPosNumS	:= aScan( oGetDados:aHeader, { |x| AllTrim( x[2] ) == "LG_SERPDV" } ) 

Default nAt		:= oGetDados:nAt

// Valida campos obrigatorios

IF Empty(AllTrim( oGetDados:aCols[nAt,nPosPDV] ))
	lRet := .F.
	Help(" ",1,"OBRIGAT2",,AllTrim(RetTitle(oGetDados:aHeader[nPosPDV,2])),3,1)
Endif

If lRet .And. Empty(AllTrim( oGetDados:aCols[nAt,nPosCod] ))
	lRet := .F.
	Help(" ",1,"OBRIGAT2",,AllTrim(RetTitle(oGetDados:aHeader[nPosCod,2])),3,1)
Endif

If lRet .And. Empty(AllTrim( oGetDados:aCols[nAt,nPosSerie] ))
	lRet := .F.
	Help(" ",1,"OBRIGAT2",,AllTrim(RetTitle(oGetDados:aHeader[nPosSerie,2])),3,1)
Endif

If lRet .And. Empty(AllTrim( oGetDados:aCols[nAt,nPosNumS] ))
	lRet := .F.
	Help(" ",1,"OBRIGAT2",,AllTrim(RetTitle(oGetDados:aHeader[nPosNumS,2])),3,1)
Endif


// Valida Duplicidades

If lRet

	For nX := 1 To Len(oGetDados:aCols)
	
		IF nX <> nAT .And. !oGetDados:aCols[nX,Len(oGetDados:aHeader)+1] 		
			
			IF oGetDados:aCols[nX,nPosPDV] == oGetDados:aCols[nAT,nPosPDV]			
				Help(" ",1,".DUPLIC.",,"PDV duplicado",3,1)
				lRet := .F.
			Endif
			
			IF lRet .And. oGetDados:aCols[nX,nPosCod] == oGetDados:aCols[nAT,nPosCod]			
				Help(" ",1,".DUPLIC.",,"Estacao duplicada",3,1)
				lRet := .F.
			Endif			
			
			IF lRet .And. oGetDados:aCols[nX,nPosSerie] == oGetDados:aCols[nAT,nPosSerie]			
				Help(" ",1,".DUPLIC.",,"S�rie duplicada",3,1)
				lRet := .F.
			Endif			

			IF lRet .And. oGetDados:aCols[nX,nPosNumS] == oGetDados:aCols[nAT,nPosNumS]			
				Help(" ",1,".DUPLIC.",,"N�mero de s�rie duplicado",3,1)
				lRet := .F.
			Endif			

		Endif
	
	Next nX

Endif

RestArea(aArea)

Return lRet