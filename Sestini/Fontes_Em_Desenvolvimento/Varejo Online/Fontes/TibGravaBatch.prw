#INCLUDE "PROTHEUS.CH" 
#INCLUDE "TBICONN.CH"
#INCLUDE "LOJXFUNC.CH"
#INCLUDE "CRDDEF.CH"

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE COMERRO   	"8"
#DEFINE EMPROCESSA	"9"


//��������������������������������������������������������?
//| Defines utilizados para trata a matriz de parcelas    |               
//��������������������������������������������������������?

#DEFINE __DATA		1        //Data de vencimento da parcela
#DEFINE __VALOR		2        //Valor da parcela
#DEFINE __FORMA		3        //Forma de recebimento
#DEFINE __ADMINIS	4        //Codigo ou nome da Administradora
#DEFINE __NUMCART	5        //Numero do cartao ou cheque
#DEFINE __AGENCIA	6        //Agencia do cheque
#DEFINE __CONTA		7        //Numero da conta do cheque 
#DEFINE __RG		8        //RG do portador do cheque 
#DEFINE __TELEFON	9        //Telefone do portador do cheque
#DEFINE __TERCEIR	10       //Indica se o cheque eh de terceiros
#DEFINE __MOEDA  	11       //Moeda da parcela 
#DEFINE __PARCTEF  12       //Tipo de parcelamento quando venda TEF(Client SiTEF DLL)
#DEFINE __ACRSFIN 	13		 //Valor separado do acrescimo para cada parcela de financiamento
#DEFINE __EMITENT 	14		 //Emitente quando o cheque for de terceiros
#DEFINE __FORMAID 	15		 //ID do Cartao de Credito ou Debito
#DEFINE __NSUTEF	16		 //NSU da trasacao TEF
#DEFINE __DOCTEF   	17		 //Num. Documento TEF
#DEFINE __FORMATEF	"CC;CD" //Formas de pagamento que utilizam opera��o TEF para valida��o
#DEFINE TEF_NAO_USADO				"1"		//Nao Utiliza TEF
#DEFINE TEF_SEMCLIENT_DEDICADO  	"2"     //Utiliza TEF Dedicado Troca de Arquivos                      
#DEFINE TEF_COMCLIENT_DEDICADO  	"3"		//Utiliza TEF Dedicado com o Client
#DEFINE TEF_DISCADO             	"4"		//Utiliza TEF Discado 
#DEFINE TEF_LOTE                	"5"		//Utiliza TEF em Lote
#DEFINE TEF_CLISITEF				"6"		//Utiliza a DLL CLISITEF
#DEFINE TEF_CENTROPAG				"7"		//Utiliza a DLL CENTRO DE PAGOS 
#DEFINE CTRL Chr(10)+Chr(13)              	//Pula linha

/*
	Tipos do LogManager
*/
#define LMOK 1								
#define LMINFO 2
#define LMALERT 3
#define LMSTOP 4
#define LMCONFIG 5

/*
	Niveis do LogManager
*/
#DEFINE LMTECH 1
#DEFINE LMPROC 2

//������������������������������������������������������������������������������������������Ŀ
//?ste Log e um recurso a ser habilitado pelo departamento de desenvolvimento para averigua-?
//?ao de possiveis problemas de transacoes TEF.                                             ?
//��������������������������������������������������������������������������������������������
#DEFINE LOG_TEF LjLogTef()

//��������������������������������������������������������?
//| Defines utilizados para tratar o Array de Totais      |
//��������������������������������������������������������?
#DEFINE __VALMERC 1
#DEFINE __DESCONT 2
#DEFINE __VALACRS 3
//��������������������������������������Ŀ
//| Defines utilizados para EventViewer |
//����������������������������������������
#DEFINE IDEVENT "050"

//��������������������������������������������������������������Ŀ
//?efines utilizados para distiguir geracao de guia de despacho ?
//����������������������������������������������������������������
#DEFINE GDP_NO			"0" 	//Nao sera gerada guia de despacho.Quando a venda n? tem item reservado com entrega do tipo 3 - Release 11.5 -  Chile - F2CHI
#DEFINE GDP_PARCIAL    	"1"		//Sera gerada guia de despacho parcial.Quando a venda possui pelo menos um item reservado com entrega do tipo 3 - Release 11.5 -  Chile - F2CHI
#DEFINE GDP_TOTAL		"2"		//Sera gerada guia de despacho total.Quando todos os itens da venda estao reservados e com entrega do tipo 3 - Release 11.5 -  Chile - F2CHI


Static cClientDir           					// Pasta de instalacao do Remote
Static lRemoteType 	:= GetRemoteType() <> -1		// Verifica a origem (Front ou Venda Assistida)
Static lArqLog				                    // Verifica se o arquivo de log ja' foi criado
Static lLogHabilitado							// Verifica se o log esta' habilitado
//��������������������������������������?
//?ariaveis utilizada no SELF-LIQUIDATE?
//��������������������������������������?
Static aPremio			:= {}  				   	// Array com os premios resgatados pelo cliente				
Static oIntVenda  	:= Nil                        // Responsavel por integrar venda
Static lR5			:= GetRpoRelease() >= "R5"		//Indica se o release e 11.5
Static lLogManager  := FindFunction("LoggerOut")
Static aTESVenda 	:= { "", {} }

//Vale Presente
Static cTipoPrdVP 	:= ""				//Armazena se o tipo de produto que estah sendo vendido na venda eh "Vale Presente" ("S") ou NAO ("N"). Pois "Vale Presente" nao pode ser vendido junto com outros produtos.
Static lVPNewRegra 	:= Nil				//Variavel para identificar se utiliza ou nao as novas modificacoes da implementacao de Vale Presente, para imprimir o comprovante nao fiscal na venda de vale presente

//��������������������������������������������������������������������?
//?ariaveis referente a implementacao de Cartao Presente (Gift Card).?
//��������������������������������������������������������������������?
Static lCPAtivo		:= .F.				// Verifica a configuracao de "Cartao Presente (Gift Card)" esta ativo.
Static aProdsCP   	:= {}				// Array com a relacao de produtos considerados "Cartao Presente (Gift Card)"
Static lTrnCPEfet	:= .F.				// Indica se na venda foi efetuada alguma recarga de "Cartao Presente (Gift Card)"
Static aRegTefCP	:= {} 				// Array com as movimentacoes do TEF da recarga de "Cartao Presente (Gift Card)"
Static cTipoPrdCP 	:= ""				// Armazena se o tipo de produto que estah sendo vendido na venda eh "recarga de Cartao Presente (Gift Card)" ("S") ou NAO ("N"). Pois "recarga de Cartao Presente" nao pode ser vendido junto com outros produtos.
Static lGiftCard 	:= Nil				// Variavel para verificar se estah tudo OK para utilizar a implementacao de Cartao Presente (Gift Card)


/*
����������������������������������������������������������������������������?
����������������������������������������������������������������������������?
�������������������������������������������������������������������������Ŀ�?
��?un��o    ?jGrvBatch?Autor ?Vendas Clientes       ?Data ?22/09/00 ��?
�������������������������������������������������������������������������Ĵ�?
��?escri��o ?rava em processo batch o com base no SL1, SL2, SL4.        ��?
�������������������������������������������������������������������������Ĵ�?
��?Uso      ?ront Loja / Siga Loja                                   	  ��?
��������������������������������������������������������������������������ٱ?
����������������������������������������������������������������������������?
����������������������������������������������������������������������������?
*/
User Function TibGravaBatch( cEmp, cFilTrab , cIntervalo, cMinReproc, cNumOrc, lGravacao )

Local nHandle						   			   				// Indica se o arquivo foi criado
Local aFiles		:= {}						   				// Arquivos
Local nIntervalo 	:= 0			   			   				// Intervalo para o Loop
Local nTimes	 	:= 0			   							// Numero de loop antes de entrar no while		
Local lRetValue  	:= .T.             			   				// Retorno da fun��o 
Local aFiliais   	:= {}              			   				// Filiais
Local aBadRecno  	:= {}			   			   				// Recnos 
Local cFileName		:= ""			   			   				// Nome do arquivo 
Local nCount 		:= 1						   				// Contador
Local cTemp			:= ""			   			   				// Temporario
Local lTemReserva	:= .F.						   				// Verifica se existe algum item com reserva
Local lProcessou	:= .F.			   			   				// Verifica se processou as vendas na Retaguarda.
Local bOldError  								   				// Bloco de tratamento de erro
Local lLJ7051		:= FindFunction("U_LJ7051")	   				// Verifica se a funcao LJ7051 esta compilada 
Local lExProc 		:= .T.			   			   				// Controla o while do Killapp 
Local lMultFil 		:= .F.			   			   				// Verifica se e' passado mais de uma filial no parametro
Local lCriouAmb		:= .F.			   			   				// Verifica se o PREPARE ENVIRONMENT foi executado
Local nSleep		:= 0			   			   				// Utilizado para atribuicao na variavel nIntervalo 	
Local aAreaSL1		:= {}			   			   				// Guarda a Area do SL1
Local nRecSL1		:= 0			   			   				// Guarda o Recno do SL1
Local lGerInt 		:= .F.             			   				// Verifica se a integracao esta habilitada
Local aRecFail		:= {}			   			   				// Registros que nao conseguiram ser travados
Local oLJCLocker 	:= Nil
Local lLj7064       := ExistBlock("LJ7064") 	   				// Verifica se existe o ponto de entrada LJ7064
Local nOpcProc		:= 0			   			   				// Opcao de processamento
Local lLstPresAt	:= .F.              		   				// SuperGetMV("MV_LJLSPRE",.F.,.F.) .AND. IIf(FindFunction("LjUpd78Ok"),LjUpd78Ok(),.F.)	//Lista de presente ativa?
Local lMvLjGrvBt	:= .F. 						   				// Parametro que define se utilizara o indice "14" para priorizar a integracao dos orcamentos com reserva.
Local lFTVD7051		:= FindFunction("U_FTVD7051")  				// Verifica se a funcao LJ7051 esta compilada 
Local lFtvdVer12	:= FindFunction("LjFTvd") .AND. LjFTVD() 	// Verifica se ?Release 11.7 e o FunName ?FATA701 - Compatibiliza��o Venda Direta x Venda Assisitida
Local cNomeProg		:= Iif(lFtvdVer12,"FATA701","LOJA701") 		// Nome da Rotina
Local lLj843GrvMv   := .F.
Local lTPLOtica 	:= .F.
Local lLj7AtuInte 	:= .F.
Local lMvLjOffLn 	:= .F.
Local lUsaInd14 	:= .F. 										// Indica se usa o indice 14 da tabela SL1 para priorizar os orcamentos com pedido.
Local lGrvEstorn	:= .F. 										// Indica se a tabela MBZ e funcao de gravacao de estorno existem na base
Local lLOJA0051		:= .F.
Local cMvLJILJLO 	:= ""
Local cCliPad		:= ""										// Cliente Padrao
Local cLojaPad		:= ""										// Loja Padrao
Local nMinReproc	:= 0										// Utilizado para atribuicao na variavel cMinReproc
Local nMinFalha		:= 0										// Tempo da ultima falha de processamento
Local lReproc		:= .T.										// Sinaliza se deve marcar como registro ja reprocessado, somente quando utiliza cMinReproc
Local xRet140Exc	:= Nil										// Retorno da Rotina de cancelamento automatico NFC-e
Local lLj7082		:= ExistBlock("LJ7082")						// Indica se o PE LJ7082 esta compilado
Local lRetLj7082	:= .F.										// Retorno do PE LJ7082
Local aRecnoNFCe	:= {}										// vetor com R_E_C_N_O_ dos orcamentos com NFC-e que deverao ser cancelados
Local nI			:= 0


Default cIntervalo  	:= 30000								// Conteudo do terceiro parametro (Parm3 do mp8srv.ini)
Default cMinReproc		:= 1000									// Conteudo do quarto parametro, para tentar processar novamente o registro quando ocorre falha por Lock(SA1)
Default cNumOrc			:= ""

//��������������������������������������������������������������Ŀ
//?Variavel Private para que caso seja execultada a funcao      ?
//?via JOB, atribua o valor padrao a nMoedaCor.				 ?
//?Rotina Utilizada para o Padrao, Lista de Presentes e Localiza?
//?cao Chile/Colombia.                                          ?
//����������������������������������������������������������������
Private nMoedaCor 	:= 1
Private aNCCItens   := {}
Private nNccUsada   := 0
Private nNccGerada  := 0

//����������������������������������������������������������?
//?ratamento caso o terceiro parametro seja passado ou nao.?
//����������������������������������������������������������?
If ValType(cIntervalo) <> "N"
	nSleep := Val(cIntervalo)
Else               
	nSleep := cIntervalo	
Endif
//����������������������������������������������������������?
//?ratamento caso o quarto parametro seja passado ou nao.  ?
//����������������������������������������������������������?
If ValType(cMinReproc) <> "N"
	nMinReproc := Val(cMinReproc)
Else               
	nMinReproc := cMinReproc	
Endif

While nCount <= Len( cFilTrab )
	
	cTemp := ""
	While SubStr( cFilTrab, nCount, 1 ) <> "," .AND. nCount <= Len( cFilTrab )
		cTemp += SubStr( cFilTrab, nCount, 1 )
		nCount++
	End
	AADD( aFiliais, { cTemp } )
	nCount++
	
End

nCount := 1

//����������������������������������������������������?
//?erifica o numero de filiais que esta sendo passado?
//����������������������������������������������������?
If Len(aFiliais) > 1
	lMultFil := .T. 
Endif

//��������������������������������������������������������������������������������������������������������������������������?
//?az a inicializacao de algumas variaveis para verificar qual o Release utilizado e/ou a Existencia de Functions no RPO.  ?
//?az esta inicializacao aqui para executar apenas uma vez, ao inves de executar varias vezes no Loop da SL1.              ?
//��������������������������������������������������������������������������������������������������������������������������?
lLj843GrvMv := FindFunction("Lj843GrvMv")	//Verifica se a funcao existed
lLj7AtuInte := FindFunction("Lj7AtuInte")	//Verifica se a funcao existe
lGrvEstorn  := FindFunction("Lj601GrPDV") 	//Checa se a funcao de gravacao de estorno existe
lLOJA0051 	:= FindFunction("LOJA0051")		//Verifica se a funcao existe 
lR5 		:= GetRpoRelease() >= "R5"
lCriouAmb 	:= .T.

//������������������������������������������������������������������������������������������������������������?
//?az a inicializacao de variaveis de PARAMETROS que serao utilizadas no Loop da SL1.                        ?
//?az esta inicializacao aqui para executar apenas uma vez, ao inves de executar varias vezes no Loop da SL1.?
//������������������������������������������������������������������������������������������������������������?

cCliPad		:= SuperGetMV("MV_CLIPAD")				// Cliente Padrao
cLojaPad	:= SuperGetMV("MV_LOJAPAD")				// Loja Padrao
lGerInt 	:= SuperGetMv("MV_LJGRINT",.F.,.F.) 	// Verifica se a integracao esta habilitada
lLstPresAt	:= SuperGetMV("MV_LJLSPRE",.F.,.F.) .AND. IIf(FindFunction("LjUpd78Ok"),LjUpd78Ok(),.F.)
lMvLjGrvBt 	:= SuperGetMv("MV_LJGRVBT",.F.,.F.) 	// Se o parametro for .F. nao utiliza o indice "14" da SL1
lUsaInd14 	:= lMvLjGrvBt .AND. AllTrim(SL1->(IndexKey(14))) = "L1_FILIAL+L1_SITUA+L1_STATUS" //Indica se usa o indice 14 da tabela SL1 para priorizar os orcamentos com pedido.
lTPLOtica 	:= HasTemplate("OTC") 					// Verifica se eh Template Otica
lMvLjOffLn  := SuperGetMv("MV_LJOFFLN", Nil, .F.) 	// Identifica se o ambiente esta operando em offline
lGrvEstorn  := lGrvEstorn .And. AliasInDic("MBZ") 	// Checa se a tabela MBZ e fun��o de gravacao de estorno existem na base
cMvLJILJLO  := SuperGetMV( "MV_LJILJLO",,"2" )    	// Se utilizar?sistema de travas nos Jobs FRTA020, LOJA1115 e LJGrvBatch. (0=N?, 1=Sim)

	cFileName := cEmp + aFiliais[ nCount ][1]
	
	If (!lMultFil .AND. lCriouAmb) .OR. ( nHandle := MSFCreate("LJGR"+cFileName+".WRK") ) >= 0
		

		ConOut("LJGrvBatch: "+STR0075 + cEmp + STR0076 + aFiliais[ nCount ][1])  // "Empresa:" ### " Filial:"
		ConOut("            "+STR0021)  //"Iniciando processo de gravacao batch..."
				
		If lLOJA0051 .And. cMvLJILJLO == "1"
			oLJCLocker := LJCGlobalLocker():New()
			If !oLJCLocker:GetLock( "LOJXFUNCILLock" )
				Return
			EndIf
		EndIf
		
		//������������������������������������������������������������������������������?
		//?odos os arquivos devem ser abertos antes de entrar no Begin Transaction.    ?
		//?aso exista customizacao, os arquivos devem ser abertos neste PE.            ?
		//?BS: O ADSSERVER nao permite uso da ChkFile() dentro de um Begin Transaction.?
		//?m outros ambientes, este problema nao ocorre.                               ?
		//?etornar um array, por exemplo {"SZ1", "SZ2"}                                ?
		//������������������������������������������������������������������������������?
		If !lFtvdVer12
			If ExistBlock("LJGRVOPEN")
				aFiles := ExecBlock("LJGRVOPEN", .F., .F.)
				RPCOpenTables(aFiles)
			EndIf
		ElseIf lFtvdVer12
			If ExistBlock("FTVDGRVOPEN")
				aFiles := ExecBlock("FTVDGRVOPEN", .F., .F.)
				RPCOpenTables(aFiles)
			EndIf
		EndIf
		
		//��������������������������������������������������������������Ŀ
		//?rocessar a transferencia de caixa automatica, se necessario. ?
		//?sse processamento deve chamar independente se existe SL1 para?
		//|explorir ou nao, no entanto foi incluido fora do Loop da SL1. ?
		//����������������������������������������������������������������
		If GetNewPar("MV_LJTRANS",.F.)
			If FindFunction("LjVerTrans")
				LjVerTrans()
			EndIf
		EndIf		
		
		SL1->( DbSetOrder(1) )		
		IF SL1->( DbSeek( xFilial("SL1") + cNumOrc ) ) .And. SL1->L1_SITUA == "RX"
		
			While SL1->( L1_FILIAL+L1_NUM == xFilial("SL1")+cNumOrc ) 
			
                aNCCItens   := {}
                nNccUsada   := 0
                nNccGerada  := 0
									
				If ( nPos := Ascan( aBadRecno, SL1->( Recno() ) ) ) > 0
					While SL1->( L1_FILIAL+L1_NUM == xFilial("SL1")+cNumOrc )  .AND. ( Ascan( aBadRecno, SL1->( Recno() ) ) > 0 )
						SL1->( DbSkip() )
					End
				EndIf
	
				//������������������������������������������������������������������������������Ŀ
				//?Executa o PE, que verifica se a venda sera processada ou nao pelo LjGrvBatch.?
				//��������������������������������������������������������������������������������
				If lLj7082
					lRetLj7082 := ExecBlock("LJ7082",.F.,.F.)
					If !lRetLj7082
						SL1->( DbSkip() )
						Loop
					EndIf
				EndIf
	
				//������������������������������������������������������������������������������?
				//?Verifica se o cliente esta alocado para outro usuario , caso esteja coloca  ?
				//?esse registro na fila novamente,apos cinco tentativas grava o mesmo como ER ?
				//������������������������������������������������������������������������������?
				dbSelectArea("SA1")
				SA1->(DbSetOrder(1))
				
				If SA1->(MsSeek(xFilial("SA1")+SL1->L1_CLIENTE+SL1->L1_LOJA))
				
					If cCliPad+cLojaPad <> SA1->A1_COD+SA1->A1_LOJA
					
						If SA1->(Rlock())
							SA1->(MsUnlock())
						Else
							nPos := aScan( aRecFail,{|x| x[1] ==  SL1->(Recno()) })
							If nPos == 0
								aAdd(aRecFail,{ SL1->(Recno()) , 1, TIME() } )
							Else
								Conout(Chr(13) + Chr(10)+"LJGrvBatch: " + TIME() + " " + STR0079 + aFiliais[ nCount ][1] + "."+STR0146+; // "Filial " #"Registro alocado,Cliente:"
													SL1->L1_CLIENTE + STR0147 + SL1->L1_LOJA ) // " Loja:"#" 
								
								If aRecFail[nPos][2] > 5
									LjGravaErr()
									ConOut( Chr(13) + Chr(10)+"LJGrvBatch: " + TIME() + " " + STR0079 + aFiliais[ nCount ][1] + "."+STR0146+;		// "Filial " #"Registro alocado,Cliente:"
											SL1->L1_CLIENTE+ STR0147 +SL1->L1_LOJA + STR0148) // #" Loja:"# " a venda sera gravada como 'ER'."
								Else
									//Verifica se ultima validacao eh inferior ao tempo de reprocessamento							
									If nMinReproc > 0 
										nMinFalha 	:= Val(SubStr( ELAPTIME( aRecFail[nPos][3], TIME() ),4,2))							
										lReproc 	:= IIF(nMinFalha < nMinReproc,.F.,.T.)
									Else
										lReproc := .T.
									EndIf
									
									If lReproc
										aRecFail[nPos][2]++
										aRecFail[nPos][3] := TIME() //Atualiza Ult verificacao
										Conout(Chr(13) + Chr(10)+"LJGrvBatch: " + TIME() + STR0079 + aFiliais[ nCount ][1] + "."+STR0146+; // "Filial " #"Registro alocado,Cliente:"
												SL1->L1_CLIENTE + STR0147 + SL1->L1_LOJA + STR0149) // " Loja:"#" tentara depois processar novamente."
									EndIf
								EndIf	
						EndIf	
						
						SL1->(DbSkip())
						
						EndIf   
						
					EndIf
					
				EndIf
				
				//������������������������������������������������������������������������������?
				//?Protejo situa��o de todos os orcamentos "RX" estarem em aBadRecno, neste    ?
				//?caso n? devo processar o proximo (que eh eof), mas sim abandonar o Loop    ?
				//������������������������������������������������������������������������������?
				If SL1->(Eof()) .OR. SL1->L1_SITUA <> 'RX'
					Return
				EndIf
				
				nOpcProc := 0
				
				//����������������������������������Ŀ
				//?ratamento de lista de presentes  ?
				//������������������������������������
				If lLstPresAt .And. lLj843GrvMv
					nOpcProc := Lj843GrvMv(SL1->L1_NUM)
					//Caso a rotina tenha identificado que existem itens de entrega, alterar a variavel identificadora de reserva
					If nOpcProc == 1
						lTemReserva := .T.
					Endif
				Endif
				
				//������������������������������������������������Ŀ
				//?erifica se os itens foram gravados corretamente?
				//?? grava como reserva, quando Template Otica   ?
				//��������������������������������������������������
				If nOpcProc == 0
					lTemReserva := .F.
					SL2->( DbSetOrder( 1 ) )
					If SL2->( DbSeek( xFilial( "SL2" ) + SL1->L1_NUM ) ) .AND. !lTPLOtica
						//����������������������������������������������Ŀ
						//?Verifica se existe item com Reserva na venda ?
						//������������������������������������������������
						While SL2->L2_FILIAL + SL2->L2_NUM == xFilial( "SL2" ) + SL1->L1_NUM
							If !Empty(SL2->L2_RESERVA) .AND. SL2->L2_ENTREGA <> "2"	//RETIRA
								lTemReserva := .T.
								nOpcProc := 1	//LJ7PEDIDO
								Exit
							Endif
							SL2->(DbSkip())					
						EndDo
					Endif
				Endif		
	
				//����������������������������������������������������������������Ŀ
				//?uando or?mento(filho) possui outro or?mento com reserva,     ?
				//?impa L1_Status para salvar como venda e n? gerar nova reserva.?
				//������������������������������������������������������������������
				If lTemReserva .AND. !Empty(SL1->L1_ORCRES)
					lTemReserva := .F.
					If nOpcProc == 1
						nOpcProc := 2
					Endif
					RecLock("SL1", .F.)
					REPLACE SL1->L1_STATUS WITH ""
					MsUnlock()					
				EndIf
	
				cEstacao  := SL1->L1_ESTACAO			
				aAreaSL1  := SL1->(GetArea())
				nRecSL1	  := SL1->(Recno())
			
				//Caso nao seja processamento de pedido (entrega) e lista de presentes do tipo credito, processar o LjGrvTudo
				If nOpcProc == 0
					nOpcProc := 2
				Endif
				ConOut("LJGrvBatch: nOpcProc = " + cValToChar(nOpcProc))

				Do Case

					//������������?
					//?J7PEDIDO  ?
					//������������?
					Case nOpcProc == 1

						Begin Transaction 
							bOldError := ErrorBlock( {|x| LjVerPedErro(x,lProcessou,aFiliais[nCount][1],nRecSL1) } ) // muda code-block de erro					
							Begin Sequence
								//��������������������������������������Ŀ
								//?Transforma o orcamento para pedido   ?
								//����������������������������������������
								lProcessou := LJ7Pedido(	{} , 2, NIL, .F.,;
															{} , .T. )
								
								If !lProcessou
									UserException("LJGrvBatch: "+ STR0079+ aFiliais[nCount][1] + ". "+STR0137 )// "Filial " ### ". " "Problemas na gera��o do Pedido"
								EndIf	
							End Sequence
							ErrorBlock( bOldError )			
						End Transaction
					
					//������������?
					//?JGRVTUDO  ?
					//������������?
					Case nOpcProc == 2
						

        					lProcessou := 		LjGrvTudo( 	.F.       		,/*lFinanceiro*/   	,/*nNCCUsada*/  	,/*aNCCItens*/	,;
        													/*nNCCGerada*/ 	,/*aImpCheque*/		,/*nMoedaCor*/    ,/*@aRecnoSE1*/ 	,;
        													/*aVlrAcres*/  	,/*aSL1*/	              ,/*aSL2*/		    ,/*cDoc*/			,;
        													/*@lVendaCup*/ 	,/*nNumItens*/	     ,/*nFrete*/		,/*nSeguro*/		,;
        													/*nDespesa*/       ,/*cLQFrete*/		     ,/*aAcrFin*/	     ,NIL				,;
        													/*cCgcCli*/        ,/*cNomeCli*/		     ,/*lNfManual*/	,/*@lExistNF*/  	,;
        													/*@cDescErro*/     ,/*@cEspecNf*/		,/*cDocFo*/		,/*@aBreakNota*/	,;
        													/*@aNewNCC*/       ,/*cTpGeraGdp*/		,/*nOpc*/		     ,/*nArredondar*/	)
						
				
				EndCase
				
				RestArea(aAreaSL1)
				
				// Variaval de controle do processo
				lGravacao	:= lProcessou
				
				If lProcessou
				
				    // -- Tratamento para NCC�s 
				    // -- Carrega NCC Vinculada ao Orcamento
				    
				    aNCCItens   := {}

				    IF SL1->L1_CREDITO > 0

					    U_LxVerNCC( @aNCCItens, SL1->L1_NUM , SL1->L1_CLIENTE , SL1->L1_LOJA )				    
					    u_LjGrvCred( @aNccItens , @nNccUsada , nNccGerada )
				    
				    Endif
				    // -- Fim do tratamento das NCCs
				
					If lMvLjOffLn .AND. lLj7AtuInte
						Lj7AtuInte(Nil, SL1->L1_NUM, SL1->L1_FILIAL, .T.)
					Endif
										
					Lj7PesqAltMot( SL1->L1_SERIE, SL1->L1_DOC , SL1->L1_NUM ) //( cSerie , cDoc , cNumOrc ) - Pesquisa se existe algum motivo de desconto cadastrado para a venda
				
					If lGerInt .And. !lTemReserva 
						LjProIntVe()
					EndIf

					FRTProcSZ()
				
					//������������������?
					//?onto de entrada.?
					//������������������?
					If lLJ7051
						bOldError := ErrorBlock( {|x| LjVerifErro(x) } ) // muda code-block de erro
						Begin Sequence
							U_LJ7051()
						Recover   
							Conout("Nao conformidades na execucao do ponto de entrada LJ7051")
						End Sequence
						//����������������������������������?
						//?Restaura rotina de erro anterior?
						//����������������������������������?
						ErrorBlock( bOldError )
					Endif
					
					//��������������������������������������������Ŀ
					//?Inclusao de chamada - Especifico Template  ?
					//����������������������������������������������
					
					If ExistTemplate("LJ7002")
						ExecTemplate( "LJ7002", .F., .F., { 2, Nil, 2 } )
					EndIf

					If ExistBlock("LJ7002")
						ExecBlock( "LJ7002", .F., .F., { 2, Nil, 2 } )
					EndIf
								

					If File("LJGR"+cFileName+".FIM")
						
						ConOut("            "+STR0023) 	//"Solicitacao para finalizar gravacao batch atendida..."					

						//��������������������������������������Ŀ
						//?omente apaga o arquivo quando existir?
						//����������������������������������������
						
						FErase("LJGR"+cFileName+".FIM")	
						lExProc := .F.
					EndIf
					nTimes := 0
						
					//����������������������������������������������������Ŀ
					//?omente apaga o arquivo de orcamentos quando existir?
					//������������������������������������������������������?
					LjxCDelArq( SL1->L1_NUM )
					
					nIntervalo := 0
					nTimes++
					
				Else
				
					LjGravaErr()
					ConOut("LJGrvBatch: "+ STR0079 + aFiliais[ nCount ][1] + ". "+STR0022) // "Filial " ### ". " "Ocorreu algum erro no processo de gravacao batch..."
					AADD(aBadRecno, SL1->(Recno()) )
					
					//������������������������������������������������������������������������������������?
					//?onto de entrada que permite verificar o erro gerado no processo de gravacao batch ?
					//������������������������������������������������������������������������������������?
					If lLj7064
						ExecBlock("LJ7064", .F., .F., {SL1->(Recno())})
					EndIf
						
				EndIf
				
				SL1->( DbSkip() )

			End
			
		Endif
		//����������������������������������������������������������������������������������������a?
		//?heca se a tabela MBZ e fun��o de gravacao de estorno existem na base                   ?
		//����������������������������������������������������������������������������������������a?
		If lGrvEstorn
			Lj601GrPDV()
		EndIf

		// ------------------------------------------
		// NOTA FISCAL CONSUMIDOR ELETRONICA (NFC-E)
		// -----------------------------------------------------------------------------------------------------------------
		// Se houve um erro na venda (rejeicao ou TSS offline), ela esta marcada para ser cancelada (L1_SITUA = 'RY')
		// Executamos o LOJA140, para que seja feita a transmissao do cancelamento para o TSS e o cancelamento perante o ERP
		//-------------------------------------------------------------------------------------------------------------------
		aRecnoNFCe	:= {}	//zeramos o array que recebera os R_E_C_N_O_
		aAreaSL1	:= SL1->( GetArea() )

		SL1->( DbSetOrder(9) )	//L1_FILIAL + L1_SITUA + L1_PDV + L1_DOC
		If SL1->( DbSeek(xFilial("SL1") + "RY") )
			While SL1->L1_FILIAL + SL1->L1_SITUA == xFilial("SL1") + "RY"
				Aadd( aRecnoNFCe, SL1->(Recno()) )
				SL1->( DbSkip() )
			End
		EndIf

		For nI := 1 to Len(aRecnoNFCe)
			//atraves do R_E_C_N_O_ posicionamos no orcamento a ser cancelado
			SL1->( DbGoTo(aRecnoNFCe[nI]) )
			//"LJGrvBatch: Envia cancelamento NFC-e para TSS. Filial:" ## " - Orcamento:" ## " - Doc.: " ## " - Serie:"
			Conout(STR0168 +SL1->L1_FILIAL+ STR0169 + AllTrim(SL1->L1_NUM) + STR0170 + SL1->L1_DOC + STR0171 + SL1->L1_SERIE )

			xRet140Exc := lj140Exc( "SL1", SL1->(Recno()), 2,Nil,.T.,SL1->L1_FILIAL,SL1->L1_NUM)

			If ValType(xRet140Exc) == "N" .AND. xRet140Exc == 1
				Conout("LJGrvBatch: Envia cancelamento NFC-e para TSS - Processado com sucesso.")	 
			Else
				Conout("LJGrvBatch: Envia cancelamento NFC-e para TSS - N? Processado!",xRet140Exc)		
			EndIf			
		Next
		
		RestArea(aAreaSL1)
				
		nIntervalo := 0
		nTimes++
		aBadRecno  := {}
		
		If( lLOJA0051 .And. cMvLJILJLO == "1", oLJCLocker:ReleaseLock( "LOJXFUNCILLock" ),NIL)
	
		If lMultFil
			If lCriouAmb .AND. FindFunction("LJ1415LP")
				LJ1415LP(.T.)
			EndIF
			RESET ENVIRONMENT
		Endif	
				
		If lMultFil
			FClose(nHandle)
			FErase("LJGR"+cFileName+".WRK")
			
			ConOut("            "+STR0075 + cEmp + STR0076 + aFiliais[ nCount ][1]+" - "+STR0024) //""Empresa:" ### " Filial: - Processo de gravacao batch finalizado..."
		Endif	
		
	Else
		ConOut(Repl("*",40)+Chr(10)+Chr(10))
		ConOut("LJGrvBatch: "+STR0075 + cEmp + STR0076 + aFiliais[ nCount ][1])  // "Empresa:" ### " Filial:"
		ConOut("            "+STR0018)  //"Processo de gravacao batch ja estava rodando"
		
		lRetValue := .F.
		Return
	EndIf 
	
	If nCount < LEN( aFiliais )
		nCount := nCount + 1
	Else
		nCount := 1
	EndIf
	//������������������������������������������������Ŀ
	//?xecu��o da rotina de limpeza para loja Off-line?
	//��������������������������������������������������
	If lCriouAmb .AND. FindFunction("LJ1415LP") .AND. !lMultFil
		LJ1415LP(.T.)	
	Endif

FClose(nHandle)
FErase("LJGR"+cFileName+".WRK")

Return ( lRetValue )



Static Function LjGravaErr()

/*
RecLock("SL1", .F.)
REPLACE	SL1->L1_SITUA WITH "ER"
SL1->(MsUnlock())
*/

Return





Static Function LjVerifErro(e, cDescErro)
Local lRet := .F.

IF e:gencode > 0  

	LjGravaLog( .T., SL1->L1_NUM, Replicate("*", 40), LMSTOP, LMPROC)
	LjGravaLog( .T., SL1->L1_NUM, "DESCRIPTION:" + e:DESCRIPTION, LMSTOP, LMPROC)				
	LjGravaLog( .T., SL1->L1_NUM, "ERRORSTACK:" + e:ERRORSTACK, LMSTOP, LMPROC)				
	LjGravaLog( .T., SL1->L1_NUM, Replicate("*", 40), LMSTOP, LMPROC)

	lRet:=.T.
	cDescErro := "DESCRIPTION: " + e:DESCRIPTION + CTRL
    cDescErro += "ERRORSTACK:" + CTRL  
    cDescErro += e:ERRORSTACK
	Break
EndIf

Return lRet




Static Function LjVerPedErro(e,lProcessou,cFilSL1,nRecnoSL1)

Local lRet 		:= .F.

Default lProcessou 	:= .T.               
Default cFilSL1		:= "" 
Default nRecnoSL1 	:= 0

IF e:gencode > 0  

	LjGravaLog( .T., SL1->L1_NUM, Replicate("*", 40), LMSTOP, LMPROC)
	LjGravaLog( .T., SL1->L1_NUM, "DESCRIPTION:" + e:DESCRIPTION, LMSTOP, LMPROC)				
	LjGravaLog( .T., SL1->L1_NUM, "ERRORSTACK:" + e:ERRORSTACK, LMSTOP, LMPROC)				
	LjGravaLog( .T., SL1->L1_NUM, Replicate("*", 40), LMSTOP, LMPROC)

	lRet:=.T.

	If InTransact()
		DisarmTransaction()
		DbSelectArea("SL1")
		DbGoTo(nRecnoSL1)
		LjGravaErr()
		ConOut("LJGrvBatch: "+ STR0079 + cFilSL1 + ". "+STR0022) // "Filial " ### ". " "Ocorreu algum erro no processo de gravacao batch..."
	EndIf
	
	__QUIT()
		
EndIf     

Return(NIL)       
   




Static Function LjxRetAliq(cProd)

Local nRet	:= 0

Default cProd 		:= ""

DbSelectArea("SB0")
DbSetOrder(1)
If DbSeek(xFilial("SB0")+ cProd)
	nRet := SB0->B0_ALIQRED
EndIf

Return(nRet)





/*/{Protheus.doc} LjVerNCCs
Verifica a existencia de NCCs vinculadas ao Orcamento e prepara array para compensa��es

@type 	     User Function
@author     Mauro Paladini
@since 	    Jan 16, 2019  1:03:02 PM
/*/

User Function LxVerNCC( aNCCItens, cNumOrc , cCliente , cLjCli )

Local cSE1 := 'NCCFILSE1'

BEGINSQL Alias cSE1

SELECT  TOP 1
        E1_SALDO, 
        E1_NUM, 
        E1_EMISSAO, 
        SE1.R_E_C_N_O_ AS RECSE1,
        E1_SALDO, 
        E1_MOEDA, 
        E1_PREFIXO, 
        E1_PARCELA, 
        E1_TIPO  

FROM    %Table:SE1% SE1

INNER JOIN %Table:MDK% MDK 
ON      MDK.MDK_NUMREC = SE1.R_E_C_N_O_ AND MDK.D_E_L_E_T_ = ' '
 
INNER JOIN %Table:MDJ% MDJ 
ON      MDJ.MDJ_FILIAL = MDK.MDK_FILIAL AND MDJ.D_E_L_E_T_ = ' ' 
        AND MDJ.MDJ_NUMORC = MDK.MDK_NUMORC
        AND MDJ.MDJ_SITUA = 'NP' AND MDJ.MDJ_NUMORC = %Exp:cNumOrc%

WHERE   E1_FILIAL  = %xFilial:SE1%
        AND E1_CLIENTE = %Exp:cCliente%
        AND E1_LOJA    = %Exp:cLjCli%
        AND E1_STATUS  = 'A' //A=Em Aberto        
        AND E1_TIPO = 'NCC'
        AND SE1.D_E_L_E_T_ = ' '
        
ENDSQL

(cSE1)->( DbGoTop() )

If (cSE1)->( !Eof() )
                
    AAdd(aNCCItens,  {    .T.,;
                           (cSE1)->E1_SALDO ,;
                           (cSE1)->E1_NUM  ,;
                           STOD((cSE1)->E1_EMISSAO) ,;
                           (cSE1)->RECSE1  ,;
                           (cSE1)->E1_SALDO ,;
                           SuperGetMV("MV_MOEDA1") ,;
                           (cSE1)->E1_MOEDA ,;
                           (cSE1)->E1_PREFIXO ,;
                           (cSE1)->E1_PARCELA ,;
                           (cSE1)->E1_TIPO })
                           
Endif

(cSE1)->( DbCloseArea() )

Return