#Include 'Protheus.ch'

/*/{Protheus.doc} TibLoadLayout

Le arquivo texto contendo mapeamento de campos e prepara array

@author Mauro Paladini
@since 12/02/2015
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)

/*/

User Function TibLoadLayout( cFileDef )

Local aLayout	:= {}
Local cLine		:= ""
Local cTipoReg	:= ""		// Identificador [E01] 

Default cFileDef	:= ""

If File(GetSrvProfString("Startpath","") + cFileDef )

	If FT_FUSE(cFileDef) >= 0

		FT_FGotop()
		
		While (!FT_FEof())
			
			cLine 		:= AllTrim(Upper(FT_FREADLN()))
			
			IF Empty(cLine)
				FT_FSKIP()
				Loop			
			Endif
					
			aLine		:= Separa(cLine,";")
			
			// Identifica aHeader do tipo do registro
			IF Left(cLine,2) $ "[E
				cTipoReg	:= Substr(cLine,2,3)
				FT_FSKIP()
				Loop			
			Endif

			aSize(aLine, Len(aLine)+1)
			aIns(aLine, 1)
			
			aLine[1]	:= cTipoReg 

			// Adiciona os campos no Array
			aAdd( aLayout , aLine ) 
	
			FT_FSKIP()
	        
		EndDo
	
	Endif
	
	FT_FUse()
	
EndIf

Return aLayout

