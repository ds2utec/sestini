#INCLUDE "TOTVS.CH"
#include 'tryexception.ch'

#DEFINE NARRAY	12

#DEFINE ITEM	1 
#DEFINE PRODUTO	2
#DEFINE QTDE	3
#DEFINE PRCVEN	4
#DEFINE PRCUNIT	5	
#DEFINE VLRITEM	6
#DEFINE B2LOCAL	7
#DEFINE LOTE	8
#DEFINE IDENTB6	9
#DEFINE REFORI	10
#DEFINE REFITEM	11
#DEFINE TES		12


/*/{Protheus.doc} ${FwExAuto}
Execu��o da rotina automatica com valida��es especiais de campos

@type 		User Function
@author 	Mauro Paladini
@since 		Jan 21, 2019  8:22:02 AM
@version 	1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

User Function FwExAuto(bRotina,uPar1,uPar2,uPar3,uPar4,uPar5,uPar6,uPar7,uPar8,uPar9,uPar10,uPar11)   

LOCAL   ElMsHelpAuto 
LOCAL	EaTela 
LOCAL	EaGets 
LOCAL	EaHeader 
LOCAL	EaCols 
LOCAL	EaRotAuto 
LOCAL	EInclui 
LOCAL	EAltera 
LOCAL	En 
LOCAL	E__cInternet 
LOCAL   E__READVAR
Local   nX
Local ElMsFinalAuto

If ( Type('lMsFinalAuto') == 'U' )
	Private lMsFinalAuto := .T.
EndIf

ElMsFinalAuto := lMsFinalAuto
lMsFinalAuto  := .T.

If Type("lMsHelpAuto") == "U"      
   Private lMsHelpAuto   := .t.	
else
    ElMsHelpAuto :=lMsHelpAuto  	
EndIf
If Type("aTELA") <> "U"
	EaTela := aClone(aTELA)
EndIf

If Type("aGETS") <> "U"	
	EaGets := aClone(aGETS)		
	For nX := 1 To Len(aGets)
		cGets		:=	aGets[nX] 
		aGets[nX]	:= cGets	
	Next nX	
EndIf

If Type("aHeader") <> "U"
	EaHeader := aClone(aHeader)
EndIf
If Type("aCols")  <> "U"
	EaCols := aClone(aCols)
EndIf                         
If Type("aRotAuto") <> "U"
	EaRotAuto := aClone(aRotAuto)
EndIf                         
If Type("Inclui") <> "U"
	EInclui := Inclui
EndIf                         
If Type("Altera") <> "U"
	EAltera := Altera
EndIf                         
If Type("n") <> "U"
	En := n
EndIf              
If Type("__cInterNet") <> "U"
	E__cInternet := __cInterNet
EndIf    

If Type("__READVAR") <> "U"
	E__READVAR := __READVAR
EndIf    
         
__cInterNet := "AUTOMATICO"

eval(bRotina,uPar1,uPar2,uPar3,uPar4,uPar5,uPar6,uPar7,uPar8,uPar9,uPar10,uPar11)

lMsHelpAuto := ElMsHelpAuto 
lMsFinalAuto:= ElMsFinalAuto
aTELA       := EaTela 
aGETS       := EaGets
aHeader     := EaHeader
aCols  		:= EaCols
aRotAuto    := EaRotAuto
Inclui		:= EInclui
Altera		:= EAltera 
n			:= En
__cInterNet := E__cInternet
__READVAR   := E__READVAR

Return .t.



/*/{Protheus.doc} DePara
Faz troca de caracteres variaves em Query SQL usando StrTran

@author Mauo Paladini 
@since  14/02/2017
/*/

User Function DePara( aDePara , cOnde) 

Local nX    := 0
Local cDe   := ''
Local cPara := ''

Default cOnde   := ''
Default aDePara := {} 

For nX := 1 To Len(aDePara)

    cDe     := aDePara[nX,1]
    cPara   := aDePara[nX,2]

    // -- Minimo para Execu��o
    If Empty(cOnde) .Or. Empty(cDe)
        Return 
    Endif
    
    cOnde := StrTran( cOnde , cDe , cPara ) 

Next nX

Return cOnde


    
 
/*/{Protheus.doc} PreparaNF()
Funcao para 
��o da NF de Saida

@author 	Mauro Paladini
@since 	10/06/2017
@version 	1.0
/*/
 
    
User Function PreparaNF( cFilNF , cPedidoNF , aLotes , cSerie )
    
Local cAliasQry := ""
Local cNota     := ""
Local cSerie    := "" 
Local cQuery    := ""

Local aPvlNfs   := {} //-- Array com os itens a serem gerados
Local aBloqueio := {} //-- Array com os itens bloqueados
Local aNotas    := {}

Local nX        := 0
Local nItemNf   := 0
Local aITLote	:= {}

Default cFilNF		:= cFilAnt
Default cPedidoNF	:= ''
Default aLotes		:= {}
Default cSerie		:= ''  

SaveInter()

u_WriteLog( cJobLog  , '[PREPDOC] Prepara��o do documento Inicio ..' ,.T.,.T. )

// -- Instancia Filial para a NF

cFilAnt	:= cFilNF

DbSelectArea("SC5") 
DbSelectArea("SC9")

SC5->( DbSetOrder(1) )

IF SC5->( DbSeek( cFilNF + cPedidoNF ) )
	
	SC9->(DbSetOrder(1))
		    
	//-- Verifica conforme o numero de serie , qual a quantidade maxima de itens por nota fiscal
	nItemNf  := 999 
   
   	If Empty(SC5->C5_LIBEROK) 
	   Ma410LbNfs(2,@aPvlNfs,@aBloqueio) 
	EndIf   
		 
   	If SC9->(MsSeek( cFilNF + SC5->C5_NUM ) )
   		
   		While SC9->( C9_FILIAL + C9_PEDIDO ) == xFilial("SC5")+SC5->C5_NUM 

   			// -- APENAS ITENS NAO LIBERADOS E NAO FATURADOS
   	
			If !(SC9->C9_BLEST=='  '.And. SC9->C9_BLCRED=='  '.And.(SC9->C9_BLWMS>='05' .OR. SC9->C9_BLWMS='  ').And. Iif(SC9->((FieldPos('C9_BLTMS') > 0)), Empty(SC9->C9_BLTMS), .T. )) ; //Item n�o Liberado  
			   	.And. !((SC9->C9_BLCRED =='10' .And. SC9->C9_BLEST  == '10').Or. SC9->C9_BLCRED =='ZZ' .And. SC9->C9_BLEST  == 'ZZ')                                                //Item n�o Faturado
			
		   		// -- Libera��o de credito e estoque para o item posicionado
				//  Parametros: 	ExpN1:  	1 - Libera��o                                     
				// 	                    		2 - Rejeicao				
				//					ExpN2:  Indica liberacao de credito
				//					ExpN3:  Indica liberacao de estoque                                         
		   				   		
		   		nPosIt	:=  Ascan(aLotes  ,{|x|  AllTrim(x[8]) == AllTrim(SC9->C9_PRODUTO) }) 		   		
		   		aItLote	:= {}

		   		If nPosIt > 0
		   		
		   			aAdd( aItLote , aLotes[nPosIt] )
		   			
			   		a450Grava( 1 , .T. , .T. , .F. , aItLote, .T. ) 
			   		
			   	Else
			   		
			   		a450Grava( 1 , .T. , .T. , .F. , , .T. )
			   	
			   	Endif
		   		
		   	EndIf

	   		SC9->(dbSkip())

		End
		
		//-- Prepara o documento de saida
		
		If !Empty(SC5->C5_LIBEROK) 
	 		Ma410LbNfs(1,@aPvlNfs,@aBloqueio)  	 
	 	EndIf 
	   	
	EndIf
   
	// -- Gera��o do documento de saida                                          �
	   
	If !Empty(aPvlNfs) 
	   
		aAdd(aNotas,{})
	
	   	For nX := 1 To Len(aPvlNfs)  
	   	 	If Len(aNotas[Len(aNotas)])>= nItemNf
	    	  	aAdd(aNotas,{})
	    	EndIf
			aAdd(aNotas[Len(aNotas)],aClone(aPvlNfs[nX]))
		Next nX			  	
	
		/*
	    ExpA1: Array com os itens a serem gerados                   
	    ExpC2: Serie da Nota Fiscal                                 
	    ExpL3: Mostra Lct.Contabil                                  
	    ExpL4: Aglutina Lct.Contabil                                
	    ExpL5: Contabiliza On-Line                                  
	    ExpL6: Contabiliza Custo On-Line                            
	    ExpL7: Reajuste de preco na nota fiscal                     
	    ExpN8: Tipo de Acrescimo Financeiro                         
	    ExpN9: Tipo de Arredondamento                               
	    ExpLA: Atualiza Amarracao Cliente x Produto                 
	    ExplB: Cupom Fiscal                                         
	    ExpCC: Numero do Embarque de Exportacao                     
	    ExpBD: Code block para complemento de atualizacao dos titu- 
	           los financeiros.                                     
	    ExpBE: Code block para complemento de atualizacao dos dados 
	           apos a geracao da nota fiscal.                       
	    ExpBF: Code Block de atualizacao do pedido de venda antes   
	           da geracao da nota fiscal                            
		*/
		
		Pergunte("MT460A",.F.)
	  	
	  	For nX := 1 To Len(aNotas) 	
	   
		   	//-- Gera a NF de saida
		   	
		   	cNota 		:= MaPvlNfs(aNotas[nX],cSerie, MV_PAR01==1,MV_PAR02==1,MV_PAR03==1,MV_PAR04==1,MV_PAR05==1,MV_PAR07,MV_PAR08,MV_PAR15==1,MV_PAR16==2) 
	
			If !Empty(cNota)
			
				IF SC5->( DbSeek( cFilNF + cPedidoNF ) )
				
					/*
					RecLock("SC5",.F.)
						SC5->C5_XPEDCO		:= cPedTransf
					MsUnLock()*/		
				
				Endif
			
				
		   	EndIf
		   
	   	Next nX 
		   
	EndIf

Endif
   
aPvlNfs  := {}
aBloquei := {}
aNotas   := {}
cNota    := ''
   
RestInter()
	
Return NIL  

