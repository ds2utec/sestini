#include "TOTVS.CH"
#include "COLORS.CH"
#INCLUDE "FILEIO.CH"
#INCLUDE "FWCOMMAND.CH"
#INCLUDE 'FWMVCDEF.CH' 

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE CANCELADO	"5"
#DEFINE COMERRO		"8"
#DEFINE EMPROCESSA	"9"


//-- LIB DE FUNCIONALIDADES

/*/{Protheus.doc} BIBuildQuery
Carrega arquivo contendo query PLSQL

@author	Mauo Paladini 
@since 	14/02/2017
/*/

User Function BIBuildQuery( cFileQuery )

Local xRet			:= ""
Local cFilePath		:= '\user_procedures\

Default cFileQuery      := ''

If File( cFilePath + cFileQuery )

	xRet := MemoRead( cFilePath + cFileQuery  )	
	CONOUT( '[BIBuildQuery] Loading query -> ' + cFilePath + cFileQuery )
	
Else
	
	CONOUT('[BIBuildQuery] ERRO ARQUIVO N�O ENCONTRADO -> ' + cFilePath + cFileQuery ) 
	
Endif

Return xRet





/*/{Protheus.doc} UseProcedure
Verifica a existencia de uma stored procedures no SQL

@type 		function
@author 	Mauro Paladini
@since 		24/07/2017
1@version 	1.0
/*/

User Function SysObject( cNomeProc , cTipo )

Local lRet		:= .F.
Local aArea		:= GetArea()
Local cProc		:= GetNextAlias()
Local cQuery	:= "SELECT ISNULL( OBJECT_ID ( '" + cNomeProc + "', '" + cTipo + "' ) , 0 ) AS ID_OBJ " 

DbuseArea( .T., "TopConn", TCGenQry(,,cQuery), cProc , .F., .F. )

If (cProc)->ID_OBJ > 0
	lRet	:= .T.
Endif

(cProc)->( DbCloseArea() )

RestArea(aArea)

Return lRet 





/*/{Protheus.doc} STNPlanilha
Exibe planilha financeira do cupom antes de processar a venda

@type 		User Function
@author 	Mauro Paladini
@since 		Feb 23, 2019  9:06:00 AM
@version 	1.0
/*/

User Function STNPlanilha( cArqImp , cCupom , lUpdStatus , lHTMl , lShow , cMsgCupom )

Local aAreas  		:= { P01->(GetArea()) , P01->(GetArea()) , P03->(GetArea()) , GetArea() }	
Local nItem			:= 1
Local cTesVenda		:= ''
Local cFS_TesSai	:= GetMv("FS_TESSAI",,"501")
Local cHTML			:= ''
Local aSimula		:= {}
Local lRet			:= .T.

Default cArqImp		:= ''
Default cCupom		:= ''
Default lUpdStatus	:= .F.
Default lHTML		:= .F.
Default	lShow		:= .F.
Default cMsgCupom	:= ''

// -- Valida Cabecalho do Cupom

P01->( DbSetOrder(6) )
IF P01->( !DbSeek( xFilial("P01") + cArqImp + cCupom ) )
	UserException('Cupom Fiscal inv�lido')
	Return 
Endif


// -- Valida Cliente

SA1->( DbSetOrder(1) )
IF SA1->( !DbSeek( xFilial("SA1") + P01->P01_CLIENT + P01->P01_LOJA ) )
	UserException('Cliente informado na venda n�o existe!')
	Return 
Endif

// -- Valida total do cupom com fun��es Fiscais 

P02->( DbSetOrder(6) )
IF P02->( DbSeek( xFilial("P02") + cArqImp + cCupom ) )

	If MafisFound()
		MaFisEnd()
	EndIf

	MaFisIni( SA1->A1_COD, SA1->A1_LOJA, "C" , "S" , ;
	          NIL          , NIL        , NIL , .F., ;
	          "SB1"        , "LOJA701"  )

	While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + P02_DOCCOO == xFilial("P02") + cArqImp + cCupom )
	
		SB1->( DbSeek( xFilial("SB1") + RTrim(P02->P02_PROD) ) )
		
		cTesVenda	:= MaTesInt( 2	, "01"	, SA1->A1_COD , SA1->A1_LOJA ,"C" , PadR(P02->P02_PROD,TamSx3("LR_PRODUTO")[1]) , NIL )		
		
		// -- Caso n�o encontre utiliza TES do Produto
		
		If Empty(cTesVenda)
			cTesVenda	:= PadR(SB1->B1_TS,TamSx3("LR_TES")[1])							
		Endif
							
		// -- Caso esteja em banco no produto utiliza TES fixa em parametro
		
		If Empty(cTesVenda)
			cTesVenda	:= PadR(cFS_TesSai,TamSx3("LR_TES")[1])					
		Endif

		SF4-> (DbSeek( xFilial("SF4") + cTesVenda ) )
	
		MaFisAdd( 	RTrim(P02->P02_PROD) ,;								// Produto
					cTesVenda ,;										// Tes
					P02->P02_QUANT 	,;									// Quantidade
					( P02->P02_PRUNIT - P02->P02_DESCIT ),; 			// Preco unitario
					P02->P02_DESCIT ,;									// Valor do desconto
					"",; 						// Numero da NF original
					"",; 						// Serie da NF original
					0,;							// Recno da NF original
					0,; 						// Valor do frete do item
					0,; 						// Valor da despesa do item
					0,; 						// Valor do seguro do item
					0,; 										// Valor do frete autonomo												
					( P02->P02_PRUNIT * P02->P02_QUANT ) ,;		// Valor da mercadoria
					0 )											// Valor da embalagem

							
		aAdd( aSimula , { 	P02->P02_ITEM ,;
							P02->P02_PROD ,;
							P02->P02_DESCRI ,;
							P02->P02_QUANT ,;
							Transform( MaFisRet(nItem , "IT_PRCUNI")  	, PesqPict("SL2","L2_VLRITEM" ) ) ,;
							Transform( MaFisRet(nItem , "IT_DESCONTO") 	, PesqPict("SL2","L2_VLRITEM" ) ) ,;
							Transform( MaFisRet(nItem , "IT_TOTAL") 	, PesqPict("SL2","L2_VLRITEM" ) ) ,;
							cTesVenda  }) 
							
				
		IF lUpdStatus
		
			IF MaFisRet(nItem , "IT_TOTAL")  <> P02->P02_VALLIQ
	
				lRet	:= .F.
				cMsg	:= 	' Vlr Varejo  R$ ' + Transform( P02->P02_VALLIQ 			, PesqPict("SL2","L2_VLRITEM" ) ) + ;
							' Vlr Totvs R$ ' + aSimula[Len(aSimula)][7] + CRLF	
	
				RecLock("P02",.F.)			
					P02->P02_STATUS	:= INVALIDO			
					P02->P02_LOGIMP	:= cMsg
				MsUnLock()
		
				CONOUT( " Item [" + AllTrim(P02->P02_ITEM) + "] Produto  " + P02->P02_PROD + cMsg )
				
				cMsgCupom += " Item [" + AllTrim(P02->P02_ITEM) + "] Produto  " + P02->P02_PROD + cMsg  
				
			Else
			
				RecLock("P02",.F.)			
					P02->P02_STATUS	:= VALIDADO			
					P02->P02_LOGIMP	:= ''
				MsUnLock()
						
			Endif
		
		Endif			

		nItem += 1				
		P02->( DbSkip() )
		
	End 

	MaFisEnd()
	
	IF lUpdStatus
		
		RecLock("P01",.F.)			
			P01->P01_STATUS	:= IIF( !lRet , INVALIDO , VALIDADO )			
			P01->P01_LOGIMP	:= IIF( !lRet , cMsgCupom , '' )
		MsUnLock()	
	Endif
	
	IF lHTMl
	
		aCab	:= { 'Item' , 'Produto' , 'Descri��o' , 'Qtde' , 'Vlr Unit' , 'Desconto' , 'Vlr Item' , 'TES' }		
		cHTML 	:= STNTableCSS( aCab , aSimula )
				
		If lShow
			STNViewHTML( cHTML )
		Endif
		
	Endif
	
Endif

AEval(aAreas, {|x| RestArea(x)})

Return lRet


/*/{Protheus.doc} STNViewHTML
Exibe codigo HTML

@type 		User Function
@author 	Mauro Paladini
@since 		Feb 23, 2019  9:41:48 AM
@version 	1.0
/*/
User Function STNViewHTML( cHTML )

Local oPanel	:= Nil
Local oEdit		:= Nil
Local oPanMemo  := Nil
Local oPan      := Nil
Local oPanMemo	:= Nil

DEFINE DIALOG oDlg Title 'Venda Assistida' FROM  0,0 to 425, 600 Pixel

oPanel	:= TPanel():New(0,0,"",oDlg,,,,,/*CLR_WHITE*/,550,100)
oPanel:Align := CONTROL_ALIGN_ALLCLIENT
	
oFwLayer := FwLayer():New()
oFwLayer:init(oPanel,.F.)
oFWLayer:addCollumn( "COL1",100, .T. , "LINHA2")
oFWLayer:addWindow( "COL1", "WIN1", 'Teste', 100, .F., .F., , "LINHA2")//Calend�rio//"Calend�rio"//"Editor de Email"

oPanel := oFWLayer:GetWinPanel("COL1","WIN1","LINHA2")

oEdit := FWSimpEdit():New( 0, 0, 500,600, 'Editor',,,.F.,.F. , oPan )//"Editor HTML"
oEdit:SetText( cHTML )

ACTIVATE DIALOG oDlg Centered

Return


User Function Totaliza()

Local aCab		:= { 'Item' , 'Produto' , 'Descri��o' , 'Qtde' , 'Vlr Unit' , 'Desconto' , 'Vlr Item' , 'TES' }		
Local aItens	:= {}

Local cHTML := u_STNTableCSS( aCab , aItens )

aAdd( aItens, { 	'0001'  ,;
					'002020' ,;
					'BOLSA SESTINI ADULTA',;
					 '1' ,;
					'R$ 1.000,00' ,;
					'R$        0' ,;
					'R$ 1.000,00' ,;
					'711' }) 



u_STNViewHTML( cHTML )

Return



/*/{Protheus.doc} STNTableCSS

@type 		User Function
@author 	Mauro Paladini
@since 		Feb 23, 2019  8:44:46 AM
/*/

User Function STNTableCSS( aCab , aItens  )

Local cHTML	:= ''
Local nX
Local nY

cHTML += '	<!DOCTYPE html>         ' + CRLF
cHTML += '	<html>    ' + CRLF
cHTML += '	<head>    ' + CRLF
cHTML += '	<style>   ' + CRLF
cHTML += '	table {   ' + CRLF
cHTML += '	  font-family: arial, sans-serif;     ' + CRLF
cHTML += '	  border-collapse: collapse;          ' + CRLF
cHTML += '	  width: 100%;          ' + CRLF   
cHTML += '	}         ' + CRLF
cHTML += '            ' + CRLF
cHTML += '	td, th {  ' + CRLF
cHTML += '	  border: 1px solid #dddddd;          ' + CRLF
cHTML += '	  text-align: left;     ' + CRLF
cHTML += '	  padding: 8px;         ' + CRLF
cHTML += '	}         ' + CRLF
cHTML += '            ' + CRLF
cHTML += '	tr:nth-child(even) {    ' + CRLF
cHTML += '	  background-color: #dddddd;          ' + CRLF
cHTML += '	}         ' + CRLF
cHTML += '	</style>  ' + CRLF
cHTML += '	</head>   ' + CRLF
cHTML += '	<body>    ' + CRLF
cHTML += '            ' + CRLF
cHTML += '	<table>   ' + CRLF

cHTML += '	  <tr>    ' + CRLF
For nX := 1 To Len(aCab)
	cHTML += '    <th>' + aCab[nX]+ '</th>	'+ CRLF
Next nX
cHTML += '	  </tr>' 		+ CRLF

For nX := 1 To Len(aItens)
	For nY := 1 To Len(aCab)
		cHTML += '    <th>' + aItens[nX][nY] + '</th>	'+ CRLF
	Next nY
Next nX

cHTML += '	</table>  ' + CRLF
cHTML += '	</body>   ' + CRLF
cHTML += '	</html>   ' + CRLF

Return cHTML




/*/{Protheus.doc} SP_Prepare
Realiza a compila��o das procedures no banco de dados

@type 		function
@author 	    Mauro Paladini
@since 		24/07/2017
@version 	1.0
/*/

User Function SP_Prepare( aAllProc , lDrop , cCFGLog , cDirCFG ) 

Local cProcError        := ''
Local cNomeProc         := ''
Local cTipo             := ''
Local lParse            := .F.
Local lProcedures       := .T.

Local nX	:= 0

// -- Deleta as procedures para recria��o

IF lDrop

	For nX := 1 To Len(aAllProc)
	
		cProcError    := ''	
		cNomeProc     := aAllProc[nX][1]
		cTipo         := aAllProc[nX][2]
		
		IF u_SysObject( cNomeProc , cTipo )
		
			cSQLProc := 'DROP ' + IIF( cTipo == 'P' , 'PROCEDURE ' , 'VIEW ' ) + cNomeProc 
			
			If ( TCSQLExec(cSQLProc) < 0 )
			
				cProcError	:= TCSQLError()
			
				u_WriteLog( cCFGLog  , 'ERRO FATAL ao apagar a procedure ' + cNomeProc  ,.T.,.T. )				
				u_WriteLog( cCFGLog  , ''  	,.F.,.F. )
				u_WriteLog( cCFGLog  , cProcError  	,.F.,.F. )
				u_WriteLog( cCFGLog  , '' ,.F.,.F. )		
			
			Else
			
				u_WriteLog( cCFGLog  , 'Sucesso ao apagar a procedure ' + cNomeProc  ,.T.,.T. )	
						
			Endif
		
		Endif
		
	Next Nx
	
	If !IsBlind() .And. Empty( cProcError ) 
		Aviso( "Sucesso" , "Procedures apagadas com sucesso !"  , {"Continuar"} , 1 )
	Endif	

Endif


// -- Verifica se as procedures existem

u_WriteLog( cCFGLog  , 'Inciando verifica��o das procedures'  	,.T.,.T. )

For nX := 1 To Len(aAllProc)

	cProcError	:= ''
	cNomeProc	:= aAllProc[nX][1]
	cTipo		:= aAllProc[nX][2]
	
	IF !u_SysObject( cNomeProc , cTipo ) 
		
		IF !lDrop
			u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Procedure n�o compilada '  	,.T.,.T. )
		Endif
			
		// -- Carrega procedure do arquivo de defini��es .SQL
		
		IF File( cDirCFG + cNomeProc + '.SQL' ) 
		
			u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Lendo ' + Upper( cNomeProc + '.SQL' ) ,.T.,.T. )
						
			cSQLProc	:= MemoRead( cDirCFG + cNomeProc + '.SQL' ) 
			
			// -- Compila a Procedure
			
			u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Iniciando compila��o da procedure ' ,.T.,.T. )
			
			If ( TCSQLExec(cSQLProc) < 0 )
				
				cProcError	:= TCSQLError()
			
				u_WriteLog( cCFGLog  , '[' + cNomeProc + '] ERRO FATAL ao compilar a procedure via TotvsDbAccess '  ,.T.,.T. )				
				u_WriteLog( cCFGLog  , ''  	,.F.,.F. )
				u_WriteLog( cCFGLog  , cProcError  	,.F.,.F. )
				u_WriteLog( cCFGLog  , '' ,.F.,.F. )
				
				IF !IsBlind() 
				    Aviso( "[BIConfigure] Problemas na Instala��o da procedure " + cNomeProc , cProcError , {"Abandonar"} , 2 )
				Endif
								
				lProcedures	:= .F.
				Exit
							
			Else
			
				u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Procedure compilada com sucesso no servidor' ,.T.,.T. )
				u_WriteLog( cCFGLog  , '' ,.F.,.F. )
				lParse	:= .T.
			
			Endif
	
		Else

			u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Arquivo da procedure n�o encontrado ' ,.T.,.T. )			
			u_WriteLog( cCFGLog  , 'Processo interrompido' ,.T.,.T. )

			lProcedures	:= .F.
			Exit
			
		Endif
	
	Else
	
		u_WriteLog( cCFGLog  , '[' + cNomeProc + '] Procedure OK '  	,.T.,.T. )
		
	Endif 

Next nX

u_WriteLog( cCFGLog  , 'Fim das verifica��es'  	,.T.,.T. )
u_WriteLog( cCFGLog  , '' ,.F.,.F. )

If !lProcedures
	
	IF !IsBlind()
	
        	cShowLog	:= MemoRead( cCFGLog )
        
        	Aviso( "Problemas na verifica��o das Procedures" , "Verifique arquivo de log " + cCFGLog +  " na pasta \system\"  , {"Abandonar"} , 2 )	
        	
        	AtShowLog( cShowLog, "Instala��o de Stored Procedures" ,.T.,.T.,.T.,.F.)
	
	Endif	

Else

	IF !IsBlind() .And. Empty( cProcError ) .And.  lParse
		Aviso( "Sucesso" , "Procedures compiladas com sucesso !"  , {"Continuar"} , 1 )
	Endif
		
Endif

Return lProcedures






/*/{Protheus.doc} xGetArea

Salva Area de diversas tabelas ao mesmo tempo

@author	Mauo Paladini 
@since 	14/02/2017
/*/

User Function xGetArea(aArqs)

	Local nX		:= 0
	Local aAlias	:= {}

	Default aArqs		:= {}

	For nX:= 1 To Len(aArqs)
		
		dbSelectArea(aArqs[nX])
		AAdd( aAlias,{ Alias(), IndexOrd(), Recno()})
	
	Next nX

Return aAlias


/*/{Protheus.doc} xRestArea()

Restaura Area de diversas tabelas ao mesmo tempo

@author	Mauo Paladini 
@since 	14/02/2017
/*/

User Function xRestArea( aAlias )

	Local nX	:= 1

	Default aAlias	:= {}

	For nX:= 1 To Len(aAlias)
		DbSelectArea(aAlias[nX,1])
		DbSetOrder(aAlias[nX,2])
		DbGoto(aAlias[nX,3])
	Next

Return




/*/{Protheus.doc} SLDelete()
Deleta Tabela dos or�amentos

@author Mauo Paladini 
@since  14/02/2017
/*/

User Function SLDelete( cFilOrc , cNumOrc , cNumDoc )

Local aAreas    := { P01->(GetArea()) , P02->(GetArea()) , P03->(GetArea()) , GetArea() }

Begin Transaction

SL1->( DbSetOrder(1) )
If SL1->( DbSeek( cFilOrc + cNumOrc  ) )
    Reclock("SL1",.F.)
    SL1->( dbDelete() )
    MsUnlock()  
Endif   

SL2->( DbSetOrder(1) )
If SL2->( DbSeek( cFilOrc + cNumOrc ) ) 
    While SL2->( !Eof() .AND. L2_FILIAL+L2_NUM == cFilOrc+cNumOrc )     
        Reclock("SL2",.F.)
        SL2->( dbDelete() )
        MsUnlock()
        SL2->( dbSkip() )
    End
EndIf

SL4->( DbSetOrder(1) )
If SL4->( DbSeek( cFilOrc + cNumOrc ) ) 
    While SL4->( !Eof() .AND. L4_FILIAL+L4_NUM == cFilOrc+cNumOrc )
        Reclock("SL4",.F.)
        SL4->( dbDelete() )
        MsUnlock()
        SL4->( dbSkip() )
    End
EndIf
   
End Transaction

AEval(aAreas, {|x| RestArea(x)})

Return Nil



/*/{Protheus.doc} WriteLog()

Funcao generica de log

@author Mauo Paladini 
@since  14/02/2017
/*/


User Function WriteLog( cArq , uTexto , lConsole , lDateTime , tInicio, tFim , lPtinternal , lFunName )

Local nHandle   := 0
Local nAuxFor   := 0
Local cTexto    := ""
Local cTempo    := ''

Local cDrive        := ""
Local cDir          := ""
Local cNomeArq      := ""
Local cExt          := ""
Local tDuracao      := ''
Local __FunName     := '[' + AllTrim(ProcName(2)) + ' line: '+ AllTRim(cValToChar(ProcLine(2))) + '] '

Default tInicio     := ''
Default tFim        := ''
Default lConsole    := .T.
Default lDateTime   := .T.
Default lPtinternal := .F.
Default lFunName    := .F.

IF !Empty(tInicio)
    MV_PAR60    := tInicio
Endif

IF !Empty(tFim)
    tDuracao    := ElapTime(MV_PAR60,Time())    
    cTempo      := '   ' + SubStr(tDuracao,1,2)+" h: "+SubStr(tDuracao,4,2)+" m: "+SubStr(tDuracao,7,2)+" s: "
Endif

cArq := StrTran(Alltrim(cArq)," ","")

If ! ( "." $ cArq )
    cArq    += ".log"
Endif   

If lPtInternal
    PtInternal( 1, uTexto )
Endif

If !File( cArq )

    // -- Tratamento para diretorios
    
    SplitPath( cArq , @cDrive, @cDir, @cNomeArq, @cExt )
    MontaDir(cDir)

    nHandle := FCreate( cArq )
    FClose( nHandle )   

Endif

If File( cArq )


    nHandle := FOpen( cArq, 2 )
    FSeek ( nHandle, 0, 2 )         // Posiciona no final do arquivo.
    
    If ValType(uTexto) == "C"
            
        If lDateTime
            If lFunName
                cTexto := dtoc(dDataBase) + " " + Time() + " " + __FunName + uTexto
            Else
                cTexto := dtoc(dDataBase) + " " + Time() + " " + uTexto
            Endif
        Else
            If lFunName
                cTexto := __FunName + uTexto
            Else
                cTexto := uTexto
            Endif
            cTexto := uTexto
        Endif
        
        IF !Empty( cTempo )
            cTexto  += cTempo
        Endif
        
        FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
        
        If lConsole
            CONOUT(cTexto)
        Endif
    
    ElseIf ValType(uTexto) == "A"
    
        For nAuxFor := 1 to len(uTexto)
        
            cTexto := dtoc(dDataBase) + " " + Time() + " " + uTexto[nAuxFor]

            IF !Empty( cTempo )
                cTexto  += cTempo
            Endif
            
            FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
            
            If lConsole
                CONOUT(cTexto)
            Endif
            
        Next nAuxFor
    
    EndIf
    
    FClose( nHandle )
    
EndIf

Return Nil







/*/{Protheus.doc} AjRetParam
Funcao para ajuste das respostas das perguntas da ParamBox

@type       function
@author     Mauro Paladini
@since      11/04/2017
@version    1.0
/*/

User Function AjRetParam(aRet,aParamBox)
    
    Local nX := 0

    If ValType(aRet) == "A" .And. Len(aRet) == Len(aParamBox)
        
        For nX := 1 To Len(aParamBox)
            If aParamBox[nX][1] == 2 .AND. ValType(aRet[nX]) == "C"
                aRet[nX] := aScan(aParamBox[nX][4],{|x| Alltrim(x) == aRet[nX]})
            Endif
        Next nX
    EndIf

Return aRet




/*/
{Protheus.doc} XXPutSx1
Cria grupo de perguntas no dicionario

@author     Mauro Paladini
@since  20/04/2016
@version    1.0
/*/

User Function XXPutSx1(cGrupo,cOrdem,cPergunt,cPerSpa,cPerEng,cVar,;
    cTipo ,nTamanho,nDecimal,nPresel,cGSC,cValid,;
    cF3, cGrpSxg,cPyme,;
    cVar01,cDef01,cDefSpa1,cDefEng1,cCnt01,;
    cDef02,cDefSpa2,cDefEng2,;
    cDef03,cDefSpa3,cDefEng3,;
    cDef04,cDefSpa4,cDefEng4,;
    cDef05,cDefSpa5,cDefEng5,;
    aHelpPor,aHelpEng,aHelpSpa,cHelp)

LOCAL aArea := GetArea()
Local cKey
Local lPort := .f.
Local lSpa  := .f.
Local lIngl := .f. 

cKey    := "P." + AllTrim( cGrupo ) + AllTrim( cOrdem ) + "."
cPyme    := Iif( cPyme      == Nil, " ", cPyme      )
cF3      := Iif( cF3        == NIl, " ", cF3        )
cGrpSxg  := Iif( cGrpSxg    == Nil, " ", cGrpSxg    )
cCnt01   := Iif( cCnt01     == Nil, "" , cCnt01     )
cHelp    := Iif( cHelp      == Nil, "" , cHelp      )

dbSelectArea( "SX1" )
dbSetOrder( 1 )

// Ajusta o tamanho do grupo. Ajuste emergencial para valida��o dos fontes.
// RFC - 15/03/2007
cGrupo := PadR( cGrupo , Len( SX1->X1_GRUPO ) , " " )

If !( DbSeek( cGrupo + cOrdem ))

    cPergunt:= If(! "?" $ cPergunt .And. ! Empty(cPergunt),Alltrim(cPergunt)+" ?",cPergunt)
    cPerSpa := If(! "?" $ cPerSpa  .And. ! Empty(cPerSpa) ,Alltrim(cPerSpa) +" ?",cPerSpa)
    cPerEng := If(! "?" $ cPerEng  .And. ! Empty(cPerEng) ,Alltrim(cPerEng) +" ?",cPerEng)

    Reclock( "SX1" , .T. )

    Replace X1_GRUPO   With cGrupo
    Replace X1_ORDEM   With cOrdem
    Replace X1_PERGUNT With cPergunt
    Replace X1_PERSPA  With cPerSpa
    Replace X1_PERENG  With cPerEng
    Replace X1_VARIAVL With cVar
    Replace X1_TIPO    With cTipo
    Replace X1_TAMANHO With nTamanho
    Replace X1_DECIMAL With nDecimal
    Replace X1_PRESEL  With nPresel
    Replace X1_GSC     With cGSC
    Replace X1_VALID   With cValid
    Replace X1_VAR01   With cVar01
    Replace X1_F3      With cF3
    Replace X1_GRPSXG  With cGrpSxg

    If Fieldpos("X1_PYME") > 0
        If cPyme != Nil
            Replace X1_PYME With cPyme
        Endif
    Endif

    Replace X1_CNT01   With cCnt01
    
    If cGSC == "C"          // Mult Escolha
    
        Replace X1_DEF01   With cDef01
        Replace X1_DEFSPA1 With cDefSpa1
        Replace X1_DEFENG1 With cDefEng1

        Replace X1_DEF02   With cDef02
        Replace X1_DEFSPA2 With cDefSpa2
        Replace X1_DEFENG2 With cDefEng2

        Replace X1_DEF03   With cDef03
        Replace X1_DEFSPA3 With cDefSpa3
        Replace X1_DEFENG3 With cDefEng3

        Replace X1_DEF04   With cDef04
        Replace X1_DEFSPA4 With cDefSpa4
        Replace X1_DEFENG4 With cDefEng4

        Replace X1_DEF05   With cDef05
        Replace X1_DEFSPA5 With cDefSpa5
        Replace X1_DEFENG5 With cDefEng5

    Endif

    MsUnlock()

    IF __Language == 'PORTUGUESE'  
        cHelp   := aHelpPor[1]
    Elseif __Language == 'SPANISH'
        cHelp   := aHelpSpa[1]           
    Elseif __Language == 'ENGLISH'
        cHelp   := aHelpEng[1]      
    Endif

    IF !Empty(cHelp)    
        fPutHelp( cKey , cHelp , .T. )
    Endif

Else

   lPort := ! "?" $ X1_PERGUNT .And. ! Empty(SX1->X1_PERGUNT)
   lSpa  := ! "?" $ X1_PERSPA  .And. ! Empty(SX1->X1_PERSPA)
   lIngl := ! "?" $ X1_PERENG  .And. ! Empty(SX1->X1_PERENG)
   
   IF __Language == 'PORTUGUESE'  
        cHelp   := aHelpPor[1]
   Elseif __Language == 'SPANISH'
        cHelp   := aHelpSpa[1]           
   Elseif __Language == 'ENGLISH'
        cHelp   := aHelpEng[1]      
   Endif

   IF !Empty(cHelp)    
        fPutHelp( cKey , cHelp , .T. )
   Endif

   If lPort .Or. lSpa .Or. lIngl
        RecLock("SX1",.F.)
        If lPort 
         SX1->X1_PERGUNT:= Alltrim(SX1->X1_PERGUNT)+" ?"
        EndIf
        If lSpa 
            SX1->X1_PERSPA := Alltrim(SX1->X1_PERSPA) +" ?"
        EndIf
        If lIngl
            SX1->X1_PERENG := Alltrim(SX1->X1_PERENG) +" ?"
        EndIf
        SX1->(MsUnLock())
    EndIf

Endif

RestArea( aArea )

Return




/*/
{Protheus.doc} fPutHelp
Cria Help das perguntas

@author     Mauro Paladini
@since      20/04/2016
@version    1.0
/*/

Static Function fPutHelp(cKey, cHelp, lUpdate)

    Local cFilePor  := "SIGAHLP.HLP"
    Local cFileEng  := "SIGAHLE.HLE"
    Local cFileSpa  := "SIGAHLS.HLS"
    Local nRet      := 0

    Default cKey    := ""
    Default cHelp   := ""
    Default lUpdate := .F.

    //-- Se a Chave ou o Help estiverem em branco

    If Empty(cKey) .Or. Empty(cHelp)
        Return
    EndIf

    // -- Portugu�s
    nRet := SPF_SEEK(cFilePor, cKey, 1)

    //Se n�o encontrar, ser� inclus�o

    If nRet < 0
        SPF_INSERT(cFilePor, cKey, , , cHelp)

    //Sen�o, ser� atualiza��o
    Else
        If lUpdate
            SPF_UPDATE(cFilePor, nRet, cKey, , , cHelp)
        EndIf
    EndIf


    // --  Ingl�s
    nRet := SPF_SEEK(cFileEng, cKey, 1)

    // -- Se n�o encontrar, ser� inclus�o
    If nRet < 0
        SPF_INSERT(cFileEng, cKey, , , cHelp)

    //Sen�o, ser� atualiza��o
    Else
        If lUpdate
            SPF_UPDATE(cFileEng, nRet, cKey, , , cHelp)
        EndIf
    EndIf


    // -- Espanhol
    nRet := SPF_SEEK(cFileSpa, cKey, 1)

    // -- Se n�o encontrar, ser� inclus�o
    If nRet < 0
        SPF_INSERT(cFileSpa, cKey, , , cHelp)

    // -- Sen�o, ser� atualiza��o
    Else
        If lUpdate
            SPF_UPDATE(cFileSpa, nRet, cKey, , , cHelp)
        EndIf
    EndIf

Return
