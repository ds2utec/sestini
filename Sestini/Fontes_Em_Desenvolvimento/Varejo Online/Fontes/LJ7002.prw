#include "totvs.ch"

/*/{Protheus.doc} LJ7002
Ponto de entrada apos as gravacoes das informacoes do cupom fiscal de venda

@author	Mauo Paladini 
@since 	14/02/2017
/*/

User Function LJ7002()

Local aArea             := GetArea()
Local cAliasTrb         := GetNextAlias()

Local aSL2Area          := SL2->( GetArea() )
Local aSD2Area          := SD2->( GetArea() )
Local cEspecie          := 'NFCE'
Local cSTrib

If ParamIXB[1] <> 2 
	Return( .t. )
Endif

If ParamIXB[3] <> 2
 	Return( .t. )
Endif
 
// Ajusta a situa��o tribut�ria na SL2
DbSelectArea('SL2')
DbSetOrder(1)
DbSeek(SL1->L1_FILIAL+SL1->L1_NUM)

Do While SL2->(L2_FILIAL+L2_NUM) == SL1->(L1_FILIAL+L1_NUM)

	DbSelectArea( 'SD2' )
	DbSetOrder(3)
	If DbSeek( xFilial('SD2') + SL1->(L1_DOC + L1_SERIE + L1_CLIENTE + L1_LOJA) + SL2->L2_PRODUTO + SL2->L2_ITEM ) .And. SD2->D2_PICM > 0
	
		cSTrib := 'T' + StrZero( SD2->D2_PICM * 100, 4, 0 )
		SL2->(RecLock('SL2',.f.))
		SL2->L2_SITTRIB := cSTrib
		SL2->(MsUnLock())
		
		RecLock('SD2',.f.)
		Replace D2_SITTRIB With cSTrib
		MsUnLock()
		
	Endif
	
	SL2->(DbSkip())
	
EndDo

// Atualiza a especie na SF2, SF3 e SFT

If !Empty( SL1->L1_SERSAT ) .Or. 'NFCE' $ Upper( SL1->L1_SERPDV )

	DbSelectArea( 'SF2' )
	DbSetOrder(1)
	If DbSeek( xFilial('SF2') + SL1->(L1_DOC + L1_SERIE + L1_CLIENTE + L1_LOJA) )
	
		RecLock('SF2',.f.)
		Replace F2_ESPECIE With cEspecie
		MsUnLock()
		
	Endif
	
	DbSelectArea( 'SF3' )
	DbSetOrder( 5 )
	DbSeek( xFilial('SF3') + SF2->(F2_SERIE + F2_DOC + F2_CLIENTE + F2_LOJA) )
	
	Do While SF3->( F3_FILIAL + F3_SERIE + F3_NFISCAL + F3_CLIEFOR + F3_LOJA ) == SF2->(F2_FILIAL + F2_SERIE + F2_DOC + F2_CLIENTE + F2_LOJA)
	
	 	RecLock('SF3')
	 	Replace F3_ESPECIE With cEspecie
	 	MsUnlock()
	 	DbSkip()
	 	
	Enddo

	DbSelectArea( 'SFT' )
	DbSetOrder( 3 )
	DbSeek( xFilial('SFT') + 'S' + SF2->(F2_CLIENTE + F2_LOJA + F2_SERIE + F2_DOC ) )
	
	Do While SFT->( FT_FILIAL + FT_TIPOMOV + FT_CLIEFOR + FT_LOJA + FT_SERIE + FT_NFISCAL ) == SF2->(F2_FILIAL + 'S' + F2_CLIENTE + F2_LOJA + F2_SERIE + F2_DOC)
	
	 	RecLock('SFT')
	 	Replace FT_ESPECIE With cEspecie
	 	MsUnlock()
	 	DbSkip()
	 	
	Enddo

Endif

RestArea(aSL2Area)
RestArea(aSD2Area)

RestArea(aArea)

Return