#Include 'Protheus.ch'
#INCLUDE "TBICONN.CH"

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE COMERRO   	"8"
#DEFINE EMPROCESSA	"9"

/*/{Protheus.doc} TibWfOrc

Workflow 
Chama a rotina de gera��o de or�amentos
Transforma os registros gravados nas tabelas da familia P  

P01 => SL1-Cabecalho
P02 => SL2-Itens
P03 => SL4-Meios de Pagamento

@author Mauro Paladini
@since 12/02/2015
@version 1.0

/*/

User Function TibWfOrc(CRPCEmp, cRPCFil)

Local lUsaMT			:= .F.
Local nQtdThread		:= 0
Local nQtdRegistros	:= 0
Local nRegsPorThread	:= 0
Local cLogL4			:= "\log_pdv\wfpagtos.log"
Local lCriouAmb		:= .F.

Private cLogT			:= "\log_pdv\wforcamentos.log"
Private nLastRow	:= 0 	

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
	RPCSetType(3) 
	RPCSETENV(cRPCEmp, cRPCFil,,,"LOJ")
	lCriouAmb	:= .T.
Endif 

lUsaMT			:= GetMv("FS_CATMT"	,,.F.)
nQtdThread		:= IIF( lUsaMT , GetMv("FS_NTORC",,5) , 0 )

If File(cLogT)
	FErase(cLogT)
Endif

// MultiThread
If nQtdThread > 0 

	//Obtem a quantidade de cupons a serem gravados como or�amento;
	nQtdRegistros := u_QtdCupom(VALIDADO) 
	
	If nQtdRegistros > 0
		
		//Se o n� de registros for maior que a qtd de threads � necess�rio o tratamento abaixo
		If nQtdRegistros > nQtdThread 
		
			nRegsPorThread := Round( (nQtdRegistros/nQtdThread),0 )
			IF !IsBlind()
				FWMsgRun( , {|| u_PackMT( nQtdThread, "u_TibMTOrcamento", { nRegsPorThread } ,"ORC" ) } ,, "Gerando registros ... "  )
			Else
				U_PackMT( nQtdThread, "u_TibMTOrcamento", { nRegsPorThread } ,"ORC" )			
			Endif
						
		//Se o n� de registros for menor ou igual a qtd de threads, sobe s� a qtd necess�ria.
		ElseIf nQtdRegistros <= nQtdThread 
		
			IF !IsBlind()
				FWMsgRun( , {|| U_PackMT( nQtdRegistros, "u_TibMTOrcamento", {1} , "ORC" ) } ,, "Gerando registros ... "  )
			Else
				U_PackMT( nQtdRegistros, "u_TibMTOrcamento", {1} ,"ORC" )		
			Endif

		EndIf

	Else
		If !IsBlind()
			MsgInfo("N�o existem cupons para serem Gerados!")
		Endif
	EndIf

Else

	// Gera os or�amentos como monothread - Processo normal.
	// Obtem a quantidade de cupons a serem validados;

	nQtdRegistros := U_QtdCupom(VALIDADO) 

	If nQtdRegistros > 0		
		IF !IsBlind()
			FWMsgRun( , {|| u_TibMTOrcamento( u_DivRegistros(0, "'" + VALIDADO + "'" ) ) } ,, "Gerando registros ... "  )
		Else
			u_TibMTOrcamento( u_DivRegistros(0, "'" + VALIDADO + "'") )		
		Endif
	Else
		If !IsBlind()
			MsgInfo("N�o existem cupons para serem Gerando!")
		Endif
	EndIf
	
EndIf
    
If !IsBlind()	
	Aviso("Aviso","Processo finalizado",{"OK"},2)
Endif

If lCriouAmb
	RESET ENVIRONMENT
Endif

Return


/*/{Protheus.doc} QtdCupom
Fun��o que busca a quantidade de cupons que ser�o gravados como or�amentos.
Serve para definir a quantidade de threads que devem criadas versus o parametro da qtde de threads

@author Leandro Prado
@since 24/02/2015
@version 1.0
@return nRet	Quantidade de registros na tabela com o status Informado
/*/

User Function QtdCupom(cStatus)

Local nRet 			:= 0
Local cP01xFilial	:= xFilial("P01")

Default cStatus	:= ""

// Select para retornar a quantidade de registros a serem processados.
BEGINSQL Alias "TRB1"

	SELECT	COUNT(*) AS QTDREG			
	FROM %table:P01%  P01 
	WHERE P01.P01_FILIAL = %exp:cP01xFilial% AND P01.D_E_L_E_T_ = ' '  
	AND P01.P01_STATUS = %exp:cStatus%

ENDSQL
	
DbSelectArea("TRB1")
TRB1->( DbGoTop() ) 

//Retorna a quantidade de registros a serem processados.
nRet	:= TRB1->QTDREG

TRB1->( DbCloseArea() )
	
Return nRet


/*/{Protheus.doc} DivRegistros
Divide os registros a serem processados
Serve para fazer o slice da tabela para cada thread.

@author Leandro Prado
@since 24/02/2015
@version 1.0
@param 	@nTop	- N�mero de registros que ser�o selecionados
@return @aRet	- Array com os par�metros da fun��o
/*/

User Function DivRegistros( nTop , cStatus )

Local aRet 		:= {}
Local cSQL		:= ""

Default	nTop 	:= 0
Default cStatus	:= "'0'"

//Se maior que zero significa que est� usando multithread. 
If nTop > 0 
	cSQL := " SELECT TOP " + AllTrim(Str(nTop)) + " Row, TRB.P01_ARQ, TRB.P01_DOC " + CRLF 
Else
	cSQL := " SELECT Row, TRB.P01_ARQ, TRB.P01_DOC " + CRLF 
EndIf

cSQL += " FROM ( " + CRLF
cSQL += " 		SELECT ROW_NUMBER() OVER(ORDER BY R_E_C_N_O_) AS Row, P01.P01_ARQ, P01.P01_DOC " + CRLF
cSQL += " 		FROM " + RETSQLNAME("P01") + " P01 " + CRLF
cSQL += " 		WHERE P01.P01_FILIAL = '" + xFilial("P01") + "'  AND P01.D_E_L_E_T_ = ' ' AND P01.P01_STATUS IN( " + cStatus + " ) ) TRB  " + CRLF
cSQL += " WHERE TRB.Row > " + Ltrim(Str(nLastRow,15)) + CRLF

// LOG 

LjWriteLog( cLogt, "Query -------" )
LjWriteLog( cLogT, cSQL )

DbUseArea(.T.,"TOPCONN",TcGenQry(,,cSQL),"TRB1",.T.,.F.)
	
TRB1->( DbGoTop() ) 

//Retorna a quantidade de registros a serem processados.
While TRB1->( !Eof() )
	
	nLastRow	:= TRB1->ROW

	//Add ao array a chave do registro, para que seja usada no processamento.
	aAdd( aRet, { TRB1->P01_ARQ, TRB1->P01_DOC } ) 
		
	TRB1->( DbSkip() )

EndDo

LjWriteLog( cLogT, "No Registros Def => " + cValToChar(nTop) + " aRet "  +  cValToChar(Len(aRet)) )
LjWriteLog( cLogT, "" )

TRB1->( DbCloseArea() )	
	
Return aRet






/*/{Protheus.doc} TibMTOrcamento
THREAD que efetivamente grava o orcamento 

@author Mauro Paladini
@since 06/03/2015
@version 1.0
@param aDados, 		array, Cupons a serem gravados como orcamento
@param aParamMT, 	array, Reservado MT

/*/
User Function TibMTOrcamento( aDados , aParamMT )

Local cArq		:= "" 
Local cCupom	:= ""
Local cLog		:= ""
Local nX		:= 0
Local cThread	:= IIF( aParamMT == Nil , "singleuser" , aParamMT[1] )
Local cLogMT	:= "\log_pdv\orcamento_thread_" + cThread + ".log"
Local lRet		:= .T.
Local lRpcClear	:= .F.

Local tInicio	
Local tFim	

Private aResume	:= {}

MakeDir("\log_pdv") 
If File(cLogMt)
	FErase(cLogMT)
Endif

LjWriteLog(cLogMT,"Iniciando geracao de orcamentos .. [" + cThread + "] ")

For nX := 1 To Len(aDados)

	cArq	:= aDados[nX][1] 
	cCupom	:= aDados[nX][2]
	tInicio	:= Time()
	
	P01->( DbSetOrder(6) )
	If P01->( DbSeek( xFilial("P01") + cArq + cCupom ) )	
		u_TibGrvOrc( cArq ,cCupom, @lRet , @lRpcClear )
	Endif
	
	// Em desenvolvimento tratamento de ERROS da MsExecAuto
	
	IF lRpcClear	
		cEmpRpc	:= cEmpAnt
		cFilRpc	:= cFilAnt			
	Endif

	tFim := ElapTime(tInicio,Time())	
	cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 

	LjWriteLog(cLogMT, "Cupom " + cCupom + " Orcamento " + P01->P01_NUMORC + IIF( lRet , " [OK] " , " [ERRO] " ) + cLog )
		 
Next nX

If aParamMT <> Nil
	PutGlbValue( aParamMT[1] , "4" ) // STATUS 4 - Finalizado
	GlbUnLock()
EndIf

Return