#Include 'Protheus.ch'

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE CANCELADO	"5"
#DEFINE COMERRO		"8"
#DEFINE EMPROCESSA	"9"


/*TibValCupom
Faz a valida��o dos registros gravados nas tabelas da familia 'P'

@author Mauro Paladini
@since 12/02/2015
@version 1.0
*/

User Function TibValCupom( lProcessou )

Local aArea		:= GetArea()
Local lCupomOk	:= .T.
Local cMsg		:= ""

Default lProcessou	:= .T.

// Valida estorno
IF P01->P01_ESTORN == "S"

	IF P02->( DbSeek( xFilial("P02") + P01->P01_ARQ + P01->P01_DOCCOO ) )

		While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + P02_DOCCOO == xFilial("P02") + P01->P01_ARQ + P01->P01_DOCCOO )
		
			RecLock("P02",.F.)
				P02->P02_STATUS	:= CANCELADO
				P02->P02_LOGIMP	:= "Cupom cancelado" 						
			MsUnLock()					
			
			P02->( DbSkip() )
			
		End
	
	Endif

	// Atualiza status
	RecLock("P01",.F.)
		P01->P01_STATUS	:= CANCELADO
		P01->P01_LOGIMP	:= "Cupom cancelado"					
	MsUnLock()
	
	RestArea(aArea)

	Return		

Endif

// Inicia a validacao do cupom fical 
// Se validado posteriormente podera ser transformado em Orcamento de venda


u_ValidaCupom( P01->P01_ARQ , RTRim(P01->P01_DOCCOO) , @lCupomOk, @cMsg )

lProcessou	:= lCupomOk
		
RestArea(aArea)
				
Return





/*/{Protheus.doc} ValidaCupom
Valida as informacoes basicas de todos os registros importados do cupom fiscal 

@author Mauro Paladini
@since 20/02/2015
@version 1.0

@param cArqImp, character, 	Arquivo importado que deu a origem ao registro
@param cCupom, character,	Numero do cupom fiscal de venda
@param lRet, ${param_type}, Retorno logico indicando se o registro foi ou n�o validado com sucesso

/*/

User Function ValidaCupom( cArqImp, cCupom, lRet, cMsgCupom )

Local aAreas  		:= { P01->(GetArea()) , P01->(GetArea()) , P03->(GetArea()) , SL1->(GetArea()) , SL2->(GetArea()) , GetArea() }
Local aArea			:= GetArea()
Local aAliq			:= {}

Local lPgtoOk			:= .T.
Local lErros			:= .F.
Local cLogAut			:= ""
Local lIncProduto		:= GetMv("FS_AUTOSB1",,.T.)			// Inclui produto automaticamente
Local lAutoTes			:= GetMv("FS_AUTOTES",,.T.)			// Usa TES padrao caso o produto esteja em branco
Local lAutoImpos		:= GetMv("FS_AUTOALQ",,.T.)			// Ajusta a aliquota do produto caso esteja em branco 
Local cFS_TesSai		:= GetMv("FS_TESSAI",,"501")
Local lVldPreco			:= GetMv("FS_VLDPRV",,.T.)
Local cFS_PGTODI		:= GetMv("FS_PGTODI",,"ContaAssin")	// Formas de pagamento que exigam o CPF no momento da venda
Local cUMPad			:= GetMv("FS_UMPAD"	,,"UN")			// Unidade de medida padrao
Local cB1LocPad			:= GetMv("FS_LOCPAD",,"01")			// Local Padrao

// -- Variaveis relacionadas a Ordem de Producao

Local cProdPai		:= ""
Local cItemPai		:= ""
Local cGrpOpc			:= ""
Local cItemOpc		:= ""
Local lOpcional		:= .F.

Local lVerEst 		:= ( GetMv("MV_ESTNEG") == "N" )
Local lObsCupom		:= .F.

Local aSB0			:= {}
Local aSB1			:= {}
Local cMsg			:= ""
Local nSdoSb2			:= 0
Local cDescProd		:= ""
Local lContaAss		:= .F.
Local cCaixa			:= ""
Local cCaixaSup		:= "" 
Local nDescPer		:= 99.99
Local nDescLoj		:= 0
Local aDesc			:= { "V", nDescLoj }	
Local cNomeCaixa		:= ""
Local cNumOrc			:= ""

Local cSX5              := GetNextAlias()
Local aNCC              := {}
Local nNCCVenda         := 0
Local aMDJ              := {}

Private lMsErroAuto	:= .F.
Private cLogValid		:= "\logpdv\validacoes\" + AllTrim(P01->P01_FILIAL) + "\" + AllTrim(cCupom) + ".log"

Default cArqImp		:= Space(TamSx3("P01_ARQ")[1])
Default cCupom 		:= Space(TamSx3("P01_DOCCOO")[1])
Default lRet			:= .T.
Default cMsgCupom		:= ""


SB1->( DbSetOrder(1) )
P02->( DbSetOrder(6) )
P03->( DbSetOrder(6) )
SAE->( DbSetOrder(3) )


// -- Valida confiura��es do ambiente SIGALOJA

IF !u_LjVldAmbiente( .F. /*lOrcamento*/ , @lRet, @cMsgCupom , @cCaixa , cArqImp , cCupom , cNumOrc , @cNomeCaixa , @cCaixaSup  )	
	AEval(aAreas, {|x| RestArea(x)})
	Return lRet
Endif


// -- Inicia as validacoes
// -- Tenta identificar o cliente atraves do CPF

IF lRet

	SA1->( DbSetOrder(3) )
	
	IF !Empty( AllTrim( P01->P01_CGC ) )
	
	    IF SA1->( DbSeek( xFilial("SA1") + P01->P01_CGC ) )
	
	    	RecLock("P01",.F.)
	    		P01->P01_CLIENT  := SA1->A1_COD 
	    		P01->P01_LOJA	   := SA1->A1_LOJA 
	    	MsUnLock()
	
	    Else
	
	        RecLock("P01",.F.)
	            P01->P01_CLIENT := GetMv("MV_CLIPAD") 
	            P01->P01_LOJA   := GetMv("MV_LOJAPAD") 
	        MsUnLock()
	
	    Endif
	
	Else
	
	    RecLock("P01",.F.)
	        P01->P01_CLIENT := GetMv("MV_CLIPAD") 
	        P01->P01_LOJA   := GetMv("MV_LOJAPAD") 
	    MsUnLock()
	
	Endif

Endif

SA1->( DbSetOrder(1) )

// -- Valida os itens do cupom fiscal

IF lRet .And. P02->( DbSeek( xFilial("P02") + cArqImp + cCupom ) )

	nTotItem  := 0

	While P02->( !Eof() .And.  P02_FILIAL + P02_ARQ + P02_DOCCOO == xFilial("P02") + cArqImp + AvKey( cCupom  , "P02_DOCCOO" )  )
	
		u_WriteLog( cLogValid , "TibValidaRegistros.prw  -------------------------" )
	
		lRet			:= .T.
		cMsg			:= ""
		
		cDescProd		:= ""
		cProdPai		:= ""
		cCodParte		:= ""
		cItemPai		:= ""
		cGrpOpc			:= ""
		cItemOpc		:= ""
		lOpcional		:= .F.
		lObsCupom		:= .F.
		nVlrItem		:= P02->P02_VALLIQ
		
		aAliq			:= RetAliquota( P02->P02_CODTOT )		
		nAliqIcms		:= aAliq[1]
		nAliqISS 		:= aAliq[2]
						
		
		// -- Valida Produto cadastrado
		
		IF lRet .And. SB1->( DbSeek( xFilial("SB1") + P02->P02_PROD ) )
		
			cDescProd	:= SB1->B1_DESC
						   
		   	If lRet .And. lVerEst
		   
			   SB2->(Dbsetorder(1))
			   
			   If SB2->(Dbseek( xFilial("SB2") + SB1->B1_COD + SB1->B1_LOCPAD ))
			      
			      nSdoSB2	:= SaldoSB2() 		      
			      If nSdoSB2 < P02->P02_QUANT
						lRet	:= .F.
						cMsg	+= "Linha " + P02->P02_LINHA + " Estoque insuficiente. Quantidade dispon�vel " + cValToChar(nSdoSB2) + CRLF														      		      
			      Endif
			      
			   Else
					lRet	:= .F.
					cMsg	+= "Linha " + P02->P02_LINHA + " Produto n�o poss�i armaz�n criado na SB2 " + CRLF														      		      
			   Endif
		   
		   Endif
		   			
		Else
		
			u_WriteLog( cLogValid , "Produto	:= " + P02->P02_PROD + "-" + LEFT(P02->P02_DESCRI,20) + " ## [NAO CADASTRADO]" )
			
			lRet := .F.
					
			IF lIncProduto
			
				If !( NNR->(MsSeek(xFilial("NNR") + cB1LocPad )))
					RecLock("NNR",.T.)  
						NNR->NNR_FILIAL	:= xFilial("NNR")
						NNR->NNR_CODIGO	:= cB1LocPad
						NNR->NNR_DESCRI	:= "Armaz�m " + cB1LocPad
					MsUnlock()
				Endif
						
				//Informacoes da tabela SB1
				aSB1 := {}
				aAdd( aSB1, { "B1_COD"		, P02->P02_PROD 					, Nil } ) 	// Codigo do produto
				aAdd( aSB1, { "B1_DESC"		, P02->P02_DESCRI					, Nil } ) 	// Descricao do Produto
				aAdd( aSB1, { "B1_TIPO"		, "PA"								, Nil } ) 	// Tipo do produto
				aAdd( aSB1, { "B1_UM"		, P02->P02_UM 					, Nil } ) 	// Unidade de medida
				aAdd( aSB1, { "B1_LOCPAD"	, cB1LocPad						, Nil } ) 	// Armazem Padrao
				aAdd( aSB1, { "B1_TS"		, "501"							, Nil } )	// Tes de Saida
				aAdd( aSB1, { "B1_PRV1"		, P02->P02_PRUNIT					, Nil } )	// Origem do Produtos	
				aAdd( aSB1, { "B1_PICM"		, nAliqIcms						, Nil } )	// Aliquota de ICMS
				aAdd( aSB1, { "B1_ALIQISS"	, nAliqISS							, Nil } )	// Aliquota de ISS
				aAdd( aSB1, { "B1_ORIGEM"	, "0"								, Nil } )	// Origem do Produtos
				
				//Informacoes da tabela SB0
				aSB0 := {}
				aAdd( aSB0, {} )
				aAdd( aSB0[1], { "B0_PRV1", P02->P02_PRUNIT , Nil } ) //Preco de venda 1
				
				//Chamada Rotina automatica
				lMsErroAuto	:= .F.
				MSExecAuto( {|X,Y,Z| LOJA110(X,Y,Z)}, aSB1, aSB0, 3 ) 
				
				If lMsErroAuto
					cLogAut	:= MostraErro()				
					lRet	:= .F.
					cMsg	+= "Linha " + P02->P02_LINHA + " Erro ao incluir o produto " + CRLF + cLogAut							
				Else
					lRet	:= .T.
				Endif
										
			Else				
				lRet	:= .F.
				cMsg	+= "Linha " + P02->P02_LINHA + " Produto n�o cadastrado " + RTrim(P02->P02_PROD) + CRLF			
			Endif
			
		Endif
								
		RecLock("P02",.F.)
			
			P02->P02_STATUS	:= IIF( lRet , VALIDADO , INVALIDO )			
			P02->P02_LOGIMP	:= cMsg
						
			// -- Complementa gravacao
						
			P02->P02_DTEMIS	:= P01->P01_DTEMIS
							
			// -- Atualiza descricao
			
			IF lRet
                P02->P02_DESCRI     := cDescProd		
			Endif				

		MsUnLock()
				
		If !lRet
			lErros	:= .T.
		Endif		
						
		nTotItem := A410Arred( nTotItem + nVlrItem  , "LR_VLRITEM" ) 
		
		P02->( DbSkip() )
		
	End
	
	If lErros .Or. !lRet 
		
		lRet		:= .F.
		lErros		:= .T.

		cMsgCupom	+= "Ocorreram problemas na valida��o dos produtos. " + CRLF
		cMsgCupom	+= cMsg	

		u_CaUpdate( .F. /*lSucesso*/ , INVALIDO , cArqImp , cCupom , "" , cMsgCupom)
		
		AEval(aAreas, {|x| RestArea(x)})

		Return .F.

	Endif

Else

	lRet := .F.
	u_CaUpdate( .F. /*lSucesso*/ , INVALIDO , cArqImp , cCupom , "" , "Itens do cupom fiscal n�o encontrados na tabela P02" )
	
	AEval(aAreas, {|x| RestArea(x)})
	
	Return .f.

Endif


// Valida meios de pagamento

IF lRet .And. P03->( DbSeek( xFilial("P03") + cArqImp + cCupom ) )
		
	lPgtoOk		:= .T.	
	nTotPagtos	:= 0
	
	SAE->( DbSetOrder(3) )
	
	While P03->( !Eof() .And.  P03_FILIAL + P03_ARQ + P03_DOCCOO == xFilial("P03") + cArqImp + AvKey( cCupom  , "P03_DOCCOO" )  )

        Msg 	:= "" 

        IF !Empty(RTrim(P03->P03_FORMPG)) .And. SAE->( DbSeek( xFilial("SAE") + RTrim(P03->P03_FORMPG) ) )
        
            cL4Forma    := RTrim(SAE->AE_TIPO)
            cDescFor    := Posicione("SX5", 1, xFilial("SX5")+ "24" + cL4Forma , "X5_DESCRI")
            cL4AdmFin   := SAE->AE_COD + "-" + SAE->AE_DESC
            nL4Valor    := P03->P03_VALOR

        Else
	
           // -- Identifica Meio de Pagamento
                
            BEGINSQL Alias cSX5                 
            SELECT TOP 1 X5_CHAVE, X5_DESCRI
            FROM   %table:SX5% SX5
                   WHERE   X5_FILIAL = %xFilial:SX5% AND
                           X5_DESCRI = %Exp:UPPER(RTrim(P03->P03_FORMPG))% AND
                           SX5.D_E_L_E_T_ = ''                                                      
            ENDSQL
            
            (cSX5)->( DbGoTop() )
            
            If (cSX5)->( !Eof() )                
                cL4Forma    := RTrim((cSX5)->X5_CHAVE ) 
                cDescFor    := RTrim((cSX5)->X5_DESCRI )
                cL4AdmFin   := ""  
                nL4Valor    := P03->P03_VALOR                                           
            Else            

                lRet        := .F.
                cMsg        := "Forma de pagto n�o localizada na tabela 24." + CRLF
                lPgtoOk     := .F.      
                cL4Forma    := ""
                cDescFor    := ""
                cL4AdmFin   := ""               
                nL4Valor    := P03->P03_VALOR                
            Endif   
            
            (cSX5)->( DbCloseArea() )      
        
        Endif    
		
         RecLock("P03",.F.)              
            P03->P03_FORMA  :=  cL4Forma    
            P03->P03_DESFOR := RTrim(cDescFor)          
            P03->P03_CODADM := RTrim(SAE->AE_COD)
            P03->P03_DESADM := RTrim(SAE->AE_DESC)
            P03->P03_STATUS	:= IIF( Empty(cMsg) , VALIDADO , INVALIDO )
            P03->P03_LOGIMP	:= cMsg			
		P03->( MsUnlock() )
		
		nTotPagtos   := A410Arred( nTotPagtos + nL4Valor  , "P03_VALOR" )
			
		P03->( DbSkip() )
	
	End

Else
	
	lPgtoOk	:= .F.
	
Endif

IF nTotPagtos <> nTotItem
	cMsgCupom	+= "Total de pagamentos � diferente do total da venda." + CRLF
	lPgtoOk	:= .F.
Endif

If !lPgtoOk 

	cMsgCupom	+= "Meios de pagamento n�o encontrados" + CRLF
	lRet		:= .F.

	u_CaUpdate( .F. /*lSucesso*/ , INVALIDO , cArqImp , cCupom , "" , cMsgCupom ) 

	AEval(aAreas, {|x| RestArea(x)})

	Return .F.

Endif 


// -- Atualiza os registros do cupom fiscal apos validacao 

u_CaUpdate( lRet /*lSucesso*/ , IIF( lRet , VALIDADO , INVALIDADO )  , cArqImp , cCupom , "" , IIF( lRet , "" , cMsgCupom ) ) 


RestArea(aArea)
AEval(aAreas, {|x| RestArea(x)})

Return lRet




/*/{Protheus.doc} RetAliquota
Identifica qual a aliquota utilizada no item do cupom fiscal
Retorna um array com a aliquota de ICMS ou ISS utilizada

@author Mauro Paladini

@since 20/02/2015
@version 1.0
@param cTotalizador, character, Elemento registrado no item do cupom fiscal

@return ${aRet[1]}, Aliquota de ICMS
@return ${aRet[2]}, Aliquota de ISS

/*/

Static Function RetAliquota( cTotalizador )

Local nAliq	:= 0
Local aRet	:= { 0 , 0 }

Default  cTotalizador	:= ""

IF Substr(cTotalizador,3,1) $ "T|S" 
	
	nAliq := Val( Substr(cTotalizador,4,2) + "." + Right(cTotalizador,2) )
	
	IF Substr(cTotalizador,3,1) == "T" 
		aRet[1]	:= nAliq
	Else
		aRet[2]	:= nAliq	
	Endif 	
		        
Endif

Return aRet








/*/{Protheus.doc} LjVldAmbiente
Valida todas as premissas para realizar um atendimento via SIGALOJA

@author Mauro Paladini

@since 20/02/2015
@version 1.0
@param cTotalizador, character, Elemento registrado no item do cupom fiscal

@return ${aRet[1]}, Aliquota de ICMS
@return ${aRet[2]}, Aliquota de ISS

/*/

User Function LjVldAmbiente( lOrcamento , lRet, cMsgCupom , cCaixa , cArqImp , cCupom , cNumOrc , cNomeCaixa , cCaixaSup , cCodEstacao )

Local aArea			:= GetArea()
Local nVlrUsuario		:= 0 
Local nDescPer		:= 99.99   
Local NoSerieSAT        := Substr( LTrim(P01->P01_SATQUP) , 1 , Len(AllTrim(P01->P01_SATQUP)) - 1 ) 
Local cSerieCF          := Right( AllTrim(P01->P01_SATQUP) , 1 )

Default lOrcamento		:= .F. 
Default lRet 			:= .T.
Default cMsgCupom 		:= ""
Default cCaixa 		:= ""
Default cArqImp  		:= P01->P01_ARQ
Default cCupom  		:= P01->P01_DOCCOO
Default cNumOrc 		:= ""
Default cNomeCaixa 	:= ""
Default cCaixaSup		:= ""
Default cCodEstacao	:= ""

If !Empty(P01->P01_NUMORC)
	lOrcamento		:= .T.
	cNumOrc		     := P01->P01_NUMORC
Endif

SLG->( DbOrderNickName("SAT0000001") ) 

IF SLG->( !DbSeek( P01->P01_FILIAL + AvKey( NoSerieSAT , "LG_SERSAT" ) +  AvKey( cSerieCF , "LG_SERIE" ) ) ) 

	lRet 		:= .F.	
	cMsgCupom	:= "ECF ou SAT nao existe no cadastro de esta��es" + CRLF

Else

	// Valida campos obrigat�rios da estacao
	IF Empty(SLG->LG_PDV)	
		lRet 		:= .F.	
		cMsgCupom	:= "PDV em branco no cadastro de esta��es" + CRLF 	
	Endif

	IF Empty(SLG->LG_SERIE)	
		lRet 		:= .F.	
		cMsgCupom	:= "S�rie em branco no cadastro de esta��es" + CRLF 	
	Endif

	IF Empty(SLG->LG_XNUMCX)	
		lRet 		:= .F.	
		cMsgCupom	:= "N�mero do operador de caixa em branco no cadastro de esta��es" + CRLF
	Endif	
	
	IF lRet
		
		RecLock("P01",.F.)
			P01->P01_LGPDV	:= SLG->LG_PDV 
			P01->P01_LGSERI	:= SLG->LG_SERIE 
			P01->P01_LGCOD	:= SLG->LG_CODIGO 	
			P01->P01_OPERAD 	:= SLG->LG_XNUMCX					
		MsUnLock()
		
		cCaixa			:= RTrim(SLG->LG_XNUMCX)
		cCodEstacao		:= SLG->LG_CODIGO
			
	Endif

Endif 

If !lRet

	u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc , cMsgCupom )

	RecLock("P01",.F.)
		P01->P01_LGPDV	:= "" 
		P01->P01_LGSERI	:= "" 
		P01->P01_LGCOD	:= "" 	
		P01->P01_OPERAD 	:= ""					
	MsUnLock()

	RestArea(aArea)
	Return .F.
	
Endif


// -- Verfica se usuario esta cadastrado como caixa.

SX5->( DbSetOrder(1) )
IF SX5->( DbSeek( FwxFilial("SX5", P01->P01_FILIAL ) + "23" + RTrim(cCaixa) ) ) 

	cNomeCaixa		:= UPPER( ALLTRIM( X5Descri() ) )

Else

	lRet		:= .F.
	cMsgCupom	:= "Revise a configura��o de usu�rios"  + CRLF 
	cMsgCupom	+= "Usuario " + cCaixa + " n�o encontrado na tabela 23-Caixa do Configudador (SX5). " 
		
	u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc , cMsgCupom )
	
	RestArea(aArea)
	
	Return .F.

Endif 


// -- Valida cadastro correto do Caixa no ambiente SIGALOJA

If !IsCaixaLoja( cCaixa ) 

	cMsgCupom	:= "Revise a configura��o de usu�rios"  + CRLF
	cMsgCupom	+= "O usu�rio " + cCaixa  + " n�o poder�efetuar atendimentos. Utilize a op��o Senhas/Caixas no Menu Miscel�nea para incluir um Caixa. Caso j� exista um cadastrado, re-entre no sistema com uma senha de Caixa."
	lRet		:= .F.
	u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc , cMsgCupom ) 

	RestArea(aArea)
	
	Return .F.

EndIf


// -- Valida Permissao para descontos

SA6->( DbSetOrder(1) )


IF SA6->( DbSeek( FwxFilial("SA6", P01->P01_FILIAL ) + RTrim(cCaixa) ) )

	cCaixaSup		:= RTrim(SA6->A6_NOME) 	
	nVlrUsuario 	:= LjGetProfile( "DESCPER" , RTrim(cCaixa) )
	
	If nVlrUsuario < nDescPer  

		lRet		:= .F.
		u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc ,  "Operador de caixa " + cCaixa +  " n�o poss�i permiss�o para efetuar descontos em %. Limite atual de R$ " + AllTrim(Str(nVlrUsuario,6,2)) ) 
		RestArea(aArea)
		
		Return .F.
	
	Endif

Else

	lRet		:= .F.
	u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc , "Usuario " + cCaixa +  " n�o localizado na tabela SA6 Bancos.  Revise o cadastro de usu�rios e as tabelas relacionadas. " ) 
	RestArea(aArea)
	
	Return .F.

Endif


// -- Valida se o cupom ja existe na base de dados

SL1->( DbSetOrder(2) )

IF lRet .And. SL1->( DbSeek( P01->P01_FILIAL + AvKey( P01->P01_LGSERI , "L1_SERIE" 	) + AvKey( P01->P01_DOCCOO  , "L1_DOC" ) +  AvKey( P01->P01_LGPDV  , "L1_PDV" ) ) )	

	lRet 		:= .F.	
	
	cMsgCupom	:= "Or�amento " + SL1->L1_NUM + " j� existe na base de dados para este n�mero de Cupom Fiscal." + CRLF 	
	
	lOrcamento	:= .T.

	u_CaUpdate( .F. /*lSucesso*/ , IIF( lOrcamento , COMERRO , INVALIDO ) , cArqImp , cCupom , cNumOrc , cMsgCupom )
	
	RestArea(aArea)
	
	Return .F.

Endif 	

Return .T.