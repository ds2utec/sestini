#Include 'Protheus.ch'
#include 'tbiconn.ch'

/*
---- CONFIGURA��O NO APPSERVER.INI ------

[TScript]
TOPDataBase=MSSQL
TOPServer=127.0.0.1
TOPAlias=Sestini_Integra
TOPPort=7890

*/

Static cCFGLog      := Str(Year(Date()),4) + '-' + CMonth(Date()) + '-' + 'stnfor_integration.log' 
Static cDirLog      := '\launcher\' + Str(Year(Date()),4) + '\'


/*/{Protheus.doc} SAUTOFOR
Fun��o de usu�rio auxiliar que prepara o objeto da FWExecAuto para integra��o.

Utiliza��o -> Gravada no campo PV9_FUNOPC no momento da inser��o do registro pela classe TransactRows()

@type 		User Function
@author 	Mauro Paladini
@since 		Aug 17, 2018  2:33:31 PM
@version 	1.0
@return 	${return}, ${return_description}

/*/

User Function SAUTOFOR()

Local   aAreas          := { PV1->(GetArea()) , PV2->(GetArea()) , PV9->(GetArea()) , GetArea() }
Local   cFuncAux        := 'U_SAUTOFOR'
Local   cIdMapFld       := GetMv('ES_MFOR2',,'0000000002')
Local 	nX				:= 0
Local   aAux			:= {}
Local 	cKeyPV9         := PV9->PV9_SEQ
Local   cOrigem			:= PV9->PV9_ORIGEM
Local   cOperacao		:= PV9->PV9_OPERAC
Local   cChave			:= PV9->PV9_CHAVE
Local   cTabOrig		:= PV9->PV9_TABORI
Local   nIndOri			:= PV9->PV9_INDORI

Local   cMapSeek        := xFilial('PV5') + AvKey( cFuncAux , 'PV5_FUNOPC' ) + cIdMapFld
Private cJobLog         := cDirLog + cCFGLog 

MontaDir(cDirLog)

// -- Valdar Mapa de Campos

If Empty ( cIdMapFld ) .OR. PV5->( ! DbSeek( cMapSeek ) )  
    u_WriteLog( cJobLog  , '[' + cFuncAux + '] Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek  ,.T.,.T. )
    Return { .F., 'Mapa de campos n�o localizado na PV5'  + ' Id: ' + cMapSeek }
Endif

// -- Instancia o objeto da classe FwExecauto
oFWExecAuto := FwExecAuto():New( 'PV4' , '' , cOrigem , cKeyPV9 , cFuncAux , cIdMapFld , .T. /*lRestAreas*/ , /*aSaveAlias*/ , .F. /*lBlankFields*/ )
oFWExecAuto:ParamFun := { 'aCabAuto' , cOperacao , '' , '' , '' , '' }

// -- Faz o load dos registros para o objeto e executa a Execauto
IF oFWExecAuto:Load()

	If cOperacao == '4'
	    (cTabOrig)->( DbSetOrder(nIndOri) )            
	    IF (cTabOrig)->( DbSeek( cChave ) )	
	    	oFWExecAuto:Execute()	    	
	    Endif
    Else
    	oFWExecAuto:Execute()    
    Endif

Endif

IF !oFWExecAuto:lResult
	u_WriteLog( cDirLog + cJobLog  , '[' + cFuncAux + '] ' + oFWExecAuto:Message ,.T.,.T. )
Endif

AEval(aAreas, {|x| RestArea(x)})

Return { oFWExecAuto:lResult , oFWExecAuto:Message }