#INCLUDE "TOTVS.CH" 
#INCLUDE "FWMVCDEF.CH"

/*
USTNCFOP
Cadastro De/Para de TES para CFOP

@author 	Mauro Paladini
@since 		20/04/2017
@version 	1.0
*/

User Function USTNCFOP()

Local oBrowse 	:= Nil

Private aRotina := MenuDef()

oBrowse:= FWMBrowse():New()
oBrowse:SetAlias("PVC")
oBrowse:SetDescription("[VAREJO ONLINE] Configurações por CFOP")
oBrowse:DisableDetails()
oBrowse:Activate()

Return Nil



/*
ModelDef
Funcao MVC para criar o modelo de dados

@author 	Mauro Paladini
@since 	20/04/2017
@version 	1.0
*/           

Static Function ModelDef()

Local oModel		:= Nil

Local oStruPVC		:= FWFormStruct(1,"PVC") 

Local cCmpFil 		:= ""
Local bPreValid		:= Nil
Local bPosValid 	:= Nil
Local bCommit 		:= Nil
Local bCancel		:= Nil  

oStruPVC:AddTrigger( "PVC_TES", "PVC_DESC", {|| .T.},;                                                                    
							 { |oModel| Padr( Posicione("SF4",1, FWFldGet("PVC_TES")  ,"F4_TEXTO") , TamSx3("PVC_DESC")[1]) } )

// Criacao da Modelo
oModel:= MpFormMOdel():New("STNCFOP",  /*bPreValid*/ , /*bPosValid*/ , /*bCommit*/ ,/*bCancel*/ )

// Cabecalho
oModel:AddFields("PVCMASTER",Nil,oStruPVC,/*prevalid*/,,/*bCarga*/)
oModel:SetDescription("Grupo de Empresas")
oModel:SetPrimaryKey( { "PVC_FILIAL" , "PVC_CFOP"  } )

Return ( oModel )



/*
ViewDef
Funcao MVC para criar as Views

@author 	Mauro Paladini
@since 	20/04/2017
@version 	1.0
*/

Static Function ViewDef()

Local oModel            := FwLoadModel("STNCFOP")
Local oStruPVC          := FWFormStruct(2,"PVC") 
Local cCmpFil           := ""
Local oView             := Nil

oStruPVC:RemoveField("PVC_FILIAL")

// Instacia a View
oView := FwFormView():New()
oView:SetModel(oModel)

// Cabecalho
oView:AddField('VW_PVCMASTER', oStruPVC , 'PVCMASTER') 

oView:CreateHorizontalBox("SUPERIOR",100)

// Define 
oView:SetOwnerView('VW_PVCMASTER',"SUPERIOR")

// Habilita titulos para melhor visualizacao do usuario
oView:EnableTitleView('VW_PVCMASTER',"[Configuração]")

Return(oView)



/*
MenuDef
Funcao generica MVC com as opcoes de menu

@author 	Mauro Paladini
@since 	20/04/2017
@version 	1.0
*/

Static Function MenuDef()

Local aRotina := {}

ADD OPTION aRotina TITLE "Pesquisar"		ACTION "PesqBrw"         OPERATION 1 ACCESS 0  //"Pesquisar"
ADD OPTION aRotina TITLE "Visualizar" 		ACTION "VIEWDEF.STNCFOP" OPERATION 2 ACCESS 0  //"Visualizar"
ADD OPTION aRotina TITLE "Incluir" 			ACTION "VIEWDEF.STNCFOP" OPERATION 3 ACCESS 0  //"Incluir"
ADD OPTION aRotina TITLE "Alterar" 			ACTION "VIEWDEF.STNCFOP" OPERATION 4 ACCESS 0  //"Alterar"
ADD OPTION aRotina TITLE "Excluir" 			ACTION "VIEWDEF.STNCFOP" OPERATION 5 ACCESS 0  //"Excluir"

Return ( aRotina )
 
 
 
 