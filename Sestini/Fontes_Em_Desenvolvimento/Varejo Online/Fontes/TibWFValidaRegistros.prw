#Include 'Protheus.ch'
#include 'tbiconn.ch'

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE COMERRO   	"8"
#DEFINE EMPROCESSA	"9"

/*/{Protheus.doc} TibWfValCupom

Workflow 
Chama a rotina de validacao dos registros importados pela CAT 52
Os registros estando aptos e validados poderao ser transformados como orcamento

@author Mauro Paladini
@since 12/02/2015
@version 1.0

/*/

User Function TibWfValCupom(CRPCEmp, cRPCFil)

Local cAlias		  
Local nQtdThread
Local nQtdRegistros     := 0
Local nRegsPorThread    := 0
Local lCriouAmb         := .F.

Local lIncPrecos    := .T.
Local lIncSaldo     := .T.
Local lRet          := .T.

Private cLogT			:= "\log_pdv\wfvalida.log"
Private nLastRow	:= 0 	

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
	RPCSetType(3) 
	RPCSETENV(cRPCEmp, cRPCFil)
	lCriouAmb	:= .T.
Endif 

lUsaMT			:= GetMv("FS_CATMT"	,,.F.)
nQtdThread		:= IIF( lUsaMT , GetMv("FS_NTVLD",,5) , 0 )
cAlias			:= GetNextAlias()
lIncPrecos		:= GetMv("FS_AUTOSB0",,.T.)		// Inclui produto automaticamente
lIncSaldo		     := GetMv("FS_AUTOSB2",,.T.)		// Inclui produto automaticamente

// Avalia se existe armazen criado para os produtos a serem produzidos
If lIncSaldo

	LjWriteLog(cLogT,"Verificando tabelas de produtos x saldos SB2" )

	BEGINSQL Alias cAlias
				
		SELECT B1_COD, B1_LOCPAD, B2_COD	
		FROM ( 	SELECT B1_COD, B1_LOCPAD, B2_COD 	
		FROM %table:SB1% SB1	
		
		LEFT OUTER JOIN %table:SB2% SB2	
		ON SB2.B2_FILIAL = %xFilial:SB2% AND SB2.B2_COD = SB1.B1_COD AND SB2.B2_LOCAL = SB1.B1_LOCPAD AND SB2.D_E_L_E_T_ = ' ' 
		
		WHERE SB1.D_E_L_E_T_ = ' ' ) TRB
		
		WHERE TRB.B2_COD IS NULL 
		
	ENDSQL
		
	(cAlias)->( DbGoTop() )
	While (cAlias)->( !Eof() ) 
		
		If !( NNR->(MsSeek(xFilial("NNR") + (cAlias)->B1_LOCPAD )))
			RecLock("NNR",.T.)  
				NNR->NNR_FILIAL	:= xFilial("NNR")
				NNR->NNR_CODIGO	:= (cAlias)->B1_LOCPAD
				NNR->NNR_DESCRI	:= "Armaz�m " + (cAlias)->B1_LOCPAD
			MsUnlock()
		Endif
				
		If !SB2->( MsSeek( xFilial('SB2') + (cAlias)->B1_COD + (cAlias)->B1_LOCPAD ))
			CriaSb2( (cAlias)->B1_COD, (cAlias)->B1_LOCPAD )		
			SB2->( MsUnLock() )
		Endif                                               	
	
		(cAlias)->( DbSkip() )		
	
	End 	
	(cAlias)->( DbCloseArea() )

Endif


If lIncPrecos

	LjWriteLog(cLogT,"Verificando tabelas de produtos x pre�os SB0" )

	cAlias			:= GetNextAlias()

	// Valida Preco de Venda na SB0
	BEGINSQL Alias cAlias
				
		SELECT DISTINCT B1_COD, B1_PRV1, B0_COD, P02_PRUNIT

		FROM ( 	SELECT B1_COD, B1_PRV1, B0_COD, P02_PRUNIT 	
		FROM %table:SB1% SB1	
		
		LEFT OUTER JOIN %table:SB0% SB0
		ON SB0.B0_FILIAL =  %xFilial:SB0% AND SB0.B0_COD = SB1.B1_COD AND SB0.D_E_L_E_T_ = ' ' 
		
		INNER JOIN %table:P02% P02
		ON P02.P02_FILIAL = %xFilial:P02% AND P02.P02_PROD = SB1.B1_COD AND P02.D_E_L_E_T_ = ' '
		
		WHERE SB1.D_E_L_E_T_ = ' ' ) TRB
		
		WHERE TRB.B0_COD IS NULL 
		
	ENDSQL
		
	
	(cAlias)->( DbGoTop() )
	While (cAlias)->( !Eof() ) 
		
		If !( SB0->( MsSeek(xFilial("SB0") + (cAlias)->B1_COD )))
			RecLock("SB0",.T.)  
				SB0->B0_FILIAL	:= xFilial("SB0")
				SB0->B0_COD		:= (cAlias)->B1_COD
				SB0->B0_PRV1	:= (cAlias)->P02_PRUNIT 
			MsUnlock()
		Endif
	
		(cAlias)->( DbSkip() )		
	
	End 	
	(cAlias)->( DbCloseArea() )

Endif

// MultiThread
If nQtdThread > 0 

	//Obtem a quantidade de cupons a serem validados
	nQtdRegistros := u_QtdCupom(INTEGRADO) + u_QtdCupom(INVALIDO) + u_QtdCupom(COMERRO)
	
	If nQtdRegistros > 0
		
		//Se o n� de registros for maior que a qtd de threads � necess�rio o tratamento abaixo
		If nQtdRegistros > nQtdThread 
		
			nRegsPorThread := Round( (nQtdRegistros/nQtdThread),0 )
			IF !IsBlind()
				FWMsgRun( , {|| U_PackMT( nQtdThread, "u_TibMTValCupom", { nRegsPorThread } ,"VLD" ) } ,, "Validando registros ... "  )
			Else
				U_PackMT( nQtdThread, "u_TibMTValCupom", { nRegsPorThread } ,"VLD" )			
			Endif
						
		//Se o n� de registros for menor ou igual a qtd de threads, sobe s� a qtd necess�ria.
		ElseIf nQtdRegistros <= nQtdThread 
		
			IF !IsBlind()
				FWMsgRun( , {|| U_PackMT( nQtdRegistros, "u_TibMTValCupom", {1} ,"VLD" ) } ,, "Validando registros ... "  )
			Else
				U_PackMT( nQtdRegistros, "u_TibMTValCupom", {1} ,"VLD" )			
			Endif

		EndIf

	Else
		If !IsBlind()
			MsgInfo("N�o existem registros para serem validados!")              
		Else
			LjWriteLog(cLogT,"N�o existem registros para serem validados!")
		Endif
	EndIf

Else

	// Monothread - Processo normal
	// Obtem a quantidade de cupons a serem validados

	nQtdRegistros := U_QtdCupom(INTEGRADO) + u_QtdCupom(INVALIDO) + u_QtdCupom(COMERRO)

	If nQtdRegistros > 0		
		IF !IsBlind()
			FWMsgRun( , {|| u_TibMTValCupom( u_DivRegistros(0, "'" + INTEGRADO + "','" + INVALIDO + "','" + COMERRO + "'" ) ) } ,, "Validando registros ... "  )
		Else
			u_TibMTValCupom( u_DivRegistros(0, "'" + INTEGRADO + "','" + INVALIDO + "','" + COMERRO + "'" ) )		
		Endif
	Else
		If !IsBlind()
			MsgInfo("N�o existem registros para serem validados na filial " + xFilial("P01") )
		Else
			LjWriteLog(cLogT,"N�o existem registros para serem validados na filial " + xFilial("P01") )	
		Endif
		lRet := .F.
	EndIf
	
EndIf

If !IsBlind() .And. lRet	
	Aviso("Aviso","Processo finalizado",{"OK"},2)
Endif

If lCriouAmb
	RESET ENVIRONMENT
Endif

Return







/*/{Protheus.doc} TibMTValCupom
THREAD que efetivamente valida os registros 

@author Mauro Paladini
@since 06/03/2015
@version 1.0
@param aDados, 		array, Cupons a serem gravados como orcamento
@param aParamMT, 	array, Reservado MT

/*/
User Function TibMTValCupom( aDados , aParamMT )

Local cArq		:= "" 
Local cCupom	       := ""
Local cLog	         := ""
Local nX		      := 0
Local cThread	:= IIF( aParamMT == Nil , "singleuser" , aParamMT[1] )
Local lRet		:= .T.

Local tInicio	
Local tFim	

Private cLogMT	:= "\log_pdv\valida_thread_" + cThread + ".log"


MakeDir("\log_pdv") 
If File(cLogMt)
	FErase(cLogMT)
Endif

LjWriteLog(cLogMT,"Iniciando validacao dos registros .. [" + cThread + "] ")

For nX := 1 To Len(aDados)

	cArq	:= aDados[nX][1] 
	cCupom	:= aDados[nX][2]
	tInicio	:= Time()
	
	P01->( DbSetOrder(6) )
	If P01->( DbSeek( xFilial("P01") + cArq + cCupom ) )	

		IF P01->P01_STATUS == INVALIDO .OR. P01->P01_STATUS == INTEGRADO .OR. P01->P01_STATUS == COMERRO .OR. P01->P01_STATUS == '' 
			u_TibValCupom( @lRet )
		Endif 

	Endif

	tFim := ElapTime(tInicio,Time())	
	cLog := SubStr(tFim,1,2)+" h: "+SubStr(tFim,4,2)+" m: "+SubStr(tFim,7,2)+" s: " 

	LjWriteLog(cLogMT, "Validacao do Cupom " + cCupom + IIF( lRet , " [OK] " , " [ERRO] " ) + cLog )
	 
Next nX

If aParamMT <> Nil
	PutGlbValue( aParamMT[1] , "4" )
	GlbUnLock()
EndIf

cLogMT	:= Nil

Return