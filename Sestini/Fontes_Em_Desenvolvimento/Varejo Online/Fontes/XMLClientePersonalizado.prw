#Include 'Protheus.ch'
#include 'tbiconn.ch'

#DEFINE INTEGRADO	"0" 
#DEFINE INVALIDO	"1"
#DEFINE VALIDADO	"2"
#DEFINE ORCAMENTO	"3"
#DEFINE VENDA		"4"
#DEFINE EMPROCESSA	"9"

#DEFINE lRecompile      If( File(GetClientDir()+"TICCONFIG.INI") , GetPvProfString("General","DropProcedures","00",GetClientDir()+"TICCONFIG.INI") == "01" , .F. )
 
Static cCFGLog      := 'TIC_WFXMLCli_' + Dtos(Date()) + '.log'
Static cDirLog      := '\log_pdv\'


/*/{Protheus.doc} WFXMLCLI
Rotina de leitura  -> Cupons Fiscais emitidos pelo Varejo Online
Interface customizada para alimentar P01 P02 P03

@type 	User Function
@author 	Mauro Paladini
@since 	Aug 17, 2018  2:33:31 PM
@version 1.0
@return 	${return}, ${return_description}

@see (links_or_references)
/*/

User Function WFXMLCli( CRPCEmp, cRPCFil )

Local aArea     := GetArea()
Local aAreaSM0  := SM0->( GetArea() )

Local lCriouAmb := .F.

IF cRPCEmp <> Nil .And. cRPCFil <> Nil
    u_WriteLog( cDirLog + cCFGLog  , '[PREP-ENVIRONMENT] [Parm01] ' + cRPCEmp +  '[Param02] '+ cRPCFil ,.T.,.T. )
    RPCSetType(3) 
    RPCSETENV(cRPCEmp, cRPCFil)
    lCriouAmb   := .T.
Endif 

// -- Valida Insta��o das Procedures no ambiente

IF !SP_Config()
    If lCriouAmb
        RESET ENVIRONMENT
    Endif
    Return
Endif


// -- Processa leitura dos movimentos

If IsBlind()   
    
    IF SM0->( DbSeek( cEmpAnt ) )
    
        While SM0->( !Eof() .And. SM0->M0_CODIGO == cEmpAnt )
        
            cFilAnt := SM0->M0_CODFIL
        
            u_XMLCli()            
            u_TibWfValCupom()
    
            SM0->( DbSkip() )       
            
        End
    
    Endif

Else
    
    FWMsgRun( , {|| u_XMLCli() } 		,, "Importando Movimentos .. "  )    
    FWMsgRun( , {|| u_TibWfValCupom() } ,, "Validando Movimentos .. "  )
    
Endif

If !IsBlind()   
    Aviso("Aviso","Processo finalizado",{"OK"},2)
Endif

If lCriouAmb
    RESET ENVIRONMENT
Endif

Return



/*/{Protheus.doc} XMLCli

LAYOUT Personalizado do Cliente
Abre arquivo XML gerado pelo software de automacao comercial
Faz a gravacao dos dados do cupom nas tabelas P01,P02 e P04 com os dados do CFe

@author 	Mauro Paladini
@since 		13/02/2017
/*/


User Function XMLCli()

Local cLogMt            := cDirLog + cFilAnt + "_Tic_LerCupons" + DTOS(Date()) + ".log"
Local cProcName         := "SP_TIC_LERCUPONS_" + cEmpAnt


Local tInicio			:= Time()	
Local tFim	
 
If File(cLogMt)
	FErase(cLogMT)
Endif

// -- SAT-CFe (Cupom Fiscal Eletronico)
// -- Executa procedure para importar movimentos

IF u_SysObject( cProcName , 'P' ) 

    u_WriteLog( cDirLog + cCFGLog  , '[' + cProcName + '] Running ' ,.T.,.T. )

    TcCommit( 5, .T. )   
    aResult := TCSPEXEC( cProcName , cFilAnt , Transform( SM0->M0_CGC , PesqPict("SA2","A2_CGC") ) )   
    TcCommit( 5, .F. )          

    TcRefresh( RETSQLNAME("P01") )
    TcRefresh( RETSQLNAME("P02") )
    TcRefresh( RETSQLNAME("P03") )

Else

    u_WriteLog( cDirLog + cCFGLog  , '[' + cProcName + '] Procedure n�o compilada ' ,.T.,.T. )

    IF !IsBlind()
        Final( "Procedure " + cProcName +  " n�o encontrada" )
    Endif
    
Endif

Return



/*/{Protheus.doc} SP_Config()
Fun��o que faz a verifica��o inicial de depend�ncias entre objetos
utilizados dentro do banco de dados.  Procedures, Views.

@author Mauo Paladini 
@since  10/04/2018
/*/

Static Function SP_Config()

Local nX        := 0
Local aAllProc  := {}
Local cDirCFG   := '\user_procedures\'
Local cSQLProc  := ''
Local lRet      := .T.

Local aObjects  := {    { "SP_TIC_LERCUPONS_"   + RTrim(cEmpAnt) , "P" } }
                        
MontaDir( cDIrCFG )

If File(cCFGLog)
    FErase(cCFGLog)
Endif

// -- Chama componente que verifica e recompila as procedures 

u_WriteLog( cDirLog + cCFGLog  , '[BIConfigure]'      ,.T.,.T. )

lRet := u_SP_Prepare( aObjects , lRecompile , cCFGLog , cDirCFG )

Return lRet
