#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} TSTALOC
Chamada para teste automatizado de alocacoes
@author DS2U (SDA)
@since 28/05/2018
@version 1.0

@type function
/*/
User Function TSTALOC( alParam )

	private aTstLess := {}

//	resetAmb()
//	testeFull( PARAMIXB[1], PARAMIXB[2], cToD( PARAMIXB[3] ) )

//	JOB DE ALOCACOES
	U_ADMVP02( 0, {PARAMIXB[1], PARAMIXB[2]}, .T. )


Return aTstLess

Static Function testeFull( clProduto, nlQtd, dlDtEntr )

	local clNewPV
	
//	if ( resetAmb() )
	
//		setLog( "..:: TESTING ::.. >> Gerando fila de aloca��es " )
//		U_ADMVP02( 3, PARAMIXB ) // Fila de Alocacoes
//		
//		setLog( "..:: TESTING ::.. >> Processando aloca��es " )
//		U_ADMVP02( 0, PARAMIXB ) // JOB de Alocacoes
		
		setLog( "..:: TESTING ::.. >> Criando novo pedido de venda" )
		clNewPV := createPV( @clProduto, nlQtd, dlDtEntr ) // Cria um novo pedido de venda
		
		if ( empty( clNewPV ) )
			setLog( "..:: TESTING ::.. >> Nao foi possivel criar novo pedido de venda" )
		else
		
//			setLog( "..:: TESTING ::.. >> Alterando pedido de venda " + clNewPV )
			
//			if ( changePV( clNewPV, clProduto, nlQtd, dlDtEntr ) )
			
				setLog( "..:: TESTING ::.. >> Processando aloca��es " )
				U_ADMVP02( 0, PARAMIXB ) // JOB de Alocacoes
				
//			else
//				setLog( "..:: TESTING ::.. >> Nao foi possivel alterar pedido de venda " + clNewPV )
//			endif
			
		endif
		
//	endif

Return

Static Function resetAmb()

	local clSql		:= ""
	local llProcOk	:= .T.
	local clFilial	:= "01010001"
	
	begin transaction
	
		//-----------------------------------------------------
		// Deletando registros da tabela de fila de aloca��es -
		//-----------------------------------------------------
		setLog( "..:: TESTING ::.. >> Deletando registros da tabela de fila de aloca��es " )
		clSql := "DELETE FROM " + retSqlName( "ZCQ" )
		if ( tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		//------------------------------------------------------------------
		// Deletando registros da tabela de historico de fila de aloca��es -
		//------------------------------------------------------------------
		setLog( "..:: TESTING ::.. >> Deletando registros da tabela de historico de fila de aloca��es " )
		clSql := "DELETE FROM " + retSqlName( "ZCP" )
		if ( tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		//---------------------------------------------
		// Deletando registros da tabela de aloca��es -
		//---------------------------------------------
		setLog( "..:: TESTING ::.. >> Deletando registros da tabela de aloca��es " )
		clSql := "DELETE FROM " + retSqlName( "ZCI" )
		if ( llProcOk .and. tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		//-------------------------------------------------------------------
		// Zerando item do pedido de venda inicial para testes de aloca��es -
		//-------------------------------------------------------------------
//		setLog( "..:: TESTING ::.. >> Zerando item do pedido de venda inicial para testes de aloca��es" )
//		clSql := "UPDATE " + retSqlName( "SC6" ) + " SET C6_XSTALOC = 'NP', C6_QTDVEN = 100, C6_VALOR = 5500 WHERE D_E_L_E_T_ = ' ' AND C6_NUM = '" + clPedido + "' "
//		if ( llProcOk .and. tcSqlExec( clSql ) < 0 )
//			llProcOk := .F.
//			setLog( "Falha na instru��o SQL: " + clSql )
//			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
//		endif
		
		//--------------------------------------------------------
		// Apagando possiveis itens criados em testes anteriores -
		//--------------------------------------------------------
		setLog( "..:: TESTING ::.. >> Apagando possiveis itens criados em testes anteriores" )
		clSql := "DELETE FROM " + retSqlName( "SC6" ) + " WHERE D_E_L_E_T_ = ' ' "
		if ( llProcOk .and. tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		//---------------------------------------------------------------------
		// Apagando novos pedidos de venda para nao influenciar nas aloca��es -
		//---------------------------------------------------------------------
		setLog( "..:: TESTING ::.. >> Apagando novos pedidos de venda para nao influenciar nas aloca��es" )
		clSql := "DELETE FROM " + retSqlName( "SC5" ) + " WHERE D_E_L_E_T_ = ' ' "
		if ( llProcOk .and. tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		//---------------------------------
		// Apagando libera��es de pedidos -
		//---------------------------------
		clSql := "DELETE FROM " + retSqlName( "SC9" ) + " WHERE D_E_L_E_T_ = ' ' "
		if ( llProcOk .and. tcSqlExec( clSql ) < 0 )
			llProcOk := .F.
			setLog( "Falha na instru��o SQL: " + clSql )
			setLog( "Erro retornado do banco de dados: " + tcSqlError() )
		endif
		
		if ( .not. llProcOk )
			disarmTransaction()
		endif
		
	end transaction

Return llProcOk

Static Function createPV( clProduto, nlQtd, dlDtEntr )

	local aCab 		:= {}
	local aItens 	:= {}
	local alErroAuto:= {}
	local clMsgErro := ""
	local clPedido	:= ""
	local clAlmox	:= "ECO"
	
	private lMsErroAuto	:= .F.
	private lMsHelpAuto    := .T.
	private lAutoErrNoFile := .T. 

	default clProduto	:= "060001-00      "
	default nlQtd		:= 100
	default dlDtEntr	:= date()
	
	dbSelectArea("SC5")
	dbSelectArea("SC6")
	
	// Checa Saldo SB2
	U_ADMVP01( 5, { clProduto, clAlmox } )
	
	clPedido	:= getSXENum( "SC5", "C5_NUM" )

	aCab := {	{"C5_FILIAL"    ,fwxFilial("SC5")	    ,Nil},;
	{"C5_NUM"       , clPedido            				,Nil},;
	{"C5_TIPO"      , "N"            					,Nil},;
	{"C5_CLIENTE"   , "61777009"						,Nil},;
	{"C5_LOJACLI"   , "0067"  	  						,Nil},;
	{"C5_CONDPAG"   , "1  "								,Nil},;
	{"C5_CLIENT" 	, "61777009"						,Nil},;
	{"C5_LOJAENT"   , "0067"							,Nil} } 

	aAdd(aItens,{  	{"C6_FILIAL"	, xFilial("SC6")		, nil},;
	{"C6_ITEM"   	, "01" 		 							, nil},;
	{"C6_PRODUTO"	, clProduto			   					, nil},; // TEM DE AJUSTAR Produto, Vendedor, TES, Data
	{"C6_TES"    	, "528"			 						, nil},;
	{"C6_QTDVEN" 	, nlQtd									, nil},;
	{"C6_PRCVEN" 	, 55	 								, nil},;
	{"C6_PRCUNIT" 	, 0										, nil},;
	{"C6_LOCAL" 	, clAlmox								, nil},;
	{"C6_ENTREG" 	, dlDtEntr								, nil}})

	msExecAuto({|x,y,z|MATA410(x,y,z)},aCab,aItens,3)
	
	if ( lMsErroAuto )
	
		rollBackSX8()
		alErroAuto := getAutoGRLog()
		aEval( alErroAuto, {|x| clMsgErro += allTrim( x ) + '<br/>'})
		setLog( clMsgErro )
		
	else
		confirmSX8()
		setLog("..:: TESTING ::.. >> Pedido [" + clPedido + "] incluido com sucesso!")			
	endif
	
Return clPedido

Static Function changePV( clPedido, clProduto, nlQtd, dlDtEntr )

	local aCab := {}
	local aItens := {}
	local alErroAuto := {}
	local clMsgErro := ""
	local llProcOk	:= .F.
	
	private lMsErroAuto	:= .F.
	private lMsHelpAuto    := .T.
	private lAutoErrNoFile := .T. 
	
	default clPedido := ""
	default clProduto := ""

	aCab := {	{"C5_FILIAL"    ,fwxFilial("SC5")	    ,Nil},;
	{"C5_NUM"      , clPedido            				,Nil},;
	{"C5_TIPO"      , "N"            					,Nil},;
	{"C5_CLIENTE"   , "61777009"						,Nil},;
	{"C5_LOJACLI"   , "0067"  	  						,Nil},;
	{"C5_CONDPAG"   , "039"								,Nil},;
	{"C5_CLIENT" 	, "61777009"						,Nil},;
	{"C5_LOJAENT"   , "0067"							,Nil},;
	{"C5_XPRIORI"   , "01"								,Nil} } 

	aAdd(aItens,{  	{"C6_FILIAL"	, xFilial("SC6")		, nil},;
	{"C6_NUM"      , clPedido            					,Nil},;
	{"C6_ITEM"   	, "01" 		 							, nil},;
	{"C6_PRODUTO"	, clProduto					   			, nil},;
	{"C6_TES"    	, "528"			 						, nil},;
	{"C6_QTDVEN" 	, nlQtd									, nil},;
	{"C6_PRCVEN" 	, 55	 								, nil},;
	{"C6_PRCUNIT" 	, 0										, nil},;
	{"C6_ENTREG" 	, dlDtEntr								, nil}})

	msExecAuto({|x,y,z|MATA410(x,y,z)},aCab,aItens,4)
	
	if ( lMsErroAuto )
	
		alErroAuto := getAutoGRLog()
		aEval( alErroAuto, {|x| clMsgErro += allTrim( x ) + '<br/>'})
		setLog( clMsgErro )
		
	else
		llProcOk := .T.
		setLog("..:: TESTING ::.. >> Pedido [" + clPedido + "] alterado com sucesso!")			
	endif
	
Return llProcOk

Static Function setLog( clMsg )
	AADD( aTstLess, clMsg + " >> " + time() ) // Array deve ser criado na funcao pai, onde foi instanciado testLess():new
Return