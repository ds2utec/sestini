#INCLUDE "PROTHEUS.CH"

User function TESTING()

	local clEmp		:= '01'
	local clFil		:= '01010001'
	local olTests	:= testLess():new( clEmp, clFil )
	
	// Adiciona roteiro de testes a serem realizados
	olTests:addSteps( "U_TSTALOC", {clEmp, clFil }, .T. /*llExecute*/ )
//	olTests:addSteps( "U_TSTALOC", {"060001-00      ", 2080, "20190217" }, .T. /*llExecute*/ )
	
	// Realiza as chamadas e capturas de logs
	olTests:testCalls()
	
	// Imprime logs
	olTests:showLogs()
	
Return