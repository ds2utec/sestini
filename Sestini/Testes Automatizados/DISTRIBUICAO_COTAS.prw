/*/{Protheus.doc} TSTDISTC
Chamada para teste automatizado de distribuicao de cotas entre enderešos, conforme a hierarquia cadastrada no cadastro de cotas
@author DS2U (SDA)
@since 28/05/2018
@version 1.0

@type function
/*/
User Function TSTDISTC()

	U_ADMVP01( 0, PARAMIXB )
	
Return