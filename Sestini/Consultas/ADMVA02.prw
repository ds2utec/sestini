#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA02
Funcao para controle do cadastro de Categoria de produtos
@author DS2U(SDA)
@since 04/05/2018
@version 1.0
@param nlOpc, numeric, opcao da funcao a ser executada
@param uParam, undefined, Parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA02( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			dbSelectArea("SZD")
			browse()
		
	endCase

Return uRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro de Categoria de produtos
@author DS2U (SDA)
@since 04/05/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= P00->( getArea() )

	private oBrowse

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("SZD")
	oBrowse:SetDescription("Cadastro de Categoria de Produtos")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef do cadastro de Categoria de produtos
@author DS2U (SDA)
@since 04/05/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"	ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"	ACTION "VIEWDEF.ADMVA02"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"		ACTION "VIEWDEF.ADMVA02"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"		ACTION "VIEWDEF.ADMVA02"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"		ACTION "VIEWDEF.ADMVA02"	OPERATION 5 ACCESS 0 // "Excluir" 
	ADD OPTION alRotina TITLE "Copiar"		ACTION "VIEWDEF.ADMVA02"	OPERATION 9 ACCESS 0 // "Copiar" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro de Categoria de produtos
@author DS2U (SDA)
@since 04/05/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruP00	:= FWFormStruct( 2, 'SZD' )
	local olModel	:= FWLoadModel( 'ADMVA02' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_SZD', olStruP00, 'SZDMASTER' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_SZD', 'SUPERIOR' )
	
	// Liga a identificacao do componente
	olView:EnableTitleView('VIEW_SZD','Cadastro de Fam�lia de Produtos' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de Categoria de produtos
@author DS2U (SDA)
@since 04/05/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruSZD	:= FWFormStruct( 1, 'SZD', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA02M', /* bPreValidacao*/,/*bPosValidacao { |olModel| tudoOk( olModel ) }*/,/*bCommit { |olModel| PRR13Commit( olModel ) }*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'SZDMASTER', /*cOwner*/, olStruSZD, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZD_FILIAL", "ZD_CODIGO"} )
	
	olModel:SetDescription('Cadastro de Fam�lia de Produtos')
	olModel:GetModel('SZDMASTER' ):SetDescription('Cadastro de Fam�lia de Produtos')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel