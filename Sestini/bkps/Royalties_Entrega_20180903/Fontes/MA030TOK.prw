#INCLUDE "PROTHEUS.CH"
#INCLUDE "rwmake.ch"

//--------------------------------------------------------------------
/*/{Protheus.doc} MA030TOK
Ponto de Entrada chamando em MATA030 - Cadastro de Cliente (SA1)

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------
User Function MA030TOK()
Local lRet:=.T.
Local aArea  := GetArea()
lRet:=A_030TOK()
If lRet .And. !Empty(M->A1_XGRPMAT)
	B_030TOK(M->A1_XGRPMAT, M->A1_XDESAG, M->A1_XPERDES, M->A1_XDSVDES, M->A1_XNCDES, M->A1_COD)
EndIf
RestArea(aArea)
Return lRet 

//--------------------------------------------------------------------
/*/{Protheus.doc} A_030TOK()
Fun��o de Valida��o de Inclus�o/Altera��o de Cadastro de Cliente (SA1)

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function A_030TOK()
Local _lRet  := .T.
Local _aArea := GetArea()

IF M->A1_XDESAG == "S" .And. M->A1_XPERDES <= 0 
   MsgAlert("Favor informar Percentual do Des�gio!")
   _lRet := .F.
EndIf   
RestArea(_aArea)
Return _lRet 

//--------------------------------------------------------------------
/*/{Protheus.doc} B_030TOK()
Tela de Replica��o de Dados de Licenciamento para Grupo 

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function B_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1)
Local _aArea := GetArea()

@ 0,0 TO 240,450 DIALOG oDlg1 TITLE "Replica��o de Licenciamento"
                                                       
@ 10,10 SAY 'Deseja Replicar as Informa��es abaixo para todos os clientes do grupo "' + AllTRim(Grupo) + '" ?' 
@ 30,10 SAY 'Informa��es a serem replicadas:' 
@ 40,10 SAY 'Des�gio: "' + Desagio + '"'
@ 50,10 SAY 'Percentual de Des�gio: ' + STR(Percent,5,2)  + ' %'
@ 60,10 SAY 'Percentual de Desconto de Des�gio: ' + STR(Descont,5,2) + ' %'
@ 70,10 SAY 'N�mero de Contrato: "' + AllTRim(Contrato) + '"'

*---------------Bot�o de OK-----------------------* 
@ 90,135 BUTTON "N�o" SIZE 35,15 ACTION Close(oDlg1) 
@ 90,185 BUTTON "Sim" SIZE 35,15 ACTION (C_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1), Close(oDlg1)) 
*-------------------------------------------------*

ACTIVATE DIALOG oDlg1 CENTER 

RestArea(_aArea)
Return 

//--------------------------------------------------------------------
/*/{Protheus.doc} C_030TOK()
Fun��o de Replica��o de Dados de Licenciamento para Grupo 

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function C_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1)
Local _aArea   := GetArea()
Local _nCnt     := 0
Local _xFilSA1 := xFilial("SA1")
Local _cOrdem  := " A1_RECNO "
Local _cQuery  := " SELECT A1_XGRPMAT, A1_COD, R_E_C_N_O_ A1_RECNO"

_cQuery += " FROM "
_cQuery += RetSqlName("SA1")        + " SA1 "
_cQuery += " WHERE '"               + _xFilSA1 + "' = A1_FILIAL "
_cQuery += " AND    A1_COD <> '"    + CodSA1   + "' "
_cQuery += " AND    A1_XGRPMAT = '" + Grupo    + "' " 
_cQuery += " AND    SA1.D_E_L_E_T_ = '' "
_cQuery += " ORDER BY "             + _cOrdem
_cQuery := ChangeQuery(_cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"SA1TMP",.F.,.T.)

Dbselectarea("SA1TMP")
SA1TMP->(dbGoTop())
COUNT to _nCnt
SA1TMP->(dbGoTop())
ProcRegua( _nCnt )

If !Eof()
	Begin Transaction
	While !Eof()
	
	    Incproc("Atualizando Cliente: " + SA1TMP->A1_COD) 
		dbSelectArea("SA1")
		SA1->(DbGoTo(SA1TMP->A1_RECNO))
		If SA1->(RecLock("SA1", .F.))
			SA1->A1_XDESAG  := Desagio
			SA1->A1_XPERDES := Percent
			SA1->A1_XDSVDES := Descont
			SA1->A1_XNCDES  := Contrato
			SA1->(MsUnLock())
		EndIf
		dbSelectArea("SA1TMP")
		SA1TMP->(DbSkip())
	Enddo   

    MsgAlert("Replica��o de Licenciamento Realizada com Sucesso!!")
    End Transaction

EndIf
DbClosearea("SA1TMP")
 
RestArea(_aArea)
Return 
