#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FWADAPTEREAI.CH"

#DEFINE COR_VERDE	"BR_VERDE"
#DEFINE COR_AMARE	"BR_AMARELO"

//------------------------------------------------------------------->
/*/{Protheus.doc} Apurar

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function Apurar()

	Local _lErro	:=.F.
	Local aAdvSize  := FWGetDialogSize(oMainWnd)
	Local oDlg
	Local aHeadSize := {40,40,40,40,40,40,40,40,40,40} 
	Local oPanelE
	Local oPanelD
	Local oFolderE
	Local oFolderD
	Local _aMatPE  	:= {}
	Local _aMatA1 	:= {}
	Local _aMatA2 	:= {}
	Local _cPerg	:= "APURA"

	Local _cIniFim	:= ""
	Local _cValTot	:= ""  
	Local _cValDes	:= "" 	

	Private _nValTot := 0
	Private _nValDes := 0

	Define Font oFont12B Name "Arial" Size 0,-12 Bold
	Define Font oFont14B Name "Arial" Size 0,-14 Bold

	If Pergunte(_cPerg, .T.)

		If Empty(SuperGetMV("ES_PRODLIC"))
			MsgAlert('Favor Informar o Par�metro "ES_PRODLIC" com o C�digo de Produto de Licenciamento! ')
			_lErro := .T.
		EndIf

		If Empty(SuperGetMV("ES_CODEFL"))
			MsgAlert('Favor Informar o Par�metro "ES_CODEFL" com o C�digo da Filial de Licenciamento! ')
			_lErro := .T.
		EndIf

		If !_lErro
			//--------------------------------------------------------------------v + Begin Painel Esquerda
			DEFINE MSDIALOG oDlg TITLE "Preview - Apura��o Des�gio" FROM aAdvSize[1],aAdvSize[2] To aAdvSize[3],aAdvSize[4] OF oMainWnd PIXEL

			@ 10,10 MSPANEL oPanelE PROMPT "Por Nota Fiscal" SIZE 335,100 of oDlg  
			oPanelE:Align := CONTROL_ALIGN_LEFT

			@ 10,02 Folder oFolderE Prompts "Nota Fiscal" Size 333,202 Pixel Of oPanelE 

			_aMatPE := aClone(NFS(mv_par01, mv_par02))

			oLBxE := TWBrowse():New(010,000,330,170,Nil,{"DOC", "SERIE", "CLIENTE", "NOME", "TOTAL", "DESAGIO"}, aHeadSize, oFolderE:aDialogs[1],,,,,,,,,,,,,,.T.)

			oLBxE:SetArray(_aMatPE)
			oLBxE:bLine:={|| {	_aMatPE[oLBxE:nAt,1],;
			_aMatPE[oLBxE:nAt,2],;
			_aMatPE[oLBxE:nAt,3],;
			_aMatPE[oLBxE:nAt,4],;
			_aMatPE[oLBxE:nAt,5],;												
			_aMatPE[oLBxE:nAt,6] } }

			//--------------------------------------------------------------------^ + End Painel Esquerda

			//-------------------------------------------------------------------->	

			//--------------------------------------------------------------------v + Begin Painel Direita			
			@ 10,10 MSPANEL oPanelD PROMPT "Por Cliente" SIZE 335,100 of oDlg  
			oPanelD:Align := CONTROL_ALIGN_RIGHT

			@ 10,02 Folder oFolderD Prompts "CNPJ","Grupo Empresarial" Size 333,202 Pixel Of oPanelD

			_aMatA1 := aClone(CPF(mv_par01, mv_par02))

			oLBxA1 := TWBrowse():New(010,003,330,170,Nil,{"CNPJ","TOTAL","DESAGIO"}, aHeadSize, oFolderD:aDialogs[1],,,,,,,,,,,,,,.T.)

			oLBxA1:SetArray(_aMatA1)
			oLBxA1:bLine:={|| {	_aMatA1[oLBxA1:nAt,1],;
			_aMatA1[oLBxA1:nAt,2],;
			_aMatA1[oLBxA1:nAt,3] } }


			_aMatA2 := aClone(GRP(mv_par01, mv_par02))

			oLBxA2 := TWBrowse():New(010,003,330,170,Nil,{"Grupo Empresarial","TOTAL","DESAGIO"}, aHeadSize, oFolderD:aDialogs[2],,,,,,,,,,,,,,.T.)

			oLBxA2:SetArray(_aMatA2)
			oLBxA2:bLine:={|| {	_aMatA1[oLBxA2:nAt,1],;
			_aMatA2[oLBxA2:nAt,2],;
			_aMatA2[oLBxA2:nAt,3] } }

			//--------------------------------------------------------------------^ + End Painel Direita			



			//--------------------------------------------------------------------v + Begin Rodap�
			@ 00,00 MSPANEL oPanelR PROMPT "" SIZE 335,60 of oDlg //30  
			oPanelR:Align := CONTROL_ALIGN_BOTTOM 
			@ 10,05 SAY oSayP PROMPT "Resumo  " 						SIZE 200,10 OF oPanelR PIXEL Font oFont14B COLOR CLR_RED

			_cIniFim := DTOC(mv_par01) + " a " + DTOC(mv_par02)
			_cValTot := Transform(_nValTot , "@E 999,999,999,999.99")  
			_cValDes := Transform(_nValDes , "@E 999,999,999,999.99")  

			@ 25,05 SAY oSayP PROMPT "Per�odo : " 					+ _cIniFim  SIZE 200, 010 OF oPanelR PIXEL Font oFont COLOR CLR_BLUE
			@ 35,05 SAY oSayP PROMPT "Valor Tot Fat Per�odo : " 	+ _cValTot  SIZE 200, 010 OF oPanelR PIXEL Font oFont COLOR CLR_BLUE
			@ 45,05 SAY oSayP PROMPT "Valor Tot Des�gio Per�odo : " + _cValDes 	SIZE 200, 010 OF oPanelR PIXEL Font oFont COLOR CLR_BLUE
			//--------------------------------------------------------------------^ + End Rodap�


			//--------------------------------------------------------------------v Begin Tela com 2 Paineis, Abas e Rodap�			
			@ 275, 560 BUTTON oButtonA PROMPT "Cancelar"  ACTION (oDlg:End())								SIZE 050, 018 OF oDlg PIXEL Font oFont12B
			@ 275, 620 BUTTON oButtonA PROMPT "Processar" ACTION (oDlg:End(), Processar(_aMatPE), Gerar())	SIZE 050, 018 OF oDlg PIXEL Font oFont12B     

			ACTIVATE MSDIALOG oDlg ON INIT (EnchoiceBar(oDlg, {||oDlg:End()}, {||oDlg:End()},,,,,,,,.F.,)) CENTERED
			//--------------------------------------------------------------------^ End   Tela com 2 Paineis, Abas e Rodap�						

		EndIf

	EndIf
Return

//------------------------------------------------------------------->
/*/{Protheus.doc} NFS

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function NFS(DtIni, DtFim)
	Local aRet		:={}
	Local _xFilSA1	:= xFilial("SA1")
	Local _xFilSD1	:= xFilial("SD1")
	Local _xFilSD2	:= xFilial("SD2")
	Local _cDoc		:= ""
	Local _cSer   	:= ""
	Local _cCli   	:= ""
	Local _cNom		:= ""
	Local _cTot   	:= ""
	Local _cDes   	:= ""

	Local _cQuery := "SELECT DOC, SERIE, CLIENTE, NOME, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO  FROM " 

	_cQuery += "( "
	_cQuery += "SELECT D2_DOC DOC, D2_SERIE SERIE, D2_CLIENTE+'/'+D2_LOJA CLIENTE, A1_NOME NOME, D2_EMISSAO, "  
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_TOTAL)  ,0) TOTAL, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_XVALDES),0) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD2") + " SD2 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D2_CLIENTE=A1_COD AND D2_LOJA=A1_LOJA "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD2.D2_TIPO='N' AND SD2.D_E_L_E_T_='' " 
	_cQuery +=  "AND A1_FILIAL = '" + _xFilSA1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY  D2_DOC, D2_SERIE, D2_CLIENTE, D2_LOJA, A1_NOME, D2_EMISSAO "
	_cQuery += "UNION "
	_cQuery += "SELECT D1_NFORI DOC, D1_SERIORI SERIE, D1_FORNECE+'/'+D1_LOJA CLIENTE, A1_NOME NOME, D2_EMISSAO, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN  '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' ,SUM(D2_TOTAL)*-1,0) TOTAL, " 
	_cQuery += "SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD1") + " SD1 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D1_FORNECE=A1_COD AND D1_LOJA=A1_LOJA "
	_cQuery += "INNER JOIN SD2990 SD2 ON D1_NFORI=D2_DOC AND D1_SERIORI=D2_SERIE AND D1_ITEMORI=D2_ITEM "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD1.D1_TIPO='D' AND SD1.D_E_L_E_T_='' AND SD2.D_E_L_E_T_='' " 
	_cQuery +=  "AND A1_FILIAL = '" + _xFilSA1 + "' AND D1_FILIAL = '"+ _xFilSD1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY D1_NFORI, D1_SERIORI, D1_FORNECE, D1_LOJA, A1_NOME, D2_EMISSAO "
	_cQuery += ")  TST "
	_cQuery += "WHERE DESAGIO<>0 "
	_cQuery += "GROUP BY DOC, SERIE, CLIENTE, NOME "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)
	Dbselectarea("TMP")
	dbGotop()

	While !Eof()
		_cDoc	:= TMP->DOC
		_cSer   := TMP->SERIE
		_cCli   := TMP->CLIENTE
		_cNom	:= TMP->NOME
		_cTot   := TransForm(TMP->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm(TMP->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		_nValTot += TMP->TOTAL
		_nValDes += TMP->DESAGIO

		aAdd(aRet,{_cDoc, _cSer, _cCli, _cNom, _cTot, _cDes})
		dbSelectArea("TMP")
		DbSkip()
	Enddo

	dbSelectArea("TMP")
	DbCloseArea() 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} CPF

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function CPF(DtIni, DtFim)
	Local aRet		:={}
	Local _xFilSA1	:= xFilial("SA1")
	Local _xFilSD1	:= xFilial("SD1")
	Local _xFilSD2	:= xFilial("SD2")
	Local _cCgc		:= ""
	Local _cTot   	:= ""
	Local _cDes   	:= ""

	Local _cQuery := "SELECT GRPCGC, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO  FROM "

	_cQuery += "( "
	_cQuery += "SELECT A1_CGC GRPCGC, D2_EMISSAO, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_TOTAL)  ,0) TOTAL, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_XVALDES),0) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD2") + " SD2 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D2_CLIENTE=A1_COD AND D2_LOJA=A1_LOJA "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD2.D2_TIPO='N' AND SD2.D_E_L_E_T_='' " 
	_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY A1_CGC, D2_EMISSAO "
	_cQuery += "UNION "
	_cQuery += "SELECT A1_CGC GRPCGC, D2_EMISSAO, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN  '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' ,SUM(D2_TOTAL)*-1,0) TOTAL, " 
	_cQuery += "SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD1") + " SD1 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D1_FORNECE=A1_COD AND D1_LOJA=A1_LOJA "
	_cQuery += "INNER JOIN SD2990 SD2 ON D1_NFORI=D2_DOC AND D1_SERIORI=D2_SERIE AND D1_ITEMORI=D2_ITEM "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD1.D1_TIPO='D' AND SD1.D_E_L_E_T_='' AND SD2.D_E_L_E_T_='' " 
	_cQuery +=  "AND A1_FILIAL = '" + _xFilSA1 + "' AND D1_FILIAL = '"+ _xFilSD1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY A1_CGC, D2_EMISSAO "
	_cQuery += ")  TST "
	_cQuery += "WHERE DESAGIO<>0 "
	_cQuery += "GROUP BY GRPCGC "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)
	Dbselectarea("TMP")
	dbGotop()

	While !Eof()
		_cCgc	:= TMP->GRPCGC
		_cTot   := TransForm(TMP->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm(TMP->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		aAdd(aRet,{_cCgc, _cTot, _cDes})
		dbSelectArea("TMP")
		DbSkip()
	Enddo

	dbSelectArea("TMP")
	DbCloseArea() 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} GRP

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->


Static Function GRP(DtIni, DtFim)
	Local aRet:={}

	Local _xFilSA1	:= xFilial("SA1")
	Local _xFilSD1	:= xFilial("SD1")
	Local _xFilSD2	:= xFilial("SD2")
	Local _cEmp		:= ""
	Local _cTot   	:= ""
	Local _cDes   	:= ""

	Local _cQuery := "SELECT GRPEMP, SUM(TOTAL) TOTAL, SUM(DESAGIO) DESAGIO  FROM "

	_cQuery += "( "
	_cQuery += "SELECT A1_XGRPEMP GRPEMP, D2_EMISSAO, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_TOTAL)  ,0) TOTAL, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN '"+ DTOS(DtIni) + "' AND '" + DTOS(DtFim) + "' ,SUM(D2_XVALDES),0) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD2") + " SD2 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D2_CLIENTE=A1_COD AND D2_LOJA=A1_LOJA "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD2.D2_TIPO='N' AND SD2.D_E_L_E_T_='' " 
	_cQuery += "AND A1_FILIAL = '" + _xFilSA1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY A1_XGRPEMP, D2_EMISSAO  "
	_cQuery += "UNION "
	_cQuery += "SELECT A1_XGRPEMP GRPEMP, D2_EMISSAO, " 
	_cQuery += "IIF(D2_EMISSAO BETWEEN  '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' ,SUM(D2_TOTAL)*-1,0) TOTAL, " 
	_cQuery += "SUM((D2_XVALDES/D2_TOTAL)*D1_TOTAL*-1) DESAGIO "
	_cQuery += "FROM " + RetSQLName("SD1") + " SD1 "
	_cQuery += "INNER JOIN SA1990 SA1 ON D1_FORNECE=A1_COD AND D1_LOJA=A1_LOJA "
	_cQuery += "INNER JOIN SD2990 SD2 ON D1_NFORI=D2_DOC AND D1_SERIORI=D2_SERIE AND D1_ITEMORI=D2_ITEM "
	_cQuery += "WHERE SA1.D_E_L_E_T_='' AND SD1.D1_TIPO='D' AND SD1.D_E_L_E_T_='' AND SD2.D_E_L_E_T_='' " 
	_cQuery +=  "AND A1_FILIAL = '" + _xFilSA1 + "' AND D1_FILIAL = '"+ _xFilSD1 + "' AND D2_FILIAL = '" + _xFilSD2 + "' " 
	_cQuery += "GROUP BY A1_XGRPEMP, D2_EMISSAO  "
	_cQuery += ")  TST "
	_cQuery += "WHERE DESAGIO<>0 "
	_cQuery += "GROUP BY GRPEMP "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)
	Dbselectarea("TMP")
	dbGotop()

	While !Eof()
		_cEmp	:= TMP->GRPEMP
		_cTot   := TransForm(TMP->TOTAL,PesqPict("SD2","D2_TOTAL"))
		_cDes   := TransForm(TMP->DESAGIO,PesqPict("SD2","D2_XVALDES"))

		aAdd(aRet,{_cEmp, _cTot, _cDes})
		dbSelectArea("TMP")
		DbSkip()
	Enddo

	dbSelectArea("TMP")
	DbCloseArea() 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} PROCESSAR

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Processar(_aMat)
	Local _aArea  := GetArea()
	Local _nTam  := Len(_aMat)
	Local _lFez  := .F.
	Local _nI

	If  _nTam > 0 
		ProcRegua( _nTam )

		Begin Transaction
			For _nI:=1 to _nTam
				Incproc("Gera��o de Fila: " + _aMat[_nI,1]) 
				dbSelectArea("ZC7")
				dbSetOrder(1)
				If ZC7->(!dbSeek(xFilial("ZC7")+(_aMat[_nI,1]+_aMat[_nI,2]))) 
					ZC7->(RecLock("ZC7", .T.))
					ZC7->ZC7_FILIAL		:= xFilial("ZC7")
					ZC7->ZC7_STATUS		:= " "
					ZC7->ZC7_CODCLI  	:= SubStr(Alltrim(_aMat[_nI,3]),1,6)	// "CLIENTE"
					ZC7->ZC7_LOJCLI  	:= SubStr(Alltrim(_aMat[_nI,3]),8,2)	// "CLIENTE" 
					ZC7->ZC7_NOME  		:= _aMat[_nI,4]							// "NOME" 
					ZC7->ZC7_DTEMIS     := dDataBase
					ZC7->ZC7_VALDES		:= Val(_aMat[_nI,6])					// "DESAGIO"
					ZC7->ZC7_NOTA  		:= _aMat[_nI,1]							// "DOC"
					ZC7->ZC7_SERIE  	:= _aMat[_nI,2]							// "SERIE"
					ZC7->ZC7_IDPDES		:= " "
					ZC7->ZC7_PEDIDO		:= _aMat[_nI,2] 						// "PEDIDO"
					ZC7->(MsUnLock())
					_lFez  := .T.
				EndIf
			Next _nI   

			If _lFez 
				MsgAlert("Gera��o de Fila de Pedidos de Licenciamento Realizada com Sucesso!!")
			Else
				MsgAlert("N�o houve nenhum Pedido de Licenciamento gerado!!")
			EndIf

		End Transaction
	Else
		MsgAlert("N�o h� nenhuma Nota Fiscal para a Gera��o de Fila de Pedidos de Licenciamento!!")
	EndIf

	RestArea(_aArea)
Return 


//------------------------------------------------------------------->
/*/{Protheus.doc} Gerar

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Gerar()

	Local aAdvSize  := FWGetDialogSize(oMainWnd)
	Local oDlg
	Local aHeadSize := {40,40,40,40,40,40,40,40,40,40} 
	Local oPanel
	Local _aMatP  	:= {}

	Define Font oFont12B Name "Arial" Size 0,-12 Bold

	DEFINE MSDIALOG oDlg TITLE "Des�gio - Gera��o de Licenciamento" FROM aAdvSize[1],aAdvSize[2] To aAdvSize[3],aAdvSize[4] OF oMainWnd PIXEL

	@ 30,05 MSPANEL oPanel PROMPT "" SIZE 665,220 of oDlg  
	oPanel:Align := CONTROL_ALIGN_CENTER // CONTROL_ALIGN_LEFT

	_aMatP := aClone(ZC7(mv_par01, mv_par02))

	If Len(_aMatP) > 0

		oLBx := TWBrowse():New(010,000,665,220,Nil,{"STATUS", "CLIENTE", "NOME", "EMISSAO", "DESAGIO"}, aHeadSize, oPanel ,,,,,,,,,,,,,,.T.)
		oLBx:SetArray(_aMatP)
		oLBx:bLine:={|| {	_aMatP[oLBx:nAt,1],;
		_aMatP[oLBx:nAt,2],;
		_aMatP[oLBx:nAt,3],;
		_aMatP[oLBx:nAt,4],;
		_aMatP[oLBx:nAt,5] } }

		@ 270, 560 BUTTON oButtonA PROMPT "Cancelar"  ACTION (oDlg:End())	SIZE 050, 018 OF oDlg PIXEL Font oFont12B
		@ 270, 620 BUTTON oButtonA PROMPT "Executar"  ACTION (oDlg:End(), Executar())	SIZE 050, 018 OF oDlg PIXEL Font oFont12B     

	EndIf

	ACTIVATE MSDIALOG oDlg ON INIT (EnchoiceBar(oDlg, {||oDlg:End()}, {||oDlg:End()},,,,,,,,.F.,)) CENTERED


Return


//------------------------------------------------------------------->
/*/{Protheus.doc} ZC7

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function ZC7(DtIni, DtFim)
	Local aRet		:={}
	Local _xFilZC7	:= xFilial("ZC7")
	Local _cSta		:= ""
	Local _cCli		:= ""
	Local _cNom		:= ""
	Local _cEmi     := ""
	Local _cIDP
	Local _cDes   	:= ""

	Local _cQuery := "SELECT ZC7_STATUS, ZC7_CODCLI, ZC7_LOJCLI, ZC7_NOME, ZC7_DTEMIS, ZC7_VALDES, ZC7_NOTA, ZC7_SERIE, ZC7_IDPDES, R_E_C_N_O_ ZC7_RECNO  " 

	_cQuery += "FROM " + RetSQLName("ZC7") + " ZC7 "
	_cQuery += "WHERE ZC7.ZC7_FILIAL = '" + _xFilZC7 + "' AND " 
	_cQuery += "ZC7.ZC7_DTEMIS BETWEEN  '"+DTOS(DtIni)+"' AND '"+DTOS(DtFim)+"' AND " 
	_cQuery += "ZC7.D_E_L_E_T_ =' '  " 
	_cQuery += "ORDER BY ZC7.ZC7_NOTA, ZC7.ZC7_SERIE "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)

	Dbselectarea("TMP")
	dbGotop()
	If !Eof()
		While !Eof() 

			If !Empty(TMP->ZC7_STATUS)
				_cSta := LoadBitmap( GetResources(), COR_VERDE ) 		// "BR_VERDE"
			Else
				_cSta := LoadBitmap( GetResources(), COR_AMARE ) 		// "BR_AMARELO"
			EndIf	

			//_cSta	:= TMP->ZC7_STATUS
			_cCli	:= TMP->ZC7_CODCLI+"/"+TMP->ZC7_LOJCLI
			_cNom	:= TMP->ZC7_NOME
			_cEmi   := DTOC(STOD(TMP->ZC7_DTEMIS))
			_cDes   := TransForm(TMP->ZC7_VALDES,PesqPict("SD2","D2_XVALDES"))		
			_cIDP   := TMP->ZC7_IDPDES
			_cRec   := TMP->ZC7_RECNO
			aAdd(aRet,{_cSta, _cCli, _cNom, _cEmi, _cDes, _cIDP, _cRec})
			dbSelectArea("TMP")
			DbSkip()
		Enddo
	EndIf
	dbSelectArea("TMP")
	DbCloseArea() 

Return aRet


//------------------------------------------------------------------->
/*/{Protheus.doc} Executar

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function Executar()

	Local _cProduto 	:= SuperGetMV("ES_PRODLIC") // "000000123456789"
	Local _cTes 		:= mv_par03
	Local _cCondPag 	
	Local _cFilDes  	:= SuperGetMV("ES_CODEFL") 	// xFilial("SC5")
	Local _cFilOri		:= xFilial("SC5")
	Local _cNrPedVen 	:= ""
	Local _cSerie       := mv_par04
	Local _aArea  		:= GetArea()
	Local _xFilZC7		:= xFilial("ZC7")
	Local _cIDPDES      
	Local _dVencto      := dDataBase


	Local _cQuery 		:= "SELECT ZC7_STATUS, ZC7_CODCLI, ZC7_LOJCLI, ZC7_NOME, ZC7_DTEMIS, ZC7_VALDES, ZC7_NOTA, ZC7_SERIE, ZC7_IDPDES, R_E_C_N_O_ ZC7_RECNO  " 

	_cQuery += "FROM " + RetSQLName("ZC7") + " ZC7 "
	_cQuery += "WHERE ZC7.ZC7_FILIAL = '" + _xFilZC7 + "' AND " 
	_cQuery += "ZC7.ZC7_IDPDES =' '  AND "
	_cQuery += "ZC7.D_E_L_E_T_ =' '  " 
	_cQuery += "ORDER BY ZC7.ZC7_NOTA, ZC7.ZC7_SERIE "

	_cQuery := ChangeQuery(_cQuery)
	dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"TMP",.F.,.T.)

	Dbselectarea("TMP")
	dbGotop()

	While !Eof() 
		If Empty(TMP->ZC7_STATUS)
			dbSelectArea("SC9")
			dbSetOrder(6) // C9_FILIAL+C9_SERIENF+C9_NFISCAL+C9_CARGA+C9_SEQCAR                                                                                                              
			If SC9->(dbSeek(xFilial("SC9")+(TMP->ZC7_NOTA, TMP->ZC7_SERIE)))
				DbselectArea("SC5")
				dbSetOrder(1)
				If DbSeek(xFilial("SC5")+SC9->C9_PEDIDO)

					Begin Transaction

						Incproc("Gera��o de Licenciamento: " + TMP->ZC7_NOTA)

						_cPedOri  := SC9->C9_PEDIDO
						_cCondPag := SC5->C5_CONDPAG
						_cIDPDES  := GetSx8Num("ZC7","ZC7_IDPDES")
						ConfirmSX8()

						If TMP->ZC7_VALDES > 0
							dbSelectArea("ZC7")
							dbGoto(TMP->ZC7_RECNO) 
							ZC7->(RecLock("ZC7", .F.))
							ZC7->ZC7_STATUS		:= "*"
							ZC7->ZC7_IDPDES		:= _cIDPDES
							ZC7->(MsUnLock())


							// Gerando Pedido
							_cNrPedVen := GeraPV( TMP->ZC7_CODCLI, TMP->ZC7_LOJCLI, TMP->ZC7_VALDES, _cProduto, _cTes, _cCondPag, _cFilDes, _cPedOri, _cIDPDES)

							// Gerando Nota Fiscal
							If !Empty(_cNrPedVen)	
								GeraNFS(_cNrPedVen, _cSerie, _cFilDes, _cFilOri)
							EndIf
						Else
							If TMP->ZC7_VALDES < 0
								// MsgAlert("Gerando NCC!!")
								// Gerando NCC
								GeraNCC(TMP->ZC7_NOTA, TMP->ZC7_SERIE, TMP->ZC7_CODCLI, TMP->ZC7_LOJCLI, TMP->ZC7_VALDES, _cFilDes, _dVencto)
								//  GeraNCC(        cNota,         cSerie,         cCODCLI,         cLOJCLI,            nVal,  cFilDes   dVencto)

							EndIf
						EndIf
					End Transaction
				EndIf

			EndIf

		EndIf	
		dbSelectArea("TMP")
		DbSkip()
	Enddo

	dbSelectArea("TMP")
	DbCloseArea() 
	RestArea(_aArea)

Return 


//------------------------------------------------------------------->
/*/{Protheus.doc} GeraPV

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->


Static Function GeraPV(cCODCLI, cLOJCLI, nVal, cProduto, cTes, cCondPag, cFilDes, cPedOri, cIDPDES)
	Local _aArea  	   := GetArea()
	Local aCab		   := {}
	Local aItens 	   := {}
	Local _aSC6 	   := {}
	Local _aSC9 	   := {}
	Local _cNrPedVen   := ""

	PRIVATE lMsErroAuto := .F.

	// MsgAlert("Gerando Pedido de Venda de Licenciamento")

	aCab := {	{"C5_FILIAL"    ,xFilial("SC5")	     	,Nil},;
	{"C5_TIPO"      , "N"            					,Nil},;
	{"C5_CLIENTE"   , cCODCLI							,Nil},;
	{"C5_LOJACLI"   , cLOJCLI  	  						,Nil},;
	{"C5_CONDPAG"   , cCondPag							,Nil},;
	{"C5_CLIENT" 	, cCODCLI							,Nil},;
	{"C5_LOJAENT"   , cLOJCLI							,Nil} } 

	aAdd(aItens,{  	{"C6_FILIAL"	, xFilial("SC6")		, nil},;
	{"C6_ITEM"   	, "01" 		 							, nil},;
	{"C6_PRODUTO"	, cProduto			   					, nil},; // TEM DE AJUSTAR Produto, Vendedor, TES, Data
	{"C6_TES"    	, cTes			 						, nil},;
	{"C6_QTDVEN" 	, 1	 									, nil},;
	{"C6_PRCVEN" 	, nVal	 								, nil},;
	{"C6_PRCUNIT" 	, 0										, nil},;
	{"C6_QTDLIB" 	, 1										, nil},;
	{"C6_ENTREG" 	, dDataBase								, nil}})

	msExecAuto({|x,y,z|MATA410(x,y,z)},aCab,aItens,3)

	ROLLBACKSX8()

	If lMSErroAuto
		MOSTRAERRO()
	Else
		//����������������������������������������������������Ŀ
		//� Gravar o campo C5_XIDPDES do PV Original           � 
		//������������������������������������������������������
		DbselectArea("SC5")
		dbSetOrder(1)
		If DbSeek(xFilial("SC5")+cPedOri)
			Reclock("SC5",.F.)
			SC5->C5_XIDPDES :=  cIDPDES 
			MsUnLock()
			_cNrPedVen := SC5->C5_NUM
		EndIf

		dbSelectArea("SX3")
		dbSetOrder(1)
		MsSeek("SC5")
		aCpos:={}
		//����������������������������������������������������Ŀ
		//� Seleciona os campos do cabecalho do PV Original    � 
		//������������������������������������������������������
		While !Eof() .And. SX3->X3_ARQUIVO == "SC5"

			If X3Uso(X3_USADO) .And. cNivel >= SX3->X3_NIVEL .And. SX3->X3_CONTEXT<>"V" .And. !(Trim(SX3->X3_CAMPO)$("C5_FILIAL#C5_NUM#C5_TIPO#C5_CLIENTE#C5_LOJACLI#C5_CONDPAG#C5_CLIENT#C5_LOJAENT#C5_EMISSAO"))
				Aadd(aCpos, {AllTrim(SX3->X3_CAMPO), SC5->(FieldGet(FieldPos(SX3->X3_CAMPO)))} )
			EndIf

			dbSelectArea("SX3")
			dbSkip()
		EndDo

		//��������������������������������������������������������������������������������������������Ŀ
		//� Replicar os conte�dos do campos do cabecalho do PV Original ao Pedido de Licenciamento     � 
		//����������������������������������������������������������������������������������������������
		DbselectArea("SC5")
		dbSetOrder(1)
		If DbSeek(xFilial("SC5")+_cNrPedVen)
			Reclock("SC5",.F.)
			SC5->C5_XIDPDES :=  cIDPDES
			SC5->C5_FILIAL  :=  cFilDes // Filial de Licenciamento
			For _nI:=1 to Len(aCpos)
				SC5->&(aCpos[_nI,1]):= aCpos[_nI,2]
			Next _nI
			MsUnLock()
		EndIf


		DbselectArea("SC6")
		dbSetOrder(1)
		_aSC6 := {}
		If DbSeek(xFilial("SC6")+_cNrPedVen)
			While !Eof() .And. _cNrPedVen == SC6->C6_NUM .And. xFilial("SC6") == SC6->C6_FILIAL
				AADD(_aSC6, SC6->(RecNo()))
				SC6->(dbSkip())
			EndDo
			For _nSC6:= 1 to Len(_aSC6)
				DbselectArea("SC6")
				dbGoto(_aSC6[_nSC6])
				Reclock("SC6",.F.)
				SC6->C6_FILIAL := cFilDes   	// Filial de Licenciamento
				MsUnLock()
			Next _nSC6
		EndIf

		DbselectArea("SC9")
		dbSetOrder(1)
		_aSC9 := {}
		If DbSeek(xFilial("SC9")+_cNrPedVen)
			While !Eof() .And. _cNrPedVen == SC9->C9_PEDIDO .And. xFilial("SC9") == SC9->C9_FILIAL
				AADD(_aSC9, SC9->(RecNo()))
				SC9->(dbSkip())
			EndDo
			For _nSC9:= 1 to Len(_aSC9)
				DbselectArea("SC9")
				dbGoto(_aSC9[_nSC9])
				Reclock("SC9",.F.)
				// SC9->C9_FILIAL := cFilDes   	// Filial de Licenciamento
				SC9->C9_BLEST  := "" 			// LIBERACAO DO ESTOQUE
				SC9->C9_BLCRED := "" 			// LIBERACAO DO CREDITO
				MsUnLock()
			Next _nSC9
		EndIf

		MsgBox("Pedido de Venda Gerado com Sucesso!!!","Gera��o PV Efetuada","Info")

	EndIf
	RestArea(_aArea)
Return(_cNrPedVen)


//------------------------------------------------------------------->
/*/{Protheus.doc} GeraNFS

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function GeraNFS(cPedido, cSerie, cFilDes, cFilOri)

	Local _aArea  := GetArea()
	Local aPvlNfs := {}
	Local aPvAux  := {}
	Local _aSD2   := {}
	Local _aSE1   := {}
	Local _aSE3   := {}
	Local _nSD2
	Local _nSE1
	Local _nSE3

	// MsgAlert("Gerando Nota Fiscal de Licenciamento")

	SC5->( dbSetOrder(1) )
	SC5->( dbSeek(xFilial("SC5")+cPedido),.F.)
	SC6->( dbSetOrder(1) )
	SC6->( dbSeek(xFilial("SC6")+cPedido),.F.)
	SC9->( dbSetOrder(1) )
	SC9->( dbSeek(xFilial("SC9")+cPedido),.F.)
	SE4->( dbSetOrder(1) )
	SE4->( dbSeek(xFilial("SE4")+SC5->C5_CONDPAG) )
	SB1->( dbSetOrder(1) )
	SB1->( dbSeek(xFilial("SB1")+SC6->C6_PRODUTO) )
	SB2->( dbSetOrder(1) )
	SB2->( dbSeek(xFilial("SB2")+SC6->C6_PRODUTO) )
	SF4->( dbSetOrder(1) )
	SF4->( dbSeek(xFilial("SF4")+SC6->C6_TES) )

	aAdd(aPvlNfs,{          SC6->C6_NUM     ,; //DOC
	SC6->C6_ITEM    ,;
	SC9->C9_SEQUEN  ,;     // SC6->C6_LOCAL   ,;
	SC6->C6_QTDVEN  ,;
	SC6->C6_VALOR   ,;
	SC6->C6_PRODUTO ,;
	.F.             ,;
	SC9->(RecNo())  ,;
	SC5->(RecNo())  ,;
	SC6->(RecNo())  ,;
	SE4->(RecNo())  ,;
	SB1->(RecNo())  ,;
	SB2->(RecNo())  ,; 
	SF4->(RecNo()) })

	If Len(aPvlNfs) <> 0
		/*
		FUNCAO MaPvlNfs(Param1,Param2,...,ParamN)
		aPvlNfs:  	Array com os itens a serem gerados
		cSerie:  	Serie da Nota Fiscal
		lMostraCtb: 	Mostra Lct.Contabil
		lAglutCtb:  	Aglutina Lct.Contabil
		lCtbOnLine: 	Contabiliza On-Line
		lCtbCusto: 		Contabiliza Custo On-Line
		lReajusta: 		Reajuste de preco na nota fiscal
		nCalAcrs: 		Tipo de Acrescimo Financeiro
		nArredPrcLis: 	Tipo de Arredondamento
		lAtuSA7: 		Atualiza Amarracao Cliente x Produto
		lECF: 			Cupom Fiscal
		cEmbExp: 		Numero do Embarque de Exportacao
		bAtuFin: 		Code block para complemento de atualizacao dos titulos financeiros.
		bAtuPGerNF: 	Code block para complemento de atualizacao dos dados apos a geracao da nota fiscal.
		bAtuPvl: 		Code Block de atualizacao do pedido de venda antes da geracao da nota fiscal
		*/    
		// cNota := MaPvlNfs(aPvlNfs,cSerie , lMostraCtb, lAglutCtb, lCtbOnLine, lCtbCusto, lReajusta, nCalcrs, nArredPrcLis, lAtuSA7, lECF)
		cNota := MaPvlNfs(aPvlNfs,cSerie , .F.       , .F.      , .F.       , .F.      , .F.      , 0      , 0           , .F.    , .F.)	                           


		If !Empty(cNota)
			//----> Ajustando Filiais

			DbselectArea("SF3")
			dbSetOrder(1)
			If DbSeek(xFilial("SF3")+cNota)
				Reclock("SF3",.F.)
				SF3->F3_FILIAL := cFilDes   	// Filial de Licenciamento
				MsUnLock()
			EndIf

			DbselectArea("SF2")
			dbSetOrder(1)
			If DbSeek(xFilial("SF2")+cNota)
				Reclock("SF2",.F.)
				SF2->F2_FILIAL := cFilDes 	// Filial de Licenciamento
				MsUnLock()
			EndIf


			DbselectArea("SD2")
			dbSetOrder(1)
			_aSD2 := {}
			If DbSeek(xFilial("SD2")+cNota)
				While !Eof() .And. _cNrPedVen == SD2->D2_DOC .And. xFilial("SD2") == SD2->D2_FILIAL
					AADD(_aSD2, SD2->(RecNo()))
					SD2->(dbSkip())
				EndDo
				For _nSD2:= 1 to Len(_aSD2)
					DbselectArea("SD2")
					dbGoto(_aSD2[_nSD2])
					Reclock("SD2",.F.)
					SC9->C9_FILIAL := cFilDes   	// Filial de Licenciamento
					SD2->D2_BLEST  := "" 			// LIBERACAO DO ESTOQUE
					SD2->D2_BLCRED := "" 			// LIBERACAO DO CREDITO
					MsUnLock()
				Next _nSD2
			EndIf

			DbselectArea("SE1")
			dbSetOrder(1)
			_aSE1 := {}
			If DbSeek(xFilial("SE1")+cNota)
				While !Eof() .And. SE1->E1_PREFIXO == cSerie .And. SE1->E1_NUM == cNota .And. xFilial("SE1") == SE1->E1_FILIAL
					AADD(_aSE1, SE1->(RecNo()))
					SE1->(dbSkip())
				EndDo
				For _nSE1:= 1 to Len(_aSE1)
					DbselectArea("SE1")
					dbGoto(_aSE1[_nSE1])
					Reclock("SE1",.F.)
					SE1->E1_FILIAL := cFilOri   	// Filial de Origem
					MsUnLock()
				Next _nSE1
			EndIf

			DbselectArea("SE3")
			dbSetOrder(1)
			_aSE1 := {}
			If DbSeek(xFilial("SE3")+cNota)
				While !Eof() .And. SE3->E3_PREFIXO == cSerie .And. SE3->E3_NUM == cNota .And. xFilial("SE3") == SE3->E1_FILIAL
					AADD(_aSE3, SE3->(RecNo()))
					SE3->(dbSkip())
				EndDo
				For _nSE3:= 1 to Len(_aSE3)
					DbselectArea("SE3")
					dbGoto(_aSE3[_nSE3])
					Reclock("SE3",.F.)
					SE3->E3_FILIAL := cFilOri   	// Filial de Origem
					MsUnLock()
				Next _nSE3
			EndIf

			MsgBox("Nota fiscal Gerada com Sucesso!!!","Gera��o NFS Efetuada","Info")

		EndIf
	Endif

	RestArea(_aArea)

Return()

//------------------------------------------------------------------->
/*/{Protheus.doc} GeraNCC

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

Static Function GeraNCC(cNota, cSerie, cCODCLI, cLOJCLI, nVal, cFilDes, dVencto)

	Local aSE1 := {}

	PRIVATE lMsErroAuto := .F.

	nVal := nVal * -1 

	aadd(aSE1,{'E1_PREFIXO'		, cSerie			, nil })
	aadd(aSE1,{'E1_NUM    '		, cNota				, nil }) 
	aadd(aSE1,{'E1_PARCELA'		, "  " 				, nil })
	aadd(aSE1,{'E1_TIPO   '		, "NCC"   			, nil })
	aadd(aSE1,{'E1_CLIENTE'		, cCODCLI  			, nil })
	aadd(aSE1,{'E1_LOJA   '		, cLOJCLI  			, nil })
	aadd(aSE1,{'E1_EMISSAO'  	, dVencto			, NIL })
	aadd(aSE1,{'E1_VENCTO '   	, dVencto			, NIL })
	aadd(aSE1,{'E1_VENCREA'  	, dVencto			, NIL })
	aadd(aSE1,{'E1_VALOR  '		, nVal				, nil })

	MSExecAuto({|x,y| Fina040(x,y)}, aSE1, 3) // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o

	If (lMsErroAuto)
		MostraErro()
		DisarmTransaction()
	Else

		DbselectArea("SE1")
		dbSetOrder(1)
		_aSE1 := {}
		If DbSeek(xFilial("SE1")+cSerie+cNota+"  NCC") // E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO 
			Reclock("SE1",.F.)
			SE1->E1_FILIAL := cFilDes   	// Filial de Licenciamento
			MsUnLock()
		EndIf


		Alert("T�tulo NCC inclu�do com sucesso!")
	Endif 

Return