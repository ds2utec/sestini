#include "rwmake.ch"

#include "topconn.ch"

#INCLUDE "PROTHEUS.CH"   

// Valida se o rateio esta sendo usado

User Function MT100TOK()

Local aArea:=GetArea()

Local lRetor:=.T.

Local nIx:=0

 


If AllTrim(FunName()) == "MATA103"

For nIx:=1 to Len(aCols)

      If !GDDeleted(nIx)

            // C.Custo Vazio e n�o tem rateio

            If Empty(GDFieldGet("D1_CC",nIx)) .and. GDFieldGet("D1_RATEIO",nIx)<>"1"

                  lRetor:=.F.

                  Aviso("Campos Obrigatorios","Preencha os campos : Centro de Custos / Conta Contabil / Canal-> "+Str(nIx,3),{"Ok"})

            EndIf        


            If Empty(GDFieldGet("D1_CLVL",nIx)  ) .and. GDFieldGet("D1_RATEIO",nIx)<>"1"

                  lRetor:=.F.

                 // Aviso("Classe de Valor","Classe de Valor nao preenchido no item -> "+Str(nIx,3),{"Ok"})

            EndIf   

            If Empty(GDFieldGet("D1_CONTA",nIx)  ) .and. GDFieldGet("D1_RATEIO",nIx)<>"1"

                  lRetor:=.F.

                  //Aviso("Conta Nao Preenchida", "Conta nao preenchida no item -> "+Str(nIx,3),{"Ok"})

            EndIf   

            If Empty(GDFieldGet("D1_EC05DB",nIx)  ) .and. GDFieldGet("D1_RATEIO",nIx)<>"1"

                  lRetor:=.F.

                  //Aviso(ntidade Diretoria�,�Entidade Diretoria nao preenchido no item -> "+Str(nIx,3),{"Ok"})

             Endif 
      Endif
Next

Endif 

RestArea(aArea)

Return(lRetor)