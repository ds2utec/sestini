#INCLUDE "TopConn.ch"

/*
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������Ŀ��
���Programa  � GerCodPro� Autor � Jose Choite Kita Jr             � Data � 04/06/08 ���
�����������������������������������������������������������������������������������Ĵ��
���Descri��o � Gera o codigo do produto                                             ���
�����������������������������������������������������������������������������������Ĵ��
���Uso       �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
���Parametros� 						                                                ���
�����������������������������������������������������������������������������������Ĵ��
���Retorno   �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
������������������������� HISTORICO DE ATUALIZACOES DA ROTINA ���������������������Ĵ��
�����������������������������������������������������������������������������������Ĵ��
��� Desenvolvedor   � Data   �Solic.� Descricao                                     ���
�����������������������������������������������������������������������������������Ĵ��
��� Choite          �04/06/08�001026� Implementacao                                 ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
������������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������  
O tipo e grupo do produto so podem ser alterados na inclusao, nao sera permitido altera-los na opcao
de alteracao pois os mesmos sao responsaveis pela geracao do codigo que e�a chave primaria do produto                      


Alterar Campos
==========================
Campo:			B1_COD              
Formato:		@!
Propriedade:	Visualizar

Campo:			B1_TIPO
Propriedade:	Alterar                        
Modo edicao:	INCLUI
Vld. Usuario:	U_VTaman(2) .and. U_GerCodPro()

Campo:			B1_GRUPO
Propriedade:	Alterar
Modo edicao:	INCLUI
Vld. Usuario:	U_VTaman(4) .and. U_GerCodPro()

*/   
USER FUNCTION GerCodPro()
LOCAL aArea	   := GetArea()
LOCAL cSql	    := ""
LOCAL lRet 		  := .T.
//***** Ajustar conforme necessidade do Cliente ****//
LOCAL cPrefCod :=  M->B1_TIPO + M->B1_GRUPO                    //Composicao do Prefixo Ex: MP0001 
LOCAL nTamCod  := 11                                           //Tamanho Total do Codigo Produto Ex: MP001001001 -> tamanho 11
//*********//
LOCAL nTamPref := Len(cPrefCod)                                //Tamanho do Prefixo
LOCAL nTamSeq  := nTamCod - nTamPref                           //Tamanho do Sequencial (Tamanho Total - Tamanho Prefixo) Ex: MP0001XXX -> 11 - 8 = 3

IF INCLUI .and. !empty(M->B1_TIPO) .And. ! empty(M->B1_GRUPO) 
	
	cSQL := "SELECT MAX(SB1.B1_COD) MAXCOD "
	cSql += " FROM " + RetSqlName("SB1") + " SB1"
	cSQL += " WHERE SB1.B1_FILIAL = '" + xFilial("SB1") + "' AND SB1.D_E_L_E_T_ <> '*' "
	cSQL += "   AND SB1.B1_GRUPO = '" + padr(M->B1_GRUPO,TamSx3("B1_GRUPO")[1]) + "'"  // POR SEGURANCA (OUTROS TIPOS SEMELHANTES COMO MDO)
	cSQL += "   AND SUBSTRING(SB1.B1_COD,1," + cValToChar(nTamPref)+ ") = '" + cPrefCod + "'"
	
	cSQL :=  ChangeQuery(cSQL)
	TCQUERY cSQL NEW ALIAS "QRY"
	
	IF !empty(QRY->MAXCOD)
		cRet := Soma1(alltrim(QRY->MAXCOD) , len(alltrim(QRY->MAXCOD)))
	ELSE
		cRet := cPrefCod + STRZERO(1, nTamSeq)  
	ENDIF
	              
	IF len(cRet) <> nTamCod
  lRet := .F.
  HS_MsgInf("O tamanho [" + cValToChar(Len(cRet)) + "] do c�digo do produto gerado est� incorreto, contactar o depto de Tecnologia."  , "Aten��o", "Tamanho do c�digo incorreto")
	ENDIF
	
	M->B1_COD := Padr(cRet,TamSx3("B1_COD")[1])
	
	DbSelectArea("QRY")
	DbCloseArea()
	
ENDIF

RestArea(aArea)
RETURN(lRet)


/*
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������Ŀ��
���Programa  � VTaman   � Autor � Jose Choite Kita Jr             � Data � 29/11/07 ���
�����������������������������������������������������������������������������������Ĵ��
���Descri��o � Valida tamanho que o campo deve ter                                  ���
�����������������������������������������������������������������������������������Ĵ��
���Uso       �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
���Parametros� nTamRef  tamanho que o conteudo informado deve ter                   ���
�����������������������������������������������������������������������������������Ĵ��
���Retorno   �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
������������������������� HISTORICO DE ATUALIZACOES DA ROTINA ���������������������Ĵ��
�����������������������������������������������������������������������������������Ĵ��
��� Desenvolvedor   � Data   �Solic.� Descricao                                     ���
�����������������������������������������������������������������������������������Ĵ��
��� Choite          �05/12/07�000794� Implementacao                                 ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
������������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
*/      
USER FUNCTION VTaman(nTamRef)
LOCAL lRet 		:= .T.
LOCAL nTamInfo	:= len( alltrim( &(readvar())  ) )
LOCAL cCampo    := alltrim(substr(readvar(),4,20))

IF nTamRef<>nTamInfo
	lRet := .F.
	HS_MsgInf("O " + Alltrim(rettitle(cCampo)) + " deve ter exatamente " + alltrim(str(nTamRef)) + " caracteres."  , "Aten��o", "Quantidade de caracteres inv�lido")
ENDIF

RETURN(lRet)   