#Include 'Protheus.ch'

//Específico Sestini
//Ponto de entrada para alterar o SEA->EA_MODELO do bordero de GNRE.

User Function F240AlMod() 

Local _cModelo := Paramixb[1] 

                         
If _cModelo == "91" 
   _cModelo := "16" 
EndIf 

Return (_cModelo)

