#Include 'protheus.ch'
#Include 'parmtype.ch'

/*/{Protheus.doc} MT410CPY
P.E. antes da abertura da interface de c�pia do pedido de venda

O ponto de entrada MT410CPY � executado ap�s o preenchimento do acols e das variaveis da enchoice, antes da apresenta��o da tela, 
permitindo altera��o do acols e variaveis da enchoice.

@author DS2U (SDA)
@since 20/08/2018
@type function
/*/
User Function MT410CPY()

	// Funcao de controle de campos que devem ser limpados na c�pia do pedido de venda
	clearVars()
	
Return

/*/{Protheus.doc} clearVars
Funcao responsavel por limpar campos que n�o devem ser copiados
@author Triyo (SDA)
@since 07/01/2017
@version undefined

@type function
/*/
Static Function clearVars()

	local nlPos
	local alFieldsCln:= {}
	local nlx
	local nly

	// Campos do cabecalho
	M->C5_XIDPROM := space( tamSX3("C5_XIDPROM")[1] )
	
	// Campos dos itens 
	// Basta adicionar o campo no array
	AADD( alFieldsCln, "C6_XIDPROM" )
	AADD( alFieldsCln, "C6_PRCVEN" )
	AADD( alFieldsCln, "C6_PRUNIT" )
	
	
	for nly := 1 to len( aCols )
	
		for nlx := 1 to len( alFieldsCln )
		
			nlPos	:= gdFieldPos( alFieldsCln[nlx] )

			if ( nlPos > 0 )
				aCols[nly][nlPos] := criaVar( alFieldsCln[nlx] )
			endif
		
		next nlx
		
	next nly
	
Return