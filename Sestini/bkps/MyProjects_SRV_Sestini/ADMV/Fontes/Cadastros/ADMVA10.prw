#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA10
Rotina de cadastro de Canais por vendedor
@author DS2U (SDA)
@since 06/08/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA10( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 1
			uRet := execView( uParam )
			
		case nlOpc == 2
			uRet := expFilSB1( uParam ) // Monta experessao de filtro para consulta SQL, onde a tabela principal � SB1 e o retorno deve ser os produtos permitidos para o vendedor corrente
			
		case nlOpc == 3
			uRet := vldPrdInC6( uParam ) // valida digitacao do produto no item do pedido de venda
			
		case nlOpc == 4
			uRet := getLocVnd( uParam ) // Retorna codigos dos locais permitidos por vendedor
			
		case nlOpc == 5
			uRet := vldLocInC6( uParam ) // valida digitacao do produto no item do pedido de venda
			
	endCase

Return uRet

/*/{Protheus.doc} execView
Responsavel por realizar a exec view do cadastro de canais por vendedor
@author DS2U (SDA)
@since 08/08/2018
@version 1.0

@type function
/*/
Static Function execView()
	fwExecView("Canais", 'ADMVA10', MODEL_OPERATION_INSERT,, { || .T. } )
Return

/*/{Protheus.doc} ModelDef
Modeldef do cadastro de Canais por Vendedor
@author sergi
@since 02/08/2018
@version 1.0
@return olModel, Objeto de model do cadastro

@type function
/*/
Static Function ModelDef()

	local olModel
	local oStruSA3	:= FWFormStruct( 1, 'SA3', { |x| allTrim( x ) $ 'A3_COD,A3_NOME' }, /*lViewUsado*/ )
	local oStruZCB	:= FWFormStruct( 1, 'ZCB', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA10M', /*{ |olModel| preVld( olModel ) }*//* bPreValidacao*/,{ |olModel| tudoOK( olModel ) }/*bPosValidacao*/, { |olModel| commit( olModel ) }/*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'SA3MASTER', /*cOwner*/, oStruSA3, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'ZCBDETAIL', 'SA3MASTER', oStruZCB, /*{|olModel,  nLine,cAction,  cField| PRR13LPre(olModel, nLine, cAction, cField)}*/ /*bLinePre*/, /*{ |olModel| ZCBVldLPos( olModel ) }*/ /*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'ZCBDETAIL', { { 'ZCB_FILIAL', 'xFilial( "ZCB" )' }, { 'ZCB_VEND', 'A3_COD' }}, ZCB->( IndexKey( 1 ) ) )
	olModel:SetPrimaryKey( {"A3_FILIAL", "A3_COD"} )
	olModel:SetDescription('Cadastro de Canais x Vendedor')
	olModel:getModel('SA3MASTER' ):SetDescription('Cadastro de Canais x Vendedor') 
	
	olModel:getModel('ZCBDETAIL'):SetOptional( .F. )
	olModel:getModel('ZCBDETAIL'):SetUniqueLine({"ZCB_CANAL"})
	
	olModel:SetActivate( {|olModel| cotaLoad( olModel ) } )

Return olModel

/*/{Protheus.doc} ViewDef
Viewdef do cadastro de Canais por Vendedor
@author sergi
@since 02/08/2018
@version 1.0
@return olView, Objeto de view do cadastro

@type function
/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local oStruSA3	:= FWFormStruct( 2, 'SA3', { |x| allTrim( x ) $ 'A3_COD,A3_NOME' }, /*lViewUsado*/ )
	local oStruZCB	:= FWFormStruct( 2, 'ZCB' )
	
	// Cria a estrutura a ser usada na View
	local olModel    := FWLoadModel( 'ADMVA10' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_SA3', oStruSA3, 'SA3MASTER' )		
		
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_ZCB', oStruZCB, 'ZCBDETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 20 )
	olView:CreateHorizontalBox( 'INFERIOR', 80 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_SA3', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_ZCB', 'INFERIOR' )
	
	olView:EnableTitleView( 'VIEW_ZCB', 'Canal x Vendedor' )
	olView:SetViewProperty("VIEW_SA3", "ONLYVIEW")
	olView:HideFolder("VIEW_SA3",1,1)
	olView:SetNoDeleteLine( "VIEW_ZCB" )

Return olView

/*/{Protheus.doc} tudoOK
Responsavel por validar todo o cadastro antes de enviar para a gra��o no banco de dados
@author DS2U (SDA)
@since 02/08/2018
@version 1.0
@return llRet, Se .T., cadastro validado, se .F. houve falha no preenchimento do cadastro
@param olModel, object, Modelo de dados do cadastro
@type function
/*/
Static Function tudoOK( olModel )

	local llRet			:= .T.
	local nlx
	local alOrdens		:= {}
	local nlOrdem		:= 0
	local olModelZCB	:= olModel:getModel('ZCBDETAIL')

	// Valida se o a ordem � unica
	for nlx := 1 to olModelZCB:length()
			
		olModelZCB:goLine( nlx )
		
		nlOrdem := olModelZCB:getValue( "ZCB_ORDEM" )
		
		if ( aScan( alOrdens, {|x| x == nlOrdem } ) > 0 )
			llRet := .F.
			exit
		else
			AADD( alOrdens, nlOrdem )
		endif
		
	next nlx
	
	if ( .not. llRet )
		U_ADMVXFUN( 1,	{"ZCB_ORDEM", "O campo ordem deve ser unico entre os canais!" } )
	endif

Return llRet

/*/{Protheus.doc} commit
Responsavel por realizar a gravacao no banco de dados
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return llRet, Se .T. validacao OK, se nao houve falha no processamento
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function commit( olModel )

	local llRet			:= .T.
	local nlx
	local olModelZCB	:= olModel:getModel('ZCBDETAIL')

	// Exclui do banco de dados os registro vinculados ao produto / canal para considerar apenas os novos registros
	if ( tcSqlExec( "DELETE FROM " + retSqlName("ZCB") + " WHERE D_E_L_E_T_ = ' ' AND ZCB_VEND = '" + SA3->A3_COD + "' " ) < 0 )
		
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZCB_VEND", "Nao foi possivel excluir os canais vinculados ao vendedor [" + allTrim( SA3->A3_COD ) + "]" + CRLF + tcSqlError() } )
		
	else
	
		llRet := .T.
		
		for nlx := 1 to olModelZCB:length()
			
			olModelZCB:goLine( nlx )
			
			if ( recLock( "ZCB", .T. ) )
			
				ZCB->ZCB_FILIAL	:= xFilial("ZCB")
				ZCB->ZCB_VEND	:= SA3->A3_COD
				ZCB->ZCB_CANAL	:= olModelZCB:getValue( "ZCB_CANAL" )
				ZCB->ZCB_ORDEM	:= olModelZCB:getValue( "ZCB_ORDEM" )
				ZCB->ZCB_ATIVAD	:= olModelZCB:getValue( "ZCB_ATIVAD" )
			
				ZCB->( msUnLock() )
				
			endif
			
		next nlx
	
	endif

Return llRet

/*/{Protheus.doc} cotaLoad
Carrega a grid de itens com os canais do ADMV
@author DS2U ( SDA )
@since 01/08/2018
@version 1.0
@param olModel, object, Modelo de dados do MVC de cadastro de cotas por item
@type function
/*/
Static Function cotaLoad( olModel )

	local alArea		:= getArea()
	local nlOpc			:= olModel:getOperation()
	local olModelSA3	:= olModel:getModel('SA3MASTER' )
	local olModelZCB	:= olModel:getModel('ZCBDETAIL')
	local alCanais		:= {}
	local nlx
	local nly
	local alInfoZCB		:= {}
	local clField		:= ""
	local clContent		:= ""
	local llFound		:= .F.
	local nlLine
	local olStruct
	local nlLinAdd		:= 0
	
	if ( nlOpc == 3 .or. nlOpc == 4 )
	
		dbSelectArea("ZCB")
		ZCB->( dbSetOrder( 1 ) ) // ZCB_FILIAL, ZCB_VEND, ZCB_CANAL
		
		dbSelectArea("SA3")
		SA3->( dbSetOrder( 1 ) )
		
		olStruct	:= olModelZCB:getStruct()
		alInfoZCB	:= olStruct:getFields()
		
		olModelSA3:loadValue("A3_COD"	, SA3->A3_COD )
		olModelSA3:loadValue("A3_NOME"	, SA3->A3_NOME )
	
		alCanais	:= getCanais( .T. )  // Adiciona apenas os canais 1� linha, pois diferente disso, nao controla cotas
		
		// Ponteiro para varrer os canais do ADMV
		for nlx := 1 to len( alCanais )
		
			llFound := ZCB->( dbSeek( xFilial("ZCB") + SA3->A3_COD + alCanais[nlx][1] ) )
			nlLinAdd++

			if ( nlOpc == 3 .or. ( nlOpc == 4 .and. .not. llFound ) )
		
				if (  nlLinAdd > 1 .or. nlOpc == 4 )
					nlLine := olModelZCB:AddLine()
					olModelZCB:goLine( nlLine )
				endif
				
				// Ponteiro para varrer os campos da tabela ZCB e preencher as informacoes na grid conforme os canais existentes
				for nly := 1 to len( alInfoZCB )
				
					clField		:= allTrim( alInfoZCB[nly][3] )
					clContent	:= ""
				
					do Case
					
						case clField == "ZCB_FILIAL"
						
							if ( llFound )
								clContent	:= ZCB->ZCB_FILIAL
							else
								clContent	:= xFilial("ZCB")
							endif
						
						case clField == "ZCB_VEND"
						
							if ( llFound )
								clContent	:= ZCB->ZCB_VEND
							else
								clContent	:= SA3->A3_COD
							endif
							
						case clField == "ZCB_CANAL"
						
							if ( llFound )
								clContent	:= ZCB->ZCB_CANAL
							else
								clContent	:= alCanais[nlx][1]
							endif
						
						case clField == "ZCB_DESCAN"
						
							if ( llFound )
								clContent	:= posicione("ZC0", 1, xFilial("ZC0") + ZCB->ZCB_CANAL, "ZC0_DESCAN" )
							else
								clContent	:= alCanais[nlx][2]
							endif
						
						case clField == "ZCB_ORDEM"
						
							if ( llFound )
								clContent	:= ZCB->ZCB_ORDEM
							else
								clContent	:= 0
							endif
					
					endCase
					
					if ( .not. empty( clContent ) )
						olModelZCB:loadValue( clField, clContent )
					endif
				
				next nly
				
			endif
				
		next nlx
	
	endif
	
	restArea( alArea )

Return .T.

/*/{Protheus.doc} getCanais
Responsavel por tratar os canais do ADMV
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return U_ADMVXFUN( 4 ), Retorno da funcao U_ADMVXFUN( 4 )
@param llOnlyPLin, boolean, Busca canais somente de primeira linha
@type function
/*/
Static Function getCanais( llOnlyPLin )
Return U_ADMVXFUN( 4, llOnlyPLin )

/*/{Protheus.doc} expFilSB1
Funcao responsavel por retorna a expressao SQL para filtro dos produtos permitidos para o vendedor
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return clSQL, Expressao SQL com o filtro

@type function
/*/
Static Function expFilSB1()

	local alArea	:= getArea()
	local clLocais	:= ""
	local clSQL		:= ""
	local clVendedor:= getVendAtu()
	
	if ( .not. empty( clVendedor ) )
	
		clLocais := getLocVnd( clVendedor )
		
		if ( .not. empty( clLocais ) )
	
			if ( .not. empty( clLocais ) )
	
				clSQL += " B1_COD IN ( "
				clSQL += 				" SELECT " 
				clSQL += 					" DISTINCT B2_COD " 
				clSQL += 				" FROM " 
				clSQL += 						retSqlTab( "SB2" )
				clSQL += 				" WHERE "
				clSQL += 					" SB2.B2_FILIAL = '" + fwxFilial("SB2") + "' "
				clSQL += 					" AND B2_LOCAL IN " + formatIn( clLocais, "/" ) 
				clSQL += 					" AND SB2.D_E_L_E_T_ = ' ' ) "
				
			endif
			
		endif
		
	endif

	restArea( alArea )

Return clSQL

/*/{Protheus.doc} getVendAtu
Reponsavel por identificar o vendedor 
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return clVendedor, Codigo do vendedor corrente

@type function
/*/
Static Function getVendAtu()

	local alArea	:= getArea()
	local clVendedor:= ""
	
	dbSelectArea( "SA3" )
	SA3->( dbSetOrder( 7 ) ) // A3_FILIAL, A3_CODUSR, R_E_C_N_O_, D_E_L_E_T_
	
	if ( SA3->( dbSeek( fwxFilial("SA3") + retCodUsr() ) ) )
		clVendedor := SA3->A3_COD
	endif
	
	restArea( alArea )
	
Return clVendedor

/*/{Protheus.doc} getLocVnd
Responsavel por retornar os armazens permitidos para o vendedor enviado por parametro
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return clLocais, Codigos dos armazens separados por /
@param clVendedor, characters, Codigo do vendedor
@type function
/*/
Static Function getLocVnd( clVendedor )
		
	local clLocais	:= ""
	local alArea	:= getArea()
	
	dbSelectArea( "ZCB" )
	ZCB->( dbSetOrder( 1 ) ) // ZCB_FILIAL, ZCB_VEND, ZCB_CANAL, R_E_C_N_O_, D_E_L_E_T_
	
	if ( ZCB->( dbSeek( fwxFilial("ZCB") + clVendedor ) ) )

		while ( ZCB->( .not. eof() ) .and. ZCB->ZCB_FILIAL == fwxFilial("ZCB") .and. ZCB->ZCB_VEND == SA3->A3_COD )
		
			if ( ZCB->ZCB_ATIVAD == "S" )
				clLocais += allTrim( ZCB->ZCB_CANAL ) + "/"
			endif
		
			ZCB->( dbSkip() )
		endDo
		
	endif
	
	restArea( alArea )
	
Return clLocais

/*/{Protheus.doc} vldPrdInC6
Responsavel por validar se o produto preenchido no item do pedido de venda � valido
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return llRet, Se .T., produto valido, se .F., produto invalido

@type function
/*/
Static Function vldPrdInC6()

	local alArea	:= getArea()
	local llRet		:= .F.
	local clSQL		:= ""
	local clAlias	:= ""
	local clFil		:= expFilSB1()
	local clProduto	:= &( readVar() )
	local clVendedor:= ""
	local clLocais	:= ""
	
	clSQL += " SELECT "
	clSQL += "		COUNT(*) AS QTD "
	clSQL += " FROM "
	clSQL += 		retSqlTab("SB1")
	clSQL += " WHERE "
	clSQL += " 		SB1.B1_FILIAL = '" + fwxFilial("SB1") + "' "
	clSQL += "		AND SB1.B1_COD = '" + clProduto + "' " 
	clSQL += "		AND SB1.D_E_L_E_T_ = ' ' " 
	
	if ( .not. empty( clFil ) )
		clSQL += "	AND " + clFil
	endif
	
	clAlias := getNextAlias()
	dbUseArea( .T., "TOPCONN", tcGenQry(,,clSQL), clAlias, .F., .T. )
			
	if ( .not. ( clAlias )->( eof() ) .and. ( clAlias )->QTD > 0 )
		llRet:= .T.	
	endif
	( clAlias )->( dbCloseArea() )
	
	if ( .not. llRet )
		clVendedor	:= getVendAtu()
		clLocais	:= getLocVnd( clVendedor )
		U_ADMVXFUN( 1,	{"C6_PRODUTO", "Produto inexistente nos armaz�ns permitidos para o vendedor [" + clLocais + "]" } )
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} vldLocInC6
Responsavel por validar se o armazem preenchido no item do pedido de venda � valido
@author DS2U (SDA)
@since 20/08/2018
@version 1.0
@return llRet, Se .T., valido, se .F., invalido

@type function
/*/
Static Function vldLocInC6()

	local alArea	:= getArea()
	local llRet		:= .F.
	local clLocal	:= &( readVar() )
	local clLocais	:= getLocVnd( clVendedor )
	
	if ( .not. ( allTrim( clLocal ) $ clLocais ) )
		U_ADMVXFUN( 1,	{"C6_LOCAL", "Armaz�m inexistente ou n�o permitido!" } )
	endif
	
	restArea( alArea )

Return llRet