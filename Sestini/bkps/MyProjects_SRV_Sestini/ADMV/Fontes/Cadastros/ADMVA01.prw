#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} ADMVA01
Rotina de cadastro de Cotas
@author DS2U (SDA)
@since 18/04/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA01( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			browse()
			
		case nlOpc == 1
			uRet := transCanal( uParam )
			
		case nlOpc == 2
			uRet := transCtImp( uParam )
			
		case nlOpc == 3
			uRet := showLogReg( uParam )
			
		case nlOpc == 4
			uRet := vldPerCota( uParam )
			
		case nlOpc == 5
			uRet := vldAlmox( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} transCanal
Responsavel por gerir a transferencia entre canais
@author DS2U (SDA)
@since 09/08/2018
@version 1.0

@type function
/*/
Static Function transCanal()

	local clFunc	:= "TRANSCANAL"
	local clTitle	:= "Transf�ncia entre Canais"
	local clDesc	:= "Este programa tem por objetivo realizar transfer�ncia entre Canais"
	local clPerg	:= "ADMVA01001"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execTransf( oSelf, MV_PAR01, MV_PAR02, MV_PAR03, MV_PAR04 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} transCtImp
Responsavel por realizar transferencia entre canais em lote
@author DS2U (SDA)
@since 10/08/2018
@version 1.0

@type function
/*/
Static Function transCtImp()

	local clFunc	:= "TRANSCTIMP"
	local clTitle	:= "Transf�ncia entre Canais em lote"
	local clDesc	:= "Este programa tem por objetivo realizar transfer�ncia entre Canais em lote, se baseando em arquivo CSV"
	local clPerg	:= "ADMVA01002"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execTrLote( oSelf, MV_PAR01) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} execTrLote
Responsavel por ler o arquivo CSV e realizar a transferencia item a item
@author DS2U (SDA)
@since 10/08/2018
@version 1.0
@param oSelf, object, Objeto de processamento
@param clFile, characters, Arquivo CSV a ser importado
@type function
/*/
Static Function execTrLote( oSelf, clFile )

	local nlHandle	:= FOPEN(clFile , FO_READWRITE + FO_SHARED )
	local nlTotLin	:= 0
	local nlCont	:= 0
	local clString	:= ""
	local clLinha	:= ""
	local nlQtdBytes:= 5000
	local nlBitLidos:= 0  
	local nlBits	:= 0
	local nlLinIni	:= 1
	local alDados	:= {}
	local llNoFound	:= .F.
	local llCanCota	:= .F.
	local nlTamPrd	:= tamSx3("B2_COD")[1]
	local nlTamCan	:= tamSx3("B2_LOCAL")[1]
	local nlQtd	:= 0
	local clProduto	:= ""
	local clCanalOri:= ""
	local clCanalDes:= ""
	local llFail	:= .F.
	
	if ( ferror() <> 0 )
		MsgAlert("Erro de abertura do arquivo: " + str( ferror() ) )
	else
	
		dbSelectArea( "ZC0" )
		ZC0->( dbSetOrder( 1 ) )
		
		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		nlTotLin := fSeek( nlHandle, 0, 2 )	// tamenho maximo de uma variavel  1048576		
		fSeek( nlHandle, 0, 0 )				// Posiciona no Inicio do Arquivo
		
		oSelf:setRegua1( 2 )
		oSelf:incRegua1( "Lendo arquivo" )
		
		while nlBitLidos <= nlTotLin
		
			nlCont++
		
			clString	:= FReadStr(nlHandle, nlQtdBytes)  
			nlBits		:= (At( CHR(13)+CHR(10),clString ))
			
			if ( nlBits == 0 )
				nlBits := Len( AllTrim( clString ) ) // Se o a ultima linha do arquivo nao conter os caractes chr(13) + chr(10) entao pego a quantidade de caracteres lidos
			endif
			
			if ( nlBits > 0 )
			
				nlBits += 1
				
				if ( nlCont == 1 )
					oSelf:setRegua2( int( nlTotLin / nlBits ) )
				endif				
				
				if ( nlCont > nlLinIni )
				
					fSeek( nlHandle, nlBitLidos, 0)				  
					clLinha	:= AllTrim( FReadStr(nlHandle, nlBits) )
					
					if ( .not. empty( clLinha ) )				
						
						alDados := separa( clLinha, ";", .T. )
						
						if ( len( alDados ) > 1 )
						
							clCanalOri	:= PADR( alDados[1], nlTamCan )
							clCanalDes	:= PADR( alDados[2], nlTamCan )
							clProduto	:= PADR( alDados[3], nlTamPrd )
							nlQtd := val( alDados[4] )
							
							if ( .not. SB1->( dbSeek( xFilial( "SB1" ) + clProduto ) ) )
							
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi encontrado o produto [" + clProduto + "]. Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
							elseif ( .not. ZC0->( dbSeek( xFilial( "ZC0" ) + clCanalOri ) ) )
							
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi encontrado o canal origem [" + clCanal + "]. Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
							elseif ( .not. ZC0->( dbSeek( xFilial( "ZC0" ) + clCanalDes ) ) )
							
								llFail := .T.
								oSelf:incRegua2("")
								oSelf:saveLog( "N�o foi encontrado o canal destino [" + clCanalDes + "]. Linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
								
							else
							
								execTransf( oSelf, clCanalOri, clCanalDes, clProduto, nlQtd )
							
							endif
							
						else
							
							llFail := .T.
							oSelf:incRegua2("")
							oSelf:saveLog( "N�o foi possivel ler corretamente a linha " + allTrim( Str( nlCont ) ) + " do arquivo CSV - Linha foi desconsiderada!" )
														
						endif
										
						alDados := {}
						
					endif
					
				endif
				
				nlBitLidos	+= nlBits
				FSeek( nlHandle, nlBitLidos, 0)
				
			else
				nlBitLidos	+= 1
			endif
		
		endDo
		
		oSelf:incRegua1( "Validando registros" )
		
		if ( .not. llFail )
			oSelf:saveLog( "Arquivo " + allTrim( clFile ) + " processado com sucesso!" )
		endif
		
	endif

Return

/*/{Protheus.doc} execTransf
Executa a transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param oSelf, object, Ojeto tNewProcess para manipula��o de controle de barra de progresso
@param clLocOri, characters, Codigo do ID origem do cadastro de cotas
@param clLocDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function execTransf( oSelf, clLocOri, clLocDest, clProduto, nlQuant )

	local alArea	:= getArea()
	local nlProc1	:= 2
	local nlProc2	:= 2
	local clAlmoxTrf:= ""
	local llProcOk	:= .F.
	local alItem	:= {}
	local alAuto	:= {}
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local clMsg		:= ""
	local llShow	:= .F.
	
	private cpErro	:= ""
	private npRecno	:= 0
	private lMsErroAuto	:= .F.

	default oSelf		:= nil
	default clLocOri	:= ""
	default clLocDest	:= ""
	default clProduto	:= ""
	default nlQuant		:= 0
	
	// Valida parametros
	if ( vldParam01(clLocOri, clLocDest, clProduto, nlQuant) )

		llShow	:= ( valType( oSelf ) == "O" )
		
		if ( llShow )
	
			// Define quantidade de passos a serem executados para validacao de quantidades
			oSelf:setRegua1( nlProc1 )
			
			// Define quantidade de passos a serem executados para transferencia
			oSelf:SetRegua2( nlProc2 )
			
		endif
		
		dbSelectArea ( "ZC0" )
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_LOCAL
				
		if ( ZC0->( dbSeek( xFilial("ZC0") + clLocOri ) ) )
		
			npRecno := ZC0->( recno() )
		
			//U_STNXLOG( 1 , { "ADMVA01", "ZC0", npRecno, "Iniciando processo de transfer�ncia de cotas" }, @cpErro )
		
			if ( llShow )
				
				oSelf:saveLog( "Validando saldo em estoque para transfer�ncia entre contas" )
				oSelf:incRegua1( "Validando saldo em estoque para transfer�ncia entre contas" )
			
				// Valida saldo em estoque para transferencia
				oSelf:incRegua1( "Validando saldo" )
				
			endif
			
			clAlmoxTrf	:= ZC0->ZC0_LOCAL
			llProcOk := vldEstoqTrf( clAlmoxTrf, clProduto, nlQuant )
			
			if ( llProcOk )
				
				if ( llShow )
					oSelf:saveLog( "Montando estrutura para rotina automatica de transfer�ncia entre canais" )
					oSelf:incRegua2( "Montando estrutura para rotina automatica de transfer�ncia entre canais" )
				endif
				
				alItem := U_ADMVP01( 2, { nlQuant, clProduto, clAlmoxTrf, clLocDest, /*clEndDe*/, /*clEndAte*/, "TRANSFERENCIA ENTRE CANAIS" } )
				
				if ( len( alItem ) > 0 )
				
					clDoc := getSxENum( "SD3", "D3_DOC", 1 )
					AADD( alAuto,{ clDoc, dDataBase } )
					AADD( alAuto, alItem )
					
					//U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Processando transfer�ncia entre canais" }, @cpErro )
					
					begin transaction
					
						if ( llShow )
							oSelf:incRegua2( "Executando rotina automatica de transfer�ncia entre canais" )
						endif
						
						MSExecAuto({|x,y| MATA261(x,y) }, alAuto, 3 )				
					
						if ( lMsErroAuto )
							
							conout("Falha no documento [" + clDoc + "] de transfer�ncia m�ltipla.")
							clArqErro  := NomeAutoLog()
							if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
								clMsgErro := memoRead( clArqErro )
								conout( clMsgErro )
								memoWrite(clArqErro," ") // Limpa log automatico
							endif
							
							rollbackSx8()
							disarmtransaction()
							
						else
							confirmSx8()
							conout("Incluido [" + clDoc + "] com sucesso!")			
						endif
						
					end transaction
					
					if ( .not. empty( clMsgErro ) )
						clMsg := "Falha no documento [" + clDoc + "] de transfer�ncia entre canais." + CRLF + CRLF + clMsgErro
						//U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, clMsg }, @cpErro )
						EECVIEW( clMsg, "LOG DE ERRO" )
					endif
						
				endif
				
			endif
			
			//U_STNXLOG( 3 , { "ADMVA01", "ZC0", npRecno, "Fim do processamento de transfer�ncia" }, @cpErro )
			
		else
			llProcOk := .F.
			clMsg := "ID [" + clLocOri + "] n�o encontrado!"
			U_ADMVXFUN( 1, {"ZC0_ID", clMsg } )
		endif
		
	else
		llProcOk := .F.
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} vldEstoqTrf
Responsavel por validar estoque referente a quantidade a ser transferida
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Existe saldo para transferencia / .F. = Nao existe saldo para transferencia 
@param clAlmoxTrf, characters, Codigo do almoxarifado
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldEstoqTrf( clAlmoxTrf, clProduto, nlQuant )
	
	local alArea	:= getArea()
	local llProcOk	:= .T.
	local clMsg		:= ""
	local nlQtDisp	:= 0
	
	//U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Validando saldo no endereco [" + clEndereco + "]" }, @cpErro )
	
	dbSelectArea( "SB2" )
	SB2->( dbSetOrder( 1 ) ) // B2_FILIAL, B2_COD, B2_LOCAL, R_E_C_N_O_, D_E_L_E_T_
	
	if ( SB2->( dbSeek( xFilial("SB2") + PADR( clProduto, tamSX3("B2_COD")[1] ) + PADR( clAlmoxTrf, tamSX3("B2_LOCAL")[1] ) ) ) )
	
		// Saldo disponivel no endereco
		nlQtDisp := saldoSB2()
	
		if ( nlQtDisp > 0 )
		
			if ( nlQtDisp < nlQuant )
				llProcOk	:= .F.
				clMsg		:= "Saldo do produto [" + clProduto + "] no canal [" + clAlmoxTrf + "], � insuficiente para a transfer�ncia. Saldo dispon�vel: " + allTrim( transform( nlQtDisp, pesqPict( "SB2", "B2_QATU" ) ) )
			endif
		
		else
			llProcOk	:= .F.
			clMsg		:= "N�o foi encontrado saldo do produto [" + clProduto + "] no canal [" + clAlmoxTrf + "] !"
		endif
		
	endif
		
	if ( .not. empty( clMsg ) )
		//U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, clMsg }, @cpErro )
		U_ADMVXFUN( 1, {"ZC0_ID", clMsg } )
	else
		//U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Estoque OK!" }, @cpErro )
	endif
	
	restArea( alArea )
	
Return llProcOk

/*/{Protheus.doc} vldParam01
Valida parametros de processamento de transferecia entre canais
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Parametros validados / .F. = parametros invalidos
@param clLocOri, characters, Codigo do ID origem do cadastro de cotas
@param clLocDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldParam01(clLocOri, clLocDest, clProduto, nlQuant)

	local llRet	:= .T.

	if ( empty( clLocOri ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_LOCAL", "Canal origem deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clLocDest ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_LOCAL", "Canal destino deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clProduto ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"B1_COD", "Produto deve ser preenchido" } )
	endif
	
	if ( llRet .and. nlQuant <= 0 )
		llRet := .F.
		U_ADMVXFUN( 1, {"QUANT", "Quantidade deve ser positiva e maior que zero!" } )
	endif
	
Return llRet

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	if ( cPerg == "ADMVA01001" )

		AADD(aPergunt,{cPerg,"01","Canal Origem        ?","MV_CH1","C",14,0,"G","","MV_PAR01","","","","","","ZC0",""})
		AADD(aPergunt,{cPerg,"02","Canal Destino       ?","MV_CH2","C",14,0,"G","","MV_PAR02","","","","","","ZC0",""})
		AADD(aPergunt,{cPerg,"03","Produto             ?","MV_CH3","C",15,0,"G","","MV_PAR03","","","","","","SB1",""})
		AADD(aPergunt,{cPerg,"04","Quantidade          ?","MV_CH4","N",tamSX3("BF_QUANT")[1],tamSX3("BF_QUANT")[2],"G","","MV_PAR04","","","","","","",""})
		
	elseif ( cPerg == "ADMVA01002" )
	
		AADD(aPergunt,{cPerg,"01","Selecione o arquivo  ","MV_CH1","C",99,0,"G","NaoVazio()","MV_PAR01","","","","","","ARQCSV",""})
	
	endif
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return 

/*/{Protheus.doc} vldAlmox
Responsavel por validar preenchimento do armazem
@author DS2U (SDA)
@since 23/07/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldAlmox()

	local alArea		:= getArea()
	local olModel		:= fwModelActive()
	local olModelZC0	:= olModel:getModel('ZC0MASTER')
	local nlOpc			:= olModel:getOperation()
	local clCodAlmox	:= fwFldGet('ZC0_LOCAL')
	local llRet			:= .T.

	dbSelectArea( "ZC0" )
	ZC0->( dbSetOrder( 1 ) )

	if ( ZC0->( dbSeek( xFilial("ZC0") + clCodAlmox ) ) .and. nlOpc == 3 )
	
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZC0_LOCAL", "Armazem " + clCodAlmox + " j� utilizado para o canal " + allTrim( ZC0->ZC0_DESCAN ) } )
	
	else
		
		dbSelectArea( "NNR" )
		NNR->( dbSetOrder( 1 ) )
	
		if ( .not. NNR->( dbSeek( xFilial("NNR") + clCodAlmox ) ) )
		
			llRet := .F.
			U_ADMVXFUN( 1,	{"ZC0_LOCAL", "Armazem " + clCodAlmox + " n�o encontrado!" } )
			
		endif
		
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} vldPerCota
Funcao para validacao de preenchimento do percental da cota
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCota()

	local llRet		:= .F.
	local nlPerCot	:= fwFldGet('ZC0_PERCOT')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, nlPerCot ) )
		
		if ( vPercNivel( nlPerCot, @nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC0_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vPercNivel
Funcao para validacao de percentual do nivel do cadastro atual
@author sergi
@since 23/07/2018
@version 1.0
@return llRet, Retorna se o percentual corrente digitado pode ser utilizado no nivel atual do cadastro que esta sendo validado
@param nlPercAtu, numeric, percentual digitado a ser validado
@param [nlSaldoPer], numeric, percentual digitado a ser validado
@type function
/*/
Static Function vPercNivel( nlPercAtu, nlSaldoPer )

	local clAlias	:= getNextAlias()
	local llRet		:= .F.
	local nlSaldoPer:= 0
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			COALESCE( SUM( ZC0_PERCOT ), 0 ) AS PERCOT
			
		FROM
			%TABLE:ZC0% ZC0
			
		WHERE
			ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0_LINHA = '1' // VALIDA COTA SOMENTE NA PRIMEIRA LINHA
			AND ZC0.D_E_L_E_T_ = ' '
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
	
		nlSaldoPer := ( 100 - ( clAlias )->PERCOT )
		llRet := ( nlPercAtu <= nlSaldoPer )
	
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= ZC0->( getArea() )

	private oBrowse

	dbSelectArea("ZC0")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC0")
	oBrowse:SetDescription("Cadastro de Cotas x Canais")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA01"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA01"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 5 ACCESS 0 // "Excluir" 
	ADD OPTION alRotina TITLE "Transfer�ncia"			ACTION "U_ADMVA01(1)"		OPERATION 9 ACCESS 0 // "Copiar"
	ADD OPTION alRotina TITLE "Transfer�ncia em massa"	ACTION "U_ADMVA01(2)"		OPERATION 9 ACCESS 0 // "Transferencia"
	ADD OPTION alRotina TITLE "Log"						ACTION "U_ADMVA01(3)"		OPERATION 2 ACCESS 0 // "Log" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC0	:= FWFormStruct( 2, 'ZC0' )
	local olModel	:= FWLoadModel( 'ADMVA01' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC0', olStruZC0, 'ZC0MASTER' )
	
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC0', 'SUPERIOR' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC0	:= FWFormStruct( 1, 'ZC0', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA01M', /* bPreValidacao*/,/*bPosValidacao*/ { |olModel| admvTudoOk( olModel ) },/*bCommit { |olModel| admvCommit( olModel ) }*/, { |olModel| admvCancel( olModel ) }/*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC0MASTER', /*cOwner*/, olStruZC0, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC0_FILIAL", "ZC0_LOCAL"} )
	
	olModel:SetDescription('Cotas x Canais')
	olModel:GetModel('ZC0MASTER' ):SetDescription('Cotas x Canais')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

Static Function admvCancel( olModel )

	local olModelZC0:= olModel:getModel('ZC0MASTER')
	local nlOpc		:= olModel:getOperation()
	
	// Se for inclusao, da rollBack no sequencial do ID
	if ( nlOpc == 3 )
		rollBackSX8()
	endif
	
Return .T.

/*/{Protheus.doc} admvTudoOk
Responsavel por realizar validacoes e processamentos no commit do MVC
@author sergi
@since 07/05/2018
@version 1.0
@type function
/*/
Static Function admvTudoOk( olModel )

	local alArea	:= getArea()
	local llRet		:= .T.
//	local clDesc	:= ""
//	local alEndCot	:= {}
//	local olModelZC0:= olModel:getModel('ZC0MASTER')
//	local nlOpc		:= olModel:getOperation()
//	local clDesc	:= ""
//	local clAlmox	:= ""
//	local llProcOk	:= .T.
//	
//	// Se for inclusao
//	if ( nlOpc == 3 )
//	
//		dbSelectArea( "SBE" )
//		SBE->( dbSetOrder( 1 ) ) // BE_FILIAL, BE_LOCAL, BE_LOCALIZ, BE_ESTFIS
//		
//		clAlmox		:= allTrim( olModelZC0:getValue( "ZC0_ALMOX" ) )
//		clEndereco	:= U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[1] // Gera ID do endereco de % Cota
//		
//		if ( SBE->( dbSeek( xFilial( "SBE" ) + clAlmox + clEndereco ) ) )
//		
//			llRet := .F.
//			U_ADMVXFUN( 1,	{"ZC0_ALMOX",	"N�o � possivel criar endere�o amarrado a este cadastro, pois j� existe o endere�o " +; 
//											allTrim( olModelZC0:getValue( "ZC0_ID" ) ) + " para o armaz�m " +;
//											 olModelZC0:getValue( "ZC0_ALMOX" ) } )
//		
//		else
//			
//			clDesc		:= allTrim( olModelZC0:getValue( "ZC0_DESC" ) )
//			clAlmox		:= allTrim( olModelZC0:getValue( "ZC0_ALMOX" ) )
//			clEndereco	:= U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[1] // Gera ID do endereco de % Cota
//			
//			// Tratamento para nao estourar o tamanho do campo de descricao
//			if ( len( clDesc ) > tamSX3( "BE_DESCRIC" )[1] )
//				clDesc := subs( clDesc, 1, tamSX3( "BE_DESCRIC" )[1] )
//			endif
//			
//			AADD( alEndCot, { clAlmox, clEndereco, clDesc } )
//			
//			// Endereco vinculado ao ID para % Retido
//			clEndereco := U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[2] // Gera ID do endereco de % Retido
//			
//			AADD( alEndCot, { clAlmox, clEndereco, clDesc + " RETIDO" } )
//			
//			begin transaction
//
//				for nlx := 1 to len( alEndCot )
//	
//					// Checa se o armazem + endereco existe, e caso nao exista cria o endereco no ERP
//					if ( .not. U_ADMVP01( 4, { alEndCot[nlx][1], alEndCot[nlx][2], alEndCot[nlx][3] } ) )
//						llProcOk := .F.
//						disarmTransaction()
//						exit
//					endif
//	
//				next nlx
//				
//			end transaction
//			
//			if ( .not. llProcOk )
//				// A funcao getSXeNum esta sendo chamada do inicializador padrao do campo ZC0_ID, QUANDO FOR INCLUSAO
//				confirmSX8()
//			endif
//		
//		endif
//		
//	// Se for exclusao
//	elseif ( nlOpc == 5 )
//	
//	endif

	llRet := chkCanDel( olModelZC0:getValue( "ZC0_LOCAL" ) )
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} chkCanDel
Responsavel por checar se o cadastro corrente pode ser excluido
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. Se pode ser excluido ou .F. se nao pode ser excluido
@param clAlmox, characters, Codigo do armazem a ser consultado
@type function
/*/
Static Function chkCanDel( clAlmox )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clAlias	:= getNextAlias()
	
	default clAlmox	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT * FROM
		(
	
			SELECT
			 	'SC7' AS ORIGEM
				, COALESCE( COUNT( SC7.R_E_C_N_O_ ), 0 ) AS QTRECNOS
				, C7_LOCAL AS LOCPRD
			
			FROM
				%TABLE:SC7% SC7
				
			WHERE 
				SC7.C7_FILIAL = %XFILIAL:SC7%
				AND SC7.%NOTDEL%
				
			GROUP BY C7_LOCAL
			
			UNION
			
			SELECT 
				'SC6' AS ORIGEM
				, COUNT( SC6.R_E_C_N_O_ ) QTRECNOS
				, C6_LOCAL AS LOCPRD
				
			FROM
				%TABLE:SC6% SC6
				
			WHERE 
				SC6.C6_FILIAL = %XFILIAL:SC7%
				AND SC6.%NOTDEL%
				
			GROUP BY C6_LOCAL
			
			UNION

			SELECT
			 	'SB2' AS ORIGEM
				, COALESCE( COUNT( SB2.R_E_C_N_O_ ), 0 ) AS QTRECNOS
				, B2_LOCAL AS LOCPRD
			
			FROM
				%TABLE:SB2% SB2
				
			WHERE 
				SB2.B2_FILIAL = %XFILIAL:SB2%
				AND SB2.%NOTDEL%
				
			GROUP BY B2_LOCAL
			
		) AS TMP
	
		WHERE
			TMP.LOCPRD IN (
	
				SELECT
					ZC0_LOCAL
					
				FROM
					%TABLE:ZC0% ZC0
					
				WHERE
					ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
					AND ZC0.ZC0_LOCAL = %EXP:clAlmox%
					AND ZC0.%NOTDEL%
					
			)
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZC0_LOCAL", "Este cadastro tem saldo em pedidos de compra ou de vendas ou em estoque. N�o pode ser exclu�do!" } )
	endif
	( clAlias )->( dbCloseArea() )
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} showLogReg
Responsavel por chamar a interface de controle de log do registro posicionado na ZC0
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@type function
/*/
Static Function showLogReg()

	local alArea	:= getArea()
	local clErro	:= ""

	// Abre monitor de log especifico da Sestini conforme o recno e entidade posicionado
	//U_STNXLOG( 9, { "ZC0" , ZC0->( recno() )  } , @clErro )

	restArea( alArea )

Return