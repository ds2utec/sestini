#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} ADMVXFUN
Bilioteca de funcoes referente ao projeto de ADM de Vendas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return uRet, retorno da funcao executada
@param nlOpc, numeric, Parametro para identificar a funcao a ser executada
@param uParam, undefined, Parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVXFUN( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 1
			uRet := helpAdmv( uParam )
		
		case nlOpc == 2
			uRet := vldIdDup( uParam )
			
		case nlOpc == 3
			uRet := vldPerc( uParam )
			
		case nlOpc == 4
			uRet := getCanais( uParam )
			
		case nlOpc == 5
			uRet := getCtByIt( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} getCanais
Reponsavel por retornar os canais do ADMV, que sao controlados por cotas
@author DS2U (SDA)
@since 01/08/2018
@version 1.0
@return alCanais,	alCanais[1][1] = Codigo do Armazem em que o canal esta vinculado
					alCanais[1][2] = Descricao do Armazem em que o canal esta vinculado
					alCanais[1][3] = Cota do Armazem em que o canal esta vinculado
					alCanais[1][4] = Linha de Produtos do Armazem em que o canal esta vinculado
									 1=1� Linha;2=2� Linha;3=2� Linha Grave;4=2� Linha Medio;5=2� Linha Leve
@param llOnlyPlin, boolean, Identifica se a query ira retornar somente canais ADMV de primeira linha
@type function
/*/
Static Function getCanais( llOnlyPlin )

	local alCanais	:= {}
	local clAlias	:= getNextAlias()
	local clOnlyPlin:= "% 1 = 1 %"
	
	default llOnlyPlin	:= .F.
	
	if ( llOnlyPlin )
		clOnlyPlin := "% ZC0_LINHA = '1' %"
	endif
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZC0_LOCAL AS ARMAZEM
			, ZC0_DESCAN AS DESCRICAO
			, ZC0_PERCOT AS COTA
			, ZC0_LINHA AS LINHA
			
		FROM
			%TABLE:ZC0% ZC0
			
		WHERE
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND %EXP:clOnlyPlin%
			AND ZC0.%NOTDEL%
	
	ENDSQL
	
	while ( .not. ( clAlias )->( eof() ) )
		
		AADD( alCanais, { ( clAlias )->ARMAZEM, ( clAlias )->DESCRICAO, ( clAlias )->COTA, ( clAlias )->LINHA } )
		
		( clAlias )->( dbSkip() )
	endDo
	( clAlias )->( dbCloseArea() )

Return alCanais

/*/{Protheus.doc} getCtByIt
Retorna a soma de percentual de cota por produto. Utilizado para validar o nivel, nao deixando ultrapassar 100%
@author DS2U (SDA)
@since 06/08/2018
@version 1.0
@return nlCota, Soma das cotas por produto
@param clProduto, characters, Codigo do produto a ser analisado
@type function
/*/
Static Function getCtByIt( clProduto )

	local nlCota	:= 0
	local clAlias	:= getNextAlias()
	
	default clProduto	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			SUM( ZC8_COTA ) AS TOTALCOTA
			
		FROM
			%TABLE:ZC8% ZC8
			
		WHERE
			ZC8.ZC8_FILIAL = %XFILIAL:ZC8%
			AND ZC8.ZC8_PROD = %EXP:clProduto%
			AND ZC8.%NOTDEL%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		nlCota :=  ( clAlias )->TOTALCOTA
	endif
	( clAlias )->( dbCloseArea() )

Return nlCota

/*/{Protheus.doc} existZC0
Responsavel por checar se ID existe no cadastro de cotas
@author DS2U (SDA)
@since 11/06/2018
@version 1.0
@return llRet, .T. = Existe / .F. = N�o existe
@param clIdCota, characters, ID da cota��o a ser checada
@type function
/*/
Static Function existZC0( clIdCota )

	local alArea	:= getArea()
	local llRet		:= .T.
	
	dbSelectArea( "ZC0" )
	ZC0->( dbSetOrder( 1 ) )
	
	llRet := ZC0->( dbSeek( xFilial( "ZC0" ) + PADR( clIdCota, tamsx3("ZC0_ID")[1] ) ) )
	
	if ( .not. llRet )
		helpAdmv( {"ZC0_ID", "Registro n�o encontrado no cadastro de cotas. Informe um registro v�lido!"} )
	endif 
	
	restArea( alArea )

Return llRet

///*/{Protheus.doc} getChvSBE
//Responsavel por definir regras de filtro para a consulta padrao de cotas ZC0
//@author DS2U (SDA)
//@since 07/05/2018
//@version 1.0
//@return llRet, Retorno booleano conforme regra aplicada ao filtro da tabela ZC0
//
//@type function
///*/
//Static Function getChvSBE( clIdAloc )
//
//	local clChave	:= XFILIAL("SBE")+SUBS(clIdAloc,1,2)+clIdAloc
//
//Return clChave

/*/{Protheus.doc} helpAdmv
Funcao customizada para apresenta��o de alerta
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@param clMsg, characters, Mensagem do usuario a ser apresentada
@type function
/*/
Static Function helpAdmv( alParam )

	local clCampo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clMsg		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local llConout	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .T. )

	default alParam	:= {}

	if ( .not. empty( clCampo ) .and. .not. empty( clMsg ) )

		if ( llConout )
			conout( clCampo )
			conout( clMsg )
		endif
		
		fwClearHLP()
		help('', 1, getSX3Cache(clCampo, "X3_TITULO"), clCampo, clMsg, 1, 0)
		
	endif
	   
Return

/*/{Protheus.doc} getAlmxPai
Funcao para buscar o codigo do almoxafirado do codigo pai enviado por parametro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return clAlmoxPai, Codigo do almoxarifado conforme o codigo pai

@type function
/*/
Static Function getAlmxPai( clCodPai )

	local alArea	:= getArea()
	local clAlmoxPai:= ""
	
	default clCodPai	:= ""
	
	if ( .not. empty( clCodPai ) )
	
		dbSelectArea("ZC0")
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( ZC0->( dbSeek( xFilial("ZC0") + clCodPai ) ) )
			clAlmoxPai := ZC0->ZC0_ALMOX
		else
			helpAdmv( {"ZC0_ALMOX", "C�digo Pai n�o existe!"} )
		endif
		
	endif
	
	restArea( alArea )
		
Return clAlmoxPai

/*/{Protheus.doc} vldPerc
Funcao para validacao de preenchimento de percentual entre 0,01 e 100 %
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retorna verdadeiro ou falso identificando se o preenchimento do percentual est� v�lido
@param nlPerc, numeric, percentual a ser avaliado
@type function
/*/
Static Function vldPerc( nlPerc )

	local llRet	:= .T.

	default nlPerc := 0
	
	if ( nlPerc <= 0 .or. nlPerc > 100 )
		
		llRet := .F.
		helpAdmv( {"ZC0_PERCOT", "O percentual deve ser preenchido entre 0.01% e 100% !"} )
		
	endif
	
Return llRet