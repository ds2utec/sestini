#Include 'TOTVS.ch'
/*/{Protheus.doc} M410STTS
Ponto de entrada para tratar comissao
@author Rodolfo
@since 21/08/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
User Function M410STTS()
Local cChave	:= ''
Local cPedido 	:= SC5->C5_NUM
Local lContinua	:= .F.
Local cAliasSZ4	:= GetNextAlias()
	
DbSelectarea('SA1')
SA1->(DbSetorder(1))
SA1->(DBseek(xfilial('SA1') + SC5->(C5_CLIENTE + C5_LOJACLI)))

dbselectarea('SC6')
SC6->(dbsetorder(1))
If SC6->(dbseek(xFilial('SC6') + cPedido)) 
	while xFilial('SC6') + cPedido == SC6->(C6_FILIAL + C6_NUM)
	
		If Select(cAliasSZ4) > 0
			(cAliasSZ4)->(DbCloseArea())
		Endif	
		
		BeginSql Alias cAliasSZ4
			
			SELECT	Z4_COMIS
			FROM %Table:SZ4% SZ4
			WHERE
			SZ4.Z4_FILIAL = %xFilial:SZ4% AND
			(SZ4.Z4_REGIAO = %Exp:SA1->A1_XREGIAO% OR SZ4.Z4_REGIAO = ' ') AND
			(SZ4.Z4_SEDE = %Exp:SA1->A1_REGIAO% OR SZ4.Z4_SEDE = ' ') AND
			(SZ4.Z4_GRPVEN = %Exp:SA1->A1_GRPVEN% OR SZ4.Z4_GRPVEN = ' ') AND
			(SZ4.Z4_CLIENTE = %Exp:SA1->A1_COD% OR SZ4.Z4_CLIENTE = ' ') AND
			(SZ4.Z4_LOJA = %Exp:SA1->A1_LOJA% OR SZ4.Z4_LOJA = ' ') AND
			(SZ4.Z4_PRODUTO = %Exp:SC6->C6_PRODUTO% OR SZ4.Z4_PRODUTO = ' ') AND
			%Exp:DTOS(ddatabase)% BETWEEN SZ4.Z4_DTINI AND SZ4.Z4_DTFIM AND
			SZ4.%notdel%
		EndSql
		
		If (cAliasSZ4)->(!EOF())
			Reclock('SC6',.F.)
			SC6->C6_COMIS1 	:= (cAliasSZ4)->Z4_COMIS
			SC6->C6_XCOMIS1 := (cAliasSZ4)->Z4_COMIS
			SC6->C6_COMIS2 	:= (cAliasSZ4)->Z4_COMIS
			SC6->C6_XCOMIS2 := (cAliasSZ4)->Z4_COMIS			
			MsUnlock()
			lContinua := .F.
		Else
			lContinua := .T.
		EndIf
		
		(cAliasSZ4)->(DbCloseArea())
		
		DbSelectArea('SZ4')
		SZ4->(DbSetOrder(3))// Z4_VEND + Z4_REGIAO + Z4_SEDE + Z4_GRPVEN + Z4_CLIENTE + Z4_LOJA + Z4_PRODUTO                                                                                  
		If SZ4->(DbSeek(xFilial('SZ4') + SA1->A1_VEND)) .And. lContinua
			SetComis(SA1->A1_VEND,'1')
		EndIf
		If SZ4->(DbSeek(xFilial('SZ4') + SA1->A1_XVEND2)) .And. lContinua
			SetComis(SA1->A1_XVEND2,'2')
		EndIf		
		
		SC6->(DbSkip())
	Enddo
EndIf

Return 

/*/{Protheus.doc} SetComis
(long_description)
@author mynam
@since 23/08/2018
@version 1.0
@param cVend, character, (Descri��o do par�metro)
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SetComis(cVend,cNumVend)
Local cChave := xFilial('SZ4') + cVend
Local cComis := 'C6_COMIS' + cNumVend 
Local cXComis := 'C6_XCOMIS' + cNumVend

If SZ4->(DbSeek(cChave + SC6->C6_PRODUTO))
	cChave +=  SC6->C6_PRODUTO
Else
	cChave +=  Space(Tamsx3('C6_PRODUTO')[1])
EndIf		

If SZ4->(DbSeek(cChave + SA1->(A1_COD+A1_LOJA)))
	cChave += SA1->(A1_COD+A1_LOJA)
Else
	cChave += Space(Tamsx3('A1_COD')[1]) + Space(Tamsx3('A1_LOJA')[1])
EndIf	
		
If SZ4->(DbSeek(cChave + SA1->A1_GRPVEN))
	cChave += SA1->A1_GRPVEN
Else
	cChave += Space(Tamsx3('A1_GRPVEN')[1])
EndIf	

If SZ4->(DbSeek(cChave + SA1->A1_REGIAO))
	cChave += SA1->A1_REGIAO
Else
	cChave += Space(Tamsx3('A1_REGIAO')[1])
EndIf							
								
If SZ4->(DbSeek(cChave + SA1->A1_XREGIAO))
	cChave += SA1->A1_XREGIAO
Else
	cChave += Space(Tamsx3('A1_XREGIAO')[1])
EndIf
		
If SZ4->(DbSeek(cChave))
	Reclock('SC6',.F.)
	SC6->&cComis  := SZ4->Z4_COMIS
	SC6->&cXComis := SZ4->Z4_COMIS
	MsUnlock()			
EndIf	

Return


User Function AX_SZ4()
AXCADASTRO('SZ4','Teste')
Return