#Include 'TOTVS.ch'
/*/{Protheus.doc} FA440VLD
Tratamento para bloquear comiss�o na devolu��o
@author Rodolfo
@since 11/09/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
User Function FA440VLD()
lRet := .T.
If Isincallstack('MATA103')
	If SD1->D1_XDEVGAR == 'S' 
		lRet := .F.
	EndIf
EndIf
Return lRet