#Include 'TOTVS.ch'
/*/{Protheus.doc} F110SE5
atualiza o campo E1_XINADIN na baixa
@author Rodolfo
@since 23/08/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
User Function SACI008()

If SE1->E1_XINADIN == 'I'
	RecLock('SE1',.F.)
	SE1->E1_XINADIN := 'A'
	MsUnlock()
EndIf
	
Return 