#Include "PROTHEUS.CH"
//--------------------------------------------------------------
/*/{Protheus.doc} ADMOFILT
Description                                                     
                                                                
@param xParam Parameter Description                             
@return xRet Return Description                                 
@author  -                                               
@since 16/08/2018                                                   
/*/                                                             
//--------------------------------------------------------------
User Function ADMOFILT()                        
Local oButton1
Local oButton2
Local oComboBo1
Local nComboBo1 := 1
Local oGet1
Local cGet1 := "Define variable value"
Local oGet10
Local cGet10 := "Define variable value"
Local oGet11
Local cGet11 := "Define variable value"
Local oGet12
Local cGet12 := "Define variable value"
Local oGet13
Local cGet13 := "Define variable value"
Local oGet14
Local cGet14 := "Define variable value"
Local oGet15
Local cGet15 := "Define variable value"
Local oGet16
Local cGet16 := "Define variable value"
Local oGet17
Local cGet17 := "Define variable value"
Local oGet18
Local cGet18 := "Define variable value"
Local oGet19
Local cGet19 := "Define variable value"
Local oGet2
Local cGet2 := "Define variable value"
Local oGet20
Local cGet20 := "Define variable value"
Local oGet21
Local cGet21 := "Define variable value"
Local oGet22
Local cGet22 := "Define variable value"
Local oGet23
Local cGet23 := "Define variable value"
Local oGet24
Local cGet24 := "Define variable value"
Local oGet3
Local cGet3 := "Define variable value"
Local oGet4
Local cGet4 := "Define variable value"
Local oGet5
Local cGet5 := "Define variable value"
Local oGet6
Local cGet6 := "Define variable value"
Local oGet7
Local cGet7 := "Define variable value"
Local oGet8
Local cGet8 := "Define variable value"
Local oGet9
Local cGet9 := "Define variable value"
Local oRadMenu1
Local nRadMenu1 := 1
Local oRadMenu2
Local nRadMenu2 := 1
Local oSay1
Local oSay10
Local oSay11
Local oSay12
Local oSay13
Local oSay14
Local oSay15
Local oSay16
Local oSay17
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oSay6
Local oSay7
Local oSay8
Local oSay9
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Filtro" FROM -050, 000  TO 610, 450 COLORS 0, 16777215 PIXEL

    @ 017, 032 SAY oSay1 PROMPT "Estabelecimento:" SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 032, 041 SAY oSay2 PROMPT "Canal Venda:" SIZE 036, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 047, 055 SAY oSay3 PROMPT "Cliente:" SIZE 019, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 152, 023 SAY oSay4 PROMPT "Data Lib. Separa��o:" SIZE 053, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 062, 030 SAY oSay5 PROMPT "Pedido do Cliente:" SIZE 046, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 077, 038 SAY oSay6 PROMPT "Transportador:" SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 092, 021 SAY oSay7 PROMPT "Transp. Redespacho:" SIZE 055, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 107, 037 SAY oSay8 PROMPT "Pedido Interno:" SIZE 040, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 122, 032 SAY oSay9 PROMPT "Data de Entrega:" SIZE 042, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 137, 040 SAY oSay10 PROMPT "Data da Lista:" SIZE 035, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 167, 064 SAY oSay11 PROMPT "UF:" SIZE 011, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 182, 061 SAY oSay12 PROMPT "CEP:" SIZE 018, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 197, 054 SAY oSay13 PROMPT "Regi�o:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 220, 052 SAY oSay14 PROMPT "Estoque:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 254, 059 SAY oSay15 PROMPT "Frete:" SIZE 017, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 005, 165 SAY oSay17 PROMPT "Final" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 015, 076 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 015, 144 MSGET oGet2 VAR cGet2 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 030, 076 MSGET oGet3 VAR cGet3 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 030, 144 MSGET oGet4 VAR cGet4 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 045, 076 MSGET oGet5 VAR cGet5 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 045, 144 MSGET oGet6 VAR cGet6 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 060, 076 MSGET oGet7 VAR cGet7 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 060, 144 MSGET oGet8 VAR cGet8 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 075, 076 MSGET oGet9 VAR cGet9 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 075, 144 MSGET oGet10 VAR cGet10 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 090, 076 MSGET oGet11 VAR cGet11 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 090, 144 MSGET oGet12 VAR cGet12 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 105, 076 MSGET oGet13 VAR cGet13 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 105, 144 MSGET oGet14 VAR cGet14 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 005, 094 SAY oSay16 PROMPT "Inicial" SIZE 030, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 120, 076 MSGET oGet15 VAR cGet15 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 120, 144 MSGET oGet16 VAR cGet16 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 135, 076 MSGET oGet17 VAR cGet17 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 135, 144 MSGET oGet18 VAR cGet18 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 150, 076 MSGET oGet19 VAR cGet19 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 150, 144 MSGET oGet20 VAR cGet20 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 165, 076 MSGET oGet21 VAR cGet21 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 165, 144 MSGET oGet22 VAR cGet22 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 180, 076 MSGET oGet23 VAR cGet23 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 180, 144 MSGET oGet24 VAR cGet24 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 195, 076 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"Todas"} SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 210, 076 RADIO oRadMenu1 VAR nRadMenu1 ITEMS "Sim","N�o","Ambos" SIZE 064, 027 OF oDlg COLOR 0, 16777215 PIXEL
    @ 245, 076 RADIO oRadMenu2 VAR nRadMenu2 ITEMS "CIF","FOB","Ambos" SIZE 064, 026 OF oDlg COLOR 0, 16777215 PIXEL
    @ 277, 024 BUTTON oButton1 PROMPT "OK" SIZE 083, 021 OF oDlg PIXEL
    @ 277, 117 BUTTON oButton2 PROMPT "Cancelar" SIZE 083, 021 OF oDlg ACTION  Close(oDlg) PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return