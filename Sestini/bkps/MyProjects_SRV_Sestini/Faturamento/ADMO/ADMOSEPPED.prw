#Include "PROTHEUS.CH"
#INCLUDE "FWBROWSE.CH"

/*
������������������������������������������������������������������������������
��� Programa  ADMOSEPPED     Autor  Rogerio Machado      Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/


User Function ADMOSEPPED()                        
Local oButton1
Local oButton10
Local oButton11
Local oButton12
Local oButton13
Local oButton14
Local oButton15
Local oButton2
Local oButton3
Local oButton4
Local oButton5
Local oButton6
Local oButton7
Local oButton8
Local oButton9
Local oComboBo1
Local nComboBo1       := 1
Local oComboBo2
Local nComboBo2       := 1
Local oComboBo3
Local nComboBo3       := 1
Local oComboBo4
Local nComboBo4       := 1
Local oGet1
Local cGet1           := 0
Local oGroup1
Local oGroup2
Local oGroup3
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oSay6
Local oSay7
Local oSay8
Local aTransp        := U_RetTransp()
Local aSepara        := U_RetSepara()
Local cAliasADMO     := GetNextAlias()

Local oDlg           := Nil
Local oBrowse        := Nil
Local oOk            := LoadBitmap(GetResources(), "LBOK"        )
Local oNo            := LoadBitmap(GetResources(), "LBNO"        )
Local oCinza         := LoadBitmap(GetResources(), "BR_CINZA"    )
Local oVerde         := LoadBitmap(GetResources(), "BR_VERDE"    )
Local oVermelho      := LoadBitmap(GetResources(), "BR_VERMELHO" )
Local oAmarelo       := LoadBitmap(GetResources(), "BR_AMARELO"  )
Local oAzul          := LoadBitmap(GetResources(), "BR_AZUL"     )
Local oPreto         := LoadBitmap(GetResources(), "BR_PRETO"    )
Local oLaranja       := LoadBitmap(GetResources(), "BR_LARANJA"  )
Local oVioleta       := LoadBitmap(GetResources(), "BR_VIOLETA"  )
Local nCountA        := 0

Local aHeader        := {}
Local aCols          := {}
Local lRet           := .F.
Local cAliasADMO     := GetNextAlias()
Local cLegend        := ""
Local nComboBo1      := "6-TODOS"
Local nComboBo2      := "TODOS"



//--------------------------------------------------------------
//   MONTAGEM DA TELA                                                            
//--------------------------------------------------------------
DEFINE MSDIALOG oDlg TITLE "Separa��o de Pedidos" FROM 000, 000  TO 500, 1044 COLORS 0, 16777215 PIXEL

@ 001, 003 GROUP oGroup1 TO 029, 523 OF oDlg COLOR 0, 16777215 PIXEL
@ 011, 005 BUTTON oButton1 PROMPT "FILTRO" SIZE 025, 012 OF oDlg ACTION U_ADMOFILT() PIXEL
@ 011, 032 BUTTON oButton2 PROMPT "TOT. PEDIDOS" SIZE 039, 012 OF oDlg PIXEL
@ 011, 073 BUTTON oButton4 PROMPT "IMPRESS�O" SIZE 034, 012 OF oDlg PIXEL
@ 011, 109 BUTTON oButton5 PROMPT "CRIT�RIOS" SIZE 030, 012 OF oDlg ACTION U_ADMOSELPED() PIXEL
@ 011, 141 BUTTON oButton6 PROMPT "LIBERAR PEDIDOS" SIZE 049, 012 OF oDlg PIXEL
@ 011, 192 BUTTON oButton7 PROMPT "DIVIS�O ROMANEIOS" SIZE 058, 012 OF oDlg PIXEL
@ 011, 252 BUTTON oButton8 PROMPT "BARCOD" SIZE 030, 012 OF oDlg PIXEL
@ 011, 310 BUTTON oButton3 PROMPT "LEGENDA" SIZE 036, 012 OF oDlg ACTION ADMOLEG() PIXEL
@ 014, 369 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"1-DEFININDO PRIORIDADE","2-PENDENTE DE SEPARA��O","3-EM SEPARA��O","4-SEPARADOS","5-MEMBRADOS","6-TODOS"} SIZE 075, 010 OF oDlg COLORS 0, 16777215 PIXEL
@ 014, 448 MSCOMBOBOX oComboBo2 VAR nComboBo2 ITEMS aSepara SIZE 069, 010 OF oDlg COLORS 0, 16777215 PIXEL
@ 005, 369 SAY oSay1 PROMPT "Status do Pedido" SIZE 045, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 000, 065 SAY oSay2 PROMPT "Status do Pedido" SIZE 045, 007 OF oSay1 COLORS 0, 16777215 PIXEL
@ 005, 448 SAY oSay3 PROMPT "Exibir Separador" SIZE 045, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 029, 003 GROUP oGroup2 TO 197, 522 OF oDlg COLOR 0, 16777215  PIXEL     
    
    
//--------------------------------------------------------------
// MONTA CABECALHO DO GRID:
//--------------------------------------------------------------
aHeader := {'', '', 'Status', 'Kit', 'Canal Venda', 'Pedido', 'Cliente', 'Compras', 'Transportadora', 'Redespacho',;
            'CEP', 'Cidade', 'UF', 'Separador', 'Frete', 'Credito', 'Dt Entrega', 'Prioridade', 'Total Itens', 'Total de Pe�as', 'Total Pe�as Separadas', 'Perc. Pe�as Separadas',;
            'Total de Pe�as Membradas', 'Perc. Pe�as Membradas', 'Valor Total da Entrega', 'Dt Lista', 'Hora Lista', 'Dt Lib Separa��o', 'Antecipacao Restrita', 'Agendamento Previo',;
            'Especial', 'Observa��o' }



//--------------------------------------------------------------
// MONTA MASSA DE DADOS
//--------------------------------------------------------------
BeginSQL Alias cAliasADMO
	SELECT DISTINCT
		CASE 
			WHEN SC5.C5_XSTATUS = '1' THEN 'Definindo Prioridade' 
			WHEN SC5.C5_XSTATUS = '2' THEN 'Pendente de Separa��o' 
			WHEN SC5.C5_XSTATUS = '3' THEN 'Em Separa��o' 
			WHEN SC5.C5_XSTATUS = '4' THEN 'Separados' 
			WHEN SC5.C5_XSTATUS = '5' THEN 'Membrados' 
			WHEN SC5.C5_XSTATUS = '6' THEN 'Todos'
			ELSE '' END AS Status,
		SC5.C5_XKITADM AS Kit,
		CASE WHEN SC5.C5_XCANAL = 'A' THEN 'ATACADO' WHEN SC5.C5_XCANAL = 'E' THEN 'E-COMMERCE' WHEN SC5.C5_XCANAL = 'V' THEN 'VAREJO' END AS [Canal_Venda],
		SC5.C5_NUM AS Pedido,
		RTRIM(SA1.A1_NOME) AS Cliente,
		'' AS Compras,
		ISNULL(RTRIM(A4_NOME),'') AS Transportadora,
		'' AS Redespacho,
		SA1.A1_CEP AS CEP,
		RTRIM(SA1.A1_MUN) AS Cidade,
		SA1.A1_EST AS UF,
		ISNULL(ZA7.ZA7_NOME,'') AS Separador,
		CASE WHEN SC5.C5_TPFRETE = 'C' THEN 'CIF' WHEN SC5.C5_TPFRETE = 'F' THEN 'FOB' END AS Frete,
		SC5.C5_XBLQCRE AS Credito,
		CONVERT(VARCHAR(12),CAST(SC6.C6_ENTREG AS DATE),103)  AS Dt_Entrega,
		'' AS Prioridade,
		COUNT(SC6.C6_PRODUTO) AS Total_Itens,
		SUM(SC6.C6_QTDVEN) AS Total_Pecas,
		'' AS Total_Pecas_Separadas,
		'' AS Perc_Pecas_Separadas,
		'' AS Total_Pecas_Membradas,
		'' AS Perc_Pecas_Membradas,
		SUM(SC6.C6_VALOR) AS Valor_Total_Entrega,
		SC6.C6_XDTLIST AS Dt_Lista,
		SC6.C6_XHRLIST AS Hora_Lista,
		SC6.C6_XDTLIB  AS Dt_Lib_Separacao,
		SC6.C6_XHRLIB  AS Antecipacao_Restrita,
		'Agendamento_Previo' AS Agendamento_Previo,
		'Especial' AS Especial,
		'Observacao' AS Observacao,
			
		SC5.C5_XPEDCOB AS PEDCOB,
		SA1.A1_XAGEND  AS AGEND,
		SC5.C5_XANTFAT AS ANTFAT,
		SA1.A1_SUFRAMA AS SUFRAMA
	FROM SC5010 AS SC5
	INNER JOIN SC6010 AS SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND C5_CLIENTE = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC6.D_E_L_E_T_ = ''
	INNER JOIN SA1010 AS SA1 ON C5_CLIENTE+C5_LOJACLI = A1_COD+A1_LOJA AND SA1.D_E_L_E_T_ = ''
	LEFT JOIN  SA4010 AS SA4 ON C5_TRANSP = A4_COD AND SA4.D_E_L_E_T_ = ''
	LEFT JOIN  ZA7010 AS ZA7 ON C5_XSEPARA = ZA7_COD AND ZA7.D_E_L_E_T_ = ''
	WHERE SC5.D_E_L_E_T_ = '' AND SC5.C5_XCANAL IN ('A','E','V') AND C6_QTDENT < C6_QTDVEN
	AND C6_XDTLIST <> ''
	GROUP BY C5_XSTATUS, C5_XKITADM, SC5.C5_NUM, C5_XCANAL, A1_NOME, A4_NOME, A1_CEP, A1_MUN, A1_EST, ZA7_NOME, C5_TPFRETE, C5_XBLQCRE, C6_ENTREG, C6_XDTLIST, C6_XHRLIST, C6_XDTLIB, C6_XHRLIB, C5_XPEDCOB, A1_XAGEND, C5_XANTFAT, A1_SUFRAMA
	ORDER BY Canal_Venda, Pedido 
EndSQL


DbSelectArea(cAliasADMO)
(cAliasADMO)->(DbGotop())	
	
	
//--------------------------------------------------------------	
// ITENS DO GRID
//--------------------------------------------------------------
While (cAliasADMO)->(!EoF())
	If (cAliasADMO)->PEDCOB == 'SIM'
		cLegend   := oVioleta
	ElseIf (cAliasADMO)->AGEND == 'SIM'
		cLegend   := oVermelho
	ElseIf ALLTRIM((cAliasADMO)->Canal_Venda) == 'E-COMMERCE'
		cLegend   := oLaranja
	ElseIf (cAliasADMO)->ANTFAT == 'NAO'
		cLegend   := oAmarelo
	ElseIf (cAliasADMO)->SUFRAMA <> ' '
		cLegend   := oAzul
	Else
		cLegend   := oVerde
	EndIf
	
	aAdd(aCols, { .F.  , cLegend , (cAliasADMO)->Status , (cAliasADMO)->Kit , (cAliasADMO)->Canal_Venda , (cAliasADMO)->Pedido , (cAliasADMO)->Cliente , (cAliasADMO)->Compras , (cAliasADMO)->Transportadora , (cAliasADMO)->Redespacho , (cAliasADMO)->CEP , (cAliasADMO)->Cidade , (cAliasADMO)->UF , (cAliasADMO)->Separador , (cAliasADMO)->Frete , (cAliasADMO)->Credito , (cAliasADMO)->Dt_Entrega , (cAliasADMO)->Prioridade , (cAliasADMO)->Total_Itens , (cAliasADMO)->Total_Pecas , (cAliasADMO)->Total_Pecas_Separadas , (cAliasADMO)->Perc_Pecas_Separadas , (cAliasADMO)->Valor_Total_Entrega , (cAliasADMO)->Dt_Lista , (cAliasADMO)->Hora_Lista , (cAliasADMO)->Dt_Lib_Separacao , (cAliasADMO)->Antecipacao_Restrita , (cAliasADMO)->Agendamento_Previo , (cAliasADMO)->Especial , (cAliasADMO)->Observacao } )  
	(cAliasADMO)->(dbSkip())
End

	
//--------------------------------------------------------------	
// DIMENS�ES DO GRID NA TELA CUSTOMIZADA
//--------------------------------------------------------------	
oBrowse := TWBrowse():New(30, 4, 517, 167, NIL, aHeader, NIL, oDlg, (cAliasADMO)->Status, "2", "2",,,,,,,,,, 'ARRAY', .T. )
oBrowse:SetArray(aCols)


oBrowse:bLine := {|| {	If(aCols[oBrowse:nAT, 1], oOk, oNo),;
						aCols[oBrowse:nAT, 2],;
						aCols[oBrowse:nAT, 3],;
						aCols[oBrowse:nAT, 4],;
						aCols[oBrowse:nAT, 5],;
						aCols[oBrowse:nAT, 6],;
						aCols[oBrowse:nAT, 7],;
						aCols[oBrowse:nAT, 8],;
						aCols[oBrowse:nAT, 9],;
						aCols[oBrowse:nAT, 10],;
						aCols[oBrowse:nAT, 11],;
						aCols[oBrowse:nAT, 12],;
						aCols[oBrowse:nAT, 13],;
						aCols[oBrowse:nAT, 14],;
						aCols[oBrowse:nAT, 15],;
						aCols[oBrowse:nAT, 16],;
						aCols[oBrowse:nAT, 17],;
						aCols[oBrowse:nAT, 18],;
						aCols[oBrowse:nAT, 19],;
						aCols[oBrowse:nAT, 20],;
						aCols[oBrowse:nAT, 21],;
						aCols[oBrowse:nAT, 22],;
						aCols[oBrowse:nAT, 23],;
						aCols[oBrowse:nAT, 24],;
						aCols[oBrowse:nAT, 25],;
						aCols[oBrowse:nAT, 26],;
						aCols[oBrowse:nAT, 27],;
						aCols[oBrowse:nAT, 28],;
						aCols[oBrowse:nAT, 29],;
						aCols[oBrowse:nAT, 30]} }

oBrowse:blDblClick := {|| aCols[oBrowse:nAt,1] := !aCols[oBrowse:nAt,1], oBrowse:Refresh()}    



//--------------------------------------------------------------
//   MONTAGEM DA TELA (CONTINUA��O)                                                            
//--------------------------------------------------------------
@ 198, 003 GROUP oGroup3 TO 250, 522 OF oDlg COLOR 0, 16777215 PIXEL
@ 205, 010 SAY oSay4 PROMPT "Separador:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 213, 010 MSCOMBOBOX oComboBo3 VAR nComboBo3 ITEMS aSepara SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
@ 212, 084 BUTTON oButton9 PROMPT "Designar" SIZE 037, 012 OF oDlg PIXEL
@ 212, 140 BUTTON oButton10 PROMPT "Ativar" SIZE 030, 012 OF oDlg ACTION ADMOKITY(aCols, oBrowse) PIXEL
@ 212, 174 BUTTON oButton16 PROMPT "Desativar" SIZE 030, 012 OF oDlg ACTION ADMOKITN(aCols, oBrowse) PIXEL
@ 205, 140 SAY oSay5 PROMPT "Kit:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 205, 222 SAY oSay6 PROMPT "Transportadora:" SIZE 040, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 213, 222 MSCOMBOBOX oComboBo4 VAR nComboBo4 ITEMS aTransp SIZE 072, 010 OF oDlg COLORS 0, 16777215 PIXEL
@ 212, 296 BUTTON oButton11 PROMPT "Aplicar" SIZE 027, 012 OF oDlg ACTION ADMOTRANS(aCols, nComboBo4, oBrowse) PIXEL
@ 205, 344 SAY oSay7 PROMPT "Prioridade:" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 215, 345 SAY oSay8 PROMPT "Seq." SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
@ 213, 360 MSGET oGet1 VAR cGet1 SIZE 025, 010 OF oDlg COLORS 0, 16777215 PIXEL
@ 212, 388 BUTTON oButton12 PROMPT "Aplicar" SIZE 027, 012 OF oDlg PIXEL
@ 212, 477 BUTTON oButton13 PROMPT "Excluir" SIZE 037, 012 OF oDlg PIXEL
@ 232, 010 BUTTON oButton14 PROMPT "OK" SIZE 060, 012 OF oDlg PIXEL
@ 232, 072 BUTTON oButton15 PROMPT "ALOCA��O DE COMPRAS" SIZE 070, 012 OF oDlg PIXEL

ACTIVATE MSDIALOG oDlg CENTERED

Return

//---------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------
Static Function MontaColunas(cCampo,cTitulo,nArrData,cPicture,nAlign,nSize,nDecimal)
	Local aColumn
	Local bData 	:= {||}
	Default nAlign 	:= 1
	Default nSize 	:= 20
	Default nDecimal:= 0
	Default nArrData:= 0
	
	
	
	If nArrData > 0
		bData := &("{||" + cCampo +"}") //&("{||oBrowse:DataArray[oBrowse:At(),"+STR(nArrData)+"]}")
	EndIf
	aColumn := {cTitulo,bData,,cPicture,nAlign,nSize,nDecimal,.F.,{||.T.},.F.,{||.T.},NIL,{||.T.},.F.,.F.,{}}
Return {aColumn}
//---------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------
/*Static Function MenuDef()
	Local aArea		:= GetArea()
	Local aRotina 	:= {}
	Local aRotina1  := {}
	
	AADD(aRotina1, {"Consulta Produto"	, "MATC050()"		, 0, 6, 0, Nil })
	AADD(aRotina1, {"Legenda"			, "U_EXEM992L"		, 0,11, 0, Nil })
	
	AADD(aRotina, {"Pesquisar"			, "PesqBrw"			, 0, 1, 0, .T. })
	AADD(aRotina, {"Visualizar"			, "U_EXEM992I"		, 0, 2, 0, .F. })
	
	AADD(aRotina, {"Incluir"			, "U_EXEM992I"		, 0, 3, 0, Nil })
	AADD(aRotina, {"Alterar"			, "U_EXEM992I"		, 0, 4, 0, Nil })
	AADD(aRotina, {"Excluir"			, "U_EXEM992I"		, 0, 5, 3, Nil })
	
	AADD(aRotina, {"Mais a��es..."    	, aRotina1                   , 0, 4, 0, Nil }      )
	
Return( aRotina )*/
//---------------------------------------------------------------------------------------




//  LISTA AS TRANSPORTADORAS
//---------------------------------------------------------------------------------------
User Function RetTransp()
	Local aTransp   := {}
	Local cAliasA4  := GetNextAlias()
	
	BeginSQL Alias cAliasA4
		SELECT A4_COD + ' - ' + RTRIM(A4_NREDUZ) AS TRANSP FROM %table:SA4% WHERE %NotDel% ORDER BY A4_NREDUZ
	EndSQL
	
	While (cAliasA4)->(!EoF())
		aAdd( aTransp, (cAliasA4)->TRANSP)
		(cAliasA4)->(dbSkip())
	End

Return(aTransp)
//---------------------------------------------------------------------------------------



//   LISTA OS OPERADORES
//---------------------------------------------------------------------------------------
User Function RetSepara()
	Local aSepara    := {}
	Local cAliasZA7  := GetNextAlias()
	
	BeginSQL Alias cAliasZA7
		SELECT ZA7_NOME FROM ZA7010 WHERE %NotDel% ORDER BY 1
	EndSQL
	
	While (cAliasZA7)->(!EoF())
		aAdd( aSepara, (cAliasZA7)->ZA7_NOME)
		(cAliasZA7)->(dbSkip())
	End

Return(aSepara)
//---------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------
Static Function ADMOKITY(aCols, oBrowse) //ADMO KIT YES

Local aCols   := aCols
Local oBrowse := oBrowse

For nCountA := 1 To Len(aCols)
	If aCols[nCountA, 1] == .T.
		SC5->(dbSetOrder(1))
		SC5->(dbGoTop())
		If SC5->(dbSeek(xFilial("SC5") + aCols[nCountA, 6]))			
			Reclock("SC5",.F.)
				SC5->C5_XKITADM := 'SIM'
			MsUnLock()
			
			aCols[oBrowse:nAt,4] := "SIM"
			oBrowse:Refresh() 			
		EndIf
	EndIf
Next nCountA

MsgInfo("A op��o de Kit foi ativada para os pedidos selecionados.","KIT")

Return
//---------------------------------------------------------------------------------------




//---------------------------------------------------------------------------------------
Static Function ADMOKITN(aCols, oBrowse) //ADMO KIT N�O

Local aCols := aCols
Local oBrowse := oBrowse

For nCountA := 1 To Len(aCols)
	If aCols[nCountA, 1] == .T.
		SC5->(dbSetOrder(1))
		SC5->(dbGoTop())
		If SC5->(dbSeek(xFilial("SC5") + aCols[nCountA, 6]))			
			Reclock("SC5",.F.)
				SC5->C5_XKITADM := 'NAO'
			MsUnLock()
			
			aCols[oBrowse:nAt,4] := "NAO"
			oBrowse:Refresh()  
		EndIf
	EndIf
Next nCountA

MsgInfo("A op��o de Kit foi desativada para os pedidos selecionados.","KIT")

Return
//---------------------------------------------------------------------------------------





//---------------------------------------------------------------------------------------
Static Function ADMOTRANS(aCols, nComboBo4, oBrowse) //ALTERA��O DE TRANSPORTADORA

Local aCols      := aCols
Local nComboBo4  := nComboBo4
Local oBrowse    := oBrowse

For nCountA := 1 To Len(aCols)
	If aCols[nCountA, 1] == .T.
		SC5->(dbSetOrder(1))
		SC5->(dbGoTop())
		If SC5->(dbSeek(xFilial("SC5") + aCols[nCountA, 6]))			
			SA4->(dbSetOrder(1))
			SA4->(dbGoTop())
			If SA4->(dbSeek(xFilial("SA4") + LEFT(cValToChar(nComboBo4),6)))			
				Reclock("SC5",.F.)
					SC5->C5_TRANSP := SA4->A4_COD
				MsUnLock()
				
			aCols[oBrowse:nAt,9] := SA4->A4_NREDUZ
			oBrowse:Refresh() 				
			EndIf
		EndIf
	EndIf
Next nCountA

MsgInfo("A transportadora foi alterada para os pedidos selecionados.","TRANSPORTADORA")

Return
//---------------------------------------------------------------------------------------



Static Function ADMOLEG()

Local aLegenda := {}
aAdd( aLegenda, { "BR_LARANJA"		, "Clientes E-Commerce" })
aAdd( aLegenda, { "BR_VERMELHO"		, "Clientes Com Agendamento" })
aAdd( aLegenda, { "BR_AMARELO"		, "Clientes N�o Antecipa Faturamento" })
aAdd( aLegenda, { "BR_AZUL"		    , "Clientes Com Suframa" })
aAdd( aLegenda, { "BR_VIOLETA"		, "Pedido Gerado Para Cobran�a" })
aAdd( aLegenda, { "BR_VERDE"		, "Outros" })
BrwLegenda( "Legenda", "Legenda", aLegenda )

Return( Nil )
