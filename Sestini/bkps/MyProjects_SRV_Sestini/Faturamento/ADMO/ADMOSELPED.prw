#Include "PROTHEUS.CH"
//--------------------------------------------------------------
/*/{Protheus.doc} ADMOSELPED
Description                                                     
                                                                
@param xParam Parameter Description                             
@return xRet Return Description                                 
@author  -                                               
@since 16/08/2018                                                   
/*/                                                             
//--------------------------------------------------------------
User Function ADMOSELPED()                        
Local oButton1
Local oButton2
Local oGet1
Local cGet1 := "Define variable value"
Local oGet2
Local cGet2 := "Define variable value"
Local oGet3
Local cGet3 := "Define variable value"
Local oGet4
Local cGet4 := "Define variable value"
Local oGet5
Local cGet5 := "Define variable value"
Local oGet6
Local cGet6 := "Define variable value"
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Sele��o dos Pedidos" FROM 000, 000  TO 200, 450 COLORS 0, 16777215 PIXEL

    @ 013, 074 SAY oSay1 PROMPT "Crit�rio de Sele��o dos Pedidos" SIZE 080, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 059 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 043, 059 MSGET oGet2 VAR cGet2 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 058, 059 MSGET oGet3 VAR cGet3 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 134 MSGET oGet4 VAR cGet4 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 043, 134 MSGET oGet5 VAR cGet5 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 058, 134 MSGET oGet6 VAR cGet6 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
    @ 029, 023 SAY oSay2 PROMPT "Num. Pedido:" SIZE 036, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 044, 026 SAY oSay3 PROMPT "Dt. Entrega:" SIZE 033, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 059, 022 SAY oSay4 PROMPT "Nome Abrev.:" SIZE 036, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 077, 073 BUTTON oButton1 PROMPT "Executar" SIZE 037, 012 OF oDlg PIXEL
    @ 077, 114 BUTTON oButton2 PROMPT "Fechar" SIZE 037, 012 OF oDlg PIXEL
    
    ACTIVATE MSDIALOG oDlg CENTERED


Return