#include 'protheus.ch'
#INCLUDE "FWMVCDEF.CH"


User Function CADKIT()

	Local aArea     := GetArea()
	Private oBrowse := FwMBrowse():New()

	oBrowse:SetAlias('ZB1')
	oBrowse:SetMenudef('CADKIT')

	//Descri��o da Parte Superior Esquerda do Browse
	oBrowse:SetDescripton("Cadastro de KIT (E-Comm)")

	//Desabilita os Detalhes da parte inferior do Browse
	oBrowse:DisableDetails()

	//Ativa o Browse
	oBrowse:Activate()
	
	RestArea(aArea)

Return


/*===========================================================================================================*/


Static Function MenuDef()

	Local aRotina := {}

	ADD OPTION aRotina TITLE 'Visualizar' ACTION 'VIEWDEF.CADKIT' OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE 'Incluir'    ACTION 'VIEWDEF.CADKIT' OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE 'Alterar'    ACTION 'VIEWDEF.CADKIT' OPERATION 4 ACCESS 0
	ADD OPTION aRotina TITLE 'Excluir'    ACTION 'VIEWDEF.CADKIT' OPERATION 5 ACCESS 0
	ADD OPTION aRotina TITLE 'Imprimir'   ACTION 'VIEWDEF.CADKIT' OPERATION 8 ACCESS 0

Return aRotina


/*===========================================================================================================*/


Static Function ModelDef()

	Local oModel    := Nil
	Local oStPai    := FWFormStruct(1,'ZB1', /*bAvalCampo*/, /*lViewUsado*/) //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
	Local oStFilho  := FWFormStruct(1,'ZB2', /*bAvalCampo*/, /*lViewUsado*/) //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
	Local aSZXRel   := {}

	//Instancia do Objeto de Modelo de Dados
	oModel := MpFormModel():New('MDCADKIT',,, {|oModel| GrvDados(oModel)})
	oModel:AddFields('PAI_CADKIT', /*cOwner*/, oStPai)
	oModel:AddGrid('FILHO_CADKIT', 'PAI_CADKIT', oStFilho)

	//Fazendo o relacionamento entre o Pai e Filho
    oModel:SetRelation('FILHO_CADKIT'    , {{'ZB2_FILIAL', 'ZB1_FILIAL'}, {'ZB2_COD', 'ZB1_COD'}})
    
	oModel:GetModel('FILHO_CADKIT'):SetUniqueLine({"ZB2_FILIAL","ZB2_COD","ZB2_COMP"})    //N�o repetir informa��es ou combina��es {"CAMPO1","CAMPO2","CAMPOX"}
    
	oModel:SetPrimaryKey({})

    //Setando as descri��es
    oModel:SetDescription("Libera��o de titulos")
    oModel:GetModel('PAI_CADKIT'):SetDescription('Cabe�alho Cadastro de KIT')
    oModel:GetModel('FILHO_CADKIT'):SetDescription('Itens Cadastro de KIT')


	
Return(oModel)


/*===========================================================================================================*/


Static Function ViewDef()

    Local oView         := Nil
    Local oModel        := FWLoadModel('CADKIT')
    Local oStPai        := FWFormStruct(2, 'ZB1')
    Local oStFilho      := FWFormStruct(2, 'ZB2')


    //Criando a View
    oView := FWFormView():New()
    oView:SetModel(oModel)

    //Retira um campo da Estrutura da View
    oStFilho:RemoveField('ZB2_COD')
	oStFilho:RemoveField('ZB2_VALTOT')


    //Adicionando os campos do cabe�alho e o grid dos filhos
    oView:AddField('VIEW_PAI',oStPai,'PAI_CADKIT')
    oView:AddGrid('VIEW_FILHO',oStFilho,'FILHO_CADKIT')


    //Setando o dimensionamento de tamanho
    oView:CreateHorizontalBox('CABEC',25)
    oView:CreateHorizontalBox('GRID',75)

    //Amarrando a view com as box
    oView:SetOwnerView('VIEW_PAI','CABEC')
    oView:SetOwnerView('VIEW_FILHO','GRID')

	oView:EnableTitleView('VIEW_FILHO' , 'Itens Cadastro de KIT (E-Comm)' )
	oView:EnableTitleView('VIEW_PAI' , 'Cabe�alho Cadastro de KIT (E-Comm)' )



    //Habilitando t�tulo
    oView:SetCloseOnOk({ || .T. })

Return oView


/*===========================================================================================================*/


/*
FUNCAO DE GRAVACAO DOS DADOS
*/
Static Function GrvDados(oModel)
Local nCountA := 0
LocAL nTotal  := 0

//-- GRAVA TODO FORMULARIO ATUAL
lRet := FwFormCommit(oModel)

//-- COMPLEMENTA A GRAVADO DOS DADOS

If lRet
	For nCountA := 1 To oModel:GetModel('FILHO_CADKIT'):Length()
		oModel:GetModel('FILHO_CADKIT'):GoLine(nCountA)
		nTotal := nTotal + oModel:GetModel('FILHO_CADKIT'):GetValue('ZB2_VALITE')
	Next nCountA

	ZB2->(DbSetOrder(1))
	If ZB2->(DbSeek(xFilial("ZB2") + oModel:GetModel('PAI_CADKIT'):GetValue('ZB1_COD')))
		While !ZB2->(Eof()) .And. ZB2->ZB2_FILIAL == xFilial('ZB2') .And. ZB2->ZB2_COD == oModel:GetModel('PAI_CADKIT'):GetValue('ZB1_COD')
			RecLock('ZB2', .F.)
			ZB2->ZB2_VALTOT   := nTotal
			ZB2->(MsUnlock())

			ZB2->(DbSkip())
		End
	EndIf

EndIf

Return(lRet)


