#include "TOTVS.ch"
#include "rwmake.ch"

/*
������������������������������������������������������������������������������
��� Programa  STNI010     Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function STNI010

	Local cDBRak	   := "MSSQL/INTEGRATIONQUEUE" 
	Local cSrvRak	   := "172.16.0.97"
	Local nPortaDBA    := 5010
	Local nHndRak	   := 0
	Local nHndERP      := AdvConnection()	
	
	//FAZ CONEX�O COM O BANCO DA RAKUTEN
	nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
	
	If nHndRak < 0
		Alert("N�o foi poss�vel conectar no banco da Rakuten. Entre em contato com o TI")
		Return
	Else
		cUpdateRK := " UPDATE RK_RECEPCAO_PEDIDO SET DataEnvioProtheus = NULL, ReenvioBonificado = 'S' " + CRLF
		cUpdateRK += " WHERE PedidoCodigo = " + cValToChar(SC5->C5_XPEDRAK) + " AND PedidoStatus = 7 "
		
		nStatusRK := TcSqlExec (cUpdateRK)
		
		//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
		TcSetConn( nHndERP )
		
		cUpdatePR := " UPDATE SC5010 SET C5_XREEBON = 'S' " + CRLF
		cUpdatePR += " WHERE D_E_L_E_T_ = '' AND C5_XPEDRAK = " + cValToChar(SC5->C5_XPEDRAK)
		
		nStatusPR := TcSqlExec (cUpdatePR)
				
		
		If (nStatusRK >= 0 .And. nStatusPR >= 0)
			MsgInfo("Favor verificar o pedido " +cValToChar(C5_XPCLIRK)+ " na pr�xima integra��o.")
		Else
			Alert("N�o foi poss�vel habilitar pedido para reenvio.")
		EndIf
	EndIf	
	
	TCUnlink()

Return





User Function STNI020

	Local cDBRak	   := "MSSQL/INTEGRATIONQUEUE" 
	Local cSrvRak	   := "172.16.0.97"
	Local nPortaDBA    := 5010
	Local nHndRak	   := 0
	Local nHndERP      := AdvConnection()	
	
	//FAZ CONEX�O COM O BANCO DA RAKUTEN
	nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
	
	If nHndRak < 0
		Alert("N�o foi poss�vel conectar no banco da Rakuten. Entre em contato com o TI")
		Return
	Else
		cUpdateRK := " UPDATE RK_RECEPCAO_PEDIDO SET DataEnvioProtheus = NULL, ReenvioBonificado = 'N' " + CRLF
		cUpdateRK += " WHERE PedidoCodigo = " + cValToChar(SC5->C5_XPEDRAK) + " AND PedidoStatus = 7 "
		
		nStatusRK := TcSqlExec (cUpdateRK)
		
		//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
		TcSetConn( nHndERP )
		
		cUpdatePR := " UPDATE SC5010 SET C5_XREEBON = 'N' " + CRLF
		cUpdatePR += " WHERE D_E_L_E_T_ = '' AND C5_XPEDRAK = " + cValToChar(SC5->C5_XPEDRAK)
		
		nStatusPR := TcSqlExec (cUpdatePR)
				
		
		If (nStatusRK >= 0 .And. nStatusPR >= 0)
			MsgInfo("Favor verificar o pedido " +cValToChar(C5_XPCLIRK)+ " na pr�xima integra��o.")
		Else
			Alert("N�o foi poss�vel habilitar pedido para reenvio.")
		EndIf
	EndIf	
	
	TCUnlink()

Return