#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE 'FWMVCDEF.CH'
/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  CopyTab     Autor  Rogerio Machado         Data 13/06/2018   º±±
±±º Desc.     Copia a Tabela de Preço aplicando desconto/acrescimo         º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/
User Function CopyTab()
	Local aArea         := GetArea()
	Local aAreaDA0      := DA0->(GetArea())
	Local aAreaDA1      := DA1->(GetArea())
	Local nOpcA         := 0
	Local oDlg
	Local oSize
	Local oFontLbl
	Local _cAliasTRB      := GetNextAlias()
	Local aTipoCopy       := {"A - Acrescimo","D - Desconto"}
	Local cUseMas         := GetMV("MV_XUSEMAS")
	Local cUsrLog         := RetCodUsr()
	Local lWhen           := .F.
	
	Private cCadastro     := "Cópia Tab. (Acres./Desc.)"
	Private cCodigoOri    := DA0->DA0_CODTAB
	Private cDescOri      := CriaVar('DA0_DESCRI')
	Private cCodigoDest   := CriaVar('DA0_CODTAB')
	Private cDescDest     := CriaVar('DA0_DESCRI')
	Private nPercDesc     := CriaVar('DA1_VLRDES')
	Private dDataIni      := dDataBase
	Private dDataFim      := StoD("20501231")
	Private cTipoCopy     := CriaVar('DA0_XTPCOP')
	
	BeginSQL Alias _cAliasTRB
		SELECT MAX(DA0_CODTAB) MAX_TAB FROM %table:DA0% AS DA0 WHERE DA0.%notDel%
	 EndSQL
	
	oSize := FwDefSize():New( .F. )

	oSize:AddObject("AREA", 100, 100, .T., .T., .T.) // Totalmente dimensionavel 
	
	oSize:lProp    := .F. // Proporcional             
	oSize:aMargins := {10, 10, 10, 10}  
	
	oSize:Process() // Dispara os calculos
	
	DEFINE FONT oFontLbl NAME "Arial" SIZE 6, 15 BOLD
	
	// Monta Dialog
	DEFINE MSDIALOG oDlg TITLE cCadastro FROM oSize:aWindSize[1],oSize:aWindSize[2] TO oSize:aWindSize[3],oSize:aWindSize[4] OF oMainWnd PIXEL
    
	// Adiciona Panel
	oPanel:= tPanel():New(000,000,,oDlg,,,,,,100,oSize:aPosObj[1,3])

	oPanel:Align := CONTROL_ALIGN_ALLCLIENT
	
	DbSelectArea(_cAliasTRB)
	(_cAliasTRB)->(dbGoTop())
	
	cCodigoDest := PADL((VAL((_cAliasTRB)->MAX_TAB)+1),3,"0")

	// Primeira fileira de campos
	@ oSize:aPosObj[01,01]      , oSize:aPosObj[01,02] + 000 SAY 'Tabela Master' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 009, oSize:aPosObj[01,02] + 000 MSGET cCodigoOri Valid DA0->(dbSeek(xFilial("DA0")+cCodigoOri)) .And. ValType(cDescOri:=DA0->DA0_DESCRI) WHEN .T. SIZE 50,12  OF oPanel PIXEL F3 "DA0"
	@ oSize:aPosObj[01,01]      , oSize:aPosObj[01,02] + 100 SAY 'Descrição Tabela' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 009, oSize:aPosObj[01,02] + 100 MSGET cDescOri Valid DA0->(dbSeek(xFilial("DA0")+cCodigoOri)) .And. ValType(cDescOri:=DA0->DA0_DESCRI) Picture PesqPict('DA0','DA0_DESCRI') WHEN .F. SIZE 150,12  OF oPanel PIXEL 

	// Segunda fileira de campos
	@ oSize:aPosObj[01,01] + 040, oSize:aPosObj[01,02] + 000 SAY 'Tabela Cópia' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 049, oSize:aPosObj[01,02] + 000 MSGET cCodigoDest Valid !DA0->(dbSeek(xFilial("DA0")+cCodigoDest)) Picture PesqPict('DA0','DA0_CODTAB') WHEN .T. SIZE 50,12 OF oPanel PIXEL
	@ oSize:aPosObj[01,01] + 040, oSize:aPosObj[01,02] + 100 SAY 'Descrição Tabela' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 049, oSize:aPosObj[01,02] + 100 MSGET cDescDest Picture PesqPict('DA0','DA0_DESCRI') SIZE 150,12  OF oPanel PIXEL
	
	If cUsrLog $ cUseMas
		lWhen := .T.
	EndIf
	
	// Terceira fileira de campos
	@ oSize:aPosObj[01,01] + 080, oSize:aPosObj[01,02] + 000 SAY 'Acrescimo ou Desconto?' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 089, oSize:aPosObj[01,02] + 000 ComboBox cTipoCopy Items aTipoCopy WHEN lWhen SIZE 50,12 OF oPanel PIXEL
	@ oSize:aPosObj[01,01] + 080, oSize:aPosObj[01,02] + 100 SAY 'Porcentagem' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 089, oSize:aPosObj[01,02] + 100 MSGET nPercDesc Valid nPercDesc > 0 Picture PesqPict('DA1','DA1_VLRDES') WHEN .T. SIZE 50,12 OF oPanel PIXEL

	// Quarta fileira de campos
	@ oSize:aPosObj[01,01] + 120, oSize:aPosObj[01,02] + 000 SAY 'Data Inicial' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 129, oSize:aPosObj[01,02] + 000 MSGET dDataIni Picture PesqPict('DA0','DA0_DATDE') WHEN .T. SIZE 50,12 OF oPanel PIXEL

	// Quinta fileira de campos
	@ oSize:aPosObj[01,01] + 160, oSize:aPosObj[01,02] + 000 SAY 'Data Final' OF oPanel PIXEL FONT oFontLbl
	@ oSize:aPosObj[01,01] + 169, oSize:aPosObj[01,02] + 000 MSGET dDataFim Picture PesqPict('DA0','DA0_DATATE') WHEN .T. SIZE 50,12 OF oPanel PIXEL

	
	ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg,{||nOpcA := 1,oDlg:End()},{||oDlg:End()},,)  
	
	If nOpcA = 1
		Begin Transaction
			U_GravCopy()
		End Transaction
	Endif
	
(_cAliasTRB)->(dbCloseArea())
		
RestArea(aArea)
RestArea(aAreaDA0)
RestArea(aAreaDA1)
	  

Return


/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  GravCopy    Autor  Rogerio Machado         Data 13/06/2018   º±±
±±º Desc.     Grava a copia da tabela Pai com o desconto informado         º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/
User Function GravCopy()
	Local _cAliasTRB	:= GetNextAlias()
	
	DA0->(dbSeek(xFilial("DA0")+cCodigoOri))
	
	Reclock("DA0",.T.)
		DA0->DA0_FILIAL  := xFilial("DA1")
		DA0->DA0_CODTAB  := cCodigoDest
		DA0->DA0_DESCRI  := cDescDest
		DA0->DA0_DATDE   := dDataIni
		DA0->DA0_HORADE  := "00:00"
		DA0->DA0_DATATE  := dDataFim
		DA0->DA0_HORATE  := "23:59"
		DA0->DA0_TPHORA  := "1"  
		DA0->DA0_ATIVO   := "1"
		DA0->DA0_XTBPAI  := cCodigoOri    
		DA0->DA0_XDESC   := nPercDesc
		DA0->DA0_XTPCOP  := LEFT(cTipoCopy,1)
	DA0->(MsUnlock())
	
	BeginSQL Alias _cAliasTRB
		SELECT * FROM %table:DA1% AS DA1 WHERE DA1.%notDel%
		AND DA1_CODTAB = %exp:cCodigoOri%
	EndSQL
	
	DbSelectArea(_cAliasTRB)
	(_cAliasTRB)->(dbGoTop())
	
	While (_cAliasTRB)->(!Eof())
		Reclock("DA1",.T.)
			DA1->DA1_FILIAL  := xFilial("DA1")
			DA1->DA1_CODTAB  := cCodigoDest
			DA1->DA1_ITEM    := (_cAliasTRB)->DA1_ITEM
			DA1->DA1_CODPRO  := (_cAliasTRB)->DA1_CODPRO
			DA1->DA1_XPRCOR  := (_cAliasTRB)->DA1_XPRCOR
			If LEFT(cTipoCopy,1) == "A"
				DA1->DA1_PRCVEN  := (_cAliasTRB)->DA1_PRCVEN * (1+(nPercDesc/100))	
			Else
				DA1->DA1_PRCVEN  := (_cAliasTRB)->DA1_PRCVEN - ( (_cAliasTRB)->DA1_PRCVEN * (nPercDesc/100) )
			EndIf
			DA1->DA1_ATIVO   := (_cAliasTRB)->DA1_ATIVO
			DA1->DA1_TPOPER  := (_cAliasTRB)->DA1_TPOPER
			DA1->DA1_QTDLOT  := (_cAliasTRB)->DA1_QTDLOT
			DA1->DA1_MOEDA   := (_cAliasTRB)->DA1_MOEDA
			DA1->DA1_DATVIG  := StoD((_cAliasTRB)->DA1_DATVIG)
			DA1->DA1_PRCMAX  := (_cAliasTRB)->DA1_PRCMAX
		DA1->(MsUnlock())
		(_cAliasTRB)->(dbSkip())
	End                                                                      
	
	AVISO(" C O N C L U Í D O ", "Cópia da tabela 'Pai' realizada com sucesso!", { "OK" }, 1)

(_cAliasTRB)->(dbCloseArea())

Return