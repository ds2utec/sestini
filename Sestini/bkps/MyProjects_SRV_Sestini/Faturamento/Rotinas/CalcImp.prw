#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  CalcImp    Autor  Rogerio Machado          Data 18/05/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function CalcImp(cPedNew)

	Local nPrcVen    := 0
	Local nQtdItens  := 0
	Local nDescSuf   := 0
	Local nPosRet    := 0
	Local nPosIPI    := 0
	Local nTotFrete  := 0
	Local nTotPed    := 0
	Local nFrete     := 0
	
	DbSelectarea("SC5")
	SC5->(DbSetorder(1))
	SC5->(DbSeek(xFilial("SC5") + cPedNew))
	
	DbSelectarea("SC6")
	SC6->(DbSetorder(1))
	SC6->(DbSeek(xFilial("SC6") + cPedNew))
	
	While SC6->(!EoF()) .And. SC6->C6_NUM == SC5->C5_NUM
		nQtdItens++
		SC6->(dbSkip())
	End
	
	DbSelectarea("SC6")
	SC6->(DbSetorder(1))
	SC6->(DbSeek(xFilial("SC6") + cPedNew))	
	
	// Faz uma varredura no aCols
	For _n := 1 to nQtdItens
		nDescSuf  := 0
	
		DbSelectarea("SA1")
		SA1->(DbSetorder(01))
		SA1->(DbSeek(xFilial("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI))


		MaFisIni(	SC5->C5_CLIENTE		,;		// 01-Codigo Cliente
					SC5->C5_LOJACLI  	,;		// 02-Loja do Cliente
					"C"				    ,;		// 03-C:Cliente , F:Fornecedor
					"N"				    ,;		// 04-Tipo da NF
					SC5->C5_TIPOCLI	    ,;		// 05-Tipo do Cliente
					Nil				    ,;		// 06-Relacao de Impostos que suportados no arquivo
					Nil				    ,;		// 07-Tipo de complemento
					Nil				    ,;		// 08-Permite Incluir Impostos no Rodape .T./.F.
					"SB1"			    ,;		// 09-Alias do Cadastro de Produtos - ("SBI" P/ Front Loja)
					"MATA461"		    ,;		// 10-Nome da rotina que esta utilizando a funcao
					Nil				    ,;		// 11-Tipo de documento
					Nil				    ,;		// 12-Especie do documento
					Nil				    ,;		// 13-Codigo e Loja do Prospect
					SA1->A1_GRPTRIB,    ;		// 14-Grupo Cliente
					Nil				    ,;		// 15-Recolhe ISS
					Nil				    ,;		// 16-Codigo do cliente de entrega na nota fiscal de saida
					Nil				    ,;		// 17-Loja do cliente de entrega na nota fiscal de saida
					Nil				    )		// 18-Informacoes do transportador [01]-UF,[02]-TPTRANS


		nPrcVen	:= SC6->C6_VALOR

		//Adiciona o Produto para Calculo dos Impostos
		nItem := 	MaFisAdd(	SC6->C6_PRODUTO  		,;   	// 1-Codigo do Produto ( Obrigatorio )
								SC6->C6_TES      		,;	   	// 2-Codigo do TES ( Opcional )
								SC6->C6_QTDVEN			,;	   	// 3-Quantidade ( Obrigatorio )
								SC6->C6_PRCVEN  		,;   	// 4-Preco Unitario ( Obrigatorio )
								0						,;  	// 5-Valor do Desconto ( Opcional )
								""						,;	   	// 6-Numero da NF Original ( Devolucao/Benef )
								""						,;		// 7-Serie da NF Original ( Devolucao/Benef )
								0						,;		// 8-RecNo da NF Original no arq SD1/SD2
								0                       ,;		// 9-Valor do Frete do Item ( Opcional )
								0						,;		// 10-Valor da Despesa do item ( Opcional )
								0						,;		// 11-Valor do Seguro do item ( Opcional )
								0						,;		// 12-Valor do Frete Autonomo ( Opcional )
								SC6->C6_VALOR    		,;		// 13-Valor da Mercadoria ( Obrigatorio )
								0						,;		// 14-Valor da Embalagem ( Opiconal )
								NIL						,;		// 15-RecNo do SB1
								NIL						,;		// 16-RecNo do SF4
								NIL						)

		aImpostos	:= MafisRet(NIL, "NF_IMPOSTOS")
		If Len(aImpostos) > 0
			nPosIPI		:= Ascan(aImpostos, {|x| AllTrim(x[01]) == "IPI"})

			If nPosIPI > 0
				nPrcVen	:= nPrcVen + aImpostos[nPosIPI][05]
				ConOut("PROSUTO: " + SC6->C6_PRODUTO +"   IPI: " +cValToChar(aImpostos[nPosIPI][05]))
			EndIf

			If SA1->A1_CALCSUF = 'S'
				nDescSuf := MafisRet(,"IT_DESCZF")
				nPrcVen  := nPrcVen - nDescSuf
			Endif

		EndIf
		
		nFrete := SC5->C5_FRETE + (SC5->C5_FRETE * (aImpostos[nPosIPI][04]/100) )

		//Finaliza Funcao Fiscal
		MaFisEnd()

		nTotPed += nPrcVen
		SC6->(dbSkip())
	Next _n

	//Somar o valor do Frete no Calculo dos Impostos
	If SC5->C5_FRETE > 0
		nTotPed += nFrete
	Endif


Return nTotPed
