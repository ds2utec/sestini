#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'


/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  CadCondRk   Autor  Rogerio Machado         Data 13/06/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function CadCondRk()

PRIVATE cCadastro  := "Cond. Pgto Rakuten"

PRIVATE aRotina     := {}

AxCadastro("ZA4", OemToAnsi(cCadastro))

 

Return Nil