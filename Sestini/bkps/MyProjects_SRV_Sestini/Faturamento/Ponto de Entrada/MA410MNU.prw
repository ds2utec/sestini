#include "TOTVS.ch"
#include "rwmake.ch"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  MA410MNU    Autor  Rogerio Machado         Data 13/06/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function MA410MNU
	
	aAdd(aRotina, {"Reenvio Bonificado"    , 'U_STNI010RK()' , 0 , 2 , 0 , NIL})
	aAdd(aRotina, {"Reenvio Cobrado"       , 'U_STNI020RK()' , 0 , 2 , 0 , NIL})
	AAdd(aRotina, {"Alterar Esp"           , "U_410ALTE"     , 0 , 2 , 0 , NIL})
	
Return	