#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  M460NOTA   Autor  Rogerio Machado          Data 18/05/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function M460NOTA()

If SC5->C5_XCANAL == "E"	
    DbSelectarea("SA1")
    SA1->(DbSetorder(01))
    If SA1->(DbSeek(xFilial("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
        RecLock('SA1', .F.)
            SA1->A1_END         := SC5->C5_XEND
            SA1->A1_BAIRRO      := SC5->C5_XBAIRRO
            SA1->A1_EST         := SC5->C5_XEST
            SA1->A1_CEP         := SC5->C5_XCEP
            SA1->A1_COD_MUN     := SC5->C5_XCODMUN
            SA1->A1_MUN         := SC5->C5_XMUN
        SA1->(MsUnlock())
    EndIf
EndIf

	
Return
