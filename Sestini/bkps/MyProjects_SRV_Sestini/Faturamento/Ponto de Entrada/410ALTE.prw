/*
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������
�����������������������������������������������������������������������������������Ŀ��
���Programa  � 410ALTE  � Autor � Andre Guirado                              																  � Data � 21/08/18���
�����������������������������������������������������������������������������������Ĵ��
���Descri��o � Altera dados do pedido de venda                                      ���
�����������������������������������������������������������������������������������Ĵ��
���Uso       �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
���Parametros� 						                                                ���
�����������������������������������������������������������������������������������Ĵ��
���Retorno   �                                                                      ���
�����������������������������������������������������������������������������������Ĵ��
������������������������� HISTORICO DE ATUALIZACOES DA ROTINA ���������������������Ĵ��
�����������������������������������������������������������������������������������Ĵ��
��� Desenvolvedor   � Data   �Solic.� Descricao                                     ���
�����������������������������������������������������������������������������������Ĵ��
���                 �        �      �                                               ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
���                 �        �      �                                               ���
������������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������������
���������������������������������������������������������������������������������������

*/
User Function 410ALTE()
Local cAlias := "SC5"
Local nOpcA  := 0
Local aSize  := {}, aObjects := {}, aInfo := {}, aPObjs := {}
Local k		 := 0
Local cCampo := ""
//Local aCpoMostra := SepCampos("C5_PESOL/C5_PBRUTO/C5_VOLUME1/C5_ESPECI1","/")  //Implementou os campos: C5_REDESP/C5_PESOL/C5_PBRUTO/C5_VOLUME1/C5_ESPECI1
Local cLinha:=GETMV("ES_CPOPED") 
Local aCpoMostra := SepCampos(cLinha,"/")  //Implementou os campos: C5_REDESP/C5_PESOL/C5_PBRUTO/C5_VOLUME1/C5_ESPECI1

Private aTela := {}, aGets := {}
Private oEnch1


nOpcA := 0

aSize		:= MsAdvSize(.T.)    // devolve o tamanho da dialog (janela) a ser criada e o tamanho da área de trabalho a ser utilizada pelos componentes
aObjects 	:= {}
aAdd( aObjects, { 100, 015, .T., .T.} )
aAdd( aObjects, { 100, 085, .T., .T.} )

aInfo  := { aSize[ 1 ], aSize[ 2 ], aSize[ 3 ], aSize[ 4 ], 0, 0 }
aPObjs := MsObjSize( aInfo, aObjects, .T.)  //  retorna o tamanho e posição de cada componente (objeto) da janela, conforme definições recebidas por meio dos parâmetros.
 
RegToMemory( "SC5", .F., .F. )

FOR k:=1 TO len(aCpoMostra)
	cCampo := aCpoMostra[k]
	M->&(cCampo) := &(cAlias + "->" + cCampo)
NEXT

Aadd(aCpoMostra, "NOUSER")

DEFINE MSDIALOG oDlg TITLE "Manutencao" From aSize[7], 0 To aSize[6], aSize[5] Of oMainWnd Pixel

oEnch1 := MsMGet():New(cAlias, &(cAlias)->(recno()), 4,,,, aCpoMostra , aPObjs[1], aCpoMostra,,,,,oDlg )
oEnch1:oBox:Align := CONTROL_ALIGN_ALLCLIENT

ACTIVATE MSDIALOG oDlg CENTER ON INIT EnchoiceBar(oDlg, {|| nOpcA := 1, IIF(Obrigatorio(aGets, aTela), oDlg:End(), nOpcA == 0)} , {|| nOpcA := 0, oDlg:End()} )

If nOpcA == 1
	RecLock(cAlias,.F.)
	FOR k:=1 TO len(aCpoMostra)
		cCampo := aCpoMostra[k]
		If cCampo <> "NOUSER"
			&(cAlias + "->" + cCampo) := M->&(cCampo)
		EndIf
	NEXT
	MsUnLock()
EndIf



Return()







/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � Sepcampos�Autor  �Choite              � Data �  24/01/05   ���
�������������������������������������������������������������������������͹��
���Desc.     � Separa campos,adiocionado os mesmo em vetor                ���
�������������������������������������������������������������������������͹��
���Uso       � AP6                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
STATIC FUNCTION SepCampos(cLinha,cSep1)
LOCAL nPos, aDadosLin := {}
cLinha := alltrim(cLinha)

WHILE len(alltrim(cLinha))<>0
	nPos := at(cSep1,cLinha)  // Posicao do separador1
	IF nPos<>0  // Nao e� ultimo campo
		aadd(aDadosLin,substr(cLinha,1,nPos-1))
	ELSE // ultimo campo
		aadd(aDadosLin,alltrim(cLinha))
		EXIT
	ENDIF
	cLinha := substr(cLinha,nPos+1,len(cLinha)-nPos)
END

RETURN(aDadosLin)
