#Include "Protheus.ch"

/*
������������������������������������������������������������������������������
��� Programa  OS010BTN    Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/
 
 /*
User Function OS010BTN()
	Local aArea    := GetArea()
	Local aButtons := {}
	                                                                          
	aAdd(aButtons,{ "Exportar CSV", {|| U_ExpTPreco()}, "Exportar CSV", Nil, Nil })
	aAdd(aButtons,{ "Importar CSV", {|| U_ImpTPreco()}, "Importar CSV", Nil, Nil })	
	aAdd(aButtons,{ "C�pia Tab. (Acres./Desc.)", {|| U_CopyTab()()}, "C�pia Tab. (Acres./Desc.)", Nil, Nil })
	
	RestArea(aArea)
Return aButtons
  */

/*
������������������������������������������������������������������������������
��� Programa  OS010BTN    Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User function Sel_Arq() 
Local cArquivo 
       
cArquivo := cGetFile("Documentos Excel|*.csv",OemToAnsi("Selecionar..."),0,"C:\",.T.,GETF_LOCALHARD+GETF_NETWORKDRIVE)

Return cArquivo


/*
������������������������������������������������������������������������������
��� Programa  ImpTPreco    Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/
 
User Function ImpTPreco()
	Local aArea     := GetArea()
	Local lRet      := .F.
	Local _cNum     := DA0->DA0_CODTAB 
	Local cTabMas   := GetMV("MV_XTABMAS")
	Local cUseMas   := GetMV("MV_XUSEMAS") 
	Local cUsrLog   := RetCodUsr()
	Local _cAliasTRB	:= GetNextAlias()
	
	
	Local cDirArq := ""
	Local cLinha  := ""
	Local lPrim   := .T.
	Local aCampos := {}
	Local aDados  := {}
	Local nXprcor := 0
	 
	Private aErro := {}
	
	If _cNum $ cTabMas
		If cUsrLog $ cUseMas
			lRet := .T.
		Else
			AVISO("Usu�rio n�o autorizado", "Usu�rio n�o autorizado para alterar tabela de pre�o master. Clique em Fechar da tela principal!", { "Sair" }, 1)
			Return
		EndIf
	EndIf
	
	cDirArq := U_Sel_Arq()
	 
	If !File(cDirArq)
		AVISO(" ARQUIVO N�O ENCONTRADO ", "O arquivo " +UPPER(cArquivo)+ " n�o foi encontrado. A importa��o ser� abortada!",{ "Sair" }, 1)
		Return
	EndIf
	 
	FT_FUSE(cDirArq)
	ProcRegua(FT_FLASTREC())
	FT_FGOTOP()
	While !FT_FEOF()
	 
		IncProc("Lendo arquivo CSV...")
	 
		cLinha := FT_FREADLN()
	 
		If lPrim
			aCampos := Separa(cLinha,";",.T.)
			lPrim := .F.
		Else
			AADD(aDados,Separa(cLinha,";",.T.))
		EndIf
	 
		FT_FSKIP()
	EndDo
	 
	Begin Transaction
		ProcRegua(Len(aDados))
		For i:=1 to Len(aDados)
	        
			IncProc("Importando Tabela de Pre�o...")
	 
			dbSelectArea("DA1")
			dbSetOrder(1)
		    dbGoTop()   
		    
            If VAL(aDados[i,6]) < VAL(aDados[i,7]) .And. i <= Len(aDados)
				AVISO(" V E R I F I C A R ", "O valor de 'Pre�o Venda' n�o pode ser maior que o valor de 'Pre�o Origem'!" + CRLF + CRLF +;
					  "C�digo do Produto: " +Alltrim(aDados[i,4]) + CRLF +;
					  "Pre�o de Origem  : " +aDados[i,6] + CRLF +;
					  "Pre�o de Venda   : " +aDados[i,7] + CRLF +;
					  CRLF, { "Sair" },3)
					  Return
            End

			If dbSeek(xFilial("DA1")+aDados[i,2]+aDados[i,4])
				Reclock("DA1",.F.)
				DA1->DA1_XPRCOR := VAL(aDados[i,6])
			    DA1->DA1_PRCVEN := VAL(aDados[i,7])
				DA1->DA1_VLRDES := VAL(aDados[i,8])
				DA1->DA1_DATVIG := StoD(aDados[i,9])
				DA1->(MsUnlock())
			Else
				BeginSQL Alias _cAliasTRB
					SELECT MAX(DA1_ITEM) MAX_ITEM FROM %table:DA1% AS DA1 WHERE DA1.%notDel%
					AND DA1_CODTAB = %exp:aDados[i,2]%
				 EndSQL
				
				DbSelectArea(_cAliasTRB)
				(_cAliasTRB)->(dbGoTop())
				
				Reclock("DA1",.T.)
				DA1->DA1_FILIAL := xFilial("DA1")
				DA1->DA1_CODTAB := aDados[i,2]
				DA1->DA1_ITEM   := PADL((VAL((_cAliasTRB)->MAX_ITEM)+1),5,"0")
				DA1->DA1_CODPRO := Transform(aDados[i,4], PesqPict("DA1","DA1_CODPRO"))
				DA1->DA1_XPRCOR := VAL(aDados[i,6])
				DA1->DA1_PRCVEN := VAL(aDados[i,7])
				DA1->DA1_VLRDES := VAL(aDados[i,8])
				DA1->DA1_ATIVO  := "1"
				DA1->DA1_TPOPER := "4"
				DA1->DA1_QTDLOT := VAL("999999.99")
				DA1->DA1_MOEDA := VAL("1")
				DA1->DA1_DATVIG := StoD(aDados[i,9])
				DA1->DA1_PRCMAX := VAL("000")
				DA1->(MsUnlock())
				(_cAliasTRB)->(dbCloseArea())
			EndIf
		Next i
	End Transaction
	 
	FT_FUSE()
	          
		AVISO("C O N C L U � D O ", "Importa��o da Tabela de Pre�o conclu�da com sucesso!" + CRLF +;
		            "Verifique a Tabela para confer�ncia da importa��o dos dados.", { "OK" }, 1)
 

Return


/*
������������������������������������������������������������������������������
��� Programa  ExpTPreco   Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function ExpTPreco()

Local lRet 		:= .T.
Local aEstrut	:= {}
Local cNomArq	:= "TABELA_PRECO-"+Alltrim(DA0->DA0_CODTAB)+"-"+Alltrim(DA0->DA0_DESCRI)+".CSV"
Local cAliasTmp	:= GetNextAlias()
Local cDest 	:= GetTempPath()
Local nHdl 		:= 0
Local cAliasTRB	:= GetNextAlias()
Local oExcelApp

// Estrutura do arquivo temporario
aAdd( aEstrut, { "DA1_FILIAL"	,"C", TamSx3("DA1_FILIAL")[1], 0 } )
aAdd( aEstrut, { "DA1_CODTAB"	,"C", TamSx3("DA1_CODTAB")[1], 0 } )
aAdd( aEstrut, { "DA0_XCODAN"	,"C", TamSx3("DA1_CODTAB")[1], 0 } )
aAdd( aEstrut, { "DA1_CODPRO"	,"C", TamSx3("DA1_CODPRO")[1], 0 } )
aAdd( aEstrut, { "B1_DESC"	    ,"C", TamSx3("B1_DESC")[1], 0 } )
aAdd( aEstrut, { "DA1_GRUPO"	,"C", TamSx3("DA1_GRUPO")[1], 0 } )
aAdd( aEstrut, { "DA1_XPRCOR"	,"N", TamSx3("DA1_XPRCOR")[1], 0 } )
aAdd( aEstrut, { "DA1_PRCVEN"	,"N", TamSx3("DA1_PRCVEN")[1], 0 } )
aAdd( aEstrut, { "DA1_VLRDES"	,"N", TamSx3("DA1_VLRDES")[1], 0 } )
aAdd( aEstrut, { "DA1_DATVIG"	,"C", TamSx3("DA1_DATVIG")[1]+2, 0 } )

// Cria o arquivo temporario
cNomeArq := CriaTrab( aEstrut, .T. )
dbUseArea( .T.,,cNomeArq, cAliasTmp, .F., .F. )

IndRegua( cAliasTmp, cNomeArq, "DA1_FILIAL+DA1_CODTAB",,,"Criando Indice, Aguarde..." )
dbClearIndex()
dbSetIndex( cNomeArq + OrdBagExt() )

// Monta nome do arquivo e diretorio onde sera gravado.
cDest	:= Iif( Right(cDest,1)=="\",cDest,cDest+"\" )

MakeDir(Left(cDest,Len(cDest)-1))
If File(cDest + cNomArq)
	If !(MsgYesNo("O arquivo '"+cDest+cNomArq+"' j� existe. Sobrescrever?","Cria��o do Arquivo"))
		// Apaga o TMP
		dbSelectArea( cAliasTmp )
		dbCloseArea()
		FErase( cNomeArq + ".DBF" )
		FErase( cNomeArq + OrdBagExt() )
		Return(lRet)
	Endif
Endif

nHdl := FCreate( cDest + cNomArq )

If nHdl < 0
	MsgStop("Nao foi possivel criar o arquivo '"+cDest+cNomArq+"'.","Cria��o do Arquivo")
	// Apaga o TMP
	dbSelectArea( cAliasTmp )
	dbCloseArea()
	FErase( cNomeArq + ".DBF" )
	FErase( cNomeArq + OrdBagExt() )
	Return(lRet)
EndIf

// Exporta Cabecalho do arquivo
FWrite(nHdl, "DA1_FILIAL; DA1_CODTAB; DA0_XCODAN; DA1_CODPRO; B1_DESC; DA1_XPRCOR; DA1_PRCVEN; DA1_VLRDES; DA1_DATVIG")
FWrite(nHdl, CRLF)

// Gera��o dos dados no arquivo de trabalho, de acordo com os par�metros
If Select(cAliasTRB) > 0
	(cAliasTRB)->(dbCloseArea())
Endif

BeginSQL Alias cAliasTRB
	SELECT * FROM %table:DA0% AS DA0
	INNER JOIN %table:DA1% AS DA1 ON DA0_FILIAL = DA1_FILIAL AND DA0_CODTAB = DA1_CODTAB AND DA1.%notDel%
	INNER JOIN %table:SB1% AS SB1 ON DA1_CODPRO = B1_COD AND SB1.%notDel%
	WHERE DA0.%notDel% AND DA0_CODTAB = %exp:DA0->DA0_CODTAB%
	ORDER BY DA1_ITEM
 EndSQL

DbSelectArea(cAliasTRB)
(cAliasTRB)->(dbGoTop())

If !Eof(cAliasTRB)

	While !Eof(cAliasTRB)
		// Grava a linha do Detalhe
		FWrite(nHdl, chr(160)+(cAliasTRB)->DA1_FILIAL + ";" + ;
					 chr(160)+(cAliasTRB)->DA1_CODTAB + ";" + ;
					 chr(160)+(cAliasTRB)->DA0_XCODAN + ";" + ;
					 chr(160)+(cAliasTRB)->DA1_CODPRO + ";" + ;
					 (cAliasTRB)->B1_DESC + ";" + ;
					 Transform((cAliasTRB)->DA1_XPRCOR, PesqPict("DA1","DA1_XPRCOR")) + ";" + ;
					 Transform((cAliasTRB)->DA1_PRCVEN, PesqPict("DA1","DA1_PRCVEN")) + ";" + ;
					 Transform((cAliasTRB)->DA1_VLRDES, PesqPict("DA1","DA1_VLRDES")) + ";" + ;
					 (cAliasTRB)->DA1_DATVIG )
		
		FWrite(nHdl, CRLF)
		
		(cAliasTRB)->(dbSkip())
	End

Else
	AVISO(" TABELA VAZIA ", "N�o existem Produtos para esta Tabela de Pre�o. Para exporta��o da Tabela � necess�rio que exista pelo menos um Produto cadastrado.", { "Sair" }, 1 )
Endif

FClose(nHdl)

// Apaga o TMP
DbCloseArea("cAliasTmp")
FErase( cNomeArq + ".DBF" )
FErase( cNomeArq + OrdBagExt() )
 
If (ApOleClient("MSExcel"))
	oExcelApp := MsExcel():New() 
	oExcelApp:WorkBooks:Open( cDest+cNomArq ) // Abre uma planilha 
	oExcelApp:SetVisible(.T.) 
	
	AVISO(" E X P O R T A � � O ",	"Se o objetivo desta exporta��o for alterar ou inserir novos itens na Tabela de Pre�o para posterior importa��o no sistema, siga as orienta��es abaixo:" + CRLF + CRLF +;
						"1)  O cabe�alho do arquivo n�o pode ser alterado ou exclu�do, e os t�tulos das colunas devem ser mantidos." + CRLF +;
						"2)  Nenhuma coluna pode ser exclu�da." + CRLF +;
						"3)  Caso sejam inseridas novas colunas no arquivo que n�o fazem parte da sua estrutura exportada estas ser�o desconsideradas na importa��o." + CRLF +;
						"4)  A coluna 'Descri��o do Produto' foi exportada somente como informativa, porque ser� considerada a descri��o atual do cadastro do Produto." + CRLF +;
						CRLF, {"OK"},3)
	
	AVISO(" E X P O R T A � � O ", "Exporta��o conclu�da!", { "OK" }, 1)
Else
	AVISO(" E X C E L ", "O Excel n�o est� instalado nesse computador!" + CRLF +;
	"Acesse o diret�rio a seguir para copiar o arquivo:" + CRLF + CRLF +;
	cDest+cNomArq, { "Sair" }, 1)    
EndIf
	
Return(lRet)