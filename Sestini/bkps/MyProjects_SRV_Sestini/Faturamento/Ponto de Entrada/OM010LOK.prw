#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
������������������������������������������������������������������������������
��� Programa  OM010LOK   Autor  Rogerio Machado          Data 18/05/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function OM010LOK()

	Local lRet      := .F. 
	Local _cNum     := DA0->DA0_CODTAB 
	Local cTabMas   := GetMV("MV_XTABMAS")
	Local cUseMas   := GetMV("MV_XUSEMAS") 
	Local cUsrLog   := RetCodUsr()
	Local nPosOri 	:= aScan(aHeader,{|x| AllTrim(x[2]) == "DA1_XPRCOR"})
	Local nPosVen 	:= aScan(aHeader,{|x| AllTrim(x[2]) == "DA1_PRCVEN"})	
	
	If _cNum $ cTabMas
		If cUsrLog $ cUseMas
			lRet := .T.
		Else
			lRet := .F.
			AVISO("Usu�rio n�o autorizado", "Usu�rio n�o autorizado para alterar tabela de pre�o master. Clique em Fechar da tela principal!", { "Sair" }, 1)
			Return(lRet)
		EndIf
	EndIf
	
	If aCols[n][7] < aCols[n][9]
		lRet := .F.
		AVISO(" V E R I F I C A R ", "O valor de 'Pre�o Origem' n�o pode ser maior que o valor de 'Pre�o de Venda'!" + CRLF + CRLF +;
		  "C�digo do Produto: " +cValToChar(Alltrim(aCols[n][5])) + CRLF +;
		  "Pre�o de Origem  : " +cValToChar(aCols[n][7]) + CRLF +;
		  "Pre�o de Venda   : " +cValToChar(aCols[n][9]) + CRLF + CRLF, { "Sair" },1)
		Return(lRet)
	Else
		lRet := .T.
	EndIf

	
Return(lRet)