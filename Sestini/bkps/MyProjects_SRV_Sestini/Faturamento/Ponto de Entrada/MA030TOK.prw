#include "TOTVS.ch"
#include "rwmake.ch"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  MA030TOK    Autor  Rogerio Machado         Data 13/08/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function MA030TOK()
	Local lRet      := .T.
	Local cSequen   := "000001"
	Local cAliasDA7 := GetNextAlias()
		
	DbSelectarea("DA7")
	DA7->(DbSetorder(2))
	If DA7->(DbSeek(xFilial("DA7") + SA1->A1_COD + SA1->A1_LOJA))
		Reclock("DA7",.F.)
			DA7_PERCUR := SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)
			DA7_ROTA   := SA1->A1_XCANAL
			DA7_CLIENT := SA1->A1_COD
			DA7_LOJA   := SA1->A1_LOJA
		DA7->(MsUnlock())
	Else
		BeginSQL Alias cAliasDA7
			SELECT MAX(DA7_SEQUEN) AS DA7_SEQUEN FROM DA7010 WHERE D_E_L_E_T_ = '' AND DA7_PERCUR = %exp:SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)% AND DA7_ROTA = %exp:SA1->A1_XCANAL%
		EndSQL
		
		If (cAliasDA7)->(!EoF())
			cSequen := (cAliasDA7)->DA7_SEQUEN
		EndIf
		
		Reclock("DA7",.T.)
			DA7_PERCUR := SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)
			DA7_ROTA   := SA1->A1_XCANAL
			DA7_SEQUEN := SOMA1(cSequen)
			DA7_CLIENT := SA1->A1_COD
			DA7_LOJA   := SA1->A1_LOJA
		DA7->(MsUnlock())
	Endif
	
Return lRet