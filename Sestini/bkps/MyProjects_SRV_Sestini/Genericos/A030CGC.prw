User Function A030CGC

	Local     aAreaOld     := SA1->(GetArea())
	Local     cCNPJ        := M->A1_CGC
		 
	// For�o atualiza��o para Juridica se o numero de digitos for maior que 11
 
If INCLUI 		
	If Len(Alltrim(cCNPJ)) > 11
		 M->A1_PESSOA = "J"
	ElseIf Len(Alltrim(cCNPJ)) > 0
		 M->A1_PESSOA = "F"
	Else
		Return
	Endif
Endif	
	
	M->A1_COD        := LEFT( Alltrim(cCNPJ),8)
    M->A1_LOJA       := RIGHT(Alltrim(cCNPJ),4)

    RestArea(aAreaOld)

	
Return A030CGC(M->A1_PESSOA, M->A1_CGC)