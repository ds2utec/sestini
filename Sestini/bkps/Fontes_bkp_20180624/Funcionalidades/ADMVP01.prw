#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVP01
Programa responsavel pelas tratativas de distribuicao de cotas
@author DS2U (SDA)
@since 14/05/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@param [llIsJob], logical, Variavel para controlar se .T. processamento em JOB ou .F. Processamento com interface com usuario. Padrao = isBlind()
@type function
/*/
User Function ADMVP01( nlOpc, uParam, llIsJob )

	local uRet		:= nil

	private lpInJob	:= .F.	

	default nlOpc	:= 0
	default uParam	:= nil
	default llIsJob	:= isBlind()
	
	lpInJob := llIsJob
	
	do case
	
		case nlOpc == 0
			uRet := distCotas( uParam )
		
		case nlOpc == 1 // Mapa de distribuicao por armazem	
			uRet := getMapDist( uParam )
			
		case nlOpc == 2 // Adiciona item no modelo de transfer�ncia multipla e retorno o array pronto para ser adicionado no arrau aAuto do MATA261	
			uRet := adTransf( uParam )
		
		case nlOpc == 3	
			uRet := distBetLoc( uParam )
		
		case nlOpc == 4	
			uRet := chkSBE( uParam ) // Checa se o armazem + endereco existe, e caso nao exista cria o endereco no ERP
			
		case nlOpc == 5
			uRet := disCotaVir( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} JOBDCOTA
Reponsavel pela preparacao do ambiente para processamento do JOB de distribuicao automatica de cotas
@author sergi
@since 06/06/2018
@version 1.0
@param clEmp, characters, Codigo da empresa para abertura de ambiente
@param clFil, characters, Codigo da filial da empresa para abertura de ambiente
@type function
/*/
User Function JOBDCOTA( clEmp, clFil )

	local alTables := {"ZC0","ZC1","ZC2","ZC3","ZC4","SBE","SBF","SB1","SB2","SDA","SDB","SF1","SD1"}

	default clEmp	:= "99"
	default clFil	:= "01"
	
	conout( "Inicio o JOB - JOBDCOTA" )

	if ( .not. empty( clEmp ) .and. .not. empty( clFil ) )

		conout( "Processando para a empresa [" + clEmp + "] / filial [" + clFil + "]" )

		RpcSetType( 3 )
		RpcSetEnv( clEmp, clFil )//,,,"EST","JOBDCOTA", alTables )
	
		jobDisCota()
		
		RpcClearEnv()
		
	endif
	
	conout( "Fim o JOB - JOBDCOTA" )

Return

/*/{Protheus.doc} disCotaVir
Responsavel por calcular vistualmente a distribuicao de quantidades do produto enviado por parametro, sem realizar transferencias
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return alInfoEnd,	[1] Codigo do armazem
					[2] Codigo do endereco
					[3] Quantidade distribuida para o endereco
@param alParam, array of logical, descricao
@type function
/*/
Static Function disCotaVir( alParam )

	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clEndereco:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local nlQtdCalc	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "N", alParam[4], 0 )
	local clDoc		:= ""
	local clSerie	:= ""
	local clFornec	:= ""
	local clLoja	:= ""
	local clTipo	:= ""
	local llVirtual	:= .T.
	local alInfoLoc	:= {}
	local alInfoEnd	:= {}
	local nlPos		:= 0
	
	// Calcula virtualmente a distribuicao entre armazens
	// alInfo:	[1] Codigo do armazem
	//			[2] Quantidade distribuida para o armazem
	alInfoLoc := distCotas( {"", clProduto, clEndereco, "", clDoc, clSerie, clFornec, clLoja, clTipo, llVirtual, nlQtdCalc} )
	
	if ( len( alInfoLoc ) > 0 )
	
		// Encontra o armazem que deve ser pesquisado o endereco
		nlPos := aScan( alInfoLoc, {|x| x[1] == allTrim( clAlmoxTrf ) } )
	
		if ( nlPos > 0 )
	
			// Calcula virtualmente a distribuicao entre os enderecos
			// alInfoEnd:	[1] Codigo do armazem
			//				[2] Codigo do endereco
			//				[3] Quantidade distribuida para o endereco
			alInfoEnd := distCotas( { clAlmoxTrf, clProduto, clEndereco, "", clDoc, clSerie, clFornec, clLoja, clTipo, llVirtual, alInfoLoc[nlPos][2] } )
			
		endif
		
	endif
	
Return alInfoEnd

/*/{Protheus.doc} distCotas
Responsavel por orquestrar os processos de distribuicao de cotas
@author DS2U (SDA)
@since 14/05/2018
@version 1.0
@param alParam, array of logical,	[1] Codigo do armazem
									[1] Codigo do produto
									[1] Codigo do endereco
									[1] Codigo do documento
									[1] Serie do documento
									[1] Codigo do fornecedor
									[1] Codigo da loja do fornecedor
									[1] Tipo do documento
									[1] Se � distribuicao virtual
									[1] Quantidade a ser distribuida virtualmente
@type function
/*/
Static Function distCotas( alParam )

	local alArea	:= getArea()
	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clEndereco:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clNumSeq	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clDoc		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )
	local clSerie	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 5 .and. valType( alParam[6] ) == "C", alParam[6], "" )
	local clFornec	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 6 .and. valType( alParam[7] ) == "C", alParam[7], "" )
	local clLoja	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 7 .and. valType( alParam[8] ) == "C", alParam[8], "" )
	local clTipo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 8 .and. valType( alParam[9] ) == "C", alParam[9], "" )
	local llVirtual	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 9 .and. valType( alParam[10] ) == "L", alParam[10], .F. )
	local nlQtdVirt	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 10 .and. valType( alParam[11] ) == "N", alParam[11], 0 )
	local uRet
	
	// Se NumSeq esta preenchido, entao a distribuicao sera feita conforme produtos a enderecar
	if ( .not. empty( clNumSeq ) )
	
		uRet := distBetSDA( {clAlmoxTrf, clProduto, clNumSeq} )
	
	// Se armazem esta vazio, entao a distribuicao sera feita entre os armazens do ADMV
	elseif ( empty( clAlmoxTrf ) )
	
		uRet := distBetLoc( {clDoc, clSerie, clFornec, clLoja, clTipo, llVirtual, nlQtdVirt} )
	
	// Se o armazem esta preenchido, entao a distribuicao sera feita entre os enderecos do armazen, obedecendo a hierarquia
	else

		uRet := distBetHie( clAlmoxTrf, clEndereco, clProduto, llVirtual, nlQtdVirt )

	endif
	
	restArea( alArea )
	
Return uRet

/*/{Protheus.doc} distBetHie
Responsavel por realizar a distribuicao entre os enderecos do armazem
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param clAlmox, characters, Codigo do armazem a ser distribuido
@param [clEndereco], characters, Codigo do endereco a ser considerado
@param [clProduto], characters, Codigo do produto a ser considerado
@type function
/*/
Static Function distBetHie( clAlmox, clEndereco, clProduto )

	local clFilAtu	:= FWxFilial( "SB2" )
	local clAlmoxTrf:= PADR( clAlmox, tamSX3("B2_LOCAL")[1] )
	local nlTamBFLoc:= tamSX3("BF_LOCAL")[1]
	local nlTamBFEnd:= tamSX3("BF_LOCALIZ")[1]
	local nlTamBFPrd:= tamSX3("BF_PRODUTO")[1]
	local nlQtDist	:= 0
	local llLocaliz := .F.
	
	default clEndereco	:= ""
	default clProduto	:= ""

	dbSelectArea( "SB2" )
	SB2->( dbSetOrder( 2 ) ) // B2_FILIAL, B2_LOCAL, B2_COD
	
	dbSelectArea( "SB1" )
	SB1->( dbSetOrder( 1 ) ) // B1_FILIAL, B1_COD
	
	if ( SB2->( dbSeek( clFilAtu + clAlmoxTrf + clProduto ) ) )
		
		// Identifica o endereco do armazem origem, conforme cadastro de cotas
		if ( empty( clEndereco ) )
			clEndereco := getEndOfLoc( clAlmoxTrf )[1]
		endif
		
		while (	.not. ( SB2->( eof() ) );
		 		.and. SB2->B2_FILIAL == clFilAtu;
		 		.and. SB2->B2_LOCAL == clAlmoxTrf;
		 		.and. ( empty( clProduto ) .or. SB2->B2_COD == clProduto );
			  )
			
			// Checa se o produto controla endereco  
			llLocaliz := U_ADMVXFUN( 8, SB2->B2_COD )
			  
			if ( llLocaliz )
			
				// Saldo disponivel no endereco
				nlQtDist := U_ADMVP02( 2, {clAlmoxTrf, clEndereco, SB2->B2_COD} )
				
				// Realiza a distribuicao do saldo para os armaz�ns e endere�os
				distSaldo( SB2->B2_COD, clAlmoxTrf, nlQtDist )
				
			endif
			  
			SB2->( dbSkip() )
		endDo
		
	else
		U_ADMVXFUN( 1, {"B2_LOCAL", "Nao foi encontrado o armazem [" + clAlmoxTrf + "] de transfer�ncia para iniciar a distribui��o de cotas" } )
	endif
	
Return

/*/{Protheus.doc} getEndOfLoc
Retorna o endereco do armazem informado
@author sergi
@since 28/05/2018
@version 1.0
@return array,	[1] endereco do % de cotas
				[2] endereco do % retido
				
@param clAlmoxTrf, characters, Codigo do armazem a ser pesquisado
@type function
/*/
Static Function getEndOfLoc( clAlmoxTrf )

	local clAlias	:= getNextAlias()
	local alEndereco:= {}
	local clEndCot	:= ""
	local clEndRet	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZC0_ID AS ID
			
		FROM
			%TABLE:ZC0% ZC0
			
		WHERE
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0_ALMOX = %EXP:clAlmoxTrf%
			AND ZC0_CODPAI = ' '
			AND ZC0.%NOTDEL%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		alEndereco := U_ADMVXFUN( 7, ( clAlias )->ID ) // Gera ID do endereco de % Cota e % retido
		clEndCot := alEndereco[1]
		clEndRet := alEndereco[2]
	endif
	( clAlias )->( dbCloseArea() )

Return { clEndCot, clEndRet }

/*/{Protheus.doc} distBetLoc
Reponsavel por distribuir as quantidades entre os armazens do Admv
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param clProduto, characters, Codigo do produto a distribuir
@param nlSaldoLoc, numeric, Saldo a distribuir
@type function
/*/
Static Function distBetLoc( alParam )

	local clDoc		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clSerie	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clFornec	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clLoja	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clTipo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" ) 
	
	default alParam	:= {}

	if ( .not. enderecar( clDoc, clSerie, clFornec, clLoja, clTipo ) )
		U_ADMVXFUN( 1, {"DB_LOCALIZ", "Enderecamento entre armazens esta incosistente. Avaliar os percentuais a serem distribuidos!" } )
	endif
			
Return

/*/{Protheus.doc} distBetSDA
Responsavel por processar enderecamento automatico conforme itens a enderecar
@author DS2U (SDA)
@since 06/06/2018
@version 1.0
@param alParam, array of logical,	[1] Codigo do armazem
									[2] Codigo do produto
									[3] NumSeq do registro na SDA
									[4] Endereco a ser utilizado para realizar o endereco automatico. Se nao for informado sera considerado o parametro ES_ENDTRAN
@type function
/*/
Static Function distBetSDA( alParam )

	local clArmazem		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clProduto		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clNumSeq		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clLocaliz		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], allTrim( getMv( "ES_ENDTRAN",,"A_TRANSFERIR" ) ) )
	local alArea	:= getArea()
	
	dbSelectArea( "SDA" )
	SDA->( dbSetOrder( 3 ) ) // DA_FILIAL, DA_PRODUTO, DA_LOCAL, DA_NUMSEQ, DA_DOC, DA_SDOC, DA_CLIFOR, DA_LOJA
	
	if ( SDA->( dbSeek( FWxFilial( "SDA" ) + PADR( clProduto, tamSX3("DA_PRODUTO")[1] ) + PADR( clArmazem, tamSX3("DA_LOCAL")[1] ) + PADR( clNumSeq, tamSX3("DA_NUMSEQ")[1] ) ) ) )
		
		if ( SDA->DA_SALDO > 0 )
			
			// Verifica se o endereco ja existe para o almoxafirado. Caso nao exista, sera criado
			chkSBE( {SDA->DA_LOCAL, clLocaliz, "A TRANSFERIR"} )
			
			// Realiza o endereco automatico
			buildEnder( SDA->DA_LOCAL, clLocaliz, SDA->DA_PRODUTO, SDA->DA_NUMSEQ, SDA->DA_SALDO, SDA->DA_DOC, SDA->DA_SERIE, SDA->DA_CLIFOR, SDA->DA_LOJA, SDA->DA_TIPONF, SDA->DA_LOTECTL )
			
		endif
		
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} enderecar
Reponsavel por posicionar as tabelas e chamar o processamento de enderecamento automatico
@author DS2U (SDA)
@since 28/05/2018
@version 1.0
@param clDoc, characters, Nr. da nota de entrada
@param clSerie, characters, Serie da nota de entrada
@param clFornec, characters, Fornecedor da nota de entrada
@param clLoja, characters, Loja do Fornecedor da nota de entrada
@param clTipo, characters, Tipo da nota de entrada
@type function
/*/
Static Function enderecar( clDoc, clSerie, clFornec, clLoja, clTipo )
 
	local alArea	:= getArea()
	local llRet		:= .F.
	local clLocaliz	:= ""
	local clAlias	:= ""
	local llLocaliz	:= .F.
	local nlReg		:= 0
	local alAreaD1	:= {}
	
	dbSelectArea("SF1")
	SF1->( dbSetOrder( 1 ) ) // F1_FILIAL + F1_DOC + F1_SERIE + F1_FORNECE + F1_LOJA
	
	if ( SF1->( dbSeek( FWxFilial( "SF1" ) + clDoc + clSerie + clFornec + clLoja + clTipo ) ) )
	
		if ( SF1->F1_TIPO == "N" )
		
			dbSelectArea("SD1")
			SD1->( dbSetOrder( 1 ) ) // D1_FILIAL + D1_DOC + D1_SERIE + D1_FORNECE + D1_LOJA
			
			dbSelectArea("SB1")
			SB1->( dbSetOrder( 1 ) )
			
			if ( SD1->( dbSeek( FWxFilial("SD1") + SF1->F1_DOC + SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA ) ) )
			
				llRet		:= .T.
				clLocaliz	:= allTrim( getMv( "ES_ENDTRAN",,"A_TRANSFERIR" ) )
			
				while	(	.not. SD1->( eof() );
				 			.and. SD1->D1_FILIAL == SF1->F1_FILIAL;
				 			.and. SD1->D1_DOC == SF1->F1_DOC;
				 			.and. SD1->D1_SERIE == SF1->F1_SERIE; 
				 			.and. SD1->D1_FORNECE == SF1->F1_FORNECE; 
				 			.and. SD1->D1_LOJA == SF1->F1_LOJA;  
						)
						
					if ( SB1->( dbSeek( FWxFilial( "SB1" ) + PADR( SD1->D1_COD, tamSX3("B1_COD")[1] ) ) ) )
						
						// Checa se o produto controla endereco  
						llLocaliz := U_ADMVXFUN( 8, SB2->B2_COD )
						
						/**
						  * Checa se o produto tem rastreamento habilitado 
						  * para realizar procedimento de enderecamento automatico
						  */
						if ( llLocaliz )
						
							// Flag que identifica se o item da nota ja foi para o endereco a transferir
							if ( .not. ( SD1->D1_XDISTOK == "S" ) )
						
								// Verifica se o almoxafirado j� existe para o produto
								// Esta fora do begin transaction pois precisa existir o registro na SBE para continuar as transferencias
								// TODO VERIFICAR SE ESTE ARMAZEM ESTARA CORRETO SE TIVER O MODULO DE INSPECAO DE ENTRADA
								chkSBE( {SD1->D1_LOCAL, clLocaliz, "A TRANSFERIR"} )
							
								nlReg := SD1->(Recno())
								alAreaD1 := SD1->( getArea() )
							
								// Realiza enderecamento automatico
								begin transaction
								
									if ( buildEnder( SD1->D1_LOCAL, clLocaliz, SD1->D1_COD, SD1->D1_NUMSEQ, SD1->D1_QUANT, SD1->D1_DOC, SD1->D1_SERIE, SD1->D1_FORNECE, SD1->D1_LOJA, SD1->D1_TIPO, SD1->D1_LOTECTL, SD1->D1_NUMLOTE ) )
										
										llRet := .T.
										
										if ( recLock( "SD1", .F. ) )
											SD1->D1_XDISTOK := "S"
											SD1->( msUnLock() )
										endif
										
									else
										llRet := .F.
										disarmTransaction()
									endif
									
								end transaction
								
								restArea( alAreaD1 )
								SD1->( dbGoto( nlReg ) )
								
							endif
							
							clAlias	:= getMapDist( {"", .F., .T.} ) // Buscao os armazens do ADMV
							
							// Cria SB2 nos armazens para o produto em questao, caso ainda nao exista
							( clAlias )->( dbGoTop() )
							
							while ( .not. ( clAlias )->( eof() ) )
							
								// Verifica se o almoxafirado j� existe para o produto
								chkSB2( SD1->D1_COD, ( clAlias )->ALMOXARIF )
								
								( clAlias )->( dbSkip() )
							endDo
							
							( clAlias )->( dbGoTop() )
	
							// Realiza transfer�ncia entre armaz�ns / endere�os de tudo que existe no endereco A TRANSFERIR
							begin transaction
							
								llRet := ( llRet .and. endBetLoc( SD1->D1_LOCAL, SD1->D1_COD, clLocaliz, clAlias ) )
								
								if ( .not. llRet )
									disarmTransaction()
								endif
								
							end transaction
							
						endif
						
					endif
						
					SD1->( dbSkip() )
				endDo
				
			endif
		
		endif
		
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} endBetLoc
Responsavel por realizar transferencias entre armazens conforme o cadastro de cotas
@author DS2u (SDA)
@since 18/05/2018
@version undefined
@return llRet, .T. = Permite continuacao do processamento se nao houve erros no processamento ou .F. = Nao permite continuacao do processamento se ouve erros no processamento
@param clAlmox, characters, Codigo do armazem origem a ser distribuido
@param clEndereco, characters, Codigo do endereco origem a ser distribuido
@param clProduto, characters, Codigo do produto
@param [clAlias], characters, Alias do arquivo de trabalho da View de consulta do mapa de distribuicao entre armazens. Deve ser passado por parametro quando esta em begin transaction
@type function
/*/
Static Function endBetLoc( clAlmox, clProduto, clEndereco, clAlias )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clFilAtu	:= FWxFilial( "SB2" )
	local clAlmoxTrf:= PADR( clAlmox, tamSX3("B2_LOCAL")[1] )
	local nlQtDisp	:= 0

	private	lMsErroAuto := .F.
	
	default clEndereco	:= allTrim( getMv( "ES_ENDTRAN",,"A_TRANSFERIR" ) ) // Identifica o endereco do armazem origem, conforme cadastro de cotas
	default clAlias		:= getMapDist( {"", .F., .T.} ) // Buscao os armazens do ADMV
	
	dbSelectArea( "SB2" )
	SB2->( dbSetOrder( 2 ) ) // B2_FILIAL, B2_LOCAL, B2_COD
	
	if ( SB2->( dbSeek( clFilAtu + clAlmoxTrf + clProduto ) ) )
		
		while (	.not. ( SB2->( eof() ) );
		 		.and. SB2->B2_FILIAL == clFilAtu;
		 		.and. SB2->B2_LOCAL == clAlmoxTrf;
		 		.and. SB2->B2_COD == clProduto;
			  )
			
			// Saldo disponivel no endereco
			nlQtDisp := U_ADMVP02( 2, {clAlmoxTrf, clEndereco, SB2->B2_COD} )
			
			if ( nlQtDisp > 0 )
	
				// Realiza a distribuicao do saldo para os armaz�ns e endere�os
				if ( .not. distSldLoc( SB2->B2_COD, clAlmoxTrf, nlQtDisp, clEndereco, clAlias ) )
					llRet := .F.
					exit
				endif
				
			endif
			  
			SB2->( dbSkip() )
		endDo
		
	else
		U_ADMVXFUN( 1, {"B2_LOCAL", "Nao foi encontrado o armazem [" + clAlmoxTrf + "] de transfer�ncia para iniciar a distribui��o de cotas" } )
	endif
		
Return llRet

/*/{Protheus.doc} distSldLoc
Responsavel pela distribuicao de cotas entre armazens
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return llRet, .T. = Distribuicao entre armazens ok / .F. = Houve falhas na distribuicao
@param clProduto, characters, descricao
@param clAlmoxTrf, characters, descricao
@param nlQtTransf, numeric, descricao
@param clEndOri, characters, descricao
@param [clAlias], characters, Alias do arquivo de trabalho da View de consulta do mapa de distribuicao entre armazens. Deve ser passado por parametro quando esta em begin transaction
@type function
/*/
Static Function distSldLoc( clProduto, clAlmoxTrf, nlQtTransf, clEndOri, clAlias )

	local alArea	:= getArea()
 	local clLocDest	:= ""
	local clAlmox	:= ""
	local clId		:= ""
	local nlTamEnder:= tamSX3("BE_LOCALIZ")[1]
	local clLocaliz	:= ""
	local alItem	:= {}
	local alAuto	:= {}
	local alQtPercs	:= {}
	local nlQtdToCot:= 0
	local nlQtdToRet:= 0
	local llRet		:= .F.
	
	default clProduto	:= ""
	default clAlmoxTrf	:= ""
	default nlQtTransf	:= 0
	default clAlias	:= getMapDist( {"", .F., .T.} ) // Buscao os armazens do ADMV
	
	if ( .not. empty( clAlias ) )
	
		while ( .not. ( clAlias )->( eof() ) )
		
			clId := ( clAlias )->ID
		
			// Armazem
			clLocDest := PADR( ( clAlias )->ALMOXARIF, tamSX3("BF_LOCAL")[1] )
			
			// Endereco % Cota
			clLocaliz	:= PADR( allTrim( U_ADMVXFUN( 7, clId )[1] ), nlTamEnder )
			
			// Calcula as quantidades a serem transferidas
			alQtPercs	:= calcQtsPer( nlQtTransf, ( clAlias )->ID, clProduto )
			nlQtdToCot	:= alQtPercs[1]
			nlQtdToRet	:= alQtPercs[2]
			
			if ( nlQtdToCot > 0 )
			
				clEndAte := U_ADMVXFUN( 7, clId )[1] // Gera ID do endereco de % Cota
				
				// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
				alItem := adTransf( { nlQtdToCot, clProduto, clAlmoxTrf, clLocDest, clEndOri, clEndAte, "DISTR. % COTAS ENTRE ARMAZENS"} )
				AADD( alAuto, alItem )
				
			endif
			
			if ( nlQtdToRet > 0 )
			
				clEndAte := U_ADMVXFUN( 7, clId )[2] // Gera ID do endereco de % Retido
				
				// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
				alItem := adTransf( { nlQtdToRet, clProduto, clAlmoxTrf, clLocDest, clEndOri, clEndAte, "DISTR. % RETIDO ENTRE ARMAZENS"} )
				AADD( alAuto, alItem )
				
			endif
			
		( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )
		
		// Monta dados e executa a transferencia multipla
		llRet := execTransf( alAuto )
		
	else
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_CODPAI", "Falha na identificacao dos armazens do ADMV!" } )
	endif
			
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} buildEnder
Responsavel por montar a estrutura de dados para enderecamento automatico conforme item da SD1 posicionado
@author DS2U (SDA)
@since 28/05/2018
@version 1.0
@return llRet, .T. = Permite continuacao do processamento se nao houve erros no processamento ou .F. = Nao permite continuacao do processamento se ouve erros no processamento
@type function
/*/
Static Function buildEnder( clArmazem, clLocaliz, clProduto, clNumSeq, nlQuant, clDoc, clSerie, clFornec, clLoja, clTipo, clLote, clNumLote )
			    
  	local alCab		:= {}
  	local alItem	:= {}
  	local alItens	:= {}
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local llRet		:= .T.
	local clItem	:= ""
	
  	private lMSErroAuto	:= .F.
  	private lAutoErrNoFile := .T.
  	
  	default clArmazem	:= ""
  	default clLocaliz	:= allTrim( getMv( "ES_ENDTRAN",,"A_TRANSFERIR" ) )
  	default clProduto	:= ""
  	default clNumSeq	:= ""
  	default clDoc		:= ""
  	default clSerie		:= ""
  	default clFornec	:= ""
  	default clLoja		:= ""
  	default clTipo		:= ""
  	default nlQuant		:= 0
  	default clLote		:= ""
  	default clNumLote 	:= ""
  
	// Verifica se o almoxafirado j� existe para o produto
	chkSB2( clProduto, clArmazem )
	
	dbSelectArea("SBE")
	SBE->( dbSetorder( 1 ) )
	
	if ( SBE->( dbSeek( FWxFilial("SBE") + clArmazem + clLocaliz  ) ) )

		alCab	  := {	{"DA_PRODUTO"	, clProduto			, NIL},;
				  		{"DA_LOCAL"		, clArmazem			, NIL},;
				 		{"DA_NUMSEQ"	, clNumSeq			, NIL},;
						{"DA_DOC"		, clDoc				, NIL},;
						{"DA_SERIE"		, clSerie			, NIL},;
						{"DA_CLIFOR"	, clFornec			, NIL},;
						{"DA_LOJA"		, clLoja			, NIL},;
						{"DA_TIPONF"	, clTipo			, NIL},;
						{"DA_ORIGEM"	, iif( empty( clDoc ), 'SDA', 'SD1' ), NIL}	}
		
		clItem := getItemSDB( clArmazem, clProduto, clNumSeq )
		
		alItens := {{	{"DB_ITEM"		, clItem			, NIL},;
						{"DB_PRODUTO"	, clProduto			, NIL},;
						{"DB_LOCAL"		, clArmazem			, NIL},;
						{"DB_LOCALIZ"	, clLocaliz			, NIL},;
						{"DB_QUANT"		, nlQuant			, NIL},;
						{"DB_DATA"		, dDataBase			, NIL},;
						{"DB_LOTECTL"	, clLote			, NIL},;
						{"DB_NUMLOTE"	, clNumLote		 	, NIL},;
						{"DB_NUMSEQ"	, clNumSeq		 	, NIL},;
						{"DB_HRINI"		, time() 			, NIL}}}

		MSExecAuto({|x,y,z| MATA265(x,y,z)}, alCab, alItens, 3)
	
		if ( lMSErroAuto )
		
			llRet := .F.
			aEval( getAutoGRLog(), {|x| clMsgErro += x } )
			
			if ( .not. empty( clMsgErro ) )
				U_ADMVXFUN( 1, {"DB_LOCALIZ", clMsgErro } )
			endif
			
		else
			conout("Enderecamento automatico realizado com sucesso do produto [" + allTrim( clProduto ) + "] / Armazem [" + clArmazem + "]")
		endif
	
	else
	
		llRet := .F.
		U_ADMVXFUN( 1, {"DB_LOCALIZ", "N�o foi encontrado o endereco [" + clLocaliz + "] para a realiza��o das transfer�ncias entre armaz�ns!" } )
		
	endif
	
Return llRet

/*/{Protheus.doc} getItemSDB
Responsavel por buscar o ultimo item do enderecamento
@author DS2U (SDA)
@since 06/06/2018
@version 1.0
@return clItem, Proximo item a ser considerado no enderecamente automatico
@param clArmazem, characters, Codigo do armazem a ser consultado o enderecamento multiplo
@param clProduto, characters, Codigo do produto a ser consultado o enderecamento multiplo
@param clNumSeq, characters, Codigo do NumSeq a ser consultado o enderecamento multiplo
@type function
/*/
Static Function getItemSDB( clArmazem, clProduto, clNumSeq )
		
	local clItem	:= ""
	local nlTamItem	:= tamSX3("DB_ITEM")[1]
	local clAlias	:= getNextAlias()
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			MAX( DB_ITEM ) AS DB_ITEM
			 
		FROM 
			%TABLE:SDB% SDB
			 
		WHERE 
			SDB.DB_FILIAL = %XFILIAL:SDB%
			AND SDB.DB_PRODUTO = %EXP:clProduto%
			AND SDB.DB_LOCAL = %EXP:clArmazem%
			AND SDB.DB_NUMSEQ = %EXP:clNumSeq%
			AND SDB.%NOTDEL%	
	
	ENDSQL

	if ( ( clALias )->( eof() ) )
		clItem := strZero(1, nlTamItem)
	else
		clItem := soma1( ( clAlias )->DB_ITEM )
	endif
	( clALias )->( dbCloseArea() )

Return clItem

/*/{Protheus.doc} chkSB2
Funcao auxilizar para checar e criar, caso nao exista, almoxarifado para o produto

@author DS2u (SDA)
@since 18/05/2018
@version undefined
@param clProduto, characters, codigo do produto
@param clAlmox, characters, codigo do almoxarifado
@type function
/*/
Static Function chkSB2( clProduto, clAlmox )

	local alArea	:= getArea()
	
	dbSelectArea("SB2")
	SB2->( dbSetOrder( 1 ) )
	
	if ( .not. SB2->( dbSeek( xFilial("SB2") + clProduto + clAlmox ) ) )
		criaSB2(clProduto, clAlmox)
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} chkSBE
Checa se o armazem + endereco existe, e caso nao exista cria o endereco no ERP
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return llRet, .T. = O endereco ja existe ou foi criado com sucesso / .F. = Nao foi possivel encontrar / criar o endereco 
@param alParam, array of logical,	[1] Codigo do armazem
									[2] Codigo do endereco
									[3] Descricao do endereco
									[4] Parametro logico: .T. = Mostra mensagem de erro caso ocorra, .F. = Nao mostra mensagem de erro caso ocorra
@type function
/*/
Static Function chkSBE( alParam )

	local alArea	:= getArea()
	local llRet		:= .F.
	local clMsgErro	:= ""
	local clLogErro	:= ""
	local clArqErro	:= ""
	local clArmazem	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clLocaliz	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clDesc	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local llShowErro:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "L", alParam[4], .F. )

	private lMsErroAuto := .F.
	
	dbSelectArea("SBE")
	SBE->( dbSetorder( 1 ) )
	
	if ( SBE->( dbSeek( FWxFilial("SBE") + PADR( clArmazem, tamSX3("BE_LOCAL")[1] ) + PADR( clLocaliz, tamSX3("BE_LOCALIZ")[1] ) ) ) )
		llRet := .T.
	else
	
		// Somente cria um novo se os parametros obrigatorios estiverem preenchidos
		if ( .not. empty( clArmazem ) .and. .not. empty( clLocaliz ) .and. .not. empty( clDesc ) )
	
			alVetor := 	{	{"BE_LOCAL"  	,clArmazem	,nil},;				
							{"BE_LOCALIZ"	,clLocaliz	,nil},;				
							{"BE_DESCRIC"	,clDesc		,nil},;
							{"BE_PRIOR"		,"ZZZ"		,nil},;				
							{"BE_STATUS"	,"1"		,nil} }
							
			MSExecAuto(  {|x,y| MATA015(x,y)}, alVetor, 3 )
			
			if ( lMsErroAuto )
				
				llRet := .F.
				clMsgErro := "Falha na inclus�o automatica de endere�o. Entrar em contato com o T.I.!"
				
				clArqErro  := NomeAutoLog()
				if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
					clLogErro += memoRead( clArqErro )
					memoWrite(clArqErro," ") // Limpa log
				endif
				
				if ( .not. empty( clMsgErro ) )
					
					U_ADMVXFUN( 1,	{"ZC0_ALMOX", clMsgErro + CRLF + clLogErro } )
					
					if ( llShowErro )
						EECVIEW( clLogErro, "LOG DE ERROS IDENTIFICADOS" )
					endif
					
				endif
				
			else
				llRet := .T.
			endif
			
		endif
	
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} distSaldo
Responsavel por controlar as regras de distribuicao de cotas
@author DS2u (SDA)
@since 18/05/2018
@version 1.0
@param clAlmoxTrf, characters, Codigo do armazem/almoxarifado
@param nlSaldoDist, numeric, Qquantidade do saldo a ser distribuido
@type function
/*/
Static Function distSaldo( clProduto, clAlmoxTrf, nlSaldoDist )

	local alArea	:= getArea()
	local clAlias	:= ""
	local alQtPercs	:= {}
	local nlQtdToCot:= 0
	local nlQtdToRet:= 0
	local nlSaldo	:= 0
	local nlSaldoFim:= 0
	local clEndAte	:= ""
	local clEndDe	:= ""
	local clEndRtPai:= ""
	local nlTamBFLoc:= 0
	local nlTamBFEnd:= 0
	local nlTamBFPrd:= 0
	local llTransfOK:= .F.
	local llFirst	:= .T.
	local alItem	:= {}
	local alAuto	:= {}
	local nlQtDisp	:= 0
	
	default clProduto	:= ""
	default clAlmoxTrf	:= ""
	default nlSaldoDist	:= 0
	
	// Busca o mapa de distribuicao de cotas a ser processado
	clAlias	:= getMapDist( {clAlmoxTrf} )
	
	if ( .not. empty ( clAlias ) )
	
		nlTamBFLoc:= tamSX3("BF_LOCAL")[1]
		nlTamBFEnd:= tamSX3("BF_LOCALIZ")[1]
		nlTamBFPrd:= tamSX3("BF_PRODUTO")[1]
		
		// Saldo inicial a ser distribuido
		nlSaldo		:= nlSaldoDist
		nlSaldoFim	:= nlSaldo
		
		while ( .not. ( clAlias )->( eof() ) )
		
			llTransfOK	:= .F.
			clEndDe		:= U_ADMVXFUN( 7, ( clAlias )->NIVEL )[1] // Gera ID do endereco de % Cota
			clEndRtPai	:= U_ADMVXFUN( 7, ( clAlias )->NIVEL )[2] // Gera ID do endereco de % Retido
			
			// Saldo disponivel no endereco
			nlQtDisp := U_ADMVP02( 2, {clAlmoxTrf, clEndDe, clProduto} )
			
			// Verifica se endere�o origem existe e se o saldo a transferir solicitado tamb�m existe
			if ( nlQtDisp > 0 )
			
				// Se for a primeira interacao, apenas valida o saldo
				// Se nao for a primeira interacao, valida se existe saldo e parametriza as variaveis para a realiza��o das transfer�ncias de cotas
				if ( .not. llFirst )

					// Saldo inicial a ser distribuido
					nlSaldo		:= nlQtDisp
					nlSaldoFim	:= nlSaldo
				
				endif
				
				if ( nlQtDisp < nlSaldo )
					llTransfOK := .F.
					U_ADMVXFUN( 1, {"BF_LOCALIZ", "Endere�o [" + clEndDe + "] sem saldo suficiente para transfer�ncia. Saldo solicitado a transferir: " + allTrim( transform( nlSaldo, pesqPict("SBF", "BF_QUANT") ) ) + " / Saldo disponivel: " + allTrim( transform( nlQtDisp, pesqPict("SBF", "BF_QUANT") ) ) } )
				else
					llTransfOK := .T.
				endif
				
			endif
			
			if ( llTransfOK )
			
				llFirst := .F.
				
				clCodPai := ( clAlias )->NIVEL
			
				// Enquanto for distribuicao do mesmo nivel, armazena as informa��es dos itens para realizar transfer�ncia multipla
				while ( .not. ( clAlias )->( eof() ) .and. ( clAlias )->NIVEL == clCodPai )
				
					// Calcula as quantidades a serem transferidas
					alQtPercs	:= calcQtsPer( nlSaldo, ( clAlias )->ID, clProduto )
					nlQtdToCot	:= alQtPercs[1]
					nlQtdToRet	:= alQtPercs[2]
					
					if ( nlQtdToCot > 0 )
					
						nlSaldoFim -= nlQtdToCot
						clEndAte := U_ADMVXFUN( 7, ( clAlias )->ID )[1] // Gera ID do endereco de % Cota
						
						// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
						alItem := adTransf( { nlQtdToCot, clProduto, clAlmoxTrf, clAlmoxTrf, clEndDe, clEndAte, "DISTRIBUICAO DE % COTAS"} )
						AADD( alAuto, alItem )
						
					endif
					
					if ( nlQtdToRet > 0 )
					
						nlSaldoFim -= nlQtdToRet
						clEndAte := U_ADMVXFUN( 7, ( clAlias )->ID )[2] // Gera ID do endereco de % Retido
						
						// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
						alItem := adTransf( { nlQtdToRet, clProduto, clAlmoxTrf, clAlmoxTrf, clEndDe, clEndAte, "DISTRIBUICAO DE % RETIDO"} )
						AADD( alAuto, alItem )
						
					endif
					
					( clAlias )->( dbSkip() )
				endDo
				
				// Se ainda existe saldo a ser distribuido, envia para o endereco retido do pai no nivel corrente
				if ( nlSaldoFim > 0 )
					
					// Adiciona item a ser transferido na execauto de multiplas transferencias de enderecos
					alItem := adTransf( { nlSaldoFim, clProduto, clAlmoxTrf, clAlmoxTrf, clEndDe, clEndRtPai, "DISTR. % RETIDO - RESIDUO"} )
					AADD( alAuto, alItem )
					
				endif
				
				// Monta dados e executa a transferencia multipla
				execTransf( alAuto )
				
				alAuto := {}
				
			else
				llFirst := .F.
				( clAlias )->( dbSkip() )
			endif
			
		endDo
		( clAlias )->( dbCloseArea() )
		
	else
		U_ADMVXFUN( 1, {"ZC0_ALMOX", "Nao foi encontrado cadastro de cotas para o armazem [" + clAlmoxTrf + "]. Distribui��o de cotas foi interrompida!" } )
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} execTransf
Reponsavel por montar dados e executar transferencia multipla conforme itens enviados por parametro
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return llRet, .T. = transferencia multipla OK / .F. = houve falhas na transferencia multipla 
@param alItens, array of logical, Array no formato de execauto da rotina MATA261 >> Exemplo:

//Origem				
AADD( alItem, SB1->B1_COD )		// D3_COD		
AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
AADD( alItem, SB1->B1_UM )		// D3_UM		
AADD( alItem, clLocalOri )		// D3_LOCAL		
AADD( alItem, clEndOri  )		// D3_LOCALIZ

// Destino
AADD( alItem, SB1->B1_COD )		// D3_COD		
AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
AADD( alItem, SB1->B1_UM )		// D3_UM	
AADD( alItem, clLocalDes )		// D3_LOCAL		
AADD( alItem, clEndDes )		// D3_LOCALIZ		
AADD( alItem, "")				// D3_NUMSERI		
AADD( alItem, "")				// D3_LOTECTL  		
AADD( alItem, "")				// D3_NUMLOTE		
AADD( alItem, cToD("//") )		// D3_DTVALID		
AADD( alItem, 0 )				// D3_POTENCI		
AADD( alItem, nlQtTransf )		// D3_QUANT		
AADD( alItem, 0 )				// D3_QTSEGUM		
AADD( alItem, "" )				// D3_ESTORNO		
AADD( alItem, "" )				// D3_NUMSEQ 		
AADD( alItem, "" )				// D3_LOTECTL		
AADD( alItem, cToD("//") )		// D3_DTVALID		
AADD( alItem, "" )				// D3_ITEMGRD
AADD( alItem, "" )				// D3_IDDCF
AADD( alItem, clObsrv )			// D3_OBSERVA

@type function
/*/
Static Function execTransf( alItens )
				
	local clDoc		:= ""
	local alAuto	:= {}
	local nlOpcAuto	:= 3
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local llRet		:= .F.
	
	private lMsHelpAuto := .T.
	private lMsErroAuto := .F.
	
	if ( len( alItens ) > 0 )
	
		// Cria cabe�alho da transfer�ncia m�ltipla
		clDoc := getSxENum( "SD3", "D3_DOC", 1 )
		AADD( alAuto,{ clDoc, dDataBase } )
		
		// Adiciona itens enviado por parametro
		aEval( alItens, {|x| AADD( alAuto, x ) } )
	
		begin transaction
	
			MSExecAuto({|x,y| MATA261(x,y) }, alAuto, nlOpcAuto )				
		
			if ( lMsErroAuto )
			
				llRet := .F.
				
				clArqErro  := NomeAutoLog()
				if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
					clMsgErro += "Falha no documento [" + clDoc + "] de transfer�ncia m�ltipla." + CRLF
					clMsgErro += memoRead( clArqErro ) + CRLF + replicate("=", 30) + CRLF
					memoWrite(clArqErro," ") // Limpa log automatico
				endif
				
				rollbackSx8()
				
				disarmtransaction()
				
			else
				llRet := .T.
				confirmSx8()
				conout("Incluido doc [" + clDoc + "] de transferencia multipla com sucesso!")			
			endif
		
		end transaction
		
		if ( .not. empty( clMsgErro ) )
			U_ADMVXFUN( 1, {"BF_LOCALIZ", clMsgErro } )
		endif
	
	endif
	
Return llRet

/*/{Protheus.doc} calcQtsPer
Responsavel por calcular as quantidades a serem tansferidas conforme percentual de cota e percentual retido
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return array,	[1] Quantidade a ser transferida conforme percentual de cotas
				[2] Quantidade a ser transferida conforme percentual retido
@param nlQtCalc, numeric, Saldo total a ser transferido
@param clId, characters, ID do cadastro de cotas a considerar para calcular os percentuais e quantidades a serem transferidas
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function calcQtsPer( nlQtCalc, clId, clProduto )

	local nlQtdToCot	:= 0
	local nlQtdToRet	:= 0
	local nlPerCot		:= 0
	local nlPerRet		:= 0
	local alPercs		:= {}
	
	default nlQtCalc	:= 0
	default clId		:= ""
	
	// Calcula os percentuais a serem considerados, conforme cadastro de cotas e suas exce��es
	alPercs		:= calcPercs( clId, clProduto )
	nlPerCot	:= alPercs[1]
	nlPerRet	:= alPercs[2]

	// Calcula a quantidade a ser transferida conforme o percentual de cotas do cadastro
	nlQtdToCot	:= round( ( nlQtCalc * ( nlPerCot / 100 ) ), 0 ) // Conforme regra definida com Paty, as quantidades devem ser arredonadas conforme regras matematicas
	
	// Calcula a quantidade a ser transferida conforme o percentual retido, se baseando na quantidade do % de cotas
	nlQtdToRet	:= round( ( nlQtdToCot * ( nlPerRet / 100 ) ), 0 ) // Conforme regra definida com Paty, as quantidades devem ser arredonadas conforme regras matematicas
	
	// Retira a quantidade de retido da quantidade calculada para cotas
	nlQtdToCot	-= nlQtdToRet
	
Return { nlQtdToCot, nlQtdToRet }

/*/{Protheus.doc} calcPercs
Responsavel por calcular os percentuais a serem considerados para calcular a quantidade a ser transferida entre os enderecos
@author DS2U (SDA)
@since 29/05/2018
@version 1.0
@return array,	[1] Percentual de cotas
				[2] Percentual retido
@param clId, characters, ID do cadastro de cotas a ser considerado para calculo de % Cotas
@param clProduto, characters, Codigo do produto
@type function
/*/
Static Function calcPercs( clId, clProduto )

	local alArea	:= getArea()
	local nlPerCot	:= 0
	local nlPerRet	:= 0
	local clColecao	:= ""
	local clCateg	:= ""
	local clGrp		:= ""
	
	default clProduto	:= ""
	
	dbSelectArea( "SB1" )
	SB1->( dbSetOrder( 1 ) )
	
	if ( SB1->( dbSeek( FWxFilial( "SB1" ) + PADR( clProduto, tamSX3("B1_COD")[1] ) ) ) )
	
		clColecao	:= SB1->B1_XCARAC
		clGrp		:= SB1->B1_GRUPO
		// TODO Verificar os campos do cadastro de produto para preencher categoria
	
		dbSelectArea( "ZC0" )
		ZC0->( dbSetOrder( 1 ) )
		
		dbSelectArea( "ZC1" )
		ZC1->( dbSetOrder( 2 ) ) // ZC1_FILIAL, ZC1_GRP, ZC1_ID, ZC1_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "ZC2" )
		ZC2->( dbSetOrder( 2 ) ) // ZC2_FILIAL, ZC2_CATEG, ZC2_ID, ZC2_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "ZC3" )
		ZC3->( dbSetOrder( 2 ) ) // ZC3_FILIAL, ZC3_COLEC, ZC3_ID, ZC3_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		dbSelectArea( "ZC4" )
		ZC4->( dbSetOrder( 2 ) ) // ZC4_FILIAL, ZC4_PROD, ZC4_ID, ZC4_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( ZC0->( dbSeek( FWxFilial( "ZC0" ) + PADR( clId, tamSX3("ZC0_ID")[1] ) ) ) )
		
			// Verifica se existe exece��o de produto
			if ( ZC4->( dbSeek( ZC0->ZC0_FILIAL + PADR( clProduto, tamSX3("ZC4_PROD")[1] ) + PADR( clId, tamSX3("ZC4_ID")[1] ) ) ) )
			
				nlPerCot	:= ZC4->ZC4_PERCOT
				nlPerRet	:= ZC4->ZC4_PERRET
			
			// Verifica se existe exece��o de colecao de produto
			elseif ( ZC3->( dbSeek( ZC0->ZC0_FILIAL + PADR( clColecao, tamSX3("ZC3_COLEC")[1] ) + PADR( clId, tamSX3("ZC3_ID")[1] ) ) ) )
			
				nlPerCot	:= ZC3->ZC3_PERCOT
				nlPerRet	:= ZC3->ZC3_PERRET
			
			// Verifica se existe exece��o de categoria de produto
			elseif ( ZC2->( dbSeek( ZC0->ZC0_FILIAL + PADR( clCateg, tamSX3("ZC2_CATEG")[1] ) + PADR( clId, tamSX3("ZC2_ID")[1] ) ) ) )
			
				nlPerCot	:= ZC2->ZC2_PERCOT
				nlPerRet	:= ZC2->ZC2_PERRET
			
			// Verifica se existe exece��o de categoria de produto
			elseif ( ZC1->( dbSeek( ZC0->ZC0_FILIAL + PADR( clGrp, tamSX3("ZC1_GRP")[1] ) + PADR( clId, tamSX3("ZC1_ID")[1] ) ) ) )
			
				nlPerCot	:= ZC1->ZC1_PERCOT
				nlPerRet	:= ZC1->ZC1_PERRET
			
			else
			
				nlPerCot	:= ZC0->ZC0_PERCOT
				nlPerRet	:= ZC0->ZC0_PERRET
			
			endif
			
		endif
		
	endif
	
	restArea( alArea )

Return { nlPerCot, nlPerRet }

/*/{Protheus.doc} adTransf
Funcao auxiliar para adicionar os itens da transferencia multipla
@author DS2U (SDA)
@since 21/05/2018
@version 1.0
@return alItem, array com o item para ser adicionado na transfer�ncia multplia entre enderecos
@param alParam, array,	[1] Quantidade a ser transferida
						[2] Codigo do produto
						[3] Armazem Origem
						[4] Armazem Destino
						[5] Endereco Origem
						[6] Endereco Destino
						[7] Observacao a ser adicionada na movimentacao da SD3
@type function
/*/
Static Function adTransf( alParam )
	
	local alArea	:= getArea()	
	local alItem	:= {}
	local nlQtTransf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "N", alParam[1], 0 )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clLocalOri:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local clLocalDes:= iif( valType( alParam ) == "A" .and. len( alParam ) > 3 .and. valType( alParam[4] ) == "C", alParam[4], "" )
	local clEndOri	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 4 .and. valType( alParam[5] ) == "C", alParam[5], "" )
	local clEndDes	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 5 .and. valType( alParam[6] ) == "C", alParam[6], "" )
	local clObsrv	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 6 .and. valType( alParam[7] ) == "C", alParam[7], "" )
	
	if ( nlQtTransf > 0 )
	
		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		if ( SB1->( dbSeek( FwxFilial( "SB1" ) + clProduto ) ) )
		
			// Valor padrao para armazem origem
			if ( empty( clLocalOri ) )
				clLocalOri := SB1->B1_LOCPAD
			endif
			
			// Valor padrao para armazem destino
			if ( empty( clLocalDes ) )
				clLocalDes := clLocalOri
			endif
			
			//Origem				
			AADD( alItem, SB1->B1_COD )		// D3_COD		
			AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
			AADD( alItem, SB1->B1_UM )		// D3_UM		
			AADD( alItem, clLocalOri )		// D3_LOCAL		
			AADD( alItem, clEndOri  )		// D3_LOCALIZ

			// Verifica se o almoxafirado j� existe para o produto
			chkSB2(  SB1->B1_COD, clLocalDes )

			// Destino
			AADD( alItem, SB1->B1_COD )		// D3_COD		
			AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
			AADD( alItem, SB1->B1_UM )		// D3_UM	
			AADD( alItem, clLocalDes )		// D3_LOCAL		
			AADD( alItem, clEndDes )		// D3_LOCALIZ		
			AADD( alItem, "")				// D3_NUMSERI		
			AADD( alItem, "")				// D3_LOTECTL  		
			AADD( alItem, "")				// D3_NUMLOTE		
			AADD( alItem, cToD("//") )		// D3_DTVALID		
			AADD( alItem, 0 )				// D3_POTENCI		
			AADD( alItem, nlQtTransf )		// D3_QUANT		
			AADD( alItem, 0 )				// D3_QTSEGUM		
			AADD( alItem, "" )				// D3_ESTORNO		
			AADD( alItem, "" )				// D3_NUMSEQ 		
			AADD( alItem, "" )				// D3_LOTECTL		
			AADD( alItem, cToD("//") )		// D3_DTVALID		
			AADD( alItem, "" )				// D3_ITEMGRD
			AADD( alItem, "" )				// D3_IDDCF
			AADD( alItem, clObsrv )			// D3_OBSERVA
									
		else
			U_ADMVXFUN( 1, {"D3_COD", "Produto [" + clProduto + "] n�o encontrado no sistema" } )
		endif
		
	else
		U_ADMVXFUN( 1, {"D3_QUANT", "Quantidade a ser transferida [" + allTrim( str( nlQtTransf ) ) + "] deve ser positivo e maior que zero!" } )
	endif
	
	restArea( alArea )

Return alItem

/*/{Protheus.doc} getMapDist
Responsavel por montar a consulta do mapa de distribuicao de cotas, conforme cadastro de cotas - tabela ZC0
@author DS2U (SDA)
@since 18/05/2018
@version 1.0
@return clAlias, Alias da projecao do banco de dados com a consulta ja ordenada de como sera feito a distribuicao de cotas
@param clAlmoxTrf, characters, Codigo do armazem a ser considerado na distribuicao de cotas
@type function
/*/
Static Function getMapDist( alParam )

	local clAlias	:= getNextAlias()
	local clSql		:= ""
	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local llOnlyHier:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "L", alParam[2], .T. )
	local llOnlyLoc	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .F. )
	
	default alParam	:= {}
	
	clSql += "IF EXISTS (SELECT * FROM sysobjects WHERE  name = 'VW_MPCOTAS') "
	clSql += "   DROP VIEW VW_MPCOTAS "
	
	// Dropa view do banco de dados caso exita
	if ( tcSqlExec( clSql ) < 0 )
		U_ADMVXFUN( 1, {"ZC0_ID", tcSqlError() } )
	else
	
		clSql := "CREATE VIEW VW_MPCOTAS AS "
		
		clSql += "WITH NIVEIS ( FILIAL, NIVEL, ID, DESCID, ALMOXARIF, PERCOT, PERRET, GROUPID, PRIORIDADE ) "
		clSql += "AS ( "
			
		/* Membro �ncora (Pai) */
		clSql += "	SELECT "
		clSql += "		ZC0PAI.ZC0_FILIAL AS FILIAL "
		clSql += "		, ZC0PAI.ZC0_CODPAI AS NIVEL "
		clSql += "		, ZC0PAI.ZC0_ID AS ID "
		clSql += "		, ZC0PAI.ZC0_DESC AS DESCID "
		clSql += "		, ZC0PAI.ZC0_ALMOX AS ALMOXARIF "
		clSql += "		, ZC0PAI.ZC0_PERCOT AS PERCOT "
		clSql += "		, ZC0PAI.ZC0_PERRET AS PERRET "
		clSql += "		, 0 AS GROUPID "
		clSql += "		, ZC0PAI.ZC0_PRIORI AS PRIORIDADE "
			
		clSql += "	FROM "
		clSql += "		" + retSqlName( "ZC0" ) + " ZC0PAI "
			
		clSql += "	WHERE "
		clSql += "		ZC0PAI.ZC0_FILIAL = '" + FWxFilial( "ZC0" ) + "' "
		clSql += "		AND ZC0PAI.ZC0_CODPAI = ' ' "
		if ( .not. empty( clAlmoxTrf ) )
			clSql += "		AND ZC0PAI.ZC0_ALMOX = '" + clAlmoxTrf + "' "
		endif
		clSql += "		AND ZC0PAI.D_E_L_E_T_ = ' ' "
			
		clSql += "	UNION ALL "
			
		/* Parte recursiva (Filhos) */
		clSql += "	SELECT "
		clSql += "		ZC0FILHO.ZC0_FILIAL AS FILIAL "
		clSql += "		, ZC0FILHO.ZC0_CODPAI AS NIVEL "
		clSql += "		, ZC0FILHO.ZC0_ID AS ID "
		clSql += "		, ZC0FILHO.ZC0_DESC AS DESCID "
		clSql += "		, ZC0FILHO.ZC0_ALMOX AS ALMOXARIF "
		clSql += "		, ZC0FILHO.ZC0_PERCOT AS PERCOT "
		clSql += "		, ZC0FILHO.ZC0_PERRET AS PERRET "
		clSql += "		, GROUPID+1 "
		clSql += "		, ZC0FILHO.ZC0_PRIORI AS PRIORIDADE "
			
		clSql += "	FROM "
		clSql += "		" + retSqlName( "ZC0" ) + " ZC0FILHO "
			
		clSql += "	INNER JOIN "
		clSql += "		NIVEIS N ON "
		clSql += "		ZC0FILHO.ZC0_FILIAL = N.FILIAL "
		clSql += "		AND ZC0FILHO.ZC0_ALMOX = N.ALMOXARIF "
		clSql += "		AND ZC0FILHO.ZC0_CODPAI = N.ID "
			
		clSql += "	WHERE "
		clSql += "		ZC0FILHO.D_E_L_E_T_ = ' ' "	
			
		clSql += "	) "
		clSql += "	SELECT * FROM NIVEIS "
		
		if ( llOnlyHier ) // Somente niveis da hierarquia, desconsiderando o armazem
			clSql += " WHERE NIVEL <> ' ' "
		elseif ( llOnlyLoc ) // Somente niveis de armazem
			clSql += " WHERE NIVEL = ' ' "
		endif
		
		// Cria view no banco de dados
		// * Foi necessario criar view pois o Portheus nao estava entendendo a clausa WITH do banco de dados
		if ( tcSqlExec( clSql ) < 0 )
			U_ADMVXFUN( 1, {"ZC0_ID", tcSqlError() } )
		else
		
			// Consulta dados da View
			clSql := "SELECT * FROM VW_MPCOTAS ORDER BY GROUPID, PRIORIDADE "
			
			dbUseArea( .T., "TOPCONN", tcGenQry(,,clSql), clAlias, .F., .T. )
			
			if ( ( clAlias )->( eof() ) )
				( clAlias )->( dbCloseArea() )
				clAlias := ""
			endif
		
		endif
		
	endif
	
Return clAlias

/*/{Protheus.doc} jobDisCota
Reponsavel pelo processamento do JOB de distribuicao automatica de cotas
@author DS2U (SDA)
@since 06/06/2018
@version 1.0

@type function
/*/
Static Function jobDisCota()

	local alArea	:= getArea()
	local clAlias	:= prdAender()
	local nlx
	local clEndereco:= ""
	local clLocal	:= ""
	
	if ( .not. empty( clAlias ) )
	
		while ( .not. ( clAlias )->( eof() ) )
		
			// Endereca autoticamente para o endereco A TRANSFERIR de cada armazem
			distCotas( { ( clAlias )->ALMOX, ( clAlias )->PRODUTO, "", ( clAlias )->NUMSEQ } )
			
			( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )
		
	endif
		
	// Busca armazens do admv
	clAlias		:= getMapDist( {"", .F., .T.} ) // Buscao os armazens do ADMV
	
	if ( .not. empty( clAlias ) )
	
		clEndereco	:= allTrim( getMv( "ES_ENDTRAN",,"A_TRANSFERIR" ) ) // Identifica o endereco do armazem origem, conforme cadastro de cotas
	
		dbSelectArea ( "SBF" )
		SBF->( dbSetOrder( 1 ) ) // BF_FILIAL, BF_LOCAL, BF_LOCALIZ, BF_PRODUTO, BF_NUMSERI, BF_LOTECTL, BF_NUMLOTE, R_E_C_N_O_, D_E_L_E_T_
		
		while ( .not. ( clAlias )->( eof() ) )
		
			clLocal		:= PADR( ( clAlias )->ALMOXARIF, tamSX3("BF_LOCAL")[1] )
			clEndereco	:= PADR( clEndereco, tamSX3("BF_LOCALIZ")[1] )
		
			if ( SBF->( dbSeek( FWxFilial( "SBF" ) + clLocal + clEndereco ) ) )
				
				while ( .not. SBF->( eof() ) .and. SBF->BF_FILIAL == FWxFilial( "SBF" ) .and. SBF->BF_LOCAL == clLocal .and. SBF->BF_LOCALIZ == clEndereco )
			
					// Realiza transfer�ncia entre armaz�ns / endere�os de tudo que existe no endereco A TRANSFERIR
					endBetLoc( SBF->BF_LOCAL, SBF->BF_PRODUTO )
					
					// Distribui entre os enderecos dos armazens envolvidos
					distCotas( { SBF->BF_LOCAL, SBF->BF_PRODUTO } )
			
					SBF->( dbSkip() )
				endDo
				
			endif
		
			( clAlias )->( dbSkip() )
		endDo
		( clAlias )->( dbCloseArea() )
	
	endif
		
	restArea( alArea )

Return

/*/{Protheus.doc} prdAender
Responsavel por consultar produtos a enderecar
@author DS2U (SDA)
@since 06/06/2018
@version 1.0
@return clAlias, Alias do arquivo de trabalho com os registros a enderecar

@type function
/*/
Static Function prdAender()

	local clAlias	:= getNextAlias()
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			DA_FILIAL AS FILIAL
			, DA_PRODUTO AS PRODUTO
			, DA_LOCAL AS ALMOX
			, DA_NUMSEQ AS NUMSEQ
			, DA_SALDO AS SALDO
			 
		FROM 
			%TABLE:SDA% SDA
			 
		WHERE 
			SDA.DA_FILIAL = %XFILIAL:SDA%
			AND SDA.DA_SALDO > 0
			AND SDA.%NOTDEL%
	
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		( clAlias )->( dbCloseArea() )
		clAlias := ""
	endif
	
Return clAlias