#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} ADMVP02
Programa responsavel pelas tratativas de aloca��es de pedidos
@author DS2U (SDA)
@since 14/05/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@param [llIsJob], logical, Variavel para controlar se .T. processamento em JOB ou .F. Processamento com interface com usuario. Padrao = isBlind()
@type function
/*/
User Function ADMVP02( nlOpc, uParam, llIsJob )

	local uRet		:= nil

	private lpInJob	:= .F.	

	default nlOpc	:= 0
	default uParam	:= nil
	default llIsJob	:= isBlind()
	
	lpInJob := llIsJob
	
	do case
	
		case nlOpc == 0
			uRet := getEstByEn( uParam )
		
		case nlOpc == 1
			uRet := getLocVend( uParam )
			
		case nlOpc == 2
			uRet := calcEstFis( uParam )
			
		case nlOpc == 3
			uRet := calcEstFut( uParam )
			
		case nlOpc == 4
			uRet := calcEstPrv( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} getEstByEn
Responsavel por retornar o calculo do estoque disponivel fisico e futuro
@author sergi
@since 31/05/2018
@version 1.0
@return array,	[1] Quantidade disponivel fisicamente
				[2] Quantidade disponivel futuramente
@param alParam, array of logical,	[1] Codigo do produto
									[2] Codigo do armazem
									[3] Codigo do endereco
@type function
/*/
Static Function getEstByEn( alParam )

	local alArea	:= getArea()
	local nlQtFisic	:= 0
	local nlQtFutur	:= 0
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clEndereco:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local alInfo	:= {}
	
	// Se nao foi informado, busca do cadastro do vendedor o endereco vinculado para consulta
	if ( empty( clAlmoxTrf ) .or. empty( clEndereco ) )
		alInfo		:= getLocVend()
		clAlmoxTrf	:= alInfo[1]
		clEndereco	:= alInfo[2]
	endif
	
	// Calcula saldo fisico do produto
	nlQtFisic := calcEstFis( {clAlmoxTrf, clEndereco, clProduto} )
	
	// Calcula saldo futuro do produto
	nlQtFutur := calcEstFut( {clAlmoxTrf, clEndereco, clProduto} )
	
	restArea( alArea )

Return { nlQtFisic, nlQtFutur }

/*/{Protheus.doc} getLocVend
Responsavel por retornar armazem, endereco de cota, endenreco retido conforme usuario conectado ao sistema
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return array,	[1] Codigo do armazem
				[2] Codigo do endereco de % de cota
				[3] Codigo do endereco de % retido
@type function
/*/
Static Function getLocVend()

	local alArea	:= getArea()
	local clCodUsr	:= retCodUsr()
	local clAmox	:= ""
	local clEndCot	:= ""
	local clEndRet	:= ""
	local alEndereco:= {}
	
	dbSelectArea( "SA3" )
	SA3->( dbSetOrder ( 7 ) ) // A3_FILIAL, A3_CODUSR, R_E_C_N_O_, D_E_L_E_T_

	if ( SA3->( dbSeek( FWxFilial( "SA3" ) + clCodUsr ) ) )
	
		if ( empty( SA3->A3_XIDALOC ) )
	
			U_ADMVXFUN( 1, {"A3_CODUSR", "Vendedor [" + SA3->A3_COD + " - " + allTrim( SA3->A3_NOME ) + "] n�o est� vinculado a nenhum ID de Alocacao (Cotas). Consulta de estoque futuro por cota esta indisponivel!" } )
	
		else
		
			dbSelectArea( "ZC0" )
			ZC0->( dbSetOrder ( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
			
			if ( ZC0->( dbSeek( FWxFilial( "ZC0") + SA3->A3_XIDALOC ) ) )
				
				clAmox	:= ZC0->ZC0_ALMOX
				
				alEndereco := U_ADMVXFUN( 7, ZC0->ZC0_ID ) // Gera ID do endereco de % Cota e % retido
				clEndCot := alEndereco[1]
				clEndRet := alEndereco[2]
				 
			endif
			
		endif
	
	else
		U_ADMVXFUN( 1, {"A3_CODUSR", "Usuario [" + clCodUsr + " - " + allTrim( usrRetName( clCodUsr ) ) + "] nao esta vinculado a nenhum vendedor. Consulta de estoque futuro por cota esta indisponivel!" } )
	endif

	restArea( alArea )

Return { clAmox, clEndCot, clEndRet }

/*/{Protheus.doc} calcEstFis
Responsavel por retornar o saldo fisico do produto no endereco informado
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return nlQtFisic, Quantidade disponivel no endereco
@param alParam, array,	[1] Codigo do armazem
						[2] Codigo do endereco
						[3] Codigo do produto
@type function
/*/
Static Function calcEstFis( alParam )

	local alArea	:= getArea()
	local nlQtFisic	:= 0
	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clEndereco:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )

	dbSelectArea ( "SBF" )
	SBF->( dbSetOrder( 1 ) ) // BF_FILIAL, BF_LOCAL, BF_LOCALIZ, BF_PRODUTO, BF_NUMSERI, BF_LOTECTL, BF_NUMLOTE, R_E_C_N_O_, D_E_L_E_T_
	
	if ( SBF->( dbSeek( FWxFilial( "SBF" ) + PADR( clAlmoxTrf, tamSX3("BF_LOCAL")[1] ) + PADR( clEndereco, tamSX3("BF_LOCALIZ")[1] ) + PADR( clProduto, tamSX3("BF_PRODUTO")[1] ) ) ) )
		nlQtFisic := ( SBF->BF_QUANT - SBF->BF_EMPENHO )
	endif

	restArea( alArea )

Return nlQtFisic

/*/{Protheus.doc} calcEstFut
Responsavel por retornar o saldo futuro do produto no endereco informado
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return nlQtFisic, Quantidade disponivel no endereco
@param alParam, array,	[1] Codigo do armazem
						[2] Codigo do endereco
						[3] Codigo do produto
@type function
/*/
Static Function calcEstFut( alParam )

	local alArea	:= getArea()
	local nlQtFutur	:= 0
	local nlQtPrev	:= 0
	local clAlmoxTrf:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clEndereco:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local clProduto	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "C", alParam[3], "" )
	local alInfo	:= {}

	// Calcula quantidade de entrada prevista do produto
	nlQtPrev := calcEstPrv( clProduto )
	
	// Calcula a distribuicao entre enderecos de forma virtual e retorna a quantidade no endereco informado no parametro
	alInfo := U_ADMVP01( 5, { clAlmoxTrf, clProduto, clEndereco, nlQtPrev } )
	nlQtFutur := alInfo[3]

	restArea( alArea )

Return nlQtFutur

Static Function calcEstPrv( clProduto )
Return