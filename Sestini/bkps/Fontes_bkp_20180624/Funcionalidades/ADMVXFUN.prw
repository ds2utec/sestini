#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} ADMVXFUN
Bilioteca de funcoes referente ao projeto de ADM de Vendas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return uRet, retorno da funcao executada
@param nlOpc, numeric, Parametro para identificar a funcao a ser executada
@param uParam, undefined, Parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVXFUN( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 1
			uRet := helpAdmv( uParam )
		
		case nlOpc == 2
			uRet := getAlmxPai( uParam )
			
		case nlOpc == 3
			uRet := vldPerc( uParam )
			
		case nlOpc == 4
//			DESTONTINUADO
//			uRet := getChvSBE( uParam )
			
		case nlOpc == 5
			uRet := vlIdAlocA3( uParam )
			
		case nlOpc == 6
			uRet := vldIdDup( uParam )
			
		case nlOpc == 7
			uRet := geraIdEnd( uParam )
		
		case nlOpc == 8	
			uRet := chkLocaliz( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} geraIdEnd
Responsavel por controlar a montagem do ID do endereco a ser criado no sistema
@author DS2U (SDA)
@since 18/05/2018
@version 1.0
@return clEndereco, codigo do endereco montado conforme almoxarifado + id do cadastro de cotas
@param clID, characters, ID do cadastro de alocacao
@type function
/*/
Static Function geraIdEnd( clID )

	default clId	:= {}
	
Return {"C" + allTrim( clId ), "R" + allTrim( clId ) }

/*/{Protheus.doc} vldIdDup
Responsavel por checar se o ID de aloca��o informado no cadastro do vendedor � unico para este cadastro, pois os
sistema deve garantir que um endere�o deve ser associado a somente 1 vendedor
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, se .F. o id ja esta sendo utilizado, se .T. esta disponivel

@type function
/*/
Static Function vldIdDup( alParam )

	local llRet		:= .T.
	local clAlias	:= getNextAlias()
	local clCodVend	:= ""
	local clIdAloc	:= ""
	
	default alParam	:= { M->A3_COD, M->A3_XIDALOC }
	
	clCodVend	:= alParam[1]
	clIdAloc	:= alParam[2]
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			A3_COD
			, A3_NOME
			
		FROM
			%TABLE:SA3% SA3
		
		WHERE
			SA3.A3_FILIAL = %XFILIAL:SA3%
			AND NOT ( A3_COD = %EXP:clCodVend% )
			AND A3_XIDALOC = %EXP:clIdAloc%
			AND SA3.%NOTDEL%
		
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
	
		llRet := .F.
		U_ADMVXFUN( 1,	{"A3_XIDALOC",	"O ID de aloca��o [" + allTrim( clIdAloc ) + "] ja est� sendo utilizado para o vendedor "  +; 
										"[" + ( clAlias )->A3_COD + " - " + allTrim( ( clAlias )->A3_NOME ) + "]" } )
	
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} vlIdAlocA3
Responsavel por validar preenchimento do campo de ID de alocacao no cadastro do vendedor
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. Se o codigo selecionado/digitado � valido ou .F., invalido

@type function
/*/
Static Function vlIdAlocA3()

	local alArea	:= getArea()
	local llRet		:= .T.
	
	// Valida se registro existe na tabela ZC0
	llRet := existZC0( M->A3_XIDALOC )

	// Valida preenchimento de ID de aloca��o
	llRet := ( llRet .and. vldIdDup( {M->A3_COD, M->A3_XIDALOC } ) )

	restArea( alArea )

Return llRet

/*/{Protheus.doc} existZC0
Responsavel por checar se ID existe no cadastro de cotas
@author DS2U (SDA)
@since 11/06/2018
@version 1.0
@return llRet, .T. = Existe / .F. = N�o existe
@param clIdCota, characters, ID da cota��o a ser checada
@type function
/*/
Static Function existZC0( clIdCota )

	local alArea	:= getArea()
	local llRet		:= .T.
	
	dbSelectArea( "ZC0" )
	ZC0->( dbSetOrder( 1 ) )
	
	llRet := ZC0->( dbSeek( xFilial( "ZC0" ) + PADR( clIdCota, tamsx3("ZC0_ID")[1] ) ) )
	
	if ( .not. llRet )
		helpAdmv( {"ZC0_ID", "Registro n�o encontrado no cadastro de cotas. Informe um registro v�lido!"} )
	endif 
	
	restArea( alArea )

Return llRet

///*/{Protheus.doc} getChvSBE
//Responsavel por definir regras de filtro para a consulta padrao de cotas ZC0
//@author DS2U (SDA)
//@since 07/05/2018
//@version 1.0
//@return llRet, Retorno booleano conforme regra aplicada ao filtro da tabela ZC0
//
//@type function
///*/
//Static Function getChvSBE( clIdAloc )
//
//	local clChave	:= XFILIAL("SBE")+SUBS(clIdAloc,1,2)+clIdAloc
//
//Return clChave

/*/{Protheus.doc} helpAdmv
Funcao customizada para apresenta��o de alerta
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@param clMsg, characters, Mensagem do usuario a ser apresentada
@type function
/*/
Static Function helpAdmv( alParam )

	local clCampo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clMsg		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local llConout	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .T. )

	default alParam	:= {}

	if ( .not. empty( clCampo ) .and. .not. empty( clMsg ) )

		if ( llConout )
			conout( clCampo )
			conout( clMsg )
		endif
		
		fwClearHLP()
		help('', 1, getSX3Cache(clCampo, "X3_TITULO"), clCampo, clMsg, 1, 0)
		
	endif
	   
Return

/*/{Protheus.doc} getAlmxPai
Funcao para buscar o codigo do almoxafirado do codigo pai enviado por parametro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return clAlmoxPai, Codigo do almoxarifado conforme o codigo pai

@type function
/*/
Static Function getAlmxPai( clCodPai )

	local alArea	:= getArea()
	local clAlmoxPai:= ""
	
	default clCodPai	:= ""
	
	if ( .not. empty( clCodPai ) )
	
		dbSelectArea("ZC0")
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( ZC0->( dbSeek( xFilial("ZC0") + clCodPai ) ) )
			clAlmoxPai := ZC0->ZC0_ALMOX
		else
			helpAdmv( {"ZC0_ALMOX", "C�digo Pai n�o existe!"} )
		endif
		
	endif
	
	restArea( alArea )
		
Return clAlmoxPai

/*/{Protheus.doc} vldPerc
Funcao para validacao de preenchimento de percentual entre 0,01 e 100 %
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retorna verdadeiro ou falso identificando se o preenchimento do percentual est� v�lido
@param nlPerc, numeric, percentual a ser avaliado
@type function
/*/
Static Function vldPerc( nlPerc )

	local llRet	:= .T.

	default nlPerc := 0
	
	if ( nlPerc <= 0 .or. nlPerc > 100 )
		
		llRet := .F.
		helpAdmv( {"ZC0_PERCOT", "O percentual deve ser preenchido entre 0.01% e 100% !"} )
		
	endif
	
Return llRet

/*/{Protheus.doc} chkLocaliz
Responsavel por checar se o produto controla endereco, levando em consideracao o cadastro de indicador de produtos (SBZ)
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return llLocaliz, .T. = Controla endereco / .F. = Nao controla endereco
@param clProduto, characters, descricao
@type function
/*/
Static Function chkLocaliz( clProduto )

	local alArea	:= getArea()
	local llLocaliz	:= .F.
	
	dbSelectArea( "SB1" )
	SB1->( dbSetOrder( 1 ) ) // B1_FILIAL, B1_COD
	
	dbSelectArea( "SBZ" )
	SBZ->( dbSetOrder( 1 ) ) // BZ_FILIAL, BZ_COD
	
	// Verifica se tem indicador de produto e se controla endereco
	if ( SBZ->( dbSeek( FWxFilial("SBZ") + clProduto ) ) )
		llLocaliz := ( SBZ->BZ_LOCALIZ == "S" )
	
	// Se nao verifica no proprio cadastro de produto se controla endereco
	elseif ( SB1->( dbSeek( FWxFilial("SB1") + clProduto ) ) )
		llLocaliz := ( SB1->B1_LOCALIZ == "S" )
	endif
	
	restArea( alArea )
	
Return llLocaliz