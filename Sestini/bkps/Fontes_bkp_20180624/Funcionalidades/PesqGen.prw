#include "rwmake.ch"
#include "topconn.ch"
//#Include "FILEIO.CH"


/*
XB_COLUNA = "RE"
XB_TIPO = "2" Execblock(ExpC1,ExpL1,ExpL2,ExpX1,ExpL3);
XB_TIPO = "5" o pr�prio retorno da Consulta.

Exemplo:

XB_ALIAS     XB_TIPO        XB_SEQ           XB_COLUNA XB_DESCRI             XB_CONTEM
_B8                1        01                  RE   Lotes                      SB8
_B8                2        01                  01   U_PesqGen( "LOTES", "SB8", 1, "B8_LOTECTL", "B8_SALDO > 0")
_B8                5        01                  01   (SB8->B8_LOTECTL)+(SB8->B8_NUMLOTE)
                   




*/

User Function PesqGen( cTitulo, cAlias, nStartOrdem, cCampoDefault, _cFiltro, _lVisu, _lIncl)
Local cTexto 		:= SPACE(60), nOrdem := nStartOrdem
Local lOk 			:= .F., aCombo, oCombo1, oFiltrar, aRadio
Local nMarca 		:= &(cAlias)->(Recno()), nOldOrd := &(cAlias)->(IndexOrd())
Local aGrid     	:= {}
Local aFiltrar  	:= {}
Local aNFiltrar 	:= {}
Local nFiltrar  	:= 1
Local nSeq      	:= 1


If _lVisu == nil
	_lVisu := .F.
EndIf

If _lIncl == nil
	_lIncl := .T.
EndIf

If _cFiltro == nil
 _cFiltro := ""
EndIF
 
&(cAlias)->(dbSetOrder(nStartOrdem))
@ 0,0 To 542,790 Dialog mkwdlg Title OemToAnsi(cTitulo)
@ 01,005 say "Buscar "
nRadio := 2 
@ 08,005 GET cTexto Picture "@!" Valid (Filtra(cAlias,cTexto,aFiltrar[nFiltrar],nRadio,_cFiltro), oABC:oBrowse:SetFocus()) Object oTexto
aRadio := {"Pelo In�cio","Qualquer Posi��o"}
@ 19,005 RADIO aRadio VAR nRadio

_cCampos := ""

If cAlias=="SA1"
	_cCampos := "A1_COD, A1_CGC, A1_NOME, A1_MUN"
	_lVisu   := .t.
EndIf

If cAlias=="SE4"
	_cCampos := "E4_CODIGO, E4_COD, E4_DESCRI"
	_lVisu   := .t.
EndIf

If cAlias=="SA4"
	_cCampos := "A4_NOME,A4_NREDUZ,A4_CGC"
	_lVisu   := .t.
EndIf
     
If cAlias=="SA2"
	_cCampos := "A2_COD,A2_LOJA,A2_NOME,A2_CGC"
	_lVisu   := .t.
EndIf     

If cAlias=="SB1"
	_cCampos := "B1_COD,B1_DESC,B1_CODBAR"
	_lVisu   := .T.
EndIf

DbSelectArea("SIX")
DbSeek(cAlias)
aCombo := {}
Do While SIX->INDICE == cAlias
	AADD(aCombo,SIX->DESCRICAO)
	DbSkip()
EndDo

DbSelectArea("SX3")
DbSetOrder(1)
DbSeek(cAlias,.T.)
Do While X3_ARQUIVO == cAlias
	If (X3_BROWSE == "S".OR. cAlias=='SA1') .AND. X3_CONTEXT <> "V" .AND. (Empty(_cCampos) .OR. (AllTrim(X3_CAMPO)$_cCampos)) 
	    IF alltrim(X3_CAMPO)=="B1_COD"
	       cPict := "@S15" 
	    ELSEIF alltrim(X3_CAMPO)=="B1_DESC"   
	       cPict := "@S80" 
	    ELSE
	       cPict := X3_PICTURE
	    ENDIF   
		AADD(aGrid,{X3_CAMPO,X3_DESCRIC,cPict})
		If X3_TIPO == "C"
			AADD(aFiltrar ,X3_CAMPO)
			AADD(aNFiltrar,X3_DESCRIC)
			If AllTrim(X3_CAMPO) == cCampoDefault
				nFiltrar := nSeq
			EndIf
			nSeq++
		EndIf
	EndIf
	DbSkip()
EndDo



@ 01,130 Say "Campo"
@ 08,130 LISTBOX nFiltrar Items aNFiltrar Size 90,26 Object oFiltrar

@ 01,225 Say "Ordem"
@ 08,225 LISTBOX nOrdem Items aCombo Size 90,26 Object oCombo1
oCombo1:bChange := {|| ;
                       oCombo1:Refresh(),;
                       &(cAlias)->(DbSetOrder(nOrdem)),;
                       &(cAlias)->(DbGoTop()), ;
                       oABC:oBrowse:GoTop(), ;
                       oABC:oBrowse:Refresh()}

DbSelectArea(cAlias)

@  45,0 TO 240,393 BROWSE cAlias FIELDS aGrid Object oABC
@ 250,180 Button OemToAnsi("_Ok")  Size 36,12 Action Eval({|| lOk:= .T., Close(mkwdlg)})  Object obj02

If _lIncl
	@ 250,231 Button OemToAnsi("_Incluir Novo") Size 36,12 Action fIncluir(cAlias) Object obj01
EndIf

If _lVisu
	@ 250,273 Button OemToAnsi("_Visualizar") Size 36,12 Action fVisualizar(cAlias) Object obj01
EndIf
@ 250,357 Button OemToAnsi("_Fechar")     Size 36,12 Action Close(mkwdlg)                        Object obj03
oABC:oBrowse:blDblClick := {|| lOk:= .T., Close(mkwdlg)}
oABC:oBrowse:lColDrag   := .t.
//oABC:oBrowse:nFreeze    := 1
Filtra(cAlias,cTexto,aFiltrar[nFiltrar],nRadio,_cFiltro)
Activate Dialog mkwdlg CENTERED 

//ON INIT {|| oABC:oBrowse:GoTop(), oABC:oBrowstotvse:GoUp(), oABC:oBrowse:Refresh(), oABC:oBrowse:SetFocus() }

nPosicionar := Recno()
DbSelectArea(cAlias)
&(cAlias)->(DbClearFil())
DbSetOrder(nOldOrd)

If lOk
	DbGoTo(nPosicionar)
Else
	DbGoTo(nMarca)
EndIf                  

Return lOk

Static Function fVisualizar(_cAlias)
Local aArea := GetArea()
DbSelectArea(_cAlias)
AxVisual(_cAlias,Recno(),2)
RestArea(aArea)
Return nil

Static Function fIncluir(_cAlias)  
Local aArea := GetArea()
Local lbkpInclui
Local lbkpAltera
Local lMarcInc := .F.
Local lMarcAlt := .F.

If !Type("INCLUI") == "U"
	lMarcInc   := .T.
	lbkpInclui := INCLUI
Endif	
If !Type("ALTERA") == "U"
	lMarcAlt   := .T.
	lbkpAltera := ALTERA
Endif	
Inclui := .T.
DbSelectArea(_cAlias)
AxInclui(_cAlias,Recno(),3)

If lMarcInc
	INCLUI := lbkpInclui
Endif

If lMarcAlt
	ALTERA := lbkpAltera
Endif

RestArea(aArea)
Return nil

Static Function Filtra(xAlias,cTexto,cCampo, nTipo, cExtraFilter)
cTexto       :=StrTran(cTexto,"/","")
cTexto       :=StrTran(cTexto,"-","")
cTexto       :=StrTran(cTexto,"\","")
cPrefCpo     := PrefixoCpo(xAlias)

If cExtraFilter == nil 
	cExtraFilter := " 0 = 0 "
Else
	cExtraFilter := StrTran(cExtraFilter,"==","=")
EndIf

If nTipo == 2 // Qualquer Posicao
	cFiltroTOP := "@"+cCampo+" LIKE '%"+AllTrim(cTexto)+"%' " + IIF(!Empty(cExtraFilter), " AND "+cExtraFilter, "")
	cFiltro    := '"'+AllTrim(cTexto)+'"'+"$"+cCampo
Else //Pelo Inicio
	cFiltroTOP := "@"+cCampo+" LIKE '"+AllTrim(cTexto)+"%' " + IIF(!Empty(cExtraFilter), " AND "+cExtraFilter, "")
	cFiltro    := cCampo+'>="'+AllTrim(cTexto)+'"'
EndIf

If FieldPos(cPrefCpo + "_MSBLQL") > 0
 cFiltroTOP += " AND " + cPrefCpo + "_MSBLQL <> '1' "
	cFiltro    += " AND " + cPrefCpo + "_MSBLQL <> '1' "
EndIf

//TRATAMENTO PARA PRODUTO
//IF cAlias == 'SB1' .And.  ProcName() == 'MMAGECN1'
//	cFiltroTop :=SB1->B1_TIPO $ ("PA_PI_MP")

	

#IFDEF TOP
	cFiltro := cFiltroTOP
#ENDIF


SET FILTER TO &cFiltro
oABC:oBrowse:GoTop()
oABC:oBrowse:Refresh()
//oABC:oBrowse:SetFocus()

Return .T.


