#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA01
Rotina de cadastro de Cotas
@author DS2U (SDA)
@since 18/04/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA01( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			browse()
		
		case nlOpc == 1
			uRet := validAlmox( uParam )
			
		case nlOpc == 2
			uRet := vldPerCota( uParam )
			
		case nlOpc == 3
			uRet := vldPerRet( uParam )
		
		case nlOpc == 4	
			uRet := vldCodPai( uParam )
			
		case nlOpc == 5
			uRet := vldPerCGrp( uParam )
			
		case nlOpc == 6
			uRet := vldPerRGrp( uParam )
			
		case nlOpc == 7
			uRet := vldPerCPrd( uParam )
			
		case nlOpc == 8
			uRet := vldPerRPrd( uParam )
			
		case nlOpc == 9
			uRet := vldId( uParam )
			
		case nlOpc == 10
			uRet := calcId( uParam )
			
		case nlOpc == 11
			uRet := vldRanking( uParam )
			
		case nlOpc == 12
			uRet := vldPerCCat( uParam )
			
		case nlOpc == 13
			uRet := vldPerRCat( uParam )
			
		case nlOpc == 14
			uRet := vldPerCCol( uParam )
			
		case nlOpc == 15
			uRet := vldPerRCol( uParam )
			
		case nlOpc == 16
			uRet := showHierar( uParam )
			
		case nlOpc == 17
			uRet := transfCota( uParam )
			
		case nlOpc == 18
			uRet := showLogReg( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= ZC0->( getArea() )

	private oBrowse

	dbSelectArea("ZC0")
	dbSelectArea("ZC1")
	dbSelectArea("ZC2")
	dbSelectArea("ZC3")
	dbSelectArea("ZC4")
	dbSelectArea("SZA")
	dbSelectArea("SZB")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC0")
	oBrowse:SetDescription("Cadastro de Cotas x Canais")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA01"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA01"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 5 ACCESS 0 // "Excluir" 
	ADD OPTION alRotina TITLE "Transfer�ncia"			ACTION "U_ADMVA01(17)"		OPERATION 9 ACCESS 0 // "Copiar"
	ADD OPTION alRotina TITLE "Analisar Hierarquia"		ACTION "U_ADMVA01(16)"		OPERATION 2 ACCESS 0 // "Analisar Hierarquia"
	ADD OPTION alRotina TITLE "Log"						ACTION "U_ADMVA01(18)"		OPERATION 2 ACCESS 0 // "Log" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC0	:= FWFormStruct( 2, 'ZC0' )
	Local olStruZC1	:= FWFormStruct( 2, 'ZC1' ) // Cota por Grupo de Produtos
	Local olStruZC2	:= FWFormStruct( 2, 'ZC2' ) // Cota por Categoria de Produtos
	Local olStruZC3	:= FWFormStruct( 2, 'ZC3' ) // Cota por Cole��o de Produtos
	Local olStruZC4	:= FWFormStruct( 2, 'ZC4' ) // Cota por Produtos
	local olModel	:= FWLoadModel( 'ADMVA01' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC0', olStruZC0, 'ZC0MASTER' )
	
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_ZC1', olStruZC1, 'ZC1DETAIL' )
	olView:AddGrid( 'VIEW_ZC2', olStruZC2, 'ZC2DETAIL' )
	olView:AddGrid( 'VIEW_ZC3', olStruZC3, 'ZC3DETAIL' )
	olView:AddGrid( 'VIEW_ZC4', olStruZC4, 'ZC4DETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 35 )
	olView:CreateHorizontalBox( 'INFERIOR', 65 )
	
	//--Cria os Folder's
	olView:CreateFolder('FLDTZC1','INFERIOR')
	
	//--Aba Grupo de Produtos
	olView:AddSheet('FLDTZC1','GRDTZC1','Exce��o - Grupo de Produtos')													
	olView:CreateHorizontalBox('EXGRPPROD'		,100,/*owner*/,/*lUsePixel*/,'FLDTZC1','GRDTZC1')	

	//--Aba de Categoria de Produtos
	olView:AddSheet('FLDTZC1','GRDTZC2','Exce��o - Categoria de Produtos')													
	olView:CreateHorizontalBox('EXCATPROD'		,100,/*owner*/,/*lUsePixel*/,'FLDTZC1','GRDTZC2')
	
	//--Aba Produtos
	olView:AddSheet('FLDTZC1','GRDTZC3','Exce��o - Cole��es de Produtos')													
	olView:CreateHorizontalBox('EXCOLEPRD'		,100,/*owner*/,/*lUsePixel*/,'FLDTZC1','GRDTZC3')
	
	//--Aba Produtos
	olView:AddSheet('FLDTZC1','GRDTZC4','Exce��o - Produtos')													
	olView:CreateHorizontalBox('EXPROD'			,100,/*owner*/,/*lUsePixel*/,'FLDTZC1','GRDTZC4')
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC0', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_ZC1', 'EXGRPPROD' )
	olView:SetOwnerView( 'VIEW_ZC2', 'EXCATPROD' )
	olView:SetOwnerView( 'VIEW_ZC3', 'EXCOLEPRD' )
	olView:SetOwnerView( 'VIEW_ZC4', 'EXPROD' )
	
	// Liga a identificacao do componente
	olView:EnableTitleView('VIEW_ZC1','Exce��o - Grupo de Produtos' )
	olView:EnableTitleView('VIEW_ZC2','Exce��o - Categoria de Produtos' )
	olView:EnableTitleView('VIEW_ZC3','Exce��o - Cole��es de Produtos' )
	olView:EnableTitleView('VIEW_ZC4','Exce��o - Produtos' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC0	:= FWFormStruct( 1, 'ZC0', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZC1 := FWFormStruct( 1, 'ZC1', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZC2 := FWFormStruct( 1, 'ZC2', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZC3 := FWFormStruct( 1, 'ZC3', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruZC4 := FWFormStruct( 1, 'ZC4', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA01M', /* bPreValidacao*/,/*bPosValidacao*/ { |olModel| admvTudoOk( olModel ) },/*bCommit { |olModel| admvCommit( olModel ) }*/, { |olModel| admvCancel( olModel ) }/*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC0MASTER', /*cOwner*/, olStruZC0, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'ZC1DETAIL', 'ZC0MASTER', olStruZC1, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ8VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'ZC2DETAIL', 'ZC0MASTER', olStruZC2, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'ZC3DETAIL', 'ZC0MASTER', olStruZC3, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'ZC4DETAIL', 'ZC0MASTER', olStruZC4, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'ZC1DETAIL', { { 'ZC1_FILIAL', 'xFilial( "ZC1" )' }, { 'ZC1_CODPAI', 'ZC0_CODPAI' }, { 'ZC1_ID', 'ZC0_ID' }}, ZC1->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'ZC2DETAIL', { { 'ZC2_FILIAL', 'xFilial( "ZC2" )' }, { 'ZC2_CODPAI', 'ZC0_CODPAI' }, { 'ZC2_ID', 'ZC0_ID' }}, ZC2->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'ZC3DETAIL', { { 'ZC3_FILIAL', 'xFilial( "ZC3" )' }, { 'ZC3_CODPAI', 'ZC0_CODPAI' }, { 'ZC3_ID', 'ZC0_ID' }}, ZC3->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'ZC4DETAIL', { { 'ZC4_FILIAL', 'xFilial( "ZC4" )' }, { 'ZC4_CODPAI', 'ZC0_CODPAI' }, { 'ZC4_ID', 'ZC0_ID' }}, ZC4->( IndexKey( 1 ) ) )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC0_FILIAL", "ZC0_CODPAI", "ZC0_ID"} )
	
	olModel:SetDescription('Cotas x Canais')
	olModel:GetModel('ZC0MASTER' ):SetDescription('Cotas x Canais')
	
	olModel:GetModel('ZC1DETAIL'):SetOptional(.T.)
	olModel:GetModel('ZC2DETAIL'):SetOptional(.T.)
	olModel:GetModel('ZC3DETAIL'):SetOptional(.T.)
	olModel:GetModel('ZC4DETAIL'):SetOptional(.T.)
	
	olModel:GetModel('ZC1DETAIL'):SetUniqueLine({"ZC1_GRP"})
	olModel:GetModel('ZC2DETAIL'):SetUniqueLine({"ZC2_CATEG"})
	olModel:GetModel('ZC3DETAIL'):SetUniqueLine({"ZC3_COLEC"})
	olModel:GetModel('ZC4DETAIL'):SetUniqueLine({"ZC4_PROD"})
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

Static Function admvCancel( olModel )

	local olModelZC0:= olModel:getModel('ZC0MASTER')
	local nlOpc		:= olModel:getOperation()
	
	// Se for inclusao, da rollBack no sequencial do ID
	if ( nlOpc == 3 )
		rollBackSX8()
	endif
	
Return .T.

/*/{Protheus.doc} admvTudoOk
Responsavel por realizar validacoes e processamentos no commit do MVC
@author sergi
@since 07/05/2018
@version 1.0
@type function
/*/
Static Function admvTudoOk( olModel )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clDesc	:= ""
	local alEndCot	:= {}
	local olModelZC0:= olModel:getModel('ZC0MASTER')
	local nlOpc		:= olModel:getOperation()
	local clDesc	:= ""
	local clAlmox	:= ""
	local llProcOk	:= .T.
	
	// Se for inclusao
	if ( nlOpc == 3 )
	
		dbSelectArea( "SBE" )
		SBE->( dbSetOrder( 1 ) ) // BE_FILIAL, BE_LOCAL, BE_LOCALIZ, BE_ESTFIS
		
		clAlmox		:= allTrim( olModelZC0:getValue( "ZC0_ALMOX" ) )
		clEndereco	:= U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[1] // Gera ID do endereco de % Cota
		
		if ( SBE->( dbSeek( xFilial( "SBE" ) + clAlmox + clEndereco ) ) )
		
			llRet := .F.
			U_ADMVXFUN( 1,	{"ZC0_ALMOX",	"N�o � possivel criar endere�o amarrado a este cadastro, pois j� existe o endere�o " +; 
											allTrim( olModelZC0:getValue( "ZC0_ID" ) ) + " para o armaz�m " +;
											 olModelZC0:getValue( "ZC0_ALMOX" ) } )
		
		else
			
			clDesc		:= allTrim( olModelZC0:getValue( "ZC0_DESC" ) )
			clAlmox		:= allTrim( olModelZC0:getValue( "ZC0_ALMOX" ) )
			clEndereco	:= U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[1] // Gera ID do endereco de % Cota
			
			// Tratamento para nao estourar o tamanho do campo de descricao
			if ( len( clDesc ) > tamSX3( "BE_DESCRIC" )[1] )
				clDesc := subs( clDesc, 1, tamSX3( "BE_DESCRIC" )[1] )
			endif
			
			AADD( alEndCot, { clAlmox, clEndereco, clDesc } )
			
			// Endereco vinculado ao ID para % Retido
			clEndereco := U_ADMVXFUN( 7, olModelZC0:getValue( "ZC0_ID" ) )[2] // Gera ID do endereco de % Retido
			
			AADD( alEndCot, { clAlmox, clEndereco, clDesc + " RETIDO" } )
			
			begin transaction

				for nlx := 1 to len( alEndCot )
	
					// Checa se o armazem + endereco existe, e caso nao exista cria o endereco no ERP
					if ( .not. U_ADMVP01( 4, { alEndCot[nlx][1], alEndCot[nlx][2], alEndCot[nlx][3] } ) )
						llProcOk := .F.
						disarmTransaction()
						exit
					endif
	
				next nlx
				
			end transaction
			
			if ( .not. llProcOk )
				// A funcao getSXeNum esta sendo chamada do inicializador padrao do campo ZC0_ID, QUANDO FOR INCLUSAO
				confirmSX8()
			endif
		
		endif
		
	// Se for exclusao
	elseif ( nlOpc == 5 )
	
		llRet := chkCanDel( olModelZC0:getValue( "ZC0_ALMOX" ), olModelZC0:getValue( "ZC0_ID" ) )
	
		// Valida exclusao de endereco
		// *** DEVE SER A ULTIMA VALIDACAO A SER EXECUTADA
		llRet := ( llRet .and. delEndVinc( olModelZC0:getValue( "ZC0_CODPAI" ), olModelZC0:getValue( "ZC0_ID" ), olModelZC0:getValue( "ZC0_ALMOX" ) ) )
	
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} chkCanDel
Responsavel por checar se o cadastro corrente pode ser excluido
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. Se pode ser excluido ou .F. se nao pode ser excluido
@param clID, characters, ID do cadastro com a inten��o de ser excluido
@type function
/*/
Static Function chkCanDel( clAlmox, clID )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clAlias	:= getNextAlias()
	local clEndereco:= ""
	
	default clAlmox	:= ""
	default clID	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			ZC0_ID
			
		FROM
			%TABLE:ZC0% ZC0
			
		WHERE
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0.ZC0_CODPAI = %EXP:clID%
			AND ZC0.%NOTDEL%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		llRet := .F.
		U_ADMVXFUN( 1,	{"ZC0_ID", "Este cadastro tem amarra��o com n�veis abaixo deste. Portanto n�o pode ser exclu�do!" } )
	endif
	( clAlias )->( dbCloseArea() )
	
	if ( llRet )
	
		clEndereco := U_ADMVXFUN( 7, clID )[1] // Gera ID do endereco de % Cota 
	
		clAlias	:= getNextAlias()
		BEGINSQL ALIAS clAlias
		
			SELECT
				A3_COD
				, A3_NOME
				
			FROM
				%TABLE:SA3% SA3
				
			WHERE
				SA3.A3_FILIAL = %XFILIAL:SA3%
				AND SA3.A3_XIDALOC = %EXP:clEndereco%
				AND SA3.%NOTDEL%
		
		ENDSQL
		
		if ( .not. ( clAlias )->( eof() ) )
			llRet := .F.
			U_ADMVXFUN( 1,	{"ZC0_ID", "O vendedor " + allTrim( ( clAlias )->A3_COD ) + " - " + allTrim( ( clAlias )->A3_NOME ) + " est� vinculado a este canal. Altere o cadastro do vendedor para que seja poss�vel continuar com a exclus�o deste registro!"  } )
		endif
		( clAlias )->( dbCloseArea() )
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} delEndVinc
Responsavel por desvincular o cadastro de endereco do ID que esta sendo excluido
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. = Se a exclusao foi feita com sucesso, .F. com erros
@param clCodPai, characters, Codigo pai do cadastro de cotas
@param clID, characters, ID do cadastro de cotas
@param clAlmox, characters, Codigo do almoxarifado
@type function
/*/
Static Function delEndVinc( clCodPai, clID, clAlmox )
	
	local alArea	:= getArea()
	local llRet		:= .T.
	local alVetor	:= {}
	local clEndereco:= ""
	local clMsgErro	:= ""
	
	private lMsErroAuto := .F.

	// Se nao for o primeiro nivel, exclui o endereco amarrado ao cadastro de cotas
	if ( .not. empty( clCodPai ) )
	
		clAlmox		:= PADR( clAlmox, tamSX3("BE_LOCAL")[1] )
		clEndereco	:= PADR( U_ADMVXFUN( 7, clID )[1], tamSX3("BE_LOCALIZ")[1] ) // Gera ID do endereco de % Cota 

		dbSelectArea( "SBE" )
		SBE->( dbSetOrder( 1 ) ) // BE_FILIAL, BE_LOCAL, BE_LOCALIZ, BE_ESTFIS
		
		if ( SBE->( dbSeek( FWxFilial( "SBE" ) + clAlmox + clEndereco ) ) )
		
			alVetor := 	{{"BE_LOCAL"  	,clAlmox	,nil},;				
						{"BE_LOCALIZ"	,clEndereco	,nil}}

			MSExecAuto({|x,y| MATA015(x,y)},alVetor, 5)
				
			if ( lMsErroAuto )
				llRet := .F.
				mostraErro()
				U_ADMVXFUN( 1,	{"ZC0_ALMOX", "Falha na exclus�o automatica de endere�o. Verifique o problema e tente novamente!" } )
			endif
			
		else
			llRet := .F.
			U_ADMVXFUN( 1,	{"ZC0_ALMOX", "Armaz�m/Endere�o n�o encontrado. " + clAlmox + "/" + clEndereco } )
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} validAlmox
Funcao para validacao de preenchimento de armaz�m no cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retona se o almoxarifado informado � valido

@type function
/*/
Static Function validAlmox()

	local alArea	:= getArea()
	local llRet		:= .T.
	local clAlmox	:= ""
	local clAlmoxDig:= fwFldGet("ZC0_ALMOX")
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	
	dbSelectArea("NNR")
	NNR->( dbSetOrder( 1 ) ) // NNR_FILIAL, NNR_CODIGO, R_E_C_N_O_, D_E_L_E_T_
	
	if ( NNR->( dbSeek( xFilial("NNR") + clAlmoxDig ) ) )
	
		if ( .not. empty( M->ZC0_CODPAI ) )
		
			clAlmox	:= U_ADMVXFUN( 2, clCodPai )
			
			// Valida preenchimento do codigo pai
			if ( .not. empty( clAlmox ) .and. .not. ( clAlmox == clAlmoxDig ) .and. NNR->( dbSeek( xFilial("NNR") + clAlmox ) ) )

				llRet := .F.
				U_ADMVXFUN( 1,	{"ZC0_ALMOX", "O armaz�m deve ser o mesmo do cadastro pai: " + allTrim( NNR->NNR_CODIGO ) + " - " + allTrim( NNR->NNR_DESCRI ) } )
							 
			endif
			
		endif
		
		if ( almoxInUse( clCodPai, clIdAtu, clAlmoxDig ) )
			U_ADMVXFUN( 1,	{"ZC0_ALMOX", "Armaz�m n�o pode ser usando novamente neste n�vel!" } )
			llRet := .F.
		endif
			
	else
		llRet := .F.
		// Nao precisa de alert pois no MVC quando tem consulta padrao ja existe validacao de integridade de dados
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} almoxInUse
Funcao auxiliar para identificar se o armazem ja esta em uso no nivel de canais (codig pai em branco)
@author DS2U (SDA)
@since 22/04/2018
@version 1.0
@return llRet, Retonar se o armazem esta em uso (.T.) ou n�o (.F.)
@param clCodPai, characters, codigo pai do cadastro de cotas
@param clIdAtu, characters, id atual do cadastro de cotas, utilizado para consultas registros diferentes deste ID
@param clAmoxAtu, characters, Almoxarifado que esta tentando ser cadastrado e que precisa ser validado
@type function
/*/
Static Function almoxInUse( clCodPai, clIdAtu, clAmoxAtu )

	local llRet	:= .F.
	local clAlias

	if ( .not. empty( clIdAtu ) .and. empty( clCodPai ) )
	
		clAlias := getNextAlias()
	
		BEGINSQL ALIAS clAlias
		
			SELECT
				ZC0_ALMOX
				
			FROM
				%TABLE:ZC0% ZC0
				
			WHERE
				ZC0_FILIAL = %XFILIAL:ZC0%
				AND ZC0_CODPAI = %EXP:clCodPai%
				AND ZC0_ID <> %EXP:clIdAtu%
				AND ZC0_ALMOX = %EXP:clAmoxAtu%
				AND ZC0.%NOTDEL%
		
		ENDSQL
		
		if ( .not. ( clAlias )->( eof() ) )
			llRet := .T.
		endif
		( clAlias )->( dbCloseArea() )
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCota
Funcao para validacao de preenchimento do percental da cota
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCota()

	local llRet		:= .F.
	local nlPerCot	:= fwFldGet('ZC0_PERCOT')
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, nlPerCot ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPerCot,,,,, @nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC0_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCGrp
Funcao para validacao de preenchimento do percental da cota do grupo de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCGrp()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('ZC1_PERCOT')
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local clCodGrp	:= fwFldGet('ZC1_GRP')
	local nlSaldoPer:= 0

	// Valida preenchimento de percentual entre 0,01 e 100
	if ( U_ADMVXFUN( 3, nlPercAtu ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu, clCodGrp,,,,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC1_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de grupo de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCCat
Funcao para validacao de preenchimento do percental da cota de Categoria de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCCat()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('ZC2_PERCOT')
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local clCodCat	:= fwFldGet('ZC2_CATEG')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->ZC2_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,clCodCat,,,@nlSaldoPer ) )		     
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC1_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCCol
Funcao para validacao de preenchimento do percental da cota de colecoes de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCCol()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('ZC3_PERCOT')
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local clCodCol	:= fwFldGet('ZC3_COLEC')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->ZC3_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,,clCodCol,,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC3_COLEC", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de cole��es de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCPrd
Funcao para validacao de preenchimento do percental da cota de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCPrd()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('ZC4_PERCOT')
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local clCodPrd	:= fwFldGet('ZC4_PROD')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->ZC4_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,,,clCodPrd,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"ZC4_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerRet
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRet()
Return U_ADMVXFUN( 3, M->ZC0_PERRET )


/*/{Protheus.doc} vldPerRet
Funcao para validacao de preenchimento do percental retino no armaz�m / endenreco do grupo de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRGrp()
Return U_ADMVXFUN( 3, M->ZC1_PERRET )

/*/{Protheus.doc} vldPerRCat
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRCat()
Return U_ADMVXFUN( 3, M->ZC2_PERRET )

/*/{Protheus.doc} vldPerRPrd
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de cole��es de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRCol()
Return U_ADMVXFUN( 3, M->ZC3_PERRET )

/*/{Protheus.doc} vldPerRPrd
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRPrd()
Return U_ADMVXFUN( 3, M->ZC4_PERRET )

/*/{Protheus.doc} vldCodPai
Valida preenchimento do codigo pai
@author DS2U (SDA)
@since 20/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldCodPai()

	local alArea	:= getArea()
	local llRet		:= .T.
	local olModel	:= fwModelActive()
	local nlOperation	:= olModel:getOperation()
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	
	if ( llRet )
	
		if ( empty( clCodPai ) )
			fwFldPut("ZC0_ALMOX", criaVar("ZC0_ALMOX"),,,,.T. /*for�a atribuicao com loadvalue*/)
		else
	
			dbSelectArea("ZC0")
			ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
			
			if ( .not. ZC0->( dbSeek( xFilial("ZC0") + clCodPai ) ) )
				llRet := .F.
				U_ADMVXFUN( 1,	{"ZC0_CODPAI", "C�digo n�o existe." } )
			else
				// Esta instru��o � necessaria pois se o usuario digitar o codigo pai ao inves de selecionar pela lupa, o gatilho do armazem nao funciona
				fwFldPut("ZC0_ALMOX", ZC0->ZC0_ALMOX)
			endif
			
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} vldId
Funcao para validar o preenchimento do ID do cadastro de cotas
@author DS2U (SDA)
@since 22/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldId()

	local alArea	:= getArea()
	local llRet		:= .T.
	local clId		:= &( readVar() )
	local olModel	:= fwModelActive()
	local nlOperation:= olModel:getOperation()
	local clIdAtu	:= fwFldGet('ZC0_ID')
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	
	if ( nlOperation == 4 ) // se for alteracao
	
		// Nao permite alterar o id pois existem dependencias deste codigo, como enderecos e filhos na hierarquia
		if ( llRet .and. .not. ( ZC0->ZC0_ID == clIdAtu ) )
		
			llRet := .F.
			U_ADMVXFUN( 1,	{"ZC0_ID", "Este ID n�o pode ser alterado. Se estiver errado deve ser excluido para que seja mantido a integridade dos dados!" } )
		
		endif
	
	endif
	
	
	if ( llRet .and. .not. empty( clId ) )
	
		dbSelectArea("ZC0")
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( ZC0->( dbSeek( xFilial("ZC0") + clId + clCodPai ) ) )
			U_ADMVXFUN( 1,	{"ZC0_ID", "Este ID j� esxiste no n�vel atual. Mude o C�digo Pai ou o ID!" } )
			llRet := .F.
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} vPercNivel
Funcao para validacao de percentual do nivel do cadastro atual
@author sergi
@since 23/04/2018
@version 1.0
@return llRet, Retorna se o percentual corrente digitado pode ser utilizado no nivel atual do cadastro que esta sendo validado
@param clCodPai, characters, codigo pai do cadastro de cotas
@param clIdAtu, characters, id do cadastro de cotas
@param nlPercAtu, numeric, percentual digitado a ser validado
@param [clCodGrp], characters, Codigo do grupo de produto
@param [clCodCat], characters, Codigo de Categoria de produto
@param [clCodCol], characters, Codigo de Colecao de produto
@param [clCodPrd], characters, Codigo de produto
@param [nlSaldoPer], numeric, percentual digitado a ser validado
@type function
/*/
Static Function vPercNivel( clCodPai, clIdAtu, nlPercAtu, clCodGrp, clCodCat, clCodCol, clCodPrd, nlSaldoPer )

	local clAlias	:= getNextAlias()
	local clField	:= ""
	local clTable	:= "% " + retSQLTab("ZC0")
	local clWhere	:= "% ZC0.D_E_L_E_T_ = ' ' "
	local llRet		:= .F.
	
	default clCodPai	:= ""
	default clIdAtu		:= ""
	default nlPercAtu	:= 0
	default clCodGrp	:= ""
	default clCodCat	:= ""
	default clCodCol	:= ""
	default clCodPrd	:= ""
	default nlSaldoPer	:= 0
	
	if ( fwIsInCallStack("VLDPERCOTA") ) // Se a validacao vem do cabe�alho

		clField := "%COALESCE( SUM( ZC0_PERCOT ), 0 ) AS PERCOT%"
		
	elseif ( fwIsInCallStack("VLDPERCGRP") ) // Se a validacao vem da aba do grupo de produtos
	
		clField := "%COALESCE( SUM( ZC1_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("ZC1")
		clWhere += " AND ZC1_FILIAL = ZC0_FILIAL AND ZC1_ID = ZC0_ID AND ZC1_GRP = '" + clCodGrp + "' AND ZC1.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCCAT") ) // Se a validacao vem da aba de Categoria de produtos
	
		clField := "%COALESCE( SUM( ZC2_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("ZC2")
		clWhere += " AND ZC2_FILIAL = ZC0_FILIAL AND ZC2_ID = ZC0_ID AND ZC2_CATEG = '" + clCodCat + "' AND ZC2.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCCOL") ) // Se a validacao vem da aba de produtos
	
		clField := "%COALESCE( SUM( ZC3_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("ZC3")
		clWhere += " AND ZC3_FILIAL = ZC0_FILIAL AND ZC3_ID = ZC0_ID AND ZC3_COLEC = '" + clCodCol + "' AND ZC3.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCPRD") ) // Se a validacao vem da aba de produtos
	
		clField := "%COALESCE( SUM( ZC4_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("ZC4")
		clWhere += " AND ZC4_FILIAL = ZC0_FILIAL AND ZC4_ID = ZC0_ID AND ZC4_PROD = '" + clCodPrd + "' AND ZC4.D_E_L_E_T_ = ' ' "
		
	endif
	
	clTable	+= " %"
	clWhere	+= " %"

	BEGINSQL ALIAS clAlias
	
		SELECT
			%EXP:clField%
			
		FROM
			%EXP:clTable%
			
		WHERE
			ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0_CODPAI = %EXP:clCodPai%
			AND ZC0_ID <> %EXP:clIdAtu%
			AND %EXP:clWhere%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
	
		nlSaldoPer := ( 100 - ( clAlias )->PERCOT )
		llRet := ( nlPercAtu <= nlSaldoPer )
	
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} calcId
Funcao para calcular o proximo ID conforme cadastro corrente
@author DS2U
@since 30/04/2018
@version 1.0
@return clID, ID inexistente na tabela ZC0
@param [clID], characters, clID para checar se j� existe na tabela ZC0. Se nao existir sera considerado no retorna da funcao

@type function
/*/
Static Function calcId( clID )

	local alArea	:= getArea()
	
	default clID	:= ""
	
	if ( empty( clID ) )
		clID := getSXENum( "ZC0", "ZC0_ID", 1 )
	endif
	
	dbSelectArea( "ZC0" )
	ZC0->( dbSetOrder( 1 ) )
	
	while ( ZC0->( dbSeek( FWxFilial("ZC0") + clID ) ) )
	
		confirmSX8()
		clID := getSXENum( "ZC0", "ZC0_ID", 1 )
	
	endDo
	
	restArea( alArea )

Return clID

/*/{Protheus.doc} vldRanking
Funcao para validacao de preenchimento de ranking
Nao pode ter o mesmo valor do ranking dentro do mesmo nivel (Cod. Pai iguais)
@author DS2U (SDA)
@since 30/04/2018
@version 1.0
@return llRet, Retorna se o numero do ranking pode ser utilizado

@type function
/*/
Static Function vldRanking()

	local clAlias	:= getNextAlias()
	local clCodPai	:= fwFldGet('ZC0_CODPAI')
	local clNivel	:= allTrim( str( fwFldGet('ZC0_PRIORI') ) )
	local llRet		:= .F.
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			ZC0_ID
			, ZC0_DESC
			
		FROM 
			%TABLE:ZC0% ZC0
			 
		WHERE 
			ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0_CODPAI = %EXP:clCodPai%
			AND ZC0_PRIORI = %EXP:clNivel%
			AND ZC0.%NOTDEL%
	
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		llRet := .T.
	else
		U_ADMVXFUN( 1,	{"ZC0_PRIORI", "Este valor de Ranking j� est� sendo utilizado em -> ID " + allTrim( ( clAlias )->ZC0_ID ) + " - " + allTrim( ( clAlias )->ZC0_DESC ) } )
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} showHierar
Monta a hierarquia do  cadastro de cotas e mostra em interface visual modelo de arvore de niveis
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function showHierar()
	
	local alTree := {}

	FwMsgRun(,{|oSay| alTree := execHierar() },"Aguarde...","Processando dados..." )
	
	if ( len( alTree ) > 0 )
		showInTree( alTree )
	endif
	
Return

/*/{Protheus.doc} showHierar
Executa a montagem a hierarquia do  cadastro de cotas e mostra em interface visual modelo de arvore de niveis
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function execHierar()

	local alArmazens	:= getLocAdmv()
	local nlx
	local alTree		:= {}
	local clAlias
	local clNoPai		:= "00"
	local clNo			:= ""
	local clImage1		:= ""
	local clImage2		:= ""
	local nlCount		:= 1
	
	for nlx := 1 to len( alArmazens )
	
		// Buscao a hierarquia ou mapa de distribuicao de cotas
		clAlias := U_ADMVP01( 1, { alArmazens[nlx][1], .F. } )
		
		if ( .not. empty( clAlias ) )
		
			while ( .not. ( clAlias )->( eof() ) )
			
				clCodPai := ( clAlias )->NIVEL
				
				while ( .not. ( clAlias )->( eof() ) .and. ( clAlias )->NIVEL == clCodPai )
				
					if ( empty( clCodPai ) )
						clImage1 := "CONTAINR"
						clNo := clNoPai
					else
						clImage1 := "VENDEDOR"
					endif
				
					AADD( alTree, {clNo,	strZero( nlCount,7), "", allTrim( ( clAlias )->DESCID ) +; 
											" - " + allTrim( transform( ( clAlias )->PERCOT, "@9 999.99" ) ) + "% Cota" +;
											" - " + allTrim( transform( ( clAlias )->PERRET, "@9 999.99" ) ) + "% Retido";
											, clImage1, clImage2} )
										
					nlCount++
					( clAlias )->( dbSkip() )
				endDo
				
				clNo := soma1( clNo )
				
			endDo
			( clAlias )->( dbCloseArea() )
		
		endif
	
	next nlx
	
Return alTree

/*/{Protheus.doc} getLocAdmv
Responsavel por retornar os armazens envolvidos no ADMV
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return alLocais, array Bidimensional onde cada elemento � um {C�DIGO DE ARMAZ�M DO ADMV, DESCRICAO}

@type function
/*/
Static Function getLocAdmv()

	local alLocais	:= {}
	local clAlias	:= getNextAlias()
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			DISTINCT
			ZC0_ALMOX
			, NNR_DESCRI
		
		FROM
			%TABLE:ZC0% ZC0
			
		LEFT JOIN
			%TABLE:NNR% NNR ON
			NNR.NNR_FILIAL = %XFILIAL:NNR%
			AND NNR_CODIGO = ZC0_ALMOX
			
		WHERE
			ZC0.ZC0_FILIAL = %XFILIAL:ZC0%
			AND ZC0.%NOTDEL%
			
		ORDER BY ZC0_ALMOX
	
	ENDSQL
	
	( clAlias )->( dbGoTop() )
	( clAlias )->( dbEval( {|| AADD( alLocais, { ( clAlias )->ZC0_ALMOX, ( clAlias )->NNR_DESCRI } ) } ) )
	( clAlias )->( dbCloseArea() )	

Return alLocais

/*/{Protheus.doc} showInTree
Responsavel por montar o objeto de arvore para mostrar a hierarquia do cadastro de cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param alTree, array of logical, Array contendo a hierarquia do objeto dbTree
@type function
/*/
Static Function showInTree( alTree )
	
	local olDlg
	local olTree
	
	DEFINE DIALOG olDlg TITLE "An�lise de Hierarquia" FROM 180,180 TO 550,700 PIXEL
	
		// Cria o objeto 
		olTree := dbTree():New(0,0,160,260,olDlg,,,.T.)
		
		// M�todo para carga dos itens
		olTree:PTSendTree( alTree )
		olTree:ALIGN := CONTROL_ALIGN_ALLCLIENT
	
	ACTIVATE DIALOG olDlg CENTERED

Return

/*/{Protheus.doc} transfCota
Rotina responsavel por realizar a transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function transfCota()

	local clFunc	:= "ADMVA01"
	local clTitle	:= "Transf�ncia entre Cotas"
	local clDesc	:= "Este programa tem por objetivo realizar transfer�ncia entre cotas"
	local clPerg	:= "ADMVA01001"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execTransf( oSelf, MV_PAR01, MV_PAR02, MV_PAR03, MV_PAR04 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	AADD(aPergunt,{cPerg,"01","ID Cota Origem      ?","MV_CH1","C",14,0,"G","","MV_PAR01","","","","","","ZC0",""})
	AADD(aPergunt,{cPerg,"02","ID Cota Destino     ?","MV_CH2","C",14,0,"G","","MV_PAR02","","","","","","ZC0",""})
	AADD(aPergunt,{cPerg,"03","Produto             ?","MV_CH3","C",15,0,"G","","MV_PAR03","","","","","","SB1",""})
	AADD(aPergunt,{cPerg,"04","Quantidade          ?","MV_CH4","N",tamSX3("BF_QUANT")[1],tamSX3("BF_QUANT")[2],"G","","MV_PAR04","","","","","","",""})
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return

/*/{Protheus.doc} execTransf
Executa a transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param oSelf, object, Ojeto tNewProcess para manipula��o de controle de barra de progresso
@param clIdOri, characters, Codigo do ID origem do cadastro de cotas
@param clIdDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function execTransf( oSelf, clIdOri, clIdDest, clProduto, nlQuant )

	local alArea	:= getArea()
	local nlProc1	:= 2
	local nlProc2	:= 2
	local clAlmoxTrf:= ""
	local clEndDe	:= ""
	local clEndAte	:= ""
	local llProcOk	:= .F.
	local alItem	:= {}
	local alAuto	:= {}
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local clMsg		:= ""
	
	private cpErro	:= ""
	private npRecno	:= 0
	private lMsErroAuto	:= .F.

	default clIdOri		:= ""
	default clIdDest	:= ""
	default clProduto	:= ""
	default nlQuant		:= 0
	
	// Valida parametros
	if ( vldParam01(clIdOri, clIdDest, clProduto, nlQuant) )
	
		// Define quantidade de passos a serem executados para validacao de quantidades
		oSelf:setRegua1( nlProc1 )
		
		// Define quantidade de passos a serem executados para transferencia
		oSelf:SetRegua2( nlProc2 )
		
		dbSelectArea ( "ZC0" )
		ZC0->( dbSetOrder( 1 ) ) // ZC0_FILIAL, ZC0_ID, ZC0_CODPAI, R_E_C_N_O_, D_E_L_E_T_
				
		if ( ZC0->( dbSeek( xFilial("ZC0") + clIdOri ) ) )
		
			npRecno := ZC0->( recno() )
		
			U_STNXLOG( 1 , { "ADMVA01", "ZC0", npRecno, "Iniciando processo de transfer�ncia de cotas" }, @cpErro )
		
			oSelf:saveLog( "Validando saldo em estoque para transfer�ncia entre contas" )
			oSelf:incRegua1( "Validando saldo em estoque para transfer�ncia entre contas" )
			
			clAlmoxTrf	:= ZC0->ZC0_ALMOX
			clEndDe		:= U_ADMVXFUN( 7, clIdOri )[1] // Gera ID do endereco de % Cota da origem
			clEndAte	:= U_ADMVXFUN( 7, clIdDest )[1] // Gera ID do endereco de % Cota do destino

			// Valida saldo em estoque para transferencia
			oSelf:incRegua1( "Validando saldo no endereco " + clEndDe )
			llProcOk := vldEstoqTrf( clAlmoxTrf, clEndDe, clProduto, nlQuant )
			
			if ( llProcOk )
				
				oSelf:saveLog( "Montando estrutura para rotina automatica de transfer�ncia entre endere�os" )
				oSelf:incRegua2( "Montando estrutura para rotina automatica de transfer�ncia entre endere�os" )
				alItem := U_ADMVP01( 2, { nlQuant, clProduto, clAlmoxTrf, clAlmoxTrf, clEndDe, clEndAte, "TRANSFERENCIA ENTRE COTAS" } )
				
				if ( len( alItem ) > 0 )
				
					clDoc := getSxENum( "SD3", "D3_DOC", 1 )
					AADD( alAuto,{ clDoc, dDataBase } )
					AADD( alAuto, alItem )
					
					U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Processando transfer�ncia entre endere�os" }, @cpErro )
					
					begin transaction
					
						oSelf:incRegua2( "Executando rotina automatica de transfer�ncia entre endere�os" )
						MSExecAuto({|x,y| MATA261(x,y) }, alAuto, 3 )				
					
						if ( lMsErroAuto )
							
							conout("Falha no documento [" + clDoc + "] de transfer�ncia m�ltipla.")
							clArqErro  := NomeAutoLog()
							if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
								clMsgErro := memoRead( clArqErro )
								conout( clMsgErro )
								memoWrite(clArqErro," ") // Limpa log automatico
							endif
							
							rollbackSx8()
							disarmtransaction()
							
						else
							confirmSx8()
							conout("Incluido [" + clDoc + "] com sucesso!")			
						endif
						
					end transaction
					
					if ( .not. empty( clMsgErro ) )
						clMsg := "Falha no documento [" + clDoc + "] de transfer�ncia entre endere�os." + CRLF + CRLF + clMsgErro
						U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, clMsg }, @cpErro )
						EECVIEW( clMsg, "LOG DE ERRO" )
					endif
						
				endif
				
			endif
			
			U_STNXLOG( 3 , { "ADMVA01", "ZC0", npRecno, "Fim do processamento de transfer�ncia" }, @cpErro )
			
		else
			llProcOk := .F.
			clMsg := "ID [" + clIdOri + "] n�o encontrado!"
			U_ADMVXFUN( 1, {"ZC0_ID", clMsg } )
		endif
		
	else
		llProcOk := .F.
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} vldEstoqTrf
Responsavel por validar estoque referente a quantidade a ser transferida
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Existe saldo para transferencia / .F. = Nao existe saldo para transferencia 
@param clAlmoxTrf, characters, Codigo do almoxarifado
@param clEndereco, characters, Codigo do endere�o
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldEstoqTrf( clAlmoxTrf, clEndereco, clProduto, nlQuant )
	
	local alArea	:= getArea()
	local llProcOk	:= .T.
	local nlTamBFLoc:= tamSX3("BF_LOCAL")[1]
	local nlTamBFEnd:= tamSX3("BF_LOCALIZ")[1]
	local nlTamBFPrd:= tamSX3("BF_PRODUTO")[1]
	local clMsg		:= ""
	local llLocaliz	:= .F.
	local nlQtDisp	:= 0
	
	U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Validando saldo no endereco [" + clEndereco + "]" }, @cpErro )
	
	// Checa se o produto controla endereco  
	llLocaliz := U_ADMVXFUN( 8, clProduto )
	
	if ( llLocaliz )
	
		// Saldo disponivel no endereco
		nlQtDisp := U_ADMVP02( 2, {clAlmoxTrf, clEndereco, clProduto} )
	
		if ( nlQtDisp > 0 )
		
			if ( nlQtDisp < nlQuant )
				llProcOk	:= .F.
				clMsg		:= "Saldo do produto [" + clProduto + "] no endere�o [" + clEndereco + "], � insuficiente para a transfer�ncia. Saldo dispon�vel: " + allTrim( transform( nlQtDisp, pesqPict( "SBF", "BF_QUANT" ) ) )
			endif
		
		else
			llProcOk	:= .F.
			clMsg		:= "N�o foi encontrado saldo do produto [" + clProduto + "] no endere�o [" + clEndereco + "] !"
		endif
		
	else
		clMsg := "Produto [" + clProduto + "] n�o controla endere�o."
	endif
	
	if ( .not. empty( clMsg ) )
		U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, clMsg }, @cpErro )
		U_ADMVXFUN( 1, {"ZC0_ID", clMsg } )
	else
		U_STNXLOG( 2 , { "ADMVA01", "ZC0", npRecno, "Estoque OK!" }, @cpErro )
	endif
	
	restArea( alArea )
	
Return llProcOk

/*/{Protheus.doc} vldParam01
Valida parametros de processamento de transferecia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Parametros validados / .F. = parametros invalidos
@param clIdOri, characters, Codigo do ID origem do cadastro de cotas
@param clIdDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldParam01(clIdOri, clIdDest, clProduto, nlQuant)

	local llRet	:= .T.

	if ( empty( clIdOri ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_ID", "ID origem deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clIdDest ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_ID", "ID destino deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clProduto ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_ID", "Produto deve ser preenchido" } )
	endif
	
	if ( llRet .and. nlQuant <= 0 )
		llRet := .F.
		U_ADMVXFUN( 1, {"ZC0_ID", "Quantidade deve ser positiva e maior que zero!" } )
	endif

Return llRet

/*/{Protheus.doc} showLogReg
Responsavel por chamar a interface de controle de log do registro posicionado na ZC0
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@type function
/*/
Static Function showLogReg()

	local alArea	:= getArea()
	local clErro	:= ""

	// Abre monitor de log especifico da Sestini conforme o recno e entidade posicionado
	U_STNXLOG( 9, { "ZC0" , ZC0->( recno() )  } , @clErro )

	restArea( alArea )

Return