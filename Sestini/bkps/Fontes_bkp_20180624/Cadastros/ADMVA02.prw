/*/{Protheus.doc} ADMVA02
Rotina de cadastro de parametros do ADMV
@author DS2U (SDA)
@since 14/06/2018
@version 1.0
@type function
/*/
User Function ADMVA02()

	olParams := paramByMv():new()
	
	olParams:setGroups( { "Alocaçoes", "Comercial", "Financeiro" } )
	olParams:setMVs( { {"ES_DIAS",1}, {"ES_DIAS2",1} } )

Return

/*
	local alPrompts	:= {"Alocação", "Comercial", "Financeiro"}
	local alSize	:= MsAdvSize()	
	local olDlg
	local olLayer
	local olFolder
	local olPainelC
	local olPainelR
	local llSave	:= .F.
	
	 // Tratamento para MDI
	if setMDIChild()
		alSize[5] := alSize[5] - 12
		alSize[3] := alSize[5] / 2
	endif

	DEFINE MSDIALOG olDlg TITLE "Parametrização do ADMV" FROM alSize[7],0 TO alSize[6],alSize[5] OF oMainWnd PIXEL
	
		olDlg:lEscClose := .F.
		
		olLayer	:= fwLayer():new()
		olLayer:init( olDlg, .F. )
		olLayer:addLine("CABECALHO", 88, .T. )
		olLayer:addLine("RODAPE", 12, .T. )
		
		olLayer:addCollumn( "CENTERC" , 100, .F., "CABECALHO" )
		olLayer:addCollumn( "CENTERR" , 100, .F., "RODAPE" )
		
		olLayer:addWindow( "CENTERC", "olPainelC", "Configuração", 100, .F., .F.,,"CABECALHO",{|| })
		
		olPainelC := olLayer:getWinPanel("CENTERC","olPainelC","CABECALHO")
		olPainelR := olLayer:getColPanel( "CENTERR", "RODAPE" )
		
		olFolder := TFolder():New( 0,0,alPrompts,,olPainelC,,,,.T.,,260,184 )
		olFolder:align := CONTROL_ALIGN_ALLCLIENT
		olFolder:bSetOption := {|| getParamB( olFolder:nOption ) }
		
		olBtnOk		:= TButton():New(15, 5,"Gravar", olPainelR,{|| llSave := .T., olDlg:End() },040,015,,,,.T.,,"",,,,)
		olBtnSair	:= TButton():New(15, 60,"Sair", olPainelR,{|| llSave := .F., olDlg:End() },040,015,,,,.T.,,"",,,,)
		
	ACTIVATE MSDIALOG olDlg CENTERED
	
	if ( llSave )
	endif
	

Return

Static Function getParamB( nlFldOpt )

	local uRet			:= nil
	local alParamBox	:= {}
	local clTitulo		:= "Parâmetros"
	local alButtons		:= {}
	local llCentered	:= .T.
	local nlPosx		:= Nil
	local nlPosy		:= Nil
	local clLoad		:= ""
	local llCanSave		:= .T.
	local llUserSave	:= .F.
	local llRet			:= .T.
	local clVldDt		:= ".T."
	local clVldSA11		:= ".T."
	local blOk			:= {|| &clVldDt }
	local alParams		:= {}
	
	// Aba alocacoes
	if ( nlFldOpt == 1 )
	
		AADD(alParamBox,{1,"Dias de reserva"			, 0,"@9",clVldSA11,"","",80,.F.})
		AADD(alParamBox,{1,"Dias com Baixa Prioridade"	, 0,"@9",clVldSA11,"","",80,.F.})
		AADD(alParamBox,{1,"Dias Intenção de Compras"	,0 ,"@9",clVldSA11,"","",80,.F.})
		
	endif
*/
	//if ( len( alParamBox ) > 0 )
	//	llRet := ParamBox( alParamBox, clTitulo, alParams, blOk, /*alButtons*/, /*llCentered*/, /*nlPosx*/, /*nlPosy*/,, /*clLoad*/, llCanSave, /*llUserSave*/)
	//endif


//Return
>>>>>>> branch 'master' of https://ds2u@bitbucket.org/ds2utec/sestini.git
