/*/{Protheus.doc} MA410BOM
Ponto de entrada para inclus�o dos produto da estrutura do produtos no pedido de venda.

@author DS2U (SDA)
@since 08/06/2018
@version 1.0

@type function
/*/
User Function MA410BOM()

	local aBOM	:= PARAMIXB[1]
	//*****************************
	// TODO PONTOS DE ATEN��O
	//*****************************
	// DEVERA SER INFORMADO NO ACOLS O PRE�O, O DESCONTO DE CADA ITEM E CAMPO CUSTOMIZADO DE QUE AQUELE COMPONENTE VEIO DE KIT

	/*
	//������������������������������������������������������Ŀ
	//� Adiciona os produtos no aCols                        �
	//��������������������������������������������������������
	For nX := 1 To Len(aBOM)
		cItem := aCols[Len(aCols)][nPItem]
		aAdd(aCOLS,Array(Len(aHeader)+1))
		For nY	:= 1 To Len(aHeader)
			If ( AllTrim(aHeader[nY][2]) == "C6_ITEM" )
				aCols[Len(aCols)][nY] := Soma1(cItem)
			Else
				If (aHeader[nY,2] <> "C6_REC_WT") .And. (aHeader[nY,2] <> "C6_ALI_WT")				
					aCols[Len(aCols)][nY] := CriaVar(aHeader[nY][2])
				EndIf	
			EndIf
		Next nY
		N := Len(aCols)
		aCOLS[N][Len(aHeader)+1] := .F.
		A410Produto(aBom[nX][1],.F.)
		aCols[N][nPProduto]      := aBom[nX][1]
		A410MultT("M->C6_PRODUTO",aBom[nX][1])
		If ExistTrigger("C6_PRODUTO")
			RunTrigger(2,N,Nil,,"C6_PRODUTO")
		Endif

		A410SegUm(.T.)
		A410MultT("M->C6_QTDVEN",aBom[nX][2])
		If ExistTrigger("C6_QTDVEN ")
			RunTrigger(2,N,Nil,,"C6_QTDVEN ")
		Endif
		If Empty(aCols[N][nPTotal]) .Or. Empty(aCols[N][nPTES])
			aCOLS[N][Len(aHeader)+1] := .T.
		EndIf
	Next nX
	*/

Return 