#include 'protheus.ch'
#include 'parmtype.ch'
#include "fileio.ch"
#include "TopConn.ch"


/*/{Protheus.doc} ADMVP04
//Rotina para gera��o do Pedido M�e de Vendas. 
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
user function ADMVP04()

Local aSC5Area 		:= SC5->(GetArea())
Local cArqCSV 		:= "" 
Local lOk			:= .F. 

// ------------------------------------------------------------------------------------
// Rotina que validar� se o pedido � m�e e/ou pode ser distribuido
lOk := FVldPm()

If lOk 
	If MsgYesNo("Deseja realizar Distribui��o deste Pedido M�e: " + AllTrim(SC5->C5_FILIAL ) + " - " + AllTrim(SC5->C5_NUM )  ) 
		cArqCSV := AllTrim(cGetFile( "Arquivo csv|*.csv",  "Procurar" , 0,, .T., GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE, .T. )) 
		If !Empty(cArqCSV)
			
			MsgRun( "Aguarde... Lendo arquivo CSV..." , "Pedido M�e de Vendas" , {|| FPrepPeds(cArqCSV) })
			
		EndIf 
		
	EndIf 
EndIf


SC5->(RestArea(aSC5Area)) 

return nil 


/*/{Protheus.doc} FGeraPeds
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FPrepPeds(cArqCSV)

Local oMemo		:= NIL 
Local oDlg		:= NIL 
Local cTexto	:= "" 
Local cMask     := "Arquivos Texto" + "(*.TXT)|*.txt|"
Local lContinua	:= .T. 
Local cMsgErro 	:= "" 
Local aDadosCSV	:= "" 
Local cArqLog 	:= "" 

Default cArqCSV	:= "" 

// ------------------------------------------------
// arquivo de Log 
cArqLog := SubStr(AllTrim(cArqCSV),1,At(".csv",cArqCSV))+"_log_"+StrTran(AllTrim(Time()),":","")+"_.csv"


// ------------------------------------------------------------------
// Validacoes 
// ------------------------------------------------------------------
If !File(cArqCSV) 
	lContinua 	:= !lContinua 
	cMsgErro 	:= "N�o encontraod o Arquivo " + AllTrim(cArqCSV) + ". Verifique o Arquivo selecionado e tente novamente." 
EndIf 



// --------------------------
// Processa 
If lContinua 

	aDadosCSV := LeArqCSV(cArqCSV,cArqLog)
	
	If Len(aDadosCSV) > 0 
		FGeraPVs(aDadosCSV)
	EndIf 

EndIf 

// --------------------------------------------
// Exibe mensagem de Erro 
If !lContinua .and. !Empty(cMsgErro)
	Aviso("Aviso", cMsgErro , {"Voltar"}, 2)	
EndIf 


// --------------------------------------------
// Chama fun��o de LOG - Final do Processamento 
u_SesSwLog(cArqLog)


Return Nil 



/*/{Protheus.doc} LeArqCSV
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}
@param cArqCSV, characters, descricao
@type function
/*/
Static Function LeArqCSV(cArqCSV,cArqLog)

Local aSA1Area		:= SA1->(GetArea())
Local aSB1Area 		:= SB1->(GetArea())
Local cCpoGrpVen	:= "A1_XGRPEMP" 			// Campo que defini grupo de vendas, grupo empresarial, antigo A1_GRPVEN - Substituido na rotina em 23/07/18  
Local aDados 		:= {} 
Local aRet 			:= {} 
Local nHD1 			:= 0  
Local nBytes		:= 0 
Local nPos			:= 0 
Local cChar			:= "" 
Local cBuffer		:= "" 
Local nLinha		:= 0 
Local cSeparador	:= ";"
Local cLogLine		:= "" 
Local cGrpCliPM		:= AllTrim(Posicione("SA1",1,xFilial("SA1")+ SC5->(C5_CLIENTE+C5_LOJACLI) , cCpoGrpVen ))
Local cFlagNotOk	:= "ERRO"
Local cFlagOk		:= "SUCESSO"
Local nTamProd		:= TamSX3("B1_COD")[1]
Local nTamCGC		:= TamSX3("A1_CGC")[1]

Local cCodCli		:= ""									// Codigo do cliente encontrado pelo CNPJ do arquivo CSV 
Local cLojaCli		:= "" 									// Loja do cliente encontrado pelo CNPJ do arquivo CSV 
Local nRecnoSC6		:= 0 									// Recno do item encontrado no pedido de vendas original 
	
// ---------------------------------------------------------
// Posicionamento  das colunas - Posi��o arq. texto 
Local nPosCGC	:= 1
Local nPosProd	:= 2
Local nPosQtd	:= 3 
Local nPosPCli	:= 4 

nHdl := Fopen(cArqCSV)

If nHdl <> -1

	nBytes := FSeek(nHdl, 0, 2)
	FSeek(nHdl, 0)
	
	If nBytes > 0
		ProcRegua(nBytes)
		
		For nPos := 1 To nBytes
		
			IncProc()
			FRead(nHdl, @cChar, 1)
			
			// Quebra de linha (chr(13)+chr(10) = CR+LF)
			If cChar == Chr(10) .Or. nPos == nBytes
				
				If !Empty(cBuffer)
				
					lContinua := .T. 
					nLinha++ 
					
					If nLinha <> 1	// Pula linha 1 de cabe�alho
					
						aDados 	:= Separa(AllTrim(cBuffer),cSeparador)
						
						
						// Algumas valida��es 
						// ------------------------------------------------------------------------------------------------------------
						// CPNJ Existente
						 
						aDados[nPosCGC]	 := PadR(AllTrim(aDados[nPosCGC]),nTamCGC) 
						SA1->(dbSetOrder(3)) // A1_FILIAL + A1_CGC 
						SA1->(dbGoTop())
						If lContinua .And. !SA1->( MsSeek( xFilial("SA1") +  AllTrim(aDados[nPosCGC]) )  ) 
					
							lContinua 	:= .F.
							cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
							cLogLine	+= "Cliente com CNPJ: " +  AllTrim(aDados[nPosCGC]) + " n�o encontrado na base de dados."
							u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
						
						Else
							
							
							// -----------------------------------------------------------------------------------------------
							// Valida se o registro no arquivo CSV � igual ao Grupo de venda do cliente do PM 
							If AllTrim(cGrpCliPM) <> Alltrim(SA1->&cCpoGrpVen)
							
								lContinua 	:= .F.
								cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
								cLogLine	+= "Cliente com CNPJ: " +  AllTrim(aDados[nPosCGC]) + " possui um grupo "
								u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
							
							
							Else 
							
								cCodCl 		:= SA1->A1_COD 
								cLojaCli 	:= SA1->A1_LOJACLI 
								
							EndIf 
							
							
						EndIf 

						
						// ------------------------------------------------------------------------------
						// Produto existente na base 
						aDados[nPosProd]	 := PadR(AllTrim(aDados[nPosProd]),nTamProd) 
						SB1->(dbSetOrder(1)) // B1_FILIAL + B1_COD 
						SB1->(dbGoTop())
						If lContinua .And. !(SB1->(MsSeek( xFilial("SB1") + aDados[nPosProd]     ))) 
							lContinua 	:= .F.
							cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
							cLogLine	+= "Produto " +  AllTrim(aDados[nPosProd]) + " n�o encontrado na base de dados."
							u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
							
						
						EndIf  
						
						
						// ---------------------------------------------
						// Quantidade informada no arquivo ser V�lida 
						If lContinua 
							aDados[nPosQtd]	:= Val(aDados[nPosQtd]) 
							If aDados[nPosQtd] <= 0 
								lContinua 	:= .F.
								cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
								cLogLine	+= "Valor < = zero(0). N�o necess�rio realizar a transferencia" 
								u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
							EndIf
						EndIf 
						
						
						
						// ------------------------------------------------------------------------------
						// Validando se o produto do arquivo CSV existe no pedido de venda 
						If lContinua 
							
							// **** Continuar aqui 
							aItensSC6 := FGetSc6(SC5->C5_PEDIDO ) // SC6 - indice 2 - C6_FILIAL + C6_PRODUTO + C6_NUM 
							If Len(aItensSC6) <= 0 
							
								lContinua 	:= .F.
								cLogLine	:= AllTrim(StrZero(nLinha,5)) + cSeparador + cFlagNotOk + cSeparador
								cLogLine	+= "N�o existe itens no pedido de vendas M�e " + AllTrim(C5->C5_PEDIDO) + " com o produto " + aDados[nPosProd] 
								u_SesWrLog( cArqLog , cLogLine , .T. , .T. , ,  , .T. )	
							
							EndIf 
							
							
						EndIf 
						
						
						// -----------------------------------------
						// Valida Saldo  - Caso a quantidade no arquivo csv seja maior que a quantidade disponivel n�o distribui nada
						// Lembrar sempre de ver todos os itens 
						If lContinua 
						
						
						EndIf 
						
						
						
						
						
						
						// ------------------------------------------------------------------------------------------------------------
						// Fim das valida��es 
						
						
						// -------------------------------------
						// Adiciona Linha 
						// -------------------------------------
						If lContinua 
							aAdd(aRet, {		cCodCli,;
												cLojaCli,;
												aDados[nPosProd]	,; 
												aDados[nPosQtd]		,; 
												aDados[nPosPCli] 	,;
												nRecnoSC6		   })
							
						EndIf 
									
					EndIf 
					
				Endif
				
				cBuffer := "" 
				
			ElseIf cChar <> Chr(13)
			
				cBuffer += cChar
				
			EndIf 
			
		Next nPos 
		
	EndIf 
	FClose(nHdl)
EndIf 


SB1->(RestArea(aSB1Area))
SA1->(RestArea(aSA1Area))

Return aRet  


/*

	
AutoGrLog( Replicate( "-", 128 ) )
AutoGrLog( Replicate( " ", 128 ) )
AutoGrLog( "LOG DA ATUALIZA��O DOS DICION�RIOS" )
AutoGrLog( Replicate( " ", 128 ) )
AutoGrLog( Replicate( "-", 128 ) )
AutoGrLog( " " )
AutoGrLog( " Dados Ambiente" )
AutoGrLog( " --------------------" )
AutoGrLog( " Empresa / Filial...: " + cEmpAnt + "/" + cFilAnt )
AutoGrLog( " Nome Empresa.......: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_NOMECOM", cEmpAnt + cFilAnt, 1, "" ) ) ) )
AutoGrLog( " Nome Filial........: " + Capital( AllTrim( GetAdvFVal( "SM0", "M0_FILIAL" , cEmpAnt + cFilAnt, 1, "" ) ) ) )
AutoGrLog( " DataBase...........: " + DtoC( dDataBase ) )
AutoGrLog( " Data / Hora �nicio.: " + DtoC( Date() )  + " / " + Time() )
AutoGrLog( " Environment........: " + GetEnvServer()  )
AutoGrLog( " Usu�rio TOTVS .....: " + __cUserId + " " +  cUserName )



*/




/*/{Protheus.doc} FVldPm
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FVldPm()	

Local lRet 		:= .T. 
Local cMsg		:= "" 
Local cGrpCliPM		:= AllTrim(Posicione("SA1",1,xFilial("SA1")+ SC5->(C5_CLIENTE+C5_LOJACLI) ,cCpoGrpVen ))

// ----------------------------------------------------------------
// Verifica se o pedido posicionado � o pedido m�e 
If !("1" $ AllTrim(SC5->C5_XISPM)) 
	lRet := .F. 
	cMsg := "O Pedido posicionado " + AllTrim(SC5->C5_FILIAL) + " - " + AllTrim(SC5->C5_NUM) + " n�o � um pedido m�e. " + CRLF + "Verifique a propriedade do pedido."
EndIf 


// -----------------------------------------------------------
// Verifica se o cliente do PM tem grupo de venda amarrado. 
If lRet .and. Empty(cGrpCliPM)
	lRet := .F. 
	cMsg := "O Cliente " + AllTrim( SC5->(C5_CLIENTE+C5_LOJACLI) ) + " n�o possui um grupo de venda atrelado." + CRLF + "Verifique o campo " + cCpoGrpVen + " no cadastro de Clientes."
EndIf 

// ---------------------------------------
// Msg de retorno 
If !lRet .and. !Empty(cMsg)
	Aviso("Aviso", cMsg , {"Voltar"}, 2)	
EndIf 



Return lRet 










