#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"
#Include "Totvs.Ch"

//-----------------------------------------------
/*/{Protheus.doc} ADMVA02
Rotina de cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVA02()

	Local cCodigo	:= "000001"
	Local nOperation:= 0
	Local cTitle	:= "Parametriza��es ADMV"

	dbSelectArea('ZC6')
	ZC6->(dbSetOrder( 1 ))
	If ZC6->( dbSeek( FWxFilial("ZC6") + cCodigo ) )
		nOperation := MODEL_OPERATION_UPDATE
	Else
		nOperation := MODEL_OPERATION_INSERT
	EndIf

    If U_FVLA2Usr( __cUserId )

        CursorWait()
        
        FWExecView( cTitle ,"ADMVA02",nOperation,,{|| .T.})		

        CursorArrow()

    EndIf

Return

//-----------------------------------------------
/*/{Protheus.doc} ModelDef
Model do cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ModelDef()
	
	Local oStruZC6	:= FWFormStruct(1,'ZC6')
	Local oModel	:= MPFormModel():New( 'ADMV2MVC',,, )
	
	oModel:SetDescription("Parametriza��es ADMV")
	
	oModel:AddFields( 'ZC6MASTER', , oStruZC6 )

	oModel:SetPrimaryKey({"ZC6_CODIGO"})

Return oModel

//-----------------------------------------------
/*/{Protheus.doc} viewdef
View do cadastro de parametros do ADMV

@author DS2U (HFA)
@since 11/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function viewdef()
	
	Local oModel		:= FWLoadModel("ADMVA02")
	Local oView			:= FWFormView():New()
	Local oStruZC6		:= FWFormStruct(2,'ZC6' )
	
	oView:SetModel( oModel )
	
	oView:AddField('VIEW_ZC6'	,oStruZC6	,'ZC6MASTER')
	
	oView:CreateHorizontalBox('CABEC'	,100)
	oView:SetOwnerView('VIEW_ZC6'	,'CABEC' 	)

    oView:AddUserButton("Usu�rios Libera Bloqueios",'CLIPS',{|| U_ADMVA08() })
	
	oView:SetCloseOnOk({||.T.})

Return oView

//-----------------------------------------------
/*/{Protheus.doc} FVLA2Usr
Valida��o dos usu�rios quem possuem acesso
a rotina, para manuten��o do cadastro de 
parametriza��es ADMV

@author DS2U (HFA)
@since 26/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function FVLA2Usr( cUserAcess )
	
	Local lRet 		:= .F.
	Local cUsers	:= Alltrim(GetMV("ES_USCADMV",,"")) // ADMV - Usuarios com permiss�o de cadastro dos par�metros ADMV

	If cUserAcess $ cUsers
		lRet := .T.
	Else
		Help(,, "FVLA2USR",, "Usu�rio sem permiss�o para acessar a rotina. Entre em contato com o departamento de TI, para verifica��o do par�metro ES_USCADMV.", 1, 0)
	EndIf
	
Return lRet

//-----------------------------------------------
/*/{Protheus.doc} ADMVSNdMail
ADMV Processamento 4 - FUN��O GEN�RICA PARA 
ENVIO DE E-MAIL.

@author DS2U (HFA)
@since 13/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVSNdMail(cAssunto, cMensagem, aAnexos, cEmailTo, cEmailCc, cErro , lAlert , lFormat )

    Local lRetorno 	:= .T.
    Local cServer   := GetMv("MV_RELSERV",,"")
    Local cAccount	:= GetMv("MV_RELACNT",,"")
    Local cPassword	:= GetMv("MV_RELAPSW",,"")
    Local lMailAuth := GetMv("MV_RELAUTH",,.F.)
    Local nTimeOut  := GetMv("MV_RELTIME",,120)

    Default cEmailTo 	:= ""
    Default cEmailCc 	:= ""
    Default aAnexos		:= {}
    Default cMensagem	:= ""
    Default cAssunto	:= ""
    Default cErro		:= ""
    Default lAlert      := .T.
    Default lFormat   := .T.

    If !Empty(cEmailTo) .And. !Empty(cAssunto) .And. !Empty(cMensagem)
        
        If MailSmtpOn( cServer, cAccount, cPassword , nTimeOut )
            
            If lMailAuth
                
                If ! ( lRetorno := MailAuth(cAccount,cPassword) )
                    
                    lRetorno := MailAuth(SubStr(cAccount,1,At("@",cAccount)-1),cPassword)
                    
                EndIf
                
            Endif
            
            If lRetorno
                
                If !MailSend(cAccount,{cEmailTo},{cEmailCc},{},cAssunto,cMensagem,aAnexos, lFormat )

                    cErro := "Erro na tentativa de e-mail para " + cEmailTo + ". ErrorMail: " + Mailgeterr()
                    lRetorno := .F.
                    
                EndIf
                
            Else
                
                cErro := "Erro na tentativa de autentica��o da conta " + cAccount + ". "
                lRetorno := .F.
                
            EndIf
            
            MailSmtpOff()
            
        Else
            
            cErro := "Erro na tentativa de conex�o com o servidor SMTP: " + cServer + " com a conta " + cAccount + ". "
            lRetorno := .F.
            
        EndIf

    Else
        
        If Empty(cEmailTo)
            
            cErro := "� neess�rio fornecedor o destin�tario para o e-mail. "
            lRetorno := .F.
            
        EndIf
        
        If Empty(cAssunto)
            
            cErro := "� neess�rio fornecedor o assunto para o e-mail. "
            lRetorno := .F.
            
        EndIf
        
        If Empty(cMensagem)
            
            cErro := "� neess�rio fornecedor o corpo do e-mail. "
            lRetorno := .F.
            
        EndIf
        
    Endif

    If lAlert .And. !IsBlind()
        If !Empty(cErro)
            ApMsgInfo(cErro)
        Else
            ApMsgInfo("e-mail enviado com sucesso!")
        EndIf
    Endif

Return(lRetorno)