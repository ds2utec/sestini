#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA04
Cadastro de Vendedor x Regiao
@author DS2U (SDA)
@since 24/06/2018
@version 1.0

@type function
/*/
User Function ADMVA04()
	
	local alArea	:= ZC2->( getArea() )

	private oBrowse

	dbSelectArea("ZC2")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("ZC2")
	oBrowse:SetDescription("Cadastro de Vendedor x Regi�o")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA04"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA04"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA04"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA04"	OPERATION 5 ACCESS 0 // "Excluir" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruZC2	:= FWFormStruct( 2, 'ZC2' )
	local olModel	:= FWLoadModel( 'ADMVA04' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_ZC2', olStruZC2, 'ZC2MASTER' )
	
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 100 )
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_ZC2', 'SUPERIOR' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruZC2	:= FWFormStruct( 1, 'ZC2', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA04M', /* bPreValidacao*/,/*bPosValidacao { |olModel| admvTudoOk( olModel ) }*/,/*bCommit { |olModel| admvCommit( olModel ) }*/, /*{ |olModel| admvCancel( olModel ) }bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'ZC2MASTER', /*cOwner*/, olStruZC2, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"ZC2_FILIAL", "ZC2_UF"} )
	
	olModel:SetDescription('Vendedor x Regi�o')
	olModel:GetModel('ZC2MASTER' ):SetDescription('Vendedor x Regi�o')
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel
