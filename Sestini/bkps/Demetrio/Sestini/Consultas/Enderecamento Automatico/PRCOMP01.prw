#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} PRCOMP01
Funcao de regras para controle de enderecamento automatico na entrada de documento fiscal
@author Triyo (SDA)
@since 19/12/2016
@version undefined
@param nlOpc, numeric, opcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function PRCOMP01( nlOpc, uParam )

	local uRet
	
	default nlOpc := 0
	
	do Case
	
		case nlopc == 0
			uRet := getLocalPrd( uParam )
			
		case nlopc == 1
			msgRun("Realizando endere�amento autom�tico...","Aguarde!", {|| uRet := enderecar() } )
	
	endCase

Return uRet

/*/{Protheus.doc} enderecar
Funcao de checagem de itens da nota para enderecamento automatico.
@author Triyo (SDA)
@since 15/12/2016
@version undefined

@type function
/*/
Static Function enderecar()

	local alArea	:= getArea()
	local nlOpcao	:= ParamIxb[1] // 3 inclusao, 4-classificar
	local nlConf	:= ParamIxb[2]
	local alInfo
	
	/**
	  * Somente se:
	  * O tipo da nota for normal
	  * Inclusao ou Classifica��o de NF
	  * Confirmou operacao
	  */
	if ( SF1->F1_TIPO == "N" .and. ( nlOpcao == 3 .or. nlOpcao == 4 ) .and. nlConf == 1 )
	
		dbSelectArea("SD1")
		SD1->( dbSetOrder( 1 ) ) // D1_FILIAL + D1_DOC + D1_SERIE + D1_FORNECE + D1_LOJA
		
		dbSelectArea("SB1")
		SB1->( dbSetOrder( 1 ) )
		
		if ( SD1->( dbSeek( FWxFilial("SD1") + SF1->F1_DOC + SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA ) ) )
		
			alInfo	:= separa( allTrim( getMv("ES_ENDRPRD") ), ";" )
		
			while	(	.not. SD1->( eof() );
			 			.and. SD1->D1_FILIAL == SF1->F1_FILIAL;
			 			.and. SD1->D1_DOC == SF1->F1_DOC;
			 			.and. SD1->D1_SERIE == SF1->F1_SERIE; 
			 			.and. SD1->D1_FORNECE == SF1->F1_FORNECE; 
			 			.and. SD1->D1_LOJA == SF1->F1_LOJA;  
					)
		
				if ( SB1->( dbSeek( xFilial("SB1") + SD1->D1_COD ) ) )
				
					/**
					  * Checa se o produto tem rastreamento habilitado 
					  * para realizar procedimento de enderecamento automatico
					  */
					if ( SB1->B1_LOCALIZ == "S" )
						enderecPrd( alInfo )
					endif
				
				endif
		
				SD1->( dbSkip() )
			endDo
		
		endif
	
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} enderecPrd
Funcao que executa a rotina automaca de enderecamento conforme item do documento de entrada e produto posicionado
@author Triyo (SDA)
@since 15/12/2016
@version undefined

@type function
/*/
Static Function enderecPrd( alInfo )

	local alArea	:= getArea()
	local alCabEnd	:= {}
	local alItensEnd:= {}
	local clLocaliz
	local clArmazem
	local nlReg		:= 0
	local alAreaD1
	local nlTamItem	:= tamSX3("DB_ITEM")[1]

	private	lMsErroAuto := .F.
	
	default alInfo	:= separa( allTrim( getMv("ES_ENDRPRD") ), ";" )
	
	if ( len( alInfo ) == 2 )
	
		clArmazem	:= allTrim( alInfo[1] )
		clLocaliz	:= PADR( allTrim( alInfo[2] ), tamSX3("BE_LOCALIZ")[1] )
	
		if ( .not. empty( clArmazem ) .and. .not. empty( clLocaliz ) )
		
			/**
			  * Verifica se o almoxafirado j� existe para o produto
			  */
			staticCall(PRRXFUN, CHKSB2, SD1->D1_COD, clArmazem)
		
			nlReg := SD1->(Recno())
			alAreaD1 := SD1->( getArea() )
		
			alCabEnd  := {	{"DA_PRODUTO"	, SD1->D1_COD		, NIL},;
					  		{"DA_LOCAL"		, clArmazem			, .F.},;
					 		{"DA_NUMSEQ"	, SD1->D1_NUMSEQ	, NIL},;
							{"DA_DOC"		, SD1->D1_DOC		, NIL},;
							{"DA_SERIE"		, SD1->D1_SERIE		, NIL},;
							{"DA_CLIFOR"	, SD1->D1_FORNECE	, NIL},;
							{"DA_LOJA"		, SD1->D1_LOJA		, NIL},;
							{"DA_TIPONF"	, SD1->D1_TIPO		, NIL},;
							{"DA_ORIGEM"	, 'SD1'				, NIL}	}
		
			alItensEnd:= {{	{"DB_ITEM"		, strZero(1, nlTamItem), NIL},;
							{"DB_LOCALIZ"	, clLocaliz			, NIL},;
							{"DB_QUANT"		, SD1->D1_QUANT		, NIL},;
							{"DB_DATA"		, dDataBase			, NIL},;
							{"DB_LOTECTL"	, SD1->D1_LOTECTL	, NIL},;
							{"DB_NUMLOTE"	, SD1->D1_NUMLOTE 	, NIL} }}
		
			dbSelectArea("SBE")
			SBE->( dbSetorder( 1 ) )
			SBE->( dbSeek( FWxFilial("SBE") + clArmazem + clLocaliz  ) )
			
			begin transaction
				
				MSExecAuto({|x,y,z| MATA265(x,y,z)}, alCabEnd, alItensEnd, 3)
		
				if ( lMSErroAuto )
					mostraErro()	
					disarmTransacTion()		
				endif
				
			end transaction
			
			restArea( alAreaD1 )
			SD1->( dbGoto( nlReg ) )
			
		else
			msgAlert("Par�metro ES_ENDRPRD est� configurado errado. Formato correto:" + CRLF + "<Armazem>;<Endereco>" + CRLF + "Exemplo: 02;RECEBIMENTO")
		endif
		
	endif
	
	restArea( alArea )
	
Return

/*/{Protheus.doc} getLocalPrd
Funcao customizada para identificacao do armazem de entrada de produtos a enderecar
@author Triyo (SDA)
@since 19/12/2016
@version undefined

@type function
/*/
Static Function getLocalPrd( nlItemPos )

	local alArea	:= getArea()
	local nlPosPrd	:= 0
	local nlPosLoc	:= 0
	local clProduto	:= ""
	local clLocal	:= ""
	local alInfo	:= {}
	
	default nlItemPos := N
	
	/**
	  * Identifica se esta sendo processado da solicita��o de compras, pedido de compras ou documento de entrada
	  */
	if ( isInCallStack("MATA110") )
	
		nlPosPrd	:= aScan(aHeader,{|x| allTrim( x[2] ) == "C1_PRODUTO"} )
		nlPosLoc	:= aScan(aHeader,{|x| allTrim( x[2] ) == "C1_LOCAL"} )
	
	elseif ( isInCallStack("MATA121") )
		
		nlPosPrd	:= aScan(aHeader,{|x| allTrim( x[2] ) == "C7_PRODUTO"} )
		nlPosLoc	:= aScan(aHeader,{|x| allTrim( x[2] ) == "C7_LOCAL"} )
		
	elseif ( isInCallStack("MATA103") .or. isInCallStack("MATA140") )
		
		nlPosPrd	:= aScan(aHeader,{|x| allTrim( x[2] ) == "D1_COD"} )
		nlPosLoc	:= aScan(aHeader,{|x| allTrim( x[2] ) == "D1_LOCAL"} )
		
	endif
	
	if ( nlPosPrd > 0 .and. nlPosLoc > 0 )
	
		clProduto	:= aCols[nlItemPos][nlPosPrd]
		clLocal		:= aCols[nlItemPos][nlPosLoc]
	
		dbSelectArea("SB1")
		SB1->( dbSetOrder( 1 ) )
		
		if ( SB1->( dbSeek( FWxFilial("SB1") + clProduto ) ) )
		
			if ( SB1->B1_LOCALIZ == "S" )
				
				/**
				  * Parametro de local e endereco padrao de enderecamento automatico
				  */
				alInfo	:= separa( allTrim( getMv("ES_ENDRPRD") ), ";" )
				
				if ( valType( alInfo ) == "A" .and. len( alInfo ) > 1 .and. .not. empty( alInfo[1] ) )
					clLocal := allTrim( alInfo[1] )
				endif
				
			endif
		
		endif
		
	endif
	
	restArea( alArea )
	
Return clLocal