#INCLUDE 'PROTHEUS.CH'

/*/{Protheus.doc} MT103FIM
Alimentar a tabela ZZ5 caso seja nota de devolu��o
@type function
@author Victor Andrade
@since 13/10/2016
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/User Function MT103FIM()

Local cTipo 	:= SF1->F1_TIPO
Local nPosNfOri	:= AScan(aHeader,{|x| AllTrim(x[2]) == "D1_NFORI"})
Local nPosSrOri	:= AScan(aHeader,{|x| AllTrim(x[2]) == "D1_SERIORI"})
Local nPosProd	:= AScan(aHeader,{|x| AllTrim(x[2]) == "D1_COD"})
Local nPosItOri	:= AScan(aHeader,{|x| AllTrim(x[2]) == "D1_ITEMORI"})
Local i			:= 0
Local aArea		:= GetArea()

// DESABILITADO. COMISSAO SERA CALCULADA POR ROTINA DE REPROCESSAMENTO
//ZZ5->( DbSetOrder(1) )
//
//If cTipo == "D"
//	
//	For i := 1 To Len(aCols)
//		
//		If !(aCols[i][Len(aCols[i])])
//			
//			If ZZ5->( DbSeek( xFilial("ZZ5") + "1" + aCols[i][nPosNfOri] + aCols[i][nPosSrOri] + aCols[i][nPosProd] + aCols[i][nPosItOri] ) )
//			
//				/**
//				  * Refaz registros de apura��es de comissoes caso o item j� tenha DEVOLU��ES
//				  */
//				U_FATPDZZ5(	aCols[i][nPosNfOri],;
//							aCols[i][nPosSrOri],;
//							SF1->F1_FORNECE,;
//							SF1->F1_LOJA,;
//							ZZ5->ZZ5_ITMPV,;
//							aCols[i][nPosProd],;
//							"1"/*clTipo*/,;
//							ZZ5->ZZ5_VEND, ""/*clPedido*/,;
//							SF1->F1_DOC,;
//							ZZ5->ZZ5_PERC,;
//							ZZ5->ZZ5_MARKUP,;
//							ZZ5_TIPO )
//			
//			EndIf
//			
//		EndIf
//		
//	Next nI
//	
//EndIf

/**
  * Verifica se � necess�rio realizar enderecamento automatico
  */
U_PRCOMP01(1)

/**
  * Informacoes de campos que devem ser levados do SC7 (pedido de compras) para (SD1) item do documento de entrada
  */
setCampos()

RestArea(aArea)

Return

/*/{Protheus.doc} setCampos
Funcao auxiliar para gravar informa��es de nome do fornecedor e CGC em campos customizados para possibilitar filtros e ordena��o
@author Triyo (SDA)
@since 09/01/2017
@version undefined

@type function
/*/
Static Function setCampos()

	local alArea

	if ( .not. SF1->F1_TIPO == "D" )
	
		alArea := getArea()
		
		dbSelectArea("SA2")
		SA2->( dbSetOrder( 1 ) )
		
		if ( SA2->( dbSeek( xFilial("SA2") + SF1->F1_FORNECE + SF1->F1_LOJA ) ) )
	
			if ( recLock("SF1", .F. ) )
				SF1->F1_XNOMFRN := SA2->A2_NOME
				SF1->F1_XCGCFRN := SA2->A2_CGC
				SF1->( msUnLock() )
			endif
			
		endif
	
		restArea( alArea )
	
	endif

Return