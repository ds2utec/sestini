#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
 
/*/{Protheus.doc} PRR013A()

			Cadastro de Ceritifcados de Qualidade
		
@Author		Leandro Heilig (Triyo)
@Since		26/08/16      
@Sample		Parafusos Rudge Ramos
@Version	P12.1.7
@menu		SIGAQIE
@param		
@Return		Nil
@Obs	    Referente ao ID 13 - Inspe��o de Entrada
@history    07/12/2016 -  Inclus�o de dados de ensaio dimensional
			13/02/2017 -  Retirada das regras e dos c�lculos que consideram a nominal como
						  Ponto de partida. O resultado ser� validado dentros dos valores de m�nimo e m�ximo
						  infornados de forma absoluta (e n�o mais varia��o) no cadastro.
			 
/*/
User Function PRR013A()

	Local oBrowse
	
	oBrowse := FWmBrowse():New()
	oBrowse:SetAlias('ZZ7')
	oBrowse:SetDescription("Cadastro de Certificados de Qualidade")
		
	oBrowse:AddLegend("ZZ7->ZZ7_STATUS == '01'"	,"GREEN"	,"Vigente"              ) 		
	oBrowse:AddLegend("ZZ7->ZZ7_STATUS == '02'"	,"YELLOW"	,"Bloqueado"            ) 	
	oBrowse:AddLegend("ZZ7->ZZ7_STATUS == '03'"	,"BLACK"	,"Cancelado"            )
	oBrowse:AddLegend("ZZ7->ZZ7_STATUS == '99'"	,"BLUE"  	,"Gerado via Assistente") 	 	
	oBrowse:AddLegend("ZZ7->ZZ7_STATUS == '04'"	,"RED"		,"Incompleto"           ) 			
	
	oBrowse:Activate()	

Return

/*/{Protheus.doc} MenuDef()
			Menu da rotina
@Author		Leandro Heilig
@Since		28/08/16      
@Sample		MenuDef()
@Version	P12.1.7
@menu		SIGAQIE
@Return		Nil
@Project    Parafusos Rugde Ramos - Customiza��es 
@Obs	   	
/*/

Static Function MenuDef()

	Local aRotina := {}

	ADD OPTION aRotina TITLE 'Pesquisar'	ACTION 'PesqBrw'			OPERATION 1 ACCESS 0
	ADD OPTION aRotina Title 'Visualizar'	Action 'VIEWDEF.PRR013A'	OPERATION 2 ACCESS 0	
	ADD OPTION aRotina Title 'Incluir'		Action 'VIEWDEF.PRR013A'	OPERATION 3 ACCESS 0		
	ADD OPTION aRotina Title 'Alterar'		Action 'VIEWDEF.PRR013A'	OPERATION 4 ACCESS 0
	ADD OPTION aRotina Title 'Excluir'		Action 'VIEWDEF.PRR013A'	OPERATION 5 ACCESS 0 	    
	ADD OPTION aRotina Title 'Excluir em Lote'	Action 'U_PRR013E' 		OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title 'Gerar Cert'	Action 'U_PRR013M'    		OPERATION 5 ACCESS 0
	ADD OPTION aRotina Title 'Copiar'		Action 'U_PRR013M(1)'    	OPERATION 3 ACCESS 0	
		
Return aRotina

/*/{Protheus.doc} ModelDef()
		    Menu da rotina
@Author		Leandro Heilig
@Since		26/08/16      
@Sample		PRR013A
@Version	P12.1.7
@menu		SIGAQIE
@Return		oModel
@Project    Parafusos Rugde Ramos - Customiza��es
@Obs	   	
 
/*/
Static Function ModelDef()
	
	Local oModel
	// Cria a estrutura a ser usada no Modelo de Dados
	Local oStruZZ7  := FWFormStruct( 1, 'ZZ7', /*bAvalCampo*/, /*lViewUsado*/ )
	Local oStruZZ8Q := FWFormStruct( 1, 'ZZ8', /*bAvalCampo*/, /*lViewUsado*/ )
	Local oStruZZ9M := FWFormStruct( 1, 'ZZ9', /*bAvalCampo*/, /*lViewUsado*/ )
	Local oStruZZAT := FWFormStruct( 1, 'ZZA', /*bAvalCampo*/, /*lViewUsado*/ )
	Local oStruZZBD := FWFormStruct( 1, 'ZZB', /*bAvalCampo*/, /*lViewUsado*/ )
	Local oStruZZCD := FWFormStruct( 1, 'ZZC', /*bAvalCampo*/, /*lViewUsado*/ )
	
	
	Local oModel
	
	//-- Cria a estrutura basica	
	oModel := MPFormModel():New('PRR013AM', /* bPreValidacao*/,{ |oModel| PRR13TdOk( oModel ) }/*bPosValidacao*/, { |oModel| PRR13Commit( oModel ) }/*bCommit*/, /*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	oModel:AddFields( 'ZZ7MASTER', /*cOwner*/, oStruZZ7, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	oModel:AddGrid( 'ZZ8DETAILQ', 'ZZ7MASTER', oStruZZ8Q, {|oModel,  nLine,cAction,  cField|PRR13LPre(oModel, nLine, cAction, cField)}/*bLinePre*/, { |oModel| ZZ8VldLPos( oModel ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	oModel:AddGrid( 'ZZ9DETAILM', 'ZZ7MASTER', oStruZZ9M, {|oModel,  nLine,cAction,  cField|PRR13LPre(oModel, nLine, cAction, cField)}/*bLinePre*/, { |oModel| ZZ9VldLPos( oModel ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	oModel:AddGrid( 'ZZADETAILT', 'ZZ7MASTER', oStruZZAT, {|oModel,  nLine,cAction,  cField|PRR13LPre(oModel, nLine, cAction, cField)}/*bLinePre*/, { |oModel| ZZAVldLPos( oModel ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )	
	oModel:AddGrid( 'ZZBDETAILD', 'ZZ7MASTER', oStruZZBD, {|oModel,  nLine,cAction,  cField|PRR13LPre(oModel, nLine, cAction, cField)}/*bLinePre*/, { |oModel| ZZBVldLPos( oModel ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	oModel:AddGrid( 'ZZCDETAILD', 'ZZ7MASTER', oStruZZCD, {|oModel,  nLine,cAction,  cField|PRR13LPre(oModel, nLine, cAction, cField)}/*bLinePre*/, { |oModel| ZZCVldLPos( oModel ) }/*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )	
		

	// Faz relaciomaneto entre os compomentes do model
	oModel:SetRelation( 'ZZ8DETAILQ', { { 'ZZ8_FILIAL', 'xFilial( "ZZ8" )' }, { 'ZZ8_CODCER', 'ZZ7_CODCER' }}, ZZ8->( IndexKey( 3 ) ) )
	oModel:SetRelation( 'ZZ9DETAILM', { { 'ZZ9_FILIAL', 'xFilial( "ZZ9" )' }, { 'ZZ9_CODCER', 'ZZ7_CODCER' }}, ZZ9->( IndexKey( 3 ) ) )
	oModel:SetRelation( 'ZZADETAILT', { { 'ZZA_FILIAL', 'xFilial( "ZZA" )' }, { 'ZZA_CODCER', 'ZZ7_CODCER' }}, ZZA->( IndexKey( 1 ) ) )
	oModel:SetRelation( 'ZZBDETAILD', { { 'ZZB_FILIAL', 'xFilial( "ZZB" )' }, { 'ZZB_CODCER', 'ZZ7_CODCER' }}, ZZB->( IndexKey( 1 ) ) )	
	oModel:SetRelation( 'ZZCDETAILD', { { 'ZZC_FILIAL', 'xFilial( "ZZC" )' }, { 'ZZC_CODCER', 'ZZ7_CODCER' }}, ZZC->( IndexKey( 1 ) ) )	
			
	//-- Configura o model
	oModel:SetPrimaryKey( {"ZZ7_FILIAL", "ZZ7_CODCER"} )

	oModel:SetDescription('Gest�o de Certificados de Qualidade')
	oModel:GetModel('ZZ7MASTER' ):SetDescription('Gest�o de Certificados de Qualidade') 
	
	oModel:GetModel('ZZ8DETAILQ'):SetOptional(.T.)
	oModel:GetModel('ZZ9DETAILM'):SetOptional(.T.)
	oModel:GetModel('ZZADETAILT'):SetOptional(.T.)	
	oModel:GetModel('ZZBDETAILD'):SetOptional(.T.)
	oModel:GetModel('ZZCDETAILD'):SetOptional(.T.)	
			
	oModel:GetModel('ZZ8DETAILQ'):SetUniqueLine({"ZZ8_ENSAIO"})
	oModel:GetModel('ZZ9DETAILM'):SetUniqueLine({"ZZ9_ENSAIO"})
	oModel:GetModel('ZZADETAILT'):SetUniqueLine({"ZZA_ENSAIO"})
	oModel:GetModel('ZZBDETAILD'):SetUniqueLine({"ZZB_ENSAIO"})
	oModel:GetModel('ZZCDETAILD'):SetUniqueLine({"ZZC_ENSAIO"})
			
	//oModel:SetActivate()
	
	oModel:SetActivate( {|oModel| PRR13LOADQ( oModel ) } )

Return oModel

/*/{Protheus.doc} ViewDef()
		 Definicao da View
@author  Leandro Heilig
@since   16/09/2016
@version 1.0
@return  oView
/*/
Static Function ViewDef()

	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	Local oStruZZ7  := FWFormStruct( 2, 'ZZ7' )
	
	Local oStruZZ8Q := FWFormStruct( 2, 'ZZ8' ) // Composi��o Qu�mica
	Local oStruZZ9M := FWFormStruct( 2, 'ZZ9' ) // Propriedades Mec�nicas
	Local oStruZZAT := FWFormStruct( 2, 'ZZA' ) // Tratamentos T�rmicos
	Local oStruZZBD := FWFormStruct( 2, 'ZZB' ) // Propriedades dimensionais ME
	Local oStruZZCD := FWFormStruct( 2, 'ZZC' ) // Propriedades dimensionais PA	
	
	// Cria a estrutura a ser usada na View
	Local oModel    := FWLoadModel( 'PRR013A' )
	Local oView
	
	// Cria o objeto de View
	oView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	oView:SetModel( oModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField( 'VIEW_ZZ7', oStruZZ7, 'ZZ7MASTER' )		
		
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	oView:AddGrid( 'VIEW_ZZ8Q', oStruZZ8Q, 'ZZ8DETAILQ' )
	oView:AddGrid( 'VIEW_ZZ9M', oStruZZ9M, 'ZZ9DETAILM' )
	oView:AddGrid( 'VIEW_ZZAT', oStruZZAT, 'ZZADETAILT' )	
	oView:AddGrid( 'VIEW_ZZBD', oStruZZBD, 'ZZBDETAILD' )
	oView:AddGrid( 'VIEW_ZZCD', oStruZZCD, 'ZZCDETAILD' )			
	
	// Criar um "box" horizontal para receber algum elemento da view
	oView:CreateHorizontalBox( 'SUPERIOR', 60 )
	oView:CreateHorizontalBox( 'INFERIOR', 40 )
	
	//--Cria os Folder's
	oView:CreateFolder('FLDTP01','INFERIOR')
	
	//--Aba Propriedades Quimicas
	oView:AddSheet('FLDTP01','GRDTP01','Propriedades Quimicas')													
	oView:CreateHorizontalBox('Propriedades Quimicas'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP01')	

	//--Aba Propriedades F�sicas
	oView:AddSheet('FLDTP01','GRDTP02','Propriedades Mec�nicas')													
	oView:CreateHorizontalBox('Propriedades Mec�nicas'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP02')
	
	//--Aba Propriedades Mec�nicas
	oView:AddSheet('FLDTP01','GRDTP03','Tratamento T�rmico e Superficial')													
	oView:CreateHorizontalBox('Propriedades T�rmicas'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP03')
	
	//--Aba Propriedades Dimensionais -  Inclu�da em 07/12/2016
	oView:AddSheet('FLDTP01','GRDTP04','Propriedades Dimensionais do Item de venda')													
	oView:CreateHorizontalBox('Propriedades Dimensionais do Item de venda'	,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP04')
	
	//--Aba Propriedades Dimensionais -  Inclu�da em 10/12/2016
	oView:AddSheet('FLDTP01','GRDTP05','Propriedades Dimensionais do Item produzido')													
	oView:CreateHorizontalBox('Propriedades Dimensionais do Item produzido'	,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP05')
			
	// Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView( 'VIEW_ZZ7', 'SUPERIOR' )
	
	oView:SetOwnerView( 'VIEW_ZZ8Q', 'Propriedades Quimicas'     )
	oView:SetOwnerView( 'VIEW_ZZ9M', 'Propriedades Mec�nicas'    ) 
	oView:SetOwnerView( 'VIEW_ZZAT', 'Propriedades T�rmicas'     )
	oView:SetOwnerView( 'VIEW_ZZBD', 'Propriedades Dimensionais do Item de venda' )
	oView:SetOwnerView( 'VIEW_ZZCD', 'Propriedades Dimensionais do Item produzido' )
		
	// Define campos que terao Auto Incremento
	oView:AddIncrementField( 'VIEW_ZZ8Q', 'ZZ8_ITEM' )
	oView:AddIncrementField( 'VIEW_ZZ9M', 'ZZ9_ITEM' )
	oView:AddIncrementField( 'VIEW_ZZAT', 'ZZA_ITEM' )
	oView:AddIncrementField( 'VIEW_ZZBD', 'ZZB_ITEM' )
	oView:AddIncrementField( 'VIEW_ZZCD', 'ZZC_ITEM' )
	
	// Liga a identificacao do componente
	oView:EnableTitleView('VIEW_ZZ8Q','Composi��o Quimica'                           )
	oView:EnableTitleView('VIEW_ZZ9M','Propriedades Mec�nicas'                       )	
	oView:EnableTitleView('VIEW_ZZAT','Tratamento T�rmico e Superficial'             )	
	oView:EnableTitleView('VIEW_ZZBD','Propriedades Dimensionais do Item de venda'   )	
	oView:EnableTitleView('VIEW_ZZCD','Propriedades Dimensionais do Item produzido'  )	
	
	//Adiciona noas a��es
	oView:AddUserButton('Excluir TODOS dados', 'CLIPS', {|oView| delAll( oView ) })
	
Return oView

/*/{Protheus.doc} delAll
Funcao responsavel por deletar todos os registros das grid dos modelos das abas do cadastro de certificado
@author Triyo (SDA)
@since 06/07/2017
@version undefined
@param oView, object, description
@type function
/*/
Static Function delAll( oView )
Local oViewLocal := FWViewActive()	//Recuperando a view ativa da interface
	if ( valType( oView ) == "U" )
		oView     := FwModelActive()
	endif
	
	auxDelMod( oView:GetModel('ZZ8DETAILQ') )
	auxDelMod( oView:GetModel('ZZ9DETAILM') )
	auxDelMod( oView:GetModel('ZZADETAILT') )
	auxDelMod( oView:GetModel('ZZBDETAILD') )
	auxDelMod( oView:GetModel('ZZCDETAILD') )

	oViewLocal:Refresh( 'VIEW_ZZ8Q' )
	oViewLocal:Refresh( 'VIEW_ZZ9M' )
	oViewLocal:Refresh( 'VIEW_ZZAT' )
	oViewLocal:Refresh( 'VIEW_ZZBD' )
	oViewLocal:Refresh( 'VIEW_ZZCD' )
	

Return

/*/{Protheus.doc} auxDelMod
Funcao auxiliar para deletar os registros da grid do modelo passado como parametro
@author Triyo (SDA)
@since 06/07/2017
@version undefined
@param olModel, object, description
@type function
/*/
Static Function auxDelMod( olModel )

	local nlx
	
	for nlx := 1 to olModel:length()
		olModel:goLine( nlx )
		olModel:deleteLine()
	next nlx
	
	olModel:goLine( 1 )

Return


/*/{Protheus.doc} PRR13Cert(clCodNorma )
		
	     Fun��o para gera��o de itens do certificado.
		 Carrega para as tabelas ZZ8, ZZ9 e ZZA os dados de um certificado do mesmo 
		 Tipo de a�o.
		 
@author  Leandro Heilig
@since   03/10/2016
@version 1.0
@return  oModel
/*/
User Function PRR13Cert(clCodNorma,cTpBsc)

	Local lRet      := .T.
	Local cCodAco   := clCodNorma
	Local cCodCert  := ""
	Local clMsgData	:= ""
	Local clStatus	:= ""
	Local aArea     := getArea()
	Local oModel    := FwModelActive()
	Local nOperation:= oModel:GetOperation()
	Local llContinua:= .T.
	Local oMdlZZ7	:= oModel:GetModel('ZZ7MASTER' )
	Local oMdlZZ8   := oModel:GetModel('ZZ8DETAILQ')
	Local oMdlZZ9   := oModel:GetModel('ZZ9DETAILM')
	Local oMdlZZA   := oModel:GetModel('ZZADETAILT')
	local oMdlZZB   := oModel:GetModel('ZZBDETAILD')
	Local oMdlZZC   := oModel:GetModel('ZZCDETAILD')
	Local clProduto	:= FwFldGet('ZZ7_PRODUT')
	Local clCodCerAt:= FwFldGet('ZZ7_CODCER')
	Local cAliasZZ7 := getNextAlias()
	Local cQuery    := ""	
	Local nlOpcSel	:= 0
	
	Default cTpBsc := "P"

	//--------------------------------------------------
	// somente ser� carregado se for uma opera��o de inclus�o ou alteracao
	//--------------------------------------------------
	If !Empty(cCodAco)	
		
		If ( nOperation == 3 .or. nOperation == 4 )
	
			If cTpBsc $ "P/N" 
			
				cQuery := " SELECT MAX(ZZ7.ZZ7_CODCER) AS CERT "
				cQuery += " FROM "+RetSQLName("ZZ7")+" ZZ7 "
				cQuery += " WHERE ZZ7.ZZ7_FILIAL ='"+xFilial("ZZ7")+"' " 
				cQuery += " AND ZZ7.ZZ7_NORMA = '" + cCodAco + "' "
				cQuery += " AND ZZ7.ZZ7_STATUS = '01'"
				cQuery += " AND ZZ7.ZZ7_PRODUT = '" + clProduto + "' "
				if ( .not. empty( clCodCerAt ) )
					cQuery += " AND ZZ7.ZZ7_CODCER <> '" + clCodCerAt + "' "
				endif
				cQuery += " AND ZZ7.D_E_L_E_T_ = ' ' "
				
				cQuery := ChangeQuery(cQuery)
				DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasZZ7)
			
				(cAliasZZ7)->(dbGoTop())  
				
				cCodCert:= (cAliasZZ7)->CERT

				If (cAliasZZ7)->(EOF()) .Or. Empty(cCodCert)
				
					lRet:= .T. 
					lRet := psqByPrd( clCodNorma )
					
				Else
				
//					If !(MsgYesNo("Foi encontrado um certificado("+AllTrim(cCodCert)+") para esta norma '"+AllTrim(cCodAco)+"' . Deseja carregar os dados?"))
//						lRet := .T. 
//						Return lRet
//					EndIf

					nlOpcSel := aviso(	"Carregar Dados ?",;
										"Foi encontrado um certificado("+AllTrim(cCodCert)+") para esta norma '"+AllTrim(cCodAco)+"' . Deseja carregar os dados de onde?",;
										{"NORMA","CERTIFICADO"}, 3)
				
					if ( nlOpcSel == 1 )
					
						lRet := psqByPrd( clCodNorma )
					
					elseif ( nlOpcSel == 2 )
				
						/**
						  * Se for alteracao, para recarregar os dados, nao pode haver itens preenchidos nas abas
						  */
						if ( nOperation == 4 )
						
							if ( .not. oMdlZZ8:canClearData() )
								clMsgData += "Exclua os itens da primeira aba!" + CRLF
								llContinua := .F.
							endif
							
							if ( .not. oMdlZZ9:canClearData() )
								clMsgData += "Exclua os itens da segunda aba!" + CRLF
								llContinua := .F.
							endif
							
							if ( .not. oMdlZZA:canClearData() )
								clMsgData += "Exclua os itens da terceira aba!" + CRLF
								llContinua := .F.
							endif
							
							if ( .not. oMdlZZB:canClearData() )
								clMsgData += "Exclua os itens da quarta aba!" + CRLF
								llContinua := .F.
							endif
							
							if ( .not. oMdlZZC:canClearData() )
								clMsgData += "Exclua os itens da quinta aba!" + CRLF
								llContinua := .F.
							endif
							
							if ( .not. llContinua )
								/**
								  * Mensagem para lembrar o usuario de deletar todos os registros das abas para poder usar a funcionalidade de carregar dados ap�s altera��o de produto/norma.
								  * Esta regra � necessaria pois no modelo MVC, em modo de altera��o, n�o � poss�vel alterar os registros de grid pois estao vinculados ao banco de dados, 
								  * portanto o metodo clearData nao funciona. Isto garante a integridade dos dados e por isso � necessario antes excluir os registros para carregar novos.
								  */
								aviso("ATEN��O!", "Para carregar os dados automaticamente, siga os seguintes passos: " + CRLF + clMsgData, {"Fechar"}, 3)
							endif
						
						endif
						
						if ( llContinua )
						
							oMdlZZ8:clearData()
							oMdlZZ9:clearData()
							oMdlZZA:clearData()
							oMdlZZB:clearData()
							oMdlZZC:clearData()
						
							clStatus := oMdlZZ7:GetValue( 'ZZ7_STATUS' )
						
							PRR13Load(cCodCert,oMdlZZ8, clStatus == "99", {'ZZ8_ENSAIO','ZZ8_DESCRI','ZZ8_UM'    ,'ZZ8_MINMAX','ZZ8_SEQLAB','ZZ8_LIE','ZZ8_LSE', 'ZZ8_ENCONT','ZZ8_OBS'}, "ZZ8")
							PRR13Load(cCodCert,oMdlZZ9, clStatus == "99", {'ZZ9_ENSAIO','ZZ9_DESCRI','ZZ9_UM'    ,'ZZ9_MINMAX','ZZ9_SEQLAB','ZZ9_LIE','ZZ9_LSE', 'ZZ9_RESULT','ZZ9_OBS'}, "ZZ9")
							PRR13Load(cCodCert,oMdlZZA, clStatus == "99", {'ZZA_ENSAIO','ZZA_DESC'  ,'ZZA_ESPECI','ZZA_RESULT' }, "ZZA")
							PRR13Load(cCodCert,oMdlZZB, clStatus == "99", {'ZZB_ENSAIO','ZZB_DESCRI','ZZB_UM'    ,'ZZB_MINMAX','ZZB_LIE','ZZB_LSE', 'ZZB_RESULT','ZZB_OBS'}, "ZZB")
							PRR13Load(cCodCert,oMdlZZC, clStatus == "99", {'ZZC_ENSAIO','ZZC_DESCRI','ZZC_UM'    ,'ZZC_MINMAX','ZZC_LIE','ZZC_LSE', 'ZZC_RESULT','ZZC_OBS'}, "ZZC")
							
						endif
						
					endif			
				
				   (cAliasZZ7)->(dbCloseArea())
				Endif
					
				/*
				If ( "ZZ7_NORMA" $ FwFldGet('ZZ7_NORMA') ) .and. cTpBsc == "N" 
	
					// Pesquisa por norma				
					SB1->(dbSetOrder(1))
					If SB1->(dbSeek(xFilial("SB1")+AllTrim(cCodAco)))
						If SB1->B1_TIPO == "SL" // Tipo normal
							IF PRR13EspLoad(SB1->B1_COD,oMdlZZ8,oMdlZZ9,oMdlZZA,oMdlZZB,oMdlZZC,cCodAco,nOperation)
								lRet := .T.																							
							Else
								Return .F.
							Endif						
						Else
							alert("N�o foi encontrada a norma. Cadastre e depois retorne para a continua��o do certificado.")
						Endif
					
					Else
						alert("N�o foi encontrada a norma. Cadastre e depois retorne para a continua��o do certificado.")
					Endif
					
				endif
				*/
			// Pesquisa por Corrida
			ElseIf cTpBsc == "C" 
			
				If Empty(FwFldGet('ZZ7_NORMA'))
					lRet:= .T. 
					alert("Codigo da norma em branco. Preencha as caracter�sticas manualmente.")
				Endif
					
				cQuery := " SELECT MAX(ZZ7.ZZ7_CODCER) AS CERT "
				cQuery += " FROM "+RetSQLName("ZZ7")+" ZZ7 "
				cQuery += " WHERE ZZ7.ZZ7_FILIAL ='"+xFilial("ZZ7")+"' " 
				cQuery += " AND ZZ7.ZZ7_CORRID='"+cCodAco+"' "
				cQuery += " AND ZZ7.D_E_L_E_T_='' "
				cQuery += " AND ZZ7.ZZ7_STATUS='01'"
				
				cQuery := ChangeQuery(cQuery)
				DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasZZ7)
			
				(cAliasZZ7)->(dbGoTop())  
								
				cCodCert:= (cAliasZZ7)->CERT
				
				If (cAliasZZ7)->(EOF()) .Or. Empty(cCodCert)
				
					lRet:= .T. 
					alert("Falha na sele��o de certificado anterior. N�o foi encontrato certificado cadastrado com este tipo de corrida. Preencha as caracter�sticas manualmente.")
						
				Else
																			
					PRR13Load(cCodCert,oMdlZZ8, .T.,{'ZZ8_ENCONT'} , "ZZ8",,"LABQUI",{'ZZ8_ENSAIO'})

				   (cAliasZZ7)->(dbCloseArea())
				Endif
										
			Endif	
		Endif		

	Else
	
		alert("O campo com a identifica��o da Norma n�o foi informado e n�o existe um associado ao Grupo de produto. N�o ser� poss�vel carregar estrutura de um certificado anterior")

	Endif
	RestArea(aArea)

Return lRet

Static Function psqByPrd( clCodNorma )
					
	Local cCodAco   := clCodNorma
	Local oModel    := FwModelActive()
	Local nOperation:= oModel:GetOperation()
	Local oMdlZZ7	:= oModel:GetModel('ZZ7MASTER' )
	Local oMdlZZ8   := oModel:GetModel('ZZ8DETAILQ')
	Local oMdlZZ9   := oModel:GetModel('ZZ9DETAILM')
	Local oMdlZZA   := oModel:GetModel('ZZADETAILT')
	local oMdlZZB   := oModel:GetModel('ZZBDETAILD')
	Local oMdlZZC   := oModel:GetModel('ZZCDETAILD')
	Local lPesqSBM	:= .F.
	Local lRet		:= .F.

	// Pesquisa os certificados por Grupo caso nao encontre por produto
	dbSelectArea("SB1")
	SB1->(dbSetOrder(1))
	If SB1->(dbSeek(xFilial("SB1")+AllTrim(FwFldGet("ZZ7_PRODUT"))))
		If !Empty(SB1->B1_XTPACO)
			If SB1->(dbSeek(xFilial("SB1")+AllTrim(SB1->B1_XTPACO))) .And. SB1->B1_TIPO == "SL"
				IF PRR13EspLoad(SB1->B1_COD,oMdlZZ8,oMdlZZ9,oMdlZZA,oMdlZZB,oMdlZZC,cCodAco,nOperation)
					lRet := .T.																									
				Else
					Return .F.
				Endif						
			Else
				lPesqSBM := .T.
			Endif
		Else
			lPesqSBM := .T.
		Endif
		
	Else
		alert("Produto n�o encontrado no cadastro. Preencha as caracter�sticas manualmente.")
	Endif

	If lPesqSBM
		// Caso o tipo do aco esteja em branco pesquisa a especificacao pelo grupo 
		SBM->(dbSetOrder(1))
		If SBM->(dbSeek(xFilial("SBM")+SB1->B1_GRUPO))
			If !Empty(SBM->BM_XNORMA)
				If SB1->(dbSeek(xFilial("SB1")+AllTrim(SBM->BM_XNORMA))) .And. SB1->B1_TIPO == "SL"
					IF PRR13EspLoad(SB1->B1_COD,oMdlZZ8,oMdlZZ9,oMdlZZA,oMdlZZB,oMdlZZC,cCodAco,nOperation)
						lRet := .T.													
					Else
						Return .F.
					Endif						
				Else
					alert("N�o foi encontrada a norma: "+AllTrim(SBM->BM_XNORMA) + " do grupo de  produto: " +AllTrim(SB1->B1_GRUPO) + ". Preencha as caracter�sticas manualmente.")
				Endif
			
			Else
				alert("Norma do grupo de produtos esta em branco. Verifique o cadastro.")
			Endif
		Else
			alert("N�o foi encontrado o grupo deste Produto. Verifique o cadastro.")
		Endif
	Endif

Return lRet

/*/{Protheus.doc} PRR13Load(cCodCert,oModelIt,aField,cOrigem)

		 Carrega Modelo de dados de um Certificado cadastrado anteriormente
		 baseado no c�digo do tipo de a�o associado
		 
@author  Leandro Heilig (Triyo)
@since   07/10/2016
@version 1.0
@return  oModel
/*/
Static Function PRR13Load(cCodCert,oModelIt, llDelLine, aField,cOrigem,aFieldCtd,cLabor,aFieldAtu,nOperation)

	Local cAliasCert := GetNextAlias()
	Local cQuery     := ""
	Local nX         := 1
	Local nI         := 1
	Local nY         := 1
	Local lFieldAtu  := .T.	
	Local aArea      := getArea()
	Local cAliasOri  := cOrigem 
	Local oView 	 := 	FWViewActive()	//Recuperando a view ativa da interface
	Local nLinGrid   := 0
	Local lClean	 := .F.
	Local nlRegTot	:= 0
		
	default llDelLine := .T.
	default cLabor := ""
	default aFieldAtu := {}
	default nOperation := 3
		
	If cAliasOri =="ZZ8"
	
		cQuery:="SELECT ZZ8_FILIAL, ZZ8_CODCER, ZZ8_PRODUT, ZZ8_REVISA, ZZ8_LABOR, ZZ8_SEQLAB, " 
		cQuery+="ZZ8.ZZ8_ENSAIO,ZZ8.ZZ8_DESCRI,ZZ8.ZZ8_UM,ZZ8.ZZ8_MINMAX, "
		cQuery+="ZZ8.ZZ8_LIE,ZZ8.ZZ8_LSE, ZZ8.ZZ8_ENCONT,ZZ8.ZZ8_OBS "
		cQuery+="FROM "+RetSQLName("ZZ8")+" ZZ8 "
		cQuery+="WHERE ZZ8.D_E_L_E_T_='' " 
		cQuery+="AND ZZ8.ZZ8_FILIAL='"+xFilial("ZZ8")+"' "
		cQuery+="AND ZZ8.ZZ8_CODCER='"+cCodCert+"' "
		cQuery+=" ORDER BY ZZ8_FILIAL, ZZ8_CODCER, ZZ8_PRODUT, ZZ8_REVISA, ZZ8_LABOR, ZZ8_SEQLAB "
	
	ElseIf cAliasOri =="ZZ9"
	
		cQuery:="SELECT ZZ9_FILIAL, ZZ9_CODCER, ZZ9_PRODUT, ZZ9_REVISA, ZZ9_LABOR, ZZ9_SEQLAB, "
		cQuery+="ZZ9.ZZ9_ENSAIO,ZZ9.ZZ9_DESCRI,ZZ9.ZZ9_UM,ZZ9.ZZ9_MINMAX, "
		cQuery+="ZZ9.ZZ9_LIE,ZZ9.ZZ9_LSE, ZZ9.ZZ9_RESULT,ZZ9.ZZ9_OBS "			
		cQuery+="FROM "+RetSQLName("ZZ9")+" ZZ9 "
		cQuery+="WHERE ZZ9.D_E_L_E_T_='' " 
		cQuery+="AND ZZ9.ZZ9_FILIAL='"+xFilial("ZZ9")+"' "
		cQuery+="AND ZZ9.ZZ9_CODCER='"+cCodCert+"' "
		cQuery+=" ORDER BY ZZ9_FILIAL, ZZ9_CODCER, ZZ9_PRODUT, ZZ9_REVISA, ZZ9_LABOR, ZZ9_SEQLAB "
	
	ElseIf cAliasOri =="ZZA"	
	
		cQuery:="SELECT ZZA.ZZA_ENSAIO,ZZA.ZZA_DESC,ZZA.ZZA_ESPECI,ZZA.ZZA_RESULT "
		cQuery+="FROM "+RetSQLName("ZZA")+" ZZA "
		cQuery+="WHERE ZZA.D_E_L_E_T_='' " 
		cQuery+="AND ZZA.ZZA_FILIAL='"+xFilial("ZZA")+"' "
		cQuery+="AND ZZA.ZZA_CODCER='"+cCodCert+"' "
		cQuery+=" ORDER BY R_E_C_N_O_ "	
		
	ElseIf cAliasOri =="ZZB"	
	
		cQuery:="SELECT ZZB.ZZB_ENSAIO,ZZB.ZZB_DESCRI,ZZB.ZZB_UM,ZZB.ZZB_MINMAX, "
		cQuery+="ZZB.ZZB_LIE,ZZB.ZZB_LSE, ZZB.ZZB_RESULT,ZZB.ZZB_OBS "
		cQuery+="FROM "+RetSQLName("ZZB")+" ZZB "
		cQuery+="WHERE ZZB.D_E_L_E_T_='' " 
		cQuery+="AND ZZB.ZZB_FILIAL='"+xFilial("ZZB")+"' "
		cQuery+="AND ZZB.ZZB_CODCER='"+cCodCert+"' "
		cQuery+=" ORDER BY R_E_C_N_O_ "	
		
	ElseIf cAliasOri =="ZZC"

		cQuery:="SELECT ZZC.ZZC_ENSAIO,ZZC.ZZC_DESCRI,ZZC.ZZC_UM,ZZC.ZZC_MINMAX, "
		cQuery+="ZZC.ZZC_LIE,ZZC.ZZC_LSE, ZZC.ZZC_RESULT,ZZC.ZZC_OBS "
		cQuery+="FROM "+RetSQLName("ZZC")+" ZZC "
		cQuery+="WHERE ZZC.D_E_L_E_T_='' " 
		cQuery+="AND ZZC.ZZC_FILIAL='"+xFilial("ZZC")+"' "
		cQuery+="AND ZZC.ZZC_CODCER='"+cCodCert+"' "
		cQuery+=" ORDER BY R_E_C_N_O_ "	
	
	ElseIf cAliasOri == "QE7" .And. !Empty(cLabor) 
	
		cQuery:="SELECT MAX(QE7.QE7_REVI)AS MAXREV,QE7.QE7_ENSAIO,QE7.QE7_UNIMED,QE7.QE7_MINMAX, QE7_SEQLAB, "
		cQuery+="QE7.QE7_LIE,QE7.QE7_LSE,QE1.QE1_DESCPO AS QE7_DESCPO "
		cQuery+="FROM "+RetSQLName("QE7")+" QE7 "
		cQuery+="INNER JOIN "+RetSQLName("QE1")+" QE1 ON QE1.QE1_FILIAL = '"+xFilial("QE1")+ "' AND QE1.QE1_ENSAIO = QE7.QE7_ENSAIO AND QE1.D_E_L_E_T_='' "		
		cQuery+="WHERE QE7.D_E_L_E_T_='' " 
		cQuery+="AND QE7.QE7_FILIAL='"+xFilial("QE7")+"' "
		cQuery+="AND QE7.QE7_LABOR='"+cLabor+"' "	
		cQuery+="AND QE7.QE7_PRODUT='"+AllTrim(cCodCert)+"' "	
		cQuery+="GROUP BY QE7_REVI,QE7_ENSAIO,QE7_UNIMED,QE7_MINMAX,QE7_LIE,QE7_LSE,QE1_DESCPO,QE7_SEQLAB "
		cQuery+="ORDER BY QE7_SEQLAB "
		
	ElseIf cAliasOri == "QE8" .And. !Empty(cLabor) 
	
		cQuery:="SELECT MAX(QE8.QE8_REVI)AS MAXREV,QE8.QE8_ENSAIO, " 
		cQuery+="QE1.QE1_DESCPO AS QE8_DESCPO, QE8_TEXTO " 
		cQuery+="FROM "+RetSQLName("QE8")+" QE8 "
		cQuery+="INNER JOIN "+RetSQLName("QE1")+" QE1 ON QE1.QE1_FILIAL = '"+xFilial("QE1")+ "' AND QE1.QE1_ENSAIO = QE8.QE8_ENSAIO AND QE1.D_E_L_E_T_='' " 
		cQuery+="WHERE QE8.D_E_L_E_T_=' ' "
		cQuery+="AND QE8.QE8_FILIAL='"+xFilial("QE8")+"' "
		cQuery+="AND QE8.QE8_LABOR='"+cLabor+"' "
		cQuery+="AND QE8.QE8_PRODUT='"+AllTrim(cCodCert)+"' "
		cQuery+="GROUP BY QE8_REVI,QE8_ENSAIO,QE1_DESCPO,QE8_TEXTO " 
		
	Endif
	
	cQuery := ChangeQuery(cQuery)
	DbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasCert)
	
	(cAliasCert)->(dbGoTop())
	(cAliasCert)->(dbEval( {|| nlRegTot++  } ))
	(cAliasCert)->(dbGotop())
	
	While !(cAliasCert)->(EOF())

		nLinGrid++
		// Atualizar todos os campos e tabelas
		If Len(aFieldAtu) == 0

			If nOperation == 3

				If oModelIt:GetLine() == 1 .And. oModelIt:IsUpdated()
					oModelIt:AddLine()
				Endif

			Else
				If nLinGrid > oModelIt:length()
					If oModelIt:IsUpdated()
						oModelIt:AddLine()
					Endif
				Else
					oModelIt:goLine( nLinGrid )
				Endif
				
			Endif
			
			For nX:=1 to  Len(aField)
			    
				If Empty(cLabor)
					cField := cOrigem + Substr(aField[nX],At('_',aField[nX]),Len(aField[nX]))			
				Else
					cField := cOrigem + Substr(aFieldCtd[nX],At('_',aFieldCtd[nX]),Len(aFieldCtd[nX]))
				Endif

				oModelIt:LoadValue( aField[nX],(cAliasCert)->&(cField) )		

			Next nX			

			if ( llDelLine )
				oModelIt:DeleteLine()
			endif

			If nOperation == 3 .and. ( ( nLinGrid + 1 ) < nlRegTot )
				oModelIt:AddLine()
			Endif

		Else
			// Atualizar apenas os campos especificos dos itens criados pela Norma
			If cOrigem == "ZZ8"

				For nI := 1 To oModelIt:Length()
					oModelIt:GoLine( nI )

					lFieldAtu := .T.
					For nX:=1 to  Len(aFieldAtu)
					    
						cField := cOrigem + Substr(aFieldAtu[nX],At('_',aFieldAtu[nX]),Len(aFieldAtu[nX]))		
						If !(oModelIt:GetValue(aFieldAtu[nX]) == (cAliasCert)->&(cField))
							lFieldAtu := .F.						
						Endif
						
					Next nX

					If lFieldAtu
						For nY:=1 to  Len(aField)
							cField := cOrigem + Substr(aField[nY],At('_',aField[nY]),Len(aField[nY]))		
							oModelIt:LoadValue( aField[nY],(cAliasCert)->&(cField) )		
						Next nY
					Endif
	
				Next nI

			Endif
		Endif
			
		(cAliasCert)->(dbSkip())
	End	
	
	(cAliasCert)->(dbCloseArea())

	If nOperation == 4

		If nLinGrid < oModelIt:length()
	
			For nY := (nLinGrid+1) to oModelIt:length()  
				oModelIt:GoLine( nY )
				For nX:=1 to  Len(aField)
					oModelIt:LoadValue( aField[nX], CriaVar(aField[nX]))		
				Next nX		
				If SubStr(aField[1],1,3) == "ZZ8"
					oModelIt:LoadValue( 'ZZ8_ENCONT',CriaVar('ZZ8_ENCONT') )
				Endif
				oModelIt:DeleteLine()
			Next
		
		Endif
	
	Endif
	
	oModelIt:GoLine(1)	
	oView:Refresh( 'VIEW_ZZ8Q' )
	oView:Refresh( 'VIEW_ZZ9M' )
	oView:Refresh( 'VIEW_ZZAT' )
	oView:Refresh( 'VIEW_ZZBD' )
	oView:Refresh( 'VIEW_ZZCD' )

Return			

/*/{Protheus.doc} PRR13LPre( oModel,nLine,cAcao,cCampo )

		 Pr�-valida��o do modelo de dados 
		 
@author  Leandro Heilig (Triyo)
@since   12/10/2016
@version 1.0
@return  oModel
/*/

Static Function PRR13LPre( oModel,nLine,cAction,cField )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	
	Local oMdlZZ7    := oModel:GetModel('ZZ7MASTER' )
	Local oMdlZZ8    := oModel:GetModel('ZZ8DETAILQ')
	Local oMdlZZ9    := oModel:GetModel('ZZ9DETAILM')
	Local oMdlZZA    := oModel:GetModel('ZZADETAILT')
	Local oMdlZZB    := oModel:GetModel('ZZBDETAILD')
	Local oMdlZZC    := oModel:GetModel('ZZCDETAILD')
	
	Local cEnsaio    := ""
	Local cClassQE1  := ""
	Local cClassQP1  := ""
	Local cTpProd    := Posicione( "SB1", 1, xFilial("SB1") + FwFldGet("ZZ7_PRODUT"), "B1_TIPO" )  //POSICIONE("SB1", 1, xFilial("SB1") + M->ZZ7_PRODUT, "B1_TIPO")
		
	Local nLinha     := nLine
	Local cAcao      := cAction 
	Local cCampo     := cField
	Local cCmpEns    := ""
	Local cCmpMinMax := ""
	Local cCrtMinMax := ""
	Local cCmpMin    := ""
	Local cCmpMax    := ""
	Local cCmpNomi   := ""
	Local cCmpResult := ""
	Local cMin       := ""
	Local cMax       := ""
	Local cNominal   := ""
	Local cResult    := ""
	

	If cAcao=="CANSETVALUE"   	
			
		// Verifica o modelo em uso para buscar o ensaio correto
		
		If	cIdModel=="ZZ8DETAILQ"
			
			cEnsaio    := oModel:GetValue('ZZ8_ENSAIO')
			cCmpEns    := 'ZZ8_DESCRI' 
			cClassQE1 := POSICIONE("QE1", 1, xFilial("QE1") + cEnsaio, "QE1_XCLASS")
				
		ElseIF cIdModel=="ZZ9DETAILM"
		
			cEnsaio   := oModel:GetValue('ZZ9_ENSAIO')
			cCmpEns   := 'ZZ9_DESCRI'
			cClassQE1 := POSICIONE("QE1", 1, xFilial("QE1") + cEnsaio, "QE1_XCLASS")	
			
		ElseIF cIdModel=="ZZADETAILT"
				
			cEnsaio   := oModel:GetValue('ZZA_ENSAIO')
			cCmpEns   := 'ZZA_DESC'
			cClassQE1 := POSICIONE("QE1", 1, xFilial("QE1") + cEnsaio, "QE1_XCLASS")
			
		ElseIF cIdModel=="ZZBDETAILD"
				
			cEnsaio   := oModel:GetValue('ZZB_ENSAIO')
			cCmpEns   := 'ZZB_DESCRI'
			cClassQE1 := POSICIONE("QE1", 1, xFilial("QE1") + cEnsaio, "QE1_XCLASS")
				
		ElseIF cIdModel=="ZZCDETAILD"
			
			cEnsaio   := oModel:GetValue('ZZC_ENSAIO')
			cCmpEns   := 'ZZC_DESCRI'					
			cClassQP1 := POSICIONE("QP1", 1, xFilial("QP1") + cEnsaio, "QP1_TPCART")		
				
		Endif	

		//--------------------------------------------------
		// Se a Vari�vel foi preenchida, os dados de ensaio
		// dever�o considerar a tabela QP1	
		//--------------------------------------------------
		If !Empty(cClassQP1)
			
			cClassQE1 := cClassQP1
			
			If cClassQE1 <> "D" .And. cCampo == cCmpEns
				
				lRet:= .F.
				alert("Linha "+str(nLinha)+"S� � permitido a inclus�o de ensaios do tipo (D) - Dimensional. Verifique o Cadasro de Ensaios no M�dulo de Inspe��o de Processos. Campo "+cCampo)
					
			Endif
		
		Endif		
		
		//--------------------------------------------------
		// Faz a verifica��o do ensaio na tabela QE1 do
		// m�dulo de inspe��o de entrada
		//--------------------------------------------------
			
		If cClassQE1 <> "5" .And. cCampo == cCmpEns				
					
			lRet:= .F.
			alert('Linha '+str(nLinha)+'S� � permitido alterar a descri��o de Ensaios do tipo Gen�rico. Campo '+cCampo)
					
		Endif	
		
		If cTpProd =="PA" .And. cIdModel=="ZZBDETAILD"
					
			lRet:= .F.
			alert('Linha '+str(nLinha)+"A pasta 'Propriedades Dimensionais do Item de Venda' S� est� dispon�vel para preenchimento de produtos do tipo 'ME' . Campo "+cCampo)
							
		Endif	
		
		If cTpProd =="ME" .And. cIdModel=="ZZCDETAILD"
					
			lRet:= .F.
			alert('Linha '+str(nLinha)+"A pasta 'Propriedades Dimensionais do Item produzido' S� est� dispon�vel para preenchimento de produtos do tipo 'PA' . Campo "+cCampo)
							
		Endif	
		
	Endif

	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet

/*/{Protheus.doc} ZZ8VldLPos( oModel )

		 P�s-valida��o do modelo de dados ZZ8DETAILQ (Composi��o Qu�mica)
		 
@author  Leandro Heilig (Triyo)
@since   30/10/2016
@version 1.0
@return  oModel
/*/

Static Function ZZ8VldLPos( oModel )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	Local oMdlZZ8    := oModel:GetModel('ZZ8DETAILQ')
	Local cStatus    := M->ZZ7_STATUS
		
	Local cCmpEns    := ""
	Local cCmpMinMax := ""
	Local cCrtMinMax := ""
	Local cCmpMin    := ""
	Local cCmpMax    := ""
	Local cCmpNomi   := ""
	Local cCmpResult := ""
	Local cMin       := ""
	Local nVldMin    := ""
	Local cMax       := ""
	Local cNominal   := ""
	Local cResult    := ""
	Local nI         := 0
	
	If cStatus=="99
	
		Return lRet
	
	Endif
	
	
	If nOperation == 3 .OR. nOperation == 4	
	
		For nI:=1 to oModel:Length()
		
			oModel:GoLine( nI )
		
			If !oModel:IsDeleted()
			
				cCrtMinMax:= FwFldGet( 'ZZ8_MINMAX' )
				
				cMin      := AllTrim(FwFldGet( 'ZZ8_LIE'    ))
				cMax      := AllTrim(FwFldGet( 'ZZ8_LSE'    ))
			//	cNominal  := AllTrim(FwFldGet( 'ZZ8_NOMINA' ))
				cResult   := AllTrim(FwFldGet( 'ZZ8_ENCONT' ))
				
				nVldMin:= At("-",cMin)
						
				If nVldMin > 0
						
					cMin    := SuperVal(SubStr(cMin ,At("-",cMin )+1))
						
				Else
						
					cMin    := SuperVal(cMin)
				
				Endif
						
				//cNominal:= SuperVal(cNominal)							
				cMax    := superVal(cMax)	
				cResult := superVal(cResult)	
				
				If cCrtMinMax =='1' // Controla Min e max
			
					//--------------------------------
					// Valida resultado informado		
					//--------------------------------			
					
					If Empty(cMin) .Or. Empty(cMax) 
					
						lRet:= .F.
						alert( 'Composi��o Qu�mica - Linha '+str(nI)+': Controle M�nimo de m�nimo do ensaio habilitado. Por favor preenche os respectivos campos, informando o limite m�nimo para a medi��o')
					
					Else						
			
						// Valida resultado informado	 
						
						If cResult < cMin //cResult < ( cNominal - cMin  ) - 13/02: Alterado conforme reuni�o com o Ederson
						
							lRet:= .F.
							alert( 'Composi��o Qu�mica - Linha '+str(nI)+': O resultado '+STR(cResult)+' informado fora dos limites do m�nimo ('+Str( cMin )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif						
						
						If cResult > cMax //cResult > ( cNominal + cMax  ) - 13/02: Alterado conforme reuni�o com o Ederson
						
							lRet:= .F.
							alert( 'Composi��o Qu�mica - Linha '+str(nI)+': O resultado '+STR(cResult)+' informado est� acima da varia��o m�xima  estabelecida ('+Str( cMax )+'). Por favor, verifque o resultado ou a varia��o m�xima e o Resultado informado')		
						
						Endif							
					
					Endif
					
				 //--------------------------------------------------
				 // Controle do m�nimo
				 //--------------------------------------------------	
				
				ElseIf cCrtMinMax =='2'   //controla min						  
				  
					// Valida resultado informado					
					
					If Empty(cMin) 
					
						lRet:= .F.
						alert( 'Composi��o Qu�mica - Linha '+str(nI)+': Controle de valor M�nimo do ensaio habilitado. Por favor, informe uma varia��o m�nima para a medi��o')		
					
					Else														
						
						// Valida resultado informado	 
						
						If cResult < cMin//cResult < ( cNominal - cMin  ) - 13/02: Alterado conforme reuni�o com o Ederson
						
							lRet:= .F.
							alert( 'Composi��o Qu�mica - Linha '+str(nI)+': O resultado informado'+STR(cResult)+' fora dos limites do m�nimo estabelecidos ('+Str( cMin  )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
					Endif			  
				  
				
				ElseIf cCrtMinMax =='3'  // Controla max
				  
					  If Empty(cMax) 
						
							lRet:= .F.
							alert( 'Composi��o Qu�mica - Linha '+str(nI)+': Controle de valor M�ximo do ensaio habilitado. Por favor, informe uma varia��o m�xima para a medi��o')		
						
						Else					
						
							// Valida resultado informado					
							
							If cResult > cMax //cResult > ( cNominal + cMax)  - 13/02: Alterado conforme reuni�o com o Ederson 
							
								lRet:= .F.
								alert( 'Composi��o Qu�mica - Linha '+str(nI)+': Resultado informado ('+Str(cResult)+') est� fora do limite de m�nimo especificado('+Str(cMax) +')')		
							
							Endif
							
						Endif					
					
				Endif	
						
			Endif		
		
		Next	
	
	Endif
		
	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet


/*/{Protheus.doc} ZZ9VldLPos( oModel )

		 P�s-valida��o do modelo de dados ZZ9DETAILQ (Propriedades mec�nicas)
		 
@author  Leandro Heilig (Triyo)
@since   30/10/2016
@version 1.0
@return  oModel
/*/

Static Function ZZ9VldLPos( oModel )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	Local oMdlZZ8    := oModel:GetModel('ZZ9DETAILM')
		
	Local cCmpEns    := ""
	Local cCmpMinMax := ""
	Local cCrtMinMax := ""
	Local cCmpMin    := ""
	Local cCmpMax    := ""
	Local cCmpNomi   := ""
	Local cCmpResult := ""
	Local cMin       := ""
	Local cMax       := ""
	Local cNominal   := ""
	Local cResult    := ""
	Local nI         := 0
	
	If nOperation == 3 .OR. nOperation == 4	
	
		For nI:=1 to oModel:Length()
		
			oModel:GoLine( nI )
		
			If !oModel:IsDeleted()
			
				cCrtMinMax:= AllTrim(FwFldGet( 'ZZ9_MINMAX' ))
				cMin      := AllTrim(FwFldGet( 'ZZ9_LIE'    ))
				cMax      := AllTrim(FwFldGet( 'ZZ9_LSE'    ))
				cNominal  := AllTrim(FwFldGet( 'ZZ9_NOMINA' ))
				cResult   := AllTrim(FwFldGet( 'ZZ9_RESULT' ))
							
				nVldMin:= At("-",cMin)
						
				If nVldMin > 0
						
					cMin    := SuperVal(SubStr(cMin ,At("-",cMin )+1))
						
				Else
						
					cMin    := SuperVal(cMin)
				
				Endif
						
				cNominal:= SuperVal(cNominal)							
				cMax    := superVal(cMax)	
				cResult := superVal(cResult)	
				
				If cCrtMinMax =='1' // Controla Min e max
			
					//--------------------------------
					// Valida resultado informado		
					//--------------------------------			
					
					If Empty(cMin) .Or. Empty(cMax) 
					
						lRet:= .F.
						alert( 'Propriedades Mec�nicas - Linha '+str(nI)+': Controle M�nimo de m�nimo do ensaio habilitado. Por favor preenche os respectivos campos, informando as varia��es poss�veis a partir dio valor nominal')
					
					Else						
			
						// Valida resultado informado	 
						
						If cResult < cMin //cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Propriedades mec�nicas - Linha '+str(nI)+': O resultado informado '+Str(cResult)+' fora dos limites do m�nimo  estabelecido ('+Str( cMin )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
						
						If cResult > cMax //cResult > ( cNominal + cMax  )
						
							lRet:= .F.
							alert( 'Propriedades mec�nicas - Linha '+str(nI)+': O resultado informado '+Str(cResult)+' est� acima do valor nominal estabelecido ('+Str( cMax )+'). Por favor, verifque o resultado ou a varia��o m�nima inferior informado')		
						
						Endif							
					
					Endif
					
				 //--------------------------------------------------
				 // Controle do m�nimo
				 //--------------------------------------------------	
				
				ElseIf cCrtMinMax =='2'   //controla min						  
				  
					// Valida resultado informado					
					
					If Empty(cMin) 
					
						lRet:= .F.
						alert( 'Propriedades mec�nicas - Linha '+str(nI)+': Controle de valor M�nimo do ensaio habilitado. Por favor, informe uma varia��o m�nima a partir da Nominal')		
					
					Else														
						
						// Valida resultado informado	 
						
						If cResult < cMin//cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Propriedades mec�nicas - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') fora dos limites do m�nimo('+Str( cMin  )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif						
						
					Endif			  
				  
				
				ElseIf cCrtMinMax =='3'  // Controla max
				  
					  If Empty(cMax) 
						
							lRet:= .F.
							alert( 'Propriedades mec�nicas - Linha '+str(nI)+': Controle de valor M�ximo do ensaio habilitado. Por favor, informe uma varia��o m�xima a partir da Nominal')		
						
						Else					
						
							// Valida resultado informado					
							
							If cResult > cMax //cResult > ( cNominal + cMax)  
							
								lRet:= .F.
								alert( 'Propriedades mec�nicas - Linha '+str(nI)+': Resultado informado ('+Str(cResult)+') fora dos limites de m�nimo especificado('+Str(cMax)+')')		
							
							Endif
							
						Endif					
					
				Endif	
						
			Endif		
		
		Next	
	
	Endif
		
	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet


/*/{Protheus.doc} ZZ9VldLPos( oModel )

		 P�s-valida��o do modelo de dados ZZADETAILT (Propriedades T�rmicas)
		 
@author  Leandro Heilig (Triyo)
@since   30/10/2016
@version 1.0
@return  oModel
/*/

Static Function ZZAVldLPos( oModel )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	Local oMdlZZA    := oModel:GetModel('ZZADETAILT')
		
	Local cCmpEns    := ""

	Local cCrtEsp    := ""
	Local cCrtRes    := ""
	
	Local nI         := 0
	
	If nOperation == 3 .OR. nOperation == 4	
	
		For nI:=1 to oModel:Length()
		
			oModel:GoLine( nI )
		
			If !oModel:IsDeleted()
			
				cCrtEsp := FwFldGet( 'ZZA_ESPECI' )
				cCrtRes := FwFldGet( 'ZZA_RESULT' )
				
				If Empty(cCrtEsp) 
				
					lRet:= .F.
					alert( 'Linha '+str(nI)+': Necess�rio informar o conte�do do campo Especifica��o do ensaio.')						  	
						
				Endif	
				
				If Empty(cCrtRes) 
				
					lRet:= .F.
					alert( 'Linha '+str(nI)+': Necess�rio informar o conte�do do campo Resultado do ensaio.')						  	
						
				Endif	
						
			Endif		
		
		Next	
	
	Endif
		
	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet

/*/{Protheus.doc} ZZBVldLPos( oModel )

		 P�s-valida��o do modelo de dados ZZBDETAILD (Ensaio Dimensional)
		 
@author  Leandro Heilig (Triyo)
@since   08/12/2016
@version 1.0
@return  oModel
/*/

Static Function ZZBVldLPos( oModel )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	Local oMdlZZ8    := oModel:GetModel('ZZBDETAILD')
		
	Local cCmpEns    := ""
	Local cCmpMinMax := ""
	Local cCrtMinMax := ""
	Local cCmpMin    := ""
	Local cCmpMax    := ""
	Local cCmpNomi   := ""
	Local cCmpResult := ""
	Local cMin       := ""
	Local cMax       := ""
	Local cNominal   := ""
	Local cResult    := ""
	Local nI         := 0
	
	If nOperation == 3 .OR. nOperation == 4	
	
		For nI:=1 to oModel:Length()
		
			oModel:GoLine( nI )
		
			If !oModel:IsDeleted()
			
				cCrtMinMax := FwFldGet( 'ZZB_MINMAX' )
				cMin       := AllTrim(FwFldGet( 'ZZB_LIE'    ))
				cMax       := AllTrim(FwFldGet( 'ZZB_LSE'    ))
				cNominal   := AllTrim(FwFldGet( 'ZZB_NOMINA' ))
				cResult    := AllTrim(FwFldGet( 'ZZB_RESULT' ))
											
				nVldMin:= At("-",cMin)
						
				If nVldMin > 0
						
					cMin    := SuperVal(SubStr(cMin ,At("-",cMin )+1))
						
				Else
						
					cMin    := SuperVal(cMin)
				
				Endif
						
				cNominal:= SuperVal(cNominal)							
				cMax    := superVal(cMax)	
				cResult := superVal(cResult)	
				
				If cCrtMinMax =='1' // Controla Min e max
			
					//--------------------------------
					// Valida resultado informado		
					//--------------------------------			
					
					If Empty(cMin) .Or. Empty(cMax) 
					
						lRet:= .F.
						alert( 'Dimensional (ME) - Linha '+str(nI)+': Controle M�nimo de m�nimo do ensaio habilitado. Por favor preenche os respectivos campos, informando as varia��es poss�veis a partir dio valor nominal')
					
					Else						
			
						// Valida resultado informado	 
						
						If cResult < cMin //cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') fora dos limites do m�nimo estabelecido ('+Str( cMin  )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
						
						If cResult > cMax //cResult > ( cNominal + cMax  )
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') est� acima do valor nominal estabelecido ('+Str( cMax )+'). Por favor, verifque o resultado ou a varia��o m�nima inferior informado')		
						
						Endif							
					
					Endif
					
				 //--------------------------------------------------
				 // Controle do m�nimo
				 //--------------------------------------------------	
				
				ElseIf cCrtMinMax =='2'   //controla min						  
				  
					// Valida resultado informado					
					
					If Empty(cMin) 
					
						lRet:= .F.
						alert( 'Dimensional (ME)  - Linha '+str(nI)+': Controle de valor M�nimo do ensaio habilitado. Por favor, informe uma varia��o m�nima a partir da Nominal')		
					
					Else														
						
						// Valida resultado informado	 
						
						If cResult < cMin //cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': O resultado informado'+Str(cResult)+' fora dos limites do m�nimo estebelecido('+Str( cMin  )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
						/*
						If cResult > cNominal
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') est� acima do valor nominal estabelecido ('+Str(cNominal)+'). Por favor, verifque o resultado ou a varia��o m�nima inferior informado')		
						
						Endif*/
						
					Endif			  
				  
				
				ElseIf cCrtMinMax =='3'  // Controla max
				  
					  If Empty(cMax) 
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': Controle de valor M�ximo do ensaio habilitado. Por favor, informe uma varia��o m�xima a partir da Nominal')		
						
						Else					
						
							// Valida resultado informado					
							
							If cResult > cMax//cResult > ( cNominal + cMax)  
							
								lRet:= .F.
								alert( 'Dimensional (ME) - Linha '+str(nI)+': Resultado especificado ('+Str(cResult)+') fora dos limites de m�nimo estabelcidos ('+Str( cMax)+')')		
							
							Endif
							
						Endif					
					
				Endif	
			
						
			Endif		
		
		Next	
	
	Endif
		
	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet

/*/{Protheus.doc} ZZBVldLPos( oModel )

		 P�s-valida��o do modelo de dados ZZBDETAILD (Ensaio Dimensional)
		 
@author  Leandro Heilig (Triyo)
@since   08/12/2016
@version 1.0
@return  oModel
/*/

Static Function ZZCVldLPos( oModel )

	Local lRet       := .T.
	Local aArea      := getArea(aArea)
	Local aSaveLines := FWSaveRows()
	Local cIdModel   := oModel:GetId()
	Local nOperation := oModel:GetOperation()
	Local oMdlZZ8    := oModel:GetModel('ZZCDETAILD')
		
	Local cCmpEns    := ""
	Local cCmpMinMax := ""
	Local cCrtMinMax := ""
	Local cCmpMin    := ""
	Local cCmpMax    := ""
	Local cCmpNomi   := ""
	Local cCmpResult := ""
	Local cMin       := ""
	Local cMax       := ""
	Local cNominal   := ""
	Local cResult    := ""
	Local nI         := 0
	
	If nOperation == 3 .OR. nOperation == 4	
	
		For nI:=1 to oModel:Length()
		
			oModel:GoLine( nI )
		
			If !oModel:IsDeleted()
			
				cCrtMinMax := FwFldGet( 'ZZC_MINMAX' )
				cMin       := FwFldGet( 'ZZC_LIE' )
				cMax       := FwFldGet( 'ZZC_LSE' )
				cNominal   := FwFldGet( 'ZZC_NOMINA' )
				cResult    := FwFldGet( 'ZZC_RESULT' )
				
										
				nVldMin:= At("-",cMin)
						
				If nVldMin > 0
						
					cMin    := SuperVal(SubStr(cMin ,At("-",cMin )+1))
						
				Else
						
					cMin    := SuperVal(cMin)
				
				Endif
						
				cNominal:= SuperVal(cNominal)							
				cMax    := superVal(cMax)	
				cResult := superVal(cResult)	
				
				If cCrtMinMax =='1' // Controla Min e max
			
					//--------------------------------
					// Valida resultado informado		
					//--------------------------------			
					
					If Empty(cMin) .Or. Empty(cMax) 
					
						lRet:= .F.
						alert( 'Dimensional (PA) - Linha '+str(nI)+': Controle M�nimo de m�nimo do ensaio habilitado. Por favor preenche os respectivos campos, informando as varia��es poss�veis a partir dio valor nominal')
					
					Else						
			
						// Valida resultado informado	 
						
						If cResult < cMin //cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Dimensional (PA)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') fora dos limites do m�nimo estabelecidos ('+Str( cMin  )+'). Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
						
						If cResult > cMax //cResult > ( cNominal + cMax  )
						
							lRet:= .F.
							alert( 'Dimensional (PA)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') est� acima do valor nominal estabelecido('+Str( cMax  )+'). Por favor, verifque o resultado ou a varia��o m�nima inferior informado')		
						
						Endif							
					
					Endif
					
				 //--------------------------------------------------
				 // Controle do m�nimo
				 //--------------------------------------------------	
				
				ElseIf cCrtMinMax =='2'   //controla min						  
				  
					// Valida resultado informado					
					
					If Empty(cMin) 
					
						lRet:= .F.
						alert( 'Dimensional (ME)  - Linha '+str(nI)+': Controle de valor M�nimo do ensaio habilitado. Por favor, informe uma varia��o m�nima a partir da Nominal')		
					
					Else														
						
						// Valida resultado informado	 
						
						If cResult < cMin//cResult < ( cNominal - cMin  )
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': O resultado informado ('+Str(cResult)+') fora dos limites do m�nimo estabelecido'+ Str( cMin  )+'. Por favor, verifque o resultado apontado ou a varia��o m�nima informada')		
						
						Endif
						
					Endif			  
				  
				
				ElseIf cCrtMinMax =='3'  // Controla max
				  
					  If Empty(cMax) 
						
							lRet:= .F.
							alert( 'Dimensional (ME)  - Linha '+str(nI)+': Controle de valor M�ximo do ensaio habilitado. Por favor, informe uma varia��o m�xima a partir da Nominal')		
						
						Else					
						
							// Valida resultado informado					
							
							If cResult > cMax//cResult > ( cNominal + cMax)  
							
								lRet:= .F.
								alert( 'Dimensional (ME) - Linha '+str(nI)+': Resultado informado ('+Str(cResult)+')  fora dos limites de m�nimo especificado ('+Str( cMax  )+')')		
							
							Endif
							
						Endif					
					
				Endif	

			Endif		
		
		Next	
	
	Endif
		
	FwRestRows(aSaveLines)
	RestArea(aArea)
	
Return lRet



/*/{Protheus.doc} PRR13Commit( oModel )

		 Valida��o do modelo de dados e grava��o de dados adicionais
		 
@author  Leandro Heilig (Triyo)
@since   09/10/2016
@version 1.0
@return  oModel
/*/

Static Function PRR13Commit( oModel )

	Local lRet          := .T. 
	Local oModel        := FwModelActive()
	Local oMdlZZ7       := oModel:GetModel('ZZ7MASTER')
	Local aAreaZZ7      := ZZ7->( GetArea() )
	Local nOperation    := oModel:GetOperation()
	Local nI            := 0
	Local cStatus       := ""
	Local dDtAlt        := dDataBase
	Local cUsrId        := RetCodUsr()
	
	If nOperation == 4
		
		cStatus := oMdlZZ7:GetValue('ZZ7_STATUS')
		
//		If cStatus <> "04" // OR272 - Pediu para mudar
		
			oMdlZZ7:SetValue("ZZ7_DTALT" ,dDtAlt )
			oMdlZZ7:SetValue("ZZ7_USRALT",cUsrId )
			oMdlZZ7:SetValue("ZZ7_RESPON",usrRetName( cUsrId ) )
			
			alert( 'Log de altera��o gerado')
		
//		Endif // OR272 - Pediu para mudar

	Endif
	
	FWFormCommit( oModel )	
	RestArea(aAreaZZ7)
	
Return lRet


/*/{Protheus.doc} PRR13TdOk( oModel )

		 P�s-Valida��o do modelo de dados
		 
@author  Leandro Heilig (Triyo)
@since   09/10/2016
@version 1.0
@return  oModel
/*/

Static Function PRR13TdOk( oModel )	

	Local	lRet		:=	.T.
	Local	nOperation	:=	oModel:GetOperation()
	Local	aArea		:=	GetArea()
	Local   cStatus     := ""
	
	If ( nOperation == 5 )
	
		cStatus:= oModel:GetModel( 'ZZ7MASTER' ):GetValue( 'ZZ7_STATUS' )
		
		// se o Status for Vigente, j� foi emitido e n�o poder� ser exclu�do
		
		If cStatus== "01" .Or. cStatus=="02"
			
			alert( 'Este certificado j� foi emitido anteriormente e n�o poder� ser exclu�do')
			lRet:= .F.
		
		Endif
		
	ElseIf ( nOperation == 3 )
	
		cStatus := oModel:GetModel( 'ZZ7MASTER' ):GetValue( 'ZZ7_STATUS' )
		
		/**
		  * Se o status for 99 (Criado via assistente) somente � manipulado pelo sistema
		  */
		If ( cStatus == "99" )
			
			alert( 'N�o � permitido selecionar o status 99 - Gerado via assistente!')
			lRet:= .F.
			
		EndIf
	
	ElseIf ( nOperation == 4 )  
	
		cStatus := oModel:GetModel( 'ZZ7MASTER' ):GetValue( 'ZZ7_STATUS' )
		
		/**
		  * Se o status for 99 (Criado via assistente) verifico se foi o usuario que selecionou esta opcao,
		  * pois o status 99 somente � manipulado pelo sistema
		  */
		If ( cStatus == "99" .and. .not. ( ZZ7->ZZ7_STATUS == cStatus ) )
			
			alert( 'N�o � permitido selecionar o status 99 - Gerado via assistente!')
			lRet:= .F.
			
		EndIf
	
	Endif
	
	RestArea(aArea)
	
Return lRet	

/*/{Protheus.doc} PRR13LOADQ( oModel )

		 Se o certificado for gerado pelo assistente, traz os itens marcados como deletados
		 para que o usu�rio possa dar manuten��o.
		 
@author  Leandro Heilig (Triyo)
@since   24/02/2017
@version 1.0
@return  oModel
/*/

Static Function PRR13LOADQ( oModelPRR )

	Local nX			:= 0
	Local lRet          := .T.
	//Local oModelPRR	
	Local nOperation    := 0
	Local oZZ7			:= Nil
	Local oZZ8			:= Nil
	Local oZZ9			:= Nil
	Local oZZA			:= Nil
	Local oZZB			:= Nil
	Local oZZC			:= Nil
	
	Local cStatus       :=""
	
	nOperation := oModelPRR:GetOperation()
	
	oZZ7 := oModelPRR:GetModel('ZZ7MASTER' )
	oZZ8 := oModelPRR:GetModel('ZZ8DETAILQ')
	oZZ9 := oModelPRR:GetModel('ZZ9DETAILM')
	oZZA := oModelPRR:GetModel('ZZADETAILT')
	oZZB := oModelPRR:GetModel('ZZBDETAILD')
	oZZC := oModelPRR:GetModel('ZZCDETAILD')
	
	cStatus  := oZZ7:GetValue('ZZ7_STATUS')
	
	//--------------------------------------------
	// Se o certificado for gerado por assistente
	//-------------------------------------------
	If cStatus == "99" .And. nOperation == 4
	
		For nX := 1 To oZZ8:Length()
		
			oZZ8:GoLine( nX )
			oZZ8:DeleteLine()		
		
		Next nX
		
		For nX := 1 To oZZ9:Length()
		
			oZZ9:GoLine( nX )
			oZZ9:DeleteLine()		
		
		Next nX
				
		For nX := 1 To oZZA:Length()
		
			oZZA:GoLine( nX )
			oZZA:DeleteLine()		
		
		Next nX
		
				
		For nX := 1 To oZZB:Length()
		
			oZZB:GoLine( nX )
			oZZB:DeleteLine()		
		
		Next nX
		
				
		For nX := 1 To oZZC:Length()
		
			oZZC:GoLine( nX )
			oZZC:DeleteLine()		
		
		Next nX
	
	
	Endif	


Return lRet

/*/{Protheus.doc} excluiLote
Funcao auxiliar para exclus�o de certificado em lote
@author Triyo (SDA)
@since 27/04/2017
@version undefined

@type function
/*/
Static Function excluiLote( clCertific )

	local alArea	:= getArea()
	
	default clCertific := ""
	
	dbSelectArea("ZZ7")
	ZZ7->( dbSetOrder( 1 ) )
	
	begin transaction
	
		if ( ZZ7->( dbSeek( xFilial("ZZ7") + clCertific ) ) )
		
			if ( ZZ7->ZZ7_STATUS $ "01/02" )
				alert( 'Este certificado [' + clCertific + '] j� foi emitido anteriormente e n�o poder� ser exclu�do')
			else
			
				if ( recLock( "ZZ7", .F., .T. ) )
					ZZ7->( dbDelete() )
					ZZ7->( msUnLock() )
				endif
			
				delGenCert( "ZZ8", clCertific, .T. )
				delGenCert( "ZZ9", clCertific, .T. )
				delGenCert( "ZZA", clCertific, .T. )
				delGenCert( "ZZB", clCertific, .T. )
				delGenCert( "ZZC", clCertific, .T. )
				
			endif
			
		endif
	
	end transaction
	
	restArea( alArea )

Return

/*/{Protheus.doc} delGenCert
Funcao generica para excluir os registros das referente ao certificado

Como os indices e nome de campos das tabelas ZZ8/ZZ9/ZZA/ZZB/ZZC s�o semelhantes, foi criado esta funcao para
facilitar a exclusao em lote

@author Triyo (SDA)
@since 27/04/2017
@version undefined
@param clAliasAux, characters, description
@param clCertific, characters, description
@type function
/*/
Static Function delGenCert( clAliasAux, clCertific, llSetIndex )

	local alArea	:= getArea()

	default llSetIndex := .F.
	
	if ( .not. empty( clAliasAux ) .and. .not. empty( clCertific ) )
	
		if ( llSetIndex )
			dbSelectArea( clAliasAux )
			( clAliasAux )->( dbSetOrder( 1 ) )
		endif

		if ( ( clAliasAux )->( dbSeek( xFilial( clAliasAux ) + clCertific ) ) )
			
			while (	.not. ( clAliasAux )->( eof() ); 
					.and. ( clAliasAux )->&(clAliasAux+"_FILIAL") == xFilial( clAliasAux ); 
					.and. ( clAliasAux )->&(clAliasAux+"_CODCERT") == clCertific; 
				  )
			
				if ( recLock( clAliasAux, .F., .T. ) )
					( clAliasAux )->( dbDelete() )
					( clAliasAux )->( msUnLock() )
				endif
			
				( clAliasAux )->( dbSkip() )
			endDo
		
		endif
		
	endif
	
	restArea( alArea )
	
Return

/*/{Protheus.doc} PRR013E
Rotina de exclus�o em lote de certificados
@author Triyo (SDA)
@since 26/04/2017
@version undefined

@type function
/*/
User Function PRR013E()

	local clPerg	:= "PRR013E"
	local aSeekOrder:= {}
	local alButtons	:= {}
	Local aSize 	:= MsAdvSize(.F.)
	local oDlg
	local oTela
	local oColumn
	local llConfirma:= .F.
	local alLegPed	:= {}
	
	private apDados	:= {}
	
	/**
	  * Cria grupo de perguntas
	  */
	ajustaSX1( clPerg )
	
	/**
	  * Pergunta parametros para usuario
	  */
	if ( pergunte( clPerg, .T. ) )
	  
		FwMsgRun(,{|oSay| getInfo(oSay) },"Aguarde...","Consultando certificados..." )
	
		If Len( apDados ) > 0

			/**
			  * Monta interface para marca��o dos certificados a serem exclu�dos
			  */
			AADD(alLegPed,{"BR_VERDE"		, 	"Vigente" })
			AADD(alLegPed,{"BR_AMARELO"		, 	"Bloqueado" })
			AADD(alLegPed,{"BR_PRETO"		, 	"Cancelado" })
			AADD(alLegPed,{"BR_AZUL"		, 	"Gerado via Assistente" })
			AADD(alLegPed,{"BR_VERMELHO"	, 	"Incompleto" })
		
			DEFINE MSDIALOG oDlg FROM aSize[7],0 TO aSize[6],aSize[5] TITLE "Itens para Beneficiamento" OF oMainWnd PIXEL
			
			DEFINE FWBROWSE oTela DATA ARRAY ARRAY apDados SEEK ORDER aSeekOrder NO REPORT Of oDlg
			
			ADD MARKCOLUMN oColumn DATA { || iif( apDados[oTela:nAt,1],'LBOK','LBNO') } DOUBLECLICK { || mark(oTela:At()), oDlg:refresh() } HEADERCLICK {|| markAll(oTela), oDlg:refresh(), oTela:refresh(.F.) }  Of oTela
			
			ADD STATUSCOLUMN oColumn DATA {|| setStatus( apDados[oTela:At(),11] ) } DOUBLECLICK { |oTela| BRwLegenda( "Legenda", "Legenda" , alLegPed )   }  OF oTela
						
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),3] } 	Title "Certificado"  	PICTURE PesqPict("ZZ7","ZZ7_CODCER")	DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 10 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),4] } 	Title "Produto"		   	PICTURE PesqPict("ZZ7","ZZ7_PRODUT")	DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 10 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),5] } 	Title "Descri��o"	   	PICTURE PesqPict("ZZ7","ZZ7_DESPRD")	DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 30 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),6] } 	Title "Cod. CLiente"	PICTURE PesqPict("ZZ7","ZZ7_CODCLI")	DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 9 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),7] } 	Title "Loj. Cliente"	PICTURE PesqPict("ZZ7","ZZ7_LOJA")		DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 5 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),8] } 	Title "Cliente"		   	PICTURE PesqPict("ZZ7","ZZ7_DESCLI")	DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 30 Of oTela
			
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),9] } 	Title "Nota Fiscal"	   	PICTURE "@!"							DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 10 Of oTela
				
			ADD COLUMN oColumn DATA { || apDados[oTela:At(),10] } 	Title "Pedido"		   	PICTURE "@!"							DOUBLECLICK {|| mark(oTela:At()), oDlg:refresh()} 	SIZE 10 Of oTela
			
			ACTIVATE FWBrowse oTela
			
			ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, { || llConfirma := .T., oDlg:End() }, { || llConfirma := .F., oDlg:End()},, alButtons) CENTERED
			
		Else
		
			MsgAlert("N�o foram encontrados registros com os par�metros informado.", "Aten��o")
		
		EndIf
	    
		/**
		  * Exclui certificados
		  */
	    if ( llConfirma )
	    	FwMsgRun(,{|oSay| execAutEx(oSay) },"Aguarde...","Excluindo certificados..." )
	  	endif
	    
	endif

Return

/*/{Protheus.doc} execAutEx
Funcao auxiliar para exclusao de cecrtificado
@author Triyo (SDA)
@since 27/04/2017
@version undefined
@param oSay, object, objeto para manipulacao de texto da interface de aguarde
@type function
/*/
Static Function execAutEx( oSay )
	aEval( apDados, {|x| iif( x[1], excluiLote( x[3] ), ) } )
Return

/*/{Protheus.doc} setStatus
Funcao auxiliar para identificar o status
@author sergi
@since 27/04/2017
@version undefined
@param clStatus, characters, codigo do status
@type function
/*/
Static Function setStatus( clStatus )

	if ( clStatus == "01" ) // Vigente
		clRetLeg := "BR_VERDE"
	elseif ( clStatus == "02" ) // Bloqueado
		clRetLeg := "BR_AMARELO"
	elseif ( clStatus == "03" ) // Cancelado
		clRetLeg := "BR_PRETO"
	elseif ( clStatus == "99" ) // Gerado via Assistente
		clRetLeg := "BR_AZUL"
	elseif ( clStatus == "04" ) // Incompleto
		clRetLeg := "BR_VERMELHO"
	endif

Return clRetLeg

/*/{Protheus.doc} ajustaSX1
Funcao de ajuste de grupo de perguntas
@author Triyo (SDA)
@since 26/04/2017
@version undefined
@param clPerg, characters, descricao
@type function
/*/
Static Function ajustaSX1( clPerg )

	PutSx1(clPerg,"01","Certificado De ?","Certificado ?","Certificado ?",;
	"MV_CH1","C",tamSX3("ZZ7_CODCER")[1],0,0,"G","","ZZ7","","","MV_PAR01","","","","" ,"","","","","","","","","","","","",;
	{},{},{})
	
	PutSx1(clPerg,"02","Certificado At� ?","Certificado At� ?","Certificado At� ?",;
	"MV_CH2","C",tamSX3("ZZ7_CODCER")[1],0,0,"G","","ZZ7","","","MV_PAR02","","","","" ,"","","","","","","","","","","","",;
	{},{},{})

Return

/*/{Protheus.doc} mark
Marca/Desmarca item posicionado
@author Triyo (SDA)
@since 26/04/2017
@version undefined
@param nLinha, numeric, numero da linha posicionada
@type function
/*/
Static Function mark( nLinha )
	
	if ( apDados[nLinha,11] $ "01/02" )
		alert( 'Este certificado j� foi emitido anteriormente e n�o poder� ser exclu�do')
		apDados[nLinha,1] := .F.
	else
		apDados[nLinha,1] := .not. apDados[nLinha,1]
	endif
	 
Return .T.

/*/{Protheus.doc} mark
Marca/Desmarca todos os itens, invertendo o status atual
@author Triyo (SDA)
@since 26/04/2017
@version undefined
@type function
/*/
Static Function markAll()
	aEval( apDados, {|x| iif( x[11] $ "01/02", x[1] := .F., x[1] := .not. x[1] ) } )
Return .T.

Static Function getInfo()

	local alArea	:= getArea()
	local clQuery	:= ""
	local clAlias	:= getNextAlias()
	
	clQuery += " SELECT	"
	clQuery += " 	ZZ7_FILIAL	"
	clQuery += " 	,ZZ7_CODCER	"
	clQuery += " 	,ZZ7_CODCLI	"
	clQuery += " 	,ZZ7_LOJA	"
	clQuery += " 	,ZZ7_DESCLI	"
	clQuery += " 	,ZZ7_DESPRD	"
	clQuery += " 	,ZZ7_PEDVEN	"
	clQuery += " 	,ZZ7_NOTA	"
	clQuery += " 	,ZZ7_SERIE	"
	clQuery += " 	,ZZ7_PRODUT	"
	clQuery += " 	,ZZ7_STATUS	"
	
	clQuery += " FROM	"
	clQuery += " 	" + retSQLName("ZZ7") + " ZZ7 "
	
	clQuery += " WHERE	"
	clQuery += " 	ZZ7_FILIAL = '" + xFilial("ZZ7") + "'	"
	clQuery += " 	AND ZZ7_CODCER >= '" + MV_PAR01 + "' "
	clQuery += " 	AND ZZ7_CODCER <= '" + MV_PAR02 + "' "
	clQuery += " 	AND ZZ7.D_E_L_E_T_ = ' ' "
	
	clQuery := changeQuery( clQuery )
	dbUseArea( .T., "TOPCONN",TcGenQry(,,clQuery), clAlias)
	
	( clAlias )->( dbGoTop() )
	apDados := {}
	( clAlias )->( dbEval( {|| AADD( apDados, {	.F.,; 
												( clAlias )->ZZ7_FILIAL,;
												( clAlias )->ZZ7_CODCER,;
												( clAlias )->ZZ7_PRODUT,;
												( clAlias )->ZZ7_DESPRD,;
												( clAlias )->ZZ7_CODCLI,;
												( clAlias )->ZZ7_LOJA,;
												( clAlias )->ZZ7_DESCLI,;
												( clAlias )->ZZ7_NOTA,;
												( clAlias )->ZZ7_PEDVEN,;
												( clAlias )->ZZ7_STATUS; 
							} ) } ) )
	( clAlias )->( dbCloseArea() )
	
	restArea( alArea )
	
Return


/*/{Protheus.doc} mark
Busca as especificoes das normas
@author Triyo (AMJ)
@since 22/08/2017
@version undefined
@type function
/*/
Static Function PRR13EspLoad(cB1_COD,oMdlZZ8,oMdlZZ9,oMdlZZA,oMdlZZB,oMdlZZC,cCodAco,nOperation)

Local lRet	:= .T.
Local lDel	:= .T.

QE7->(dbSetOrder(1))
If QE7->(dbSeek(xFilial("QE7")+cB1_COD,.T.))
	lRet := .T.

	if ( nOperation == 4 )

		if ( .not. oMdlZZ8:canClearData() ) .Or. ;
			( .not. oMdlZZ9:canClearData() ) .Or. ;
			( .not. oMdlZZA:canClearData() ) .Or. ;
			( .not. oMdlZZB:canClearData() ) .Or. ;
			( .not. oMdlZZC:canClearData() )			
	
			If !(MsgYesNo("Para carregar os novos ensaios ser� necessario excluir todos j� existentes. Deseja excluir?"))
				lDel := .F.
			Endif
		
			if ( lDel )
				auxDelMod( oMdlZZ8 )
				auxDelMod( oMdlZZ9 )
				auxDelMod( oMdlZZA )
				auxDelMod( oMdlZZB )
				auxDelMod( oMdlZZC )
			endif
			
		Endif
	Else
	
		oMdlZZ8:clearData()
		oMdlZZ9:clearData()
		oMdlZZA:clearData()
		oMdlZZB:clearData()
		oMdlZZC:clearData()
			
	Endif
	
	if ( lDel )
                             												
		PRR13Load(cCodAco,oMdlZZ8, .T.,{'ZZ8_ENSAIO' ,'ZZ8_DESCRI','ZZ8_SEQLAB','ZZ8_UM'    ,'ZZ8_MINMAX','ZZ8_LIE','ZZ8_LSE'} , "QE7",;
										{'QE7_ENSAIO','QE1_DESCPO','QE7_SEQLAB','QE7_UNIMED','QE7_MINMAX','QE7_LIE','QE7_LSE'},"LABQUI",,nOperation)
			
		PRR13Load(cCodAco,oMdlZZ9, .T.,{'ZZ9_ENSAIO' ,'ZZ9_DESCRI','ZZ9_SEQLAB','ZZ9_UM'    ,'ZZ9_MINMAX','ZZ9_LIE','ZZ9_LSE'} , "QE7",;
										{'QE7_ENSAIO','QE1_DESCPO','QE7_SEQLAB','QE7_UNIMED','QE7_MINMAX','QE7_LIE','QE7_LSE'},"LABFIS",,nOperation)
		
		PRR13Load(cCodAco,oMdlZZA, .T.,{'ZZA_ENSAIO' ,'ZZA_DESC','ZZA_ESPECI'} , "QE8",;
										{'QE8_ENSAIO','QE1_DESCPO','QE8_TEXTO',},"LABFIS",,nOperation)
										
	endif
									
Else

	alert("N�o foi encontrada a especifica��o: "+AllTrim(cB1_COD) + ". Cadastre e depois retorne para a continua��o do certificado.")						

Endif

Return lRet													