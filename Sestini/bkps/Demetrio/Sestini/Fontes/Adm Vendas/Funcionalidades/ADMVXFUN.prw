#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} ADMVXFUN
Bilioteca de funcoes referente ao projeto de ADM de Vendas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return uRet, retorno da funcao executada
@param nlOpc, numeric, Parametro para identificar a funcao a ser executada
@param uParam, undefined, Parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVXFUN( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 1
			uRet := helpAdmv( uParam )
		
		case nlOpc == 2
			uRet := getAlmxPai( uParam )
			
		case nlOpc == 3
			uRet := vldPerc( uParam )
			
		case nlOpc == 4
			uRet := getChvSBE( uParam )
			
		case nlOpc == 5
			uRet := vlIdAlocA3( uParam )
			
		case nlOpc == 6
			uRet := vldIdDup( uParam )
			
		case nlOpc == 7
			uRet := geraIdEnd( uParam )
		
		case nlOpc == 8	
			uRet := chkLocaliz( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} geraIdEnd
Responsavel por controlar a montagem do ID do endereco a ser criado no sistema
@author DS2U (SDA)
@since 18/05/2018
@version 1.0
@return clEndereco, codigo do endereco montado conforme almoxarifado + id do cadastro de cotas
@param clID, characters, ID do cadastro de alocacao
@type function
/*/
Static Function geraIdEnd( clID )

	default clId	:= {}
	
Return {"C" + allTrim( clId ), "R" + allTrim( clId ) }

/*/{Protheus.doc} vldIdDup
Responsavel por checar se o ID de aloca��o informado no cadastro do vendedor � unico para este cadastro, pois os
sistema deve garantir que um endere�o deve ser associado a somente 1 vendedor
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, se .F. o id ja esta sendo utilizado, se .T. esta disponivel

@type function
/*/
Static Function vldIdDup( alParam )

	local llRet		:= .T.
	local clAlias	:= getNextAlias()
	local clCodVend	:= ""
	local clIdAloc	:= ""
	
	default alParam	:= { M->A3_COD, M->A3_XIDALOC }
	
	clCodVend	:= alParam[1]
	clIdAloc	:= alParam[2]
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			A3_COD
			, A3_NOME
			
		FROM
			%TABLE:SA3% SA3
		
		WHERE
			SA3.A3_FILIAL = %XFILIAL:SA3%
			AND NOT ( A3_COD = %EXP:clCodVend% )
			AND A3_XIDALOC = %EXP:clIdAloc%
			AND SA3.%NOTDEL%
		
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
	
		llRet := .F.
		U_ADMVXFUN( 1,	{"A3_XIDALOC",	"O ID de aloca��o [" + allTrim( clIdAloc ) + "] ja est� sendo utilizado para o vendedor "  +; 
										"[" + ( clAlias )->A3_COD + " - " + allTrim( ( clAlias )->A3_NOME ) + "]" } )
	
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} vlIdAlocA3
Responsavel por validar preenchimento do campo de ID de alocacao no cadastro do vendedor
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. Se o codigo selecionado/digitado � valido ou .F., invalido

@type function
/*/
Static Function vlIdAlocA3()

	local alArea	:= getArea()
	local llRet		:= .T.

	// Valida preenchimento de ID de aloca��o
	llRet := vldIdDup( {M->A3_COD, M->A3_XIDALOC } )

	restArea( alArea )

Return llRet

/*/{Protheus.doc} getChvSBE
Responsavel por definir regras de filtro para a consulta padrao de cotas P00
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, Retorno booleano conforme regra aplicada ao filtro da tabela P00

@type function
/*/
Static Function getChvSBE( clIdAloc )

	local clChave	:= XFILIAL("SBE")+SUBS(clIdAloc,1,2)+clIdAloc

Return clChave

/*/{Protheus.doc} helpAdmv
Funcao customizada para apresenta��o de alerta
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@param clMsg, characters, Mensagem do usuario a ser apresentada
@type function
/*/
Static Function helpAdmv( alParam )

	local clCampo	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 0 .and. valType( alParam[1] ) == "C", alParam[1], "" )
	local clMsg		:= iif( valType( alParam ) == "A" .and. len( alParam ) > 1 .and. valType( alParam[2] ) == "C", alParam[2], "" )
	local llConout	:= iif( valType( alParam ) == "A" .and. len( alParam ) > 2 .and. valType( alParam[3] ) == "L", alParam[3], .T. )

	default alParam	:= {}

	if ( .not. empty( clCampo ) .and. .not. empty( clMsg ) )

		if ( llConout )
			conout( clCampo )
			conout( clMsg )
		endif
		
		fwClearHLP()
		help('', 1, getSX3Cache(clCampo, "X3_TITULO"), clCampo, clMsg, 1, 0)
		
	endif
	   
Return

/*/{Protheus.doc} getAlmxPai
Funcao para buscar o codigo do almoxafirado do codigo pai enviado por parametro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return clAlmoxPai, Codigo do almoxarifado conforme o codigo pai

@type function
/*/
Static Function getAlmxPai( clCodPai )

	local alArea	:= getArea()
	local clAlmoxPai:= ""
	
	default clCodPai	:= ""
	
	if ( .not. empty( clCodPai ) )
	
		dbSelectArea("P00")
		P00->( dbSetOrder( 1 ) ) // P00_FILIAL, P00_ID, P00_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( P00->( dbSeek( xFilial("P00") + clCodPai ) ) )
			clAlmoxPai := P00->P00_ALMOX
		else
			helpAdmv( {"P00_ALMOX", "C�digo Pai n�o existe!"} )
		endif
		
	endif
	
	restArea( alArea )
		
Return clAlmoxPai

/*/{Protheus.doc} vldPerc
Funcao para validacao de preenchimento de percentual entre 0,01 e 100 %
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retorna verdadeiro ou falso identificando se o preenchimento do percentual est� v�lido
@param nlPerc, numeric, percentual a ser avaliado
@type function
/*/
Static Function vldPerc( nlPerc )

	local llRet	:= .T.

	default nlPerc := 0
	
	if ( nlPerc <= 0 .or. nlPerc > 100 )
		
		llRet := .F.
		helpAdmv( {"P00_PERCOT", "O percentual deve ser preenchido entre 0.01% e 100% !"} )
		
	endif
	
Return llRet

/*/{Protheus.doc} chkLocaliz
Responsavel por checar se o produto controla endereco, levando em consideracao o cadastro de indicador de produtos (SBZ)
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@return llLocaliz, .T. = Controla endereco / .F. = Nao controla endereco
@param clProduto, characters, descricao
@type function
/*/
Static Function chkLocaliz( clProduto )

	local alArea	:= getArea()
	local llLocaliz	:= .F.
	
	dbSelectArea( "SB1" )
	SB1->( dbSetOrder( 1 ) ) // B1_FILIAL, B1_COD
	
	dbSelectArea( "SBZ" )
	SBZ->( dbSetOrder( 1 ) ) // BZ_FILIAL, BZ_COD
	
	// Verifica se tem indicador de produto e se controla endereco
	if ( SBZ->( dbSeek( FWxFilial("SBZ") + clProduto ) ) )
		llLocaliz := ( SBZ->BZ_LOCALIZ == "S" )
	
	// Se nao verifica no proprio cadastro de produto se controla endereco
	elseif ( SB1->( dbSeek( FWxFilial("SB1") + clProduto ) ) )
		llLocaliz := ( SB1->B1_LOCALIZ == "S" )
	endif
	
	restArea( alArea )
	
Return llLocaliz





/*/{Protheus.doc} SesWrLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 20/04/2018
@version 1.0
@return ${return}, ${return_description}
@param cArq, characters, descricao
@param uTexto, undefined, descricao
@param lConsole, logical, descricao
@param lDateTime, logical, descricao
@param tInicio, , descricao
@param tFim, , descricao
@param lPtinternal, logical, descricao
@type function
/*/
User Function SesWrLog( cArq , uTexto , lConsole , lDateTime , tInicio, tFim , lPtinternal )

Local nHandle 	:= 0
Local nAuxFor 	:= 0
Local cTexto  	:= ""
Local cTempo	:= ''

Local cDrive		:= ""
Local cDir			:= ""
Local cNomeArq		:= ""
Local cExt			:= ""
Local tDuracao		:= ''

Default tInicio		:= ''
Default tFim		:= ''
Default lConsole	:= .T.
Default lDateTime	:= .T.
Default lPtinternal	:= .F.

IF !Empty(tInicio)
	MV_PAR60	:= tInicio
Endif

IF !Empty(tFim)
	tDuracao 	:= ElapTime(MV_PAR60,Time())	
	cTempo 		:= '   ' + SubStr(tDuracao,1,2)+" h: "+SubStr(tDuracao,4,2)+" m: "+SubStr(tDuracao,7,2)+" s: "
Endif

cArq := StrTran(Alltrim(cArq)," ","")

If ! ( "." $ cArq )
	cArq	+= ".log"
Endif   

If lPtInternal
	PtInternal( 1, uTexto )
Endif

If !File( cArq )

	// -- Tratamento para diretorios
	
	SplitPath( cArq , @cDrive, @cDir, @cNomeArq, @cExt )
	MontaDir(cDir)

	nHandle := FCreate( cArq )
	FClose( nHandle )	

Endif

If File( cArq )


	nHandle := FOpen( cArq, 2 )
	FSeek ( nHandle, 0, 2 )			// Posiciona no final do arquivo.
	
	If ValType(uTexto) == "C"
		
		If lDateTime
			cTexto := dtoc(dDataBase) + ";" + Time() + ";" + uTexto
		Else
			cTexto := uTexto
		Endif
		
		IF !Empty( cTempo )
			cTexto	+= cTempo
		Endif
		
		FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
		
		If lConsole
			CONOUT(cTexto)
		Endif
	
	ElseIf ValType(uTexto) == "A"
	
		For nAuxFor := 1 to len(uTexto)
		
			cTexto := dtoc(dDataBase) + " " + Time() + " " + uTexto[nAuxFor]

			IF !Empty( cTempo )
				cTexto	+= cTempo
			Endif
			
			FWrite( nHandle, cTexto + CRLF, Len(cTexto) + 2 )
			
			If lConsole
				CONOUT(cTexto)
			Endif
			
		Next nAuxFor
	
	EndIf
	
	FClose( nHandle )
	
EndIf

Return Nil



/*/{Protheus.doc} SesSwLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 11/06/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
User Function SesSwLog(cArqLog)

Local oMemo		:= NIL 
Local oDlg		:= NIL 
Local cFile		:= "" 
Local cTexto	:= "" 
Local cMask     := "Arquivos Texto" + "(*.csv)|*.csv|"

// --------------------------------------------
// Le arquivo de Log 
cTexto := LeLog(cArqLog)

Define MsDialog oDlg Title "Atualiza��o concluida." From 3, 0 to 340, 417 Pixel

	@ 5, 5 Get oMemo Var cTexto Memo Size 200, 145 Of oDlg Pixel
	oMemo:bRClicked := { || AllwaysTrue() }
	
	Define SButton From 153, 175 Type  1 Action oDlg:End() Enable Of oDlg Pixel // Apaga
	Define SButton From 153, 145 Type 13 Action ( cFile := cGetFile( cMask, "" ), If( cFile == "", .T., ;
	MemoWrite( cFile, cTexto ) ) ) Enable Of oDlg Pixel

Activate MsDialog oDlg Center


Return Nil


//--------------------------------------------------------------------
/*/{Protheus.doc} LeLog
Fun��o de leitura do LOG gerado com limitacao de string

@author TOTVS Protheus
@since  21/05/2018
@obs    Gerado por EXPORDIC - V.5.2.1.0 EFS / Upd. V.4.20.15 EFS
@version 1.0
/*/
//--------------------------------------------------------------------
Static Function LeLog(cArqLog)
Local cRet  := ""
Local cFile := cArqLog
Local cAux  := ""

FT_FUSE( cFile )
FT_FGOTOP()

While !FT_FEOF()

	cAux := FT_FREADLN()

	If Len( cRet ) + Len( cAux ) < 1048000
		cRet += cAux + CRLF
	Else
		cRet += CRLF
		cRet += Replicate( "=" , 128 ) + CRLF
		cRet += "Tamanho de exibi��o maxima do LOG alcan�ado." + CRLF
		cRet += "LOG Completo no arquivo " + cFile + CRLF
		cRet += Replicate( "=" , 128 ) + CRLF
		Exit
	EndIf

	FT_FSKIP()
End

FT_FUSE()

Return cRet

