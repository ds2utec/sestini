#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH"

/*/{Protheus.doc} ADMVA01
Rotina de cadastro de Cotas
@author DS2U (SDA)
@since 18/04/2018
@version 1.0
@return uRet, retorno da funcao que foi executada
@param nlOpc, numeric, numero da opcao da funcao a ser executada
@param uParam, undefined, parametro a ser enviado para a funcao a ser executada
@type function
/*/
User Function ADMVA01( nlOpc, uParam )

	local uRet		:= nil

	default nlOpc	:= 0
	default uParam	:= nil
	
	do case
	
		case nlOpc == 0
			browse()
		
		case nlOpc == 1
			uRet := validAlmox( uParam )
			
		case nlOpc == 2
			uRet := vldPerCota( uParam )
			
		case nlOpc == 3
			uRet := vldPerRet( uParam )
		
		case nlOpc == 4	
			uRet := vldCodPai( uParam )
			
		case nlOpc == 5
			uRet := vldPerCGrp( uParam )
			
		case nlOpc == 6
			uRet := vldPerRGrp( uParam )
			
		case nlOpc == 7
			uRet := vldPerCPrd( uParam )
			
		case nlOpc == 8
			uRet := vldPerRPrd( uParam )
			
		case nlOpc == 9
			uRet := vldId( uParam )
			
		case nlOpc == 10
			uRet := calcId( uParam )
			
		case nlOpc == 11
			uRet := vldRanking( uParam )
			
		case nlOpc == 12
			uRet := vldPerCCat( uParam )
			
		case nlOpc == 13
			uRet := vldPerRCat( uParam )
			
		case nlOpc == 14
			uRet := vldPerCCol( uParam )
			
		case nlOpc == 15
			uRet := vldPerRCol( uParam )
			
		case nlOpc == 16
			uRet := showHierar( uParam )
			
		case nlOpc == 17
			uRet := transfCota( uParam )
			
		case nlOpc == 18
			uRet := showLogReg( uParam )
			
	endCase

Return uRet

/*/{Protheus.doc} browse
Fun��o Browse do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0

@type function
/*/
Static Function browse()

	local alArea	:= P00->( getArea() )

	private oBrowse

	dbSelectArea("P00")
	dbSelectArea("P01")
	dbSelectArea("P02")
	dbSelectArea("P03")
	dbSelectArea("P04")
	dbSelectArea("SZA")
	dbSelectArea("SZB")

	oBrowse 	:= FWMBrowse():New()
	oBrowse:SetAlias("P00")
	oBrowse:SetDescription("Cadastro de Cotas x Canais")
	oBrowse:Activate()	
	
	restArea( alArea )

Return

/*/{Protheus.doc} MenuDef
Funcao de menudef
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return alRotina, array com as a�oes do browse (aRotina)

@type function
/*/
Static Function MenuDef()

	local alRotina := {}
	
	ADD OPTION alRotina TITLE "Pesquisar"				ACTION "PesqBrw"			OPERATION 1 ACCESS 0 // "Pesquisar"
	ADD OPTION alRotina TITLE "Visualizar"				ACTION "VIEWDEF.ADMVA01"	OPERATION 2 ACCESS 0 // "Visualizar"
	ADD OPTION alRotina TITLE "Incluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 3 ACCESS 0 // "Incluir"
	ADD OPTION alRotina TITLE "Alterar"					ACTION "VIEWDEF.ADMVA01"	OPERATION 4 ACCESS 0 // "Alterar"
	ADD OPTION alRotina TITLE "Excluir"					ACTION "VIEWDEF.ADMVA01"	OPERATION 5 ACCESS 0 // "Excluir" 
	ADD OPTION alRotina TITLE "Transfer�ncia"			ACTION "U_ADMVA01(17)"		OPERATION 9 ACCESS 0 // "Copiar"
	ADD OPTION alRotina TITLE "Analisar Hierarquia"		ACTION "U_ADMVA01(16)"		OPERATION 2 ACCESS 0 // "Analisar Hierarquia"
	ADD OPTION alRotina TITLE "Log"						ACTION "U_ADMVA01(18)"		OPERATION 2 ACCESS 0 // "Log" 

Return alRotina

/*/{Protheus.doc} viewDef
Funcao de view para gera��o da interface de cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olView, objeto de modelagem da view

@type function
/*/
Static Function viewDef()
	
	// Cria um objeto de Modelo de Dados baseado no ModelDef do fonte informado
	local olStruP00	:= FWFormStruct( 2, 'P00' )
	Local olStruP01	:= FWFormStruct( 2, 'P01' ) // Cota por Grupo de Produtos
	Local olStruP02	:= FWFormStruct( 2, 'P02' ) // Cota por Categoria de Produtos
	Local olStruP03	:= FWFormStruct( 2, 'P03' ) // Cota por Cole��o de Produtos
	Local olStruP04	:= FWFormStruct( 2, 'P04' ) // Cota por Produtos
	local olModel	:= FWLoadModel( 'ADMVA01' )
	local olView
	
	// Cria o objeto de View
	olView := FWFormView():New()

	// Define qual o Modelo de dados ser� utilizado
	olView:SetModel( olModel )

	//Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	olView:AddField( 'VIEW_P00', olStruP00, 'P00MASTER' )
	
	//Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
	olView:AddGrid( 'VIEW_P01', olStruP01, 'P01DETAIL' )
	olView:AddGrid( 'VIEW_P02', olStruP02, 'P02DETAIL' )
	olView:AddGrid( 'VIEW_P03', olStruP03, 'P03DETAIL' )
	olView:AddGrid( 'VIEW_P04', olStruP04, 'P04DETAIL' )
	
	// Criar um "box" horizontal para receber algum elemento da view
	olView:CreateHorizontalBox( 'SUPERIOR', 35 )
	olView:CreateHorizontalBox( 'INFERIOR', 65 )
	
	//--Cria os Folder's
	olView:CreateFolder('FLDTP01','INFERIOR')
	
	//--Aba Grupo de Produtos
	olView:AddSheet('FLDTP01','GRDTP01','Exce��o - Grupo de Produtos')													
	olView:CreateHorizontalBox('EXGRPPROD'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP01')	

	//--Aba de Categoria de Produtos
	olView:AddSheet('FLDTP01','GRDTP02','Exce��o - Categoria de Produtos')													
	olView:CreateHorizontalBox('EXCATPROD'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP02')
	
	//--Aba Produtos
	olView:AddSheet('FLDTP01','GRDTP03','Exce��o - Cole��es de Produtos')													
	olView:CreateHorizontalBox('EXCOLEPRD'		,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP03')
	
	//--Aba Produtos
	olView:AddSheet('FLDTP01','GRDTP04','Exce��o - Produtos')													
	olView:CreateHorizontalBox('EXPROD'			,100,/*owner*/,/*lUsePixel*/,'FLDTP01','GRDTP04')
	
	// Relaciona o ID da View com o "box" para exibicao
	olView:SetOwnerView( 'VIEW_P00', 'SUPERIOR' )
	olView:SetOwnerView( 'VIEW_P01', 'EXGRPPROD' )
	olView:SetOwnerView( 'VIEW_P02', 'EXCATPROD' )
	olView:SetOwnerView( 'VIEW_P03', 'EXCOLEPRD' )
	olView:SetOwnerView( 'VIEW_P04', 'EXPROD' )
	
	// Liga a identificacao do componente
	olView:EnableTitleView('VIEW_P01','Exce��o - Grupo de Produtos' )
	olView:EnableTitleView('VIEW_P02','Exce��o - Categoria de Produtos' )
	olView:EnableTitleView('VIEW_P03','Exce��o - Cole��es de Produtos' )
	olView:EnableTitleView('VIEW_P04','Exce��o - Produtos' )
	
	//For�a o fechamento da janela na confirma��o
	olView:SetCloseOnOk({||.T.})
	
Return olView

/*/{Protheus.doc} modelDef
Funcao de modelagem de dados do cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return olModel, objeto da modelagem de dados

@type function
/*/
Static Function modelDef()
	
	local olModel
	local olStruP00	:= FWFormStruct( 1, 'P00', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruP01 := FWFormStruct( 1, 'P01', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruP02 := FWFormStruct( 1, 'P02', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruP03 := FWFormStruct( 1, 'P03', /*bAvalCampo*/, /*lViewUsado*/ )
	local olStruP04 := FWFormStruct( 1, 'P04', /*bAvalCampo*/, /*lViewUsado*/ )
	
	//-- Cria a estrutura basica	
	olModel := MPFormModel():New('ADMVA01M', /* bPreValidacao*/,/*bPosValidacao*/ { |olModel| admvTudoOk( olModel ) },/*bCommit { |olModel| admvCommit( olModel ) }*/, { |olModel| admvCancel( olModel ) }/*bCancel*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por campo
	olModel:AddFields( 'P00MASTER', /*cOwner*/, olStruP00, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )
	
	// Adiciona ao modelo uma estrutura de formul�rio de edi��o por grid
	olModel:AddGrid( 'P01DETAIL', 'P00MASTER', olStruP01, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ8VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'P02DETAIL', 'P00MASTER', olStruP02, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'P03DETAIL', 'P00MASTER', olStruP03, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	olModel:AddGrid( 'P04DETAIL', 'P00MASTER', olStruP04, /*{|olModel, nLine, cAction, cField| PRR13LPre(olModel, nLine, cAction, cField)}*//*bLinePre*/, /*{ |olModel| ZZ9VldLPos( olModel ) }*//*bLinePost*/, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	// Faz relaciomaneto entre os compomentes do model
	olModel:SetRelation( 'P01DETAIL', { { 'P01_FILIAL', 'xFilial( "P01" )' }, { 'P01_CODPAI', 'P00_CODPAI' }, { 'P01_ID', 'P00_ID' }}, P01->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'P02DETAIL', { { 'P02_FILIAL', 'xFilial( "P02" )' }, { 'P02_CODPAI', 'P00_CODPAI' }, { 'P02_ID', 'P00_ID' }}, P02->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'P03DETAIL', { { 'P03_FILIAL', 'xFilial( "P03" )' }, { 'P03_CODPAI', 'P00_CODPAI' }, { 'P03_ID', 'P00_ID' }}, P03->( IndexKey( 1 ) ) )
	olModel:SetRelation( 'P04DETAIL', { { 'P04_FILIAL', 'xFilial( "P04" )' }, { 'P04_CODPAI', 'P00_CODPAI' }, { 'P04_ID', 'P00_ID' }}, P04->( IndexKey( 1 ) ) )
	
	//-- Configura o model
	olModel:SetPrimaryKey( {"P00_FILIAL", "P00_CODPAI", "P00_ID"} )
	
	olModel:SetDescription('Cotas x Canais')
	olModel:GetModel('P00MASTER' ):SetDescription('Cotas x Canais')
	
	olModel:GetModel('P01DETAIL'):SetOptional(.T.)
	olModel:GetModel('P02DETAIL'):SetOptional(.T.)
	olModel:GetModel('P03DETAIL'):SetOptional(.T.)
	olModel:GetModel('P04DETAIL'):SetOptional(.T.)
	
	olModel:GetModel('P01DETAIL'):SetUniqueLine({"P01_GRP"})
	olModel:GetModel('P02DETAIL'):SetUniqueLine({"P02_CATEG"})
	olModel:GetModel('P03DETAIL'):SetUniqueLine({"P03_COLEC"})
	olModel:GetModel('P04DETAIL'):SetUniqueLine({"P04_PROD"})
	
	olModel:SetActivate( /*{|olModel| PRR13LOADQ( olModel ) }*/ )
	
Return olModel

Static Function admvCancel( olModel )

	local olModelP00:= olModel:getModel('P00MASTER')
	local nlOpc		:= olModel:getOperation()
	
	// Se for inclusao, da rollBack no sequencial do ID
	if ( nlOpc == 3 )
		rollBackSX8()
	endif
	
Return .T.

/*/{Protheus.doc} admvTudoOk
Responsavel por realizar validacoes e processamentos no commit do MVC
@author sergi
@since 07/05/2018
@version 1.0
@type function
/*/
Static Function admvTudoOk( olModel )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clDesc	:= ""
	local alEndCot	:= {}
	local olModelP00:= olModel:getModel('P00MASTER')
	local nlOpc		:= olModel:getOperation()
	local clDesc	:= ""
	local clAlmox	:= ""
	local llProcOk	:= .T.
	
	// Se for inclusao
	if ( nlOpc == 3 )
	
		dbSelectArea( "SBE" )
		SBE->( dbSetOrder( 1 ) ) // BE_FILIAL, BE_LOCAL, BE_LOCALIZ, BE_ESTFIS
		
		clAlmox		:= allTrim( olModelP00:getValue( "P00_ALMOX" ) )
		clEndereco	:= U_ADMVXFUN( 7, olModelP00:getValue( "P00_ID" ) )[1] // Gera ID do endereco de % Cota
		
		if ( SBE->( dbSeek( xFilial( "SBE" ) + clAlmox + clEndereco ) ) )
		
			llRet := .F.
			U_ADMVXFUN( 1,	{"P00_ALMOX",	"N�o � possivel criar endere�o amarrado a este cadastro, pois j� existe o endere�o " +; 
											allTrim( olModelP00:getValue( "P00_ID" ) ) + " para o armaz�m " +;
											 olModelP00:getValue( "P00_ALMOX" ) } )
		
		else
			
			clDesc		:= allTrim( olModelP00:getValue( "P00_DESC" ) )
			clAlmox		:= allTrim( olModelP00:getValue( "P00_ALMOX" ) )
			clEndereco	:= U_ADMVXFUN( 7, olModelP00:getValue( "P00_ID" ) )[1] // Gera ID do endereco de % Cota
			
			// Tratamento para nao estourar o tamanho do campo de descricao
			if ( len( clDesc ) > tamSX3( "BE_DESCRIC" )[1] )
				clDesc := subs( clDesc, 1, tamSX3( "BE_DESCRIC" )[1] )
			endif
			
			AADD( alEndCot, { clAlmox, clEndereco, clDesc } )
			
			// Endereco vinculado ao ID para % Retido
			clEndereco := U_ADMVXFUN( 7, olModelP00:getValue( "P00_ID" ) )[2] // Gera ID do endereco de % Retido
			
			AADD( alEndCot, { clAlmox, clEndereco, clDesc + " RETIDO" } )
			
			begin transaction

				for nlx := 1 to len( alEndCot )
	
					// Checa se o armazem + endereco existe, e caso nao exista cria o endereco no ERP
					if ( .not. U_ADMVP01( 4, { alEndCot[nlx][1], alEndCot[nlx][2], alEndCot[nlx][3] } ) )
						llProcOk := .F.
						disarmTransaction()
						exit
					endif
	
				next nlx
				
			end transaction
			
			if ( .not. llProcOk )
				// A funcao getSXeNum esta sendo chamada do inicializador padrao do campo P00_ID, QUANDO FOR INCLUSAO
				confirmSX8()
			endif
		
		endif
		
	// Se for exclusao
	elseif ( nlOpc == 5 )
	
		llRet := chkCanDel( olModelP00:getValue( "P00_ALMOX" ), olModelP00:getValue( "P00_ID" ) )
	
		// Valida exclusao de endereco
		// *** DEVE SER A ULTIMA VALIDACAO A SER EXECUTADA
		llRet := ( llRet .and. delEndVinc( olModelP00:getValue( "P00_CODPAI" ), olModelP00:getValue( "P00_ID" ), olModelP00:getValue( "P00_ALMOX" ) ) )
	
	endif
	
	restArea( alArea )

Return llRet

/*/{Protheus.doc} chkCanDel
Responsavel por checar se o cadastro corrente pode ser excluido
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. Se pode ser excluido ou .F. se nao pode ser excluido
@param clID, characters, ID do cadastro com a inten��o de ser excluido
@type function
/*/
Static Function chkCanDel( clAlmox, clID )

	local alArea	:= getArea()
	local llRet		:= .T.
	local clAlias	:= getNextAlias()
	local clEndereco:= ""
	
	default clAlmox	:= ""
	default clID	:= ""
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			P00_ID
			
		FROM
			%TABLE:P00% P00
			
		WHERE
			P00.P00_FILIAL = %XFILIAL:P00%
			AND P00.P00_CODPAI = %EXP:clID%
			AND P00.%NOTDEL%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
		llRet := .F.
		U_ADMVXFUN( 1,	{"P00_ID", "Este cadastro tem amarra��o com n�veis abaixo deste. Portanto n�o pode ser exclu�do!" } )
	endif
	( clAlias )->( dbCloseArea() )
	
	if ( llRet )
	
		clEndereco := U_ADMVXFUN( 7, clID )[1] // Gera ID do endereco de % Cota 
	
		clAlias	:= getNextAlias()
		BEGINSQL ALIAS clAlias
		
			SELECT
				A3_COD
				, A3_NOME
				
			FROM
				%TABLE:SA3% SA3
				
			WHERE
				SA3.A3_FILIAL = %XFILIAL:SA3%
				AND SA3.A3_XIDALOC = %EXP:clEndereco%
				AND SA3.%NOTDEL%
		
		ENDSQL
		
		if ( .not. ( clAlias )->( eof() ) )
			llRet := .F.
			U_ADMVXFUN( 1,	{"P00_ID", "O vendedor " + allTrim( ( clAlias )->A3_COD ) + " - " + allTrim( ( clAlias )->A3_NOME ) + " est� vinculado a este canal. Altere o cadastro do vendedor para que seja poss�vel continuar com a exclus�o deste registro!"  } )
		endif
		( clAlias )->( dbCloseArea() )
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} delEndVinc
Responsavel por desvincular o cadastro de endereco do ID que esta sendo excluido
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, .T. = Se a exclusao foi feita com sucesso, .F. com erros
@param clCodPai, characters, Codigo pai do cadastro de cotas
@param clID, characters, ID do cadastro de cotas
@param clAlmox, characters, Codigo do almoxarifado
@type function
/*/
Static Function delEndVinc( clCodPai, clID, clAlmox )
	
	local alArea	:= getArea()
	local llRet		:= .T.
	local alVetor	:= {}
	local clEndereco:= ""
	local clMsgErro	:= ""
	
	private lMsErroAuto := .F.

	// Se nao for o primeiro nivel, exclui o endereco amarrado ao cadastro de cotas
	if ( .not. empty( clCodPai ) )
	
		clEndereco := U_ADMVXFUN( 7, clID )[1] // Gera ID do endereco de % Cota 

		dbSelectArea( "SBE" )
		SBE->( dbSetOrder( 1 ) ) // BE_FILIAL, BE_LOCAL, BE_LOCALIZ, BE_ESTFIS
		
		if ( SBE->( dbSeek( FWxFilial( "SBE" ) + PADR( clAlmox, tamSX3("BE_LOCAL")[1] ) + PADR( clEndereco, tamSX3("BE_LOCALIZ")[1] ) ) ) )
		
			alVetor := 	{{"BE_LOCAL"  	,clAlmox	,nil},;				
						{"BE_LOCALIZ"	,clEndereco	,nil}}

			MSExecAuto({|x,y| MATA015(x,y)},alVetor, 5)
				
			if ( lMsErroAuto )
				llRet := .F.
				mostraErro()
				U_ADMVXFUN( 1,	{"P00_ALMOX", "Falha na exclus�o automatica de endere�o. Verifique o problema e tente novamente!" } )
			endif
			
		else
			llRet := .F.
			U_ADMVXFUN( 1,	{"P00_ALMOX", "Armaz�m/Endere�o n�o encontrado. " + clAlmox + "/" + clEndereco } )
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} validAlmox
Funcao para validacao de preenchimento de armaz�m no cadastro de cotas
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, Retona se o almoxarifado informado � valido

@type function
/*/
Static Function validAlmox()

	local alArea	:= getArea()
	local llRet		:= .T.
	local clAlmox	:= ""
	local clAlmoxDig:= fwFldGet("P00_ALMOX")
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	
	dbSelectArea("NNR")
	NNR->( dbSetOrder( 1 ) ) // NNR_FILIAL, NNR_CODIGO, R_E_C_N_O_, D_E_L_E_T_
	
	if ( NNR->( dbSeek( xFilial("NNR") + clAlmoxDig ) ) )
	
		if ( .not. empty( M->P00_CODPAI ) )
		
			clAlmox	:= U_ADMVXFUN( 2, clCodPai )
			
			// Valida preenchimento do codigo pai
			if ( .not. empty( clAlmox ) .and. .not. ( clAlmox == clAlmoxDig ) .and. NNR->( dbSeek( xFilial("NNR") + clAlmox ) ) )

				llRet := .F.
				U_ADMVXFUN( 1,	{"P00_ALMOX", "O armaz�m deve ser o mesmo do cadastro pai: " + allTrim( NNR->NNR_CODIGO ) + " - " + allTrim( NNR->NNR_DESCRI ) } )
							 
			endif
			
		endif
		
		if ( almoxInUse( clCodPai, clIdAtu, clAlmoxDig ) )
			U_ADMVXFUN( 1,	{"P00_ALMOX", "Armaz�m n�o pode ser usando novamente neste n�vel!" } )
			llRet := .F.
		endif
			
	else
		llRet := .F.
		// Nao precisa de alert pois no MVC quando tem consulta padrao ja existe validacao de integridade de dados
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} almoxInUse
Funcao auxiliar para identificar se o armazem ja esta em uso no nivel de canais (codig pai em branco)
@author DS2U (SDA)
@since 22/04/2018
@version 1.0
@return llRet, Retonar se o armazem esta em uso (.T.) ou n�o (.F.)
@param clCodPai, characters, codigo pai do cadastro de cotas
@param clIdAtu, characters, id atual do cadastro de cotas, utilizado para consultas registros diferentes deste ID
@param clAmoxAtu, characters, Almoxarifado que esta tentando ser cadastrado e que precisa ser validado
@type function
/*/
Static Function almoxInUse( clCodPai, clIdAtu, clAmoxAtu )

	local llRet	:= .F.
	local clAlias

	if ( .not. empty( clIdAtu ) .and. empty( clCodPai ) )
	
		clAlias := getNextAlias()
	
		BEGINSQL ALIAS clAlias
		
			SELECT
				P00_ALMOX
				
			FROM
				%TABLE:P00% P00
				
			WHERE
				P00_FILIAL = %XFILIAL:P00%
				AND P00_CODPAI = %EXP:clCodPai%
				AND P00_ID <> %EXP:clIdAtu%
				AND P00_ALMOX = %EXP:clAmoxAtu%
				AND P00.%NOTDEL%
		
		ENDSQL
		
		if ( .not. ( clAlias )->( eof() ) )
			llRet := .T.
		endif
		( clAlias )->( dbCloseArea() )
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCota
Funcao para validacao de preenchimento do percental da cota
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCota()

	local llRet		:= .F.
	local nlPerCot	:= fwFldGet('P00_PERCOT')
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, nlPerCot ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPerCot,,,,, @nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"P00_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCGrp
Funcao para validacao de preenchimento do percental da cota do grupo de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCGrp()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('P01_PERCOT')
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local clCodGrp	:= fwFldGet('P01_GRP')
	local nlSaldoPer:= 0

	// Valida preenchimento de percentual entre 0,01 e 100
	if ( U_ADMVXFUN( 3, nlPercAtu ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu, clCodGrp,,,,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"P01_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de grupo de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCCat
Funcao para validacao de preenchimento do percental da cota de Categoria de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCCat()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('P02_PERCOT')
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local clCodCat	:= fwFldGet('P02_CATEG')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->P02_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,clCodCat,,,@nlSaldoPer ) )		     
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"P01_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCCol
Funcao para validacao de preenchimento do percental da cota de colecoes de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCCol()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('P03_PERCOT')
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local clCodCol	:= fwFldGet('P03_COLEC')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->P03_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,,clCodCol,,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"P03_COLEC", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de cole��es de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerCPrd
Funcao para validacao de preenchimento do percental da cota de produtos
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerCPrd()

	local llRet		:= .F.
	local nlPercAtu	:= fwFldGet('P04_PERCOT')
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local clCodPrd	:= fwFldGet('P04_PROD')
	local nlSaldoPer:= 0

	if ( U_ADMVXFUN( 3, M->P04_PERCOT ) )
		
		if ( vPercNivel( clCodPai, clIdAtu, nlPercAtu,,,,clCodPrd,@nlSaldoPer ) )
			llRet := .T.
		else
			U_ADMVXFUN( 1,	{"P04_PERCOT", "Percentual digitado ultrapassa o limite de 100% da cota do n�vel e da exce��o de produtos. O saldo dispon�vel para o n�vel � de " + allTrim( transform( nlSaldoPer, "999,99" ) ) + "%" } )
		endif
	
	endif

Return llRet

/*/{Protheus.doc} vldPerRet
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRet()
Return U_ADMVXFUN( 3, M->P00_PERRET )


/*/{Protheus.doc} vldPerRet
Funcao para validacao de preenchimento do percental retino no armaz�m / endenreco do grupo de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRGrp()
Return U_ADMVXFUN( 3, M->P01_PERRET )

/*/{Protheus.doc} vldPerRCat
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRCat()
Return U_ADMVXFUN( 3, M->P02_PERRET )

/*/{Protheus.doc} vldPerRPrd
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de cole��es de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRCol()
Return U_ADMVXFUN( 3, M->P03_PERRET )

/*/{Protheus.doc} vldPerRPrd
Funcao para validacao de preenchimento do percental retido no armaz�m / endenreco de produtos do cadastro
@author DS2U (SDA)
@since 19/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldPerRPrd()
Return U_ADMVXFUN( 3, M->P04_PERRET )

/*/{Protheus.doc} vldCodPai
Valida preenchimento do codigo pai
@author DS2U (SDA)
@since 20/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldCodPai()

	local alArea	:= getArea()
	local llRet		:= .T.
	local olModel	:= fwModelActive()
	local nlOperation	:= olModel:getOperation()
	local clCodPai	:= fwFldGet('P00_CODPAI')
	
	if ( llRet )
	
		if ( empty( clCodPai ) )
			fwFldPut("P00_ALMOX", criaVar("P00_ALMOX"),,,,.T. /*for�a atribuicao com loadvalue*/)
		else
	
			dbSelectArea("P00")
			P00->( dbSetOrder( 1 ) ) // P00_FILIAL, P00_ID, P00_CODPAI, R_E_C_N_O_, D_E_L_E_T_
			
			if ( .not. P00->( dbSeek( xFilial("P00") + clCodPai ) ) )
				llRet := .F.
				U_ADMVXFUN( 1,	{"P00_CODPAI", "C�digo n�o existe." } )
			else
				// Esta instru��o � necessaria pois se o usuario digitar o codigo pai ao inves de selecionar pela lupa, o gatilho do armazem nao funciona
				fwFldPut("P00_ALMOX", P00->P00_ALMOX)
			endif
			
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} vldId
Funcao para validar o preenchimento do ID do cadastro de cotas
@author DS2U (SDA)
@since 22/04/2018
@version 1.0
@return llRet, booleano identificado se .T.=Preenchimento Valido / .F.= Preenchimento invalido

@type function
/*/
Static Function vldId()

	local alArea	:= getArea()
	local llRet		:= .T.
	local clId		:= &( readVar() )
	local olModel	:= fwModelActive()
	local nlOperation:= olModel:getOperation()
	local clIdAtu	:= fwFldGet('P00_ID')
	local clCodPai	:= fwFldGet('P00_CODPAI')
	
	if ( nlOperation == 4 ) // se for alteracao
	
		// Nao permite alterar o id pois existem dependencias deste codigo, como enderecos e filhos na hierarquia
		if ( llRet .and. .not. ( P00->P00_ID == clIdAtu ) )
		
			llRet := .F.
			U_ADMVXFUN( 1,	{"P00_ID", "Este ID n�o pode ser alterado. Se estiver errado deve ser excluido para que seja mantido a integridade dos dados!" } )
		
		endif
	
	endif
	
	
	if ( llRet .and. .not. empty( clId ) )
	
		dbSelectArea("P00")
		P00->( dbSetOrder( 1 ) ) // P00_FILIAL, P00_ID, P00_CODPAI, R_E_C_N_O_, D_E_L_E_T_
		
		if ( P00->( dbSeek( xFilial("P00") + clId + clCodPai ) ) )
			U_ADMVXFUN( 1,	{"P00_ID", "Este ID j� esxiste no n�vel atual. Mude o C�digo Pai ou o ID!" } )
			llRet := .F.
		endif
		
	endif
	
	restArea( alArea )
	
Return llRet

/*/{Protheus.doc} vPercNivel
Funcao para validacao de percentual do nivel do cadastro atual
@author sergi
@since 23/04/2018
@version 1.0
@return llRet, Retorna se o percentual corrente digitado pode ser utilizado no nivel atual do cadastro que esta sendo validado
@param clCodPai, characters, codigo pai do cadastro de cotas
@param clIdAtu, characters, id do cadastro de cotas
@param nlPercAtu, numeric, percentual digitado a ser validado
@param [clCodGrp], characters, Codigo do grupo de produto
@param [clCodCat], characters, Codigo de Categoria de produto
@param [clCodCol], characters, Codigo de Colecao de produto
@param [clCodPrd], characters, Codigo de produto
@param [nlSaldoPer], numeric, percentual digitado a ser validado
@type function
/*/
Static Function vPercNivel( clCodPai, clIdAtu, nlPercAtu, clCodGrp, clCodCat, clCodCol, clCodPrd, nlSaldoPer )

	local clAlias	:= getNextAlias()
	local clField	:= ""
	local clTable	:= "% " + retSQLTab("P00")
	local clWhere	:= "% P00.D_E_L_E_T_ = ' ' "
	local llRet		:= .F.
	
	default clCodPai	:= ""
	default clIdAtu		:= ""
	default nlPercAtu	:= 0
	default clCodGrp	:= ""
	default clCodCat	:= ""
	default clCodCol	:= ""
	default clCodPrd	:= ""
	default nlSaldoPer	:= 0
	
	if ( fwIsInCallStack("VLDPERCOTA") ) // Se a validacao vem do cabe�alho

		clField := "%COALESCE( SUM( P00_PERCOT ), 0 ) AS PERCOT%"
		
	elseif ( fwIsInCallStack("VLDPERCGRP") ) // Se a validacao vem da aba do grupo de produtos
	
		clField := "%COALESCE( SUM( P01_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("P01")
		clWhere += " AND P01_FILIAL = P00_FILIAL AND P01_ID = P00_ID AND P01_GRP = '" + clCodGrp + "' AND P01.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCCAT") ) // Se a validacao vem da aba de Categoria de produtos
	
		clField := "%COALESCE( SUM( P02_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("P02")
		clWhere += " AND P02_FILIAL = P00_FILIAL AND P02_ID = P00_ID AND P02_CATEG = '" + clCodCat + "' AND P02.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCCOL") ) // Se a validacao vem da aba de produtos
	
		clField := "%COALESCE( SUM( P03_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("P03")
		clWhere += " AND P03_FILIAL = P00_FILIAL AND P03_ID = P00_ID AND P03_COLEC = '" + clCodCol + "' AND P03.D_E_L_E_T_ = ' ' "
		
	elseif ( fwIsInCallStack("VLDPERCPRD") ) // Se a validacao vem da aba de produtos
	
		clField := "%COALESCE( SUM( P04_PERCOT ), 0 ) AS PERCOT%"
		clTable	+= " ," + retSQLTab("P04")
		clWhere += " AND P04_FILIAL = P00_FILIAL AND P04_ID = P00_ID AND P04_PROD = '" + clCodPrd + "' AND P04.D_E_L_E_T_ = ' ' "
		
	endif
	
	clTable	+= " %"
	clWhere	+= " %"

	BEGINSQL ALIAS clAlias
	
		SELECT
			%EXP:clField%
			
		FROM
			%EXP:clTable%
			
		WHERE
			P00_FILIAL = %XFILIAL:P00%
			AND P00_CODPAI = %EXP:clCodPai%
			AND P00_ID <> %EXP:clIdAtu%
			AND %EXP:clWhere%
	
	ENDSQL
	
	if ( .not. ( clAlias )->( eof() ) )
	
		nlSaldoPer := ( 100 - ( clAlias )->PERCOT )
		llRet := ( nlPercAtu <= nlSaldoPer )
	
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} calcId
Funcao para calcular o proximo ID conforme cadastro corrente
@author DS2U
@since 30/04/2018
@version 1.0
@return clID, ID inexistente na tabela P00
@param [clID], characters, clID para checar se j� existe na tabela P00. Se nao existir sera considerado no retorna da funcao

@type function
/*/
Static Function calcId( clID )

	local alArea	:= getArea()
	
	default clID	:= ""
	
	if ( empty( clID ) )
		clID := getSXENum( "P00", "P00_ID", 1 )
	endif
	
	dbSelectArea( "P00" )
	P00->( dbSetOrder( 1 ) )
	
	while ( P00->( dbSeek( FWxFilial("P00") + clID ) ) )
	
		confirmSX8()
		clID := getSXENum( "P00", "P00_ID", 1 )
	
	endDo
	
	restArea( alArea )

Return clID

/*/{Protheus.doc} vldRanking
Funcao para validacao de preenchimento de ranking
Nao pode ter o mesmo valor do ranking dentro do mesmo nivel (Cod. Pai iguais)
@author DS2U (SDA)
@since 30/04/2018
@version 1.0
@return llRet, Retorna se o numero do ranking pode ser utilizado

@type function
/*/
Static Function vldRanking()

	local clAlias	:= getNextAlias()
	local clCodPai	:= fwFldGet('P00_CODPAI')
	local clNivel	:= allTrim( str( fwFldGet('P00_PRIORI') ) )
	local llRet		:= .F.
	
	BEGINSQL ALIAS clAlias
	
		SELECT 
			P00_ID
			, P00_DESC
			
		FROM 
			%TABLE:P00% P00
			 
		WHERE 
			P00_FILIAL = %XFILIAL:P00%
			AND P00_CODPAI = %EXP:clCodPai%
			AND P00_PRIORI = %EXP:clNivel%
			AND P00.%NOTDEL%
	
	ENDSQL
	
	if ( ( clAlias )->( eof() ) )
		llRet := .T.
	else
		U_ADMVXFUN( 1,	{"P00_PRIORI", "Este valor de Ranking j� est� sendo utilizado em -> ID " + allTrim( ( clAlias )->P00_ID ) + " - " + allTrim( ( clAlias )->P00_DESC ) } )
	endif
	( clAlias )->( dbCloseArea() )

Return llRet

/*/{Protheus.doc} showHierar
Monta a hierarquia do  cadastro de cotas e mostra em interface visual modelo de arvore de niveis
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function showHierar()
	
	local alTree := {}

	FwMsgRun(,{|oSay| alTree := execHierar() },"Aguarde...","Processando dados..." )
	
	if ( len( alTree ) > 0 )
		showInTree( alTree )
	endif
	
Return

/*/{Protheus.doc} showHierar
Executa a montagem a hierarquia do  cadastro de cotas e mostra em interface visual modelo de arvore de niveis
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function execHierar()

	local alArmazens	:= getLocAdmv()
	local nlx
	local alTree		:= {}
	local clAlias
	local clNoPai		:= "00"
	local clNo			:= ""
	local clImage1		:= ""
	local clImage2		:= ""
	local nlCount		:= 1
	
	for nlx := 1 to len( alArmazens )
	
		// Buscao a hierarquia ou mapa de distribuicao de cotas
		clAlias := U_ADMVP01( 1, { alArmazens[nlx][1], .F. } )
		
		if ( .not. empty( clAlias ) )
		
			while ( .not. ( clAlias )->( eof() ) )
			
				clCodPai := ( clAlias )->NIVEL
				
				while ( .not. ( clAlias )->( eof() ) .and. ( clAlias )->NIVEL == clCodPai )
				
					if ( empty( clCodPai ) )
						clImage1 := "CONTAINR"
						clNo := clNoPai
					else
						clImage1 := "VENDEDOR"
					endif
				
					AADD( alTree, {clNo,	strZero( nlCount,7), "", allTrim( ( clAlias )->DESCID ) +; 
											" - " + allTrim( transform( ( clAlias )->PERCOT, "@9 999.99" ) ) + "% Cota" +;
											" - " + allTrim( transform( ( clAlias )->PERRET, "@9 999.99" ) ) + "% Retido";
											, clImage1, clImage2} )
										
					nlCount++
					( clAlias )->( dbSkip() )
				endDo
				
				clNo := soma1( clNo )
				
			endDo
			( clAlias )->( dbCloseArea() )
		
		endif
	
	next nlx
	
Return alTree

/*/{Protheus.doc} getLocAdmv
Responsavel por retornar os armazens envolvidos no ADMV
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return alLocais, array Bidimensional onde cada elemento � um {C�DIGO DE ARMAZ�M DO ADMV, DESCRICAO}

@type function
/*/
Static Function getLocAdmv()

	local alLocais	:= {}
	local clAlias	:= getNextAlias()
	
	BEGINSQL ALIAS clAlias
	
		SELECT
			DISTINCT
			P00_ALMOX
			, NNR_DESCRI
		
		FROM
			%TABLE:P00% P00
			
		LEFT JOIN
			%TABLE:NNR% NNR ON
			NNR.NNR_FILIAL = %XFILIAL:NNR%
			AND NNR_CODIGO = P00_ALMOX
			
		WHERE
			P00.P00_FILIAL = %XFILIAL:P00%
			AND P00.%NOTDEL%
			
		ORDER BY P00_ALMOX
	
	ENDSQL
	
	( clAlias )->( dbGoTop() )
	( clAlias )->( dbEval( {|| AADD( alLocais, { ( clAlias )->P00_ALMOX, ( clAlias )->NNR_DESCRI } ) } ) )
	( clAlias )->( dbCloseArea() )	

Return alLocais

/*/{Protheus.doc} showInTree
Responsavel por montar o objeto de arvore para mostrar a hierarquia do cadastro de cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param alTree, array of logical, Array contendo a hierarquia do objeto dbTree
@type function
/*/
Static Function showInTree( alTree )
	
	local olDlg
	local olTree
	
	DEFINE DIALOG olDlg TITLE "An�lise de Hierarquia" FROM 180,180 TO 550,700 PIXEL
	
		// Cria o objeto 
		olTree := dbTree():New(0,0,160,260,olDlg,,,.T.)
		
		// M�todo para carga dos itens
		olTree:PTSendTree( alTree )
		olTree:ALIGN := CONTROL_ALIGN_ALLCLIENT
	
	ACTIVATE DIALOG olDlg CENTERED

Return

/*/{Protheus.doc} transfCota
Rotina responsavel por realizar a transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0

@type function
/*/
Static Function transfCota()

	local clFunc	:= "ADMVA01"
	local clTitle	:= "Transf�ncia entre Cotas"
	local clDesc	:= "Este programa tem por objetivo realizar transfer�ncia entre cotas"
	local clPerg	:= "ADMVA01001"

	createSX1( clPerg )

	tNewProcess():New(	clFunc;
						, clTitle;
						, {|oSelf| execTransf( oSelf, MV_PAR01, MV_PAR02, MV_PAR03, MV_PAR04 ) };
						, clDesc;
						, clPerg )

Return

/*/{Protheus.doc} createSX1
Responsavel por controlar o pergunte da rotina de transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param cPerg, characters, descricao
@type function
/*/
Static Function createSX1( cPerg )

	local aAreaBKP := GetArea()
	local lTipLocl := .T. 
	local i
	local aPergunt	:= {}

	AADD(aPergunt,{cPerg,"01","ID Cota Origem      ?","MV_CH1","C",14,0,"G","","MV_PAR01","","","","","","P00",""})
	AADD(aPergunt,{cPerg,"02","ID Cota Destino     ?","MV_CH2","C",14,0,"G","","MV_PAR02","","","","","","P00",""})
	AADD(aPergunt,{cPerg,"03","Produto             ?","MV_CH3","C",15,0,"G","","MV_PAR03","","","","","","SB1",""})
	AADD(aPergunt,{cPerg,"04","Quantidade          ?","MV_CH4","N",tamSX3("BF_QUANT")[1],tamSX3("BF_QUANT")[2],"G","","MV_PAR04","","","","","","",""})
	
	dbSelectArea("SX1")
	SX1->( dbSetOrder( 1 ) )
	SX1->( dbGoTop() )
	
	cPerg := padr(aPergunt[1,1],len(X1_GRUPO))
	For i := 1 To Len(aPergunt)
		
		lTipLocl := !SX1->(dbSeek(cPerg+aPergunt[i,2]))
		
		if ( SX1->(RecLock("SX1",lTipLocl ) ) )
			SX1->X1_GRUPO		:= cPerg
			SX1->X1_ORDEM		:= aPergunt[i,2]
			SX1->X1_PERGUNT		:= aPergunt[i,3]
			SX1->X1_PERSPA		:= aPergunt[i,3]
			SX1->X1_PERENG		:= aPergunt[i,3]
			SX1->X1_VARIAVL		:= aPergunt[i,4]
			SX1->X1_TIPO		:= aPergunt[i,5]
			SX1->X1_TAMANHO		:= aPergunt[i,6]
			SX1->X1_DECIMAL		:= aPergunt[i,7]
			SX1->X1_GSC			:= aPergunt[i,8]
			SX1->X1_VALID		:= aPergunt[i,09]
			SX1->X1_VAR01		:= aPergunt[i,10]
			SX1->X1_DEF01		:= aPergunt[i,11]
			SX1->X1_DEF02		:= aPergunt[i,12]
			SX1->X1_DEF03		:= aPergunt[i,13]
			SX1->X1_DEF04		:= aPergunt[i,14]
			SX1->X1_DEF05		:= aPergunt[i,15]
			SX1->X1_F3			:= aPergunt[i,16]
			SX1->X1_PICTURE		:= aPergunt[i,17]
			SX1->( msUnlock() )
		endif
		
	next i
	
	restArea(aAreaBKP)

Return

/*/{Protheus.doc} execTransf
Executa a transferencia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@param oSelf, object, Ojeto tNewProcess para manipula��o de controle de barra de progresso
@param clIdOri, characters, Codigo do ID origem do cadastro de cotas
@param clIdDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function execTransf( oSelf, clIdOri, clIdDest, clProduto, nlQuant )

	local alArea	:= getArea()
	local nlProc1	:= 2
	local nlProc2	:= 2
	local clAlmoxTrf:= ""
	local clEndDe	:= ""
	local clEndAte	:= ""
	local llProcOk	:= .F.
	local alItem	:= {}
	local alAuto	:= {}
	local clArqErro	:= ""
	local clMsgErro	:= ""
	local clMsg		:= ""
	
	private cpErro	:= ""
	private npRecno	:= 0
	private lMsErroAuto	:= .F.

	default clIdOri		:= ""
	default clIdDest	:= ""
	default clProduto	:= ""
	default nlQuant		:= 0
	
	// Valida parametros
	if ( vldParam01(clIdOri, clIdDest, clProduto, nlQuant) )
	
		// Define quantidade de passos a serem executados para validacao de quantidades
		oSelf:setRegua1( nlProc1 )
		
		// Define quantidade de passos a serem executados para transferencia
		oSelf:SetRegua2( nlProc2 )
		
		dbSelectArea ( "P00" )
		P00->( dbSetOrder( 1 ) ) // P00_FILIAL, P00_ID, P00_CODPAI, R_E_C_N_O_, D_E_L_E_T_
				
		if ( P00->( dbSeek( xFilial("P00") + clIdOri ) ) )
		
			npRecno := P00->( recno() )
		
			U_STNXLOG( 1 , { "ADMVA01", "P00", npRecno, "Iniciando processo de transfer�ncia de cotas" }, @cpErro )
		
			oSelf:saveLog( "Validando saldo em estoque para transfer�ncia entre contas" )
			oSelf:incRegua1( "Validando saldo em estoque para transfer�ncia entre contas" )
			
			clAlmoxTrf	:= P00->P00_ALMOX
			clEndDe		:= U_ADMVXFUN( 7, clIdOri )[1] // Gera ID do endereco de % Cota da origem
			clEndAte	:= U_ADMVXFUN( 7, clIdDest )[1] // Gera ID do endereco de % Cota do destino

			// Valida saldo em estoque para transferencia
			oSelf:incRegua1( "Validando saldo no endereco " + clEndDe )
			llProcOk := vldEstoqTrf( clAlmoxTrf, clEndDe, clProduto, nlQuant )
			
			if ( llProcOk )
				
				oSelf:saveLog( "Montando estrutura para rotina automatica de transfer�ncia entre endere�os" )
				oSelf:incRegua2( "Montando estrutura para rotina automatica de transfer�ncia entre endere�os" )
				alItem := U_ADMVP01( 2, { nlQuant, clProduto, clAlmoxTrf, clAlmoxTrf, clEndDe, clEndAte, "TRANSFERENCIA ENTRE COTAS" } )
				
				if ( len( alItem ) > 0 )
				
					clDoc := getSxENum( "SD3", "D3_DOC", 1 )
					AADD( alAuto,{ clDoc, dDataBase } )
					AADD( alAuto, alItem )
					
					U_STNXLOG( 2 , { "ADMVA01", "P00", npRecno, "Processando transfer�ncia entre endere�os" }, @cpErro )
					
					begin transaction
					
						oSelf:incRegua2( "Executando rotina automatica de transfer�ncia entre endere�os" )
						MSExecAuto({|x,y| MATA261(x,y) }, alAuto, 3 )				
					
						if ( lMsErroAuto )
							
							conout("Falha no documento [" + clDoc + "] de transfer�ncia m�ltipla.")
							clArqErro  := NomeAutoLog()
							if ( .not. ( valType(clArqErro) == "U" ) .and. .not. empty( clArqErro ) )
								clMsgErro := memoRead( clArqErro )
								conout( clMsgErro )
								memoWrite(clArqErro," ") // Limpa log automatico
							endif
							
							rollbackSx8()
							disarmtransaction()
							
						else
							confirmSx8()
							conout("Incluido [" + clDoc + "] com sucesso!")			
						endif
						
					end transaction
					
					if ( .not. empty( clMsgErro ) )
						clMsg := "Falha no documento [" + clDoc + "] de transfer�ncia entre endere�os." + CRLF + CRLF + clMsgErro
						U_STNXLOG( 2 , { "ADMVA01", "P00", npRecno, clMsg }, @cpErro )
						EECVIEW( clMsg, "LOG DE ERRO" )
					endif
						
				endif
				
			endif
			
			U_STNXLOG( 3 , { "ADMVA01", "P00", npRecno, "Fim do processamento de transfer�ncia" }, @cpErro )
			
		else
			llProcOk := .F.
			clMsg := "ID [" + clIdOri + "] n�o encontrado!"
			U_ADMVXFUN( 1, {"P00_ID", clMsg } )
		endif
		
	else
		llProcOk := .F.
	endif
	
	restArea( alArea )

Return

/*/{Protheus.doc} vldEstoqTrf
Responsavel por validar estoque referente a quantidade a ser transferida
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Existe saldo para transferencia / .F. = Nao existe saldo para transferencia 
@param clAlmoxTrf, characters, Codigo do almoxarifado
@param clEndereco, characters, Codigo do endere�o
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldEstoqTrf( clAlmoxTrf, clEndereco, clProduto, nlQuant )
	
	local alArea	:= getArea()
	local llProcOk	:= .T.
	local nlTamBFLoc:= tamSX3("BF_LOCAL")[1]
	local nlTamBFEnd:= tamSX3("BF_LOCALIZ")[1]
	local nlTamBFPrd:= tamSX3("BF_PRODUTO")[1]
	local clMsg		:= ""
	local llLocaliz	:= .F.
	local nlQtDisp	:= 0
	
	U_STNXLOG( 2 , { "ADMVA01", "P00", npRecno, "Validando saldo no endereco [" + clEndereco + "]" }, @cpErro )
	
	// Checa se o produto controla endereco  
	llLocaliz := U_ADMVXFUN( 8, clProduto )
	
	if ( llLocaliz )
	
		// Saldo disponivel no endereco
		nlQtDisp := U_ADMVP02( 2, {clAlmoxTrf, clEndereco, clProduto} )
	
		if ( nlQtDisp > 0 )
		
			if ( nlQtDisp < nlQuant )
				llProcOk	:= .F.
				clMsg		:= "Saldo do produto [" + clProduto + "] no endere�o [" + clEndereco + "], � insuficiente para a transfer�ncia. Saldo dispon�vel: " + allTrim( transform( nlQtDisp, pesqPict( "SBF", "BF_QUANT" ) ) )
			endif
		
		else
			llProcOk	:= .F.
			clMsg		:= "N�o foi encontrado saldo do produto [" + clProduto + "] no endere�o [" + clEndereco + "] !"
		endif
		
	else
		clMsg := "Produto [" + clProduto + "] n�o controla endere�o."
	endif
	
	if ( .not. empty( clMsg ) )
		U_STNXLOG( 2 , { "ADMVA01", "P00", npRecno, clMsg }, @cpErro )
		U_ADMVXFUN( 1, {"P00_ID", clMsg } )
	else
		U_STNXLOG( 2 , { "ADMVA01", "P00", npRecno, "Estoque OK!" }, @cpErro )
	endif
	
	restArea( alArea )
	
Return llProcOk

/*/{Protheus.doc} vldParam01
Valida parametros de processamento de transferecia entre cotas
@author DS2U (SDA)
@since 27/05/2018
@version 1.0
@return llRet, .T. = Parametros validados / .F. = parametros invalidos
@param clIdOri, characters, Codigo do ID origem do cadastro de cotas
@param clIdDest, characters, Codigo do ID destino do cadastro de cotas
@param clProduto, characters, Codigo do Produto
@param nlQuant, numeric, Quantidade a ser transferida
@type function
/*/
Static Function vldParam01(clIdOri, clIdDest, clProduto, nlQuant)

	local llRet	:= .T.

	if ( empty( clIdOri ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"P00_ID", "ID origem deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clIdDest ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"P00_ID", "ID destino deve ser preenchido" } )
	endif
	
	if ( llRet .and. empty( clProduto ) )
		llRet := .F.
		U_ADMVXFUN( 1, {"P00_ID", "Produto deve ser preenchido" } )
	endif
	
	if ( llRet .and. nlQuant <= 0 )
		llRet := .F.
		U_ADMVXFUN( 1, {"P00_ID", "Quantidade deve ser positiva e maior que zero!" } )
	endif

Return llRet

/*/{Protheus.doc} showLogReg
Responsavel por chamar a interface de controle de log do registro posicionado na P00
@author DS2U (SDA)
@since 30/05/2018
@version 1.0
@type function
/*/
Static Function showLogReg()

	local alArea	:= getArea()
	local clErro	:= ""

	// Abre monitor de log especifico da Sestini conforme o recno e entidade posicionado
	U_STNXLOG( 9, { "P00" , P00->( recno() )  } , @clErro )

	restArea( alArea )

Return