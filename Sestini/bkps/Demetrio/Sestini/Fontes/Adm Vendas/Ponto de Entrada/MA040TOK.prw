#INCLUDE "PROTHEUS.CH"

/*/{Protheus.doc} MA040TOK
Ponto de entrada na inclus�o e altera��o de vendedores
@author DS2U (SDA)
@since 07/05/2018
@version 1.0
@return llRet, Retorna se o cadastro esta validado e pode ser incluido/alterado na base de dados. 

@type function
/*/
User Function MA040TOK()

	local alArea	:= getArea()
	local llRet		:= .T.
	
	// Valida preenchimento de ID de aloca��o
	llRet := U_ADMVXFUN(6, {M->A3_COD, M->A3_XIDALOC } )
	
	restArea( alArea )

Return llRet