#include 'protheus.ch'
#include 'parmtype.ch'


/*/{Protheus.doc} MA410MNU
Ponto de entrada disparado antes da abertura do Browse, caso Browse inicial da rotina esteja habilitado, ou antes da apresenta��o do Menu de op��es, caso Browse inicial esteja desabilitado.  
Para habilitar ou desabilitar o Browse, entre na rotina, clique em Configura��es/Browse Inicial e selecione
a op��o desejada:
Sim - Habilitar Browse Inicial
N�o - Desabilitar Browse Inicial 
Este ponto de entrada pode ser utilizado para inserir novas op��es no array aRotina.

@author Demetrio Fontes De Los Rios 	
@since 30/05/0218
@version undefined

@type function
/*/
User Function MA410MNU() 

	// ------------------------------------------------------------------
	// Add bot�o da funcionalidade do Pedido M�e
	aAdd(aRotina,{ "Pedido M�e" ,"U_ADMVP04",0,3,0 ,NIL} )		//"Pedido M�e" 

Return