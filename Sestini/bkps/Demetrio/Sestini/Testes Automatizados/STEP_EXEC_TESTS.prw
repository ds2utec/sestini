#INCLUDE "PROTHEUS.CH"

User function TESTING()

	local olTests	:= testLess():new()
	
	// Adiciona roteiro de testes a serem realizados
//	olTests:addSteps( "U_TMATA261", {"003000-03      ", "01", "011.1          ", "01", "011            ", 100,, "TESTLESS - DISTR. COTAS"}, .T. /*llExecute*/ )
	olTests:addSteps( "U_TSTDISTL", {"","","","000000009","PRE","000001","01","N"}, .F. /*llExecute*/ )
	olTests:addSteps( "U_TSTDISTC", "01", .T. /*llExecute*/ ) // TESTE AUTOMATIZADO PARA RODAR A DISTRIBUICAO DE COTAS
	
	// Realiza as chamadas e capturas de logs
	olTests:testCalls()
	
	// Imprime logs
	olTests:showLogs()
	
Return