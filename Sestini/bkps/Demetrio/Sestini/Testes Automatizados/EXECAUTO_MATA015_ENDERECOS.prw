#INCLUDE "RWMAKE.CH" 
#INCLUDE "TBICONN.CH"

/*/{Protheus.doc} tMata015
Funcao criada para teste automatizado de criacao de enderecos
@author DS2U (SDA)
@since 27/04/2018
@version 1.0

@type function
/*/
User Function tMata015()

	local alVetor	:= {}
	local nlOpc		:= 0
	
	private lMsErroAuto := .F.  
	
	alVetor := 	{{"BE_LOCAL"  	,PARAMIXB[1]	,Nil},;				
				{"BE_LOCALIZ"	,PARAMIXB[2]	,NIL},;				
				{"BE_PRIOR"		,PARAMIXB[3]	,NIL},;				
				{"BE_STATUS"	,PARAMIXB[4]	,NIL} }	
				
	nlOpc := PARAMIXB[5]	// inclusao			
	MSExecAuto({|x,y| MATA015(x,y)},alVetor, nlOpc)     
	
Return