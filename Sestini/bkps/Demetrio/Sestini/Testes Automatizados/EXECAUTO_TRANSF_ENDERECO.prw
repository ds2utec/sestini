#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICONN.CH"

User Function TMATA261()

	local alArea	:= getArea()
	local clProduto	:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 0 .and. valType( PARAMIXB[1] ) == "C", PARAMIXB[1], "" )
	local clLocalOri:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 1 .and. valType( PARAMIXB[2] ) == "C", PARAMIXB[2], "" )
	local clEndOri	:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 2 .and. valType( PARAMIXB[3] ) == "C", PARAMIXB[3], "" )
	local clLocalDes:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 3 .and. valType( PARAMIXB[4] ) == "C", PARAMIXB[4], "" )
	local clEndDes	:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 4 .and. valType( PARAMIXB[5] ) == "C", PARAMIXB[5], "" )
	local nlQtTransf:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 5 .and. valType( PARAMIXB[6] ) == "N", PARAMIXB[6], 0 )
	local nlOpcAuto	:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 6 .and. valType( PARAMIXB[7] ) == "N", PARAMIXB[7], 3 )
	local clObsrv	:= iif( valType( PARAMIXB ) == "A" .and. len( PARAMIXB ) > 7 .and. valType( PARAMIXB[8] ) == "C", PARAMIXB[8], "" )
	local clDoc		:= ""
	local alItem	:= {}
	local alAuto	:= {}
	
	private lMsHelpAuto := .T.
	private lMsErroAuto := .F.
	
	if ( nlQtTransf > 0 )
	
		dbSelectArea( "SB1" )
		SB1->( dbSetOrder( 1 ) )
		
		if ( SB1->( dbSeek( FwxFilial( "SB1" ) + clProduto ) ) )
		
			clDoc := getSxENum( "SD3", "D3_DOC", 1 )
			
			// Valor padrao para armazem origem
			if ( empty( clLocalOri ) )
				clLocalOri := SB1->B1_LOCPAD
			endif
			
			// Valor padrao para armazem destino
			if ( empty( clLocalDes ) )
				clLocalDes := clLocalOri
			endif
			
			if ( empty( nlOpcAuto ) .or. .not. ( valtype( nlOpcAuto ) == "N" ) )
				nlOpcAuto := 3
			endif
			
			begin transaction   		
			
				AADD( alAuto,{ clDoc, dDataBase } )  //Cabecalho		
				
				//Itens a Incluir				
				AADD( alItem, SB1->B1_COD )		// D3_COD		
				AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
				AADD( alItem, SB1->B1_UM )		// D3_UM		
				AADD( alItem, clLocalOri )		// D3_LOCAL		
				AADD( alItem, clEndOri  )		// D3_LOCALIZ
	
				// Destino
				AADD( alItem, SB1->B1_COD )		// D3_COD		
				AADD( alItem, SB1->B1_DESC )	// D3_DESCRI				
				AADD( alItem, SB1->B1_UM )		// D3_UM	
				AADD( alItem, clLocalDes )		// D3_LOCAL		
				AADD( alItem, clEndDes )		// D3_LOCALIZ		
				AADD( alItem, "")				// D3_NUMSERI		
				AADD( alItem, "")				// D3_LOTECTL  		
				AADD( alItem, "")				// D3_NUMLOTE		
				AADD( alItem, cToD("//") )		// D3_DTVALID		
				AADD( alItem, 0 )				// D3_POTENCI		
				AADD( alItem, nlQtTransf )		// D3_QUANT		
				AADD( alItem, 0 )				// D3_QTSEGUM		
				AADD( alItem, "" )				// D3_ESTORNO		
				AADD( alItem, "" )				// D3_NUMSEQ 		
				AADD( alItem, "" )				// D3_LOTECTL		
				AADD( alItem, cToD("//") )		// D3_DTVALID		
				AADD( alItem, "" )				// D3_ITEMGRD
				AADD( alItem, "" )				// D3_IDDCF
				AADD( alItem, clObsrv )			// D3_OBSERVA						
				AADD( alAuto, alItem )								
				
				MSExecAuto({|x,y| MATA261(x,y) }, alAuto, nlOpcAuto )				
				
				if ( .not. lMsErroAuto )			
					ConOut("Incluido [" + clDoc + "] com sucesso!")			
				endif		
			
				ConOut("Fim: " + Time() )  	
					
			end transaction
		
		else
			conout( "Produto [" + clProduto + "] n�o encontrado no sistema" )
		endif
		
	else
		conout( "Quantida a ser transferida deve ser positivo e maioir que zero!" )
	endif
	
	restArea( alArea )

Return
/*
User Function MyMata261()

	Local cProd	:= "PA001"
	Local cUM	:= ""
	Local cLocal:= ""
	Local cDoc	:= ""
	Local cLote	:= ""
	Local dDatVal	:= ""  
	Local nQuant	:= 0
	Local lOk	:= .T.
	Local alItem	:= {}
	Local nOpcAuto:= 3 // Indica qual tipo de a��o ser� tomada (Inclus�o/Exclus�o)
	
	PRIVATE lMsHelpAuto := .T.
	PRIVATE lMsErroAuto := .F.
	
	//��������������������������������������������������������������Ŀ
	//| Abertura do ambiente                                         |
	//����������������������������������������������������������������
	PREPARE ENVIRONMENT EMPRESA "YY" FILIAL "01" MODULO "EST"   
	
	DbSelectArea("SB1")
	DbSetOrder(1)
	
	If !SB1->(MsSeek(xFilial("SB1")+cProd))	
		lOk := .F.	
		ConOut(OemToAnsi("Cadastrar produto: " + cProd))
	Else		
		cProd 	:= B1_COD 	
		cDescri	:= B1_DESC        
		cUM 	:= B1_UM        
		cLocal	:= B1_LOCPAD
	EndIf                      
	
	DbSelectArea("SD5")
	DbSetOrder(1)
	If !SD5->(MsSeek(xFilial("SD5")+ "      " + cProd ))	
		lOk := .F.	ConOut(OemToAnsi("Cadastrar lote: " + cLote ))
	Else	        
		cLote 	:= D5_LOTECTL        
		dDataVl	:= D5_DTVALID        
		nQuant	:= D5_QUANT
	EndIf
		
	If lOk		
		cDoc	:= GetSxENum("SD3","D3_DOC",1)		
		ConOut(Repl("-",80))	
		ConOut(PadC("Teste de Transf. Mod2",80))	
		ConOut("Inicio: "+Time())		
		
		//��������������������������������������������������������������Ŀ	
		//| Teste de Inclusao                                            |	
		//����������������������������������������������������������������   		
		
		Begin Transaction   		
		
			//Cabecalho a Incluir			
			alAuto := {}		
			AADD(alAuto,{cDoc,dDataBase})  //Cabecalho		
			
			//Itens a Incluir				
			AADD(alItem,cProd)	//D3_COD		
			AADD(alItem,cDescri)	//D3_DESCRI				
			AADD(alItem,cUM)		//D3_UM		
			AADD(alItem,cLocal)	//D3_LOCAL		
			AADD(alItem,"")		//D3_LOCALIZ

			// Destino
			AADD(alItem,cProd)	//D3_COD		
			AADD(alItem,cDescri)	//D3_DESCRI				
			AADD(alItem,cUM)		//D3_UM	
			AADD(alItem,"02")	//D3_LOCAL		
			AADD(alItem,"")		//D3_LOCALIZ		
			AADD(alItem,"")		//D3_NUMSERI		
			AADD(alItem,cLote)	//D3_LOTECTL  		
			AADD(alItem,"")		//D3_NUMLOTE		
			AADD(alItem,dDataVl)	//D3_DTVALID		
			AADD(alItem,0)		//D3_POTENCI		
			AADD(alItem,nQuant)	//D3_QUANT		
			AADD(alItem,0)		//D3_QTSEGUM		
			AADD(alItem,"")		//D3_ESTORNO		
			AADD(alItem,"")		//D3_NUMSEQ 		
			AADD(alItem,cLote)	//D3_LOTECTL		
			AADD(alItem,dDataVl)	//D3_DTVALID		
			AADD(alItem,"")		//D3_ITEMGRD						
			AADD(alAuto,alItem)								
			
			MSExecAuto({|x,y| mata261(x,y)},alAuto,nOpcAuto)				
			
			If !lMsErroAuto			
				ConOut("Incluido com sucesso! " + cDoc)			
			Else			
				ConOut("Erro na inclusao!")			
				MostraErro()		
			EndIf		
		
			ConOut("Fim  : "+Time())  	
				
		End Transaction
		
	EndIf
	
	RESET ENVIRONMENT
	
Return Nil

//Exemplo Estorno: 
User Function Tmata261()

	Local alAuto := {}
	Local cDoc	:= "000113001"
	Local cProd	:= "TESTE          "
	
	Private lMsErroAuto := .F.                                          
	
	RpcSetEnv("99","01",,,,,,,,,)
	
	DbSelectArea("SD3")
	DbSetOrder(2)
	DbSeek(xFilial("SD3")+cDoc+cProd)
	
	alAuto := {}					
	
	MSExecAuto({|x,y| mata261(x,y)},alAuto,6)
	
	If !lMsErroAuto	
		ALERT("Incluido com sucesso! ")	
		ALERT(CVALTOCHAR(LMSERROAUTO))	
	Else	
		ALERT("Erro na inclusao!")  	
		MostraErro()
	EndIf
	
Return
*/