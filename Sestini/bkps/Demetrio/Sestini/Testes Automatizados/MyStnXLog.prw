#include 'protheus.ch'
#include 'parmtype.ch'



/*/{Protheus.doc} MyStnXLog
//Fonte para testes da rotina de Log 
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
user function MyStnXLog()

Local nTipo			:= 0 
Local cNomeProc		:= "TESTE"
Local cEntidade 	:= "SC5"
Local nRec 			:= 4
Local cTpMsg		:= ""
Local cIdNumProc	:= "" 
Local cMsg 			:= "Teste de inicio" 
Local cErro			:= "" 
Local aParam		:= {} 

Local uRet 			:= NIL 


/* ------------------------------------------------------------------------
		Possibilidades do nTipo 
---------------------------------
		1 - Abertura - inicio 
		2 - Processamentos, (//Alertas, Cancelamentos...)
		3 - Final do log. 
		4 - Retorna os Ids 
		9 - Monitor
------------------------------------------------------------------------ */



// ------------------------------------------
// Chama Monitor de Log. 
nTipo 	:= 9 
//aParam 	:= { cEntidade , "nRec"  } 
//u_StnXLog( nTipo , aParam , @cErro )


cErro := "" 
aParam 	:= { cEntidade , nRec  } 
u_StnXLog( nTipo , aParam , @cErro )


Return 

// -----------------------------------------
// Chamada para iniciar - nTipo = 1 
nTipo := 1 
aParam := { cNomeProc, cEntidade, nRec, cMsg } 

uRet := u_StnXLog( nTipo , aParam , @cErro )
If uRet == "ERRO"
	////Alert("ERRO " + cErro )
Else  
	cIdNumProc := uRet
	////Alert("Funcao Ok " + cIdNumProc ) 

	// -----------------------------------------
	// Chamada para iniciar - nTipo = 2
	nTipo := 2 
	
	// Processamento 
	aParam := { cIdNumProc, cNomeProc , "01" /*cTpMsg*/, cEntidade, nRec, "Mensagem de teste de Processamento" /*cMsg*/ } 
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		////Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		////Alert("Funcao Processamento - Ok " + cIdNumProc ) 
	Endif 
	
	// Aviso 
	aParam := { cIdNumProc, cNomeProc , "02" /*cTpMsg*/, cEntidade, nRec, "Mensagem de teste de Aviso" /*cMsg*/ } 
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		//Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		//Alert("Funcao Aviso - Ok " + cIdNumProc ) 
	Endif 
	
	// Cancelamento ou Exclusao  
	aParam := { cIdNumProc, cNomeProc , "03" /*cTpMsg*/, cEntidade, nRec, "Mensagem de teste de Cancelamento ou Exclus�o" /*cMsg*/ } 
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		//Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		//Alert("Funcao Exclusao - Ok " + cIdNumProc ) 
	Endif 
	
	
	// Erro de Processamento   
	aParam := { cIdNumProc, cNomeProc , "98" /*cTpMsg*/, cEntidade, nRec, "Mensagem de teste de erro de Processamento" /*cMsg*/ } 
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		//Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		//Alert("Funcao Erro Proc - Ok " + cIdNumProc ) 
	Endif 
	
	// Simula Erro 
	aParam := { cIdNumProc, "98" /*cTpMsg*/, cEntidade, nRec } 
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		//Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		//Alert("Funcao Erro Proc - Ok " + cIdNumProc ) 
	Endif 
	
	// -----------------------------------------
	// Chamada para iniciar - nTipo = 3 
	
	nTipo := 3 
	cErro := ""
	
	aParam := { cIdNumProc, "Mensagem de teste de Finaliza��o" }
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	If uRet == "ERRO"
		//Alert("ERRO " + cErro )
	Else 
		cIdNumProc := uRet
		//Alert("Funcao Final do processamento - Ok " + cIdNumProc ) 
	Endif 
	
	
	// -----------------------------------------
	// Chamada para iniciar - nTipo = 4 - Retorna os Ids 
	
	nTipo 	:= 4 
	cErro 	:= ""
	aParam 	:= { cEntidade , nRec  } 
	
	uRet := u_StnXLog( nTipo , aParam , @cErro )
	
	
		varinfo("uRet: ", uRet )
		//Alert("Funcao Final do processamento - Ok " ) 

Endif 



	
return