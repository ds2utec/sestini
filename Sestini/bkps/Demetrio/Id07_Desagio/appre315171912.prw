#include 'protheus.ch'
#include 'parmtype.ch'



/*/{Protheus.doc} ADMVP07
//Rotina de gra��o do Processo de Des�gio 
@author DemetrioDeLosRios (dema)
@since 16/07/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
user function ADMVP07()

Local aPerg		:= {}										// Array para perguntes do ParamBox 
Local cTitulo  	:= "Rotina de Gera��o - Processo Des�gio"	// Descricao do Parambox 
Local aRet 		:= {}										// Array para retorno do Parambox 
Local bOk		:= {|| FPreview() }

aAdd(aPerg, {1,"Dt. De?", cToD("//")  , /*cPictPar*/ , /*cValidPar*/ , /*ConsultaF3*/ , /*cWhen*/ , 50/*TamGet*/ ,  .T. /*lObrigat*/  } )
aAdd(aPerg, {1,"Dt. Ate?", cToD("//")  , /*cPictPar*/ , /*cValidPar*/ , /*ConsultaF3*/ , /*cWhen*/ , 50/*TamGet*/ , .T. /*lObrigat*/  } )
aAdd(aPerg, {1,"TES Lic.?", Space(3) , /*cPictPar*/ , /*cValidPar*/ , "SF4" , /*cWhen*/ , 30/*TamGet*/ , .T. /*lObrigat*/  } )


// --------------------------------------------
// Chama perguntas de periodo 
If ParamBox(aPerg,cTitulo,@aRet, bOk )

EndIf 

// Tabela ZC7



	
return Nil 





/*/{Protheus.doc} FPreview
//Rotina que gera um browse / Preview com todas as informa��es que ser�o geradas 
@author DemetrioDeLosRios (dema)
@since 16/07/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FPreview()

Local lContinua := .T. 

// ----------------------------------------------------
// Valida��es para abertura da Tela ou n�o 
// *********************************************************

MsgRun("Gerando Fila de Pedidos de Venda...","Aguarde...",{|| Sleep(9000) })


if lContinua 


	
	

endif 





Return Nil 


/*
[1] Tipo do par�metro  (num�rico)
 //primeiro elemento nao se altera, � sempre o tipo
 //os demais se alteram conforme os tipos abaixo:
 1 - MsGet
  [2] : Descri��o
  [3] : String contendo o inicializador do campo
  [4] : String contendo a Picture do campo
  [5] : String contendo a valida��o
  [6] : Consulta F3
  [7] : String contendo a valida��o When
  [8] : Tamanho do MsGet
  [9] : Flag .T./.F. Par�metro Obrigat�rio ?

 2 - Combo
  [2] : Descri��o
  [3] : Num�rico contendo a op��o inicial do combo
  [4] : Array contendo as op��es do Combo
  [5] : Tamanho do Combo
  [6] : Valida��o
  [7] : Flag .T./.F. Par�metro Obrigat�rio ?
  
 3 - Radio
  [2] : Descri��o
  [3] : Num�rico contendo a op��o inicial do Radio
  [4] : Array contendo as op��es do Radio
  [5] : Tamanho do Radio
  [6] : Valida��o
  [7] : Flag .T./.F. Par�metro Obrigat�rio ?
  [8] : String contendo a valida��o When
  
 4 - CheckBox ( Com Say )
  [2] : Descri��o
  [3] : Indicador L�gico contendo o inicial do Check
  [4] : Texto do CheckBox
  [5] : Tamanho do Radio
  [6] : Valida��o
  [7] : Flag .T./.F. Par�metro Obrigat�rio ?

 5 - CheckBox ( linha inteira )
  [2] : Descri��o
  [3] : Indicador L�gico contendo o inicial do Check
  [4] : Tamanho do Radio
  [5] : Valida��o
  [6] : Flag .T./.F. Indica se campo � edit�vel ou n�o

 6 - File
  [2] : Descri��o
  [3] : String contendo o inicializador do campo
  [4] : String contendo a Picture do campo
  [5] : String contendo a valida��o
  [6] : String contendo a valida��o When
  [7] : Tamanho do MsGet
  [8] : Flag .T./.F. Par�metro Obrigat�rio ?
  [9] : Texto contendo os tipos de arquivo Ex.: "Arquivos .CSV |*.CSV"
  [10]: Diret�rio inicial do cGetFile
  [11]: PARAMETROS do cGETFILE
  
 7 - Montagem de express�o de filtro
  [2] : Descri��o
  [3] : Alias da tabela
  [4] : Filtro inicial
  [5] : Opcional - Clausula When Bot�o Editar Filtro
  
 8 - MsGet Password
  [2] : Descri��o
  [3] : String contendo o inicializador do campo
  [4] : String contendo a Picture do campo
  [5] : String contendo a valida��o
  [6] : Consulta F3
  [7] : String contendo a valida��o When
  [8] : Tamanho do MsGet
  [9] : Flag .T./.F. Par�metro Obrigat�rio ?
  
 9 - MsGet Say
  [2] : String Contendo o Texto a ser apresentado
  [3] : Tamanho da String
  [4] : Altura da String
  [5] : Negrito (l�gico)

 10- Range
  [2] : Descri��o
  [3] : Range Inicial
  [4] : ConsultaF3
  [5] : Largo em pixels do Get
  [6] : Tipo
  [7] : Tamanho do campo (em chars)
  [8] : String contendo a valida��o When

11 - MultiGet (Memo)
  [2] : Descri��o
  [3] : Inicializador padrao
  [4] : String contendo a valida��o
  [5] : String contendo a valida��o When
  [6] : Flag .T./.F. Par�metro Obrigat�rio ?

 12 - Filtro de usu�rio por Rotina
  [2] : Titulo do filtro
  [3] : Alias da tabela aonde ser� aplicado o filtro
  [4] : Filtro inicial
  [5] : Valida��o When
  */
