#include 'protheus.ch'
#include 'parmtype.ch'
#INCLUDE "tbiconn.CH"
#INCLUDE "MSMGADD.CH"
#INCLUDE "TCBROWSE.CH"
#INCLUDE 'FWBROWSE.CH'

/* 
Possiveis cTpMsg 
'00' - In�cio do Processo
'01' - Processamento 
'02' - Aviso 
'03' - Cancelamento ou exclus�o 
'98' - Erro Processamento 
'99' - Fim de Processamento 
*/

/*/{Protheus.doc} StnXLog
//Fucnao responsavel pelos Logs / Tracker do Sistema
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}
@param nOpc, numeric, descricao
				1 - Abertura - inicio 
				2 - Processamentos, Alertas, Cancelamentos...
				3 - Final do log. 
				4 - Retorna todos os Ids 
				9 - Monitor
@param uParam, undefined, descricao
@type function
/*/
user function StnXLog(nOpc,uParam,cErro)

Local uRet 		:= NIL 
Local aArea		:= GetArea() 

Default nOpc 	:= 0 
Default uParam	:= NIL 

SaveInter() // Guarda variaveis de publicas de memoria 

// ------------------------------------------------------------------
// Para chamada da funcao de inicio do Tracker.Log
// ------------------------------------------------------------------
If nOpc == 1								 // 1 - Abertura - inicio 
	
	uRet := StnIniLog(@uParam,@cErro)

// ------------------------------------------------------------------
// Para chamada da funcao de processamento do Tracker.Log
// ------------------------------------------------------------------
Elseif nOpc == 2 							// 2 - Processamentos, Alertas, Cancelamentos...

	uRet := StnProcLog(@uParam,@cErro)

// ------------------------------------------------------------------
// Para chamada da funcao de finalizacao do Tracker.Log
// ------------------------------------------------------------------
ElseIf nOpc == 3							// 3 - Final do log. 

	uRet := StnFimLog(@uParam,@cErro)


// ------------------------------------------------------------------
// Para chamada da funcao que retorna todos os Ids relacionados a entidade e Recno 
// ------------------------------------------------------------------
ElseIf nOpc == 4							// 4 - Retorna Ids Ref. a entidade + Recnos 

	uRet := StnRetIds(@uParam,@cErro)

// ------------------------------------------------------------------
// Para chamada da funcao Visual do Monitor do Tracker.Log
// ------------------------------------------------------------------
ElseIf nOpc == 9							// 9 - Monitor 

	uRet := StnShowLog(@uParam,@cErro)
	
EndIf


RestInter()
RestArea(aArea)

return uRet 



/*/{Protheus.doc} StnFimLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function StnShowLog(aParam,cErro)

Local cEntidade 	:= ""		 
Local nRec 			:= 0 
Local lContinue		:= .T. 
Local aDados		:= {} 

// Verifica Tamanho 
if Len(aParam)>1 

	// --------
	// Valida conteudo do parametro 1 - cIdNumProc 
	if lContinue .and. (ValType( aParam[1] ) == "C") 
		cEntidade := aParam[1] 
	else 
		lContinue := .F. 
		cErro := "Variavel ref. a Entidade - cEntidade - N�o est� conforme defini��o"
	endIf 
	
	// --------
	// Valida conteudo do parametro 2 - nRec   
	if lContinue .and. Empty(cErro) 
		if  ValType( aParam[2] ) == "N" 
			nRec := aParam[2]
		else 
			lContinue := .F.
			cErro := "Variavel ref. ao Recno - nRec - N�o est� conforme defini��o"
		endIf 
	endIf 
	
	
	// Executa 
	if lContinue 
		
		cErro := "" 
		aDadosLog := u_StnXLog( 4 , aParam , @cErro )
		
		if Len(aDadosLog) > 0 
			
			FMontaTela(aDadosLog,aParam)
			
		else 
			
			Aviso("Aten��o","N�o h� registros na tabela de Log para a entidade e registro solicitado.", {"Fechar"} , 3)

		endif 
		
	else 
		Aviso("Aten��o","N�o foi poss�vel montar a tela de logs" + CRLF + "Verifique mensagem de Erro @cErro " + cErro + CRLF , {"Fechar"} , 3)
	endIf 
	

else 

	cErro := "Array de parametros menor do que o exigido - { cEntidade, nRec }"

endif 



Return NIL  




/*/{Protheus.doc} FMontaTela
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 21/05/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FMontaTela(aDadosLog,aParam)

Local olDlg			:= NIL 
local alSize		:= MsAdvSize()
local olLayer1 		:= FWLayer():new()
local olLayer2 		:= FWLayer():new()
local olLayer3		:= FWLayer():new()
local olPanel1		:= NIL 
local olPanel2		:= NIL 
local alObjects		:= {}
local alInfo		:= {}
local alPosObj		:= {}
Local olBtEnd		:= NIL 
Local olBtExp		:= NIL 
Local olBtEnv		:= NIL 
local nlFator		:= 1
local clStyle1		:= "QFrame{ border-style:solid; border-width:2px; border-color:#b7b7b7; background-color:#f5f5f5 }"

local clCadastro	:= "Interface de Logs - Processamentos Sestini"

private opGrd1	  	:= NIL 
private opGrd2	  	:= NIL 
private opObserv 	:= NIL 

private apDados    	:= aDadosLog
private apDetails  	:= {{"","","","","","","",""}}  // Tamanho do Array ApDetails *** ATENCAO *** se incluir novas colunas, incluir aqui tamb�m!

AAdd( alObjects, { 100 , 85, .T., .T. } )
AAdd( alObjects, { 100 , 15, .T., .T. } )
alInfo := { alSize[ 1 ], alSize[ 7 ], alSize[3]/nlFator, alSize[4]/nlFator, 3, 3 }
alPosObj := MsObjSize( alInfo, alObjects,.T.)

// PRCOMP04 - Exemplo Tela. 

// --------------------------------------------
// Tela - Tamanho metade da tela corrente 
DEFINE MSDIALOG olDlg TITLE clCadastro From alSize[7],0 To alSize[6]/nlFator,alSize[5]/nlFator OF oMainWnd PIXEL

	/*		|---------------------------|
			|          oPainel 1 		| 
			| 			   | 			|
			|			   | 			|
			| C1_Win01	   | C2_Win01	|
			|			   | 			|
			|			   | 			|
			|---------------------------|
			| Painel 2 / oLayer 3       | 
			|___________________________|	*/


	// ---------------------------------------
	// Paineis
	
	olPanel1 := TPanelCss():New(alPosObj[1,1],alPosObj[1,2],nil,olDlg,nil,nil,nil,nil,nil,alPosObj[1,4],alPosObj[1,3],nil,nil)
	olPanel1:setCSS(clStyle1)
		
	oPanel2 := TPanelCss():New(alPosObj[2,1],alPosObj[2,2],nil,olDlg,nil,nil,nil,nil,nil,alPosObj[2,4],alPosObj[2,3]-180,nil,nil)
	oPanel2:setCSS(clStyle1)
	
	// ---------------------------------------
	// Layers 
	// ---------------------------------------
	olLayer1:init(olPanel1,.T.)
	olLayer1:addCollumn('Col01',20,.T.)
	olLayer1:addCollumn('Col02',80,.T.)
	
	olLayer1:addWindow('Col01','C1_Win01',"Entidade " + AllTrim(aParam[1]) + " - Registro: " + AllTrim( StrZero(aParam[2],5) )  ,100,.F.,.T.,{||  },,{||  })
	olLayer1:addWindow('Col02','C2_Win01',"Detalhes do Log "  ,60,.F.,.T.,{||  },,{||  })
	olLayer1:addWindow('Col02','C2_Win02',"Observa�oes "  ,40,.F.,.T.,{||  },,{||  })
	
	FGrid1( olLayer1:getWinPanel('Col01','C1_Win01')  )
	FGrid2( olLayer1:getWinPanel('Col02','C2_Win01')  )
	
	opGrd1:bChange := {||  FAtuaGrid("opGrd1") }
	opGrd2:bChange := {||  FAtuaGrid("opGrd2",.T.) }
	
	opObserv := tSimpleEditor():New(0, 0, olLayer1:getWinPanel('Col02','C2_Win02') , (olLayer1:getWinPanel('Col02','C2_Win02'):nwidth/2) , (olLayer1:getWinPanel('Col02','C2_Win02'):nHeight/2)    )
	opObserv:Load("") 
	
	olLayer3:init(oPanel2,.T.)
	olLayer3:addCollumn('Col01',100,.T.)
	
	olLayer3:addWindow('Col01','C1_Win01',"A��es",100,.F.,.T.,{||  },,{||  })
		
	olBtEnd := TButton():New( 002 ,alPosObj[2,4]-065 ,":: Fechar ",olLayer3:getWinPanel('Col01','C1_Win01'), {|| olDlg:End() },45,15,,,.F.,.T.,.F.,,.F.,,,.F. ) 

	// --------------------------------------
	// Carrega Browse
	FAtuaGrid("opGrd1")
	FAtuaGrid("opGrd2",.T.)
	
ACTIVATE MSDIALOG olDlg CENTERED


Return NIL 



/*/{Protheus.doc} FAddGrid
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 28/05/2018
@version 1.0
@return ${return}, ${return_description}
@param cVar, characters, descricao
@type function
/*/
Static Function FAtuaGrid(cVar,lAnalitic)

Local oObjGrid 		:= &cVar  
Local nPosDados		:= oObjGrid:nat
Local aDadosTemp	:= {} 
Local cTexto 		:= "" 

Default lAnalitic 	:= .F. 

// ----------------------
// Atualizacao do Browse 
If !lAnalitic 

	for nCont:=1 to Len(apDados[nPosDados,3])

		aAdd( aDadosTemp , { apDados[nPosDados,3,nCont,2]				,; 		// 01 - Seq
		  					 apDados[nPosDados,3,nCont,8]				,;		// 02 - Tipo 
		  					 DToC(apDados[nPosDados,3,nCont,6])			,;		// 03 - Data
		  					 apDados[nPosDados,3,nCont,7]				,;		// 04 - Hora
		  					 apDados[nPosDados,3,nCont,10]				,;  	// 05 - C�d User 
		  					 UsrRetName(apDados[nPosDados,3,nCont,10])	,;		// 06 - Nome usu�rio 
		  					 apDados[nPosDados,3,nCont,3]				,;		// 07 - Processo  
		  					 apDados[nPosDados,3,nCont,9]				})		// 08 - Mensagem  
		
	next nCont
	
	// -----------------------------------------
	// Atualiza Grid 
	apDetails 		:= AClone(aDadosTemp)
	aDadosTemp 		:= {} 
	opGrd2:SetArray(apDetails)
	opGrd2:nAt := Len(aDadosTemp)
	opGrd2:Refresh(.T.)
	
	// -----------------------------------------
	// Atualiza Memo de acordo com browse recem atualizado.
	FAtuaGrid("opGrd2",.T.)
	  
Else 

	// ------------------------------------
	// Atualizacao do detalhamento MEMO
	cTexto := "" 
	
	// -----------------------------------------------------
	// Monta Texto 
	// -----------------------------------------------------
	cTexto += ""
	cTexto += "<b> Entidade: </b>" + AllTrim(apDados[opGrd1:nat,3,nPosDados,4]) + " - " + "<b>Registro:</b>" + AllTrim(Str(apDados[opGrd1:nat,3,nPosDados,5]))  + " <br> "
	cTexto += "<b>IdLog: </b>" + AllTrim(apDados[opGrd1:nat,3,nPosDados,1]) +  " - " + "<b> Sequencia: </b>" + AllTrim(apDados[opGrd1:nat,3,nPosDados,2]) + " <br> "
	cTexto += "<b> Tipo: </b>" + FGetTipo(AllTrim(apDados[opGrd1:nat,3,nPosDados,8])) + " <br> "
	cTexto += "<b> Data / Hora: </b>" + AllTrim(DtoC(apDados[opGrd1:nat,3,nPosDados,6])) + " - " + AllTrim(apDados[opGrd1:nat,3,nPosDados,7]) + " <br> "
	cTexto += "<b> Usu�rio: </b>" + AllTrim(apDados[opGrd1:nat,3,nPosDados,10]) + " - " + AllTrim(UsrRetName(apDados[opGrd1:nat,3,nPosDados,10])) + " <br> "
	cTexto += "<font color=brown  > " + AllTrim(apDados[opGrd1:nat,3,nPosDados,9]) + "</font>"
	
	opObserv:Load(cTexto) 
	opObserv:Refresh(.T.)
	
EndIf 

Return Nil 



/*/{Protheus.doc} FGetTipo
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 28/05/2018
@version 1.0
@return ${return}, ${return_description}
@param cTipo, characters, descricao
@type function
/*/
Static Function FGetTipo(cTipo)

Local cRet := "" 

Do Case
Case cTipo == '00'
	cRet := 'In�cio do Processo'
Case cTipo == '01'
	cRet := 'Processamento'
Case cTipo == '02'
	cRet := 'Aviso'
Case cTipo == '03'
	cRet := 'Cancelamento ou exclus�o'
Case cTipo == '98'
	cRet := 'Erro Processamento '
Case cTipo == '99'
	cRet := 'Fim do Processamento'
EndCase

cRet := cTipo + " - " + cRet 
 
Return cRet 


/*/{Protheus.doc} FCriaGrd
//Funcao carrega Tree de acordo com a estrutura
@author DemetrioDeLosRios
@since 13/02/2017
@version undefined
@type function
/*/
Static Function FGrid1( oTela )

local bValidEdit	:= {|| } 

	DEFINE FWBROWSE opGrd1 DATA ARRAY EDITCELL bValidEdit ARRAY apDados NO SEEK NO CONFIG NO REPORT  Of oTela 
		ADD COLUMN oColumn DATA { || apDados[opGrd1:At(),1] } 	Title "Id. Log" 				SIZE 10  		Of opGrd1
		ADD COLUMN oColumn DATA { || apDados[opGrd1:At(),2] } 	Title "Qtd. Eventos" 			SIZE 4			Of opGrd1
	ACTIVATE FWBrowse opGrd1
	

Return NIL 



/*/{Protheus.doc} FGrid2
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 28/05/2018
@version 1.0
@return ${return}, ${return_description}
@param oTela, object, descricao
@type function
/*/
Static Function FGrid2( oTela )

local bValidEdit	:= {|| } 

	DEFINE FWBROWSE opGrd2 DATA ARRAY EDITCELL bValidEdit ARRAY apDetails  Of oTela 
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),1] } 	Title "Sequencia" 			SIZE 4  		Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),2]	} 	Title "Tipo" 				SIZE 10			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),3]	} 	Title "Data" 				SIZE 8			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),4] 	} 	Title "Hora" 				SIZE 8			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),5] 	} 	Title "C�d. Usu�rio" 		SIZE 10			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),6] 	} 	Title "Nome Usu�rio" 		SIZE 15			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),7] 	} 	Title "Processo" 			SIZE 10			Of opGrd2
		ADD COLUMN oColumn DATA { || apDetails[opGrd2:At(),8] 	} 	Title "Mensagem" 			SIZE 30			Of opGrd2
	ACTIVATE FWBrowse opGrd2

Return NIL 




/*/{Protheus.doc} StnRetIds
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}
@param aParam, array, dos parametros de processo inicial 
					cEntidade, - Entidade informado pelo usuario - "SC6"
					nRec, - Recno da entidade 
@type function
/*/
Static Function StnRetIds(aParam,cErro)

Local cEntidade := ""		 
Local nRec 		:= 0 
Local aRetIds 	:= {} 	// Array com os Ids relacionados ao Registro da entidade 

Local lContinue	:= .T. 

// Verifica Tamanho 
if Len(aParam)>1 

	// --------
	// Valida conteudo do parametro 1 - cIdNumProc 
	if lContinue .and. (ValType( aParam[1] ) == "C") 
		cEntidade := aParam[1] 
	else 
		lContinue := .F. 
		cErro := "Variavel ref. a Entidade - cEntidade - N�o est� conforme defini��o"
	endIf 
	
	// --------
	// Valida conteudo do parametro 2 - nRec   
	if lContinue .and. Empty(cErro) 
		if  ValType( aParam[2] ) == "N" 
			nRec := aParam[2]
		else 
			lContinue := .F.
			cErro := "Variavel ref. ao Recno - nRec - N�o est� conforme defini��o"
		endIf 
	endIf 
	
	
	aRetIds := FGetIds(cEntidade, nRec )
	
	// --------------------------------------------------------------
	// Trata, caso n�o haja Ids para a Entidade + Recno informado 
	If Len(aRetIds) == 0
		lContinue := .F.
		cErro := "N�o h� Ids para essa entidade e recno: " + cEntidade + " - " + AllTrim(Str(nRec))
	EndIf 
	

else 

	cErro := "Array de parametros menor do que o exigido - { cEntidade, nRec }"

endif 


Return aRetIds 



/*/{Protheus.doc} FGetIds
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 07/05/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FGetIds(cEntidade, nReg)

Local cQry 			:= "" 
Local cSqlAlias 	:= GetNextAlias() 
Local aRets			:= {}
Local cIds			:= "" 
Local aAux			:= {} 
Local cPV8Alias 	:= "PV8" 

Default cEntidade 	:= "" 
Default nReg 		:= 0 


dbSelectArea(cPV8Alias)
(cPV8Alias)->(dbSetOrder(1))
(cPV8Alias)->(dbGoTop())


cQry := "" 
cQry += " SELECT PV8.R_E_C_N_O_ AS _REC , PV8.PV8_ID AS _ID   " 
cQry += " FROM " + RetSqlName("PV8") + " PV8 "  
cQry += " WHERE PV8.PV8_FILIAL = '"  +  xFilial("PV8") + "' "
cQry += " AND PV8.D_E_L_E_T_ = ' ' " 
cQry += " AND PV8.PV8_ENTIDA = '" + cEntidade  + "' "
cQry += " AND PV8.PV8_RECNO = " + AllTrim(cValToChar(nReg)) + " "
cQry := ChangeQuery(cQry)
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQry) ,cSqlAlias)

While (cSqlAlias)->(!EOF())

	cId := (cSqlAlias)->_ID
	aAux := {} 
	
	aAdd(aRets , { cId } )							// [1,1] - Id
	
	While (cSqlAlias)->(!EOF()) .AND. (cSqlAlias)->_ID == cId 
	
		(cPV8Alias)->(dbGoTo( (cSqlAlias)->_REC ))
	
		aAdd( aAux , { 				  (cPV8Alias)->PV8_ID		,; 	// [n,1]
								 	  (cPV8Alias)->PV8_SEQ 		,;	// [n,2]
									  (cPV8Alias)->PV8_PROCES 	,;	// [n,3]
									  (cPV8Alias)->PV8_ENTIDA 	,;	// [n,4]
									  (cPV8Alias)->PV8_RECNO	,;	// [n,5] 
									  (cPV8Alias)->PV8_DATA 	,; 	// [n,6]
									  (cPV8Alias)->PV8_HORA 	,;	// [n,7]
									  (cPV8Alias)->PV8_TIPO 	,;	// [n,8] 
									  (cPV8Alias)->PV8_MSG 		,; 	// [n,9]
									  (cPV8Alias)->PV8_USER 	} ) 	// [n,10]
									
	
		(cSqlAlias)->(dbSkip())
		
	EndDo  
	
	
	aAdd( aRets[ Len(aRets) ], AllTrim(cValToChar(Len(aAux))) )
	aAdd( aRets[ Len(aRets) ], aAux )
	
	
EndDo 

(cSqlAlias)->(dbCloseArea())


Return aRets  




/*/{Protheus.doc} StnIniLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}
@param aParam, array, dos parametros de processo inicial 
				 	cNomeProc, - Nome do Processo passado pelo usuario - Exemplo: "Pedido"  
					cEntidade, - Entidade informado pelo usuario - "SC6"
					nRec, - Recno da entidade 
					cMsg, - Mensagem a ser gravada 
@type function
/*/
Static Function StnIniLog(aParam,cErro)

Local cNumProcRet	:= "ERRO" 		// Numero  do processo que ser� retornado. 
Local cTpMsg 		:= "00"			// Tipo da Mensagem - 00 - Inicio de Log 

Local cNomeProc		:= "" 		
Local cEntid 		:= ""
Local nRec			:= 0
Local cMsg 			:= ""
Local lContinue 	:= .T.  

Default aParam 		:= {}

// Verifica Tamanho 
if Len(aParam)>3 

	// --------
	// Valida conteudo do parametro 1 - cNomeProc 
	if lContinue .and. ValType( aParam[1] ) == "C" 
		cNomeProc := aParam[1]
	else 
		lContinue := .F. 
		cErro := "Variavel ref. a cNomeProc - N�o est� conforme defini��o"
	endIf 
	
	// --------
	// Valida conteudo do parametro 2 - cEntidade  
	if lContinue .and. Empty(cErro) 
		if  ValType( aParam[2] ) == "C" 
			cEntid := aParam[2]
		else 
			lContinue := .F.
			cErro := "Variavel ref. a cEntidade - N�o est� conforme defini��o"	
		endif 
	endIf 
	
	// --------
	// Valida conteudo do parametro 3 - nRec   
	if lContinue .and. Empty(cErro) 
		if  ValType( aParam[3] ) == "N" 
			nRec := aParam[3]
		else 
			lContinue := .F.
			cErro := "Variavel ref. ao Recno - nRec - N�o est� conforme defini��o"
		endIf 
	endIf 
	
	// --------
	// Valida conteudo do parametro 4 - cMsg    
	if lContinue .and. Empty(cErro)
		if ValType( aParam[4] ) == "C" 
			cMsg := aParam[4]
		else 
			lContinue := .F.
			cErro := "Variavel ref. a Mensagem- N�o est� conforme defini��o"
		endif 		
	endIf 
	
	if lContinue 
	
		// --------------------------------------------
		// Pega numera��o sequencial
		dbSelectArea("PV8")
		cIdNumProc	:= GetSX8Num("PV8","PV8_ID")	
		PV8->(ConfirmSX8())
		
		// Chama funcao de gravacao da tabela 
		lGravaPV8 := FGravaPV8(cIdNumProc,FGetSeq(cIdNumProc),cNomeProc ,cEntid,nRec,cTpMsg,cMsg)
		
		if lGravaPV8
			cNumProcRet 	:= PV8->(PV8_ID + PV8_SEQ)
		endIf 

	endIf 

else 

	cErro := "Array de parametros menor do que o exigido - { cNomeProc, cEntidade, nRec, cMsg }"

endIf 


Return cNumProcRet 


/*/{Protheus.doc} StnProcLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 07/05/2018
@version 1.0
@return ${return}, ${return_description}
@param aParam, array, parametros iniciais do processo de ProcLog 
					 	cIdNumProc, 
					 	cNomeProc
						cTpMsg, 
						cEntidade, 
						nRec, 
						cMsg   
@param cErro, characters, descricao
@type function
/*/
Static Function StnProcLog(aParam,cErro)

Local cNumProcRet	:= "ERRO"			// Numero do processo para retorno 
Local cTpMsg 		:= "01"				// Tipo da Mensagem - 01 - Processamento / 02 - Aviso / 03 - Cancelamento  
Local cIdNumProc	:= "" 				// Id num proc.
Local cNomeProc		:= "" 				// Nome do processamento
Local cEntid		:= "" 				// Entidade - Tabela 
Local nRec			:= 0				// Recno do registro a ser passado 
Local cMsg			:= "" 				// Mensagem 

Local lContinue 	:= .T. 				
Local lGravaPV8		:= .F. 				

Local aPV8Area		:= PV8->(GetArea()) // Guarda Area da PV8.

Default aParam 		:= {}

// Verifica Tamanho 
If Len(aParam)>5


	// --------
	// Valida conteudo do parametro 1 - cIdNumProc 
	if lContinue .and. ValType( aParam[1] ) == "C" 
		cIdNumProc := PadL(aParam[1],TamSX3("PV8_ID")[1],"0")
		// ----------------------------
		// Verifica se j� existe realmente registros com o ID passado. 
		dbSelectArea("PV8")
		PV8->(dbSetOrder(1)) // PV8_FILIAL + PV8_ID + PV8_SEQ 
		if !PV8->(MSSeek( xFilial("PV8") + cIdNumProc ))
			lContinue := .F. 
			cErro := "N�o existe na base de dados PV8 o ID " + AllTrim(cIdNumProc) 
		endIf 
	else 
		lContinue := .F. 
		cErro := "Variavel ref. a cIdNumProc - N�o est� conforme defini��o"
	endIf 
	
	// --------
	// Valida conteudo do parametro 2 - cNomeProc  
	if lContinue .and. Empty(cErro) 
		if ValType( aParam[2] ) == "C" 
			cNomeProc := aParam[2]
		else
			lContinue := .F.
			cErro := "Variavel ref. a cNomeProc - N�o est� conforme defini��o"
		endif 
	endif
	
	// --------
	// Valida conteudo do parametro 3 - cTpMsg   
	IF lContinue .and. Empty(cErro)
		
		if ValType( aParam[3] ) == "C" 
			cTpMsg := aParam[3]
		else
			lContinue := .F.
			cErro := "Variavel ref. a cTpMsg - N�o est� conforme defini��o"
		endif
		
	endif 
	
	// --------
	// Valida conteudo do parametro 4 - cEntidade  
	if lContinue .and. Empty(cErro) 
		if ValType( aParam[4] ) == "C" 
			cEntid := aParam[4]
		else
			lContinue := .F.
			cErro := "Variavel ref. a cEntidade - N�o est� conforme defini��o"
		endif 
	endif 
	
	// --------
	// Valida conteudo do parametro 5 - nRec   
	IF lContinue .and. Empty(cErro)
		if ValType( aParam[5] ) == "N" 
			nRec := aParam[5]
		else 
			lContinue := .F.
			cErro := "Variavel ref. ao Recno - nRec - N�o est� conforme defini��o"
		endif 
	EndIf 
	
	
	// --------
	// Valida conteudo do parametro 6 - cMsg    
	IF lContinue .and. Empty(cErro)
		if ValType( aParam[6] ) == "C" 
			cMsg := aParam[6]
		else 
			lContinue := .F.
			cErro := "Variavel ref. a cMsg - N�o est� conforme defini��o"
		endif
	EndIf 
	
	
	// Chama gravacao do processamento 
	If lContinue 
	
		lGravaPV8 := FGravaPV8(cIdNumProc,FGetSeq(cIdNumProc),cNomeProc ,cEntid,nRec,cTpMsg,cMsg)
		if lGravaPV8
			cNumProcRet 	:= PV8->(PV8_ID + PV8_SEQ)
		endIf 
	
	EndIf 

	

Else 

	cErro := "Array de parametros menor do que o exigido - { cIdNumProc, cNomeProc, cTpMsg , cEntidade, nRec, cMsg }"
	
EndIf 



PV8->(RestArea(aPV8Area))

Return cNumProcRet  

/*/{Protheus.doc} StnFimLog
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 07/05/2018
@version 1.0
@return ${return}, ${return_description}
@param aParam, array, parametros iniciais do processo de ProcLog 
					 	cIdNumProc, 
						cMsg   
@param cErro, characters, descricao
@type function
/*/
Static Function StnFimLog(aParam,cErro)

Local cNumProcRet	:= "ERRO"			// Numero do processo para retorno 
Local cTpMsg 		:= "99"				// Tipo da Mensagem - 99 - Final do processamento 

Local cIdNumProc	:= "" 				// Id num proc.
Local cNomeProc		:= "" 				// Nome do processamento
Local cEntid		:= "" 				// Entidade - Tabela 
Local nRec			:= 0				// Recno do registro a ser passado 
Local cMsg			:= "" 				// Mensagem 

Local lContinue 	:= .T. 				
Local lGravaPV8		:= .F. 				

Local aPV8Area		:= PV8->(GetArea()) // Guarda Area da PV8.

Default aParam 		:= {}

// Verifica Tamanho 
If Len(aParam)>1

	// --------
	// Valida conteudo do parametro 1 - cIdNumProc 
	if lContinue .and. ValType( aParam[1] ) == "C" 
		cIdNumProc := PadL(aParam[1],TamSX3("PV8_ID")[1],"0")
		// ----------------------------
		// Verifica se j� existe realmente registros com o ID passado. 
		dbSelectArea("PV8")
		PV8->(dbSetOrder(1)) // PV8_FILIAL + PV8_ID + PV8_SEQ 
		if !PV8->(MSSeek( xFilial("PV8") + cIdNumProc ))
		
			lContinue := .F. 
			cErro := "N�o existe na base de dados PV8 o ID " + AllTrim(cIdNumProc) 
		
		else 
		
			// Aliemtna as variaveis do processamento / ID posicionado 
			cNomeProc 	:= PV8->PV8_PROCES	
			cEntid		:= PV8->PV8_ENTIDA
			nRec		:= PV8->PV8_RECNO
	
		endIf 
	else 
		lContinue := .F. 
		cErro := "Variavel ref. a cIdNumProc - N�o est� conforme defini��o"
	endIf 
	
	// --------
	// Valida conteudo do parametro 2 - cMsg    
	if lContinue .and. Empty(cErro) .and.  ValType( aParam[2] ) == "C" 
		cMsg := aParam[2]
	else 
		lContinue := .F.
		cErro := "Variavel ref. a cMsg - N�o est� conforme defini��o"
	endif 
	
	
	// Chama gravacao do processamento 
	If lContinue 
	
		lGravaPV8 := FGravaPV8(cIdNumProc,FGetSeq(cIdNumProc),cNomeProc ,cEntid,nRec,cTpMsg,cMsg)
		if lGravaPV8
			cNumProcRet 	:= PV8->(PV8_ID + PV8_SEQ)
		endIf 
	
	EndIf 

Else 

	cErro := "Array de parametros menor do que o exigido - { cIdNumProc, cMsg }"
	
EndIf 

PV8->(RestArea(aPV8Area))

return cNumProcRet 


/*/{Protheus.doc} FGravaPV8
//TODO Descri��o auto-gerada.
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
Static Function FGravaPV8(cIdNumProc,cSeq,cNomeProc,cEntid,nRec,cTipo,cMsg)

Local lRet := .F. 

Default cIdNumProc 	:= "" 
Default cSeq 		:= "" 
Default cNomeProc	:= "" 
Default cEntid 		:= "" 
Default nRec 		:= "" 
Default cTipo 		:= "" 
Default cMsg		:= "" 

If RecLock("PV8",.T.)
	PV8->PV8_FILIAL 	:= xFilial("PV8")
	PV8->PV8_ID			:= cIdNumProc
	PV8->PV8_SEQ		:= cSeq 
	PV8->PV8_PROCES		:= cNomeProc
	PV8->PV8_ENTIDA		:= cEntid
	PV8->PV8_RECNO		:= nRec
	PV8->PV8_USER		:= AllTrim(__cUserID)
	PV8->PV8_DATA		:= dDataBase 
	PV8->PV8_HORA		:= AllTrim(Time())
	PV8->PV8_TIPO		:= cTipo	
	PV8->PV8_MSG 		:= AllTrim(cMsg)			
	PV8->(MsUnLock())
	lRet := .T. 
EndIf 

Return lRet 

/*/{Protheus.doc} FGetSeq
//Funcao para retornar a sequencia daquele ID para gravacao.
@author DemetrioDeLosRios
@since 30/04/2018
@version 1.0
@return ${return}, ${return_description}
@param cIdNumProc, characters, descricao
@type function
/*/
Static Function FGetSeq(cIdNumProc)
Local cSeqRet	:= "001" 
Local cQry 		:= "" 
Local cSqlAlias := GetNextAlias() 

cQry := "" 
cQry += " SELECT IsNull ( MAX(PV8_SEQ) , '0' ) AS _LASTSEQ  FROM " + RetSqlName("PV8") + " PV8 " 
cQry += " WHERE PV8.PV8_FILIAL = '" + xFilial("PV8") + "' " 
cQry += " AND PV8.PV8_ID = '" + cIdNumProc +  "' " 
cQry += " AND PV8.D_E_L_E_T_ = ' ' " 
dbUseArea( .T. , "TOPCONN" , TcGenQry(,,cQry) ,cSqlAlias)

cSeqRet := StrZero(Val((cSqlAlias)->_LASTSEQ) + 1 , 3) 

(cSqlAlias)->(dbCloseArea())

Return cSeqRet 