#INCLUDE 'PROTHEUS.CH'
#INCLUDE "FWBROWSE.CH"
#INCLUDE "FWMVCDEF.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TOTVS.CH"


#DEFINE _cEnter "<BR>"   


//-----------------------------------------------
/*/{Protheus.doc} ADMVA12
Rotina de historico dos movimentos de Bloqueio
e Libera��o do pedido de venda

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function ADMVA12()

	Local aCoors 		:= FWGetDialogSize(oMainWnd)
	Local cTitle		:= "Bloqueios/Libera��es Pedido de venda"

	Private oBrowse		:= NIL
	Private oDlg		:= NIL

	dbSelectArea('ZCG')
	ZCG->(dbSetOrder( 1 ))

	DEFINE MSDIALOG oDlg Title cTitle From aCoors[1], aCoors[2] To aCoors[3], aCoors[4] PIXEL

		oBrowse:= FWMBrowse():New()
		oBrowse:SetOwner(oDlg)
		oBrowse:SetDescription(cTitle)
		oBrowse:SetMenuDef('ADMVA12')
		oBrowse:SetAlias('ZCG')
		oBrowse:Alias('ZCG')
		oBrowse:ForceQuitButton()
		oBrowse:Activate()

	ACTIVATE MSDIALOG oDlg CENTER

Return

//-----------------------------------------------
/*/{Protheus.doc} ModelDef
Model cadastro de historico dos movimentos de
Bloqueio e Libera��o do pedido de venda

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ModelDef()

	Local oStruZCG	:= FWFormStruct(1,'ZCG')
	Local oModel	:= MPFormModel():New( 'ADMV12MVC',,, )

	oModel:SetDescription("Bloqueios/Libera��es Pedido de venda")

	oModel:AddFields( 'ZCGMASTER', , oStruZCG )

	oModel:SetPrimaryKey({"ZCG_PEDIDO"})

Return oModel

//-----------------------------------------------
/*/{Protheus.doc} viewdef
View cadastro de historico dos movimentos de
Bloqueio e Libera��o do pedido de venda

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function viewdef()

	Local oModel		:= FWLoadModel("ADMVA12")
	Local oView			:= FWFormView():New()
	Local oStruZCG		:= FWFormStruct(2,'ZCG' )

	oView:SetModel( oModel )

	oView:AddField('VIEW_ZCG'	,oStruZCG	,'ZCGMASTER')

	oView:CreateHorizontalBox('CABEC'	,100)
	oView:SetOwnerView('VIEW_ZCG'	,'CABEC' 	)

	oView:SetCloseOnOk({||.T.})

Return oView

//-----------------------------------------------
/*/{Protheus.doc} MenuDef
Menu da rotina de Rotina de historico dos
movimentos de Bloqueio e Libera��o do pedido de
venda

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function MenuDef()

	Local aRotina	:= {}

	ADD OPTION aRotina TITLE "Visualizar" ACTION "VIEWDEF.ADMVA12" OPERATION MODEL_OPERATION_VIEW	 ACCESS 0

Return aRotina



/*/{Protheus.doc} ADP12Grv
//TODO Descri��o auto-gerada.
@author demet
@since 27/08/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
User Function ADP12Grv( cPedido, cTipoBloq , aDados , cStatsC5 )

	Local aSC5Area			:= SC5->(GetArea())		// Guardar pedido posicionado
	Local nCont				:= 0
	Local lRet				:= .T.	
	Local aCmpStC5			:= {  {"1" , "C5_XBLQCRE"} ,  {"2" , "C5_XALCADA"}  , {"3","C5_XSTCOAD"}  ,  {"4" , "C5_XESTOQU"}  , {"5" , "C5_XBLQCRE"}  }
	Local cMotivo			:= ""
	Local cSttBkp			:= ""
	
	Default aDados 			:= {} 
	Default cPedido 		:= "" 
	Default cTipoBloq		:= 3
	 
	For nCont := 1 To Len(aDados)
		
		lRet := ADP12Set( cPedido , cTipoBloq , aDados[nCont,1]  , __cUserID , aDados[nCont,2] )
		cMotivo +=  IIF( !Empty( cMotivo ) , CRLF , "" ) + aDados[nCont,2]
	
	Next nCont 
	
	nPos := aScan( aCmpStC5 , {|x| AllTrim(x[1]) == AllTrim(cTipoBloq) }  )

	If !Empty( cStatsC5 )
	
		If nPos > 0  .AND. (SC5->(FieldPos(aCmpStC5[nPos,2]))>0)  .AND. SC5->( MsSeek(  xFilial("SC5") +  cPedido  )   )
			
			cSttBkp	:= SC5->&(aCmpStC5[nPos,2])
			
			// Grava Status na SC5
			If RecLock("SC5", !lRet )
				
				SC5->&(aCmpStC5[nPos,2]) := cStatsC5

				SC5->(MSUnLock())
			EndIf 	
		
		
			If aCmpStC5[nPos,2] == "C5_XBLQCRE" 
				
				If ( cStatsC5 $ "P|C" .And. cSttBkp $ " |A|S|L" )  .Or. cStatsC5 $ "R" 
				
					U_ADP4MlPV( cMotivo )
				
				EndIf

			EndIf
			
		EndIf 
	
	EndIf
	RestArea( aSC5Area )
	
Return Nil 


/*/{Protheus.doc} ADP12View
//TODO Descri��o auto-gerada.
@author demet
@since 27/08/2018
@version 1.0
@return ${return}, ${return_description}

@type function
/*/
User Function ADP12View() 


Local olDlg			:= NIL 
local alSize		:= MsAdvSize()
local clStyle1		:= ""//"QFrame{ border-style:solid; border-width:2px; border-color:#b7b7b7; background-color:#ffffff }"
local clCadastro	:= "Situa��o ADMV " +  DtoC(dDataBase) + " - " + AllTrim(Time()) +  " / Pedido: " + SC5->C5_NUM 

Local olPanel1		:= NIL
Local olPanel2		:= NIL
Local oFontSay		:= NIL 

Local olLayer1		:= FWLayer():new() 
Local olLayer2		:= FWLayer():new() 
Local olLayerC		:= FWLayer():new()

Local alObjects		:= {} 
Local alInfo		:= {} 
Local alPosObj		:= {} 
Local oEnchoice		:= NIL 

Local nContTp		:= 0 

Local oObsCol1		:= NIL
Local oObsCol2		:= NIL 	
Local oObsCol3		:= NIL 
Local oObsCol4		:= NIL 
Local oObsCol5 		:= NIL

Local oBmpCol1		:= NIL 
Local oBmpCol2		:= NIL 
Local oBmpCol3		:= NIL 
Local oBmpCol4		:= NIL 
Local oBmpCol5		:= NIL

Local oSayCol1		:= NIL 
Local oSayCol2		:= NIL 
Local oSayCol3		:= NIL 
Local oSayCol4		:= NIL 
Local oSayCol5		:= NIL
Local aCpos			:={"C5_NUM","C5_CLIENTE","C5_LOJACLI","C5_TIPOCLI","C5_CONDPAG","C5_TABELA","C5_EMISSAO","C5_TPFRETE","NOUSER"} 

Private cCorSts1	 	:= "BR_CINZA"						// Cor do Status Cr�dito PV 
Private cCorSts2	 	:= "BR_CINZA"						// Cor do Status Al�ada 
Private cCorSts3	 	:= "BR_CINZA"						// Cor do Status Comercial  
Private cCorSts4	 	:= "BR_CINZA"						// Cor do Status Estoque  
Private cCorSts5	 	:= "BR_CINZA"						// Cor do Status Cr�dito Cliente

Private cSaySts1		:= ""								// Mensagem Status Cr�dito PV
Private cSaySts2		:= ""								// Mensagem Status Al�ada
Private cSaySts3		:= ""								// Mensagem Status Comercial
Private cSaySts4		:= ""								// Mensagem Status Estoque
Private cSaySts5		:= ""								// Mensagem Status Cr�dito Cliente

Private cObsCol1		:= ""								// Mensagem MEMO - Observa�ao Cr�dito PV
Private cObsCol2		:= ""								// Mensagem MEMO - Observa�ao Al�ada
Private cObsCol3		:= ""								// Mensagem MEMO - Observa�ao Comercial
Private cObsCol4		:= ""								// Mensagem MEMO - Observa�ao Estoque
Private cObsCol5		:= ""								// Mensagem MEMO - Observa�ao Cr�dito Cliente

CursorWait()

AAdd( alObjects, { 100 , 25, .T., .T. } )
AAdd( alObjects, { 100 , 100, .T., .T. } )
alInfo := { alSize[ 1 ], alSize[ 7 ], alSize[3], alSize[4], 3, 3 }
alPosObj := MsObjSize( alInfo, alObjects,.T.)

// --------------------------------------------
// Tela - Tamanho metade da tela corrente 
DEFINE MSDIALOG olDlg TITLE clCadastro From alSize[7],0 To alSize[6],alSize[5] OF oMainWnd PIXEL

	DEFINE FONT oFontSay  NAME "Courier New" SIZE    7 ,   16  BOLD
	

	// ---------------------------------------
	// Paineis
	
	olPanel1 := TPanelCss():New(alPosObj[1,1],alPosObj[1,2],nil,olDlg,nil,nil,nil,nil,nil,alPosObj[1,4],alPosObj[1,3],nil,nil)
	olPanel1:setCSS(clStyle1)
		
	olPanel2 := TPanelCss():New(alPosObj[2,1],alPosObj[2,2],nil,olDlg,nil,nil,nil,nil,nil,alPosObj[2,4],alPosObj[2,3]-alPosObj[2,1],nil,nil)
	olPanel2:setCSS(clStyle1)
	
	
	olLayer1:init(olPanel1,.T.)
	olLayer1:addCollumn('Col01',90,.T.)
	olLayer1:addCollumn('Col02',10,.T.)
	
	olLayer1:addWindow('Col01','C1_Win01',"Dados Pedido de Venda"  	,100,.F.,.T.,{||  },,{||  })
	olLayer1:addWindow('Col02','C2_Win01',"A��es"				  	,100,.F.,.T.,{||  },,{||  })
	
	private aTELA[0][0]
	private aGETS[0]	

	RegToMemory( "SC5", .F., .F. )
	oEnchoice := MsmGet():New("SC5",SC5->(Recno()),2,/*aCRA*/,/*cLetras*/,/*cTexto*/,aCpos,{000,000,000,000};
									,/*aAlterEnch*/,/*nModelo*/,/*nColMens*/,/*cMensagem*/, /*cTudoOk*/,olLayer1:getWinPanel('Col01','C1_Win01'),/*lF3*/,.T.;
									,/*lColumn*/,/*caTela*/,/*lNoFolder*/,/*lProperty*/,/*aField*/,/*aFolder*/,/*lCreate*/,/*lNoMDIStretch*/,/*cTela*/)   	 
	oEnchoice:oBox:align := CONTROL_ALIGN_ALLCLIENT
	
	olLayer2:init(olPanel2,.T.)
	olLayer2:addCollumn('Col01',25,.T.)
	olLayer2:addCollumn('Col02',25,.T.)
	olLayer2:addCollumn('Col03',25,.T.)
	olLayer2:addCollumn('Col04',25,.T.)
	
	olLayerC:init( olLayer2:GetColPanel('Col01') ,.T.)
	olLayerC:AddLine( 'SUPERIOR', 50, .F. )
	olLayerC:AddCollumn( 'Col01', 100, .T., 'SUPERIOR' )
	olLayerC:addWindow('Col01','CC1_Win01',"Situa��o <<  Cr�dito PV  >>"  		,100,.F.,.T.,{||  },'SUPERIOR',{||  })		
	
	olLayerC:AddLine( 'INFERIOR', 50, .F. )
	olLayerC:AddCollumn( 'Col05', 100, .T., 'INFERIOR' )
	olLayerC:addWindow('Col05','CC5_Win01',"Obs Blq. PV  por Cr�dito Cliente"  	,100,.F.,.T.,{||  },'INFERIOR',{||  })
	
	olLayer2:addWindow('Col02','C2_Win01',"Situa��o <<  AL�ADA  >>"  		,100,.F.,.T.,{||  },,{||  })
	olLayer2:addWindow('Col03','C3_Win01',"Situa��o <<  COMERCIAL  >>"  	,100,.F.,.T.,{||  },,{||  })
	olLayer2:addWindow('Col04','C4_Win01',"Situa��o <<  ESTOQUE  >>"  		,100,.F.,.T.,{||  },,{||  })
	
	// -------------------------------------------------
	// Pega os Status - Memo da ZCG  
	For nContTp := 1 To  5 
		fGetStat( nContTp, SC5->C5_NUM,   ("cCorSts"+AllTrim(Str(nContTp)))     , ("cSaySts"+AllTrim(Str(nContTp)))    , ("cObsCol"+AllTrim(Str(nContTp)))    )
	Next nContTp  

	If !Empty(cObsCol1)
		oObsCol1 := tSimpleEditor():New(1.5, 0, olLayerC:getWinPanel('Col01','CC1_Win01', 'SUPERIOR') , (olLayerC:getWinPanel('Col01','CC1_Win01', 'SUPERIOR'):nwidth/2) , (olLayerC:getWinPanel('Col01','CC1_Win01', 'SUPERIOR'):nHeight/2.5)   )
		oObsCol1:Load( cObsCol1 ) 
	EndIf 

	If !Empty(cObsCol5)
		oObsCol5 := tSimpleEditor():New(1.5, 0, olLayerC:getWinPanel('Col05','CC5_Win01', 'INFERIOR') , (olLayerC:getWinPanel('Col05','CC5_Win01', 'INFERIOR'):nwidth/2) , (olLayerC:getWinPanel('Col05','CC5_Win01', 'INFERIOR'):nHeight/2.2)   )
		oObsCol5:Load( cObsCol5 ) 
		oObsCol5:Align := CONTROL_ALIGN_ALLCLIENT
	EndIf
		
	If !Empty(cObsCol2)
		oObsCol2 := tSimpleEditor():New(1.5, 0, olLayer2:getWinPanel('Col02','C2_Win01') , (olLayer2:getWinPanel('Col02','C2_Win01'):nwidth/2) , (olLayer2:getWinPanel('Col02','C2_Win01'):nHeight/2.2)   )
		oObsCol2:Load( cObsCol2 ) 
	EndIf
	
	If !Empty(cObsCol3)
		oObsCol3 := tSimpleEditor():New(1.5, 0, olLayer2:getWinPanel('Col03','C3_Win01') , (olLayer2:getWinPanel('Col03','C3_Win01'):nwidth/2) , (olLayer2:getWinPanel('Col03','C3_Win01'):nHeight/2.2)   )
		oObsCol3:Load( cObsCol3  ) 
	EndIf 
	
	If !Empty(cObsCol4)
		oObsCol4 := tSimpleEditor():New(1.5, 0, olLayer2:getWinPanel('Col04','C4_Win01') , (olLayer2:getWinPanel('Col04','C4_Win01'):nwidth/2) , (olLayer2:getWinPanel('Col04','C4_Win01'):nHeight/2.2)   )
		oObsCol4:Load( cObsCol4 ) 
	EndIf 

	@ 0,0 BITMAP oBmpCol1 OF olLayerC:getWinPanel('Col01','CC1_Win01', 'SUPERIOR')  SIZE 10,10 NOBORDER 
	oBmpCol1:lStretch  	:= .T.
	oBmpCol1:cResName	:= cCorSts1
		
	@ 0,0 BITMAP oBmpCol2 OF olLayer2:getWinPanel('Col02','C2_Win01')  SIZE 10,10 NOBORDER 
	oBmpCol2:lStretch  	:= .T.
	oBmpCol2:cResName	:= cCorSts2
	
	@ 0,0 BITMAP oBmpCol3 OF olLayer2:getWinPanel('Col03','C3_Win01')  SIZE 10,10 NOBORDER 
	oBmpCol3:lStretch  	:= .T.
	oBmpCol3:cResName	:= cCorSts3 //"BR_VIOLETA"
	
	@ 0,0 BITMAP oBmpCol4 OF olLayer2:getWinPanel('Col04','C4_Win01')  SIZE 10,10 NOBORDER 
	oBmpCol4:lStretch  	:= .T.
	oBmpCol4:cResName	:= cCorSts4
	
	@ 1 , 15 SAY cSaySts1 FONT oFontSay OF  olLayerC:getWinPanel('Col01','CC1_Win01', 'SUPERIOR') SIZE 200, 10 PIXEL COLOR CLR_BLACK 
	@ 1 , 15 SAY cSaySts2 FONT oFontSay OF  olLayer2:getWinPanel('Col02','C2_Win01') SIZE 200, 10 PIXEL COLOR CLR_BLACK 
	@ 1 , 15 SAY cSaySts3 FONT oFontSay OF  olLayer2:getWinPanel('Col03','C3_Win01') SIZE 200, 10 PIXEL COLOR CLR_BLACK 
	@ 1 , 15 SAY cSaySts4 FONT oFontSay OF  olLayer2:getWinPanel('Col04','C4_Win01') SIZE 200, 10 PIXEL   
	
ACTIVATE MSDIALOG olDlg CENTERED

CursorArrow()

Return NIL 




/*/{Protheus.doc} fGetStat
//TODO Descri��o auto-gerada.
@author demet
@since 03/09/2018
@version 1.0
@return ${return}, ${return_description}
@param nTpBloq, numeric, descricao
@param cNumPed, characters, descricao
@param cCorSts, characters, descricao
@param cSaySts, characters, descricao
@type function
/*/
Static Function fGetStat(nTpBloq , cNumPed, cCorSts , cSaySts, cObsCol)

Local cRet 		:= "" 
Local cTpBloq	:= AllTrim(STR(nTpBloq))


// -----------------------------------------------------------------------------------
// Alimenta Status - COR - Farol ; Legenda- Conforme cada regra de cada tipo de bloqueio.
Do Case 
	
	// --------------------------------------------------------------------
	Case nTpBloq == 1 // Credito
	
		If SC5->C5_XBLQCRE == "P" // Blq.Cred.Ped Venda

			&(cCorSts) 	:= "BR_MARROM"
			&(cSaySts)	:= "BLQ.CRED.PED VENDA"
			
		
		ElseIf SC5->C5_XBLQCRE == "C" // Blq.Cred Cliente

			&(cCorSts) 	:= "BR_PRETO"
			&(cSaySts)	:= "BLQ.CRED CLIENTE"		
		
		ElseIf SC5->C5_XBLQCRE == "A" // Aprovado Manual

			&(cCorSts) 	:= "BR_VERDE"
			&(cSaySts)	:= "APROVADO MANUAL"
					
		ElseIf SC5->C5_XBLQCRE == "R" // Reprovado

			&(cCorSts) 	:= "BR_VERMELHO"
			&(cSaySts)	:= "REPROVADO"
					
		ElseIf SC5->C5_XBLQCRE == "S" // Sem Analise

			&(cCorSts) 	:= "BR_AMARELO"
			&(cSaySts)	:= "SEM ANALISE"
					
		ElseIf SC5->C5_XBLQCRE == "L" // Liberado Sem Bloqueios

			&(cCorSts) 	:= "BR_BRANCO"
			&(cSaySts)	:= "LIBERADO SEM BLOQUEIOS"
		
		EndIf 

	// --------------------------------------------------------------------
	Case nTpBloq == 2	// Alcada		

		If SC5->C5_XALCADA == "L" // Liberado 

			&(cCorSts) 	:= "BR_VERDE"
			&(cSaySts)	:= "LIBERADO"

		ElseIf SC5->C5_XALCADA == "A" // Aprovado
			
			&(cCorSts) 	:= "BR_AZUL"
			&(cSaySts)	:= "APROVADO"

		ElseIf SC5->C5_XALCADA == "P" // Pr�-Aprovado
			
			&(cCorSts) 	:= "BR_AMARELO"
			&(cSaySts)	:= "Pr�-Aprovado"

		ElseIf SC5->C5_XALCADA == "N" // N�o Aprovado

			&(cCorSts) 	:= "BR_PRETO"
			&(cSaySts)	:= "N�o Aprovado"

		ElseIf SC5->C5_XALCADA == "R" // Reprovado
			
			&(cCorSts) 	:= "BR_VERMELHO"
			&(cSaySts)	:= "REPROVADO"
			
		EndIf


	// --------------------------------------------------------------------
	Case nTpBloq == 3 	// Comercial 
	
		If SC5->C5_XSTCOAD == "B" // Bloqueado 
			
			&(cCorSts) 	:= "BR_VERMELHO"
			&(cSaySts)	:= "BLOQUEADO"
			
		ElseIf  SC5->C5_XSTCOAD == "L" // Liberado
		
			&(cCorSts) 	:= "BR_VERDE"
			&(cSaySts)	:= "LIBERADO"
		
		ElseIf  SC5->C5_XSTCOAD == "A" // Aprovado   
		
			&(cCorSts) 	:= "BR_AZUL"
			&(cSaySts)	:= "APROVADO"
			
		Endif 
	
	
	// --------------------------------------------------------------------
	Case nTpBloq == 4 	// Estoque  
	
		If SC5->C5_XESTOQU == "A"

			&(cCorSts) 	:= "BR_AZUL"
			&(cSaySts)	:= "APROVADO"

		ElseIf SC5->C5_XESTOQU == "R"
			
			&(cCorSts) 	:= "BR_VERMELHO"
			&(cSaySts)	:= "REPROVADO"
			
		ElseIf SC5->C5_XESTOQU == "L"

			&(cCorSts) 	:= "BR_VERDE"
			&(cSaySts)	:= "LIBERADO"

		EndIf
		

EndCase 
	

cRet := U_GAD12Memo( cNumPed , cTpBloq , cSaySts )


&(cObsCol)	:= cRet 

Return 

//-----------------------------------------------
/*/{Protheus.doc} GAD12Memo
Retorna o Texto Memo, concatenando os eventos
da ZCG

@author DS2U (HFA)
@since 18/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function GAD12Memo( cNumPed , cTpBloq , cSaySts )

	Local cChave	:= ""
	Local cRet		:= ""
	
	Default cSaySts	:= "" 
	
	dbSelectArea("ZCG")
	ZCG->(dbSetOrder(1))	// ZCG_FILIAL+ZCG_PEDIDO+ZCG_TPBLOQ+ZCG_SEQ              
	ZCG->(dbGoTop())
	
	cChave := xFilial("ZCG") + cNumPed + cTpBloq
	If ZCG->( MsSeek(  cChave  ) )
	
		// -----------------------------------------------------------------------------------
		// Escreve status atual do Pedido - e Sequencias vide tabela de bloqueios. 
		If !Empty( &(cSaySts) )
			cRet := "<b>" +  "PEDIDO: " + SC5->(C5_FILIAL + C5_NUM )  + " *** " + &cSaySts + " *** " + "</b>" +  _cEnter + _cEnter
		EndIf
		
		While ZCG->(!EOF()) .AND. ( cChave == ZCG->( ZCG_FILIAL+ZCG_PEDIDO+ZCG_TPBLOQ ) )
			
			cRet += "Sequencia: " + ZCG->ZCG_SEQ  + _cEnter 
			cRet += "Tipo Movimenta��o: " + ZCG->ZCG_TPMOV  + " - " + Iif( AllTrim(ZCG->ZCG_TPMOV) == "1" , "Bloqueio" , "Libera��o" )  + _cEnter 
			cRet += "Usu�rio: " + Alltrim(ZCG->ZCG_USER)  + " - " + Alltrim(ZCG->ZCG_NOME)  + _cEnter 
			cRet += "Data - Hora: " +  AllTrim(DtoC(ZCG->ZCG_DATA)) + " - " +  AllTrim(ZCG->ZCG_HORA) + _cEnter
			cRet += "Obs.: " + AllTrim(ZCG->ZCG_OBS) + _cEnter
			cRet += "<hr>" + _cEnter 
		
			ZCG->(dbSkip())
		EndDo 
		
	Endif

Return cRet

//-----------------------------------------------
/*/{Protheus.doc} ADP12Set
Grava��o do motivo do bloqueio/libera��o
do pedido de venda por: al�ada, estoque, credito
comercial

@author DS2U (HFA)
@since 11/08/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function ADP12Set( cPedido , cTpBloq , cTpMovim , cUserMov , cObs  )

	Local oModel			:= FWLoadModel("ADMVA12")
	Local oModZCG			:= oModel:GetModel("ZCGMASTER")
	Local aCampos			:= {}
	Local cNameUsr			:= UsrRetName( cUserMov )
	Local nC				:= 0
	Local cSeq				:= StrZero(0 , TamSx3("ZCG_SEQ")[1])
	Local lRet				:= .T.
	Local cError			:= ""
	
	
	// Para garantir caso a grava��o seja chamada desposicionada do pedido de venda 
	dbSelectArea("SC5")
	SC5->(dbSetOrder(1)) 		// C5_FILIAL + C5_NUM 
	SC5->(dbGoTop())	

	// ------------------------------------------------
	// *** DEMA:  Avaliar em Trocar por SelectMAX 
	
	dbSelectArea( "ZCG" )
	ZCG->( dbSetOrder( 2 ) ) // ZCG_FILIAL+ZCG_PEDIDO+ZCG_SEQ+ZCG_TPBLOQ
	If ZCG->( dbSeek( FWxFilial("ZCG") + cPedido  ) )
		While ZCG->(!Eof()) .And. ZCG_FILIAL+ZCG_PEDIDO == FWxFilial("ZCG") + cPedido

			cSeq := ZCG->ZCG_SEQ

			ZCG->( dbSkip() )
		EndDo
	EndIf

	cSeq := Soma1( cSeq )

	aadd( aCampos , {"ZCG_PEDIDO"	, cPedido	} )
	aadd( aCampos , {"ZCG_SEQ"		, cSeq		} )
	aadd( aCampos , {"ZCG_TPBLOQ"	, cTpBloq	} )
	aadd( aCampos , {"ZCG_TPMOV"	, cTpMovim	} )
	aadd( aCampos , {"ZCG_USER"		, cUserMov	} )
	aadd( aCampos , {"ZCG_NOME"		, cNameUsr	} )
	aadd( aCampos , {"ZCG_OBS"		, cObs		} )

	oModel:SetOperation( MODEL_OPERATION_INSERT )
	If ( lRet := oModel:Activate() )

		For nC := 1 To Len(aCampos)

			lRet := oModZCG:SetValue( aCampos[ nC , 1 ] , aCampos[ nC , 2 ] )

			If !lRet
				Exit
			EndIf

		Next nC

		If lRet
			If ( lRet := oModel:VldData() )
				oModel:CommitData()
			EndIf
		EndIf

	EndIf

	If !lRet

		cError := ""
		AEval( oModel:GetErrorMessage(), { | x |  cError  += cValToChar( x ) +  IIF( !Empty( cError ), CRLF , "" ) } )

		If Empty( cError )
			cError := "N�o foi possivel gravar o motivo do bloqueio/libera��o do pedido de venda"
		EndIf

		Help(,, "ADP12Set_01",, cError, 1, 0)

	EndIf

	oModel:Destroy()
	FreeObj(oModel)
	oModel:= Nil

Return lRet 
