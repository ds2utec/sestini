#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} M410PVNF
P.E executado antes da tela de Wizar, para
prepara��o do documento de saida, verificar se
o Pedido liberado pelo ADMV para faturamento

@author DS2U (HFA)
@since 25/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function M410PVNF()

    Local aArea := GetArea()
    Local lRet	:= .T.
    
    //-------------------------------------------------------------------------------------
    // Verifica se o pedido de vendas est� liberado pelas regras ADMV para ser faturado
    //-------------------------------------------------------------------------------------
    lRet := U_ADMVXFUN( 14 )

    RestArea( aArea )

Return lRet