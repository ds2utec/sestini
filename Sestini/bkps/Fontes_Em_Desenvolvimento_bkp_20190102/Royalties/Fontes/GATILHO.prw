#INCLUDE "TOTVS.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"

//------------------------------------------------------------------->
/*/{Protheus.doc} Gatilho
@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function Gatilho()
Local nTamCols := Len(aCols)
Local cRet
cRet := U_GatoA()
U_GatoB(Nil, nTamCols)
Return cRet


//------------------------------------------------------------------->
/*/{Protheus.doc} GatilhoA
@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function GatoA()
Local cRet := Posicione("SA1",1,xFilial("SA1")+M->C5_CLIENTE+M->C5_LOJACLI,"SA1->A1_XDESAG")                      
Return cRet

//------------------------------------------------------------------->
/*/{Protheus.doc} GatilhoB
@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//------------------------------------------------------------------->

User Function GatoB(cCampo, nVezes)

Local nPerDes 		:= Posicione("SA1",1,xFilial("SA1")+M->C5_CLIENTE+M->C5_LOJACLI,"SA1->A1_XPERDES")
Local nPosQTDVen	:= GDFIELDPOS("C6_QTDVEN")  
Local nPosVlrVen	:= GDFIELDPOS("C6_VALOR")  

Local nPosValDes	:= GDFIELDPOS("C6_XVALDES") // 'Valor Total do Des�gio do Item'  
Local nPosVlItDs	:= GDFIELDPOS("C6_XVLITDS") // 'Valor Unit�rio do Des�gio do Item' 

Default nVezes := 1

If nVezes == 1
	aCols[n,nPosValDes]	:= aCols[n,nPosVlrVen] * (nPerDes/100)
	aCols[n,nPosVlItDs]	:= aCols[n,nPosValDes] / IIf(aCols[n,nPosQTDVen]>0,aCols[n,nPosQTDVen],1)	           					// C6_XVALDES / C6_QTDVEN
Else
	For nVezes:= 1 To Len(aCols)
		aCols[nVezes,nPosValDes]	:= aCols[nVezes,nPosVlrVen] * (nPerDes/100)
		aCols[nVezes,nPosVlItDs]	:= aCols[nVezes,nPosValDes] / IIf(aCols[nVezes,nPosQTDVen]>0,aCols[nVezes,nPosQTDVen],1)	// C6_XVALDES / C6_QTDVEN
	Next nVezes
EndIf

Return cCampo