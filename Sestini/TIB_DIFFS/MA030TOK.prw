#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"

//--------------------------------------------------------------------
/*/{Protheus.doc} MA030TOK
Ponto de Entrada chamando em MATA030 - Cadastro de Cliente (SA1)

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------
User Function MA030TOK()

	lRet := execRoyalt()
	
	if ( lRet )
		lRet := execItCTD()
		lRet := execRota()
	endif

Return lRet

Static Function execRoyalt()

	Local lRet:=.T.
	Local aArea  := GetArea()
	lRet:=A_030TOK()
	If lRet .And. !Empty(M->A1_XGRPMAT)
		B_030TOK(M->A1_XGRPMAT, M->A1_XDESAG, M->A1_XPERDES, M->A1_XDSVDES, M->A1_XNCDES, M->A1_COD)
	EndIf
	RestArea(aArea)
	
Return lRet 

//--------------------------------------------------------------------
/*/{Protheus.doc} A_030TOK()
Fun��o de Valida��o de Inclus�o/Altera��o de Cadastro de Cliente (SA1)

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function A_030TOK()
Local _lRet  := .T.
Local _aArea := GetArea()

IF M->A1_XDESAG == "S" .And. M->A1_XPERDES <= 0 
   MsgAlert("Favor informar Percentual do Des�gio!")
   _lRet := .F.
EndIf   
RestArea(_aArea)
Return _lRet 

//--------------------------------------------------------------------
/*/{Protheus.doc} B_030TOK()
Tela de Replica��o de Dados de Licenciamento para Grupo 

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function B_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1)
Local _aArea := GetArea()

@ 0,0 TO 240,450 DIALOG oDlg1 TITLE "Replica��o de Licenciamento"
                                                       
@ 10,10 SAY 'Deseja Replicar as Informa��es abaixo para todos os clientes do grupo "' + AllTRim(Grupo) + '" ?' 
@ 30,10 SAY 'Informa��es a serem replicadas:' 
@ 40,10 SAY 'Des�gio: "' + Desagio + '"'
@ 50,10 SAY 'Percentual de Des�gio: ' + STR(Percent,5,2)  + ' %'
@ 60,10 SAY 'Percentual de Desconto de Des�gio: ' + STR(Descont,5,2) + ' %'
@ 70,10 SAY 'N�mero de Contrato: "' + AllTRim(Contrato) + '"'

*---------------Bot�o de OK-----------------------* 
@ 90,135 BUTTON "N�o" SIZE 35,15 ACTION Close(oDlg1) 
@ 90,185 BUTTON "Sim" SIZE 35,15 ACTION (C_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1), Close(oDlg1)) 
*-------------------------------------------------*

ACTIVATE DIALOG oDlg1 CENTER 

RestArea(_aArea)
Return 

//--------------------------------------------------------------------
/*/{Protheus.doc} C_030TOK()
Fun��o de Replica��o de Dados de Licenciamento para Grupo 

@author TOTVS Protheus
@since  30/07/2018
@obs    
@version 1.0
/*/
//--------------------------------------------------------------------

Static Function C_030TOK(Grupo, Desagio, Percent, Descont, Contrato, CodSA1)
Local _aArea   := GetArea()
Local _nCnt     := 0
Local _xFilSA1 := xFilial("SA1")
Local _cOrdem  := " A1_RECNO "
Local _cQuery  := " SELECT A1_XGRPMAT, A1_COD, R_E_C_N_O_ A1_RECNO"

_cQuery += " FROM "
_cQuery += RetSqlName("SA1")        + " SA1 "
_cQuery += " WHERE '"               + _xFilSA1 + "' = A1_FILIAL "
_cQuery += " AND    A1_COD <> '"    + CodSA1   + "' "
_cQuery += " AND    A1_XGRPMAT = '" + Grupo    + "' " 
_cQuery += " AND    SA1.D_E_L_E_T_ = '' "
_cQuery += " ORDER BY "             + _cOrdem
_cQuery := ChangeQuery(_cQuery)
dbUseArea(.T.,"TOPCONN",TCGENQRY(,,_cQuery),"SA1TMP",.F.,.T.)

Dbselectarea("SA1TMP")
SA1TMP->(dbGoTop())
COUNT to _nCnt
SA1TMP->(dbGoTop())
ProcRegua( _nCnt )

If !Eof()
	Begin Transaction
	While !Eof()
	
	    Incproc("Atualizando Cliente: " + SA1TMP->A1_COD) 
		dbSelectArea("SA1")
		SA1->(DbGoTo(SA1TMP->A1_RECNO))
		If SA1->(RecLock("SA1", .F.))
			SA1->A1_XDESAG  := Desagio
			SA1->A1_XPERDES := Percent
			SA1->A1_XDSVDES := Descont
			SA1->A1_XNCDES  := Contrato
			SA1->(MsUnLock())
		EndIf
		dbSelectArea("SA1TMP")
		SA1TMP->(DbSkip())
	Enddo   

    MsgAlert("Replica��o de Licenciamento Realizada com Sucesso!!")
    End Transaction

EndIf
DbClosearea("SA1TMP")
 
RestArea(_aArea)
Return 

Static Function execItCTD()

Local _aArea   := GetArea()
Local _cQuery  := ""
Local _cCodigo := "C"+M->A1_COD+M->A1_LOJA
Local _cNome   := Alltrim(M->A1_NOME)
Local _lRet    := .T.

If (Select("TMP") <> 0)
	dbSelectArea("TMP")
	dbCloseArea()
Endif

If INCLUI
	_cQuery := " SELECT CTD_FILIAL, CTD_ITEM "
	_cQuery += " FROM " + RetSqlName("CTD")
	_cQuery += " WHERE CTD_FILIAL = '"+xFilial("CTD")+"' AND CTD_ITEM = '"+_cCodigo+"' AND D_E_L_E_T_ = ' ' "
 
	TcQuery _cQuery New Alias "TMP"

	If TMP->(Eof())
		If RecLock("CTD",.T.)
			CTD->CTD_FILIAL := xFilial("CTD")
			CTD->CTD_ITEM   := _cCodigo
			CTD->CTD_CLASSE := "2"
			CTD->CTD_NORMAL := "2"
			CTD->CTD_DESC01 := _cNome
			CTD->CTD_BLOQ   := "2"
			CTD->CTD_DTEXIS := Ctod("01/01/17")
			MsUnLock("CTD")
		Endif    
	Endif
Endif

RestArea(_aArea)

Return(_lRet)

Static Function execRota()

Local lRet      := .T.
	Local cSequen   := "000001"
	Local cAliasDA7 := GetNextAlias()
		
	DbSelectarea("DA7")
	DA7->(DbSetorder(2))
	If DA7->(DbSeek(xFilial("DA7") + SA1->A1_COD + SA1->A1_LOJA))
		Reclock("DA7",.F.)
			DA7_PERCUR := SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)
			DA7_ROTA   := SA1->A1_XCANAL
			DA7_CLIENT := SA1->A1_COD
			DA7_LOJA   := SA1->A1_LOJA
		DA7->(MsUnlock())
	Else
		BeginSQL Alias cAliasDA7
			SELECT MAX(DA7_SEQUEN) AS DA7_SEQUEN FROM DA7010 WHERE D_E_L_E_T_ = '' AND DA7_PERCUR = %exp:SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)% AND DA7_ROTA = %exp:SA1->A1_XCANAL%
		EndSQL
		
		If (cAliasDA7)->(!EoF())
			cSequen := (cAliasDA7)->DA7_SEQUEN
		EndIf
		
		Reclock("DA7",.T.)
			DA7_PERCUR := SA1->A1_EST + LEFT(SA1->A1_COD_MUN,4)
			DA7_ROTA   := SA1->A1_XCANAL
			DA7_SEQUEN := SOMA1(cSequen)
			DA7_CLIENT := SA1->A1_COD
			DA7_LOJA   := SA1->A1_LOJA
		DA7->(MsUnlock())
	Endif
	
Return lRet