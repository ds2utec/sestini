/*/{Protheus.doc} SF2460I
Ponto de entrada localizado ap�s a atualiza��o das tabelas referentes � nota fiscal (SF2/SD2), mas antes da contabiliza��o. 
ATEN��O: Este ponto de entrada est� dentro da transa��o na grava��o das tabelas do documento.

Link TDN: http://tdn.totvs.com/pages/releaseview.action?pageId=6784447

@author DS2U (SDA)
@since 31/10/2018
@version 1.0

@type function
/*/
User Function SF2460I()

	local alArea	:= getArea()

	//-----------------------------------------------
	// Desaloca quantidade conforme itens faturados -
	//-----------------------------------------------
	eraseAloc()
	
	If SC5->(FieldPos("C5_NUM")) > 0	
		
		RECLOCK("SF2",.F.)
		SF2->F2_XCANAL	:= SC5->C5_XCANAL  
		MsUnLock()
		
	EndIf

	restArea( alArea )

Return

/*/{Protheus.doc} eraseAloc
Funcao auxiliar para desalocar os itens faturados
@author DS2U (SDA)
@since 31/10/2018
@version 1.0

@type function
/*/
Static Function eraseAloc()

	dbSelectArea( "SD2" )
	SD2->( dbSetOrder( 3 ) ) // D2_FILIAL, D2_DOC, D2_SERIE, D2_CLIENTE, D2_LOJA, D2_COD, D2_ITEM, R_E_C_N_O_, D_E_L_E_T_
	
	if ( SD2->( dbSeek( SF2->( F2_FILIAL + F2_DOC + F2_SERIE + F2_CLIENTE + F2_LOJA ) ) ) )
	
		while (	.not. SD2->( eof() );
				.and. SD2->D2_FILIAL == SF2->F2_FILIAL;
				.and. SD2->D2_DOC == SF2->F2_DOC;
				.and. SD2->D2_SERIE == SF2->F2_SERIE;
				.and. SD2->D2_CLIENTE == SF2->F2_CLIENTE;
				.and. SD2->D2_LOJA == SF2->F2_LOJA;
			  )
		
			//--------------------------------------------
			// Retira de aloca��es a quantidade faturada -
			//--------------------------------------------
			U_ADMVP02( 4, { SD2->D2_FILIAL, SD2->D2_PEDIDO, SD2->D2_ITEMPV, "", "", ( SD2->D2_QUANT * -1 ), date(), SD2->D2_LOCAL, SD2->D2_COD } )
		
			SD2->( dbSkip() )
		endDo
	
	endif

Return