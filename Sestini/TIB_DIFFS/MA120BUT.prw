/*/{Protheus.doc} MA120BUT
LOCALIZA��O : Function A120PEDIDO - Fun��o do Pedido de Compras e Autoriza��o de Entrega responsavel pela inclus�o, altera��o, exclus�o e c�pia dos PCs. 
EM QUE PONTO : No inico da Fun��o, antes de&nbsp;montar a&nbsp;ToolBar do Pedido de Compras, deve ser usado para adicionar bot�es do usuario na toolbar do PC ou AE atrav�s do retorno de um Array com a estrutura do bot�o a adicionar.

Link TDN: http://tdn.totvs.com/pages/releaseview.action?pageId=6085345

@author DS2U (SDA)
@since 08/08/2018
@version 1.0
@return alButtons, array com informa��es de opcoes customizadas para o  pedido de compras

@type function
/*/
User Function MA120BUT()

	local alButtons	:= {}

	if ( .not. INCLUI .and. .not. ALTERA )
		AADD( alButtons,{ "BMPPOST",  {|| U_ADMVA09( 1 ) }, "Cotas", "Cotas" }  )
	endif

Return alButtons