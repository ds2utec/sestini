/*/{Protheus.doc} DIRA140
Ponto de entrada apos gerar a tabela AH5 com valores por item de comissao dos direitos autorais
@type function
@author B. Vinicius
@since 29/08/2018
@version 1.0
/*/User Function DIRA140()

Local aArea := GetArea()

//Atualiza o canal de vendas
If Reclock("AH5",.F.)
	AH5_XCANAL := SD2->D2_XCANAL
	AH5->(MsUnLock())
Endif

RestArea(aArea)

Return