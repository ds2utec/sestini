 /*
Desc: Ponto de entrada para levar campo da sc7 para sd1
Autor: Bruno Landi
Data: 23/05/2018
Vers�o: 23.05.2018a
OBS: NECESS�RIO CRIAR CAMPO D1_X_DESC     
Altera��o: - 
*/


User Function MT103IPC

Local _nItem := PARAMIXB[1]
Local _nDesc := AsCan(aHeader,{|x|Alltrim(x[2])=="D1_XDESC"})
//Local _nQuant:= AsCan(aHeader,{|x|Alltrim(x[2])=="D1_XQUANT"})

If _nItem > 0
	aCols[_nItem,_nDesc] := SC7->C7_DESCRI
	//aCols[_nItem,_nQuant] := SC7->C7_QUANT
Else
	IF _nItem > 0
		aCols[_nItem,_nDesc] := SC7->C7_DESCRI
		//aCols[_nItem,_nQuant] := SC7->C7_QUANT
	ENDIF
Endif

Return  