#INCLUDE "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � STNR015�   Autor � Rogerio Machado    � Data �  12/09/2018 ���
�������������������������������������������������������������������������͹��
���Desc.     � Programa para gerar relatorio de Metas                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function STNA055()

	Local   cPerg       := "APROVPC"
	Private cAliasApr 	:= GetNextAlias()
	
	Pergunte(cPerg,.T.)
	
   /*
	*    1 = SA  |  2 = SC  |  3 =  PC  |
	*/
	If MV_PAR01 == 1
       /*
	    *    TRATATIVAS PARA SA
	    */
		BeginSql alias cAliasApr
			SELECT AK_COD, 
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'NAO' ELSE 'SIM' END AS LIBDOC FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBDOC,
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'SIM' ELSE 'NAO' END AS LIBNEXT FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBNEXT,
			SCR.*
			FROM SCR010 SCR
			INNER JOIN SAL010 AS SAL ON CR_GRUPO = AL_COD AND CR_USER = AL_USER AND SAL.D_E_L_E_T_ = ''
			INNER JOIN SAK010 AS SAK ON AK_USER = %exp:__cUserID% AND SAK.D_E_L_E_T_ = ''
			WHERE SCR.D_E_L_E_T_= '' AND CR_NUM BETWEEN %exp:MV_PAR02% AND %exp:MV_PAR03% AND CR_USER = %exp:__cUserID%  AND CR_STATUS IN ('02')
			AND CR_TIPO = 'SA'
		EndSQL
		
		If (cAliasApr)->(!EOF())
			While (cAliasApr)->(!EOF())
				dbSelectArea("SCR")
				dbSetOrder(1)
				If dbSeek(xFilial("SCR")+ "PC" + (cAliasApr)->CR_NUM + ALLTRIM((cAliasApr)->CR_NIVEL))
					//LIBERA PC NO NIVEL DO USUARIO
					Reclock("SCR",.F.)
						SCR->CR_STATUS  := "03"
						SCR->CR_DATALIB := dDataBase
						SCR->CR_USERLIB := __cUserID
						SCR->CR_LIBAPRO := (cAliasApr)->AK_COD
						SCR->CR_VALLIB  := (cAliasApr)->CR_TOTAL
					SCR->(MsUnlock())

					//ATUALIZA SALDO
					dbSelectArea("SCS")
					dbSetOrder(1)
					If dbSeek(xFilial("SCS")+ __cUserID)
						Reclock("SCS",.F.)
							SCS->CS_SALDO := SCS->CS_SALDO-(cAliasApr)->CR_TOTAL
						SCS->(MsUnlock())			
					EndIf
					
					//DEIXA STATUS PRONTO PARA O PROXIMO N�VEL
					If (cAliasApr)->LIBNEXT == 'SIM'
						SCR->(DbSkip())
						Reclock("SCR",.F.)
							SCR->CR_STATUS  := "02"
						SCR->(MsUnlock())
					EndIf
				EndIf
				
				//LIBERA PC NO NIVEL MAXIMO
				If (cAliasApr)->LIBDOC == 'SIM'
					dbSelectArea("SCP")
					dbSetOrder(1)
					If dbSeek(xFilial("SCP") + ALLTRIM((cAliasApr)->CR_NUM))
						While SCP->(!EOF()) .And. SCP->CP_NUM == ALLTRIM((cAliasApr)->CR_NUM)
							Reclock("SCP",.F.)
								SCP->CP_STATSA := 'L'
							SCP->(MsUnlock())							
							SCP->(DbSkip())
						End
					EndIf				
				EndIf
				//LIBERA PC NO NIVEL MAXIMO
				
				(cAliasApr)->(DbSkip())
			End
			
			(cAliasApr)->(dbCloseArea())
			AVISO(" DOC'S LIBERADOS ","Clique em OK.",{"OK"},1)
		Else
			MsgAlert("Nenhum Pedido de Compra encontrado de acordo com o range e n�vel informado.","Arquivo Vazio")
		EndIf
	
		
			    
	ElseIf MV_PAR01 == 2
       /*
	    *    TRATATIVAS PARA SC
	    */
		BeginSql alias cAliasApr
			SELECT AK_COD, 
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'NAO' ELSE 'SIM' END AS LIBDOC FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBDOC,
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'SIM' ELSE 'NAO' END AS LIBNEXT FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBNEXT,
			SCR.*
			FROM SCR010 SCR
			INNER JOIN SAL010 AS SAL ON CR_GRUPO = AL_COD AND CR_USER = AL_USER AND SAL.D_E_L_E_T_ = ''
			INNER JOIN SAK010 AS SAK ON AK_USER = %exp:__cUserID% AND SAK.D_E_L_E_T_ = ''
			WHERE SCR.D_E_L_E_T_= '' AND CR_NUM BETWEEN %exp:MV_PAR02% AND %exp:MV_PAR03% AND CR_USER = %exp:__cUserID%  AND CR_STATUS IN ('02')
			AND CR_TIPO = 'SC'
		EndSQL
		
		If (cAliasApr)->(!EOF())
			While (cAliasApr)->(!EOF())
				dbSelectArea("SCR")
				dbSetOrder(1)
				If dbSeek(xFilial("SCR")+ "SC" + (cAliasApr)->CR_NUM + ALLTRIM((cAliasApr)->CR_NIVEL))
					//LIBERA SC NO NIVEL DO USUARIO
					Reclock("SCR",.F.)
						SCR->CR_STATUS  := "03"
						SCR->CR_DATALIB := dDataBase
						SCR->CR_USERLIB := __cUserID
						SCR->CR_LIBAPRO := (cAliasApr)->AK_COD
						SCR->CR_VALLIB  := (cAliasApr)->CR_TOTAL
					SCR->(MsUnlock())

					//ATUALIZA SALDO
					dbSelectArea("SCS")
					dbSetOrder(1)
					If dbSeek(xFilial("SCS")+ __cUserID)
						Reclock("SCS",.F.)
							SCS->CS_SALDO := SCS->CS_SALDO-(cAliasApr)->CR_TOTAL
						SCS->(MsUnlock())			
					EndIf					
					
					//DEIXA STATUS PRONTO PARA O PROXIMO N�VEL
					If (cAliasApr)->LIBNEXT == 'SIM'
						SCR->(DbSkip())
						Reclock("SCR",.F.)
							SCR->CR_STATUS  := "02"
						SCR->(MsUnlock())
					EndIf					
				EndIf
				
				//LIBERA SC NO NIVEL MAXIMO
				If (cAliasApr)->LIBDOC == 'SIM'
					dbSelectArea("SC1")
					dbSetOrder(1)
					If dbSeek(xFilial("SC1") + ALLTRIM((cAliasApr)->CR_NUM))
						While SC1->(!EOF()) .And. SC1->C1_NUM == ALLTRIM((cAliasApr)->CR_NUM)
							Reclock("SC1",.F.)
								SC1->C1_APROV := 'L'
							SC1->(MsUnlock())
							SC1->(DbSkip())
						End
					EndIf				
				EndIf
				//LIBERA SC NO NIVEL MAXIMO
				
				(cAliasApr)->(DbSkip())
			End
			
			(cAliasApr)->(dbCloseArea())
			AVISO(" DOC'S LIBERADOS ","Clique em OK.",{"OK"},1)
		Else
			MsgAlert("Nenhuma Solicita��o de Compra encontrada de acordo com o range e n�vel informado.","Arquivo Vazio")
		EndIf
		
		    
	Else
	   /*
	    *    TRATATIVAS PARA PC
	    */
		BeginSql alias cAliasApr
			SELECT AK_COD, 
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'NAO' ELSE 'SIM' END AS LIBDOC FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBDOC,
			(SELECT DISTINCT CASE WHEN SAL.AL_NIVEL < MAX(SAL2.AL_NIVEL) THEN 'SIM' ELSE 'NAO' END AS LIBNEXT FROM SAL010 SAL2 WHERE SAL2.D_E_L_E_T_ = '' AND SAL2.AL_COD = CR_GRUPO) AS LIBNEXT,
			SCR.*
			FROM SCR010 SCR
			INNER JOIN SAL010 AS SAL ON CR_GRUPO = AL_COD AND CR_USER = AL_USER AND SAL.D_E_L_E_T_ = ''
			INNER JOIN SAK010 AS SAK ON AK_USER = %exp:__cUserID% AND SAK.D_E_L_E_T_ = ''
			WHERE SCR.D_E_L_E_T_= '' AND CR_NUM BETWEEN %exp:MV_PAR02% AND %exp:MV_PAR03% AND CR_USER = %exp:__cUserID%  AND CR_STATUS IN ('02')
			AND CR_TIPO = 'PC'
		EndSQL
		
		If (cAliasApr)->(!EOF())
			While (cAliasApr)->(!EOF())
				dbSelectArea("SCR")
				dbSetOrder(1)
				If dbSeek(xFilial("SCR")+ "PC" + (cAliasApr)->CR_NUM + ALLTRIM((cAliasApr)->CR_NIVEL))
					//LIBERA PC NO NIVEL DO USUARIO
					Reclock("SCR",.F.)
						SCR->CR_STATUS  := "03"
						SCR->CR_DATALIB := dDataBase
						SCR->CR_USERLIB := __cUserID
						SCR->CR_LIBAPRO := (cAliasApr)->AK_COD
						SCR->CR_VALLIB  := (cAliasApr)->CR_TOTAL
					SCR->(MsUnlock())

					//ATUALIZA SALDO
					dbSelectArea("SCS")
					dbSetOrder(1)
					If dbSeek(xFilial("SCS")+ __cUserID)
						Reclock("SCS",.F.)
							SCS->CS_SALDO := SCS->CS_SALDO-(cAliasApr)->CR_TOTAL
						SCS->(MsUnlock())			
					EndIf
					
					//DEIXA STATUS PRONTO PARA O PROXIMO N�VEL
					If (cAliasApr)->LIBNEXT == 'SIM'
						SCR->(DbSkip())
						Reclock("SCR",.F.)
							SCR->CR_STATUS  := "02"
						SCR->(MsUnlock())
					EndIf
				EndIf
				
				//LIBERA PC NO NIVEL MAXIMO
				If (cAliasApr)->LIBDOC == 'SIM'
					dbSelectArea("SC7")
					dbSetOrder(1)
					If dbSeek(xFilial("SC7") + ALLTRIM((cAliasApr)->CR_NUM))
						While SC7->(!EOF()) .And. SC7->C7_NUM == ALLTRIM((cAliasApr)->CR_NUM)
							Reclock("SC7",.F.)
								SC7->C7_CONAPRO := 'L'
							SC7->(MsUnlock())							
							SC7->(DbSkip())
						End
					EndIf				
				EndIf
				//LIBERA PC NO NIVEL MAXIMO
				
				(cAliasApr)->(DbSkip())
			End
			
			(cAliasApr)->(dbCloseArea())
			AVISO(" DOC'S LIBERADOS ","Clique em OK.",{"OK"},1)
		Else
			MsgAlert("Nenhum Pedido de Compra encontrado de acordo com o range e n�vel informado.","Arquivo Vazio")
		EndIf
		
	EndIf

Return