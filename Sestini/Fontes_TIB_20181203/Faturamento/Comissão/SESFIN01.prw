#INCLUDE "PROTHEUS.CH"
/*/{Protheus.doc} SESFIN01
Calculo de inadimpletes
@author Rodolfo
@since 27/08/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
User Function SESFIN01()
Local aParamBox := {}
Private aPergRet	:= {}


aAdd(aParamBox, {1, "Data De"		, dDataBase ,  ,/*'U_VlDtIni()'*/, ,, 50, .T.} )
aAdd(aParamBox, {1, "Data At�"		, dDataBase ,  ,/*'U_VlDtFim()'*/, ,, 50, .T.} )
aAdd(aParamBox, {1, "Vendedor De"	, Space(Tamsx3('A3_COD')[1]) , "@!" ,,'SA3' ,, 50, .F.} )
aAdd(aParamBox, {1, "Vendedor At�"	, Space(Tamsx3('A3_COD')[1]) , "@!" ,,'SA3' ,, 50, .T.} )
aAdd(aParamBox, {1, "Data Pagto"	, dDataBase ,  ,, ,, 50, .T.} ) //TODO pra que serve essa data?

If ParamBox(aParamBox, 'Calculo de Inadimplentes', aPergRet)	
	MsgRun('Processando calculo de inadimplentes', 'Aguarde...', {|| SE3Load()})
EndIf

Return


/*/{Protheus.doc} SE3Load
Realiza inclusao na SE3
@author Rodolfo
@since 28/08/2018
@version 1.0
@return ${return}, ${return_description}
@example
(examples)
@see (links_or_references)
/*/
Static Function SE3Load()
Local cAliasQry := ''
Local cQuery	:= ''
Local aAuto 	:= {}
Local cDtAux	:= ''
Local dData		:= Stod('')
Local cDia		:= ''
lMsErroAuto 	:= .F.


cQuery := "SELECT SE1.* , SC6.* "
cQuery += "  FROM " + RetSQLTab('SE1') 
cQuery += "  JOIN " + RetSQLTab('SC6') + "  ON " 
cQuery += "  SC6.C6_FILIAL = '" + xFilial("SE1") + "' "
cQuery += "  AND SC6.C6_NUM = SE1.E1_PEDIDO "
cQuery += "  AND SC6.D_E_L_E_T_  = ' '"
cQuery += "  JOIN " + RetSQLTab('SA1') + "  ON " 
cQuery += "  SA1.A1_FILIAL = '" + xFilial("SA1") + "' "
cQuery += "  AND SA1.A1_COD = SE1.E1_CLIENTE "
cQuery += "  AND SA1.A1_LOJA = SE1.E1_LOJA "
cQuery += "  AND SA1.D_E_L_E_T_  = ' '"
cQuery += " WHERE SE1.E1_FILIAL   = '" + xFilial("SE1") + "' " 
cQuery += "   AND SE1.E1_XINADIN  = 'I'"
cQuery += "   AND SE1.E1_SALDO    > 0 "
cQuery += "   AND SE1.E1_VENCTO   < '" + DTOS(dDataBase) + "' "  
cQuery += "   AND ((SA1.A1_VEND    >= '" + aPergRet[3] + "' "
cQuery += "   AND SA1.A1_VEND    <= '" + aPergRet[4] + "') OR "	
cQuery += "   (SA1.A1_XVEND2    >= '" + aPergRet[3] + "' "
cQuery += "   AND SA1.A1_XVEND2    <= '" + aPergRet[4] + "'))"			
cQuery += "   AND SE1.D_E_L_E_T_  = ' '"

//-- PROCESSA QUERY
cQuery    := ChangeQuery(cQuery)
cAliasQry := GetNextAlias()
dbUseArea(.T., "TOPCONN", TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

While (cAliasQry)->(!EOF())
	//ESTORNA A COMISSAO
	//TODO ver como sera tratado vendedor 2
	If (cAliasQry)->C6_COMIS1 > 0
		dbselectarea('SA1')
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial('SA1') + (cAliasQry)->E1_CLIENTE + (cAliasQry)->E1_LOJA))
		
		
		dbSelectArea('SA3')
		SA3->(DbSetOrder(1))
		SA3->(DbSeek(xFilial('SA3') + SA1->A1_VEND))
		aAuto := {}
	
		aAdd(aAuto,{"E3_VEND" ,SA3->A3_COD ,Nil})
		aAdd(aAuto,{"E3_NUM" ,(cAliasQry)->E1_NUM ,Nil})
		aAdd(aAuto,{"E3_EMISSAO" ,dDataBase ,Nil})
		aAdd(aAuto,{"E3_SERIE" ,(cAliasQry)->E1_SERIE ,Nil})
		aAdd(aAuto,{"E3_CODCLI" ,SA1->A1_COD ,Nil})
		aAdd(aAuto,{"E3_LOJA" ,SA1->A1_LOJA ,Nil})
		aAdd(aAuto,{"E3_BASE" ,((cAliasQry)->E1_SALDO * -1) ,Nil})
		aAdd(aAuto,{"E3_PORC" ,(cAliasQry)->C6_COMIS1 ,Nil})
		aAdd(aAuto,{"E3_DATA" ,aPergRet[5] ,Nil})
		aAdd(aAuto,{"E3_PREFIXO" ,(cAliasQry)->E1_PREFIXO ,Nil})
		aAdd(aAuto,{"E3_PARCELA" ,(cAliasQry)->E1_PARCELA ,Nil})
		aAdd(aAuto,{"E3_SEQ" ,"  " ,Nil})
		aAdd(aAuto,{"E3_TIPO" ,(cAliasQry)->E1_TIPO ,Nil})
		cDia   := IIf(Len(Alltrim(Str(SA3->A3_DIA))) == 1 , '0' + Alltrim(Str(SA3->A3_DIA)) , Alltrim(Str(SA3->A3_DIA)))
		cDtAux := Dtoc(dDatabase)
		dData  := MonthSum(cTod(Stuff(cDtAux,1,2,cDia)),1)	
		aAdd(aAuto,{"E3_VENCTO" ,dData ,Nil})
		aAdd(aAuto,{"E3_PEDIDO" ,(cAliasQry)->E1_PEDIDO ,Nil})
		aAdd(aAuto,{"E3_PROCCOM" ,"INCLUSAO" ,Nil})
		aAdd(aAuto,{"E3_MOEDA" ,"01" ,Nil})
		MSExecAuto({|x,y| Mata490(x,y)},aAuto,3) //Inclusao	
		
		If lMsErroAuto
			If lMsErroAuto
				Conout("Ocorreu algum problema.")
				MOSTRAERRO()
			Else
				Conout("Executado com sucesso.")	
			Endif
		EndIf	
	EndIf
	
	If (cAliasQry)->C6_COMIS2 > 0
		dbselectarea('SA1')
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial('SA1') + (cAliasQry)->E1_CLIENTE + (cAliasQry)->E1_LOJA))
		
		
		dbSelectArea('SA3')
		SA3->(DbSetOrder(1))
		SA3->(DbSeek(xFilial('SA3') + SA1->A1_XVEND2))
		aAuto := {}
	
		aAdd(aAuto,{"E3_VEND" ,SA3->A3_COD ,Nil})
		aAdd(aAuto,{"E3_NUM" ,(cAliasQry)->E1_NUM ,Nil})
		aAdd(aAuto,{"E3_EMISSAO" ,dDataBase ,Nil})
		aAdd(aAuto,{"E3_SERIE" ,(cAliasQry)->E1_SERIE ,Nil})
		aAdd(aAuto,{"E3_CODCLI" ,SA1->A1_COD ,Nil})
		aAdd(aAuto,{"E3_LOJA" ,SA1->A1_LOJA ,Nil})
		aAdd(aAuto,{"E3_BASE" ,((cAliasQry)->E1_SALDO * -1) ,Nil})
		aAdd(aAuto,{"E3_PORC" ,(cAliasQry)->C6_COMIS2 ,Nil})
		aAdd(aAuto,{"E3_DATA" ,aPergRet[5] ,Nil})
		aAdd(aAuto,{"E3_PREFIXO" ,(cAliasQry)->E1_PREFIXO ,Nil})
		aAdd(aAuto,{"E3_PARCELA" ,(cAliasQry)->E1_PARCELA ,Nil})
		aAdd(aAuto,{"E3_SEQ" ,"  " ,Nil})
		aAdd(aAuto,{"E3_TIPO" ,(cAliasQry)->E1_TIPO ,Nil})
		cDia   := IIf(Len(Alltrim(Str(SA3->A3_DIA))) == 1 , '0' + Alltrim(Str(SA3->A3_DIA)) , Alltrim(Str(SA3->A3_DIA)))
		cDtAux := Dtoc(dDatabase)
		dData  := MonthSum(cTod(Stuff(cDtAux,1,2,cDia)),1)	
		aAdd(aAuto,{"E3_VENCTO" ,dData ,Nil})
		aAdd(aAuto,{"E3_PEDIDO" ,(cAliasQry)->E1_PEDIDO ,Nil})
		aAdd(aAuto,{"E3_PROCCOM" ,"INCLUSAO" ,Nil})
		aAdd(aAuto,{"E3_MOEDA" ,"01" ,Nil})
		MSExecAuto({|x,y| Mata490(x,y)},aAuto,3) //Inclusao	
		
		If lMsErroAuto
			If lMsErroAuto
				Conout("Ocorreu algum problema.")
				MOSTRAERRO()
			Else
				Conout("Executado com sucesso.")	
			Endif
		EndIf	
	EndIf	
	(cAliasQry)->(DbSkip())
EndDo


If Select(cAliasQry) > 0
	(cAliasQry)->(DbCloseArea())
Endif	

cQuery := "SELECT SE1.* , SC6.* , SE5.* "
cQuery += "  FROM " + RetSQLTab('SE1')  
cQuery += "  JOIN " + RetSQLTab('SC6') + " ON " 
cQuery += "  SC6.C6_FILIAL = '" + xFilial("SE1") + "' "
cQuery += "  AND SC6.C6_NUM = SE1.E1_PEDIDO "
cQuery += "  AND SC6.D_E_L_E_T_  = ' '"
cQuery += "  JOIN " + RetSQLTab('SE5') + "  ON "
cQuery += "  SE5.E5_FILIAL 		= '" + xFilial("SE5") + "' "
cQuery += "  AND SE5.E5_PREFIXO = SE1.E1_PREFIXO "
cQuery += "  AND SE5.E5_NUMERO 	= SE1.E1_NUM "
cQuery += "  AND SE5.E5_PARCELA = SE1.E1_PARCELA "
cQuery += "  AND SE5.E5_TIPO 	= SE1.E1_TIPO "
cQuery += "  AND SE5.D_E_L_E_T_  = ' '"
cQuery += "  JOIN " + RetSQLTab('SA1') + "  ON " 
cQuery += "  SA1.A1_FILIAL = '" + xFilial("SA1") + "' "
cQuery += "  AND SA1.A1_COD = SE1.E1_CLIENTE "
cQuery += "  AND SA1.A1_LOJA = SE1.E1_LOJA "
cQuery += " WHERE SE1.E1_FILIAL   = '" + xFilial("SE1") + "' " 
cQuery += "   AND SE1.E1_XINADIN  = 'A'"
cQuery += "   AND SE1.E1_SALDO    = 0 "
cQuery += "   AND SE1.E1_VENCTO   < '" + DTOS(dDataBase) + "' "  
cQuery += "   AND SE1.E1_BAIXA   >= '" + Dtos(aPergRet[1]) + "' "
cQuery += "   AND SE1.E1_BAIXA   <= '" + Dtos(aPergRet[2]) + "' "
cQuery += "   AND ((SA1.A1_VEND    >= '" + aPergRet[3] + "' "
cQuery += "   AND SA1.A1_VEND    <= '" + aPergRet[4] + "') OR "	
cQuery += "   (SA1.A1_XVEND2    >= '" + aPergRet[3] + "' "
cQuery += "   AND SA1.A1_XVEND2    <= '" + aPergRet[4] + "'))"	
cQuery += "   AND SE1.D_E_L_E_T_  = ' '"

//-- PROCESSA QUERY
cQuery    := ChangeQuery(cQuery)
cAliasQry := GetNextAlias()
dbUseArea(.T., "TOPCONN", TCGENQRY(,,cQuery), cAliasQry, .F., .T.)

While (cAliasQry)->(!EOF())
	If (cAliasQry)->C6_COMIS1 > 0
		//CREDITA A COMISSAO
		//TODO ver como sera tratado vendedor 2
		dbselectarea('SA1')
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial('SA1') + (cAliasQry)->E1_CLIENTE + (cAliasQry)->E1_LOJA))
		
		
		dbSelectArea('SA3')
		SA3->(DbSetOrder(1))
		SA3->(DbSeek(xFilial('SA3') + (cAliasQry)->E1_VEND1))
		aAuto := {}		
		
		aAdd(aAuto,{"E3_VEND" ,SA3->A3_COD ,Nil})
		aAdd(aAuto,{"E3_NUM" ,(cAliasQry)->E1_NUM ,Nil})
		aAdd(aAuto,{"E3_EMISSAO" ,dDataBase ,Nil})
		aAdd(aAuto,{"E3_SERIE" ,(cAliasQry)->E1_SERIE ,Nil})
		aAdd(aAuto,{"E3_CODCLI" ,SA1->A1_COD ,Nil})
		aAdd(aAuto,{"E3_LOJA" ,SA1->A1_LOJA ,Nil})
		aAdd(aAuto,{"E3_BASE" ,(cAliasQry)->E5_VALOR ,Nil})
		aAdd(aAuto,{"E3_PORC" ,(cAliasQry)->C6_COMIS1 ,Nil})
		aAdd(aAuto,{"E3_COMIS" ,(cAliasQry)->E5_VALOR * (cAliasQry)->C6_COMIS1 ,Nil})
		aAdd(aAuto,{"E3_DATA" ,aPergRet[5] ,Nil})
		aAdd(aAuto,{"E3_PREFIXO" ,(cAliasQry)->E1_PREFIXO ,Nil})
		aAdd(aAuto,{"E3_PARCELA" ,(cAliasQry)->E1_PARCELA ,Nil})
		aAdd(aAuto,{"E3_SEQ" ,"  " ,Nil})
		aAdd(aAuto,{"E3_TIPO" ,(cAliasQry)->E1_TIPO ,Nil})
		cDia   := IIf(Len(Alltrim(Str(SA3->A3_DIA))) == 1 , '0' + Alltrim(Str(SA3->A3_DIA)) , Alltrim(Str(SA3->A3_DIA)))
		cDtAux := Dtoc(dDatabase)
		dData  := MonthSum(cTod(Stuff(cDtAux,1,2,cDia)),1)	
		aAdd(aAuto,{"E3_VENCTO" ,dData ,Nil})
		aAdd(aAuto,{"E3_PEDIDO" ,(cAliasQry)->E1_PEDIDO ,Nil})
		aAdd(aAuto,{"E3_PROCCOM" ,"INCLUSAO" ,Nil})
		aAdd(aAuto,{"E3_MOEDA" ,"01" ,Nil})	
		
		MSExecAuto({|x,y| Mata490(x,y)},aAuto,3) //Inclusao	
		
		If lMsErroAuto
			If lMsErroAuto
				Conout("Ocorreu algum problema.")
				MOSTRAERRO()
			Else
				Conout("Executado com sucesso.")	
			Endif
		EndIf	
	EndIf
	If (cAliasQry)->C6_COMIS2 > 0
		//***** Exemplo de Inclus�o *****
		//TODO ver como sera tratado vendedor 2
		dbselectarea('SA1')
		SA1->(DbSetOrder(1))
		SA1->(DbSeek(xFilial('SA1') + (cAliasQry)->E1_CLIENTE + (cAliasQry)->E1_LOJA))
		
		
		dbSelectArea('SA3')
		SA3->(DbSetOrder(1))
		SA3->(DbSeek(xFilial('SA3') + (cAliasQry)->E1_VEND1))
		aAuto := {}		
		
		aAdd(aAuto,{"E3_VEND" ,SA3->A3_COD ,Nil})
		aAdd(aAuto,{"E3_NUM" ,(cAliasQry)->E1_NUM ,Nil})
		aAdd(aAuto,{"E3_EMISSAO" ,dDataBase ,Nil})
		aAdd(aAuto,{"E3_SERIE" ,(cAliasQry)->E1_SERIE ,Nil})
		aAdd(aAuto,{"E3_CODCLI" ,SA1->A1_COD ,Nil})
		aAdd(aAuto,{"E3_LOJA" ,SA1->A1_LOJA ,Nil})
		aAdd(aAuto,{"E3_BASE" ,(cAliasQry)->E5_VALOR ,Nil})
		aAdd(aAuto,{"E3_PORC" ,(cAliasQry)->C6_COMIS2 ,Nil})
		aAdd(aAuto,{"E3_COMIS" ,(cAliasQry)->E5_VALOR * (cAliasQry)->C6_COMIS2 ,Nil})
		aAdd(aAuto,{"E3_DATA" ,aPergRet[5] ,Nil})
		aAdd(aAuto,{"E3_PREFIXO" ,(cAliasQry)->E1_PREFIXO ,Nil})
		aAdd(aAuto,{"E3_PARCELA" ,(cAliasQry)->E1_PARCELA ,Nil})
		aAdd(aAuto,{"E3_SEQ" ,"  " ,Nil})
		aAdd(aAuto,{"E3_TIPO" ,(cAliasQry)->E1_TIPO ,Nil})
		cDia   := IIf(Len(Alltrim(Str(SA3->A3_DIA))) == 1 , '0' + Alltrim(Str(SA3->A3_DIA)) , Alltrim(Str(SA3->A3_DIA)))
		cDtAux := Dtoc(dDatabase)
		dData  := MonthSum(cTod(Stuff(cDtAux,1,2,cDia)),1)	
		aAdd(aAuto,{"E3_VENCTO" ,dData ,Nil})
		aAdd(aAuto,{"E3_PEDIDO" ,(cAliasQry)->E1_PEDIDO ,Nil})
		aAdd(aAuto,{"E3_PROCCOM" ,"INCLUSAO" ,Nil})
		aAdd(aAuto,{"E3_MOEDA" ,"01" ,Nil})	
		
		MSExecAuto({|x,y| Mata490(x,y)},aAuto,3) //Inclusao	
		
		If lMsErroAuto
			If lMsErroAuto
				Conout("Ocorreu algum problema.")
				MOSTRAERRO()
			Else
				Conout("Executado com sucesso.")	
			Endif
		EndIf	
	EndIf	
	(cAliasQry)->(DbSkip())
EndDo	

MsgInfo('Processo Finalizado!','Fim')
Return	