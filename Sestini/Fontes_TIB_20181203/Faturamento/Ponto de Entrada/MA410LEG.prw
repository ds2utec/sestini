#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MA410LEG
Ponto de entrada para incluir novas legendas no
browse do pedido de vendas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MA410LEG()

    Local aCores := aClone( Paramixb )

	// Adiciona legendas referente ao ADMV
	addLegADMV( aCores )

Return aCores

//-----------------------------------------------
/*/{Protheus.doc} addLegADMV
Tratamento para as novas legendas, de bloqueio
de credito Sestini, referente ao ADMV

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function addLegADMV( aCores )

	AADD(aCores,{ "BR_MARROM","Suspenso"})

Return aCores