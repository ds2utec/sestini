#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  SF2520E    Autor  Rogerio Machado          Data 18/05/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function SF2520E()

	Local   aArray      := {}
	Private lMsErroAuto := .F.
		
	DbSelectArea("SD2")
	DbSetOrder(3)
	SD2->(DbSeek(xFilial("SD2") + SF2->F2_DOC + SF2->F2_SERIE)) 

	DbSelectArea("SC5")
	DbSetOrder(1)
	If SC5->(DbSeek(xFilial("SC5") + SD2->D2_PEDIDO)) .AND. SC5->C5_XCANAL == 'E' .AND. SC5->C5_XTIPO <> 'BOL'

		DbSelectArea("SE1")
		DbSetOrder(1)
		SE1->(DbSeek(xFilial("SE1") + SD2->D2_SERIE + SD2->D2_DOC))
		
		While SE1->(!EoF())	.AND. SE1->E1_FILIAL == xFilial("SE1") .AND. SE1->E1_NUM == SF2->F2_DOC
			aArray := { { "E1_PREFIXO" , SE1->E1_PREFIXO , NIL },;
	                  {   "E1_NUM"     , SE1->E1_NUM     , NIL } }
	 
	        MsExecAuto( { |x,y| FINA040(x,y)} , aArray, 5) 		
	        
	        If lMsErroAuto
	        	MostraErro()
	        EndIf
	        
	        SE1->(dbSkip())
	        
	    End
	    
	EndIf

Return
