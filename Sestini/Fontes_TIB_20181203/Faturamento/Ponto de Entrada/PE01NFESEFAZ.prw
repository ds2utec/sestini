#include "PROTHEUS.CH"
#include "TOPCONN.CH"
 
#define MENSCLI 2
#define NOTA 5

/*/{Protheus.doc} PE01NFESEFAZ
Ponto de Entrada na NFESFEZAZ para alterar informacoes das tags de envio do XML
@type function
@author B. Vinicius
@since 30/08/2018
/*/User Function PE01NFESEFAZ()
 
Local aArea    := GetArea()
Local aAreaSD1 := SD1->(GetArea())
Local aAreaSD2 := SD2->(GetArea())
Local aAreaSW7 := SW7->(GetArea())
Local aAreaSWD := SWD->(GetArea())
Local aNfe     := PARAMIXB  
Local cMensCli := "" 
Local lNFEntrada := aNfe[NOTA,4]=="0"
Local cNota
Local nValCofins := 0//Valor Cofins
Local nValCofMaj := 0//Valor do Cofins Majorado
Local nValPis := 0 //Valor do Pis
Local cChaveSD1 := ""
Local cChaveSD2 := ""
Local nDI := 0
Local nTotItens := 0
Local nQtdItens := 0
Local cPedVen := ""

If lNFEntrada //NF de Entrada EIC
	
	If SF1->F1_EST == "EX" // Apenas exportacao
	
	cChaveSD1 := SF1->F1_FILIAL + SF1->F1_DOC + SF1->F1_SERIE + SF1->F1_FORNECE + SF1->F1_LOJA
	
	SD1->(dbSetOrder(1))
	If SD1->(dbSeek( cChaveSD1 ))
	While (!SD1->(EOF())) .And. SD1->D1_FILIAL + ;
								SD1->D1_DOC +;
								SD1->D1_SERIE +; 
								SD1->D1_FORNECE +;
								SD1->D1_LOJA == cChaveSD1
								
		nValCofins += SD1->D1_VALIMP5 - SD1->D1_VALCMAJ  
 		nValCofMaj += SD1->D1_VALCMAJ 
 		nValPis += SD1->D1_VALIMP6 
 		nDI 	+= SD1->D1_II 
 		
 		SD1->(dbSkip())
			
	EndDo 
	
	Endif
	//Pega o valor atual da tag dados adicionais/informacoes complementares
	cMensCli := aNfe[MENSCLI]
	 
	//Busca NF e Serie 
	cNota     :=  aNfe[NOTA,2]
	cSerie   := aNfe[NOTA,1]
	
	//Busca dados adicionais customizados Sestini
	cMensCli += " " 
	cMensCli += " Conforme DI "  + RetDI(SF1->F1_HAWB)  + " emitida em "  + Dtoc(SF1->F1_EMISSAO)
	cMensCli +=  " - Embarque: " +  Alltrim(SF1->F1_HAWB) +  " - Embalagem:"  + Alltrim(SF1->F1_ESPECI1) +  " - Contendo:" +   Alltrim(Transform(SF1->F1_VOLUME1,PesqPict("SF1","F1_VOLUME1"))) +  "Volumes."
	cMensCli += " VALOR DO PIS R$" +  Alltrim(Transform(nValPis,PesqPict("SD1","D1_VALIMP6"))) +  " VALOR DO COFINS R$" +  Alltrim(Transform(nValCofins,PesqPict("SD1","D1_VALIMP5")))
	cMensCli += " VALOR DO COFINS MAJORADO R$" +  Alltrim(Transform(nValCofMaj,PesqPict("SD1","D1_VALCMAJ")))
	cMensCli += " PO NUMERO: " +  Alltrim(RetNumPo(SF1->F1_HAWB))
	cMensCli += " VALOR CAPATAZIA: " +  RetValores('714')
	cMensCli += " VALOR AFRMM: " +  RetValores('405')
		
	//Dados adicionais
	aNfe[MENSCLI] := cMenscli
	
	Endif

Else
	
	//Apenas para notas normais
	If SF2->F2_TIPO == "N"
	cChaveSD2 := SF2->F2_FILIAL + SF2->F2_DOC + SF2->F2_SERIE + SF2->F2_CLIENTE + SF2->F2_LOJA
	
	SD2->(dbSetOrder(3))
	If SD2->(dbSeek( cChaveSD2 ))
		
		cPedVen := SD2->D2_PEDIDO
		While (!SD2->(EOF())) .And. SD2->D2_FILIAL + ;
									SD2->D2_DOC +;
									SD2->D2_SERIE +; 
									SD2->D2_CLIENTE +;
									SD2->D2_LOJA == cChaveSD2
									
	 		
	 		nTotItens ++
	 		nQtdItens += SD2->D2_QUANT
	 		
	 		SD2->(dbSkip())
		EndDo	
	Endif
	
	cMensCli := aNfe[MENSCLI]
	
	//Busca dados adicionais customizados Sestini
	cMensCli += " "
	cMensCli += "Nr. Pedido " + cPedVen + " " + "ROMANEIO NR. " + Alltrim(SF2->F2_XCARGA)
	cMensCli += " TOTAL DE ITENS: " + cValToChar(nTotItens)
	cMensCli += " TOTAL DE PE�AS: " + Alltrim(Transform(nQtdItens,PesqPict("SD2","D2_QUANT")))
	cMensCli += " CUBAGEM: " + Alltrim(Transform(SF2->F2_XCUBAG,PesqPict("SF2","F2_XCUBAG")))
	cMensCli += " VOLUME: " + Alltrim(Transform(SF2->F2_VOLUME1,PesqPict("SF2","F2_VOLUME1")))
	
	//Dados adicionais
	aNfe[MENSCLI] := cMenscli
	
	Endif
	
Endif 

RestArea(aAreaSD2)
RestArea(aAreaSD1)
RestArea(aAreaSW7)
RestArea(aAreaSWD)
RestArea(aArea)

Return aNfe 

//Retorna valores de despesas
Static Function RetValores(cDespesa)

	Local aDados := {}
	Local cAliasQry := GetNextAlias()
	Local nRet
	
	BeginSQL Alias cAliasQry

		SELECT WD_VALOR_R FROM %Table:SWD% SWD

		WHERE WD_FILIAL = %Exp:FWxFilial("SWD")%

		AND WD_HAWB = %Exp:SF1->F1_HAWB% AND WD_NF_COMP = %Exp:SF1->F1_DOC%

		AND WD_SE_NFC = %Exp:SF1->F1_SERIE%

		AND SWD.D_E_L_E_T_ = ''

		AND WD_DESPESA  = %Exp:cDespesa%
		
		ORDER BY WD_DESPESA 
		
	EndSQL
	
	If !(cAliasQry)->(EOF())
		nRet := WD_VALOR_R
	Endif

	nRet := Alltrim(Transform(nRet,PesqPict("SWD","WD_VALOR_R")))
	
	(cAliasQry)->(dbCloseArea())
	
Return nRet

//Retorna numero do PO
Static Function RetNumPo(cHawb)

Local cRet := ""
Local cAliasQry := GetNextAlias()

BeginSQL Alias cAliasQry

		SELECT W7_PO_NUM FROM %Table:SW7% SW7

		WHERE W7_FILIAL = %Exp:FWxFilial("SW7")%

		AND W7_HAWB = %Exp:cHawb%

		AND  SW7.D_E_L_E_T_ = ''

EndSQL

If !(cAliasQry)->(EOF())
	cRet := (cAliasQry)->W7_PO_NUM
Endif

(cAliasQry)->(dbCloseArea())

Return cRet

Static Function RetDI(cHawb)

Local cRet := ""
Local cAliasQry := GetNextAlias()

BeginSQL Alias cAliasQry

SELECT W6_DI_NUM FROM %Table:SW6% SW6

		WHERE W6_FILIAL = %Exp:FWxFilial("SW6")%

		AND W6_HAWB = %Exp:cHawb%

		AND SW6.D_E_L_E_T_ = ''

EndSQL

If !(cAliasQry)->(EOF())
	cRet := (cAliasQry)->W6_DI_NUM
Endif

(cAliasQry)->(dbCloseArea())

Return cRet