#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  OS010GRV   Autor  Rogerio Machado          Data 18/05/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function OS010GRV()
	Local aArea         := GetArea()
	Local aAreaDA0      := DA0->(GetArea())
	Local aAreaDA1      := DA1->(GetArea())
	Local _cAliasTRB	:= GetNextAlias()
	Local _cAliasTRC	:= GetNextAlias()
    
	//Tabelas 'filhas' com o percentual de desconto
	BeginSQL Alias _cAliasTRB
		SELECT DISTINCT DA0_FILIAL, DA0_CODTAB, DA0_XDESC, DA0_XTPCOP FROM %table:DA0% AS DA0 
		WHERE DA0.%notDel%
		AND DA0_XTBPAI = %exp:DA0->DA0_CODTAB%
		ORDER BY DA0_FILIAL, DA0_CODTAB
	EndSQL
	
	//Itens da tabela 'Pai'
	BeginSQL Alias _cAliasTRC
		SELECT * FROM %table:DA1% AS DA1
		WHERE DA1.%notDel%
		AND DA1_CODTAB = %exp:DA0->DA0_CODTAB% AND DA1_FILIAL = %exp:xFilial("DA1")%
		ORDER BY DA1_FILIAL, DA1_CODTAB, DA1_ITEM
	EndSQL
	
	DbSelectArea(_cAliasTRB)
	(_cAliasTRB)->(dbGoTop())
	
	DbSelectArea(_cAliasTRC)
	(_cAliasTRC)->(dbGoTop())
	
	While (_cAliasTRB)->(!Eof()) 
		While (_cAliasTRC)->(!Eof())
			DA1->(dbSetOrder(2))
			DA1->(dbGoTop())
			If DA1->(dbSeek(xFilial("DA1")+(_cAliasTRC)->DA1_CODPRO+(_cAliasTRB)->DA0_CODTAB+(_cAliasTRC)->DA1_ITEM))
				Reclock("DA1",.F.)
					If (_cAliasTRB)->DA0_XTPCOP == "A"
						//DA1->DA1_PRCVEN  := (_cAliasTRC)->DA1_PRCVEN + ( (_cAliasTRC)->DA1_PRCVEN * ( (_cAliasTRB)->DA0_XDESC/100 ) )
						DA1->DA1_PRCVEN  := (_cAliasTRC)->DA1_PRCVEN * (1+((_cAliasTRB)->DA0_XDESC/100))	
					Else
						DA1->DA1_PRCVEN  := (_cAliasTRC)->DA1_PRCVEN - ( (_cAliasTRC)->DA1_PRCVEN * ( (_cAliasTRB)->DA0_XDESC/100 ) )
					EndIf
				DA1->(MsUnlock())
			EndIf
			
			//AVISO(" C O N C L U Í D O ", "Tabela filha: " +(_cAliasTRB)->DA0_CODTAB+ " Tabela Pai: " +DA0->DA0_CODTAB, { "OK" }, 1)
			(_cAliasTRC)->(dbSkip())
		End 
		
		(_cAliasTRC)->(dbGoTop())

		(_cAliasTRB)->(dbSkip())
	End 

(_cAliasTRB)->(dbCloseArea())
(_cAliasTRC)->(dbCloseArea())
	
RestArea(aArea)
RestArea(aAreaDA0)
RestArea(aAreaDA1)

Return