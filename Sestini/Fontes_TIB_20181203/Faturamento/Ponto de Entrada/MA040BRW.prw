/*/{Protheus.doc} MA040BRW
O ponto de entrada MA040BRW  ser� utilizado para filtrar os registros apresentados no browse, referente aos cadastros de vendedores que ser�o exibidos. 

Link TDN: http://tdn.totvs.com/display/public/PROT/MA040BRW+-+Filtro+de+registros

@author DS2U (SDA)
@since 08/08/2018
@version 1.0
@return alButtons, "Array� com as defini��es dos bot�es. aBotao[1]="ICONE"aBotao[2]=bBlocoaBotao[3]=Ajuda aBotao :={{"POSCLI",{|| Rotina()},�Dica�}}

@type function
/*/
User Function MA040BRW()

	local clFiltro	:= ""

	// Adiciona novas rotinas no aRotina da Browse de vendedores
	addAdmvBtn()

Return clFiltro

Static Function addAdmvBtn()

	AADD( aRotina, { "Canais", "U_ADMVA10(1)", 0, 3 } )
	
Return