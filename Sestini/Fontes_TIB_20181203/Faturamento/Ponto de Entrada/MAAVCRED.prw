#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MAAVCRED
P.E. Libera��odo Pedido de Venda.

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MAAVCRED()

    Local lRet  := .T.

    lRet := FCredADMV()

Return lRet

//-----------------------------------------------
/*/{Protheus.doc} FCredADMV
Valida��o de Credito ADMV da Sestini.

@author DS2U (HFA)
@since 20/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function FCredADMV()

	Local lRet  := .T.

    If !( Alltrim(SC5->C5_XBLQCRE) $ "A|L" )
        lRet := .F.
        Help(,, "MAAVCRED_01",, "O Pedido "+ Alltrim( SC5->C5_NUM ) +" est� bloqueado por "+  X3COMBO('C5_XBLQCRE',SC5->C5_XBLQCRE)  , 1, 0)
    EndIf

Return lRet