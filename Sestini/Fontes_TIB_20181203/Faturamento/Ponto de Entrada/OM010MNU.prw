#Include "Protheus.ch"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  OM010MNU    Autor  Rogerio Machado         Data 13/06/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/
 
User Function OM010MNU()
   //AAdd(aRotina, {"Msg. Info."                , "MsgInfo('ADICIONADO', 'AVISO')", 0, 10, 32, NIL})
   aAdd(aRotina, {"Importar CSV"              , "U_ImpTPreco()"                 , 0, 3 , 32, NIL})
   aAdd(aRotina, {"Exportar CSV"              , "U_ExpTPreco()"                 , 0, 3 , 32, NIL})
   aAdd(aRotina, {"Cópia Tab. (Acres./Desc.)" , "U_CopyTab()"                   , 0, 3 , 32, NIL})
Return (NIL)
