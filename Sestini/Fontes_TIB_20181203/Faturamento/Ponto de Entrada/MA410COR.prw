#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "RWMAKE.CH"

//-----------------------------------------------
/*/{Protheus.doc} MA410COR
Ponto de entrada para incluir novas legendas/cores
no browse do pedido de vendas

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
User Function MA410COR()

    Local aCores    := aClone( Paramixb )

	// Adiciona legendas referente ao ADMV
	addLegADMV( aCores )

Return aCores


//-----------------------------------------------
/*/{Protheus.doc} addLegADMV
Tratamento para as novas legendas, de bloqueio
de credito Sestini

@author DS2U (HFA)
@since 12/07/2018
@version 1.0
@type function
/*/
//-----------------------------------------------
Static Function addLegADMV( aCores )

    Local aAuxCor   := {}

    aAuxCor := { " C5_XSITUAC == 'S' ","BR_MARROM","Suspenso"}
	ASize(aCores, Len(aCores) + 1 )
	AINS( aCores, 1 )
	aCores[1] := aAuxCor

Return aCores