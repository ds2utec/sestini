#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  M410LIOK   Autor  Rogerio Machado          Data 01/08/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function M410LIOK()


Local lOk     := .T.
Local aArea   := GetArea()
Local nPosLoc := AScan(aHeader,{|x| AllTrim(x[2]) == "C6_LOCAL" })


If M->C5_XCANAL == "E" .And. AllTrim(aCols[N][nPosLoc]) <> "ECO"
	AVISO("Verificar Armazem", "Para pedidos E-Commerce, o armazem não pode ser diferente de 'ECO' ", { "Fechar"}, 2)
	lOk = .F.
EndIf

RestArea(aArea)

Return (lOk)