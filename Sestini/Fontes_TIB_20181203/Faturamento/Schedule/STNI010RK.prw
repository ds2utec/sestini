#Include "Protheus.ch"
#INCLUDE "TbiConn.ch"

/*
������������������������������������������������������������������������������
��� Programa  STNA250     Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/
 
User Function STNI010RK()

	Local cDBRak	   := "MSSQL/INTEGRATIONQUEUE" 
	Local cSrvRak	   := "172.16.0.97"
	Local nPortaDBA    := 5010
	Local nHndRak	   := 0
	Local nHndERP      := 0	
	Local cAliasC5     := ""
	Local _cAliasRKT   := ""
	Local aArea        := {}
	Local aAreaSC5     := {}
	Local aAreaSC6     := {}
	Local aAreaTRB     := {}
	Local lRetorno     := .T.
	Local lRetLoja     := .F.
	Local lRetCli      := .F.
	Local lRetPed      := .F.
	Local lRetPedPr    := .F.
	Local nPedWeb      := 0
	Local nPedID       := 0
	Local cCPF         := ""
	Local cCondPg      := ""
	Local cUpdate      := ""
	Local nDescon      := 0
	Local _nFrete      := 0
	Local lBonifica    := .F.

	
	Private lMsErroAuto := .F.
	
	RPCSETTYPE(3)
	PREPARE ENVIRONMENT EMPRESA "01" FILIAL "01010001" FUNNAME "U_STNI010RK" MODULO "FAT" TABLES "SC5","SC6","SA1","SA2","SB1","SB2","SF4","SBZ"
	
	//HABILITA SEMAFORO
	If !LockByName("ECOMMERCE",.T.,.F.)
		ConOut("[RAKUTEN][INTRAKUT ] - A ROTINA DE IMPORTA��O DOS PEDIDOS J� EST� EM EXECU��O.")
		RESET ENVIRONMENT
		Return
	EndIf
	
	ConOut("[RAKUTEN][INTRAKUT ] - SEMAFORO HABILITADO PARA A ROTINA DE IMPORTA��O DE PEDIDOS. 'ECOMMERCE' ")
	
	aArea        := GetArea()
	aAreaSC5     := SC5->(GetArea())
	aAreaSC6     := SC6->(GetArea())
	nHndERP      := AdvConnection()
	_cAliasRKT   := GetNextAlias()
	cAliasC5     := GetNextAlias()

	
	//FAZ CONEX�O COM O BANCO DA RAKUTEN
	nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
	
	If nHndRak < 0
		ConOut("[RAKUTEN][INTRAKUT ] - ERRO (" +cValToChar(nHndRak)+ ") AO CONECTAR COM " +cDBRak+ " EM " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " AS " +Time()+ " " +DtoC(Date()))
		Return
	Else
		ConOut("[RAKUTEN][INTRAKUT ] - CONECTADO NO SERVIDOR " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " COM " +cDBRak+ " AS " +Time()+ " " +DtoC(Date()))
	
	
		BeginSQL Alias _cAliasRKT
			SELECT 
				PedidoCodigo AS PED, 
				RKPED.ID AS ID, 
				CPF AS CPF, 
				ServicoEntregaCodigo AS SRVENTCOD,
				QtdeParcelas AS QTDPARC, 
				ValorFreteCobrado + ValorAgendamento AS VLRFRT, 
				ISNULL( (SELECT DISTINCT RKITEM.Presente FROM RK_RECEPCAO_PEDIDO_ITEM AS RKITEM WHERE RKPED.ID = RKITEM.PedidoID AND RKITEM.Presente = 1) ,0) AS Presente, 
				SUBSTRING(CarOperadoraRetornoMensagem, CHARINDEX('||', CarOperadoraRetornoMensagem)+2, 100) AS NSU, CarAutorizacaoCodigo AS CARAUTORIZ, RKPED.* 
			FROM RK_RECEPCAO_PEDIDO AS RKPED
			LEFT JOIN RK_RECEPCAO_PEDIDO_CARTAO AS RKCARD ON RKPED.ID = RKCARD.PedidoID
			WHERE
			PedidoCodigo NOT IN (SELECT PedidoCodigo FROM RK_RECEPCAO_PEDIDO WHERE PedidoStatus = 5 ) 
			AND DataEnvioProtheus IS NULL
			AND pedidostatus = 7
			AND Data >= '20180819 22:00'
			ORDER BY DATA 
		EndSQL
			

		//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
		TcSetConn( nHndERP )
		
		DbSelectArea(_cAliasRKT)
		(_cAliasRKT)->(dbGoTop())
		
		If (_cAliasRKT)->(EoF())
			ConOut("[RAKUTEN][INTRAKUT ] - NENHUM PEDIDO PARA SER BAIXADO DO RAKUTEN DE ACORDO COM O FILTRO CONFIGURADO")
		Else
			While (_cAliasRKT)->(!EoF())
			
				nPedWeb   := Alltrim((_cAliasRKT)->PED)
				nPedID    := (_cAliasRKT)->ID
				cCPF      := Alltrim((_cAliasRKT)->CPF)
				cCondPg   := (_cAliasRKT)->FormaPagtoInterno
				_nFrete   := (_cAliasRKT)->VLRFRT
				nDesPed   := ( (_cAliasRKT)->Desconto/(_cAliasRKT)->ValorSubTotal )
				_nPrese   := (_cAliasRKT)->ValorPresente
				nValTot   := (_cAliasRKT)->ValorTotal
				cPagto    := cValToChar((_cAliasRKT)->FormaPagto)
			
			
				//VERIFICA SE PEDIDO RAKUTEN JA ESTA NO SC5 (PEDIDO DE VENDA PROTHEUS)
				cAliasC5     := GetNextAlias()
				
				BeginSQL Alias cAliasC5
					SELECT	DISTINCT C5_NUM
					FROM	%Table:SC5%
					WHERE	%NotDel%
					AND		C5_XPEDRAK = %Exp:(_cAliasRKT)->PED%
					AND 	C5_FILIAL = %Exp:xFilial("SC5")%
					AND     C5_XREEBON NOT IN ('S','N') 
				EndSQL
				
			
				If !(cAliasC5)->(Eof())
					lRetorno 	:= .F.
					ConOut( "[RAKUTEN][INTRAKUT ] - ATEN��O: PEDIDO RAKUTEN " +%Exp:nPedWeb%+ " J� CADASTRADO NO PROTHEUS." )
				
					//VOLTA CONEX�O PARA O BANCO DO RAKUTEN
					TcSetConn( nHndRak )
					
					//ALTERAR O CAMPO DE FLAG DO RAKUTEN PARA PEDIDO INTEGRADO
					cUpdate := " UPDATE RK_RECEPCAO_PEDIDO SET DataEnvioProtheus = GETDATE() " + CRLF
					cUpdate += " WHERE PedidoCodigo = " + (_cAliasRKT)->PED + " AND PedidoStatus = 7 "
					
					TcSqlExec (cUpdate)				
							
					//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
					TcSetConn( nHndERP )	
					
					(cAliasC5)->(DbCloseArea())		
				
				Else
					
					//CADASTRAR O CLIENTE(SA1)
					If lRetorno
						lRetCli := U_CadSA1((_cAliasRKT)->PED)
					EndIf
		
					//REENVIO BONIFICADO
					If lRetorno .And. lRetCli .And. Alltrim((_cAliasRKT)->ReenvioBonificado) == 'S' 
						lRetPed := U_INTPEDIDO( nPedWeb, nPedID, cCPF, cCondPg, _nFrete, nDesPed, _nPrese, "11")
						lBonifica := .T.
					ElseIf lRetorno .And. lRetCli .And. Alltrim((_cAliasRKT)->Estado) == 'SP' .And. (_cAliasRKT)->Presente == 1 
						//PEDIDO PRESENTE DENTRO DO ESTADO DE SP - SERA GERADO DOIS PEDIDOS - OPERACAO 03 E 16
						lRetPed   := U_INTPEDIDO( nPedWeb, nPedID, cCPF, cCondPg, _nFrete, nDesPed, _nPrese, "03")
						lRetPedPr := U_INTPEDIDO( nPedWeb, nPedID, cCPF, cCondPg, _nFrete, nDesPed, _nPrese, "16")
					ElseIf lRetorno .And. lRetCli
						//INSERE O PEDIDO COM OPERACAO 01 - VENDA NORMAL
						lRetPed := U_INTPEDIDO( nPedWeb, nPedID, cCPF, cCondPg, _nFrete, nDesPed, _nPrese, "01")
					EndIf
					
					
					If lRetorno .And. lRetCli .And. lRetPed .And. !lBonifica
						//INSERE O OR�AMENTO NO M�DULO SIGALOJA
						//lRetLoja := U_STNLOJA701( Alltrim(nPedWeb), Alltrim(nPedID), nValTot, cPagto )
					EndIf
					
					If lRetorno .And. lRetCli .And. lRetPed //.And. lRetLoja
						ConOut("[RAKUTEN][INTRAKUT ] - PEDIDO E-COMMERCE TOTALMENTE PROCESSADO COM SUCESSO ")
						//VOLTA CONEX�O PARA O BANCO DO RAKUTEN
						TcSetConn( nHndRak )
						
						//ALTERAR O CAMPO DE FLAG DO RAKUTEN PARA PEDIDO INTEGRADO
						cUpdate := " UPDATE RK_RECEPCAO_PEDIDO SET DataEnvioProtheus = GETDATE() " + CRLF
						cUpdate += " WHERE PedidoCodigo = " + (_cAliasRKT)->PED + " AND PedidoStatus = 7 "
						
						TcSqlExec (cUpdate)				
								
						//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
						TcSetConn( nHndERP )										
					EndIf
			
					//Sleep(2000) //AGUARDE PARA O PROXIMO PEDIDO
				EndIf
				
				(_cAliasRKT)->(dbSkip())
			End
			
			(_cAliasRKT)->(DbCloseArea())
			
			//FECHA CONEX�O COM O BANCO DA RAKUTEN
			TcUnlink( nHndRak )
			
			//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
			TcSetConn( nHndERP )		
			
			RestArea(aArea)
			RestArea(aAreaSC5)
			RestArea(aAreaSC6)
		 
		EndIf

	Endif
	
	//DESABILITA SEMAFORO
	UnLockByName("ECOMMERCE",.T.,.F.,.F.)
	
	RESET ENVIRONMENT
	
Return



/*
������������������������������������������������������������������������������
��� Programa  CadSA1      Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function CadSA1(_cPedRak)
       Local cDBRak        := "MSSQL/INTEGRATIONQUEUE"
       Local cSrvRak       := "172.16.0.97"
       Local nPortaDBA     := 5010
       Local nHndRak       := 0
       Local nHndERP       := 0   
       Local _cAliasSA1    := ""
       Local aAreaSA1      := {}
       Local aArea         := {}
       Local aButtons      := {}
       Local aValues       := {}
       Local cTipoCli      := "F"
       Local cCodCli       := ""
       Local cLojaCli      := ""
       Local nOpc          := 3
       Local cIBGE         := 0
       Local lAchou        := .F.
       Local lRetCli       := .F.
       Local cMosErr       := ""
       Local _cExec        := ""
       Local nIBGEini      := 0
       Local nCondPag      := 0
       Local cCondPag      := ""
       Local cTpFret       := 0
       Local cTransp       := ""
       Local cVend         := ""
       Local cPedRCli      := ""
       Local _cAliasRKT    := ""
       Local _cRegiao      := GetMv("ES_REGECO")
       Local cNatEco       := GetMv("ES_NATECO")
       Local cGrpEmp       := GetMv("ES_GRPEMP")
       Local cGpTribE      := GetMv("ES_GPTRIBE")
       Local cContEco      := GetMv("ES_CONTECO")
       Local cCEco         := GetMv("ES_CCECO")
       Local cDirEco       := GetMv("ES_DIRECO")
       
       Private lMsErroAuto := .F.
       
       
       aArea         := GetArea()
       aAreaSA1      := SA1->(GetArea())
       nHndERP       := AdvConnection()
       _cAliasSA1    := GetNextAlias()
       _cAliasSE4    := GetNextAlias()
       

       //FAZ CONEX�O COM O BANCO DA RAKUTEN
       nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
       
       If nHndRak < 0
             ConOut("[RAKUTEN][CADSA1   ] - ERRO (" +cValToChar(nHndRak)+ ") AO CONECTAR COM " +cDBRak+ " EM " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " AS " +Time()+ " " +DtoC(Date()))
             Return
       Else
             ConOut("[RAKUTEN][CADSA1   ] - CONECTADO NO SERVIDOR " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " COM " +cDBRak+ " AS " +Time()+ " " +DtoC(Date()))
             
             
             BeginSQL Alias _cAliasSA1
                    SELECT 
                           PedidoCodigo AS PED, 
                           PedidoCodigoCliente AS cPedRCli,
                           RKPED.ID AS ID, 
                           CPF AS CPF, 
                           ServicoEntregaCodigo AS SRVENTCOD, 
                           ValorFreteCobrado AS VLRFRT, 
                           ISNULL( (SELECT DISTINCT RKITEM.Presente FROM RK_RECEPCAO_PEDIDO_ITEM AS RKITEM WHERE RKPED.ID = RKITEM.PedidoID AND RKITEM.Presente = 1) ,0) AS Presente, 
                           SUBSTRING(CarOperadoraRetornoMensagem, CHARINDEX('||', CarOperadoraRetornoMensagem)+2, 100) AS NSU, CarAutorizacaoCodigo AS CARAUTORIZ, RKPED.* 
                    FROM RK_RECEPCAO_PEDIDO AS RKPED
                    LEFT JOIN RK_RECEPCAO_PEDIDO_CARTAO AS RKCARD ON RKPED.ID = RKCARD.PedidoID
                    WHERE PedidoCodigo = %exp:_cPedRak% AND 
                    PedidoCodigo NOT IN (SELECT PedidoCodigo FROM RK_RECEPCAO_PEDIDO WHERE PedidoStatus = 5 ) 
                    AND DataEnvioProtheus IS NULL
                    AND pedidostatus = 7
             EndSQL
             
             //VOLTA CONEX�O PARA O BANCO DO PROTHEUS
             TcSetConn( nHndERP )
             

             DbSelectArea(_cAliasSA1)
             (_cAliasSA1)->(dbGoTop())         
             
             While (_cAliasSA1)->(!EoF())

                    cPedRCli := cValToChar( (_cAliasSA1)->cPedRCli )
                    
                    
                    If (_cAliasSA1)->VLRFRT > 0
                           cTpFret := "F"
                    Else
                           cTpFret := "C"
                    EndIf
                    
                    DbSelectarea("ZA3")
                    ZA3->(DbSetorder(1))
                    If ZA3->(DbSeek(xFilial("ZA3") + cValtoChar((_cAliasSA1)->SRVENTCOD)))                    
                           cTransp  := ZA3->ZA3_TRANSP
                    EndIf
                    
            		DbSelectarea("ZA5")
					ZA5->(DbSetorder(1))
					If ZA5->(DbSeek(xFilial("ZA5") + CValToChar((_cAliasSA1)->FormaPagto)))
							cVend := ZA5->ZA5_VEND
					EndIf                    
                    
                    
                    If (_cAliasSA1)->Pagamento == 3
                    	DbSelectarea("ZA4")
	                    ZA4->(DbSetorder(1))
	                    If ZA4->(DbSeek(xFilial("ZA4") + "0")) //SEMPRE VAI OLHAR A COND PAGTO ATRELADO AO C�DIGO ZERO
	                    	cCondPag := ZA4->ZA4_COND
	                    EndIf
	                Else
	                	DbSelectarea("ZA4")
	                    ZA4->(DbSetorder(1))
	                    If ZA4->(DbSeek(xFilial("ZA4") + cValToChar((_cAliasSA1)->QtdeParcelas)))
	                    	cCondPag := ZA4->ZA4_COND
	                    EndIf
                    EndIf
                    
                                   
                    //CODIGO DO MUNICIPIO
                    cWSIBGE  := HttpGet("https://viacep.com.br/ws/" +AllTrim((_cAliasSA1)->CEP)+ "/piped/","",200)
                    nIBGEini := AT("ibge:", cWSIBGE)
                    cIBGE    := LEFT(SubStr( cWSIBGE, nIBGEini+7),5)
                    
       
                    //VERIFICA SE JA EXISTE NO CADASTRO DE CLIENTES
                    DbSelectarea("SA1")
                    SA1->(DbSetorder(03))
                    If SA1->(DbSeek(xFilial("SA1") + (_cAliasSA1)->CPF))
							
							
							SA1->(RecLock("SA1",.F.))
								SA1->A1_VEND   := cVend
								SA1->A1_COND   := cCondPag
								SA1->A1_TRANSP := cTransp
								SA1->A1_TPFRET := cTpFret
							SA1->(MsUnlock())
							
							
                    		ConOut("[RAKUTEN][CADSA1   ] - CLIENTE J� CADASTRADO! ATUALIZADO ALGUNS CAMPOS.")
                    		lRetCli := .T.
                    Else
                           //CADASTRA O CLIENTE
                           cCodCli  := LEFT( Alltrim((_cAliasSA1)->CPF),8)
                           cLojaCli := RIGHT(Alltrim((_cAliasSA1)->CPF),4)
       
                           aAdd(aValues, {"A1_TIPO"       ,"F"                                                                                           , Nil})
                           aAdd(aValues, {"A1_PESSOA"     ,"F"                                                                                           , Nil})
                           aAdd(aValues, {"A1_CGC"        ,Alltrim((_cAliasSA1)->CPF)                                                                    , Chr(9)})
						   aAdd(aValues, {"A1_COD"        ,cCodCli                                                                                       , Nil})
                           aAdd(aValues, {"A1_LOJA"       ,cLojaCli                                                                                      , Nil})
                           aAdd(aValues, {"A1_NOME"       ,UPPER( AllTrim((_cAliasSA1)->Nome) ) + " " + UPPER( AllTrim((_cAliasSA1)->SobreNome) )              , Nil})
                           aAdd(aValues, {"A1_NREDUZ"     ,UPPER( AllTrim((_cAliasSA1)->Nome) ) + " " + UPPER( AllTrim((_cAliasSA1)->SobreNome) )              , Nil}) 
                           aAdd(aValues, {"A1_END"        ,UPPER( Alltrim((_cAliasSA1)->Logradouro) +", "+ cValToChar( UPPER(Alltrim((_cAliasSA1)->Numero)) ) + " " + UPPER(Alltrim((_cAliasSA1)->Complemento)))  , Nil})
                           aAdd(aValues, {"A1_BAIRRO"     ,UPPER( Alltrim((_cAliasSA1)->Bairro ))                                                        , Nil})						   
                           aAdd(aValues, {"A1_EST"        ,UPPER( Alltrim((_cAliasSA1)->Estado) )                                                        , Nil})
                           aAdd(aValues, {"A1_CEP"        ,Alltrim((_cAliasSA1)->CEP)                                                                    , Nil})
                           aAdd(aValues, {"A1_COD_MUN"    ,Alltrim(cIBGE)                                                                                , Chr(9)})
                           aAdd(aValues, {"A1_MUN"        ,UPPER(Alltrim((_cAliasSA1)->Cidade))                                                          , Nil})
                           aAdd(aValues, {"A1_REGIAO"     ,_cRegiao                                                                                      , Nil})
                           DbSelectarea("SZV")
                           SZV->(DbSetorder(1))
                           If SZV->(DbSeek(xFilial("SZV") + _cRegiao))
                                  aAdd(aValues, {"A1_DSCREG"  ,SZV->ZV_REGIAO                                                                            , Nil})
                                  aAdd(aValues, {"A1_XSEDE"   ,SZV->ZV_SEDE                                                                              , Nil})                                       
                           EndIf
                           aAdd(aValues, {"A1_DDD"        ,IIf( Empty((_cAliasSA1)->Telefone1), Alltrim((_cAliasSA1)->DDDCelular), Alltrim((_cAliasSA1)->DDD1 ))           , Nil})
                           aAdd(aValues, {"A1_TEL"        ,IIf( Empty((_cAliasSA1)->Telefone1), Alltrim((_cAliasSA1)->Celular)   , Alltrim((_cAliasSA1)->Telefone1 ))      , Nil})						   
                           aAdd(aValues, {"A1_PAIS"        ,"105"                                                                                       , Nil})	
                           aAdd(aValues, {"A1_INSCR"      ,"ISENTO"                                                                                     , Nil})
                           aAdd(aValues, {"A1_NATUREZ"     ,cNatEco                                                                                     , Nil})						   
                           aAdd(aValues, {"A1_VEND"        ,cVend                                                                                       , Nil})
                           aAdd(aValues, {"A1_COND"        ,cCondPag                                                                                    , Nil})						   
                           aAdd(aValues, {"A1_TPESSOA"     ,"PF"                                                                                        , Nil})
                           aAdd(aValues, {"A1_XGRPEMP"     ,cGrpEmp                                                                                     , Nil}) //VERIFICAR
                           aAdd(aValues, {"A1_TRANSP"      ,cTransp                                                                                     , Nil})                                                                                                                         
                           aAdd(aValues, {"A1_XDESAG"     ,"N"                                                                                          , Nil}) //VERIFICAR
                           aAdd(aValues, {"A1_XPROMO"     ,"S"                                                                                          , Nil}) //VERIFICAR
                           aAdd(aValues, {"A1_CODPAIS"    ,"01058"                                                                                      , Nil})						   
                           aAdd(aValues, {"A1_EMAIL"      ,Alltrim((_cAliasSA1)->Email)                                                                 , Nil})						   
                           aAdd(aValues, {"A1_DTINCLU"    ,DtoC(Date())                                                                                 , Nil})
                           aAdd(aValues, {"A1_MSBLQL"     ,"2"                                                                                          , Nil})						   
                           aAdd(aValues, {"A1_GRPTRIB"    ,cGpTribE                                                                                     , Nil})						   
                           aAdd(aValues, {"A1_XCANAL"     ,"E"                                                                                          , Nil})
						   aAdd(aValues, {"A1_TABELA"     ,""                                                                                           , Nil}) //VERIFICAR
                           aAdd(aValues, {"A1_TPFRET"     ,cTpFret                                                                                      , Nil})
                           aAdd(aValues, {"A1_RISCO"      ,"A"                                                                                          , Nil})
                           aAdd(aValues, {"A1_CONTRIB"    ,"2"                                                                                          , Nil})
                           aAdd(aValues, {"A1_CONTA"      ,cContEco                                                                                     , Nil})
                           aAdd(aValues, {"A1_XCC"        ,cCEco                                                                                        , Nil})
                           aAdd(aValues, {"A1_XDIRET"     ,cDirEco                                                                                      , Nil})						   
                           //aAdd(aValues, {"A1_ENDCOB"     ,UPPER( Alltrim((_cAliasSA1)->Logradouro) +", "+ cValToChar( UPPER(Alltrim((_cAliasSA1)->Numero)) ) + " " + UPPER(Alltrim((_cAliasSA1)->Complemento)))  , Nil})
                           //aAdd(aValues, {"A1_BAIRROC"    ,UPPER( Alltrim((_cAliasSA1)->Bairro ))                                                        , Nil})						   
                           //aAdd(aValues, {"A1_ESTC"       ,UPPER( Alltrim((_cAliasSA1)->Estado) )                                                        , Nil})
                           //aAdd(aValues, {"A1_CEPC"       ,Alltrim((_cAliasSA1)->CEP)                                                                    , Nil})
                           //aAdd(aValues, {"A1_COD_MUN"    ,Alltrim(cIBGE)                                                                                , Chr(9)})
                           //aAdd(aValues, {"A1_MUNC"       ,UPPER(Alltrim((_cAliasSA1)->Cidade))                                                          , Nil})

                           
                           Begin Transaction
                                  MSExecAuto({|x,y| MATA030(x,y)}, aValues, nOpc)
                           End Transaction 
                           
                           If lMsErroAuto
                               cMosErr := MostraErro()
                               RollBackSX8()
	                           
	                           //ENVIA WORKFLOW
	                           U_WfCadSa1(cValToChar(cMosErr), cValToChar((_cAliasSA1)->NomeDestinatario), cValToChar((_cAliasSA1)->CPF), (_cAliasSA1)->PED, cPedRCli, cValToChar((_cAliasSA1)->Cidade), cValToChar((_cAliasSA1)->Estado), IIf( Empty((_cAliasSA1)->Telefone1), Alltrim((_cAliasSA1)->DDDCelular), Alltrim((_cAliasSA1)->DDD1) ), IIf( Empty((_cAliasSA1)->Telefone1), Alltrim((_cAliasSA1)->Celular), Alltrim((_cAliasSA1)->Telefone1) ))
	                       Else
	                           ConOut("[RAKUTEN][CADSA1   ] - CLIENTE: " + SA1->A1_COD + " LOJA: " + SA1->A1_LOJA + " CADASTRADO COM SUCESSO ")
	                           lRetCli := .T.	                           
                           EndIf
                    EndIf
                    
                    
                    (_cAliasSA1)->(dbSkip())
             End
             
             (_cAliasSA1)->(DbCloseArea())     
       
       Endif
       
       //FECHA CONEX�O COM O BANCO DA RAKUTEN
       TcUnlink( nHndRak )
       
       RestArea(aArea)
       RestArea(aAreaSA1)
       

Return(lRetCli)



/*
������������������������������������������������������������������������������
��� Programa  WfCadSa1    Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function WfCadSa1(cMensagem, cDescClie, cNumCPF, cNumPed, cPedRCli, cCidade, cEstado, cDDD, cPhone)
	
	Local cPara     := AllTrim(GetMv("ES_CSA1TO"))
	Local cArquivo  := "\MODELOS\CADSA1.HTM"
	Local cMailCC   := AllTrim(GetMv("ES_CSA1CC"))
	Local cMailBCC  := AllTrim(GetMv("ES_CSA1BCC"))
	Local cSaudacao := ""
	Local _oProcess := Nil
	Local lEnd      := .F.
	Local cCidEst   := Alltrim(cCidade) +"/"+ Alltrim(cEstado)
	Local cTelCel   := "(" +Alltrim(cDDD)+ ") "  +Alltrim(cPhone)
	Local cMsgInt   := "Voc� est� recebendo um aviso de erro na integra��o do cadastro de cliente onde as informa��es se encontram abaixo. Havendo d�vidas, favor entrar em contato com o setor de TI - (11) 3123-1635"
	
	Private lMsErroAuto := .F.
	
	
	If Time() > "18:00:00"
		cSaudacao := "Boa noite"
	ElseIf Time() > "12:00:00"
		cSaudacao := "Boa tarde"
	Else
		cSaudacao := "Bom dia"
	Endif
	
	oProcess := TWFProcess():New("WfCadSa1","WF CADASTRO CLIENTE")
	oProcess:NewTask("Erro de Integra��o No Cadastro de Cliente",cArquivo)
	oHTML := oProcess:oHTML
	
	oProcess:cSubject := "[RAKUTEN] - Erro de Integra��o - CPF: " +Transform(cNumCPF , "@R 999.999.999-99")
	oProcess:USerSiga := "000000"
	
	oHtml:ValByName("cSaudacao", cSaudacao)
	oHtml:ValByName("cMsgInt"  , cMsgInt)
	oHtml:ValByName("cMensagem", cMensagem)
	oHtml:ValByName("cDescClie", UPPER(cDescClie))
	oHtml:ValByName("cNumCPF"  , Transform(cNumCPF , "@R 999.999.999-99"))
	oHtml:ValByName("cNumPed"  , cNumPed)
	oHtml:ValByName("cPedCli"  , cPedRCli)
	oHtml:ValByName("cCidEst"  , UPPER(cCidEst))
	oHtml:ValByName("cTelCel"  , cTelCel)
	
	oProcess:cTo      := cPara
	oProcess:cBCC     := cMailBCC
	oProcess:cCC      := cMailCC
	oProcess:Start()
	oProcess:Finish()
	

Return



/*
������������������������������������������������������������������������������
��� Programa  INTPEDIDO   Autor  Rogerio Machado         Data 03/07/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/
 
User Function INTPEDIDO(nPedWeb, nPedID, cCPF, cCondPg, _nFrete, nDesPed, _nPrese, _cTpOper)

	Local _cAliasC6		:= ""
	Local lRetPed		:= .T.
	Local aCabec 		:= {}
	Local aItens 		:= {}
	Local aLinha 		:= {}
//	Local _cTpOper      := "01"
	Local dDtEmiss      := date()
	Local cPedNew       := ""
	Local cItemPV       := ""
	Local _cMosErr      := ""
	Local cDBRak	    := "MSSQL/INTEGRATIONQUEUE"
	Local cSrvRak	    := "172.16.0.97"
	Local nPortaDBA     := 5010
	Local nHndRak	    := 0
	Local nHndERP       := 0
	Local nVlrKit       := 0
	Local nPrcVen       := 0
	Local nPesoTot      := 0
	Local aIPI          := {}
	Local aPesItem      := {}
	Local aPercFr       := {}
	Local aFreRat       := {}
	Local nFreRat       := 0

	Local aPerDes       := {}
	Local aDesRat       := {}
	Local nDesRat       := 0
	Local _cAliasRKT    := ""
	Local nVlrImp       := ""
	Local cAdmin        := ""
	Local cArmaz        := GetMv("ES_ARECOMM")
	Local X,Y,Z

	
	Private lMsErroAuto := .F.
	
	
	cPedNew       := GetSx8Num("SC5", "C5_NUM")
	cItemPV       := StrZero(0, Len(SC6->C6_ITEM))
	nHndERP       := AdvConnection()
	_cAliasC6	  := GetNextAlias()
	_cAliasRKT    := GetNextAlias()
	
	//FAZ CONEX�O COM O BANCO DA RAKUTEN
	nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
	
	If nHndRak < 0
		ConOut("[RAKUTEN][INTPEDIDO] - ERRO (" +cValToChar(nHndRak)+ ") AO CONECTAR COM " +cDBRak+ " EM " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " AS " +Time()+ " " +DtoC(Date()))
		Return
	Else
		ConOut("[RAKUTEN][INTPEDIDO] - CONECTADO NO SERVIDOR " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " COM " +cDBRak+ " AS " +Time()+ " " +DtoC(Date()))
	
	
		BeginSQL Alias _cAliasC6
			SELECT ItemPartNumber, ItemQtde, ItemValorFinal, ItemValor  FROM RK_RECEPCAO_PEDIDO_ITEM
			WHERE PedidoID = %exp:nPedID%
		EndSQL
		
		
		BeginSQL Alias _cAliasRKT
			SELECT
				PedidoCodigoCliente AS PEDCLI,
				PedidoCodigo AS PED, 
				RKPED.ID AS ID, 
				CPF AS CPF, 
				ServicoEntregaCodigo AS SRVENTCOD, 
				QtdeParcelas AS QTDPARC,
				ValorFreteCobrado AS VLRFRT, 
				ISNULL( (SELECT DISTINCT RKITEM.Presente FROM RK_RECEPCAO_PEDIDO_ITEM AS RKITEM WHERE RKPED.ID = RKITEM.PedidoID AND RKITEM.Presente = 1) ,0) AS Presente, 
				SUBSTRING(CarOperadoraRetornoMensagem, CHARINDEX('||', CarOperadoraRetornoMensagem)+2, 100) AS NSU, CarAutorizacaoCodigo AS CARAUTORIZ, RKPED.* 
			FROM RK_RECEPCAO_PEDIDO AS RKPED
			LEFT JOIN RK_RECEPCAO_PEDIDO_CARTAO AS RKCARD ON RKPED.ID = RKCARD.PedidoID
			WHERE  PedidoCodigo = %exp:nPedWeb% AND 
			PedidoCodigo NOT IN (SELECT PedidoCodigo FROM RK_RECEPCAO_PEDIDO WHERE PedidoStatus = 5 ) 
			AND DataEnvioProtheus IS NULL
			AND pedidostatus = 7
		EndSQL		
		
		//VOLTA CONEX�O PARA O BANCO DO PROTHEUS
		TcSetConn( nHndERP )
	
		nPedidoW := nPedWeb
		
		//CODIGO DO MUNICIPIO
		cWSIBGE  := HttpGet("https://viacep.com.br/ws/" +AllTrim((_cAliasRKT)->CEP)+ "/piped/","",200)
		nIBGEini := AT("ibge:", cWSIBGE)
		cIBGE    := LEFT(SubStr( cWSIBGE, nIBGEini+7),5)		
		
		DbSelectArea("SC5")
		DbSetOrder(1)
	
		While SC5->(DbSeek(xFilial("SC5") + cPedNew))
			ConfirmSx8()
			cPedNew := GetSx8Num("SC5", "C5_NUM")
		Enddo
		
		DbSelectArea("SA1")
		DbSetOrder(3)
		If SA1->(DbSeek(xFilial("SA1") + AllTrim(cCPF)))
		
		
			aItens := {}
			While (_cAliasC6)->(!EoF())
				DbSelectArea("ZB2")
				DbSetOrder(1)
				If ZB2->(DbSeek(xFilial("ZB2") + Alltrim((_cAliasC6)->ItemPartNumber))) //SE FOR KIT
					While ZB2->(!EoF()) .And. Alltrim(ZB2->ZB2_COD) == Alltrim((_cAliasC6)->ItemPartNumber)
						DbSelectArea("SB1")
						DbSetOrder(1)
						SB1->(DbSeek(xFilial("SB1") + ZB2->ZB2_COMP))

						DbSelectArea("SBZ")
						DbSetOrder(1)
						SBZ->(DbSeek(xFilial("SBZ") + ZB2->ZB2_COMP))						
						
						nVlrKit      := ( ( ( (_cAliasC6)->ItemValorFinal * (ZB2->ZB2_VALITE/ZB2->ZB2_VALTOT) ) / (SB1->B1_IPI+100) ) *100 )
						//nVlrKit      := ( ( ( ( (_cAliasC6)->ItemValorFinal * (ZB2->ZB2_VALITE/ZB2->ZB2_VALTOT) ) * (_cAliasC6)->ItemQtde) / (SB1->B1_IPI+100) ) *100 ) / (_cAliasC6)->ItemQtde) 
						
						//FRETE
						If _nFrete > 0
							nPesoTot += (_cAliasC6)->ItemQtde * SB1->B1_PESO
							AAdd(aPesItem,(_cAliasC6)->ItemQtde * SB1->B1_PESO)
							AAdd(aIPI    ,SBZ->BZ_IPI)
						EndIf
						//FRETE

												
						cItemPV := Soma1(cItemPV)
						aAdd(aItens, {	{'C6_ITEM'	, cItemPV				                                                                      , NIL},;
													{'C6_PRODUTO'	, SB1->B1_COD		    	                                                  , NIL},;
													{'C6_LOCAL'		, cArmaz         		                                                      , NIL},;
													{'C6_QTDVEN'	, ZB2->ZB2_QUANT * (_cAliasC6)->ItemQtde   	                                  , NIL},;
													{'C6_QTDLIB'	, ZB2->ZB2_QUANT * (_cAliasC6)->ItemQtde   	                                  , NIL},;
													{'C6_PRCVEN'	, IIF(nVlrKit-(nVlrKit*nDesPed) == 0, 0.01, nVlrKit-(nVlrKit*nDesPed))        , NIL},;
													{'C6_XKIT'	    , ZB2->ZB2_COD                                                                , NIL},;							
													{'C6_OPER'		, _cTpOper 				                                                      , NIL} })
						If Len(aItens) > 0
							aAdd(aItens[Len(aItens)], {'C6_COMIS1'	, 0                     	, NIL})
						EndIf
						ZB2->(dbSkip())					 
					End
				Else
					DbSelectArea("SB1")
					DbSetOrder(1)
					SB1->(DbSeek(xFilial("SB1") + Alltrim((_cAliasC6)->ItemPartNumber)))

					DbSelectArea("SBZ")
					DbSetOrder(1)
					SBZ->(DbSeek(xFilial("SBZ") + Alltrim((_cAliasC6)->ItemPartNumber)))
					

					nPrcVen := IIf (SBZ->BZ_IPI > 0, (_cAliasC6)->ItemValorFinal/((100+SBZ->BZ_IPI)/100), (_cAliasC6)->ItemValorFinal)
					
					//FRETE
					If _nFrete > 0
						nPesoTot += (_cAliasC6)->ItemQtde * SB1->B1_PESO
						AAdd(aPesItem,(_cAliasC6)->ItemQtde * SB1->B1_PESO)
						AAdd(aIPI    ,SBZ->BZ_IPI)
					EndIf
					//FRETE
					
					
					cItemPV := Soma1(cItemPV)
					aAdd(aItens, {	{'C6_ITEM'	, cItemPV				                                                                       , NIL},;
												{'C6_PRODUTO'	, SB1->B1_COD		    	                                                   , NIL},;
												{'C6_LOCAL'		, cArmaz        		                                                       , NIL},;
												{'C6_QTDVEN'	, (_cAliasC6)->ItemQtde	                                                       , NIL},;
												{'C6_QTDLIB'	, (_cAliasC6)->ItemQtde	                                                       , NIL},;
												{'C6_PRCVEN'	, IIF(nPrcVen-(nPrcVen*nDesPed) == 0, 0.01, nPrcVen-(nPrcVen*nDesPed))         , NIL},;							
												{'C6_OPER'		, _cTpOper 				                                                       , NIL} })
					If Len(aItens) > 0
						aAdd(aItens[Len(aItens)], {'C6_COMIS1'	, 0                     	, NIL})
					EndIf
				
				EndIf
			
		
				(_cAliasC6)->(dbSkip())
			End
			
			//nPrcVen := IIf (SBZ->BZ_IPI > 0, nPrcVen/((100+SBZ->BZ_IPI)/100), nPrcVen)
			
			//FRETE
			If _nFrete > 0
				For X := 1 To LEN(aPesItem)
					AAdd(aPercFr, aPesItem[X]/nPesoTot)
				Next
				
				For Y := 1 To LEN(aPercFr)
					AAdd(aFreRat, aPercFr[Y]*_nFrete)
				Next
				
				For Z := 1 To LEN(aIPI)
					nFreRat += aFreRat[Z]/(1+aIPI[Z]/100)
				Next
			EndIf
			//FRETE

/*
			//PRESENTE
			If _nPrese > 0
				For A := 1 To LEN(aPesItem)
					AAdd(aPerDes, aPesItem[A]/nPesoTot)
				Next
				
				For B := 1 To LEN(aPerDes)
					AAdd(aDesRat, aPerDes[B]*_nPrese)
				Next
				
				For C := 1 To LEN(aIPI)
					nDesRat += aDesRat[C]/(1+aIPI[C]/100)
				Next
			EndIf
			//PRESENTE
*/
			
			DbSelectArea("ZA5")
			DbSetOrder(1)
			If ZA5->(DbSeek(xFilial("ZA5") + CValToChar((_cAliasRKT)->FormaPagto)))
				cAdmin := Alltrim(ZA5->ZA5_DESC)
				DbSelectarea("SAE")
				SAE->(DbSetorder(1))
				SAE->(DbSeek(xFilial("SAE") + ZA5->ZA5_ADMIN))			
			EndIf			
			
			aAdd(aCabec, {'C5_NUM'	    , cPedNew    									, NIL})
			aAdd(aCabec, {'C5_TIPO'	    , "N"											, NIL})
			aAdd(aCabec, {'C5_CLIENTE'	, SA1->A1_COD    								, NIL})
			aAdd(aCabec, {'C5_LOJACLI'	, SA1->A1_LOJA	    							, NIL})	
			aAdd(aCabec, {'C5_EMISSAO'	, dDtEmiss										, NIL})
			aAdd(aCabec, {'C5_CONDPAG'	, SA1->A1_COND	    							, NIL})
			aAdd(aCabec, {'C5_TRANSP'	, SA1->A1_TRANSP								, NIL})
			aAdd(aCabec, {'C5_TPFRETE'	, SA1->A1_TPFRET        						, NIL})
			aAdd(aCabec, {'C5_FRETE'	, IIF(_cTpOper == "16",0,nFreRat)               , NIL})
			aAdd(aCabec, {'C5_DESPESA'	, IIF(_cTpOper == "16",0,_nPrese)               , NIL})
			aAdd(aCabec, {'C5_MENNOTA'	, ""                                            , NIL})
			aAdd(aCabec, {'C5_XPEDRAK'	, VAL(nPedidoW)                                 , NIL})
			aAdd(aCabec, {'C5_XORIGEM'  , "R"                                           , NIL})
			aAdd(aCabec, {'C5_COMIS1'   , 0                                             , NIL})
			aAdd(aCabec, {'C5_XDESRAK'  , nDesPed                                       , NIL})
			aAdd(aCabec, {'C5_XPCLIRK'  , VAL((_cAliasRKT)->PEDCLI)                     , NIL})
			aAdd(aCabec, {'C5_XEND'     , UPPER( Alltrim((_cAliasRKT)->Logradouro) +", "+ cValToChar( UPPER(Alltrim((_cAliasRKT)->Numero)) ) + " " + UPPER(Alltrim((_cAliasRKT)->Complemento)))  , NIL})
			aAdd(aCabec, {'C5_XBAIRRO'  , UPPER( (_cAliasRKT)->Bairro )                 , NIL})
			aAdd(aCabec, {'C5_XCEP'     , (_cAliasRKT)->CEP                             , NIL})
			aAdd(aCabec, {'C5_XEST   '  , (_cAliasRKT)->Estado                			, NIL})
			aAdd(aCabec, {'C5_XCODMUN'  , cIBGE                               			, NIL})
			aAdd(aCabec, {'C5_XMUN'     , UPPER((_cAliasRKT)->Cidade)         			, NIL})
			aAdd(aCabec, {'C5_VEND1'    , SA1->A1_VEND                        			, NIL})
			aAdd(aCabec, {'C5_XDTINT'   , dDtEmiss                            			, NIL})
			aAdd(aCabec, {'C5_XHRINT'   , TIME()                              			, NIL})
			aAdd(aCabec, {'C5_XCANAL'   , "E"                                 			, NIL})
			aAdd(aCabec, {'C5_XADMIN'   , cAdmin                              			, NIL})
			aAdd(aCabec, {'C5_XPZENT'   , (_cAliasRKT)->PrazoEntreg           			, NIL})
			aAdd(aCabec, {'C5_XQTDPAR'  , cValToChar((_cAliasRKT)->QTDPARC)  		 	, NIL})
			aAdd(aCabec, {'C5_XSRVENT'  , cValToChar((_cAliasRKT)->SRVENTCOD) 			, NIL})
			aAdd(aCabec, {'C5_XNSU'     , cValToChar((_cAliasRKT)->NSU) 			    , NIL})
			aAdd(aCabec, {'C5_XCARAUT'  , cValToChar((_cAliasRKT)->CARAUTORIZ) 	        , NIL})
			aAdd(aCabec, {'C5_XOPERAD'  , If((_cAliasRKT)->Pagamento == 3,"BOLETO","REDECARD SA" )    , NIL})
			aAdd(aCabec, {'C5_XTIPO'    , SAE->AE_TIPO                      	        , NIL})

				
			lMSErroAuto := .F.
			Begin Transaction
				MSExecAuto({|x, y, z| MATA410(x, y, z)}, aCabec, aItens, 3) 
			End Transaction
			
			If lMsErroAuto
				lRetPed	:= .F.
				_cMosErr := MostraErro()
				U_WfIntPed(cValToChar(_cMosErr), Alltrim(SA1->A1_NOME), Alltrim(SA1->A1_CGC), cPedNew, cValToChar(nPedidoW), SA1->A1_MUN, SA1->A1_EST, SA1->A1_DDD, SA1->A1_TEL)
			Else
				ConfirmSX8()
				ConOut("[RAKUTEN][INTPEDIDO] - PEDIDO " +cPedNew+ " CADASTRADO COM SUCESSO ")
				//nVlrImp := U_CalcImp(cPedNew) 
				//ConOut("[RAKUTEN][INTPEDIDO] - VALOR TOTAL COM IMPOSTOS DO PEDIDO " + cValToChar( nVlrImp ) )
				lRetPed	:= .T.
			EndIf	

/*			
			//AJUSTE DE VALOR
		    If (_cAliasRKT)->ValorTotal <> nVlrImp
		    	nDifer := (_cAliasRKT)->ValorTotal - nVlrImp
		    	//If nDifer <= 2
		    		DbSelectarea("SC6")
					SC6->(DbSetorder(1))
					SC6->(DbSeek(xFilial("SC6") + cPedNew))
					
					ConOut("[RAKUTEN] " + cValToChar(SC6->C6_PRCVEN))
					SC6->(RecLock("SC6",.F.))
						ConOut("[RAKUTEN] " + SC6->C6_PRODUTO)
						SC6->C6_PRCVEN := SC6->C6_PRCVEN + nDifer
						SC6->C6_VALOR  := (SC6->C6_PRCVEN + nDifer) * SC6->C6_QTDVEN
					SC6->(MsUnlock())
					ConOut("[RAKUTEN] " + cValToChar(SC6->C6_PRCVEN))	
		    	//EndIf
		    	ConOut("[RAKUTEN][INTPEDIDO] - DIFEREN�A PROTHEUS E RAKUTEN " + cValToChar( nDifer ) )
		    EndIf
*/

			
			(_cAliasC6)->(DbCloseArea())
		EndIf
	Endif
	
	
Return(lRetPed)



/*
������������������������������������������������������������������������������
��� Programa  WfIntPed    Autor  Rogerio Machado         Data 03/07/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/

User Function WfIntPed(cMensagem, cDescClie, cNumCPF, cNumPed, cPedRCli, cCidade, cEstado, cDDD, cPhone)
	
	Local cPara     := AllTrim(GetMv("ES_CSC5TO"))
	Local cArquivo  := "\MODELOS\PEDRAKUTEN.HTM"
	Local cMailCC   := AllTrim(GetMv("ES_CSC5CC"))
	Local cMailBCC  := AllTrim(GetMv("ES_CSC5BCC"))
	Local cSaudacao := ""
	Local _oProcess := Nil
	Local lEnd      := .F.
	Local cCidEst   := Alltrim(cCidade) +"/"+ Alltrim(cEstado)
	Local cTelCel   := "(" +Alltrim(cDDD)+ ") "  +Alltrim(cPhone)
	Local cMsgInt   := "Voc� est� recebendo um aviso de erro na integra��o do Pedido de Vendas onde as informa��es se encontram abaixo. Havendo d�vidas, favor entrar em contato com o setor de TI - (11) 3123-1635"
	
	Private lMsErroAuto := .F.
	
	
	If Time() > "18:00:00"
		cSaudacao := "Boa noite"
	ElseIf Time() > "12:00:00"
		cSaudacao := "Boa tarde"
	Else
		cSaudacao := "Bom dia"
	Endif
	
	oProcess := TWFProcess():New("WfIntPed","WF PEDIDO DE VENDA")
	oProcess:NewTask("Erro de Integra��o Do Pedido de Venda",cArquivo)
	oHTML := oProcess:oHTML
	
	oProcess:cSubject := "[RAKUTEN] - Erro de Integra��o - CPF: " +Transform(cNumCPF , "@R 999.999.999-99")
	oProcess:USerSiga := "000000"
	
	oHtml:ValByName("cSaudacao", cSaudacao)
	oHtml:ValByName("cMsgInt"  , cMsgInt)
	oHtml:ValByName("cMensagem", cMensagem)
	oHtml:ValByName("cDescClie", UPPER(cDescClie))
	oHtml:ValByName("cNumCPF"  , Transform(cNumCPF , "@R 999.999.999-99"))
	oHtml:ValByName("cNumPed"  , cNumPed)
	oHtml:ValByName("cPedCli"  , cPedRCli)
	oHtml:ValByName("cCidEst"  , UPPER(cCidEst))
	oHtml:ValByName("cTelCel"  , cTelCel)
	
	oProcess:cTo      := cPara
	oProcess:cBCC     := cMailBCC
	oProcess:cCC      := cMailCC
	oProcess:Start()
	oProcess:Finish()
	
Return












/*
������������������������������������������������������������������������������
��� Programa  STNLOJA701   Autor  Rogerio Machado         Data 13/06/2018   ���
��� Desc.                                                                  ���
������������������������������������������������������������������������������
*/
 


/*
User Function STNLOJA701(nPedWeb, nPedID, nValTot, cPagto)
	Local cDBRak	   := "MSSQL/INTEGRATIONQUEUE"
	Local cSrvRak	   := "172.16.0.97"
	Local nPortaDBA    := 5010
	Local nHndRak	   := 0
	Local _cAliasCAB   := GetNextAlias()
	Local _cAliasITM   := GetNextAlias()
	Local _cAliasSC5   := GetNextAlias()
	Local _cAliasSC6   := GetNextAlias()	
	Local nHndERP      := AdvConnection()
	LocaL nValTot      := 0
	Local lRetLoja     := .T.
	Local _cEmpresa    := "01"        //Codigo da Empresa que deseja incluir o orcamento
	Local _cFilial     := "01010001"  //Codigo da Filial que deseja incluir o orcamento
	
	Local nTamProd     := 0
	Local nTamUM       := 0
	Local nTamTabela   := 0
	
	Local _aCab         := {} //Array do Cabe�alho do Or�amento
	Local _aItem        := {} //Array dos Itens do Or�amento
	Local _aParcela     := {} //Array das Parcelas do Or�amento
	
	Private lMsHelpAuto := .T. //Variavel de controle interno do ExecAuto
	Private lMsErroAuto := .F. //Variavel que informa a ocorr�ncia de erros no ExecAuto
	Private INCLUI      := .T. //Variavel necess�ria para o ExecAuto identificar que se trata de uma inclus�o
	Private ALTERA      := .F. //Variavel necess�ria para o ExecAuto identificar que se trata de uma altera��o
	
	//PREPARE ENVIRONMENT EMPRESA _cEmpresa FILIAL _cFilial MODULO "LOJA"
	
	//Indica inclus�o
	lMsHelpAuto := .T.
	lMsErroAuto := .F.
		
	
	_cAliasCAB   := GetNextAlias()
	_cAliasITM   := GetNextAlias()
	_cAliasSC5   := GetNextAlias()
	_cAliasSC6   := GetNextAlias()
	nHndERP      := AdvConnection()
	

	//Faz conex�o com o banco da Rakuten
	nHndRak := TcLink(cDBRak,cSrvRak,nPortaDBA)
	
	If nHndRak < 0
		ConOut("[RAKUTEN][STNLOJA701] - ERRO (" +cValToChar(nHndRak)+ ") AO CONECTAR COM " +cDBRak+ " EM " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " AS " +Time()+ " " +DtoC(Date()))
		Return
	Else
		ConOut("[RAKUTEN][STNLOJA701] - CONECTADO NO SERVIDOR " +cSrvRak+ ":" +cValToChar(nPortaDBA)+ " COM " +cDBRak+ " AS " +Time()+ " " +DtoC(Date()))
	
		
		//Volta conex�o para o banco do Protheus
		TcSetConn( nHndERP )
		
		
		BeginSQL Alias _cAliasSC5
			SELECT * FROM SC5010 WHERE %NotDel% AND C5_FILIAL = %Exp:xFilial("SC5")% AND C5_XPEDRAK = %Exp:nPedWeb%
		EndSQL
		
		DbSelectArea(_cAliasSC5)
		(_cAliasSC5)->(dbGoTop())
		
			
		DbSelectarea("SC5")
		SC5->(DbSetorder(1))
		SC5->(DbSeek(xFilial("SC5") + Alltrim( (_cAliasSC5)->C5_NUM )) )		

		DbSelectarea("SC6")
		SC6->(DbSetorder(1))
		SC6->(DbSeek(xFilial("SC6") + Alltrim( (_cAliasSC5)->C5_NUM )) )		
		
		DbSelectarea("SA1")
		SA1->(DbSetorder(1))
		SA1->(DbSeek(xFilial("SA1") + SC5->C5_CLIENTE + SC5->C5_LOJACLI))

		
		//Retorna o tamanho dos campos
		nTamProd   := TamSX3("LR_PRODUTO")[1]
		nTamUM     := TamSX3("LR_UM")[1]
		nTamTabela := TamSX3("LR_TABELA")[1]

		
		//Monta cabe�alho do or�amento (SLQ)
		aAdd( _aCab, {"LQ_VEND"    , PadR(SC5->C5_VEND1,TamSX3("A3_COD")[1])   , NIL} )
		aAdd( _aCab, {"LQ_COMIS"   , SC5->C5_COMIS1  , NIL} )
		aAdd( _aCab, {"LQ_CLIENTE" , SA1->A1_COD     , NIL} )
		aAdd( _aCab, {"LQ_LOJA"    , SA1->A1_LOJA    , NIL} )
		aAdd( _aCab, {"LQ_TIPOCLI" , SA1->A1_TIPO    , NIL} )
		aAdd( _aCab, {"LQ_DESCONT" , 0               , NIL} )
		aAdd( _aCab, {"LQ_DTLIM"   , dDatabase       , NIL} )
		aAdd( _aCab, {"LQ_EMISSAO" , dDatabase       , NIL} )
		aAdd( _aCab, {"LQ_CONDPG"  , SC5->C5_CONDPAG , NIL} )
		aAdd( _aCab, {"LQ_NUMMOV"  , "1 "            , NIL} )
		aAdd( _aCab, {"LQ_PEDRES"  , SC5->C5_NUM     , NIL} )
		aAdd( _aCab, {"LQ_TIPO"    , "V"             , NIL} )
		aAdd( _aCab, {"LQ_SITUA"   , "OK"            , NIL} )
		aAdd( _aCab, {"LQ_IMPRIME" , "2S"            , NIL} )
		aAdd( _aCab, {"LQ_IMPNF"   , "T"             , NIL} )
		aAdd( _aCab, {"LQ_OPERADO" , "C02"           , NIL} )

	
		While SC6->(!EoF()) .And. SC6->C6_NUM == SC5->C5_NUM
			DbSelectarea("SB1")
			SB1->(DbSetorder(1))
			If SB1->(DbSeek(xFilial("SB1") + SC6->C6_PRODUTO))
			
				aAdd( _aItem, {} )
				aAdd( _aItem[Len(_aItem)], {"LR_PRODUTO", ALLTRIM(SB1->B1_COD)                  , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_QUANT"  , SC6->C6_QTDVEN                        , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_UM"     , SB1->B1_UM                            , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_DESCRI" , ALLTRIM(SB1->B1_DESC)                 , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_VALDESC", 0                                     , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_PEDRES" , SC5->C5_NUM                           , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_TABELA" , ""                                    , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_DESCPRO", 0                                     , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_VEND"   , SC5->C5_VEND1                         , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_TIPO"   , "V"                                   , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_ENTREGA", "3"                                   , NIL} )
				aAdd( _aItem[Len(_aItem)], {"LR_FDTENTR", dDatabase                             , NIL} )
			
			EndIf
			
			SC6->(dbSkip())
		End

		DbSelectarea("ZA5")
		ZA5->(DbSetorder(1))
		If ZA5->(DbSeek(xFilial("ZA5") + cPagto))
			DbSelectarea("SAE")
			SAE->(DbSetorder(1))
			SAE->(DbSeek(xFilial("SAE") + ZA5->ZA5_ADMIN))			
			_cAdmin := ZA5->ZA5_ADMIN
		EndIf

		//Monta o cabe�alho do or�amento (aPagtos)
		aAdd( _aParcela, {} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_DATA"    , dDatabase                                , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_VALOR"   , nValTot                                  , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_FORMA"   , ALLTRIM(SAE->AE_TIPO)                    , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_ADMINIS" , SAE->AE_COD + " - " + SAE->AE_DESC       , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_NUMCART" , " "                                      , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_FORMAID" , " "                                      , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_MOEDA"   , 1                                        , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_NSUTEF"  , "144777888"                              , NIL} )
		aAdd( _aParcela[Len(_aParcela)], {"L4_DATATEF" , dDatabase                                , NIL} )		
	
		SetFunName("LOJA701")
		MSExecAuto({|a,b,c,d,e,f,g,h| Loja701(a,b,c,d,e,f,g,h)},.F.,3,"","",{},_aCab,_aItem ,_aParcela)
		
		If lMsErroAuto
			lRetLoja := .F.
			ConOut("[RAKUTEN][STNLOJA701] - Erro no ExecAuto")
			MostraErro() 
			DisarmTransaction() // Libera sequencial RollBackSx8() 
		Else 
			ConOut("[RAKUTEN][STNLOJA701] - Sucesso na execu��o do ExecAuto")
		EndIf
	Endif
		
	//RESET ENVIRONMENT
	
Return(lRetLoja)
*/
