#include 'protheus.ch'
#INCLUDE "FWMVCDEF.CH"


User Function ADMOREGIAO()

	Local aArea     := GetArea()
	Private oBrowse := FwMBrowse():New()

	oBrowse:SetAlias('ZA8')
	oBrowse:SetMenudef('ADMOREGIAO')

	//Descri��o da Parte Superior Esquerda do Browse
	oBrowse:SetDescripton("Cadastro de Regi�es por CEP ADMO")

	//Desabilita os Detalhes da parte inferior do Browse
	oBrowse:DisableDetails()

	//Ativa o Browse
	oBrowse:Activate()
	
	RestArea(aArea)

Return


/*===========================================================================================================*/


Static Function MenuDef()

	Local aRotina := {}

	ADD OPTION aRotina TITLE 'Visualizar' ACTION 'VIEWDEF.ADMOREGIAO' OPERATION 2 ACCESS 0
	ADD OPTION aRotina TITLE 'Incluir'    ACTION 'VIEWDEF.ADMOREGIAO' OPERATION 3 ACCESS 0
	ADD OPTION aRotina TITLE 'Alterar'    ACTION 'VIEWDEF.ADMOREGIAO' OPERATION 4 ACCESS 0
	ADD OPTION aRotina TITLE 'Excluir'    ACTION 'VIEWDEF.ADMOREGIAO' OPERATION 5 ACCESS 0
	ADD OPTION aRotina TITLE 'Imprimir'   ACTION 'VIEWDEF.ADMOREGIAO' OPERATION 8 ACCESS 0

Return aRotina


/*===========================================================================================================*/


Static Function ModelDef()

	Local oModel    := Nil
	Local oStPai    := FWFormStruct(1,'ZA8', /*bAvalCampo*/, /*lViewUsado*/) //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
	Local oStFilho  := FWFormStruct(1,'ZA9', /*bAvalCampo*/, /*lViewUsado*/) //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
	Local aZA9Rel   := {}

	//Instancia do Objeto de Modelo de Dados
	oModel := MpFormModel():New('MDADMOREGIAO') //,,, {|oModel| GrvDados(oModel)})
	oModel:AddFields('PAI_ADMOREGIAO', /*cOwner*/, oStPai)
	oModel:AddGrid('FILHO_ADMOREGIAO', 'PAI_ADMOREGIAO', oStFilho)

	//Fazendo o relacionamento entre o Pai e Filho
    oModel:SetRelation('FILHO_ADMOREGIAO', {{'ZA9_FILIAL', 'ZA8_FILIAL'}, {'ZA9_COD', 'ZA8_COD'}})

    //oModel:SetRelation('FILHO_ADMOREGIAO', aZA9Rel, ZA8->(IndexKey(1))) //IndexKey -> quero a ordena��o e depois filtrado
    //oModel:GetModel('PAI_ADMOREGIAO'):SetUniqueLine({"ZA8_COD"})    //N�o repetir informa��es ou combina��es {"CAMPO1","CAMPO2","CAMPOX"}
    oModel:SetPrimaryKey({})

    //Setando as descri��es
    oModel:SetDescription("Cadastro de Regi�es por CEP ADMO")
    oModel:GetModel('PAI_ADMOREGIAO'):SetDescription('Cabe�alho - Cadastro de Regi�es ADMO')
    oModel:GetModel('FILHO_ADMOREGIAO'):SetDescription('Itens - Cadastro de Regi�es ADMO')



Return(oModel)


/*===========================================================================================================*/


Static Function ViewDef()

    Local oView         := Nil
    Local oModel        := FWLoadModel('ADMOREGIAO')
    Local oStPai        := FWFormStruct(2, 'ZA8')
    Local oStFilho      := FWFormStruct(2, 'ZA9')

    
    //Criando a View
    oView := FWFormView():New()
    oView:SetModel(oModel)

    //Retira um campo da Estrutura da View
	oStFilho:RemoveField('ZA9_COD')

    //Adicionando os campos do cabe�alho e o grid dos filhos
    oView:AddField('VIEW_PAI',oStPai,'PAI_ADMOREGIAO')
    oView:AddGrid('VIEW_FILHO',oStFilho,'FILHO_ADMOREGIAO')
    

    //Setando o dimensionamento de tamanho
    oView:CreateHorizontalBox('CABEC',30)
    oView:CreateHorizontalBox('GRID',70)

    //Amarrando a view com as box
    oView:SetOwnerView('VIEW_PAI','CABEC')
    oView:SetOwnerView('VIEW_FILHO','GRID')
    
	oView:EnableTitleView('VIEW_FILHO' , 'Itens - Cadastro de Regi�es por CEP ADMO' )
	oView:EnableTitleView('VIEW_PAI'   , 'Cabe�alho - Cadastro de Regi�es por CEP ADMO' )



    //Habilitando t�tulo
    oView:SetCloseOnOk({ || .T. })

Return oView


/*===========================================================================================================*/


/*
FUNCAO DE GRAVACAO DOS DADOS

Static Function GrvDados(oModel)
Local nCountA := 0
LocAL nTotal  := 0

//-- GRAVA TODO FORMULARIO ATUAL
lRet := FwFormCommit(oModel)


Return(lRet)

*/
