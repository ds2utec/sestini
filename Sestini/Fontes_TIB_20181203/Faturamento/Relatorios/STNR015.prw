#INCLUDE "PROTHEUS.CH"
#INCLUDE "TbiConn.ch"
#INCLUDE "TOTVS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � STNR015�   Autor � Rogerio Machado    � Data �  12/09/2018 ���
�������������������������������������������������������������������������͹��
���Desc.     � Programa para gerar relatorio de Metas                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function STNR015()                  

	Local aDados  := {}
	
	
	Local aArea			:= GetArea()
	Local oExcel    	:= FWMSEXCEL():New()
	Local cDir      	:= GetSrvProfString("Startpath","")
	Local cDirTmp       := GetTempPath()
	Local   cPerg       := "RELMETAS" 
	
	Private cCadastro   := "Gerar Excel"
	Private cNameW      := "REL. METAS"
	Private cAliasAna 	:= GetNextAlias()
	Private cAliasSin 	:= GetNextAlias()   
	
	//U_AjustaSx1()   
	
	Pergunte(cPerg,.T.)
	
	BEGIN SEQUENCE
	

	
		oExcel:AddworkSheet(cNameW)
		                                                  
		oExcel:AddTable (cNameW,cNameW)
			
		If MV_PAR34 == 1
		
			BeginSql alias cAliasAna                                  	
				SELECT DISTINCT D2_FILIAL AS Filial, F2_VEND1 AS Vendedor, A3_NOME AS Nome_Vendedor, D2_CLIENTE AS Cliente, D2_LOJA AS Loja, A1_NOME AS Razao, 
				A1_XGRPEMP AS GrpEmp, Z0_DESC AS DescGrp, A1_GRPVEN AS GrpMatriz, ACY_DESCRI AS DescMatriz, A1_DSCREG AS Regiao, A1_XSEDE AS Sede, 
				CASE WHEN A1_XCANAL = 'A' THEN 'ATACADO' WHEN A1_XCANAL = 'E' THEN 'E-COMMERCE' WHEN A1_XCANAL = 'V' THEN 'VAREJO' END AS Canal, 
				D2_PEDIDO AS Pedido, 
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) As PedEmis, 
				CONVERT(VARCHAR(12),CAST(C6_ENTREG AS DATE),103) AS DtEnt,
				D2_DOC AS Nota,	
				CONVERT(VARCHAR(12),CAST(D2_EMISSAO AS DATE),103) AS NFEmis, 
				D2_COD AS SKU, B1_DESC AS SKUDesc, B1_XDPERSO AS Personagem, B1_XDCOLEC AS Colecao, B1_XDGRUPO AS GrpEst, '' AS CodFam, '' AS FamDesc, 
				A1_MUN AS MunCli, A1_EST AS Estado,  
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) AS DtImplantacao, 
				CASE WHEN SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),3,2) +  SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),1,2) >= '0102' AND 
					SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),3,2) +  SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),1,2) <= '3107' THEN '1� Semestre' Else '2� Semestre' END AS Semestre, 
				'' AS QtdCarteira, '' AS VlrCarteira, SUM(D2_QUANT) AS Quantidade_Ven, SUM(D2_TOTAL) AS VlrTot_Ven, '' AS Quantidade_Dev, '' AS VlrTot_Dev, '' AS MargemBruta, '' AS MargemLiquida, 
				SUM(B2_QATU) AS EstAtual, SUM(B2_SALPEDI) AS EstFuturo
				FROM SF2010 AS SF2
				INNER JOIN SD2010 AS SD2 ON F2_FILIAL = D2_FILIAL AND F2_DOC = D2_DOC AND F2_SERIE = D2_SERIE AND F2_CLIENTE = D2_CLIENTE AND F2_LOJA = D2_LOJA AND SD2.D_E_L_E_T_ = ''
				INNER JOIN SC5010 AS SC5 ON D2_FILIAL = C5_FILIAL AND D2_PEDIDO = C5_NUM AND SC5.D_E_L_E_T_ = ''
				INNER JOIN SC6010 AS SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC6.D_E_L_E_T_ = ''
				INNER JOIN SA1010 AS SA1 ON F2_CLIENTE = A1_COD AND F2_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = ''
				INNER JOIN SA3010 AS SA3 ON F2_VEND1 = A3_COD AND SA3.D_E_L_E_T_ = ''
				INNER JOIN SB1010 AS SB1 ON D2_COD = B1_COD AND SB1.D_E_L_E_T_ = ''
				INNER JOIN SB2010 AS SB2 ON D2_FILIAL = B2_FILIAL AND D2_COD = B2_COD AND SB2.D_E_L_E_T_ = ''
				LEFT  JOIN SZ0010 AS SZ0 ON A1_XGRPEMP = Z0_COD AND SZ0.D_E_L_E_T_ = ''
				LEFT  JOIN ACY010 AS ACY ON A1_GRPVEN = ACY_GRPVEN AND ACY.D_E_L_E_T_ = ''
				WHERE SF2.D_E_L_E_T_ = ''
				AND F2_VEND1   BETWEEN %exp:MV_PAR01% AND %exp:MV_PAR02%
				AND F2_REGIAO  BETWEEN %exp:MV_PAR03% AND %exp:MV_PAR04%
				AND A1_NOME    BETWEEN %exp:MV_PAR05% AND %exp:MV_PAR06%
				AND A1_XGRPEMP BETWEEN %exp:MV_PAR07% AND %exp:MV_PAR08%
				AND D2_PEDIDO  BETWEEN %exp:MV_PAR09% AND %exp:MV_PAR10%
				AND F2_DOC     BETWEEN %exp:MV_PAR11% AND %exp:MV_PAR12%
				AND A1_GRPVEN  BETWEEN %exp:MV_PAR13% AND %exp:MV_PAR14%
				AND D2_COD     BETWEEN %exp:MV_PAR17% AND %exp:MV_PAR18%
				AND B1_XDGRUPO BETWEEN %exp:MV_PAR21% AND %exp:MV_PAR22%
				AND F2_EMISSAO BETWEEN %exp:MV_PAR25% AND %exp:MV_PAR26%
				GROUP BY D2_FILIAL, F2_VEND1, A3_NOME, D2_CLIENTE, D2_LOJA, A1_NOME, A1_XGRPEMP, A1_GRPVEN, A1_DSCREG, A1_XSEDE, A1_XCANAL, D2_PEDIDO, 
				D2_DOC, D2_EMISSAO, A1_MUN, Z0_DESC, ACY_DESCRI, C5_EMISSAO, F2_EMISSAO, C6_ENTREG, D2_COD, B1_DESC, B1_XDPERSO, B1_XDCOLEC, B1_XDGRUPO, A1_EST

				
				UNION
				
				
				SELECT DISTINCT D1_FILIAL AS Filial, A3_COD AS Vendedor, A3_NOME AS Nome_Vendedor, A1_COD AS Cliente, A1_LOJA AS Loja, A1_NOME AS Razao, 
				A1_XGRPEMP AS GrpEmp, Z0_DESC AS DescGrp, A1_GRPVEN AS GrpMatriz, ACY_DESCRI AS DescMatriz, A1_DSCREG AS Regiao, A1_XSEDE AS Sede, 
				CASE WHEN A1_XCANAL = 'A' THEN 'ATACADO' WHEN A1_XCANAL = 'E' THEN 'E-COMMERCE' WHEN A1_XCANAL = 'V' THEN 'VAREJO' END AS Canal, 
				D2_PEDIDO AS Pedido, 
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) As PedEmis, 
				CONVERT(VARCHAR(12),CAST(C6_ENTREG AS DATE),103) AS DtEnt,
				D1_DOC AS Nota,	
				CONVERT(VARCHAR(12),CAST(D1_EMISSAO AS DATE),103) AS NFEmis, 
				D1_COD AS SKU, B1_DESC AS SKUDesc, B1_XDPERSO AS Personagem, B1_XDCOLEC AS Colecao, B1_XDGRUPO AS GrpEst, '' AS CodFam, '' AS FamDesc, 
				A1_MUN AS MunCli, A1_EST AS Estado,  
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) AS DtImplantacao, 
				CASE WHEN SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),3,2) +  SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),1,2) >= '0102' AND 
					SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),3,2) +  SUBSTRING((RIGHT(CONVERT(VARCHAR(12), CAST(F2_EMISSAO AS DATE), 112), 4)),1,2) <= '3107' THEN '1� Semestre' Else '2� Semestre' END AS Semestre, 
				'' AS QtdCarteira, '' AS VlrCarteira, '' AS Quantidade_Ven, '' AS VlrTot_Ven, SUM(D1_QUANT) AS Quantidade_Dev, SUM(D1_TOTAL) AS VlrTot_Dev, '' AS MargemBruta, '' AS MargemLiquida, 
				SUM(B2_QATU) AS EstAtual, SUM(B2_SALPEDI) AS EstFuturo
				FROM SF1010 AS SF1
				INNER JOIN SD1010 AS SD1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_SERIE = D1_SERIE AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND SD1.D_E_L_E_T_ = ''
				INNER JOIN SF2010 AS SF2 ON F1_FILIAL = F2_FILIAL AND F1_NFORIG = F2_DOC AND F2_SERIORI = F2_SERIE AND F1_FORNECE+F1_LOJA = F2_CLIENTE+F2_LOJA AND SF2.D_E_L_E_T_ = ''
				INNER JOIN SD2010 AS SD2 ON F2_FILIAL = D2_FILIAL AND F2_DOC = D2_DOC AND F2_SERIE = D2_SERIE AND F2_CLIENTE = D2_CLIENTE AND F2_LOJA = D2_LOJA AND SD2.D_E_L_E_T_ = '' 
				INNER JOIN SC5010 AS SC5 ON D2_FILIAL = C5_FILIAL AND D2_PEDIDO = C5_NUM AND SC5.D_E_L_E_T_ = ''
				INNER JOIN SC6010 AS SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC6.D_E_L_E_T_ = ''
				INNER JOIN SA1010 AS SA1 ON F2_CLIENTE = A1_COD AND F2_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = ''
				INNER JOIN SA3010 AS SA3 ON F2_VEND1 = A3_COD AND SA3.D_E_L_E_T_ = ''
				INNER JOIN SB1010 AS SB1 ON D1_COD = B1_COD AND SB1.D_E_L_E_T_ = ''
				INNER JOIN SB2010 AS SB2 ON D1_FILIAL = B2_FILIAL AND D1_COD = B2_COD AND SB2.D_E_L_E_T_ = ''
				LEFT  JOIN SZ0010 AS SZ0 ON A1_XGRPEMP = Z0_COD AND SZ0.D_E_L_E_T_ = ''
				LEFT  JOIN ACY010 AS ACY ON A1_GRPVEN = ACY_GRPVEN AND ACY.D_E_L_E_T_ = ''
				WHERE SF1.D_E_L_E_T_ = ''
				AND F2_VEND1   BETWEEN %exp:MV_PAR01% AND %exp:MV_PAR02%
				AND F2_REGIAO  BETWEEN %exp:MV_PAR03% AND %exp:MV_PAR04%
				AND A1_NOME    BETWEEN %exp:MV_PAR05% AND %exp:MV_PAR06%
				AND A1_XGRPEMP BETWEEN %exp:MV_PAR07% AND %exp:MV_PAR08%
				AND D2_PEDIDO  BETWEEN %exp:MV_PAR09% AND %exp:MV_PAR10%
				AND F2_DOC     BETWEEN %exp:MV_PAR11% AND %exp:MV_PAR12%
				AND A1_GRPVEN  BETWEEN %exp:MV_PAR13% AND %exp:MV_PAR14%
				AND D2_COD     BETWEEN %exp:MV_PAR17% AND %exp:MV_PAR18%
				AND B1_XDGRUPO BETWEEN %exp:MV_PAR21% AND %exp:MV_PAR22%
				AND F2_EMISSAO BETWEEN %exp:MV_PAR25% AND %exp:MV_PAR26%
				GROUP BY D1_FILIAL, A3_COD, A3_NOME, A1_COD, A1_LOJA, A1_NOME, A1_XGRPEMP, A1_GRPVEN, A1_DSCREG, A1_XSEDE, A1_XCANAL, D2_PEDIDO, D1_DOC, D1_EMISSAO, 
				D1_COD, B1_DESC, B1_XDPERSO, B1_XDCOLEC, B1_XDGRUPO, A1_MUN, Z0_DESC, ACY_DESCRI, C5_EMISSAO, C6_ENTREG, F2_EMISSAO, A1_EST
				
				
				ORDER BY D2_FILIAL, F2_VEND1, D2_DOC
			EndSql
		
			oExcel:AddColumn(cNameW,cNameW,'Filial'		                ,1,2)		//01
			oExcel:AddColumn(cNameW,cNameW,'Canal'		                ,1,2)		//02
			oExcel:AddColumn(cNameW,cNameW,'Regional'		            ,1,2)		//03
			oExcel:AddColumn(cNameW,cNameW,'C�d. Vendedor'		        ,1,2)		//04
			oExcel:AddColumn(cNameW,cNameW,'Nome Vendedor'		        ,1,2)		//05
			oExcel:AddColumn(cNameW,cNameW,'Pedido'		                ,1,2)		//06			
			oExcel:AddColumn(cNameW,cNameW,'Nota Fiscal'		        ,1,2)		//07
			oExcel:AddColumn(cNameW,cNameW,'Dt. Implanta��o'	        ,1,2)		//08
			oExcel:AddColumn(cNameW,cNameW,'Dt. Emiss�o'		        ,1,2)		//09
			oExcel:AddColumn(cNameW,cNameW,'Dt. Entrega'		        ,1,2)		//10
			oExcel:AddColumn(cNameW,cNameW,'Semestre'		            ,1,2)		//11
			oExcel:AddColumn(cNameW,cNameW,'Raz�o Social'		        ,1,2)		//12
			oExcel:AddColumn(cNameW,cNameW,'C�d. GR Empresarial'        ,1,2)		//13
			oExcel:AddColumn(cNameW,cNameW,'Descri��o GR Empresarial'	,1,2)		//14
			oExcel:AddColumn(cNameW,cNameW,'C�d. Matriz'		        ,1,2)		//15
			oExcel:AddColumn(cNameW,cNameW,'Descri��o Matriz'		    ,1,2)		//16
			oExcel:AddColumn(cNameW,cNameW,'Cidade'		                ,1,2)		//17
			oExcel:AddColumn(cNameW,cNameW,'UF'		                    ,1,2)		//18			
			oExcel:AddColumn(cNameW,cNameW,'SKU'		                ,1,2)		//19
			oExcel:AddColumn(cNameW,cNameW,'Descri��o do Item'		    ,1,2)		//20
			oExcel:AddColumn(cNameW,cNameW,'C�d. Fam�lia'		        ,1,2)		//21
			oExcel:AddColumn(cNameW,cNameW,'Descri��o Familia'		    ,1,2)		//22
			oExcel:AddColumn(cNameW,cNameW,'Cole��o'		            ,1,2)		//23
			oExcel:AddColumn(cNameW,cNameW,'Grupo Estoque'		        ,1,2)		//24
			oExcel:AddColumn(cNameW,cNameW,'Personagem'		            ,1,2)		//25
			oExcel:AddColumn(cNameW,cNameW,'Qtde Mercadoria'            ,1,2)		//26
			oExcel:AddColumn(cNameW,cNameW,'Valor Mercadoria'           ,1,2)		//27
			oExcel:AddColumn(cNameW,cNameW,'Qtde Devolu��o'             ,1,2)		//28
			oExcel:AddColumn(cNameW,cNameW,'Valor Total Devolu��o'      ,1,2)		//29
			oExcel:AddColumn(cNameW,cNameW,'Qtde Carteira'		        ,1,2)		//30
			oExcel:AddColumn(cNameW,cNameW,'Valor em Carteira'		    ,1,2)		//31			
			oExcel:AddColumn(cNameW,cNameW,'Margem Bruta'		        ,1,2)       //32
			oExcel:AddColumn(cNameW,cNameW,'Margem Liquida'	            ,1,2)       //33
			
			DbSelectArea(cAliasAna)
			(cAliasAna)->(DBGOTOP())	
			
			While (cAliasAna)->(!EOF())
		   			aDados := {	(cAliasAna)->Filial  				,;   //01
		   						(cAliasAna)->Canal	  				,;   //02
		   						(cAliasAna)->Regiao					,;   //03
		   						(cAliasAna)->Vendedor				,;   //04
								(cAliasAna)->Nome_Vendedor			,;   //05
		   						(cAliasAna)->Pedido					,;   //06
		   						(cAliasAna)->Nota					,;   //07
		   						(cAliasAna)->DtImplantacao			,;   //08
		   						(cAliasAna)->PedEmis		        ,;   //09
		   						(cAliasAna)->DtEnt					,;   //10
		   						(cAliasAna)->Semestre				,;   //11
		   						(cAliasAna)->Razao					,;   //12
						   		(cAliasAna)->GrpEmp	   				,;   //13
								(cAliasAna)->DescGrp				,;   //14
						   		(cAliasAna)->GrpMatriz	  			,;   //15
								(cAliasAna)->DescMatriz				,;   //16
								(cAliasAna)->MunCli					,;   //17
								(cAliasAna)->Estado					,;   //18
								(cAliasAna)->SKU					,;   //19
								(cAliasAna)->SKUDesc				,;   //20
								(cAliasAna)->CodFam					,;   //21
								(cAliasAna)->FamDesc				,;   //22
								(cAliasAna)->Colecao				,;   //23
								(cAliasAna)->GrpEst					,;   //24
								(cAliasAna)->Personagem				,;   //25
								(cAliasAna)->Quantidade_Ven 		,;   //26
								(cAliasAna)->VlrTot_Ven				,;   //27
								(cAliasAna)->Quantidade_Dev         ,;   //28
								(cAliasAna)->VlrTot_Dev             ,;   //29
								(cAliasAna)->QtdCarteira			,;   //30
								(cAliasAna)->VlrCarteira			,;   //31
								(cAliasAna)->MargemBruta            ,;   //32
								(cAliasAna)->MargemLiquida          }    //33

								
				oExcel:AddRow(cNameW,cNameW,aDados)
			
				(cAliasAna)->( dbSkip() )
			End					
			
		Else
	
			BeginSql alias cAliasSin                                  	
				SELECT DISTINCT D2_FILIAL AS Filial, F2_VEND1 AS Vendedor, A3_NOME AS Nome_Vendedor, D2_CLIENTE AS Cliente, D2_LOJA AS Loja, A1_NOME AS Razao, 
				A1_XGRPEMP AS GrpEmp, Z0_DESC AS DescGrp, A1_GRPVEN AS GrpMatriz, ACY_DESCRI AS DescMatriz, A1_DSCREG AS Regiao, A1_XSEDE AS Sede, 
				CASE WHEN A1_XCANAL = 'A' THEN 'ATACADO' WHEN A1_XCANAL = 'E' THEN 'E-COMMERCE' WHEN A1_XCANAL = 'V' THEN 'VAREJO' END AS Canal, 
				D2_PEDIDO AS Pedido, 
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) As PedEmis, 
				CONVERT(VARCHAR(12),CAST(C6_ENTREG AS DATE),103) AS DtEnt,
				D2_DOC AS Nota,	
				CONVERT(VARCHAR(12),CAST(D2_EMISSAO AS DATE),103) AS NFEmis, 
				SUM(D2_QUANT) AS Quantidade_Ven, SUM(D2_TOTAL) AS VlrTot_Ven, '' AS Quantidade_Dev, '' AS VlrTot_Dev, '' AS MargemBruta, '' AS MargemLiquida, 
				SUM(B2_QATU) AS EstAtual, SUM(B2_SALPEDI) AS EstFuturo
				FROM SF2010 AS SF2
				INNER JOIN SD2010 AS SD2 ON F2_FILIAL = D2_FILIAL AND F2_DOC = D2_DOC AND F2_SERIE = D2_SERIE AND F2_CLIENTE = D2_CLIENTE AND F2_LOJA = D2_LOJA AND SD2.D_E_L_E_T_ = ''
				INNER JOIN SC5010 AS SC5 ON D2_FILIAL = C5_FILIAL AND D2_PEDIDO = C5_NUM AND SC5.D_E_L_E_T_ = ''
				INNER JOIN SC6010 AS SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC6.D_E_L_E_T_ = ''
				INNER JOIN SA1010 AS SA1 ON F2_CLIENTE = A1_COD AND F2_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = ''
				INNER JOIN SA3010 AS SA3 ON F2_VEND1 = A3_COD AND SA3.D_E_L_E_T_ = ''
				INNER JOIN SB1010 AS SB1 ON D2_COD = B1_COD AND SB1.D_E_L_E_T_ = ''
				INNER JOIN SB2010 AS SB2 ON D2_FILIAL = B2_FILIAL AND D2_COD = B2_COD AND SB2.D_E_L_E_T_ = ''
				LEFT  JOIN SZ0010 AS SZ0 ON A1_XGRPEMP = Z0_COD AND SZ0.D_E_L_E_T_ = ''
				LEFT  JOIN ACY010 AS ACY ON A1_GRPVEN = ACY_GRPVEN AND ACY.D_E_L_E_T_ = ''
				WHERE SF2.D_E_L_E_T_ = ''
				AND F2_VEND1   BETWEEN %exp:MV_PAR01% AND %exp:MV_PAR02%
				AND F2_REGIAO  BETWEEN %exp:MV_PAR03% AND %exp:MV_PAR04%
				AND A1_NOME    BETWEEN %exp:MV_PAR05% AND %exp:MV_PAR06%
				AND A1_XGRPEMP BETWEEN %exp:MV_PAR07% AND %exp:MV_PAR08%
				AND D2_PEDIDO  BETWEEN %exp:MV_PAR09% AND %exp:MV_PAR10%
				AND F2_DOC     BETWEEN %exp:MV_PAR11% AND %exp:MV_PAR12%
				AND A1_GRPVEN  BETWEEN %exp:MV_PAR13% AND %exp:MV_PAR14%
				AND D2_COD     BETWEEN %exp:MV_PAR17% AND %exp:MV_PAR18%
				AND B1_XDGRUPO BETWEEN %exp:MV_PAR21% AND %exp:MV_PAR22%
				AND F2_EMISSAO BETWEEN %exp:MV_PAR25% AND %exp:MV_PAR26%
				GROUP BY D2_FILIAL, F2_VEND1, A3_NOME, D2_CLIENTE, D2_LOJA, A1_NOME, A1_XGRPEMP, A1_GRPVEN, A1_DSCREG, A1_XSEDE, A1_XCANAL, D2_PEDIDO, 
				D2_DOC, D2_EMISSAO, A1_MUN, Z0_DESC, ACY_DESCRI, C5_EMISSAO, F2_EMISSAO, C6_ENTREG
								
								
				UNION
								
								
				SELECT DISTINCT D1_FILIAL AS Filial, A3_COD AS Vendedor, A3_NOME AS Nome_Vendedor, A1_COD AS Cliente, A1_LOJA AS Loja, A1_NOME AS Razao, 
				A1_XGRPEMP AS GrpEmp, Z0_DESC AS DescGrp, A1_GRPVEN AS GrpMatriz, ACY_DESCRI AS DescMatriz, A1_DSCREG AS Regiao, A1_XSEDE AS Sede, 
				CASE WHEN A1_XCANAL = 'A' THEN 'ATACADO' WHEN A1_XCANAL = 'E' THEN 'E-COMMERCE' WHEN A1_XCANAL = 'V' THEN 'VAREJO' END AS Canal, 
				D2_PEDIDO AS Pedido, 
				CONVERT(VARCHAR(12),CAST(C5_EMISSAO AS DATE),103) As PedEmis, 
				CONVERT(VARCHAR(12),CAST(C6_ENTREG AS DATE),103) AS DtEnt,
				D1_DOC AS Nota,	
				CONVERT(VARCHAR(12),CAST(D1_EMISSAO AS DATE),103) AS NFEmis, 
				'' AS Quantidade_Ven, '' AS VlrTot_Ven, SUM(D1_QUANT) AS Quantidade_Dev, SUM(D1_TOTAL) AS VlrTot_Dev, '' AS MargemBruta, '' AS MargemLiquida, 
				SUM(B2_QATU) AS EstAtual, SUM(B2_SALPEDI) AS EstFuturo
				FROM SF1010 AS SF1
				INNER JOIN SD1010 AS SD1 ON F1_FILIAL = D1_FILIAL AND F1_DOC = D1_DOC AND F1_SERIE = D1_SERIE AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND SD1.D_E_L_E_T_ = ''
				INNER JOIN SF2010 AS SF2 ON F1_FILIAL = F2_FILIAL AND F1_NFORIG = F2_DOC AND F2_SERIORI = F2_SERIE AND F1_FORNECE+F1_LOJA = F2_CLIENTE+F2_LOJA AND SF2.D_E_L_E_T_ = ''
				INNER JOIN SD2010 AS SD2 ON F2_FILIAL = D2_FILIAL AND F2_DOC = D2_DOC AND F2_SERIE = D2_SERIE AND F2_CLIENTE = D2_CLIENTE AND F2_LOJA = D2_LOJA AND SD2.D_E_L_E_T_ = '' 
				INNER JOIN SC5010 AS SC5 ON D2_FILIAL = C5_FILIAL AND D2_PEDIDO = C5_NUM AND SC5.D_E_L_E_T_ = ''
				INNER JOIN SC6010 AS SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC6.D_E_L_E_T_ = ''
				INNER JOIN SA1010 AS SA1 ON F2_CLIENTE = A1_COD AND F2_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = ''
				INNER JOIN SA3010 AS SA3 ON F2_VEND1 = A3_COD AND SA3.D_E_L_E_T_ = ''
				INNER JOIN SB1010 AS SB1 ON D1_COD = B1_COD AND SB1.D_E_L_E_T_ = ''
				INNER JOIN SB2010 AS SB2 ON D1_FILIAL = B2_FILIAL AND D1_COD = B2_COD AND SB2.D_E_L_E_T_ = ''
				LEFT  JOIN SZ0010 AS SZ0 ON A1_XGRPEMP = Z0_COD AND SZ0.D_E_L_E_T_ = ''
				LEFT  JOIN ACY010 AS ACY ON A1_GRPVEN = ACY_GRPVEN AND ACY.D_E_L_E_T_ = ''
				WHERE SF1.D_E_L_E_T_ = ''
				AND F2_VEND1   BETWEEN %exp:MV_PAR01% AND %exp:MV_PAR02%
				AND F2_REGIAO  BETWEEN %exp:MV_PAR03% AND %exp:MV_PAR04%
				AND A1_NOME    BETWEEN %exp:MV_PAR05% AND %exp:MV_PAR06%
				AND A1_XGRPEMP BETWEEN %exp:MV_PAR07% AND %exp:MV_PAR08%
				AND D2_PEDIDO  BETWEEN %exp:MV_PAR09% AND %exp:MV_PAR10%
				AND F2_DOC     BETWEEN %exp:MV_PAR11% AND %exp:MV_PAR12%
				AND A1_GRPVEN  BETWEEN %exp:MV_PAR13% AND %exp:MV_PAR14%
				AND D2_COD     BETWEEN %exp:MV_PAR17% AND %exp:MV_PAR18%
				AND B1_XDGRUPO BETWEEN %exp:MV_PAR21% AND %exp:MV_PAR22%
				AND F2_EMISSAO BETWEEN %exp:MV_PAR25% AND %exp:MV_PAR26%
				GROUP BY D1_FILIAL, A3_COD, A3_NOME, A1_COD, A1_LOJA, A1_NOME, A1_XGRPEMP, A1_GRPVEN, A1_DSCREG, A1_XSEDE, A1_XCANAL, D2_PEDIDO, D1_DOC, D1_EMISSAO, 
				Z0_DESC, ACY_DESCRI, C5_EMISSAO, F2_EMISSAO, C6_ENTREG
								
								
				ORDER BY D2_FILIAL, F2_VEND1, D2_DOC
			EndSql
		
			oExcel:AddColumn(cNameW,cNameW,'Filial'		                 ,1,2)		//01
			oExcel:AddColumn(cNameW,cNameW,'C�d. Vendedor'	             ,1,2)		//02
			oExcel:AddColumn(cNameW,cNameW,'Nome Vendedor'		         ,1,2)		//03
			oExcel:AddColumn(cNameW,cNameW,'Raz�o Social'		         ,1,2)		//06
			oExcel:AddColumn(cNameW,cNameW,'C�d. GR Empresarial'         ,1,2)		//07
			oExcel:AddColumn(cNameW,cNameW,'Descri��o GR Empresarial'	 ,1,2)		//08
			oExcel:AddColumn(cNameW,cNameW,'C�d. Matriz'	             ,1,2)		//09
			oExcel:AddColumn(cNameW,cNameW,'Descri��o Matriz'		     ,1,2)		//10
			oExcel:AddColumn(cNameW,cNameW,'Regional'		             ,1,2)		//11
			oExcel:AddColumn(cNameW,cNameW,'Canal'		                 ,1,2)		//13
			oExcel:AddColumn(cNameW,cNameW,'Pedido'		                 ,1,2)		//14
			oExcel:AddColumn(cNameW,cNameW,'Dt. Emiss�o'		         ,1,2)		//15
			oExcel:AddColumn(cNameW,cNameW,'Dt. Entrega'		         ,1,2)		//16
			oExcel:AddColumn(cNameW,cNameW,'Nota Fiscal'		         ,1,2)		//17
			oExcel:AddColumn(cNameW,cNameW,'Dt. Emiss�o'		         ,1,2)		//18
			oExcel:AddColumn(cNameW,cNameW,'Qtde Venda'                  ,1,2)		//31
			oExcel:AddColumn(cNameW,cNameW,'Valor Total Venda'           ,1,2)		//32
			oExcel:AddColumn(cNameW,cNameW,'Qtde Devolu��o'              ,1,2)		//31
			oExcel:AddColumn(cNameW,cNameW,'Valor Total Devolu��o'       ,1,2)		//32
			oExcel:AddColumn(cNameW,cNameW,'Margem Bruta'		         ,1,2)
			oExcel:AddColumn(cNameW,cNameW,'Margem Liquida'		         ,1,2)			

			
			DbSelectArea(cAliasSin)
			(cAliasSin)->(DBGOTOP())
			While (cAliasSin)->(!EOF())
		   			aDados := {	(cAliasSin)->Filial  				,;   //01
								(cAliasSin)->Vendedor				,;   //02
								(cAliasSin)->Nome_Vendedor			,;   //03
								(cAliasSin)->Razao					,;   //04
						   		(cAliasSin)->GrpEmp	   				,;   //05
								(cAliasSin)->DescGrp				,;   //06
						   		(cAliasSin)->GrpMatriz	  			,;   //07
								(cAliasSin)->DescMatriz				,;   //08
								(cAliasSin)->Regiao					,;   //09
								(cAliasSin)->Canal	  				,;   //10
								(cAliasSin)->Pedido					,;   //11
								(cAliasSin)->PedEmis		        ,;   //12
								(cAliasSin)->DtEnt					,;   //13
								(cAliasSin)->Nota					,;   //14
								(cAliasSin)->NFEmis					,;   //15
								(cAliasSin)->Quantidade_Ven 		,;   //16
								(cAliasSin)->VlrTot_Ven				,;   //17
								(cAliasSin)->Quantidade_Dev         ,;   //18
								(cAliasSin)->VlrTot_Dev             ,;   //19
								(cAliasSin)->MargemBruta            ,;   //20
								(cAliasSin)->MargemLiquida          }    //21
								
				oExcel:AddRow(cNameW,cNameW,aDados)
			
				(cAliasSin)->( dbSkip() )
			End							
			
		EndIf
		
	
		oExcel:Activate()
		      
		cArq := CriaTrab( NIL, .F. ) + ".xml"
		MsgRun( "Gerando o arquivo, aguarde...", cCadastro, {|| oExcel:GetXMLFile( cArq ) } )
		If __CopyFile( cArq, cDirTmp + cArq )
			oExcelApp := MsExcel():New()
			oExcelApp:WorkBooks:Open( cDirTmp + cArq )
			oExcelApp:SetVisible(.T.)
		Else
			MsgInfo( "Arquivo n�o copiado para tempor�rio do usu�rio." )
		Endif
	
	END SEQUENCE
	
	Restarea( aArea )

Return( Nil )


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  AjustaSx1  �Autor  �                    � Data �  25/01/2016 ���
�������������������������������������������������������������������������͹��
���Desc.     � Criacao do sx1 para o relatoro                             ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSx1()
Local aArea 	:= GetArea()
//Local cPerg := "RELMETAS"
     
aHelpPor	:= {}
aHelpEng	:= {}
aHelpSpa	:= {}  



	aHelpPor := {"C�d. Vendedor de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"C�d. Vendedor de: "} 
	PutSx1(cPerg,'01','C�d. Vendedor de: '       ,'','','mv_ch1','C',06,0,0,'G',''        ,'SA3','','','mv_par01','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)
	
	
	aHelpPor := {"C�d. Vendedor ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"C�d. Vendedor ate: "}
	PutSx1(cPerg,'02','C�d. Vendedor ate: '      ,'','','mv_ch2','C',06,0,0,'G',''        ,'SA3','','','mv_par02','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)
	
	
	aHelpPor := {"Regional de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Regional de: "}
	PutSx1(cPerg,'03','Regional de: '      ,'','','mv_ch3','C',06,0,0,'G',''        ,'SZV','','','mv_par03','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Regional ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Regional ate: "}
	PutSx1(cPerg,'04','Regional ate: '     ,'','','mv_ch4','C',06,0,0,'G',''        ,'SZV','','','mv_par04','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  

/*
	aHelpPor := {"Sede de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Sede de: "}
	PutSx1(cPerg,'05','Sede de: '      ,'','','mv_ch5','C',06,0,0,'G',''        ,'','','','mv_par05','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Sede ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Sede ate:  "}
	PutSx1(cPerg,'06','Sede ate: '     ,'','','mv_ch6','C',06,0,0,'G',''        ,'','','','mv_par06','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)
	
	
	aHelpPor := {"Cliente de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Cliente de: "}
	PutSx1(cPerg,'07','Cliente de: '      ,'','','mv_ch7','C',08,0,0,'G',''        ,'SA1','','','mv_par07','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Cliente ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Cliente ate: "}
	PutSx1(cPerg,'08','Cliente ate: '     ,'','','mv_ch8','C',08,0,0,'G',''        ,'SA1','','','mv_par08','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
*/	
	
	aHelpPor := {"Raz�o Social de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Raz�o Social de: "}
	PutSx1(cPerg,'09','Raz�o Social de: '      ,'','','mv_ch9','C',100,0,0,'G',''        ,'','','','mv_par09','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Raz�o Social ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Raz�o Social ate: "}
	PutSx1(cPerg,'10','Raz�o Social ate: '     ,'','','mv_ch10','C',100,0,0,'G',''        ,'','','','mv_par10','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)	
	
	
	aHelpPor := {"Grupo Empresarial de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Grupo Empresarial de: "}
	PutSx1(cPerg,'11','Grupo Empresarial de: '      ,'','','mv_ch11','C',06,0,0,'G',''        ,'SZ0','','','mv_par11','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Grupo Empresarial ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Grupo Empresarial ate: "}
	PutSx1(cPerg,'12','Grupo Empresarial ate: '     ,'','','mv_ch12','C',06,0,0,'G',''        ,'SZ0','','','mv_par12','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)	


	aHelpPor := {"Pedido de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Pedido de: "}
	PutSx1(cPerg,'13','Pedido de: '      ,'','','mv_ch13','C',06,0,0,'G',''        ,'SC5','','','mv_par13','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Pedido ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Pedido ate: "}
	PutSx1(cPerg,'14','Pedido ate: '     ,'','','mv_ch14','C',06,0,0,'G',''        ,'SC5','','','mv_par14','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Nota Fiscal de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Nota Fiscal de: "}
	PutSx1(cPerg,'15','Nota Fiscal de: '      ,'','','mv_ch15','C',09,0,0,'G',''        ,'','','','mv_par15','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Nota Fiscal ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Nota Fiscal ate: "}
	PutSx1(cPerg,'16','Nota Fiscal ate: '     ,'','','mv_ch16','C',09,0,0,'G',''        ,'','','','mv_par16','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)



	aHelpPor := {"Matriz Cliente de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Matriz Cliente de: "}
	PutSx1(cPerg,'17','Matriz Cliente de: '      ,'','','mv_ch17','C',06,0,0,'G',''        ,'','','','mv_par17','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Matriz Cliente ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Matriz Cliente ate: "}
	PutSx1(cPerg,'18','Matriz Cliente ate: '     ,'','','mv_ch18','C',06,0,0,'G',''        ,'','','','mv_par18','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)



	aHelpPor := {"Canal de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Canal de: "}
	PutSx1(cPerg,'19','Canal de: '      ,'','','mv_ch19','C',06,0,0,'G',''        ,'','','','mv_par19','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Canal ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Canal ate: "}
	PutSx1(cPerg,'20','Canal ate: '     ,'','','mv_ch20','C',06,0,0,'G',''        ,'','','','mv_par20','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"SKU de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"SKU de: "}
	PutSx1(cPerg,'21','SKU de: '      ,'','','mv_ch21','C',15,0,0,'G',''        ,'','','','mv_par21','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"SKU ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"SKU ate: "}
	PutSx1(cPerg,'22','SKU ate: '     ,'','','mv_ch22','C',15,0,0,'G',''        ,'','','','mv_par22','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)

		

	aHelpPor := {"Personagem de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Personagem de: "}
	PutSx1(cPerg,'23','Personagem de: '      ,'','','mv_ch23','C',30,0,0,'G',''        ,'','','','mv_par23','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Personagem ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Personagem ate: "}
	PutSx1(cPerg,'24','Personagem ate: '     ,'','','mv_ch24','C',30,0,0,'G',''        ,'','','','mv_par24','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Grupo de Estoque de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Grupo de Estoque de: "}
	PutSx1(cPerg,'25','Grupo de Estoque de: '      ,'','','mv_ch25','C',30,0,0,'G',''        ,'','','','mv_par25','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Grupo de Estoque ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Grupo de Estoque ate: "}
	PutSx1(cPerg,'26','Grupo de Estoque ate: '     ,'','','mv_ch26','C',30,0,0,'G',''        ,'','','','mv_par26','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Familia de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Familia de: "}
	PutSx1(cPerg,'27','Familia de: '      ,'','','mv_ch27','C',30,0,0,'G',''        ,'','','','mv_par27','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Familia ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Familia ate: "}
	PutSx1(cPerg,'28','Familia ate: '     ,'','','mv_ch28','C',30,0,0,'G',''        ,'','','','mv_par28','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Dt. Emiss�o de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Dt. Emiss�o de: "}
	PutSx1(cPerg,'29','Dt. Emiss�o de: '      ,'','','mv_ch29','D',08,0,0,'G',''        ,'','','','mv_par29','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Dt. Emiss�o ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Dt. Emiss�o ate: "}
	PutSx1(cPerg,'30','Dt. Emiss�o ate: '     ,'','','mv_ch30','D',08,0,0,'G',''        ,'','','','mv_par30','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)



	aHelpPor := {"Dt. Entrega de: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Dt. Entrega de: "}
	PutSx1(cPerg,'31','Dt. Entrega de: '      ,'','','mv_ch31','D',08,0,0,'G',''        ,'','','','mv_par31','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)  
	
	
	aHelpPor := {"Dt. Entrega ate: "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Dt. Entrega ate: "}
	PutSx1(cPerg,'32','Dt. Entrega ate: '     ,'','','mv_ch32','D',08,0,0,'G',''        ,'','','','mv_par32','','','','','','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Considera devolu��es? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Considera devolu��es? "}
	PutSx1(cPerg,'33','Considera devolu��es? '     ,'','','mv_ch33','D',08,0,0,'C',''        ,'','','','mv_par33','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Considera Ped Carteira Venda? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Considera Ped Carteira Venda? "}
	PutSx1(cPerg,'34','Considera Ped Carteira Venda? '     ,'','','mv_ch34','D',08,0,0,'C',''        ,'','','','mv_par34','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)



	aHelpPor := {"Imprime Margem Liquida? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Margem Liquida? "}
	PutSx1(cPerg,'35','Imprime Margem Liquida? '     ,'','','mv_ch35','D',08,0,0,'C',''        ,'','','','mv_par35','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Imprime Margem Bruta? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Margem Bruta? "}
	PutSx1(cPerg,'36','Imprime Margem Bruta? '     ,'','','mv_ch36','D',08,0,0,'C',''        ,'','','','mv_par36','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


/*
	aHelpPor := {"Imprime Estoque? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Estoque? "}
	PutSx1(cPerg,'37','Imprime Estoque? '     ,'','','mv_ch37','D',08,0,0,'C',''        ,'','','','mv_par37','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)
*/

	aHelpPor := {"Imprime Entrada Futura? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Entrada Futura? "}
	PutSx1(cPerg,'38','Imprime Entrada Futura? '     ,'','','mv_ch38','D',08,0,0,'C',''        ,'','','','mv_par38','Sim','','','','Nao','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Imprime Sintetico/Analitico? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Sintetico/Analitico? "}
	PutSx1(cPerg,'39','Imprime Sintetico/Analitico? '     ,'','','mv_ch39','D',08,0,0,'C',''        ,'','','','mv_par39','S-Sintetico','','','','A-Analitico','','','','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)


	aHelpPor := {"Imprime Desagio? "}
	Aadd( aHelpSpa, ' ' )
	aHelpEng := {"Imprime Desagio? "}
	PutSx1(cPerg,'40','Imprime Desagio? '     ,'','','mv_ch40','D',08,0,0,'C',''        ,'','','','mv_par40','Sim','','','','Nao','','','Tudo','','','','','','','','',aHelpPor,aHelpEng,aHelpSpa)

	
	
RestArea(aArea)
Return