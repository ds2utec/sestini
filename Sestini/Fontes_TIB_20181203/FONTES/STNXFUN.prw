#INCLUDE "PROTHEUS.CH"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNXFUN   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �BIBLIOTECA DE FUNCOES UTILIZADAS NO PROJETO SESTINI         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/



/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNXNext  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Obtem o proximo codigo do cliente/fornecedor disponivel,    ���
���          �quando os mesmos NAO sao pessoa Fisica/Juridica             ���
���          �Funcao para ser acionada no gatilho dos campos A1_CGC|A2_CGC���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Function STNXNext(cTable)
Local cAliasQry := GetNextAlias()
Local cRet      := ''
Local aArea     := GetArea()
Local aAreaSA1  := SA1->(GetArea())
Local aAreaSA2  := SA2->(GetArea())

If cTable == 'SA1'
	BeginSQL Alias cAliasQry
		SELECT MAX(SA1.A1_COD) CODIGO
		  FROM %Table:SA1% SA1
		 WHERE SA1.A1_FILIAL = %xFilial:SA1%
		   AND SA1.A1_TIPO   = 'X'
		   AND SA1.%NotDel%
	EndSQL

	cRet := Soma1((cAliasQry)->CODIGO)
	SA1->(DbSetOrder(1)) //--A1_FILIAL+A1_COD+A1_LOJA
	While SA1->(DbSeek(xFilial('SA1')+cRet))
		cRet := Soma1(cRet)
	End

ElseIf cTable == 'SA2'
	BeginSQL Alias cAliasQry
		SELECT MAX(SA2.A2_COD) CODIGO
		  FROM %Table:SA2% SA2
		 WHERE SA2.A2_FILIAL = %xFilial:SA2%
		   AND SA2.A2_TIPO   = 'X'
		   AND SA2.%NotDel%
	EndSQL

	cRet := Soma1((cAliasQry)->CODIGO)
	SA2->(DbSetOrder(1)) //--A2_FILIAL+A2_COD+A2_LOJA
	While SA2->(DbSeek(xFilial('SA2')+cRet))
		cRet := Soma1(cRet)
	End

EndIf

//-- RESTAURA AMBIENTE
RestArea(aArea)
RestArea(aAreaSA1)
RestArea(aAreaSA2)

//-- FECHA QUERY:
(cAliasQry)->(DbCloseArea())


Return(cRet)