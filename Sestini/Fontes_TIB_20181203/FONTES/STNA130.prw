#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA130   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �MANUTENCAO AREAS x RESPONSAVEIS                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA130()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZQ')
oBrowse:SetDescription('�reas X Respons�veis')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'		ACTION 'PesqBrw'			OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'		ACTION 'VIEWDEF.STNA130'	OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'			ACTION 'VIEWDEF.STNA130'	OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'			ACTION 'VIEWDEF.STNA130'	OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'			ACTION 'VIEWDEF.STNA130'	OPERATION MODEL_OPERATION_DELETE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStrResp   := FWFormStruct(1, 'SZQ', {|cCampo| .T.})
Local oStrUser   := FWFormStruct(1, 'SZR', {|cCampo| .T.})
Local oStrCampos := FWFormStruct(1, 'SZS', {|cCampo| .T.})

Local oModel     := NIL
Local bCommit    := {|oModel| STNA130Commit(oModel)}

oModel:= MPFormModel():New('STNA130',, {|oModel| STNA130Pos(oModel)}, bCommit)

oModel:SetDescription('�reas x Respons�veis')
oModel:SetVldActivate({|oModel| STNA130Pre(oModel)})

oModel:AddFields('mdlResp', NIL, oStrResp)
oModel:GetModel('mdlResp'):SetDescription('�reas')
oModel:SetPrimaryKey({'ZQ_FILIAL+ZQ_CODIGO'})

oModel:AddGrid('mdlUser', 'mdlResp', oStrUser) 
oModel:SetRelation('mdlUser', {{'ZR_FILIAL','xFilial("SZR")'}, {'ZR_CODAREA', 'ZQ_CODIGO'}}, SZR->(IndexKey(1))) 
oModel:GetModel('mdlUser'):SetUniqueLine({'ZR_CODUSR'})

oModel:AddGrid('mdlCampos', 'mdlUser', oStrCampos) 
oModel:SetRelation('mdlCampos', {{'ZS_FILIAL','xFilial("SZS")'}, {'ZS_CODAREA', 'ZQ_CODIGO'}, {'ZS_CODRESP', 'ZR_CODUSR'}}, SZS->(IndexKey(1))) 
oModel:GetModel('mdlCampos'):SetUniqueLine({'ZS_CAMPO'})

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView      := Nil
Local oModel     := FWLoadModel('STNA130')
Local oStrResp   := FWFormStruct(2, 'SZQ', {|cCampo| .T. })
Local oStrUser   := FWFormStruct(2, 'SZR', {|cCampo| .T. })
Local oStrCampos := FWFormStruct(2, 'SZS', {|cCampo| .T. })


//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('viewResp', oStrResp, 'mdlResp')
oView:EnableTitleView('viewResp', '�reas')

oView:AddGrid('viewUser', oStrUser, 'mdlUser')
oView:EnableTitleView('viewUser', 'Respons�veis')

oView:AddGrid('viewCampos', oStrCampos, 'mdlCampos')
oView:EnableTitleView('viewCampos', 'Campos')

oView:CreateHorizontalBox('hdrResp'		, 20)
oView:CreateHorizontalBox('grdUser'		, 40)
oView:CreateHorizontalBox('grdCampos'	, 40)

oView:SetOwnerView('viewResp'	, 'hdrResp')
oView:SetOwnerView('viewUser'	, 'grdUser')
oView:SetOwnerView('viewCampos'	, 'grdCampos')


Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA130Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA130Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA130Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA130Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA130Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA130Commit(oModel)
Local lRet        := .T.
Default oModel    := FWModelActive()

// ---------------------------
// TRATA O DOCUMENTO VINCULADO
// ---------------------------
If oModel:GetOperation() == MODEL_OPERATION_DELETE

EndIf


// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	lRet := FwFormCommit(oModel)
EndIf

Return(lRet)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA130F3    �Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Consulta padrao para listar os campos da tabela             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
User Function STNA130F3()
Local aCabec   := {}
Local aItens   := {}
Local oModel   := FWModelActive()
Local cTabela  := oModel:GetValue('mdlCampos', 'ZS_TABELA')
Local nPosA    := 0
Local nRet     := 0
Local lRet     := .F.
Local aAreaSX3 := SX3->(GetArea())

If Empty(cTabela)
	lRet := .F.
	Help(,, 'STNA130F3',, 'Informe uma tabela v�lida para realizar a consulta!', 1, 0)

Else
	aAdd(aCabec, 'Campo')
	aAdd(aCabec, 'Titulo')
	aAdd(aCabec, 'Descricao')
	aAdd(aCabec, 'Tipo')
	aAdd(aCabec, 'Tamanho')
	aAdd(aCabec, 'Decimais')

	SX3->(DbSetOrder(1))
	SX3->(DbSeek(cTabela))
	While !SX3->(Eof()) .And. SX3->X3_ARQUIVO == cTabela
		If SX3->X3_CONTEXT <> 'V' .And. X3Uso(SX3->X3_USADO) .And. cNivel >= SX3->X3_NIVEL
			aAdd(aItens, {	SX3->X3_CAMPO,;
							SX3->X3_TITULO,;
							SX3->X3_DESCRIC,;
							SX3->X3_TIPO,;
							SX3->X3_TAMANHO,;
							SX3->X3_DECIMAL })
		EndIf
		SX3->(DbSkip())
	End

	nRet := TmsF3Array(aCabec, aItens, 'Consulta Campos', .T.,, aCabec)
	If nRet <> 0
		lRet := .T.
		VAR_IXB := aItens[nRet, 1]
	EndIf

EndIf

RestArea(aAreaSX3)
Return(lRet)