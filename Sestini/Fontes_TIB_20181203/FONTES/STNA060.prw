#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA060   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �MANUTENCAO DA CARACTERISTICAS DE PRODUTO                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA060()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZF')
oBrowse:SetDescription('Caracteristicas de Produto SESTINI')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'			ACTION 'PesqBrw'				OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'			ACTION 'VIEWDEF.STNA060'		OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'				ACTION 'VIEWDEF.STNA060'		OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'				ACTION 'VIEWDEF.STNA060'		OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'				ACTION 'VIEWDEF.STNA060'		OPERATION MODEL_OPERATION_DELETE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStruct    := FWFormStruct(1, 'SZF', {|cCampo| .T.})
Local oModel      := NIL
Local bCommit     := {|oModel| STNA060Commit(oModel)}

oModel:= MPFormModel():New('STNA060',, {|oModel| STNA060Pos(oModel)}, bCommit)

oModel:SetDescription('Caracteristicas de Produto SESTINI')
oModel:SetVldActivate({|oModel| STNA060Pre(oModel)})

oModel:AddFields('SZFMASTER', NIL, oStruct)
oModel:GetModel('SZFMASTER'):SetDescription('Caracteristicas de Produto SESTINI')
oModel:SetPrimaryKey({'ZF_FILIAL+ZF_CODIGO'})

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView  		:= Nil
Local oModel  		:= FWLoadModel('STNA060')
Local oStruct 	    := FWFormStruct(2, 'SZF', {|cCampo| .T. })

//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('VIEW_SZF', oStruct, 'SZFMASTER')

Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA060Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA060Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA060Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA060Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA060Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA060Commit(oModel)
Local lRet        := .T.
Default oModel    := FWModelActive()

// ---------------------------
// TRATA O DOCUMENTO VINCULADO
// ---------------------------
If oModel:GetOperation() == MODEL_OPERATION_DELETE

EndIf


// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	lRet := FwFormCommit(oModel)
EndIf

Return(lRet)
