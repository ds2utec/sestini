#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWMVCDEF.CH" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���PROGRAMA  �STNA100   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �MANUTENCAO DA TABELA DE MATERIAIS PUXADOR                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/ 
Function STNA100()
Local oBrowse   := NIL
Private aRotina	:= MenuDef()

oBrowse := FWMBrowse():New()
oBrowse:SetAlias('SZJ')
oBrowse:SetDescription('Tabela de Materiais Puxador SESTINI')
oBrowse:Activate()

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �MenuDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Menu Funcional                                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MenuDef()     
Local aRotina := {}

ADD OPTION aRotina TITLE 'Pesquisar'			ACTION 'PesqBrw'				OPERATION 0							ACCESS 0
ADD OPTION aRotina TITLE 'Visualizar'			ACTION 'VIEWDEF.STNA100'		OPERATION MODEL_OPERATION_VIEW		ACCESS 0
ADD OPTION aRotina TITLE 'Incluir'				ACTION 'VIEWDEF.STNA100'		OPERATION MODEL_OPERATION_INSERT	ACCESS 0
ADD OPTION aRotina TITLE 'Alterar'				ACTION 'VIEWDEF.STNA100'		OPERATION MODEL_OPERATION_UPDATE	ACCESS 0
ADD OPTION aRotina TITLE 'Excluir'				ACTION 'VIEWDEF.STNA100'		OPERATION MODEL_OPERATION_DELETE	ACCESS 0

Return(aRotina)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ModelDef  �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao do Modelo                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ModelDef()
Local oStruct    := FWFormStruct(1, 'SZJ', {|cCampo| .T.})
Local oModel      := NIL
Local bCommit     := {|oModel| STNA100Commit(oModel)}

oModel:= MPFormModel():New('STNA100',, {|oModel| STNA100Pos(oModel)}, bCommit)

oModel:SetDescription('Tabela de Materiais Puxador SESTINI')
oModel:SetVldActivate({|oModel| STNA100Pre(oModel)})

oModel:AddFields('SZJMASTER', NIL, oStruct)
oModel:GetModel('SZJMASTER'):SetDescription('Tabela de Materiais Puxador SESTINI')
oModel:SetPrimaryKey({'ZJ_FILIAL+ZJ_CODIGO'})

Return(oModel)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �ViewDef   �Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Definicao da Visao                                          ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ViewDef()
Local oView  		:= Nil
Local oModel  		:= FWLoadModel('STNA100')
Local oStruct 	    := FWFormStruct(2, 'SZJ', {|cCampo| .T. })

//-----------------------------------------
//Monta o modelo da interface do formul�rio
//-----------------------------------------
oView := FWFormView():New()
oView:SetModel(oModel)   

oView:AddField('VIEW_SZJ', oStruct, 'SZJMASTER')

Return(oView)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA100Pre �Autor  �V. Raspa                                ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a ativacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA100Pre(oModel)
Local lRet := .T.

Return(lRet)

 /*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA100Pos�Autor  �V. Raspa                                 ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Valida a gravacao do formulario                             ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA100Pos(oModel)
Local lRet := .T.

Return(lRet)


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���FUNCAO    �STNA100Commit�Autor  �V. Raspa                              ���
�������������������������������������������������������������������������Ĵ��
���DESCRICAO �Realiza a gravacao do formulario                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function STNA100Commit(oModel)
Local lRet        := .T.
Default oModel    := FWModelActive()

// ---------------------------
// TRATA O DOCUMENTO VINCULADO
// ---------------------------
If oModel:GetOperation() == MODEL_OPERATION_DELETE

EndIf


// -------------------------------
// PROCESSA O COMMIT DO FORMULARIO
// -------------------------------
If lRet 
	lRet := FwFormCommit(oModel)
EndIf

Return(lRet)
