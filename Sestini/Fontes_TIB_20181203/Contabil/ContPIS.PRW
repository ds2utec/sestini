#Include "Protheus.ch"

//Fun��o que retorna PIS na inclus�o da nf de entrada.

User Function ContPIS()
	Local wret    := 0
	Local cCodUni := SuperGetMv("MV_UNIAO")  // CODIGO DO FORNECEDOR UNIAO  
	Local cNaturez:= SuperGetMv("MV_PISNAT")    // Natureza PIS
	
	SE2->(DbSetOrder(1))
	SE2->(DbSeek(xFilial("SE2") + SF1->F1_SERIE + SF1->F1_DOC))
	While !SE2->(Eof()) .and. SE2->E2_NUM == SF1->F1_DOC .and. SE2->E2_PREFIXO == SF1->F1_SERIE  
	   If AllTrim(SE2->E2_FORNECE) == cCodUni .AND. SE2->E2_LOJA == "00" .and. ALLTRIM(SE2->E2_NATUREZ) == ALLTRIM(cNaturez) .and. !EMPTY(SE2->E2_TITPAI)
	      wret+=SE2->E2_VALOR
	   Endif
		SE2->(DbSkip())
	End
Return wret