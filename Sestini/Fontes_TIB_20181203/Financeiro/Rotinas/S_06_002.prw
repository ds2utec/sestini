#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*
Funcao      : S_06_002
Parametros  : Nenhum
Retorno     : Nenhum
Objetivos   : Leitura e gera��o de arquivo Serasa relato conciliado. LAYOUT DE REMESSAS NORMAIS/CONCILIA��O.
Autor       : Ciro Pedreira
Cliente		: Sestini
Data/Hora   : 28/09/2018
*/

User Function S_06_002()

Local _oProcess

Local _aAreaMem := GetArea()
Local _bProcess := {|_oSelf| EXECUTA(_oSelf) }
Local _cPerg    := ''
Local _aInfo    := {}

_oProcess := tNewProcess():New("S_06_001", "Serasa relato concilia��o", _bProcess, "Rotina respons�vel pela leitura e gera��o do arquivo Serasa relato conciliado.", _cPerg, _aInfo, .T., 5, "Gera arquivo Serasa relato conciliado.", .T.)

RestArea(_aAreaMem)

Return

/*
Funcao      : EXECUTA
Parametros  : Objeto da classe tNewProcess.
Retorno     : Nenhum
Objetivos   : Gera��o de arquivo Serasa relato conciliado.
Autor       : Ciro Pedreira
Cliente		: Sestini
Data/Hora   : 28/09/2018
*/

Static Function EXECUTA(_oRegua)

Local _cPasta1  := 'c:\temp\'
Local _cPasta2  := 'serasa relato\'
Local _cArqTxt  := 'serasa_relato_conciliado_' + DToS(dDataBase) + '_' + StrTran(Time(), ':', '') + '.txt'
Local _nHdl     := 0
Local _nHdlC    := 0
Local _cLinha   := ''
Local _dDtFim   := CToD('//')
Local _cArqConc := ''
Local _nTamChav := 0
Local _aPerg    := {}
Local _cTela    := ''
Local _aRet     := {}

MakeDir(_cPasta1)
MakeDir(_cPasta1 + _cPasta2)

AAdd(_aPerg, {6, 'Arquivo de concilia��o', Space(200),,,, 90, .T., "Todos os arquivos |*.*", "", GETF_LOCALHARD+GETF_LOCALFLOPPY+GETF_NETWORKDRIVE})

If ParamBox(_aPerg, _cTela, @_aRet)
	
	_cArqConc := _aRet[1]
	
	_nHdlC := FOpen(_cArqConc, 0) // Abre o arquivo de concilia��o.
	
	If _nHdlC == -1
		
		MsgInfo("Ocorreu um erro na abertura do arquivo. Por favor, tente novamente.")
		
	Else
		
		_nHdl := FCreate(_cPasta1 + _cPasta2 + _cArqTxt, 0)
		
		If _nHdlC == -1
			
			MsgInfo("Ocorreu um erro na cria��o do arquivo. Por favor, tente novamente.")
			
		Else
			
			SET DELETED OFF // Habilita o uso com registros deletados.
			
			FSeek(_nHdlC, 0, 2) // Posiciona o ponteiro.
			FT_FUse(_cArqConc) // Deixa em uso o arquivo.
			FT_FGoTop() // Posiciona na primeira linha do arquivo.
			
			While !FT_FEOF() // Verifica todas as linhas at� o fim do arquivo.
				
				_cLinha := FT_FReadLN() // Obtem a linha posicionada no arquivo.
				
				If SubStr(_cLinha, 1, 2) == '00'
					
					_dDtFim := SToD(SubStr(_cLinha, 45, 8))
					
					If !(SubStr(_cLinha, 37, 8) == 'CONCILIA') // Verifica se o arquivo � de uma concilia��o do Serasa.
						
						FT_FUse() // Fecha o uso do arquivo.
						FClose(_nHdlC) // Fecha o arquivo de concilia��o enviado pela Serasa.
						
						FClose(_nHdl) // Fecha o novo arquivo que est� sendo gerado.
						
						_oRegua:SaveLog("Arquivo incompat�vel com a concilia��o. Usu�rio: " + AllTrim(UsrRetName(__cUserID)) + ". Arquivo: " + AllTrim(_cArqConc))
						
					EndIf
					
					FWrite(_nHdl, _cLinha)
					
				ElseIf SubStr(_cLinha, 1, 2) == '01' .And. SubStr(_cLinha, 17, 2) == '01' // Tempo de relacionamento.
					
					FWrite(_nHdl, _cLinha)
					
				ElseIf SubStr(_cLinha, 1, 2) == '01' .And. SubStr(_cLinha, 17, 2) == '05' // T�tulos.
					
					_nTamChav := TamSX3('E1_FILIAL')[1]+TamSX3('E1_PREFIXO')[1]+TamSX3('E1_NUM')[1]+TamSX3('E1_PARCELA')[1]+TamSX3('E1_TIPO')[1]
					
					If SE1->(DbSeek(SubStr(_cLinha, 68, _nTamChav)))
						
						If SubStr(_cLinha, 58, 8) == '99999999' // Serasa envia 99999999.
							
							If SE1->(Deleted())
								
								// No caso de titulo excluido, � necess�rio enviar 9999999999999 no campo de valor para a exclus�o
								// do titulo da base de dados do Serasa.
								_cLinha := SubStr(_cLinha, 1, 36) + '9999999999999' + SubStr(_cLinha, 50)
								
								// Limpo a posi��o de data de pagamento.
								_cLinha := SubStr(_cLinha, 1, 57) + Space(8) + SubStr(_cLinha, 66)
								
							Else
								
								If SE1->E1_SALDO == 0 // Verifica se o titulo foi pago.
									
									If SE1->E1_BAIXA <= _dDtFim // A data de pagamento n�o pode ser superior a data final do periodo do arquivo.
										
										// No caso de titulo pago, troca-se 99999999 pela data do pagamento no formato AAAAMMDD.
										_cLinha := SubStr(_cLinha, 1, 57) + DToS(SE1->E1_BAIXA) + SubStr(_cLinha, 66)
										
									ElseIf Empty(SE1->E1_BAIXA)
										
										// No caso de titulo em aberto, troca-se 99999999 por espa�os.
										_cLinha := SubStr(_cLinha, 1, 57) + Space(8) + SubStr(_cLinha, 66)
										
									EndIf
									
								EndIf
								
							EndIf
							
						EndIf
						
						FWrite(_nHdl, _cLinha)
						
					EndIf
					
				ElseIf SubStr(_cLinha, 1, 2) == '99' // Trailler.
					
					FWrite(_nHdl, _cLinha)
					
				EndIf
				
				FT_FSkip() // Pula linha.
				
			EndDo
			
			FT_FUse() // Fecha o uso do arquivo.
			FClose(_nHdlC) // Fecha o arquivo de concilia��o enviado pela Serasa.
			
			FClose(_nHdl) // Fecha o novo arquivo que est� sendo gerado.
			
			SET DELETED ON // Desabilita o uso de registros deletados.
			
			_oRegua:SaveLog("Arquivo gerado com sucesso. Usu�rio: " + AllTrim(UsrRetName(__cUserID)) + ". Caminho: " + _cPasta1 + _cPasta2 + _cArqTxt)
			
		EndIf
		
	EndIf
	
EndIf

Return