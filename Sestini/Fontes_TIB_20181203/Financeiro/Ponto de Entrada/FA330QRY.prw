#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*
Funcao      : FA330QRY
Parametros  : Query SQL para filtro dos titulos.
Retorno     : Query SQL para filtro dos titulos.
Objetivos   : Ponto de entrada utilizado para manipular a query SQL do filtro dos titulos para compensa��o do contas a receber.
            : TDN: http://tdn.totvs.com/display/public/PROT/FA330QRY+-+Manipula+Query
Autor       : Ciro Pedreira
Cliente		: Sestini
Data/Hora   : 03/10/2018
*/

User Function FA330QRY()

Local _aAreaMem := GetArea()
Local _cQuery   := Paramixb[1]
Local _aPerg    := {}
Local _cTela    := '' //'Filtro por descri��o do Grupo Matriz'
Local _nTamDesc := TamSX3('A1_NREDUZ')[1]
Local _cNovoSQL := ''
Local _aBkpMV   := {}
Local _nB       := 0
Local _lTela    := .T.

Private _aRet := {}

// ATEN��O:
// Realizar o backup das variaveis MV_, pois a fun��o ParamBox tamb�m utiliza as mesmas e assim altera as caracteristicas originais dessas variaveis.
AAdd(_aBkpMV, {'MV_PAR01', MV_PAR01})
AAdd(_aBkpMV, {'MV_PAR02', MV_PAR02})

//If XB_ALIAS+XB_TIPO+XB_SEQ+XB_COLUNA

AAdd(_aPerg, {1, 'Nome Fantasia De', Space(_nTamDesc), PesqPict('SA1', 'A1_NREDUZ'),, 'SA1_X',, _nTamDesc, .F.})
AAdd(_aPerg, {1, 'At� o Nome Fantasia', Replicate('Z', _nTamDesc), PesqPict('SA1', 'A1_NREDUZ'),, 'SA1_X',, _nTamDesc, .T.})

While _lTela
	
	If ParamBox(_aPerg, _cTela, @_aRet)
		
		_nPos1 := At('LEFT JOIN SA1', _cQuery)
		_nPos2 := At('WHERE', _cQuery)
		
		If _nPos1 > 0 .And. _nPos2 > 0
			
			If Empty(_aRet[1]) .And. 'ZZZ' $ _aRet[2]
				
				If MsgYesNo('Foi definido um range muito grande de grupos matriz, isso pode deixar o processo lento. Deseja continuar?', 'ATEN��O')
					
					_lTela := .F. // Sai do loop.
					
				Else
					
					Loop
					
				EndIf
				
			EndIf
			
			// Primeiro ajuste na query SQL.
			// Altera condi��o do JOIN com o cadastro de clientes para que a nova condi��o por Grupo Matriz funcione corretamente.
			_cQuery := StrTran(_cQuery, 'LEFT JOIN SA1', 'INNER JOIN SA1')
			
			// Segundo ajuste na query SQL.
			// Adiciona filtro por Grupo Matriz.
			/*_cNovoSQL += "INNER JOIN " + RetSQLName("ACY") + " ACY ON "
			_cNovoSQL += "ACY.D_E_L_E_T_ = ' ' "
			_cNovoSQL += "AND ACY.ACY_FILIAL = '" + xFilial('ACY') + "' "
			_cNovoSQL += "AND ACY.ACY_DESCRI >= '" + _aRet[1] + "' "
			_cNovoSQL += "AND ACY.ACY_DESCRI <= '" + _aRet[2] + "' "
			_cNovoSQL += "AND ACY.ACY_GRPVEN = SA1.A1_GRPVEN "
			_cNovoSQL += "WHERE"*/
			
			_cNovoSQL += "AND SA1.A1_NREDUZ >= '" + _aRet[1] + "' "
			_cNovoSQL += "AND SA1.A1_NREDUZ <= '" + _aRet[2] + "' "
			_cNovoSQL += "WHERE"
			
			_cQuery := StrTran(_cQuery, 'WHERE', _cNovoSQL)
			
		Else
			
			MsgAlert('N�o foi possivel alterar a query SQL desta rotina! Por favor, solicitar ajuste no ponto de entrada FA330QRY para a equipe de TI.', 'ATEN��O')
			
		EndIf
		
		_lTela := .F. // Sai do loop.
		
	Else
		
		If MsgYesNo('Como a tela de grupo matriz foi cancelada, essa informa��o n�o far� parte do filtro dos titulos. Deseja continuar?', 'ATEN��O')
			
			_lTela := .F. // Sai do loop.
			
		EndIf
		
	EndIf
	
EndDo

// Restaura as variaveis F12 da rotina.
For _nB := 1 To Len(_aBkpMV)
	
	&(_aBkpMV[_nB][1]) := _aBkpMV[_nB][2]
	
Next _nB

MemoWrite('c:\temp\sestini\fa330qry.sql', _cQuery)

RestArea(_aAreaMem)

Return _cQuery

/*
Funcao      : S_06_003
Parametros  : Nenhum.
Retorno     : Logico.
Objetivos   : Monta tela de consulta padr�o para uso na customiza��o do Grupo Matriz na tela de Compensa��o CR.
Autor       : Ciro Pedreira
Cliente		: Sestini
Data/Hora   : 03/10/2018
*/

User Function S_06_003()

Local _oDlgCons := Nil // Objeto Tela.
Local _oListBox := Nil // Objeto Listbox com as opcoes.
Local _nPosLbx  := 0 // Posicao do Listbox.
Local _nPos     := 0 // Contador.          
Local _aItens   := {} // Array dos itens.
Local _lRet     := .F. // Retorno da funcao.
Local _cQuery   := ''

CursorWait()

/*_cQuery := "SELECT ACY_GRPVEN, ACY_DESCRI "
_cQuery += "FROM " + RetSQLName('ACY') + " ACY "
_cQuery += "WHERE ACY.D_E_L_E_T_ = '' "
_cQuery += "AND ACY.ACY_FILIAL = '" + xFilial('ACY') + "' "
_cQuery += "AND ACY.ACY_DESCRI LIKE '%" + AllTrim(&(ReadVar())) + "%' "
_cQuery += "ORDER BY ACY_DESCRI"*/

_cQuery := "SELECT A1_COD, A1_LOJA, A1_NREDUZ, A1_NOME "
_cQuery += "FROM " + RetSQLName('SA1') + " SA1 "
_cQuery += "WHERE SA1.D_E_L_E_T_ = '' "
_cQuery += "AND SA1.A1_FILIAL = '" + xFilial('SA1') + "' "
_cQuery += "AND SA1.A1_NREDUZ LIKE '%" + AllTrim(&(ReadVar())) + "%' "
_cQuery += "ORDER BY A1_NREDUZ"

TcQuery _cQuery New Alias 'S_06_003'

S_06_003->(DbGoTop())

While S_06_003->(!EOF())
	
	AAdd(_aItens, {S_06_003->A1_NREDUZ, S_06_003->A1_COD, S_06_003->A1_LOJA, S_06_003->A1_NOME})
	
	S_06_003->(DbSkip())
	
EndDo

S_06_003->(DbCloseArea())

CursorArrow()

// Se n�o encontrou grupos, n�o mostra tela de consulta.
If Len(_aItens) <= 0
	
	MsgStop("N�o foram encontrados grupos com a descri��o informada.", "ATEN��O")
	
	Return _lRet
	
EndIf

DEFINE MSDIALOG _oDlgCons FROM 0, 0 TO 230, 440 TITLE "Consulta por Grupo Matriz" PIXEL
	
	@ 05, 05 LISTBOX _oListBox VAR _nPosLbx FIELDS HEADER "Nome fantasia", "C�digo", "Loja", "Raz�o social" SIZE 180, 100 OF _oDlgCons PIXEL NOSCROLL
	_oListBox:SetArray(_aItens)
    _oListBox:bLine := {|| {_aItens[_oListBox:nAt, 1],;
						    _aItens[_oListBox:nAt, 2],;
						    _aItens[_oListBox:nAt, 3],;
						    _aItens[_oListBox:nAt, 4]}}
	
    _oListBox:BlDblClick := {||(_lRet := .T., _nPos := _oListBox:nAt, _oDlgCons:End()) }
	_oListBox:Refresh()
	
    DEFINE SBUTTON FROM 05, 190 TYPE 1 ENABLE OF _oDlgCons ACTION (_lRet := .T., _nPos := _oListBox:nAt, _oDlgCons:End()) // Ok.
    
    DEFINE SBUTTON FROM 20, 190 TYPE 2 ENABLE OF _oDlgCons ACTION (_lRet := .F., _oDlgCons:End()) // Cancelar.
    
    //DEFINE SBUTTON FROM 35, 190 TYPE 4 ENABLE OF _oDlgCons ACTION (_lRet := .F., S_Pesq3(), _oDlgCons:End()) // Pesquisar.
    
ACTIVATE MSDIALOG _oDlgCons CENTERED

// Posiciona no item selecionado na tela.
If _lRet
	
	SA1->(DbSetOrder(1)) // Indice 1 - A1_FILIAL+A1_COD+A1_LOJA
	SA1->(DbSeek(xFilial('SA1')+_aItens[_nPos][2]+_aItens[_nPos][3]))
	
EndIf

Return _lRet
/*
Static Function S_Pesq3(_oListBox, _aItens)

Local nAscan := 0
Local _oDlg
Local _nOpca := 0  
Local nPosAux	:= 0
Local _nA := 0
Local nB			:= 0
Local nList		:= 1
Local lFind := .F.
Local _cTexto := Space(TamSX3('ACY_DESCRI')[1])

DEFINE MSDIALOG _oDlg TITLE 'Pesquisa por grupo matriz' From 9,2 To 15,70 OF oMainWnd
	@  19, 05 Say 'Descri��o' OF _oDlg PIXEL SIZE 50,9  
	@  19, 55 MSGET _cTexto  OF _oDlg PIXEL PICTURE "@!"
ACTIVATE MSDIALOG _oDlg ON INIT EnchoiceBar(_oDlg,{|| _nOpca := 1, _oDlg:End() },{ || _nOpca := 0, _oDlg:End()}) CENTER

If nOpca == 1 
	
	For _nA := 1 To Len(_aItens)
		
 		_lFind  := Upper(AllTrim(_cTexto)) $ Upper(AllTrim(_aItens[_nA, 1]))
		
		If _lFind
			
			Exit
			
		EndIf
		
	Next _nA
	
   	// Se encontrou, atualiza a linha do objeto.
   	If _nA > 0 .And. _nA <= Len(_aItens)
   		
   		_oListBox:nAt := _nA
   		_oListBox:Refresh(.T.)
   		
   	EndIf
	
EndIf

Return*/