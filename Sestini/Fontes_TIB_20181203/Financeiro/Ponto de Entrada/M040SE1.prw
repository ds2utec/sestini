#Include "Totvs.ch"
#INCLUDE "PROTHEUS.CH"

/*
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
±±º Programa  M040SE1    Autor  Rogerio Machado          Data 18/05/2018   º±±
±±º Desc.                                                                  º±±
ÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜÜ
*/

User Function M040SE1()

	DbSelectArea("SC5")
	DbSetOrder(1)
	If SC5->(DbSeek(xFilial("SC5") + SD2->D2_PEDIDO)) .AND. SC5->C5_XCANAL == 'E' .AND. SC5->C5_XTIPO <> 'BOL'
		DbSelectArea("ZA6")
		DbSetOrder(1)
		If ZA6->(DbSeek(xFilial("ZA6")))		
		DbSelectArea("SA1")
		DbSetOrder(1)
		SA1->(DbSeek(xFilial("SA1") + ZA6->ZA6_CLIENT + ZA6->ZA6_LOJA))
	
			SE1->(RecLock("SE1",.F.))
				SE1->E1_NSUTEF    := SC5->C5_XNSU
				SE1->E1_DOCTEF    := SC5->C5_XNSU 
				SE1->E1_CARTAUT   := SC5->C5_XCARAUT
				SE1->E1_TIPO      := SC5->C5_XTIPO
				SE1->E1_CLIENTE   := ZA6->ZA6_CLIENT
				SE1->E1_LOJA      := ZA6->ZA6_LOJA
				SE1->E1_NOMCLI    := ALLTRIM(SA1->A1_NREDUZ)
			SE1->(MsUnlock())

		EndIf

	EndIf

Return