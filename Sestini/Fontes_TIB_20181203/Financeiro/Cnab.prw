#Include "Rwmake.ch"                                                                                                               

//Preenchimento da posi��o 140 a 147 do cnab Bradesco

User Function BradAC()

Local cRet	:= ""
Local cAgenc:= ""
Local cBanco:= ""
Local cDigito:= ""

cBanco := SA6->A6_NUMBCO
cAgenc := Strzero(Val(TiraAcento(SA6->A6_AGENCIA)),4)
cDigito:= Strzero(Val(SA6->A6_DVAGE),1) 

cRet	:= IIF(SE1->E1_OCORREN$Alltrim("  ,01"),Alltrim(cBanco)+Alltrim(cAgenc)+Alltrim(cDigito),Strzero(0,8))

Return(cRet)


Static Function TiraAcento( cTexto )

Local cRet1 := ""
Local aChr  := {}
Local i     := 0
Local cChar := ""
Local nElem := 0

/* Maiusculo */
aadd( aChr, { "�","A" } )
aadd( aChr, { "�","E" } )
aadd( aChr, { "�","I" } )
aadd( aChr, { "�","O" } )
aadd( aChr, { "�","U" } )
aadd( aChr, { "�","C" } )
aadd( aChr, { "�","A" })
aadd( aChr, { "�","A"})
aadd( aChr, {"�","E" } )
aadd( aChr, { "�","I" } )
aadd( aChr, { "�","O" } )
aadd( aChr, { "�","U" } )
aadd( aChr, { "'�","A" } )
aadd( aChr, { "�","E" } )
aadd( aChr, { "�","I" } )
aadd( aChr, { "�","O" } )
aadd( aChr, { "�","U" } )
aadd( aChr, { "'","" } )
aadd( aChr, { "-","" } )
aadd( aChr, { ".","" } )
aadd( aChr, { "**","" } )
aadd( aChr, { "/","" } )
aadd( aChr, { "*","" } )
aadd( aChr, { "=","" } )
aadd( aChr, { "..","" } )
aadd( aChr, { "#","" } )
aadd( aChr, { "(","" } )
aadd( aChr, { ")","" }  )
aadd( aChr, { "{","" }  )
aadd( aChr, { "}","" } )
aadd( aChr, { "\","" } )
aadd( aChr, { "@","" } )
aadd( aChr, { "+","" } )
aadd( aChr, { "<","" } )
aadd( aChr, { ">","" } )
aadd( aChr, { ":","" } )
aadd( aChr, { ";","" } )

/* Minusculo */
aadd( aChr, { "�","a" } )
aadd( aChr, { "�","e" } )
aadd( aChr, { "�","i" } )
aadd( aChr, { "�","o" } )
aadd( aChr, { "�","u" } )
aadd( aChr, { "�","c" } )
aadd( aChr, { "�","a" } )
aadd( aChr, { "�","a"})
aadd( aChr, {"�","e" } )
aadd( aChr, { "�","i" } )
aadd( aChr, { "�","o" } )
aadd( aChr, { "�","u" } )
aadd( aChr, { "'�","a" } )
aadd( aChr, { "�","e" } )
aadd( aChr, { "�","i" } )
aadd( aChr, { "�","o" } )
aadd( aChr, { "�","u" } )
aadd( aChr, { "-","" } )

For i := 1 to len( cTexto )
	
	cChar := Subst( cTexto, i, 1 )
	nElem := aScan( aChr,{ |x| Upper(x[1]) == Upper(cChar)} )
	If nElem > 0
		cChar := aChr[ nElem, 2]
	Endif
	cRet1 += cChar
	
Next

Return( cRet1 )                                                               

#INCLUDE "PROTHEUS.CH"              


/*���������������������������������������������������������������������������
���Programa  �CAMMOD    												   ��
�������������������������������������������������������������������������͹��
���Desc.     � Fonte para tratamento do Codigo da Camara Centralizadora.  ���
���			 � "000" = Credito em Conta									  ���
���			 � "700" = Doc												  ���
���			 � "018" = Ted Cip											  ���
���			 � "810" = Ted Str                                            ���
���			 � Layout Santander PAGFOR 240 Posi��es (018 a 020).		  ���
�������������������������������������������������������������������������͹��
���Uso       � 					                          ���
���������������������������������������������������������������������������*/
  

User Function CAMMOD()

Local cRet := ""

If SEA->EA_MODELO=="03"
   cRet := "700"
ElseIf SEA->EA_MODELO=="01"
   cRet := "000"
ElseIf SEA->EA_MODELO$"41/43"
   cRet := "018"
Else
   cRet := "810"
EndIf
Return (cRet) 

#include "rwmake.ch"
          

User Function CNPJCPF()

Local _cCNPJCPF := Padl(AllTrim(SA2->A2_CGC),15,"0")
//If SA2->A2_TIPO # "J"
If Len(AllTrim(SA2->A2_CGC)) == 11
	_cCNPJCPF := Left(SA2->A2_CGC,9)+"0000"+SubStr(SA2->A2_CGC,10,2)
EndIf

Return(_cCNPJCPF)  



#include "rwmake.ch"

///--------------------------------------------------------------------------\
//| Fun��o: CODBAR     														 |
//|--------------------------------------------------------------------------|
//| Essa Fun��o foi desenvolvida com base no Manual do Bco. Ita� e no RDMAKE:|
//|--------------------------------------------------------------------------|
//| Descri��o: Fun��o para Valida��o de C�digo de Barras (CB) e Representa��o|
//|            Num�rica do C�digo de Barras - Linha Digit�vel (LD).    	     |
//|                                                                          |
//|            A LD de Bloquetos possui tr�s Digitos Verificadores (DV) que  |
//|	       s�o consistidos pelo M�dulo 10, al�m do D�gito Verificador    |
//|		eral (DVG) que � consistido pelo M�dulo 11. Essa LD t�m 47   |
//|            D�gitos.                                                      |
//|                                                                          |
//|            A LD de T�tulos de Concessin�rias do Servi�o P�blico e IPTU   |
//|		ossui quatro Digitos Verificadores (DV) que s�o consistidos  |
//|            pelo M�dulo 10, al�m do Digito Verificador Geral (DVG) que    |
//|            tamb�m � consistido pelo M�dulo 10. Essa LD t�m 48 D�gitos.   |
//|                                                                          |
//|            O CB de Bloquetos e de T�tulos de Concession�rias do Servi�o  |
//|            P�blico e IPTU possui apenas o D�gito Verificador Geral (DVG) |
//|            sendo que a �nica diferen�a � que o CB de Bloquetos �         |
//|            consistido pelo M�dulo 11 enquanto que o CB de T�tulos de     |
//|            Concession�rias � consistido pelo M�dulo 10. Todos os CB�s    |
//|            t�m 44 D�gitos.                                               |
//|                                                                          |
//|            Para utiliza��o dessa Fun��o, deve-se criar o campo E2_CODBAR,|
//|            Tipo Caracter, Tamanho 48 e colocar na Valida��o do Usu�rio:  |
//|            EXECBLOCK("CODBAR",.T.).                                      |
//|                                                                          |
//|            Utilize tamb�m o gatilho com a Fun��o CONVLD() para converter |
//|            a LD em CB.																     |
//\--------------------------------------------------------------------------/

USER FUNCTION CodBar()

Local i := 1
SETPRVT("cStr,lRet,cTipo,nConta,nMult,nVal,nDV,cCampo,i,nMod,nDVCalc")

//If inclui==.T.
	// Retorna .T. se o Campo estiver em Branco.
	IF VALTYPE(M->E2_CODBAR) == NIL .OR. EMPTY(M->E2_CODBAR)
		RETURN(.T.)
	ENDIF
	
	cStr := LTRIM(RTRIM(M->E2_CODBAR))
/*
Else 
	// Retorna .T. se o Campo estiver em Branco.
	IF VALTYPE(SE2->E2_CODBAR) == NIL .OR. EMPTY(SE2->E2_CODBAR)
		RETURN(.T.)
	ENDIF
	
	cStr := LTRIM(RTRIM(SE2->E2_CODBAR))
Endif
*/

// Se o Tamanho do String for 45 ou 46 est� errado! Retornar� .F.
lRet := IF(LEN(cStr)==45 .OR. LEN(cStr)==46,.F.,.T.)

// Se o Tamanho do String for menor que 44, completa com zeros at� 47 d�gitos. Isso �
// necess�rio para Bloquetos que N�O t�m o vencimento e/ou o valor informados na LD.
cStr := IF(LEN(cStr)<44,cStr+REPL("0",47-LEN(cStr)),cStr)

// Verifica se a LD � de (B)loquetos ou (C)oncession�rias/IPTU. Se for CB retorna (I)ndefinido.
cTipo := IF(LEN(cStr)==47,"B",IF(LEN(cStr)==48,"C","I"))

// Verifica se todos os d�gitos s�o num�rios.
FOR i := LEN(cStr) TO 1 STEP -1
	lRet := IF(SUBSTR(cStr,i,1) $ "0123456789",lRet,.F.)
NEXT

IF LEN(cStr) == 47 .AND. lRet
	// Consiste os tr�s DV�s de Bloquetos pelo M�dulo 10.
	nConta  := 1
	WHILE nConta <= 3
		nMult  := 2
		nVal   := 0
		nDV    := VAL(SUBSTR(cStr,IF(nConta==1,10,IF(nConta==2,21,32)),1))
		cCampo := SUBSTR(cStr,IF(nConta==1,1,IF(nConta==2,11,22)),IF(nConta==1,9,10))
		FOR i := LEN(cCampo) TO 1 STEP -1
			nMod  := VAL(SUBSTR(cCampo,i,1)) * nMult
			nVal  := nVal + IF(nMod>9,1,0) + (nMod-IF(nMod>9,10,0))
			nMult := IF(nMult==2,1,2)
		NEXT
		nDVCalc := 10-MOD(nVal,10)
		// Se o DV Calculado for 10 � assumido 0 (Zero).
		nDVCalc := IF(nDVCalc==10,0,nDVCalc)
		lRet    := IF(lRet,(nDVCalc==nDV),.F.)
		nConta  := nConta + 1			
	ENDDO
	// Se os DV�s foram consistidos com sucesso (lRet=.T.), converte o n�mero para CB para consistir o DVG. 
  	cStr := IF(lRet,SUBSTR(cStr,1,4)+SUBSTR(cStr,33,15)+SUBSTR(cStr,5,5)+SUBSTR(cStr,11,10)+SUBSTR(cStr,22,10),cStr)
ENDIF

IF LEN(cStr) == 48 .AND. lRet
	// Consiste os quatro DV�s de T�tulos de Concession�rias de Servi�o P�blico e IPTU pelo M�dulo 10.
	nConta  := 1
	WHILE nConta <= 4
		nMult  := 2
		nVal   := 0
		nDV    := VAL(SUBSTR(cStr,IF(nConta==1,12,IF(nConta==2,24,IF(nConta==3,36,48))),1))
		cCampo := SUBSTR(cStr,IF(nConta==1,1,IF(nConta==2,13,IF(nConta==3,25,37))),11)
		FOR i := 11 TO 1 STEP -1
			nMod  := VAL(SUBSTR(cCampo,i,1)) * nMult
			nVal  := nVal + IF(nMod>9,1,0) + (nMod-IF(nMod>9,10,0))
			nMult := IF(nMult==2,1,2)
		NEXT
		nDVCalc := 10-MOD(nVal,10)
		// Se o DV Calculado for 10 � assumido 0 (Zero).
		nDVCalc := IF(nDVCalc==10,0,nDVCalc)
		lRet    := IF(lRet,(nDVCalc==nDV),.F.)
		nConta  := nConta + 1			
	ENDDO
	// Se os DV�s foram consistidos com sucesso (lRet=.T.), converte o n�mero para CB para consistir o DVG. 
  	cStr := IF(lRet,SUBSTR(cStr,1,11)+SUBSTR(cStr,13,11)+SUBSTR(cStr,25,11)+SUBSTR(cStr,37,11),cStr)
ENDIF

IF LEN(cStr) == 44 .AND. lRet
	IF cTipo $ "BI"
		// Consiste o DVG do CB de Bloquetos pelo M�dulo 11.
		nMult  := 2
		nVal   := 0
		nDV    := VAL(SUBSTR(cStr,5,1))
		cCampo := SUBSTR(cStr,1,4)+SUBSTR(cStr,6,39)
		FOR i := 43 TO 1 STEP -1
			nMod  := VAL(SUBSTR(cCampo,i,1)) * nMult
			nVal  := nVal + nMod
			nMult := IF(nMult==9,2,nMult+1)
		NEXT
		nDVCalc := 11-MOD(nVal,11)
		// Se o DV Calculado for 0,10 ou 11 � assumido 1 (Um).
		nDVCalc := IF(nDVCalc==0 .OR. nDVCalc==10 .OR. nDVCalc==11,1,nDVCalc)		
		lRet    := IF(lRet,(nDVCalc==nDV),.F.)
		// Se o Tipo � (I)ndefinido E o DVG N�O foi consistido com sucesso (lRet=.F.), tentar�
		// consistir como CB de T�tulo de Concession�rias/IPTU no IF abaixo.  
	ENDIF
	IF cTipo == "C" .OR. (cTipo == "I" .AND. !lRet)
		// Consiste o DVG do CB de T�tulos de Concession�rias pelo M�dulo 10.
		lRet   := .T.
		nMult  := 2
		nVal   := 0
		nDV    := VAL(SUBSTR(cStr,4,1))
		cCampo := SUBSTR(cStr,1,3)+SUBSTR(cStr,5,40)
		FOR i := 43 TO 1 STEP -1
			nMod  := VAL(SUBSTR(cCampo,i,1)) * nMult
			nVal  := nVal + IF(nMod>9,1,0) + (nMod-IF(nMod>9,10,0))
			nMult := IF(nMult==2,1,2)
		NEXT
		nDVCalc := 10-MOD(nVal,10)
		// Se o DV Calculado for 10 � assumido 0 (Zero).
		nDVCalc := IF(nDVCalc==10,0,nDVCalc)
		lRet    := IF(lRet,(nDVCalc==nDV),.F.)
	ENDIF
ENDIF

IF !lRet
	HELP(" ",1,"ONLYNUM")
ENDIF

RETURN(lRet)  



#include "rwmake.ch"

///--------------------------------------------------------------------------\
//| Fun��o: CONVLD		            										 |
//|--------------------------------------------------------------------------|
//| Descri��o: Fun��o para Convers�o da Representa��o Num�rica do C�digo de  |
//|            Barras - Linha Digit�vel (LD) em C�digo de Barras (CB).       |
//|                                                                          |
//|            Para utiliza��o dessa Fun��o, deve-se criar um Gatilho para o |
//|            campo E2_CODBAR, Conta Dom�nio: E2_CODBAR, Tipo: Prim�rio,    |
//|            Regra: EXECBLOCK("CONVLD",.T.), Posiciona: N�o.               |
//|                                                                          |
//|            Utilize tamb�m a Valida��o do Usu�rio para o Campo E2_CODBAR  |
//|            EXECBLOCK("CODBAR",.T.) para Validar a LD ou o CB.            |
//\--------------------------------------------------------------------------/


#include "rwmake.ch"

///--------------------------------------------------------------------------\
//| Fun��o: CONVLD															 |
//|--------------------------------------------------------------------------|
//| Descri��o: Fun��o para Convers�o da Representa��o Num�rica do C�digo de  |
//|            Barras - Linha Digit�vel (LD) em C�digo de Barras (CB).       |
//|                                                                          |
//|            Para utiliza��o dessa Fun��o, deve-se criar um Gatilho para o |
//|            campo E2_CODBAR, Conta Dom�nio: E2_CODBAR, Tipo: Prim�rio,    |
//|            Regra: EXECBLOCK("CONVLD",.T.), Posiciona: N�o.               |
//|                                                                          |
//|            Utilize tamb�m a Valida��o do Usu�rio para o Campo E2_CODBAR  |
//|            EXECBLOCK("CODBAR",.T.) para Validar a LD ou o CB.            |
//\--------------------------------------------------------------------------/

User function CONVLD()
                      
SETPRVT("cStr","cStr1","cStr2","cStr3")

//If inclui==.T.
   	cStr3 := SUBSTR(M->E2_LINDG,1,60)   // pega o numero completo com todos os espa�os se houver
   	cStr1 := ALLTRIM(M->E2_LINDG)       // pega e tira os espa�os do come�o e do fim....
   	cStr2 := STRTRAN(M->E2_LINDG," ","",1) // transforma o espa�o em vazio
   	cStr  := SUBSTR(cStr2,1,48)     //tem que trazer o numero sem nenhum espa�o
   	cStr := STRTRAN(M->E2_LINDG," ")
	IF VALTYPE(M->E2_LINDG) == NIL .OR. EMPTY(M->E2_LINDG)
		// Se o Campo est� em Branco n�o Converte nada.
		cStr := ""
   //	ELSEIF SE2->E2_LINDG>48
     //   cStr :=ALLTRIM(SE2->(E2_LINDG, 1,11)+(E2_LINDG,13,1)+(E2_LINDG,15,11)+(E2_LINDG,27,1)+(E2_LINDG,29,11)+(E2_LINDG,41,1)+(E2_LINDG,43,11)+(E2_LINDG,55,1))
    
	ELSE
		// Se o Tamanho do String for menor que 44, completa com zeros at� 47 d�gitos. Isso �
		// necess�rio para Bloquetos que N�O t�m o vencimento e/ou o valor informados na LD.
		cStr := IF(LEN(cStr)<44,cStr+REPL("0",47-LEN(cStr)),cStr)
	ENDIF
/*
Else
	cStr := ALLTRIM(SE2->E2_LINDG)
	IF VALTYPE(SE2->E2_LINDG) == NIL .OR. EMPTY(SE2->E2_LINDG)
		// Se o Campo est� em Branco n�o Converte nada.
		cStr := ""
	ELSE
		// Se o Tamanho do String for menor que 44, completa com zeros at� 47 d�gitos. Isso �
		// necess�rio para Bloquetos que N�O t�m o vencimento e/ou o valor informados na LD.
		cStr := IF(LEN(cStr)<44,cStr+REPL("0",47-LEN(cStr)),cStr)
	ENDIF
Endif
*/

DO CASE
CASE LEN(cStr) == 47
	cStr := SUBSTR(cStr,1,4)+SUBSTR(cStr,33,15)+SUBSTR(cStr,5,5)+SUBSTR(cStr,11,10)+SUBSTR(cStr,22,10)
CASE LEN(cStr) == 48
   cStr := SUBSTR(cStr,1,11)+SUBSTR(cStr,13,11)+SUBSTR(cStr,25,11)+SUBSTR(cStr,37,11)
OTHERWISE
	cStr := cStr+SPACE(48-LEN(cStr))
ENDCASE

RETURN(cStr)
                      
	
#INCLUDE "PROTHEUS.CH"

/*���������������������������������������������������������������������������
���Programa  �HSBCAGC     �Autor  �smartins          � Data �  18/07/12   ���
�������������������������������������������������������������������������͹��
���Desc.     � Programa para carregar no arquivo de Cobran�a HSBC as      ���
���          � Posi��es 34 a 44 do Header e 25 a 35 do Detalhe do arquivo.���
�������������������������������������������������������������������������͹��
���Uso       � 			                                          ���
���������������������������������������������������������������������������*/

User Function HsbcAgc()

Local cConta := ""
If AllTrim(SA6->A6_COD) $ "399"
	cConta := PadL(SUBSTR(SA6->A6_AGENCIA,1,4),4,"0") // 34 a 37
	cConta += PadL(SUBSTR(SA6->A6_NUMCON,1,6),6,"0")   // 38 a 43
	cConta += PadL(SUBSTR(SA6->A6_DVCTA,1,1),1,"0")   // 44 a 44	
Else // Outros Bancos
	cConta := PadL(AllTrim(StrTran(SA6->A6_AGENCIA,"-","")),5,"0") // 34 a 37
	cConta += SubStr(PadL(Left(AllTrim(StrTran(SA6->A6_NUMCON,"-","")),Len(AllTrim(StrTran(SA6->A6_NUMCON,"-","")))-1),10,"0"),1,10) // 38 a 43
	cConta += PadL(SUBSTR(SA6->A6_DVCTA,1,1),1,"0")   // 44 a 44
EndIf
Return cConta    



User Function ItauAgc()
Local cAgencia	:= ""

If Alltrim(SA2->A2_BANCO)$"341/409"
	cAgencia:="0"+PADL(ALLTRIM(STRTRAN(SA2->A2_AGENCIA,"-","")),4,"0")+" "+"000000"+PADL(ALLTRIM(STRTRAN(SA2->A2_NUMCON,"-","")),6,"0")+" "+PADL(RIGHT(TRIM(SA2->A2_DVCTA),1),1,"0")
Else 
	cAgencia:=PADL(ALLTRIM(STRTRAN(SA2->A2_AGENCIA,"-","")),5,"0")+" "+PADL(ALLTRIM(STRTRAN(SA2->A2_NUMCON,"-","")),12,"0")+" "+PADL(RIGHT(TRIM(SA2->A2_DVCTA),1),1,"0")
Endif

Return(cAgencia)    



#include "protdef.ch"
#include "rwmake.ch" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � ItauTrib    � Autor � smartins		� Data � 28/03/13 ���
�������������������������������������������������������������������������Ĵ��
�������������������������������������������������������������������������Ĵ��
��� Uso      � CNAB SISPAG - Banco do Itau                                ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function ItauTrib()                      

Local _cRet		:=""
Local _cCodUF	:=""
Local _cCodMun	:=""
Local _cCodPla	:=""


If SEA->EA_MODELO$"17" //Pagamento GPS
	_cRet:="01"																//Codigo do tributo / pos. 018-019
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023	
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Competencia / pos. 024-029
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 030-043
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento do INSS / 044-057
	_cRet+=STRZERO(SE2->E2_ACRESC*100,14)									//Valor somado ao valor do documento / 058-071
	_cRet+=REPL("0",14)														//Atualiza��o monetaria /	072-085
	_cRet+=STRZERO((SE2->E2_SALDO+SE2->E2_ACRESC)*100,14)					//Valor total arrecadado / 086-099
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data da arrecada��o / 100-107
	_cRet+=Space(08)														//Brancos / 108-115
	_cRet+=Space(50)														//Uso da empresa / 116-165
	_cRet+=Substr(SM0->M0_NOMECOM,1,30)										//Nome do Contribuinte / 166-195
	

Elseif SEA->EA_MODELO$"16" //Pagamento de Darf Normal
	_cRet:="02"																//Codigo do tributo / pos. 018-019	
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 039-046
	_cRet+=IIF(EMPTY(SE2->E2_ESNREF),STRZERO(0,17),STRZERO(VAL(SUBSTR(SE2->E2_ESNREF,1,17)),17))	//Numero de referencia / 047-063	
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor Principal / 064-077
	_cRet+=STRZERO(SE2->E2_MULTA*100,14)									//Valor da Multa / 078-091
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,14)						//Valor de Juros+Encargos / 092-105
	_cRet+=STRZERO((SE2->E2_SALDO+SE2->E2_MULTA+SE2->E2_JUROS+SE2->E2_ACRESC)*100,14)//Valor total arrecadado / 106-119
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 120-127
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 128-135
	_cRet+=space(30)		                              					// Brancos / 136-165
	_cRet+=Substr(SM0->M0_NOMECOM,1,30)										//Nome do Contribuinte / 166-195	
	
	
Elseif SEA->EA_MODELO$"18"//Pagamento de Darf Simples
	_cRet:="03"																//Codigo do tributo / pos. 018-019	
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 039-046
	_cRet+=STRZERO(SE2->E2_ESVRBA*100,9)									//Valor da receita bruta acumulada / 047-055
	_cRet+=STRZERO(SE2->E2_ESPRB,4)											//Percentual da receita Bruta / 056-059
	_cRet+=Space(04)														//Brancos / 060-063	
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor Principal / 064-077
	_cRet+=STRZERO(SE2->E2_MULTA*100,14)									//Valor da Multa / 078-091
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,14)						//Valor de Juros+Encargos / 092-105
	_cRet+=STRZERO(SE2->E2_SALDO+SE2->E2_MULTA+SE2->E2_JUROS+SE2->E2_ACRESC*100,14)	//Valor total arrecadado / 106-119
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 120-127
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 128-135
	_cRet+=space(30)		                              					// Brancos / 136-165
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 166-195	

Elseif SEA->EA_MODELO$"21" // Pagamento de DARJ
	_cRet:="03"																//Codigo do tributo / pos. 018-019	
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=PADL(ALLTRIM(SM0->M0_INSC),8,"0")								//Identifica��o do Contribuinte - IE / 039-046
	_cRet+=STRZERO(SE2->E2_ESNORIG,16)										//Numero do documento de origem / 047-062
	_cRet+=Space(01)														//Branco / 063-063
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento do INSS / 064-077
	_cRet+=STRZERO(SE2->E2_ACRESC*100,14)									//Valor somado ao valor do documento / 078-091
	_cRet+=STRZERO(SE2->E2_JUROS*100,14)									//Valor de Juros+Encargos / 092-105	
	_cRet+=STRZERO(SE2->E2_MULTA*100,14)									//Valor da Multa / 106-119
	_cRet+=STRZERO(SE2->E2_SALDO+SE2->E2_MULTA+SE2->E2_JUROS+SE2->E2_ACRESC*100,14)	//Valor total arrecadado / 120-133
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 134-141
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 142-149
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Competencia / pos. 150-155
	_cRet+=space(10)		                              					// Brancos / 156-165
	_cRet+=Substr(SM0->M0_NOMECOM,1,30)										//Nome do Contribuinte / 166-195	


Elseif SEA->EA_MODELO$"22" //Pagamento de Gare-SP (ICMS/DR/ITCMD)
	_cRet:="04"																//Codigo do tributo / pos. 018-019	
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=PADL(ALLTRIM(SM0->M0_INSC),12,"0")								//Identifica��o do Contribuinte - IE / 039-050
	_cRet+=STRZERO(SE2->E2_ESCDA,13)										//Numero da divida ativa / 051-063
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Competencia / pos. 064-069
	_cRet+=STRZERO(SE2->E2_ESNPN,13)										//Numero da parcela / 070-082
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento / 083-091
	_cRet+=STRZERO((SE2->E2_JUROS+SE2->E2_ACRESC)*100,14)					//Valor de Juros+Encargos / 092-096
	_cRet+=STRZERO(SE2->E2_MULTA*100,14)									//Valor da Multa / 097-110
	_cRet+=STRZERO(SE2->E2_SALDO+SE2->E2_MULTA+SE2->E2_JUROS+SE2->E2_ACRESC*100,14)	//Valor total arrecadado / 111-124
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 139-146
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 147-154
	_cRet+=space(11)		                              					// Brancos / 155-165
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 166-195	
				

Elseif SEA->EA_MODELO$"25" //Pagamentto de IPVA
	_cRet:="07"																//Codigo do tributo / pos. 018-019	
	_cRet+=Space(04)														//Brancos			 / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / pos. 039-042
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodRen),9,"0")									//Codigo do Renavan / 043-051
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 052-053
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=PADL(ALLTRIM(_cCodMun),9,"0")									//Codigo do Municipio / 054-058
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 059-065
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 066-066
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento / 067-080
	_cRet+=STRZERO(SE2->E2_DESCONT*100,14)									//Valor de desconto / 081-094 
	_cRet+=STRZERO((SE2->E2_SALDO-SE2->E2_DESCONT)*100,14)					//Valor de pagamento / 095-108
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 109-116
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 117-124
	_cRet+=space(41)		                              					// Brancos / 125-165
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 166-195	
		
Elseif SEA->EA_MODELO$"27" //Pagamento DPVAT
	_cRet:="08"																//Codigo do tributo / pos. 018-019	
	_cRet+=Space(04)														//Brancos			 / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / pos. 039-042
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodRen),9,"0")									//Codigo do Renavan / 043-051
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 052-053
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=PADL(ALLTRIM(_cCodMun),9,"0")									//Codigo do Municipio / 054-058
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 059-065
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 066-066
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento / 067-080
	_cRet+=STRZERO(SE2->E2_DESCONT*100,14)									//Valor de desconto / 081-094 
	_cRet+=STRZERO((SE2->E2_SALDO-SE2->E2_DESCONT)*100,14)					//Valor de pagamento / 095-108
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 109-116
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 117-124
	_cRet+=space(41)		                              					// Brancos / 125-165
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 166-195	

Elseif SEA->EA_MODELO$"35" // Pagamento de FGTS c/ Codigo de Barras
	_cRet:="11"																//Codigo do tributo / pos. 018-019	
	_cRet+=Substr(SE2->E2_ESCRT  ,1,4)										//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=SUBSTR(SE2->E2_CODBAR,1,48)										//Codigo de Barras / 039-086
	_cRet+=STRZERO(SE2->E2_ESNFGTS,16)										//Ident. do FGTS / 087-102
	_cRet+=STRZERO(SE2->E2_ESLACRE,9)										//Lacre do FGTS / 103-111
	_cRet+=STRZERO(SE2->E2_ESDGLAC,2)										//DG Lacre do FGTS / 112-113
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 114-143
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 144-151
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento / 152-165
	_cRet+=space(30)		                              					// Brancos / 166-195
Endif

Return(_cRet)
                 


#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagacta()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_CTACED,_RETDIG,_DIG1,_DIG2,_DIG3,_DIG4,_NPOSDV")
SetPrvt("_DIG5,_DIG6,_DIG7,_MULT,_RESUL,_RESTO")
SetPrvt("_DIGITO,")

/////  PROGRAMA PARA SEPARAR A C/C DO CODIGO DE BARRA
/////  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (105-119)

_CtaCed := "000000000000000" 
_cBanco := SUBSTR(SE2->E2_CODBAR,1,3)
Do Case
   Case _cBanco == "237"	// BRADESCO

    _CtaCed  :=  STRZERO(VAL(SUBSTR(SE2->E2_CODBAR,37,7)),13,0)
    
    _RETDIG := " "
    _DIG1   := SUBSTR(SE2->E2_CODBAR,37,1)
    _DIG2   := SUBSTR(SE2->E2_CODBAR,38,1)
    _DIG3   := SUBSTR(SE2->E2_CODBAR,39,1)
    _DIG4   := SUBSTR(SE2->E2_CODBAR,40,1)
    _DIG5   := SUBSTR(SE2->E2_CODBAR,41,1)
    _DIG6   := SUBSTR(SE2->E2_CODBAR,42,1)
    _DIG7   := SUBSTR(SE2->E2_CODBAR,43,1)
    
    _MULT   := (VAL(_DIG1)*2) +  (VAL(_DIG2)*7) +  (VAL(_DIG3)*6) +   (VAL(_DIG4)*5) +  (VAL(_DIG5)*4) +  (VAL(_DIG6)*3)  + (VAL(_DIG7)*2)
    _RESUL  := INT(_MULT /11 )
    _RESTO  := INT(_MULT % 11)
    _DIGITO := STRZERO((11 - _RESTO),1,0)

    _RETDIG := IF( _resto == 0,"0",IF(_resto == 1,"P",_DIGITO))

    _CtaCed := _CtaCed + _RETDIG
   
OTHERWISE
    _CtaCed :=  STRZERO(VAL(SUBSTR(SA2->A2_NUMCON,1,13)),13)+STRZERO(VAL(SUBSTR(SA2->A2_DVCTA,1,1)),1)+SPACE(1)
ENDCASE

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_Ctaced)
Return(_CtaCed)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00       


#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagacta1()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_CTACED,_RETDIG,_DIG1,_DIG2,_DIG3,_DIG4,_NPOSDV")
SetPrvt("_DIG5,_DIG6,_DIG7,_MULT,_RESUL,_RESTO")
SetPrvt("_DIGITO,_Mod")

/////  PROGRAMA PARA SEPARAR A C/C DO CODIGO DE BARRA
/////  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (105-119)

_Mod := SUBSTR(SEA->EA_MODELO,1,2)
_CtaCed := "000000000000000" 
_cBanco := SUBSTR(SE2->E2_CODBAR,1,3)
If _Mod == "30" .OR. _Mod == "31"
   // _CtaCed  :=  STRZERO(VAL(SUBSTR(SE2->E2_CODBAR,37,7)),13,0)
  //  _CtaCed  :=  STRZERO(0,13)
  IF cBanco == "237"
   
    _RETDIG := " "
    _DIG1   := SUBSTR(SE2->E2_CODBAR,37,1)
    _DIG2   := SUBSTR(SE2->E2_CODBAR,38,1)
    _DIG3   := SUBSTR(SE2->E2_CODBAR,39,1)
    _DIG4   := SUBSTR(SE2->E2_CODBAR,40,1)
    _DIG5   := SUBSTR(SE2->E2_CODBAR,41,1)
    _DIG6   := SUBSTR(SE2->E2_CODBAR,42,1)
    _DIG7   := SUBSTR(SE2->E2_CODBAR,43,1)
    
    _MULT   := (VAL(_DIG1)*2) +  (VAL(_DIG2)*7) +  (VAL(_DIG3)*6) +   (VAL(_DIG4)*5) +  (VAL(_DIG5)*4) +  (VAL(_DIG6)*3)  + (VAL(_DIG7)*2)
    _RESUL  := INT(_MULT /11 )
    _RESTO  := INT(_MULT % 11)                                                     
    
    _DIGITO := STRZERO((11 - _RESTO),1,0)

    _RETDIG := IF( _resto == 0,"0",IF(_resto == 1,"P",_DIGITO))

    _CtaCed := _CtaCed + _RETDIG                         
    
   // _CtaCed  :=  STRZERO(VAL(SUBSTR(SE2->E2_CODBAR,37,7)),13,0) + _RETDIG
   // _CtaCed  :=  STRZERO(VAL(SUBSTR(SE2->E2_CODBAR,37,7)),13,0) 
    _CtaCed :=  STRZERO(VAL(SUBSTR(SE2->E2_CODBAR,37,7)),13)+ _RETDIG // PADR(ALLTRIM(SA2->A2_DVCTA),2," ")
  ELSE 
   _CtaCed  :=  "0000000000000  "
  
  ENDIF
   
ElseIf (_Mod == "01" .OR. _Mod == "02" .OR. _Mod == "03" .OR. _Mod == "07" .OR. _Mod == "08".OR. _Mod == "05")
    _CtaCed :=  STRZERO(VAL(SUBSTR(SA2->A2_NUMCON,1,13)),13)+PADR(ALLTRIM(SA2->A2_DVCTA),2," ")
Else
    _CtaCed :=  "0000000000000  "
Endif

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_Ctaced)
Return(_CtaCed)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

 
 
   
#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagagen()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_AGENCIA,_RETDIG,_DIG1,_DIG2,_DIG3,_DIG4")
SetPrvt("_MULT,_RESUL,_RESTO,_DIGITO,")

//     PROGRAMA PARA SEPARAR A AGENCIA DO CODIGO DE BARRA
//     CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (99-104)

_Agencia := "000000"
_cBanco := SUBSTR(SE2->E2_CODBAR,1,3)

Do Case 

   Case _cBanco == "237"	// BRADESCO
      _Agencia  :=  "0" + SUBSTR(SE2->E2_CODBAR,20,4)

      _RETDIG := " "
      _DIG1   := SUBSTR(SE2->E2_CODBAR,20,1)
      _DIG2   := SUBSTR(SE2->E2_CODBAR,21,1)
      _DIG3   := SUBSTR(SE2->E2_CODBAR,22,1)
      _DIG4   := SUBSTR(SE2->E2_CODBAR,23,1)

      _MULT   := (VAL(_DIG1)*5) +  (VAL(_DIG2)*4) +  (VAL(_DIG3)*3) +   (VAL(_DIG4)*2)
      _RESUL  := INT(_MULT /11 )
      _RESTO  := INT(_MULT % 11)
      _DIGITO := 11 - _RESTO

      _RETDIG := IF( _RESTO == 0,"0",IF(_RESTO == 1,"0",ALLTRIM(STR(_DIGITO))))

      _Agencia:= _Agencia + _RETDIG

   Otherwise               

      _Agencia :=  STRZERO(VAL(STRTRAN(SA2->A2_AGENCIA,"-","")),6)
	
Endcase
// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __Return(_Agencia)
Return(_Agencia)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

  


#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagagen1()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_AGENCIA,_RETDIG,_DIG1,_DIG2,_DIG3,_DIG4")
SetPrvt("_MULT,_RESUL,_RESTO,_DIGITO,_Mod")

//     PROGRAMA PARA SEPARAR A AGENCIA DO CODIGO DE BARRA
//     CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (99-104)

_Mod := SUBSTR(SEA->EA_MODELO,1,2)
_Agencia := "000000"
_cBanco := SUBSTR(SE2->E2_CODBAR,1,3)

If _Mod == "30" .OR. _Mod == "31"
    
    IF cBanco =="237"               
    
	  _RETDIG := " "
      _DIG1   := SUBSTR(SE2->E2_CODBAR,20,1)
      _DIG2   := SUBSTR(SE2->E2_CODBAR,21,1)
      _DIG3   := SUBSTR(SE2->E2_CODBAR,22,1)
      _DIG4   := SUBSTR(SE2->E2_CODBAR,23,1)

      _MULT   := (VAL(_DIG1)*5) +  (VAL(_DIG2)*4) +  (VAL(_DIG3)*3) +   (VAL(_DIG4)*2)
      _RESUL  := INT(_MULT /11 )
      _RESTO  := INT(_MULT % 11)
      _DIGITO := 11 - _RESTO
      _RETDIG := IF( _RESTO == 0,"0",IF(_RESTO == 1,"0",ALLTRIM(STR(_DIGITO))))
      _Agencia:= _Agencia + _RETDIG
      
       
      _Agencia  :=  "0" + SUBSTR(SE2->E2_CODBAR,20,4) + _RETDIG
    
    ELSE
      
      _Agencia  :=  "00000 "
    
    /*  _RETDIG := " "
      _DIG1   := SUBSTR(SE2->E2_CODBAR,20,1)
      _DIG2   := SUBSTR(SE2->E2_CODBAR,21,1)
      _DIG3   := SUBSTR(SE2->E2_CODBAR,22,1)
      _DIG4   := SUBSTR(SE2->E2_CODBAR,23,1)

      _MULT   := (VAL(_DIG1)*5) +  (VAL(_DIG2)*4) +  (VAL(_DIG3)*3) +   (VAL(_DIG4)*2)
      _RESUL  := INT(_MULT /11 )
      _RESTO  := INT(_MULT % 11)
      _DIGITO := 11 - _RESTO
      _RETDIG := IF( _RESTO == 0,"0",IF(_RESTO == 1,"0",ALLTRIM(STR(_DIGITO))))
      _Agencia:= _Agencia + _RETDIG */
      
    ENDIF
      
ElseIf (_Mod == "01" .OR. _Mod == "02" .OR. _Mod == "03" .OR. _Mod == "07" .OR. _Mod == "08" .OR. _Mod == "05")
      _Agencia := STRZERO(VAL(SUBSTR(SA2->A2_AGENCIA,1,5)),5)+PADR(ALLTRIM(SA2->A2_DVAGE),1," ")
  Else
      _Agencia := "00000 "
Endif
// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __Return(_Agencia)
Return(_Agencia)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00


   

#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagban()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_BANCO,")

//  PROGRAMA PARA SEPARAR O BANCO DO CODIGO DE BARRAS
//  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (96-98)

IF Substr(SE2->E2_CODBAR,1,3)=="   "
   _BANCO := SUBSTR(SA2->A2_BANCO,1,3)
ELSE
   _BANCO := SUBSTR(SE2->E2_CODBAR,1,3)
ENDIF

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_BANCO)

Return(_BANCO)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00
         


#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagcar()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_RETCAR,")

////  PROGRAMA PARA SELECIONAR A CARTEIRA NO CODIGO DE BARRAS QUANDO
////  NAO TIVER TEM QUE SER COLOCADO "00"

IF SUBS(SE2->E2_CODBAR,1,3) != "237"
   _Retcar := "000"
Else
   _Retcar := "0" + SUBS(SE2->E2_CODBAR,24,2)
EndIf

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_Retcar)
Return(_Retcar)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

    
      

#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagdoc()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_Doc,_Mod,")

/////  PROGRAMA GRAVAR AS INFORMACOES COMPLEMENTARES
/////  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (374-413)

_Mod := SUBSTR(SEA->EA_MODELO,1,2)

IF _Mod == "  "
   IF SUBSTR(SE2->E2_CODBAR,1,3) == "237"
      _Mod == "31"
   ELSEIF SUBSTR(SE2->E2_CODBAR,1,3) != "237"
      _Mod == "31" 
   ELSE   
      _Mod == "31" 
   ENDIF
ENDIF

DO CASE
   CASE _Mod == "03" .OR. _Mod == "07" .OR. _Mod == "08"
        _Doc := IIF(SA2->A2_CGC==SM0->M0_CGC,"D","C")+"000000"+"01"+"01"+SPACE(29)
   CASE _Mod == "31"
        _Doc := SUBSTR(SE2->E2_CODBAR,20,25)+SUBSTR(SE2->E2_CODBAR,5,1)+SUBSTR(SE2->E2_CODBAR,4,1)+SPACE(13)
   OTHERWISE
        _Doc := SPACE(40)
ENDCASE

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_DOC)
Return(_Doc)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00
           
     
            
   
#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagmod()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_AMODEL,")

/////  PROGRAMA PARA INDICAR A MODALIDADE DO PAGAMENTO
/////  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (264-265)

_aModel := SUBSTR(SEA->EA_MODELO,1,2)

IF _aModel == "  "
   IF SUBSTR(SE2->E2_CODBAR,1,3) == "237"
      _aModel := "30"
   ELSE
      _aModel := "31"
   ENDIF
ENDIF

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __Return(_aModel)
Return(_aModel)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00




#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagnos()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_RETNOS,")

//// RETORNA O NOSSO NUMERO QUANDO COM VALOR NO E2_CODBAR, E ZEROS QUANDO NAO
//// TEM VALOR POSICAO ( 139 - 150 )

IF SUBS(SE2->E2_CODBAR,01,3) != "237"
    _RETNOS := "000000000000"
Else
    _RETNOS := "0"+SUBS(SE2->E2_CODBAR,26,11)
EndIf

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_RETNOS)
Return(_RETNOS)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00



#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagval()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_VALOR,")

/// VALOR DO DOCUMENTO  DO CODIGO DE BARRA DA POSICAO 06 - 19, NO ARQUIVO E
/// DA POSICAO 191 - 204, QUANDO NAO FOR CODIGO DE BARRA VAI O VALOR DO SE2

_VALOR :=Replicate("0",14)

IF SUBSTR(SE2->E2_CODBAR,1,3) == "   "

    _VALOR   :=  STRZERO((SE2->E2_SALDO*100),14,0)

Else

    _VALOR  :=  "0" + SUBSTR(SE2->E2_CODBAR,6,14)

Endif

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_VALOR)
Return(_VALOR)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00


#INCLUDE "RWMAKE.CH"        



User Function PAGVENC()

SetPrvt("_VENCVAL")
// VERIFICACAO DO VENCIMENTO DO CODIGO DE BARRAS 
_VENCVAL  :=  ""
If !Empty(SE2->E2_CODBAR)
   _VENCVAL := PADL(SUBSTR(SE2->E2_CODBAR,6,14),14,"0")
EndIf
If Empty(_VENCVAL)                                
	If !EMPTY(SE2->E2_ACRESC) .OR. !EMPTY(SE2->E2_DECRESC) .OR. !EMPTY(SE2->E2_JUROS) .OR. !EMPTY(SE2->E2_MULTA)
  	 	_VENCVAL := STRZERO(SE2->(E2_SALDO + E2_ACRESC - E2_DECRESC + E2_JUROS + E2_MULTA)*100,14)        
 	ElseIf EMPTY(SE2->E2_ACRESC) .OR. EMPTY(SE2->E2_DECRESC) .OR. EMPTY(SE2->E2_JUROS) .OR. EMPTY(SE2->E2_MULTA)
  		_VENCVAL  :=  "00000000000000"
    EndIf
EndIf

Return(_VENCVAL)




   
#include "rwmake.ch" 

User Function RECOLH()        

Local cEnder := ''

If(Alltrim(SE2->E2_ESCRT)$'2631/2658')

cEnder := PADL(SUBSTR(ALLTRIM(SM0->M0_NOMECOM),1,40),40)
 
Else 
 
cEnder := SPACE(40)
       
EndIf
  
Return 

              
       

#Include "Rwmake.ch"

//Pega CEP do cadastro de clientes ref. posi��o 327 a 334


User Function RETCep()

Local cRet:=""

	If Empty(SA1->A1_CEPC)
		cRet := SUBSTR(SA1->A1_CEP,1,5)+SUBSTR(SA1->A1_CEP,6,3)
	Else
		cRet := SUBSTR(SA1->A1_CEPC,1,5)+SUBSTR(SA1->A1_CEPC,6,3)
	Endif

Return (cRet)        

    


#INCLUDE "PROTDEF.CH"
#INCLUDE "RWMAKE.CH" 

/*���������������������������������������������������������������������������
���Programa  � SANTTRIB    												   ��
�������������������������������������������������������������������������Ĵ��
�������������������������������������������������������������������������Ĵ��
��� Uso      � CNAB SISPAG - Banco do SANTANDER (Pagamento de Tributos)   ���
��			 � Gps (Modalidade 17)										   ��
��			 � Darf Normal (Modalidade 16)								   ��
��			 � Darf Simples (Modalidade 18)								   ��
��			 � Gare SP (ICMS/DR/ITCMD) (Modalidade 22)					   ��
��			 � Ipva (Modalidade 25)										   ��
��			 � Dpvat (Modalidade 26)									   ��
��			 � Licenciamento (Modalidade 27)							   ��
��			 � Fgts (Modalidade 35)										   ��
��������������������������������������������������������������������������ٱ�
��� Cliente                              				   ��
���������������������������������������������������������������������������*/

User Function SANTTRIB()                      

Local _cRet		:=""
Local _cCodUF	:=""
Local _cCodMun	:=""
Local _cCodPla	:=""

If SEA->EA_MODELO$"17" //Pagamento GPS
	_cRet:=PadL(Substr(SE2->E2_ESCRT,1,4),6,"0")									//Codigo do pagamento / pos. 111-116	
	_cRet+="02"																//Tipo de Inscr. Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+="17"																//Cod. pgto do Contribuinte / pos. 133-134
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Competencia / pos. 135-140
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor de pagamento do INSS / 141-155
	_cRet+=STRZERO(SE2->E2_ACRESC*100,15)									//Valor somado ao valor do documento / 156-170
	_cRet+=REPL("0",15)														//Atualiza��o monetaria /	171-185
	_cRet+=Space(45)														//Uso da empresa / 186-230
ElseIf SEA->EA_MODELO$"16" //Pagamento de Darf Normal
	_cRet:=PadL(Substr(SE2->E2_ESCRT,1,4),6,"0")									//Codigo do pagamento / pos. 111-116		
	_cRet+="02"																//Tipo de Inscr. Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+="16"																//Cod. pgto do Contribuinte / pos. 133-134	
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 135-142
	_cRet+=STRZERO(SE2->E2_ESNREF,17)										//Numero de referencia / 143-159
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor Principal / 160-174
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 175-189
	_cRet+=STRTRAN(STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15,2),".","")	//Valor de Juros+Encargos / 190-204
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 205-212
	_cRet+=space(18)		                              					// Brancos / 213-230
ElseIf SEA->EA_MODELO$"18"//Pagamento de Darf Simples
	_cRet:="006106"															//Codigo do pagamento / pos. 111-116		
	_cRet+="02"																//Tipo de Inscr. Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+="18"																//Cod. pgto do Contribuinte / pos. 133-134	
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 135-142
	_cRet+=STRZERO(SE2->E2_ESVRBA*100,15)									//Valor da receita bruta acumulada / 143-157
	_cRet+=STRZERO(SE2->E2_ESPRB,7)									  		//Percentual da receita Bruta / 158-164
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor Principal / 165-179
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 180-194
	_cRet+=STRTRAN(STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15,2),".","")	//Valor de Juros+Encargos / 195-209
	_cRet+=space(21)		                              					// Brancos / 210-230
ElseIf SEA->EA_MODELO$"22" .Or. SEA->EA_MODELO$"23" .Or. SEA->EA_MODELO$"24"//Pagamento de Gare-SP (ICMS (22)/DR (23) /ITCMD(24))
	_cRet:=PadL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo do pagamento / pos. 111-116		
	_cRet+="02"																//Tipo de Inscr. Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+="22"																//Cod. pgto do Contribuinte / pos. 133-134	
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 135-142
	_cRet+=PADL(ALLTRIM(SM0->M0_INSC),12,"0")								//Identifica��o do Contribuinte - IE / 143-154	
	_cRet+=STRZERO(0,13)													//Numero da divida ativa / 155-167	
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Competencia / pos. 168-173
	_cRet+=STRZERO(0,7)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Numero da parcela / 174-186
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor de pagamento / 187-201
	_cRet+=STRZERO((SE2->E2_JUROS+SE2->E2_ACRESC)*100,14)					//Valor de Juros+Encargos / 202-215
	_cRet+=STRZERO(SE2->E2_MULTA*100,14)									//Valor da Multa / 216-229
	_cRet+=space(1)			                              					// Brancos / 230-230
ElseIf SEA->EA_MODELO$"25" //Pagamentto de IPVA
	_cRet:=Padl(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=STRZERO(val(SE2->E2_ESTIC),02)									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Space(68)														//Exclusivo Febraban / 163-230		
ElseIf SEA->EA_MODELO$"27" //Pagamento DPVAT
	_cRet:=Padl(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=STRZERO(val(SE2->E2_ESTIC),02)									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Space(68)														//Exclusivo Febraban / 163-230		
ElseIf SEA->EA_MODELO$"26" // Pagamento de Licenciamento
	_cRet:=Padl(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=STRZERO(val(SE2->E2_ESTIC),02)									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVEI,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Alltrim(SE2->E2_ESCRVL)											//Op��o de Retirada do CRVL / 163-163		
	_cRet+=Space(67)														//Exclusivo Febraban / 164-230
ElseIf SEA->EA_MODELO$"35" // Pagamento de FGTS c/ Codigo de Barras
	_cRet:="11"																//Codigo do tributo / pos. 018-019	
	_cRet+=PadL(Substr(SE2->E2_ESCRT,1,6),6,"0")									//Codigo do pagamento / pos. 020-023		
	_cRet+="2"																//Tipo de Inscr. Contribuinte / pos. 024-024
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 025-038
	_cRet+=SUBSTR(SE2->E2_CODBAR,1,48)										//Codigo de Barras / 039-086
	_cRet+=STRZERO(SE2->E2_ESNFGTS,16)										//Ident. do FGTS / 087-102
	_cRet+=STRZERO(SE2->E2_ESLACRE,9)										//Lacre do FGTS / 103-111
	_cRet+=STRZERO(SE2->E2_ESDGLAC,2)										//DG Lacre do FGTS / 112-113
	_cRet+=Substr(sm0->m0_nomecom,1,30)										//Nome do Contribuinte / 114-143
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de pagamento  / 144-151
	_cRet+=STRZERO(SE2->E2_SALDO*100,14)									//Valor de pagamento / 152-165
	_cRet+=space(30)		                              					// Brancos / 166-195
Endif
Return(_cRet)

                   


#include "rwmake.ch"


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �SISPAG    												   ��
�������������������������������������������������������������������������͹��
���Desc.     �Obtem as informacoes necessarias a montagem do arquivo SisPag                                                          ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico 		                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function SisPag(cOpcao)

Local cReturn   := ""
Local nReturn   := 0
Local cAgencia  := " "
Local cNumCC    :=" " 
Local cDVAgencia:= " "
Local cDVNumCC  := " "


If cOpcao == "1"    // obter numero de conta e agencia

    cAgencia :=  Alltrim(SA2->A2_AGENCIA)

   If AT("-",cAgencia) > 0
      cAgencia := Substr(cAgencia,1,AT("-",cAgencia)-1)
   Endif

   cAgencia := STRTRAN(cAgencia,".","")

// Obtem o digito da agencia

    cDVAgencia :=  Alltrim(SA2->A2_AGENCIA)
   If AT("-",cDVAgencia) > 0
     cDVAgencia := Substr(cDVAgencia,AT("-",cDVAgencia)+1,1)
   Else
     cDVAgencia := Space(1)
   Endif

// Obtem o numero da conta corrente

   cNumCC :=  Alltrim(SA2->A2_NUMCON)
        
   If AT("-",cNumCC) > 0
     cNumCC := Substr(cNumCC,1,AT("-",cNumCC)-1)
   Endif


// obtem o digito da conta corrente
     
   cDVNumCC :=  Alltrim(SA2->A2_NUMCON)
   If AT("-",cDVNumCC) > 0
     cDVNumCC := Substr(cDVNumCC,AT("-",cDVNumCC)+1,2)
   Else
     cDVNumCC := Space(1)
   Endif
	
		
	
	If SA2->A2_BANCO == "341"  // se for o pr�prio Ita� - credito em C/C
		
		nReturn:= "0"+cAgencia+space(1)+Replicate("0",7)+cNumCC+space(1)+cDVNumCC
		
	Else  // para os outros bancos - DOC
		
		If Len(Alltrim(cDvNumCC)) > 1
			nReturn:= StrZero(Val(cAgencia),5)+space(1)+StrZero(val(cNumCC),12)+cDvNumCC
		Else
			nReturn:= StrZero(Val(cAgencia),5)+space(1)+StrZero(val(cNumCC),12)+space(1)+cDvNumCC
		EndIf
		
	EndIf
	
ElseIf cOpcao == "2"  // valor a pagar
	
	_nVlTit:= SE2->E2_SALDO+SE2->E2_ACRESC-SE2->E2_DECRESC
	
	nReturn := Strzero((_nVlTit * 100),15)
	
ElseIf  cOpcao == "3"  // Verifica o DV Geral
	
	If Len(Alltrim(SE2->E2_CODBAR)) > 44
		nReturn := Substr(SE2->E2_CODBAR,33,1)
	Else
		nReturn:= Substr(SE2->E2_CODBAR,5,1)
	EndIf
	
ElseIf  cOpcao == "4"       // FATOR DE VENCIMENTO
	
	If Len(Alltrim(SE2->E2_CODBAR)) > 44
		nReturn := Substr(SE2->E2_CODBAR,34,4)
	Else
		nReturn:= Substr(SE2->E2_CODBAR,6,4)
	EndIf
	
ElseIf  cOpcao == "5"       // Valor constante do codigo de barras
	
  If Len(Alltrim(SE2->E2_CODBAR)) > 44
		nValor := Substr(SE2->E2_CODBAR,38,10)
	Else
		nValor := Substr(SE2->E2_CODBAR,10,10)
  EndIf
		
	nReturn := Strzero(Val(nValor),10)
	
ElseIf  cOpcao == "6"       // Campo Livre
	
	If Len(Alltrim(SE2->E2_CODBAR)) > 44
		nReturn := Substr(SE2->E2_CODBAR,5,5)+Substr(SE2->E2_CODBAR,11,10)+;
		Substr(SE2->E2_CODBAR,22,10)
	Else
		nReturn := Substr(SE2->E2_CODBAR,20,25)
	EndIf
	
ElseIf cOpcao == "7"  // pagamento de tributos

cTributo:= SubsTr(SEA->EA_MODELO,1,2)
cCodRec:= SE2->E2_CODREC
cTpInscr := "2"  // cnpj
cCNPJ:= StrZero(Val(SM0->M0_CGC),14)
dPeriodo:= GravaData(SE2->E2_EMISSAO,.F.,5)
cReferen:= SE2->(E2_PREFIXO+E2_NUM+E2_PARCELA)
nValor:= StrZero((SE2->E2_SALDO*100),14)
nValorMult:=  "00000000000000"
nValorJuros:= "00000000000000"
dVencto:= GravaData(SE2->E2_VENCTO,.F.,5)
dDtPagto:= GravaData(SE2->E2_VENCREA,.F.,5)
cBrancos:= space(30)
cContrib:= Substr(SM0->M0_NOMECOM,1,30)


nReturn:= cTributo + cCodRec +  cTpInscr + (cCNPJ) + dPeriodo + Padr(cReferen,17)+(nValor)+(nValorMult)+(nValorJuros)+;
                  (nValor) + dVencto + dDtPagto+space(60)


ElseIf  cOpcao == "8"       // Valor nominal
	
  If Len(Alltrim(SE2->E2_CODBAR)) > 44
		nValor := Substr(SE2->E2_CODBAR,38,10)
	Else
		nValor := Substr(SE2->E2_CODBAR,10,10)
  EndIf
  
	If Val(nValor) > 0	
	   nReturn := Strzero(Val(nValor*100),15)
	Else 
		nReturn:= Strzero(SE2->E2_SALDO*100,15)   
    EndIf
    	 
EndIf

Return(nReturn)     



             
#include "protdef.ch"
#include "rwmake.ch" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TECH08      												   ��
�������������������������������������������������������������������������Ĵ��
�������������������������������������������������������������������������Ĵ��
��� Uso      � CNAB SISPAG - Banco do Brasil                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function TECH08()                      

Local _cRet		:=""
Local _cCodUF	:=""
Local _cCodMun	:=""
Local _cCodPla	:=""


If SEA->EA_MODELO$"17" //Pagamento GPS
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)         								//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=SUBSTR(SE2->E2_XXPER,1,6)										//Competencia / 135-140
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor de pagamento do INSS / 141-155
	_cRet+=STRZERO(SE2->E2_ACRESC*100,15)									//Valor somado ao valor do documento / 156-170
	_cRet+=REPL("0",15)														//Atualiza��o monetaria / 171-185
	_cRet+=Space(45)														//Exclusivo Febraban / 186-230

Elseif SEA->EA_MODELO$"16" //Pagamento de Darf Normal
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)      									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=SUBSTR(SE2->E2_XXPER,1,6)										//135-142
	_cRet+=STRZERO(SE2->E2_ESNREF,17)										//Numero de referencia / 143-159
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor Principal / 160-174
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 175-189
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15)						//Valor de Juros+Encargos / 190-204
	_cRet+=StrZero(Day(SE2->E2_VENCREA),2)+STRZERO(MONTH(SE2->E2_VENCREA),2)+STR(YEAR(SE2->E2_VENCREA),4)//Data de Vencimento / 205-218
	_cRet+=Space(18)														//Exclusivo Febraban / 213-230
	
Elseif SEA->EA_MODELO$"18"//Pagamento de Darf Simples
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")  					   		//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)    									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 135-142
	_cRet+=STRZERO(SE2->E2_ESVRBA*100,15)									//Valor da receita bruta acumulada / 143-157
	_cRet+=STRZERO(SE2->E2_ESPRB,7)											//Percentual da receita Bruta / 158-164
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor principal / 165-179
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 180-194
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15)						//Valor de Juros+Encargos / 195-209
	_cRet+=Space(21)														//Exclusivo Febraban / 210-230

Elseif SEA->EA_MODELO$"22,23,24" //Pagamento de Gare-SP (ICMS/DR/ITCMD)
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)										//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 135-142
	_cRet+=SUBS(SM0->M0_INSC,1,12)            								//Numero Inscri��o Estadual  / 143-154
	_cRet+=PADL(ALLTRIM(SE2->E2_ESCDA),13,"0")								//Codigo da divida Ativa / 155-167
	_cRet+=STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4)	//Periodo de Referencia / 168-173
	_cRet+=PADL(ALLTRIM(SE2->E2_ESNPN),13,"0")								//Numero Parcela-Notifica��o / 174-186
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor da receita / 187-201
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15)						//Valor de Juros+Encargos / 202-215
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 216-229
	_cRet+=Space(01)														//Exclusivo Febraban / 230-230	
				
Elseif SEA->EA_MODELO$"25" //Pagamentto de IPVA
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)    									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Space(68)														//Exclusivo Febraban / 163-230		
	
	

Elseif SEA->EA_MODELO$"27" //Pagamento DPVAT
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)										//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Space(68)														//Exclusivo Febraban / 163-230		

Elseif SEA->EA_MODELO$"26" // Pagamento de Licenciamento
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)										//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=Alltrim(SEA->EA_MODELO)											//Codigo de identifica��o do contribuinte - Modelo de pagamento / 133-134
	_cRet+=STR(YEAR(SE2->E2_EMISSAO),4)										//Competencia / 135-138
	_cCodRen:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_RENAVA")
	_cRet+=PADL(ALLTRIM(_cCodVei),9,"0")									//Codigo do Renavan / 139-147
	_cCodUF:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_ESTPLA")	
	_cRet+=_cCodUF															//UF do estado do veiculo / 148-149
	_cCodMun:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVE,"DA3_CODMUN")	
	_cRet+=_cCodMun															//Codigo do Municipio / 150-154
	_cCodPla:=Posicione("DA3",1,xFilial("DA3")+SE2->E2_ESCODVEI,"DA3_PLACA")	
	_cRet+=_cCodPla                                                         //Placa do Veiculo / 155-161
	_cRet+=Alltrim(SE2->E2_ESOPIP)											//Codigo da cond. de pgto / 162-162
	_cRet+=Alltrim(SE2->E2_ESCRVL)											//Op��o de Retirada do CRVL / 163-163		
	_cRet+=Space(67)														//Exclusivo Febraban / 164-230		
	
Elseif SEA->EA_MODELO$"21" // Pagamento de DARJ
	_cRet:=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 111-116
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)   									//Tipo de Identifica��o do Contribuinte / pos. 117-118
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 119-132
	_cRet+=SUBS(SM0->M0_INSC,1,8)            								//Numero Inscri��o Estadual  / 133-140
	_cRet+=STRZERO(SE2->E2_NUM,16)											//Numero do documento de origem / 141-156
	_cRet+=STRZERO(SE2->E2_SALDO*100,15)									//Valor da receita / 157-171
	_cRet+=REPL("0",15)														//Atualiza��o monetaria / 172-186	
	_cRet+=STRZERO(SE2->E2_JUROS+SE2->E2_ACRESC*100,15)						//Valor de Juros+Encargos / 187-201
	_cRet+=STRZERO(SE2->E2_MULTA*100,15)									//Valor da Multa / 202-216
	_cRet+=StrZero(Day(SE2->E2_EMISSAO),2)+STRZERO(MONTH(SE2->E2_EMISSAO),2)+STR(YEAR(SE2->E2_EMISSAO),4) //Competencia / 217-224
	_cRet+=Space(06)														//Exclusivo Febraban / 225-230
    

Elseif SEA->EA_MODELO$"35" // Pagamento de FGTS c/ Codigo de Barras
	_cRet:="01"																//Identificador do Tributo / pos. 177-178
	_cRet+=PADL(Substr(SE2->E2_ESCRT,1,4),6,"0")							//Codigo da Receita do Tributo / pos. 179-184
	_cRet+=SUBSTR(SE2->E2_ESTPID,1,2)   									//Tipo de Identifica��o do Contribuinte / pos. 185-186
	_cRet+=PADL(ALLTRIM(SM0->M0_CGC),14,"0")								//Identifica��o do Contribuinte - CNPJ/CGC/CPF / 187-200
	_cRet+=STRZERO(SE2->E2_ESNFGTS,16)										//Campo Identificador do FGTS / 201-216
	_cRet+=STRZERO(SE2->E2_ESLACR,9)										//Codigo do Lacre / 217-225
	_cRet+=STRZERO(SE2->E2_ESDGLAC,2)					   					//Digito do codigo do Lacre / 226-227
	_cRet+=Space(01)

											//Exclusivo Febraban / 228-228


Endif                                                                                           

Return(_cRet)



                     

#include "protdef.ch"
#include "rwmake.ch" 

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � TECH09     												   ��
�������������������������������������������������������������������������Ĵ��
�������������������������������������������������������������������������Ĵ��
��� Uso      � CNAB SISPAG - Banco do Brasil                              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function TECH09()                      

Local _cRet:=""

//Programa para identificar qual tipo de DOC est� sendo processado

/*
'02' = Pagamento de Aluguel/Condom�nio
'10' = Transfer�ncia Internacional em Real
*/

If SEA->EA_MODELO=="03"
	If SEA->EA_TIPOPAG=="20"
		_cRet:="07"
	Elseif SEA->EA_TIPOPAG=="30"
		_cRet:="06"
	Elseif SEA->EA_TIPOPAG=="32"
		_cRet:="18"
	Elseif SEA->EA_TIPOPAG=="34"
		_cRet:="19"
	Elseif SEA->EA_TIPOPAG=="33"
		_cRet:="16"
	Elseif SEA->EA_TIPOPAG=="98"
		_cRet:="13"
	Elseif SEA->EA_TIPOPAG=="10"
		_cRet:="04"
	Elseif SEA->EA_TIPOPAG=="22"
		_cRet:="09"
	Elseif SEA->EA_TIPOPAG=="03"
		_cRet:="03"
	Elseif SEA->EA_TIPOPAG=="05"
		_cRet:="05"		
	Elseif SEA->EA_TIPOPAG=="40"
		_cRet:="08"		
	Elseif SEA->EA_TIPOPAG=="80"
		_cRet:="17"		

	Endif		
ElseIf SEA->EA_MODELO=="01"	
	_cRet:="01"                 
ElseIf SEA->EA_MODELO=="05"	
	_cRet:="11"
ElseIf SEA->EA_MODELO$"71/72"	
	_cRet:="12"
Else
	_cRet:=""
Endif

Return(_cRet)



                       

	#include "rwmake.ch"  

/*���������������������������������������������������������������������������
���Programa  �TOTENT   													   ��
�������������������������������������������������������������������������͹��
���Desc.     � Fonte criado para tratar as Posi��es do Cnab 038 a 051.    ���
���          � Valores Outras Entidades Guia da GPS - INSS  (SISPAG)      ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
�������������������������������������������������������������������������ͼ��
���������������������������������������������������������������������������*/

User Function Totent() 

Local _area   := GetArea()
Local _Ret := 0 
_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")
_Ret := StrZero(_Acres * 100,14) 
RestArea(_area)

Return _Ret

                      


#include "rwmake.ch"  

User Function Totpag() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Juros:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_JUROS")
_Multa:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_MULTA")
//_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")

_DESC := VAL(STR(_Desc*100))
_Soma1:= (((_Juros+_Multa)*100) - _Desc)
_SOMA1:= STR(_SOMA1)
_SOMA1:= STRZERO(VAL(_SOMA1),14)
      
RestArea(_area)

Return(_Soma1) 

                        


#include "rwmake.ch"  

User Function Totpag1() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Juros:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_JUROS")
_Multa:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_MULTA")
//_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")

_DESC := VAL(STR(_Desc*100))
_Soma1:= (((_Soma+(_Juros+_Multa)*100) - _Desc))  
//_Soma1:= ((_Soma+(_Juros+_Multa)*100)-(_Acres+_Desc))
_SOMA1:= STR(_SOMA1)
_SOMA1:= STRZERO(VAL(_SOMA1),18)
      
RestArea(_area)

Return(_Soma1) 
                


#include "rwmake.ch"  

User Function Totpag2() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Juros:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_JUROS")
_Multa:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_MULTA")
//_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")

_DESC := VAL(STR(_Desc*100))
_Soma1:= (((_Soma+(_Juros+_Multa)*100) - _Desc))  
//_Soma1:= ((_Soma+(_Juros+_Multa)*100)-(_Acres+_Desc))
_SOMA1:= STR(_SOMA1)
_SOMA1:= STRZERO(VAL(_SOMA1),14)
      
RestArea(_area)

Return(_Soma1) 

                         


#include "rwmake.ch"  

User Function Totpag3() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Juros:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_JUROS")
_Multa:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_MULTA")
_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")

_DESC := VAL(STR(_Desc*100))
_Soma1:= ((_Soma-(_Acres)*100))
_SOMA1:= STR(_SOMA1)
_SOMA1:= STRZERO(VAL(_SOMA1),14)
      
RestArea(_area)

Return(_Soma1)


         

#include "rwmake.ch"  

User Function Totpag4() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Juros:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_JUROS")
_Multa:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_MULTA")
//_Acres:= SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_ACRESC")

_DESC := VAL(STR(_Desc*100))
_Soma1:= (((_Soma+(_Juros+_Multa)*100) - _Desc))  
//_Soma1:= ((_Soma+(_Juros+_Multa)*100)-(_Acres+_Desc))
_SOMA1:= STR(_SOMA1)
_SOMA1:= STRZERO(VAL(_SOMA1),18)
      
RestArea(_area)

Return(_Soma1) 


             

#INCLUDE "PROTHEUS.CH"


/*���������������������������������������������������������������������������
���Programa  �TRLARQ     												  ���
�������������������������������������������������������������������������͹��
���Desc.     �PROGRAMA PARA TRATAMENTO DO TRAILLER DO ARQUIVO DE CNAB DE  ���
���          �TODOS OS SEGMENTOS REFERENTE A QUANTIDADE DE REGISTROS.     ���
�������������������������������������������������������������������������͹��
���Uso       �                                                            ���
���������������������������������������������������������������������������*/

User Function TRLARQ()

cRet := ""
//TRATAMENTO PARA O TRAILLER DO ARQUIVO DOS SEGMENTOS J / N / O / W /D
If SEA->EA_MODELO$"11/13/30/31/16/17/18/19/20/21/22/23/24/25/26/27/33/35" .AND. SEA->EA_PORTADO $ "001/341/237/033/041/104"
	cRet := STRZERO(M->NSEQ+4,6)
	//ElseIf SEA->EA_MODELO$"11/13/30/31/16/17/18/19/20/21/22/23/24/25/26/27/33/35".AND. SEA->EA_PORTADO == "041" //TRATAMENTO PARA O TRAILLER DO ARQUIVO DOS SEGMENTOS J / N / O / W / D
	//	cRet := STRZERO(M->NSEQ+3,6)
ElseIf SEA->EA_MODELO$"01/02/03/04/05/10/41/43/50" .AND. SEA->EA_PORTADO == "033"	//TRATAMENTO PARA O TRAILLER DO ARQUIVO DOS SEGMENTOS A / B
	cRet := STRZERO(M->NSEQ+4,6)
ElseIf SEA->EA_MODELO$"01/02/03/04/05/10/41/43/50" .AND. SEA->EA_PORTADO $ "001/341/237/104"	//TRATAMENTO PARA O TRAILLER DO ARQUIVO DOS SEGMENTOS A / B
	cRet := STRZERO((NSEQ+1)*2,6)
ElseIf SEA->EA_MODELO$"01/02/03/04/05/10/41/43/50" .AND. SEA->EA_PORTADO == "041"	//TRATAMENTO PARA O TRAILLER DO ARQUIVO DOS SEGMENTOS A / B
	cRet := STRZERO(NSEQ+4,6)
Else
	cRet := STRZERO(0,6)
EndIf

Return (cRet)

      

#include "rwmake.ch"  

User Function ValTot() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_DESC := VAL(STR(_Desc*100))
_Soma1 := ((_Soma - _Desc))
_SOMA1 := STR(_SOMA1)
_SOMA1 := STRZERO(VAL(_SOMA1),17)
      
RestArea(_area)

Return(_Soma1)                  
 



#include "rwmake.ch"  


User Function ValTot1() 

Local _area   := GetArea()
Local _Soma   := SOMAVALOR()
local _Soma1  := 0
Local _Desc   := 0

//_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")
_Desc := SOMAR("SE2","E2_NUMBOR=SEA->EA_NUMBOR ","E2_DECRESC")

//_DESC := VAL(STR(_Desc*100))
_DESC := VAL(STR(_Desc*100))

//_Soma1 := ((_Soma - _Desc))
_Soma1 := ((_Soma))

//_SOMA1 := STR(_SOMA1)
_SOMA1 := STR(_SOMA1)

//_SOMA1 := STRZERO(VAL(_SOMA1),17)
_SOMA1 := STRZERO(VAL(_SOMA1),17)
      
RestArea(_area)

Return(_Soma1) 
      
      
      
         
#INCLUDE "PROTHEUS.CH"


User Function MORA()

Local _nMora 	:= 0
Local _cBanco	:= SEE->EE_CODIGO //"341"
Local _cAgencia	:= SEE->EE_AGENCIA //"4807 "
Local _cConta	:= SEE->EE_CONTA //"092293    "	

DbSelectArea("SEE")       
Dbsetorder(1)
Dbseek(xfilial("SEE")+_cBanco+_cAgencia+_cConta+"R")                     

If _cBanco=="341"
	_nMora := Padl( Alltrim(StrTran(Transform(((SE1->E1_VALOR*SEE->EE_JUROS)/100)/30,"@E 99,999,999.99"),",","")), 13, "0" )
Elseif _cBanco=="001" .OR. _cBanco=="399"
	_nMora := Padl( Alltrim(StrTran(Transform(((SEE->EE_JUROS)/100)/30,"@E 99,999,999.9999"),",","")), 15, "0" )
Else                                                                                                                        
	_nMora := Padl( Alltrim(StrTran(Transform(((SE1->E1_VALOR*SEE->EE_JUROS)/100)/30,"@E 99,999,999.99"),",","")), 13, "0" )
Endif
Return _nMora




#INCLUDE "PROTHEUS.CH"


User Function MULTAK()

Local _nMulta 	:= 0
Local _cBanco	:= SEE->EE_CODIGO //
Local _cAgencia	:= SEE->EE_AGENCIA //
Local _cConta	:= SEE->EE_CONTA //

DbSelectArea("SEE")       
Dbsetorder(1)
Dbseek(xfilial("SEE")+_cBanco+_cAgencia+_cConta+"R")                     

If _cBanco=="237"
	_nMulta := Padl( Alltrim(StrTran(Transform((SEE->EE_PERCMUL),"@E 99.99"),",","")), 4, "0" )
Else                                                                                                                        
	_nMulta := Padl( Alltrim(StrTran(Transform(((SE1->E1_VALOR*SEE->EE_MULTA)/100),"@E 99,999,999.99"),",","")), 13, "0" )
Endif
Return _nMulta
                 
                     
 
#include "rwmake.ch"        


User Function PAGACRE()

//SetPrvt("_VENC")

Local _ACRE  :=  ""


IF Empty(_ACRE)                                
	
	If  !EMPTY(SE2->E2_ACRESC) .AND. SE2->E2_AJUSTE == "DB"
  
  	 	_ACRE := STRZERO(SE2->E2_ACRESC*100,15)                              
  	
  	ElseIf  !EMPTY(SE2->E2_DECRESC) .AND. SE2->E2_AJUSTE == "AI"
  	
  		_ACRE  :=  STRZERO(0,15)
    
   
 	ElseIf  EMPTY(SE2->E2_ACRESC)
  	
  		_ACRE :=  STRZERO(0,15)
    
   	Endif

ENDIF

Return(_ACRE)  



#include "rwmake.ch"        


User Function PAGDEC()

//SetPrvt("_VENC")

Local _DEC  :=  ""


IF Empty(_DEC)                                
	
	If  !EMPTY(SE2->E2_DECRESC) .AND. SE2->E2_AJUSTE == "DB"
  
  	 	_DEC := STRZERO(SE2->E2_DECRESC*100,15)                             
  	
  	ElseIf  !EMPTY(SE2->E2_DECRESC) .AND. SE2->E2_AJUSTE == "AI"
  	
  		_DEC  :=  STRZERO(0,15)
    
   
 	ElseIf  EMPTY(SE2->E2_DECRESC)
  	
  		_DEC  :=  STRZERO(0,15)
    
   	Endif

ENDIF

Return(_DEC)
                  
                   

#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function Pagdoc1()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_Doc,_Mod,")

/////  PROGRAMA GRAVAR AS INFORMACOES COMPLEMENTARES
/////  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (374-413)

_Mod := SUBSTR(SEA->EA_MODELO,1,2)

IF _Mod == " " 
   IF SUBSTR(SE2->E2_CODBAR,1,3) == "237"
//      _Mod == "30" //segundo suporte bradesco a regra de pagamento da modalidade 30 agora � especifico para DDA
//   ELSE
      _Mod == "31"
   ENDIF
ENDIF

DO CASE
   CASE _Mod == "03" .OR. _Mod == "07" .OR. _Mod == "08"
        _Doc := IIF(SA2->A2_CGC==SM0->M0_CGC,"D","C")+"000000"+"01"+"01"+SPACE(29)
   CASE _Mod == "31" .or. _Mod == "30"
   		If SUBSTR(SE2->E2_CODBAR,1,3) == "237"
   			_Mod := "31"
	       // _Doc := SPACE(40)   		
	    //Else
	        _Doc := SUBSTR(SE2->E2_CODBAR,20,25)+SUBSTR(SE2->E2_CODBAR,5,1)+SUBSTR(SE2->E2_CODBAR,4,1)+SPACE(13)
	    Else
	        _Doc := SUBSTR(SE2->E2_CODBAR,20,25)+SUBSTR(SE2->E2_CODBAR,5,1)+SUBSTR(SE2->E2_CODBAR,4,1)+SPACE(13)
	    Endif
	    
   OTHERWISE
        _Doc := SPACE(40)
ENDCASE

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_DOC)
Return(_Doc)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00
                                                                                        
                
                

#include "rwmake.ch"        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

User Function PagNOTA()        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00

//���������������������������������������������������������������������Ŀ
//� Declaracao de variaveis utilizadas no programa atraves da funcao    �
//� SetPrvt, que criara somente as variaveis definidas pelo usuario,    �
//� identificando as variaveis publicas do sistema utilizadas no codigo �
//� Incluido pelo assistente de conversao do AP5 IDE                    �
//�����������������������������������������������������������������������

SetPrvt("_Numero , _Parcela , _252 , _251")                

_Numero  := SUBSTR(SE2->E2_NUM,1,9)
_Parcela := SUBSTR(SE2->E2_PARCELA,1,1)


//  PROGRAMA PARA JUNTAR O NUMERO DA NOTA + PARFCELA SEM ESPA�O E COMSTRZERO
//  CNAB BRADESCO A PAGAR (PAGFOR) - POSICOES (252-261)
       
_251  := ALLTRIM(_Numero) + ALLTRIM(_Parcela)
_252  := STRZERO(VAL(_251),10)
 

// Substituido pelo assistente de conversao do AP5 IDE em 26/09/00 ==> __return(_BANCO)

Return(_252)        // incluido pelo assistente de conversao do AP5 IDE em 26/09/00
                                                                                          
                

#include "rwmake.ch"        


User Function PAGVENC1()

SetPrvt("_VENCVAL")

// VERIFICACAO DO VENCIMENTO DO CODIGO DE BARRAS 

_VENCVAL  :=  ""

IF !Empty(SE2->E2_CODBAR)
   
   _VENCVAL := PADL(SUBSTR(SE2->E2_CODBAR,6,14),14,"0")

EndIf

IF Empty(_VENCVAL)                                
	If !EMPTY(SE2->E2_ACRESC) .OR. !EMPTY(SE2->E2_DECRESC)
  
  	 	_VENCVAL := STRZERO(SE2->(E2_SALDO)*100,14)        
  
 	ElseIf EMPTY(SE2->E2_ACRESC) .OR. EMPTY(SE2->E2_DECRESC)
  	
  		_VENCVAL  :=  "00000000000000"
    
    Endif

ENDIF

Return(_VENCVAL)

     
 
 
#include "rwmake.ch"        


User Function PAGVENC2()

SetPrvt("_VENC")

_VENC  :=  ""

IF Empty(_VENC)                                
	If  !EMPTY(SE2->E2_DECRESC)
  
  	 	_VENC := DTOS(SE2->E2_VENCREA)                                       
  
 	ElseIf  EMPTY(SE2->E2_DECRESC)
  	
  		_VENCVAL  :=  "00000000"
    
   	Endif

ENDIF

Return(_VENC)   



#include "rwmake.ch"        


User Function PAGVENC3()

//SetPrvt("_VENC")

Local _VENC  :=  ""


IF Empty(_VENC)                                
	
	If  !EMPTY(SE2->E2_DECRESC) .AND. SE2->E2_AJUSTE == "DB"
  
  	 	_VENC := DTOS(SE2->E2_VENCREA)                                       
  	
  	ElseIf  !EMPTY(SE2->E2_DECRESC) .AND. SE2->E2_AJUSTE == "AI"
  	
  		_VENC  :=  "00000000"
    
   
 	ElseIf  EMPTY(SE2->E2_DECRESC)
  	
  		_VENC  :=  "00000000"
    
   	Endif

ENDIF

Return(_VENC)   





User Function RETDATE(dData)
Local cDatInv := "" 	
Local cData := Dtos(dData)

cDatInv := Substr(cData,7,2) + Substr(cData,5,2) + Substr(cData,1,4)

Return(cDatInv)   





/*{Protheus.doc} RetModl
Retorna o Tipo de Pagamento para o CNAB do banco 021 - Banestes (Layout: Pagamentos Diversos)
@author Cristia Cerda
@since 07/06/2016
@version 1.0
@return cTppag, ${return_description}

	
User Function PGBC021A( )
Local cTppag 	:= ""   //Tipo Pagamento

If SE2->E2_DIRF = "1" .AND. !EMPTY(SE2->E2_CODRET) .AND. SUBSTR(SE2->E2_CODBAR,1,2) $ "85"
	cTpPag := "GPB"																//GPS c/ codigo de barras
ElseIf !Empty(SE2->E2_CODBAR) .AND. SUBSTR(SE2->E2_CODBAR,1,2) $ "82/83/84"
	cTpPag := "CCS"																//Concession�rias
ElseIf SE2->E2_DIRF = "1" .AND. !Empty(SE2->E2_CODRET) .AND. !Empty(SE2->E2_CODBAR)
	cTpPag := "DRB"																//DARF c/ codigo de barras
ElseIf !Empty(SE2->E2_CODBAR) .AND. SUBSTR(SE2->E2_CODBAR,1,1) <> "8"	
	cTpPag := "COB"																//Boleto Banc�rio
ElseIf SA2->A2_BANCO = "021" .AND. A2_TIPCTA = "1"
	cTpPag := "CC"																//Credito em Conta Corrente BANESTES
ElseIf SA2->A2_BANCO = "021" .AND. A2_TIPCTA = "2"
	cTpPag := "CP"																//Credito em Conta Poupan�a BANESTES
ElseIf	SA2->A2_BANCO <> "021"
	cTpPag := "DOC"																//DOC
EndIf

Return PADR(cTpPag,3)

  
{Protheus.doc} RetModl
Retorna o Tipo de Pagamento para o CNAB do banco 021 - Banestes (Layout: Pagamentos Diversos)
@author Cristia Cerda
@since 07/06/2016
@version 1.0
@return cTppag, ${return_description}


User Function POSI( cCampo )

Local cPosi		:= ""
Local cTipoPag 	:= u_PGBC021A()

If cCampo=="BancoDest" .AND. cTipoPag == "DOC"
	cPosi := 
ElseIF cCampo=="AgenciaDest" .AND. cTipoPag == "DOC"
	cPosi := 
ElseIf cCampo=="NumConta" .AND. cTipoPag $ "DOC/CC /CP "
	cPosi := 
EndIf 

Return cPosi 


u_POSI( "BancoDest" )
u_POSI( "AgenciaDest" )

*/